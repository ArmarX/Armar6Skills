# ==================================================================
# ImageToPointCloudApp properties
# ==================================================================

# ArmarX.AdditionalPackages:  List of additional ArmarX packages which should be in the list of default packages. If you have custom packages, which should be found by the gui or other apps, specify them here. Comma separated List.
#  Attributes:
#  - Default:            Default value not mapped.
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.AdditionalPackages = Default value not mapped.


# ArmarX.ApplicationName:  Application name
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.ApplicationName = ""


# ArmarX.CachePath:  Path for cache files. If relative path AND env. variable ARMARX_CONFIG_DIR is set, the cache path will be made relative to ARMARX_CONFIG_DIR. Otherwise if relative it will be relative to the default ArmarX config dir (${ARMARX_WORKSPACE}/armarx_config)
#  Attributes:
#  - Default:            mongo/.cache
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.CachePath = mongo/.cache


# ArmarX.Config:  Comma-separated list of configuration files 
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Config = ""


# ArmarX.DataPath:  Semicolon-separated search list for data files
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.DataPath = ""


# ArmarX.DefaultPackages:  List of ArmarX packages which are accessible by default. Comma separated List. If you want to add your own packages and use all default ArmarX packages, use the property 'AdditionalPackages'.
#  Attributes:
#  - Default:            Default value not mapped.
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.DefaultPackages = Default value not mapped.


# ArmarX.DependenciesConfig:  Path to the (usually generated) config file containing all data paths of all dependent projects. This property usually does not need to be edited.
#  Attributes:
#  - Default:            ./config/dependencies.cfg
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.DependenciesConfig = ./config/dependencies.cfg


# ArmarX.DisableLogging:  Turn logging off in whole application
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.DisableLogging = false


# ArmarX.EnableProfiling:  Enable profiling of CPU load produced by this application
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.EnableProfiling = false


# ArmarX.ImageToPointCloud.CalibrationProviderName:  CalibrationProvider name. Optional. Useful if the images are processed and forwarded.
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
ArmarX.ImageToPointCloud.CalibrationProviderName = "OpenNIPointCloudProvider"


# ArmarX.ImageToPointCloud.CompressionQuality:  Quality of the compression: PNG: 0-9 (9: best compression, but slowest), JPEG: 0-100 (100: best quality) 
#  Attributes:
#  - Default:            95
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.ImageToPointCloud.CompressionQuality = 95


# ArmarX.ImageToPointCloud.CompressionType:  Compression algorithms to be used. Values: None, PNG, JPEG
#  Attributes:
#  - Default:            None
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {JPEG, JPG, None, PNG}
# ArmarX.ImageToPointCloud.CompressionType = None


# ArmarX.ImageToPointCloud.ComputeViewAngleFromCalibration:  Use the field of view provided by the calibration provider
#  Attributes:
#  - Default:            true
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
ArmarX.ImageToPointCloud.ComputeViewAngleFromCalibration = false


# ArmarX.ImageToPointCloud.DebugObserverName:  Name of the topic the DebugObserver listens on
#  Attributes:
#  - Default:            DebugObserver
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.ImageToPointCloud.DebugObserverName = DebugObserver


# ArmarX.ImageToPointCloud.EnableProfiling:  enable profiler which is used for logging performance events
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.ImageToPointCloud.EnableProfiling = false


# ArmarX.ImageToPointCloud.ForceIceTransfer:  If set to true, this image processor will always use the Ice transfer for images instead of shared memory.
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.ImageToPointCloud.ForceIceTransfer = false


# ArmarX.ImageToPointCloud.HorizontalViewAngle:  The horizontal viewing angle of the camera. Only used when no calibration can be retrieved.
#  Attributes:
#  - Default:            57
#  - Case sensitivity:   yes
#  - Required:           no
ArmarX.ImageToPointCloud.HorizontalViewAngle = 75


# ArmarX.ImageToPointCloud.MaxDist:  The maximum distance for measurements. Ignored if smaller than zero
#  Attributes:
#  - Default:            -1
#  - Case sensitivity:   yes
#  - Required:           no
ArmarX.ImageToPointCloud.MaxDist = 2000


# ArmarX.ImageToPointCloud.MinDist:  The minimum distance for measurements. 
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.ImageToPointCloud.MinDist = 0


# ArmarX.ImageToPointCloud.MinimumLoggingLevel:  Local logging level only for this component
#  Attributes:
#  - Default:            Undefined
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {Debug, Error, Fatal, Important, Info, Undefined, Verbose, Warning}
# ArmarX.ImageToPointCloud.MinimumLoggingLevel = Undefined


# ArmarX.ImageToPointCloud.NaNValue:  NaN value to use if reported disparity is not available.
#  Attributes:
#  - Default:            -1
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.ImageToPointCloud.NaNValue = -1


# ArmarX.ImageToPointCloud.ObjectName:  Name of IceGrid well-known object
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
ArmarX.ImageToPointCloud.ObjectName = MaskRCNNPointCloudProvider


# ArmarX.ImageToPointCloud.PointCloudHeight:  Must match the image height (cannot be read automaically from imageprovider as early as needed)
#  Attributes:
#  - Default:            480
#  - Case sensitivity:   yes
#  - Required:           no
ArmarX.ImageToPointCloud.PointCloudHeight = 720


# ArmarX.ImageToPointCloud.PointCloudRotationZ:  Rotation around Z of the provided pointcloud
#  Attributes:
#  - Default:            180
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.ImageToPointCloud.PointCloudRotationZ = 180


# ArmarX.ImageToPointCloud.PointCloudWidth:  Must match the image width (cannot be read automaically from imageprovider as early as needed)
#  Attributes:
#  - Default:            640
#  - Case sensitivity:   yes
#  - Required:           no
ArmarX.ImageToPointCloud.PointCloudWidth = 1280


# ArmarX.ImageToPointCloud.ProviderName:  ImageProvider name. image[0]: rgb image, image[1]: depth image, image[2](optional): label integer
#  Attributes:
#  - Case sensitivity:   yes
#  - Required:           yes
ArmarX.ImageToPointCloud.ProviderName = MaskRCNNImageProcessorResult


# ArmarX.ImageToPointCloud.VerticalViewAngle:  The vertical viewing angle of the camera. Only used when no calibration can be retrieved.
#  Attributes:
#  - Default:            43
#  - Case sensitivity:   yes
#  - Required:           no
ArmarX.ImageToPointCloud.VerticalViewAngle = 65


# ArmarX.LoadLibraries:  Libraries to load at start up of the application. Must be enabled by the Application with enableLibLoading(). Format: PackageName:LibraryName;... or /absolute/path/to/library;...
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.LoadLibraries = ""


# ArmarX.LoggingGroup:  The logging group is transmitted with every ArmarX log message over Ice in order to group the message in the GUI.
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.LoggingGroup = ""


# ArmarX.RedirectStdout:  Redirect std::cout and std::cerr to ArmarXLog
#  Attributes:
#  - Default:            true
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.RedirectStdout = true


# ArmarX.RemoteHandlesDeletionTimeout:  The timeout (in ms) before a remote handle deletes the managed object after the use count reached 0. This time can be used by a client to increment the count again (may be required when transmitting remote handles)
#  Attributes:
#  - Default:            3000
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.RemoteHandlesDeletionTimeout = 3000


# ArmarX.SecondsStartupDelay:  The startup will be delayed by this number of seconds (useful for debugging)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.SecondsStartupDelay = 0


# ArmarX.StartDebuggerOnCrash:  If this application crashes (segmentation fault) qtcreator will attach to this process and start the debugger.
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.StartDebuggerOnCrash = false


# ArmarX.ThreadPoolSize:  Size of the ArmarX ThreadPool that is always running.
#  Attributes:
#  - Default:            1
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.ThreadPoolSize = 1


# ArmarX.TopicSuffix:  Suffix appended to all topic names for outgoing topics. This is mainly used to direct all topics to another name for TopicReplaying purposes.
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.TopicSuffix = ""


# ArmarX.UseTimeServer:  Enable using a global Timeserver (e.g. from ArmarXSimulator)
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.UseTimeServer = false


# ArmarX.Verbosity:  Global logging level for whole application
#  Attributes:
#  - Default:            Info
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {Debug, Error, Fatal, Important, Info, Undefined, Verbose, Warning}
# ArmarX.Verbosity = Info


