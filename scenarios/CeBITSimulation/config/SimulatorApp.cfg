# ==================================================================
# SimulatorApp properties
# ==================================================================

# ArmarX.AdditionalPackages:  List of additional ArmarX packages which should be in the list of default packages. If you have custom packages, which should be found by the gui or other apps, specify them here. Comma separated List.
#  Attributes:
#  - Default:            Default value not mapped.
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.AdditionalPackages = Default value not mapped.


# ArmarX.ApplicationName:  Application name
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.ApplicationName = ""


# ArmarX.CachePath:  Path for cache files. If relative path AND env. variable ARMARX_CONFIG_DIR is set, the cache path will be made relative to ARMARX_CONFIG_DIR. Otherwise if relative it will be relative to the default ArmarX config dir (${ARMARX_WORKSPACE}/armarx_config)
#  Attributes:
#  - Default:            mongo/.cache
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.CachePath = mongo/.cache


# ArmarX.Config:  Comma-separated list of configuration files 
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Config = ""


# ArmarX.DataPath:  Semicolon-separated search list for data files
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.DataPath = ""


# ArmarX.DefaultPackages:  List of ArmarX packages which are accessible by default. Comma separated List. If you want to add your own packages and use all default ArmarX packages, use the property 'AdditionalPackages'.
#  Attributes:
#  - Default:            Default value not mapped.
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.DefaultPackages = Default value not mapped.


# ArmarX.DependenciesConfig:  Path to the (usually generated) config file containing all data paths of all dependent projects. This property usually does not need to be edited.
#  Attributes:
#  - Default:            ./config/dependencies.cfg
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.DependenciesConfig = ./config/dependencies.cfg


# ArmarX.DisableLogging:  Turn logging off in whole application
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.DisableLogging = false


# ArmarX.EnableProfiling:  Enable profiling of CPU load produced by this application
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.EnableProfiling = false


# ArmarX.LoadLibraries:  Libraries to load at start up of the application. Must be enabled by the Application with enableLibLoading(). Format: PackageName:LibraryName;... or /absolute/path/to/library;...
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.LoadLibraries = ""


# ArmarX.LoggingGroup:  The logging group is transmitted with every ArmarX log message over Ice in order to group the message in the GUI.
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.LoggingGroup = ""


# ArmarX.RedirectStdout:  Redirect std::cout and std::cerr to ArmarXLog
#  Attributes:
#  - Default:            true
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.RedirectStdout = true


# ArmarX.RemoteHandlesDeletionTimeout:  The timeout (in ms) before a remote handle deletes the managed object after the use count reached 0. This time can be used by a client to increment the count again (may be required when transmitting remote handles)
#  Attributes:
#  - Default:            3000
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.RemoteHandlesDeletionTimeout = 3000


# ArmarX.SecondsStartupDelay:  The startup will be delayed by this number of seconds (useful for debugging)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.SecondsStartupDelay = 0


# ArmarX.Simulator.CommonStorageName:  Name of CommonStorage
#  Attributes:
#  - Default:            CommonStorage
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.CommonStorageName = CommonStorage


# ArmarX.Simulator.DrawCoMVisu:  If true the CoM of each robot is drawn as a circle on the ground after each simulation cycle.
#  Attributes:
#  - Default:            true
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.DrawCoMVisu = true


# ArmarX.Simulator.EnableProfiling:  enable profiler which is used for logging performance events
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.EnableProfiling = false


# ArmarX.Simulator.FixedObjects:  Fixate objects or parts of a robot in the world. Comma separated list. 
# Define objects by their name, robots can be defined with robotName:RobotNodeName
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.FixedObjects = ""


# ArmarX.Simulator.FixedTimeStepLoopNrSteps:  The maximum number of internal simulation loops (fixed time step mode).
# After max number of time steps, the remaining time is interpolated.
#  Attributes:
#  - Default:            10
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.FixedTimeStepLoopNrSteps = 10


# ArmarX.Simulator.FixedTimeStepStepTimeMS:  The simulation's internal timestep (fixed time step mode). 
# This property determines the precision of the simulation. 
# Needs to be set to bigger values for slow PCs, but the simulation results might be bad. Smaller value for higher precision, but slower calculation.
#  Attributes:
#  - Default:            16
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.FixedTimeStepStepTimeMS = 16


# ArmarX.Simulator.FloorPlane:  Enable floor plane.
#  Attributes:
#  - Default:            true
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.FloorPlane = true


# ArmarX.Simulator.FloorTexture:  Texture file for floor.
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.FloorTexture = ""


# ArmarX.Simulator.InitialRobotConfig:  Initial robot config as comma separated list (RobotNodeName:value)
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
ArmarX.Simulator.InitialRobotConfig = ArmR5_Elb1:1.57, ArmL5_Elb1:1.57


# ArmarX.Simulator.InitialRobotConfig_0:  Initial robot config as comma separated list (RobotNodeName:value)
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotConfig_0 = ""


# ArmarX.Simulator.InitialRobotConfig_1:  Initial robot config as comma separated list (RobotNodeName:value)
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotConfig_1 = ""


# ArmarX.Simulator.InitialRobotConfig_2:  Initial robot config as comma separated list (RobotNodeName:value)
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotConfig_2 = ""


# ArmarX.Simulator.InitialRobotPose.pitch:  Initial robot pose: pitch component of RPY angles (radian)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.pitch = 0


# ArmarX.Simulator.InitialRobotPose.pitch_0:  Initial robot pose: pitch component of RPY angles (radian)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.pitch_0 = 0


# ArmarX.Simulator.InitialRobotPose.pitch_1:  Initial robot pose: pitch component of RPY angles (radian)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.pitch_1 = 0


# ArmarX.Simulator.InitialRobotPose.pitch_2:  Initial robot pose: pitch component of RPY angles (radian)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.pitch_2 = 0


# ArmarX.Simulator.InitialRobotPose.roll:  Initial robot pose: roll component of RPY angles (radian)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.roll = 0


# ArmarX.Simulator.InitialRobotPose.roll_0:  Initial robot pose: roll component of RPY angles (radian)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.roll_0 = 0


# ArmarX.Simulator.InitialRobotPose.roll_1:  Initial robot pose: roll component of RPY angles (radian)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.roll_1 = 0


# ArmarX.Simulator.InitialRobotPose.roll_2:  Initial robot pose: roll component of RPY angles (radian)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.roll_2 = 0


# ArmarX.Simulator.InitialRobotPose.x:  x component of initial robot position (mm)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
ArmarX.Simulator.InitialRobotPose.x = 4680


# ArmarX.Simulator.InitialRobotPose.x_0:  x component of initial robot position (mm)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.x_0 = 0


# ArmarX.Simulator.InitialRobotPose.x_1:  x component of initial robot position (mm)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.x_1 = 0


# ArmarX.Simulator.InitialRobotPose.x_2:  x component of initial robot position (mm)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.x_2 = 0


# ArmarX.Simulator.InitialRobotPose.y:  y component of initial robot position (mm)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
ArmarX.Simulator.InitialRobotPose.y = 1300


# ArmarX.Simulator.InitialRobotPose.y_0:  y component of initial robot position (mm)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.y_0 = 0


# ArmarX.Simulator.InitialRobotPose.y_1:  y component of initial robot position (mm)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.y_1 = 0


# ArmarX.Simulator.InitialRobotPose.y_2:  y component of initial robot position (mm)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.y_2 = 0


# ArmarX.Simulator.InitialRobotPose.yaw:  Initial robot pose: yaw component of RPY angles (radian)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
ArmarX.Simulator.InitialRobotPose.yaw = 0


# ArmarX.Simulator.InitialRobotPose.yaw_0:  Initial robot pose: yaw component of RPY angles (radian)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.yaw_0 = 0


# ArmarX.Simulator.InitialRobotPose.yaw_1:  Initial robot pose: yaw component of RPY angles (radian)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.yaw_1 = 0


# ArmarX.Simulator.InitialRobotPose.yaw_2:  Initial robot pose: yaw component of RPY angles (radian)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.yaw_2 = 0


# ArmarX.Simulator.InitialRobotPose.z:  z component of initial robot position (mm)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.z = 0


# ArmarX.Simulator.InitialRobotPose.z_0:  z component of initial robot position (mm)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.z_0 = 0


# ArmarX.Simulator.InitialRobotPose.z_1:  z component of initial robot position (mm)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.z_1 = 0


# ArmarX.Simulator.InitialRobotPose.z_2:  z component of initial robot position (mm)
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.InitialRobotPose.z_2 = 0


# ArmarX.Simulator.LoadAgentsFromSnapshot:  Also load the agents from the snapshot into the WorkingMemory
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.LoadAgentsFromSnapshot = false


# ArmarX.Simulator.LoadSnapshotToWorkingMemory:  Load the snapshot also into the WorkingMemory
#  Attributes:
#  - Default:            true
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
ArmarX.Simulator.LoadSnapshotToWorkingMemory = 0


# ArmarX.Simulator.LogRobot:  Enable robot logging. If true, the complete robot state is logged to a file each step.
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.LogRobot = false


# ArmarX.Simulator.LongtermMemory.SnapshotName:  Name of snapshot to load the scene
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.LongtermMemory.SnapshotName = ""


# ArmarX.Simulator.LongtermMemoryName:  Name of LongtermMemory
#  Attributes:
#  - Default:            LongtermMemory
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.LongtermMemoryName = LongtermMemory


# ArmarX.Simulator.MaxRealTime:  If not zero and the real time mode is off, the simulator will not run 
# 	faster than real-time * MaxRealTime even if the machine is fast enough 
# 	(a sleep is inserted to slow down the simulator - the precision remains unchanged).
# 	If set to 2, the simulator will not run faster than double speed. 
# 	If set to 0.5, the simulator will not run faster than half speed. 
# 	If set to 0, the simulator will run as fast as possible.
# 
#  Attributes:
#  - Default:            1
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.MaxRealTime = 1


# ArmarX.Simulator.MinimumLoggingLevel:  Local logging level only for this component
#  Attributes:
#  - Default:            Undefined
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {Debug, Error, Fatal, Important, Info, Undefined, Verbose, Warning}
# ArmarX.Simulator.MinimumLoggingLevel = Undefined


# ArmarX.Simulator.ObjectName:  Name of IceGrid well-known object
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.ObjectName = ""


# ArmarX.Simulator.PriorKnowledgeName:  Name of PriorKnowledge
#  Attributes:
#  - Default:            PriorKnowledge
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.PriorKnowledgeName = PriorKnowledge


# ArmarX.Simulator.RealTimeMode:  If true the timestep is adjusted to the computing power of the host and 
# the time progress in real time. Can result in bad results on slow PCs. 
# The property StepTimeMS has then no effect. FixedTimeStepStepTimeMS needs 
# to be adjusted if real time cannot be achieved on slower PCs.
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.RealTimeMode = false


# ArmarX.Simulator.ReportDataFrequency:  How often should the robot data be published. Value is given in Hz. (0 to disable)
#  Attributes:
#  - Default:            30
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.ReportDataFrequency = 30


# ArmarX.Simulator.ReportRobotPose:  Report the global robot pose.
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.ReportRobotPose = false


# ArmarX.Simulator.ReportVisuFrequency:  How often should the visualization data be published. Value is given in Hz. (0 to disable)
#  Attributes:
#  - Default:            30
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.ReportVisuFrequency = 30


# ArmarX.Simulator.ReportVisuTopicName:  The topic on which the visualization updates are published.
#  Attributes:
#  - Default:            SimulatorVisuUpdates
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.ReportVisuTopicName = SimulatorVisuUpdates


# ArmarX.Simulator.RobotCollisionModel:  Show the collision model of the robot.
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.RobotCollisionModel = false


# ArmarX.Simulator.RobotCollisionModel_0:  Show the collision model of the robot.
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.RobotCollisionModel_0 = false


# ArmarX.Simulator.RobotCollisionModel_1:  Show the collision model of the robot.
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.RobotCollisionModel_1 = false


# ArmarX.Simulator.RobotCollisionModel_2:  Show the collision model of the robot.
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.RobotCollisionModel_2 = false


# ArmarX.Simulator.RobotControllerPID.d:  Setup robot controllers: PID paramter d.
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotControllerPID.d = 0


# ArmarX.Simulator.RobotControllerPID.d_0:  Setup robot controllers: PID paramter d.
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotControllerPID.d_0 = 0


# ArmarX.Simulator.RobotControllerPID.d_1:  Setup robot controllers: PID paramter d.
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotControllerPID.d_1 = 0


# ArmarX.Simulator.RobotControllerPID.d_2:  Setup robot controllers: PID paramter d.
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotControllerPID.d_2 = 0


# ArmarX.Simulator.RobotControllerPID.i:  Setup robot controllers: PID paramter i.
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotControllerPID.i = 0


# ArmarX.Simulator.RobotControllerPID.i_0:  Setup robot controllers: PID paramter i.
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotControllerPID.i_0 = 0


# ArmarX.Simulator.RobotControllerPID.i_1:  Setup robot controllers: PID paramter i.
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotControllerPID.i_1 = 0


# ArmarX.Simulator.RobotControllerPID.i_2:  Setup robot controllers: PID paramter i.
#  Attributes:
#  - Default:            0
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotControllerPID.i_2 = 0


# ArmarX.Simulator.RobotControllerPID.p:  Setup robot controllers: PID paramter p.
#  Attributes:
#  - Default:            10
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotControllerPID.p = 10


# ArmarX.Simulator.RobotControllerPID.p_0:  Setup robot controllers: PID paramter p.
#  Attributes:
#  - Default:            10
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotControllerPID.p_0 = 10


# ArmarX.Simulator.RobotControllerPID.p_1:  Setup robot controllers: PID paramter p.
#  Attributes:
#  - Default:            10
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotControllerPID.p_1 = 10


# ArmarX.Simulator.RobotControllerPID.p_2:  Setup robot controllers: PID paramter p.
#  Attributes:
#  - Default:            10
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotControllerPID.p_2 = 10


# ArmarX.Simulator.RobotFileName:  Simox/VirtualRobot robot file name, e.g. robot_model.xml
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotFileName = ""


# ArmarX.Simulator.RobotFileName_0:  Simox/VirtualRobot robot file name, e.g. robot_model.xml
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotFileName_0 = ""


# ArmarX.Simulator.RobotFileName_1:  Simox/VirtualRobot robot file name, e.g. robot_model.xml
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotFileName_1 = ""


# ArmarX.Simulator.RobotFileName_2:  Simox/VirtualRobot robot file name, e.g. robot_model.xml
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotFileName_2 = ""


# ArmarX.Simulator.RobotInstanceName:  Name of this robot instance
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotInstanceName = ""


# ArmarX.Simulator.RobotInstanceName_0:  Name of this robot instance
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotInstanceName_0 = ""


# ArmarX.Simulator.RobotInstanceName_1:  Name of this robot instance
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotInstanceName_1 = ""


# ArmarX.Simulator.RobotInstanceName_2:  Name of this robot instance
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotInstanceName_2 = ""


# ArmarX.Simulator.RobotIsStatic:  A static robot does not move due to the physical environment (i.e. no gravity). 
# It is a static (but kinematic) object in the world.
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.RobotIsStatic = false


# ArmarX.Simulator.RobotIsStatic_0:  A static robot does not move due to the physical environment (i.e. no gravity). 
# It is a static (but kinematic) object in the world.
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.RobotIsStatic_0 = false


# ArmarX.Simulator.RobotIsStatic_1:  A static robot does not move due to the physical environment (i.e. no gravity). 
# It is a static (but kinematic) object in the world.
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.RobotIsStatic_1 = false


# ArmarX.Simulator.RobotIsStatic_2:  A static robot does not move due to the physical environment (i.e. no gravity). 
# It is a static (but kinematic) object in the world.
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.RobotIsStatic_2 = false


# ArmarX.Simulator.RobotScaling:  Scaling of the robot (1 = no scaling).
#  Attributes:
#  - Default:            1
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotScaling = 1


# ArmarX.Simulator.RobotScaling_0:  Scaling of the robot (1 = no scaling).
#  Attributes:
#  - Default:            1
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotScaling_0 = 1


# ArmarX.Simulator.RobotScaling_1:  Scaling of the robot (1 = no scaling).
#  Attributes:
#  - Default:            1
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotScaling_1 = 1


# ArmarX.Simulator.RobotScaling_2:  Scaling of the robot (1 = no scaling).
#  Attributes:
#  - Default:            1
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.RobotScaling_2 = 1


# ArmarX.Simulator.RobotSelfCollisions:  If false, the robot bodies will not collide with other bodies of this 
# robot but with all other bodies in the world. 
# If true, the robot bodies will collide with all other bodies 
# (This needs an accurately modelled robot model without self collisions 
# if the robot does not move.)
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.RobotSelfCollisions = false


# ArmarX.Simulator.RobotSelfCollisions_0:  If false, the robot bodies will not collide with other bodies of this 
# robot but with all other bodies in the world. 
# If true, the robot bodies will collide with all other bodies 
# (This needs an accurately modelled robot model without self collisions 
# if the robot does not move.)
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.RobotSelfCollisions_0 = false


# ArmarX.Simulator.RobotSelfCollisions_1:  If false, the robot bodies will not collide with other bodies of this 
# robot but with all other bodies in the world. 
# If true, the robot bodies will collide with all other bodies 
# (This needs an accurately modelled robot model without self collisions 
# if the robot does not move.)
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.RobotSelfCollisions_1 = false


# ArmarX.Simulator.RobotSelfCollisions_2:  If false, the robot bodies will not collide with other bodies of this 
# robot but with all other bodies in the world. 
# If true, the robot bodies will collide with all other bodies 
# (This needs an accurately modelled robot model without self collisions 
# if the robot does not move.)
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.Simulator.RobotSelfCollisions_2 = false


# ArmarX.Simulator.Scene.ObjectPackage:  Package to search for object models.Used when loading scenes from JSON files.
#  Attributes:
#  - Default:            PriorKnowledgeData
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.Scene.ObjectPackage = PriorKnowledgeData


# ArmarX.Simulator.Scene.Path:  Path to one or multiple JSON scene file(s) to load.
# Example: 'PriorKnowledgeData/scenes/MyScene.json'.
# Multiple paths are separated by semicolons (';').
# The package to load a scene from is automaticallyextracted from its first path segment.
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
ArmarX.Simulator.Scene.Path = PriorKnowledgeData/scenes/R003.json


# ArmarX.Simulator.SimoxSceneFileName:  Simox/VirtualRobot scene file name, e.g. myScene.xml
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.SimoxSceneFileName = ""


# ArmarX.Simulator.SimulationType:  Simulation type (Supported: Bullet, Kinematics, Mujoco)
#  Attributes:
#  - Default:            bullet
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {bullet, kinematics, mujoco}
ArmarX.Simulator.SimulationType = kinematics


# ArmarX.Simulator.StepTimeMS:  The simulation's time progress per step. 
#  Attributes:
#  - Default:            25
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.StepTimeMS = 25


# ArmarX.Simulator.WorkingMemoryName:  Name of WorkingMemory
#  Attributes:
#  - Default:            WorkingMemory
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.Simulator.WorkingMemoryName = WorkingMemory


# ArmarX.Simulator.inheritFrom:  No Description
#  Attributes:
#  - Default:            RobotConfig
#  - Case sensitivity:   no
#  - Required:           no
ArmarX.Simulator.inheritFrom = RobotConfig


# ArmarX.StartDebuggerOnCrash:  If this application crashes (segmentation fault) qtcreator will attach to this process and start the debugger.
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.StartDebuggerOnCrash = false


# ArmarX.ThreadPoolSize:  Size of the ArmarX ThreadPool that is always running.
#  Attributes:
#  - Default:            1
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.ThreadPoolSize = 1


# ArmarX.TopicSuffix:  Suffix appended to all topic names for outgoing topics. This is mainly used to direct all topics to another name for TopicReplaying purposes.
#  Attributes:
#  - Default:            ""
#  - Case sensitivity:   yes
#  - Required:           no
# ArmarX.TopicSuffix = ""


# ArmarX.UseTimeServer:  Enable using a global Timeserver (e.g. from ArmarXSimulator)
#  Attributes:
#  - Default:            false
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {0, 1, false, no, true, yes}
# ArmarX.UseTimeServer = false


# ArmarX.Verbosity:  Global logging level for whole application
#  Attributes:
#  - Default:            Info
#  - Case sensitivity:   yes
#  - Required:           no
#  - Possible values: {Debug, Error, Fatal, Important, Info, Undefined, Verbose, Warning}
# ArmarX.Verbosity = Info


