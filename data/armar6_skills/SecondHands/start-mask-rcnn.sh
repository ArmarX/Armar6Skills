#!/bin/bash

trap 'echo "Killing childern $(jobs -p)"; kill $(jobs -p)' EXIT SIGTERM SIGINT

echo "Sourcing Anaconda"
source $HOME/repos/anaconda3/bin/activate ""
echo "Activating tf-gpu env"
conda activate tf-gpu

cd $HOME/repos/sh/mask-rcnn/ocado_ucl_repo/
export PYTHONPATH=$PYTHONPATH:./

echo "Passing parameter to MaskRCNN image processor: $1"
python EXAMPLES/KIT/maskrcnn_image_processor.py $1 2>&1 &

child=$!
wait "$child"






