#!/bin/bash

trap 'kill $(jobs -p)' EXIT SIGTERM SIGINT

echo "Sourcing Anaconda"
source $HOME/repos/anaconda3/bin/activate ""
echo "Activating tf-gpu env"
conda activate tf-gpu

cd $HOME/repos/sh/pose-estimation/ocado_ucl_repo/
export PYTHONPATH=$PYTHONPATH:./

echo "Passing parameter to UCL pose estimation: $1"
python EXAMPLES/KIT/armarx_secondhands.py --visualize-icp False $1 2>&1 &

child=$!
wait "$child"


