#!/usr/bin/env python3
# pylint: disable=missing-docstring

import logging
import time

from armarx.parser import ArmarXArgumentParser as ArgumentParser

from demo import helper
from demo.helper import handover, robot, move_to, move_joints, say

logger = logging.getLogger(__name__)


def main():
    parser = ArgumentParser('KIT science week opening demo script')
    parser.parse_args()

    # helper.wait_for_input()

    robot.init_pose()
    for i in range(2):
        robot.open_hand('right')
    # move_to('start')

    logger.debug('testing handover statechart')

    move_joints('carry_01')
    handover()
    move_joints('carry_01')

    time.sleep(2)
    robot.close_hand('right')

    time.sleep(2)
    # move_to('hallway')


if __name__ == '__main__':
    # main()
    main()
    pass
