#!/usr/bin/env python3
# pylint: disable=missing-docstring

import os
import logging
import time

from armarx import slice_loader
from armarx.parser import ArmarXArgumentParser as ArgumentParser
from armarx import arviz as viz

slice_loader.load_armarx_slice("RobotAPI", "core/FramedPoseBase.ice")
from armarx import FramedPositionBase

from demo import helper
from demo.helper import move_to, move_joints, say, robot
from demo.helper import handover

logger = logging.getLogger(__name__)
arviz = viz.Client(os.path.basename(__name__))


def run_demo():
    do_hallway = do_corner = do_handover = do_present = do_return = False
    do_hallway = True
    do_corner = True
    do_handover = True
    do_present = True
    do_return = True

    helper.visu_locations(arviz)

    if do_hallway:
        # move_to('hallway')  # Done in handover_test.
        # move_to('behind-pcs')
        # move_to('corner-01')
        move_to('corner-02')
        # move_to('corner-out')
    
    logger.info("Go?")
    if not helper.wait_for_input():
        return

    if do_corner:
        move_joints("look-at-viewers")
        move_to('corner-to-handover')

    if do_handover:
        f = move_to('to-handover', True)
        say('Schau mal was ich gefunden habe. Hilft dir das?')
        move_joints('carry_waving_03')
        time.sleep(0)
        move_joints("look-ahead")

        do_waving = False
        if do_waving:
            while not f.done():
                move_joints('carry_waving_03')
                time.sleep(0.8)
                move_joints('carry_waving_04')
                time.sleep(0.8)
        else:
            f.result()

        move_to('handover')
        move_joints('carry_01_w_left')

        say('Bitte sehr.')
        handover()

        robot.gaze.fixate(FramedPositionBase(0, 1000, 1650, frame='root', agent='Armar6'))
        time.sleep(1.0)

        say('Gerne.')

    if do_present:
        # Dialog Mod.: Wer bist du denn?
        time.sleep(2)

        f = move_to('present', move_async=True)
        move_joints('init', wait=False)
        for i in range(2):
            robot.open_hand('right')
        # f.result()

        time.sleep(1.5)
        say('Ich bin armar 6!  Ich wurde am KIT  entwickelt, um Menschen im Alltag '
            'und bei der Arbeit zu unterstützen.'
            'Durch meine Künstliche Intelligenz kann ich vom Menschen lernen '
            'und mit Menschen in natürlicher Sprache kommunizieren')

        # for i in range(1, 7):
        for i in range(1, 6):
            move_joints(f'configWave{i}')
        # time.sleep(1.0)

        # say('Ich freue mich, bei der KIT Science Week dabei sein zu dürfen.')
        say('Ich freue mich bei der KIT Science Week mit <phoneme alphabet="ipa" ph="daːbaɪ̯ zaɪ̯n">dabei sein</phoneme> zu dürfen')
        move_joints('init', wait=False)
        time.sleep(5.0)

    if do_return:
        f = move_to('turn-back', True)
        # move_joints('init')
        f.result()

        move_to("return-corner-01")
        move_to("return-corner-02")
        move_to("return-hallway")
        move_to("return-end")

    logger.info("Finished.")


def main():
    parser = ArgumentParser('KIT science week opening demo script')
    parser.parse_args()
    run_demo()


if __name__ == '__main__':
    main()
    pass
