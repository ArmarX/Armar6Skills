#pylint: disable=missing-docstring

import os
import time
import logging
import json
from typing import Dict

import numpy as np

from armarx import SimpleStatechartExecutorInterfacePrx

from armarx.robots import A6
from armarx import arviz as viz

logger = logging.getLogger(__name__)
arviz = viz.Client(os.path.basename(__name__))



def get_robot_configs():
    configs_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'configs.json')
    with open(configs_path) as f:
        robot_configs = json.load(f)
    return robot_configs


def get_robot_locations():
    locations_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'locations.json')
    with open(locations_path) as f:
        robot_locations = json.load(f)
        robot_locations: Dict[str, np.ndarray] = {
            name: np.array(pose) for name, pose in robot_locations.items()
        }
    return robot_locations


robot = A6()
robot_configs = get_robot_configs()
robot_locations = get_robot_locations()


def handover() -> bool:
    parameter = {}
    #{ 'HandName': StateParameterIceBase('Hand_R_EEF', None, True, False) }

    executor = SimpleStatechartExecutorInterfacePrx.get_proxy()
    group_fullname = 'Armar6RealHandOverGroupRemoteStateOfferer'
    name = 'ReceiveFromRobot'


    if not executor.hasExecutionFinished():
        logger.warning('Another statechart is currently executed. Ignoring this task')
        executor.stopImmediatly()
        return False

    libraries = ['::armarx::Vector3Base', '::armarx::FramedOrientationBase',
                 '::armarx::FramedPositionBase']

    logger.debug('loading libraries %s', libraries)
    executor.preloadLibrariesFromHumanNames(libraries)

    if executor.startStatechart(group_fullname, name, parameter):
        logger.debug(f'starting {name}')
    else:
        logger.warning(f'unable to start {name}')
        return False

    while not executor.hasExecutionFinished():
        time.sleep(0.05)

    return True


def visu_locations(arviz):
    import transforms3d as tf3d
    with arviz.begin_stage() as stage:
        layer = stage.layer("Locations")
        for k, v in robot_locations.items():
            position = (v[0], v[1], 0)
            orientation = tf3d.axangles.axangle2mat((0, 0, 1), v[2])
            layer.add(viz.Pose(k, position=position, orientation=orientation,
                               scale=10))

        arviz.commit(stage)


visu_locations(arviz=arviz)



def move_to(name: str, move_async: bool = False):
    logger.debug('moving robot to %s', name)
    if move_async:
        return robot.navigator.movePlatformAsync(*robot_locations[name])
    return robot.navigator.movePlatform(*robot_locations[name])


def move_joints(name: str, wait: bool = True):
    logger.debug('moving joints to %s', name)
    robot.move_joints(robot_configs[name])

    if wait:
        robot.wait_for_joints(robot_configs[name])


def say(text: str):
    logger.debug('saying "%s"', text)
    robot.say(f'<speak><voice language="de-de">{text}</voice></speak>')


def wait_for_input() -> bool:
    try:
        logger.info("Press enter to go or Ctrl+C to abort.")
        i = input()
        return True
    except Exception:
        # PyCharm raises a custom exception (instead of KeyboardInterrupt)
        # when pressing Ctrl+C in Python Console mode.
        return False
