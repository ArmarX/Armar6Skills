#!/usr/bin/env python3
# pylint: disable=missing-docstring

import os
import logging
import time

from armarx import slice_loader
from armarx.parser import ArmarXArgumentParser as ArgumentParser
from armarx import arviz as viz

from demo import helper
from demo.helper import move_to, move_joints, say, robot
from demo.helper import handover

logger = logging.getLogger(__name__)
arviz = viz.Client(os.path.basename(__name__))


def run_demo():

    move_joints("birthday-init")
    # move_joints("birthday-goal")

    logger.info("Finished.")


def main():
    parser = ArgumentParser('KIT science week opening demo script')
    parser.parse_args()
    run_demo()


if __name__ == '__main__':
    main()
