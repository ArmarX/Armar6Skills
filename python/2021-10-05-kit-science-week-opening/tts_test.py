#!/usr/bin/env python3
# pylint: disable=missing-docstring

import logging
import time

from armarx.parser import ArmarXArgumentParser as ArgumentParser

from demo.helper import handover, robot, move_to, move_joints, say

logger = logging.getLogger(__name__)


def main():
    parser = ArgumentParser('KIT science week opening demo script')
    parser.parse_args()

    texts = ["Bitte sehr.", "Gern geschehen."]

    for text in texts:
        say(text)
        time.sleep(2)


if __name__ == '__main__':
    main()
    pass
