#!/usr/bin/env python3

import json
import time
import math
import numpy as np

from typing import Dict, List

from armarx.robots import A6


def main():
    with open('poses.json') as f:
        robot_poses = json.load(f)
    with open('locations.json') as f:
        robot_locations = json.load(f)
        robot_locations: Dict[str, np.ndarray] = {
            name: np.array(pose) for name, pose in robot_locations.items()
        }
    print("Loaded configurations: \n- {}".format("\n- ".join(robot_poses.keys())))
    print("Loaded locations: \n- {}".format("\n- ".join(robot_locations.keys())))

    robot = A6()

    def say(text: str, sleep_s=0.0):
        print(f"Saying: \n'{text}' and sleeping for {sleep_s} s")
        robot.say(text)
        if sleep_s > 0:
            time.sleep(sleep_s)


    def move_joints(name: str, sleep_s: float = 1.5):
        print(f"Moving joints to configuration '{name}' and waiting {sleep_s:.2f} s")
        robot.move_joints(robot_poses[name])
        # robot.wait_for_joints(robot_poses[i])
        if sleep_s > 0:
            time.sleep(sleep_s)

    def move_to(name: str):
        print(f"Moving to location '{name}'")
        robot.navigator.movePlatform(*robot_locations[name])

    def wave():
        print("Waving ...")
        move_joints("configWave1")
        move_joints("configWave2")
        move_joints("configWave3")
        move_joints("configWave4")
        move_joints("configWave5")
        move_joints("configWaveEnd")

    prepare_welcome = False
    run_gate = True
    run_room02 = True

    if prepare_welcome:
        move_to("welcome")
        move_joints("wave_static_left_arm")
        exit()

    welcome_text = (
        "Hello! "
        "My Name is Armar6. "
        "I am a humanoid robot developed at KIT to help and assist humans in daily life. "
        "I am happy to welcome you here at the press conference of the KIT science week."
    )

    robot.close_hand()
    robot.open_hand()
    move_joints("init", sleep_s=0)

    run_tests = False
    if run_tests:
        move_joints("init", sleep_s=0)
        move_joints("please_take_your_seat_left", sleep_s=0)
        move_joints("please_take_your_seat_right", sleep_s=0)

        move_joints("head_01", sleep_s=0)
        move_joints("head_02", sleep_s=0)

    exit()

    if run_gate:
        say(welcome_text, sleep_s=15.0)

        say("Let's go to the press conference. I will show you the way.")
        move_to("gate_entrance")

        say("This seems quite narrow. I will make myself a little bit smaller. "
            "Watch out for your heads.",
            sleep_s=4)
        move_joints("gate_entrance", sleep_s=3)

        move_to("gate_middle")
        move_joints("look_ahead", sleep_s=0)

        move_to("gate_exit")
        move_joints("init", sleep_s=0)

        # move_to("post_gate")

    if run_room02:

        move_to("room02_entrance")
        move_to("room02_chairs")
        move_to("room02_podests")

        move_to("room02_please_take_your_seat")

        move_to("room02_final")
        time.sleep(2)

        move_to("room02_please_take_your_seat")

        robot.say("Please take your seat!")
        move_joints("please_take_your_seat_left", sleep_s=0)
        time.sleep(3)

        move_joints("init", sleep_s=0)
        for i in range(3):
            robot.open_hand(hand_name="left")
        time.sleep(2)

        robot.say("Enjoy the press conference!")
        move_to("room02_final")


    print("Finished.")
    exit()






if __name__ == '__main__':
    # main()
    pass
