configure_file("startDockerContainer.sh.in" "${ARMARX_BIN_DIR}/startDockerContainer.sh" @ONLY)
configure_file("stopDockerContainerWatcher.sh.in" "${ARMARX_BIN_DIR}/stopDockerContainerWatcher.sh" @ONLY)

exec_program(chmod "${ARMARX_BIN_DIR}" ARGS a+x *.sh)

set(FILES
    "${ARMARX_BIN_DIR}/startDockerContainer.sh"
    "${ARMARX_BIN_DIR}/stopDockerContainerWatcher.sh"
)

install(PROGRAMS ${FILES} COMPONENT binaries DESTINATION bin)
