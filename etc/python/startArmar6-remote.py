#!/usr/bin/python

import os
import sys
import socket
import argparse
import progressbar
from time import sleep

parser = argparse.ArgumentParser(description='Start the Armar6 Demo Environment on all relevant PCs')
optionalNamed = parser.add_argument_group('properties')
optionalNamed.add_argument('-s', '--silent', action='store_true', dest="silent", default=False, help='Use this script in silent mode')
optionalNamed.add_argument('-n', '--no-setup', action='store_true', dest="setup", default=False, help='Do not use a setup command')
requiredNamed = parser.add_argument_group('modes')
requiredNamed.add_argument('mode', action='store', choices=['basic', 'all', 'vision', 'highlevel'], help='Specify the amount of start applications')


# Variable definitions
BOLD = "\033[1m"
FAIL = "\033[91m"
SUCC = "\033[92m"
ENDC = "\033[0m"

ARMAR6a_REALTIME = "armar6a-0"
ARMAR6a_PLANNING = "armar6a-1"
ARMAR6a_SPEECH = "armar6a-2"
ARMAR6a_VISION = "armar6a-3"
VISUALIZING_PC = "i61pc042"
CONTROL_PC = "i61pc043"

ARMAR6a_USER="armar6-user"

SETUP_COMMAND = ""

args = parser.parse_args(sys.argv[1:])

silent = args.silent
setup = args.setup
mode = args.mode

# Modes
START_RT = False
START_VISION = False
RUN_VISION = False
RUN_HIGHLEVEL = False

if mode == "basic" or mode == "all":
  START_RT = True
if mode == "basic" or mode == "all" or mode == "vision" or mode == "highlevel":
  START_VISION = True
if mode == "all" or mode == "vision":
  RUN_VISION = True
if(mode == "all" or mode == "highlevel"):
  RUN_HIGHLEVEL = True

# RemoteSyncDir
if not("ARMARX_REMOTE_SYNC_DIR" in os.environ):
  USER = os.environ['USER']
  os.environ["ARMARX_REMOTE_SYNC_DIR"] = "armarx_"+USER

# Setup command
SETUP_COMMAND = "true "
if setup:
  PATH = os.environ['PATH']
  SETUP_COMMAND = "export LD_LIBRARY_PATH=\$PATH/$ARMARX_REMOTE_SYNC_DIR/lib:\$HOME/$ARMARX_REMOTE_SYNC_DIR/usr/local/lib:/usr/local/lib/:\$LD_LIBRARY_PATH && export PATH=\$HOME/$ARMARX_REMOTE_SYNC_DIR/bin:\$PATH "
  #&& export ARMARX_USER_CONFIG_DIR=$ARMARX_USER_CONFIG


# Start commands
if START_VISION:
    # then start armarx and memory
    if not(silent):
      print BOLD + "Starting armarx and memoryx on vision pc" + ENDC

    os.system("ssh -XC " + ARMAR6a_USER + "@" + ARMAR6a_VISION + " '" + SETUP_COMMAND + " && ! armarx status &>/dev/null && armarx start' &")
    os.system("ssh -XC " + ARMAR6a_USER + "@" + ARMAR6a_VISION + " '" + SETUP_COMMAND + " && armarx memory assureRunning' &")
    if not(silent):
      print "------------------------------------"


if START_RT:
    # start the RT part and base components on the RT PC
    if not(silent):
      print BOLD + "Starting RealTime Part" + ENDC

    os.system("ssh -XC " + ARMAR6a_USER + "@" + ARMAR6a_REALTIME + " '" + SETUP_COMMAND + " && armarx scenario -p Armar6RT stop Armar6UnitOnly && sleep 2 && armarx scenario -p Armar6RT start Armar6UnitOnly' &")
    if not(silent):
      print "------------------------------------"


if RUN_HIGHLEVEL:
    # start the high level components on the vision pc
    if not(silent):
      print BOLD + "Starting Highlevel and LowLevel Components" + ENDC

#    os.system("ssh -XC " + ARMAR6a_USER + "@" + ARMAR6a_VISION + " '" + SETUP_COMMAND + " && armarx scenario -p armar6_skills restart Armar6HighlevelApps      --parameters \\\"--ArmarX.Verbosity=Important --MemoryX.Verbosity=Important\\\" ' &")
#    os.system("ssh -XC " + ARMAR6a_USER + "@" + ARMAR6a_VISION + " '" + SETUP_COMMAND + " && armarx scenario -p Armar6RT     restart Armar6LowLevelComponents --parameters \\\"--ArmarX.Verbosity=Important --MemoryX.Verbosity=Important\\\" ' &")
    if not(silent):
      print "------------------------------------"


if RUN_VISION:
    # start the vision components on the vision pc
    if not(silent):
      print BOLD + "Starting Vision Components" + ENDC

#    os.system("ssh -XC " + ARMAR6a_USER + "@" + ARMAR6a_VISION + " '" + SETUP_COMMAND + " && armarx scenario -p armar6_skills restart Armar6Vision --parameters \\\"--VisionX.Verbosity=Important\\\" ' &")
#    os.system("ssh -XC " + ARMAR6a_USER + "@" + ARMAR6a_VISION + " '" + SETUP_COMMAND + " && armarx scenario -p armar6_skills status  Armar6Cameras | grep -q Stopped && armarx scenario -p armar6_skills start Armar6Cameras --parameters \\\"--VisionX.Verbosity=Important\\\" ' &")
    if not(silent):
      print "------------------------------------"
