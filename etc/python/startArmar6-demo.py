#!/usr/bin/python

import os
import sys
import socket
import argparse
import progressbar
from time import sleep

# Init parser
parser = argparse.ArgumentParser(description='Start the Armar6 Demo Environment on all relevant PCs')

# Only allow some filetypes as arguments
def file_choices(choices,fname):
    if not(os.path.isfile(fname)):
        parser.error("The file doesn't exist.")
    ext = os.path.splitext(fname)[1][1:]
    if ext not in choices:
       parser.error("The file doesn't end with one of {}".format(choices))
    return fname

# Define arguments
optionalNamed = parser.add_argument_group('properties')
optionalNamed.add_argument('-s', '--silent', action='store_true', dest="silent", default=False, help='Use this script in silent mode')
optionalNamed.add_argument('-f', '--fast', action='store_true', dest="fast", default=False, help='Fast mode. Do not wait at startup')
requiredNamed = parser.add_argument_group('configurations')
requiredNamed.add_argument('-c', action='store', default="", dest='control_config', type=lambda f:file_choices(("scx"),f), help='Specify the configuration file of the control GUI')
requiredNamed.add_argument('-v', action='store', default="", dest='vision_config', type=lambda f:file_choices(("scx"),f), help='Specify the configuration file of the vision GUI')



# Variable definitions
BOLD = "\033[1m"
FAIL = "\033[91m"
SUCC = "\033[92m"
ENDC = "\033[0m"

ARMAR6a_REALTIME = "armar6a-0"
ARMAR6a_PLANNING = "armar6a-1"
ARMAR6a_SPEECH = "armar6a-2"
ARMAR6a_VISION = "armar6a-3"
VISUALIZING_PC = "i61pc042"
CONTROL_PC = "i61pc043"

ARMAR6a_USER="armar6-user"

args = parser.parse_args(sys.argv[1:])

silent = args.silent
fast = args.fast
control_config = args.control_config
vision_config = args.vision_config

# Welcome message
if not(silent):
  print "------------------------------------"
  print BOLD + "Welcome to the Armar6 Startup script!" + ENDC
  print "This script will start all necessary components of ArmarX and connect this computer to all relevant computers, namely:"
  print ARMAR6a_REALTIME + "        <- realtime"
  print ARMAR6a_PLANNING + "        <- planning"
  print ARMAR6a_SPEECH + "        <- speech"
  print ARMAR6a_VISION + "        <- vision"
  print VISUALIZING_PC + "         <- Visualizing PC"
  print "------------------------------------"

# check for

# Check whether this is the correct computer
hostname = socket.gethostname()
if not(silent):
  print BOLD + "Check whether you use the correct pc" + ENDC

if(hostname != CONTROL_PC):
  print FAIL + "... ERROR: The hostname of this pc (" + hostname + ") is different than " + CONTROL_PC + ENDC
  sys.exit()

if(hostname == CONTROL_PC and not(silent)):
    print SUCC + "... The hostname ist correct" + ENDC

# Wait for security reasons
seconds = 5
if not(fast):
  if not(silent):
    print BOLD + "Wait for " + str(seconds) + " seconds due to security reasons:" + ENDC

    bar = progressbar.ProgressBar(maxval=10*seconds, \
    widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
    bar.start()

    for i in xrange(seconds*10):
      bar.update(i)
      sleep(0.1)
    bar.update(seconds*10)
    bar.finish()
    print "------------------------------------"
  else:
    sleep(seconds)


# Check connectivity
packages = 2
hosts = [ARMAR6a_REALTIME, ARMAR6a_PLANNING, ARMAR6a_SPEECH, ARMAR6a_VISION, VISUALIZING_PC]

if not(silent):
  print BOLD + "Check connections to all relevant pcs" + ENDC

for host in hosts:
  if not(silent):
    print "Check connection to " + host

  host_up  = True if os.system("ping -c " + str(packages) + " " + host + " > /dev/null") is 0 else False
  if host_up:
    if not(silent):
      print SUCC + "... Connection successful" + ENDC
  else:
    print FAIL + "... ERROR: Cant ping to $host" + ENDC
    sys.exit()

if not(silent):
  print "------------------------------------"



# Start all components on PCs
remote_args = " "
if(silent):
    remote_args += "-s "

dir = os.path.dirname(os.path.abspath(__file__))
os.system(dir + "/startArmar6-remote.py " + remote_args + " basic")

# Assure armarx is running
if not(silent):
  print BOLD + "Check for ArmarX Ice node" + ENDC

armarx_found  = True if os.system("! armarx status &>/dev/null") is 0 else False

if not(armarx_found):
  print FAIL + "... ERROR: Armarx not found" + ENDC
if armarx_found and not(silent):
  print SUCC + "... ArmarX found!" + ENDC


# Open GUIs with config on current pc
if not(silent):
  print BOLD + "Open ArmarXGui on current PC" + ENDC

os.system("armarx gui " + control_config + "> /dev/null")
os.system("ssh -XC " + ARMAR6a_USER + "@" + ARMAR6a_VISION + " 'armarx gui " + vision_config + " > /dev/null' &")

if not(silent):
  print "------------------------------------"
