#pragma once
#include <ArmarXCore/interface/core/BasicTypes.ice>

module armarx
{

    interface LookAtHandInterface
    {
        void enableLookAtHand();
        void disableLookAtHand();
        void setHandOffset(Ice::DoubleSeq offset);
        void setRobotNodeSetName(string nodeSetName);
        void setWorkspace(Ice::DoubleSeq workspaceMin, Ice::DoubleSeq workspaceMax);
    };
};
