#pragma once

#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/core/BasicVectorTypes.ice>

#include <RobotAPI/interface/core/PoseBase.ice>
#include <RobotAPI/interface/objectpose/object_pose_types.ice>
#include <RobotAPI/interface/units/GraspCandidateProviderInterface.ice>
#include <RobotAPI/interface/armem/client/MemoryListenerInterface.ice>
#include <armarx/manipulation/action_execution/ActionExecutorInterface.ice>

module armarx
{
module grasping
{

    struct CalculateGraspCandidatesInput
    {
        /// The object to grasp.
        objpose::data::ObjectPose objectPose;
        /// The side ("Left" or "Right").
        string side;
    };

    struct CalculateGraspCandidatesOutput
    {
        grasping::GraspCandidateDict candidates;
    };

    struct ExecutePickInput
    {
        string candidateID;
        Vector3Base liftVector = nullptr;  // Leave null for default.
    };
    struct ExecutePickOutput
    {
        bool executed = false;
    };

    struct ExecuteRetreatInput
    {
        string side;
    };
    struct ExecuteRetreatOutput
    {
        bool executed = false;
    };

    struct ExecutePreposeInput
    {
        string side;
    };
    struct ExecutePreposeOutput
    {
        bool executed = false;
    };

    struct ExecutePreposeReverseInput
    {
        string side;
    };
    struct ExecutePreposeReverseOutput
    {
        bool executed = false;
    };

    struct ExecutePushInput
    {
        string side;
    };
    struct ExecutePushOutput
    {
        bool executed = false;
    };

    struct ExecutePlaceInput
    {
        string side;
        Vector3Base placePositionGlobal = nullptr;  // Leave null for default.
    };
    struct ExecutePlaceOutput
    {
        bool executed = false;
    };

    struct LocalizeObjectInHandInput
    {
        /// Which object to look for.
        data::ObjectID objectID;
        /// In which hand the object is held.
        string side = "Right";

        /// Whether to actively move the head to look at the TCP.
        bool moveHead = false;

        bool interpolatePose = false;

        /// How long to wait before starting localization.
        int initialWaitMS = 0;
        /// How frequently to poll for new poses.
        float pollFrequency = 10;
        /// How long to wait for pose updates.
        int timeoutMS = 2000;
        /// For how many updates to wait until accepting.
        int minPoseUpdates = 2;
        /// Ignore pose updates which are far away from the TCP (negative value to disable).
        float maxDistanceFromTcp = 200;
    };
    struct LocalizeObjectInHandOutput
    {
        /// Whether the object was found.
        bool success = false;
        /// The object pose.
        objpose::data::ObjectPose objectPose;
        PoseBase tcpPoseRobot;
    };

    interface PickAndPlaceComponentInterface extends armarx::manipulation::action_execution::ActionExecutorInterface //extends armarx::armem::client::MemoryListenerInterface
    {
        CalculateGraspCandidatesOutput calculateGraspCandidates(CalculateGraspCandidatesInput input);

        ExecutePickOutput executePick(ExecutePickInput input);

        ExecuteRetreatOutput executeRetreat(ExecuteRetreatInput input);

        ExecutePreposeOutput executePrepose(ExecutePreposeInput input);

        ExecutePreposeReverseOutput executePreposeReverse(ExecutePreposeReverseInput input);

        ExecutePushOutput executePush(ExecutePushInput input);

        ExecutePlaceOutput executePlace(ExecutePlaceInput input);

        LocalizeObjectInHandOutput localizeObjectInHand(LocalizeObjectInHandInput input);
    };

};
};
