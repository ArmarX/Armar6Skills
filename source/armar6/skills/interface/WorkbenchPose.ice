#pragma once

#include <ArmarXCore/interface/observers/ObserverInterface.ice>
#include <ArmarXCore/interface/observers/RequestableService.ice>

#include <RobotAPI/interface/core/PoseBase.ice>

#include <VisionX/interface/core/PointCloudProcessorInterface.ice>


module armarx
{

    module data
    {
        /**
         * @brief Estimated workbench pose.
         *
         * All coordinates are given in robot frame.
         *
         */
        struct WorkbenchPose
        {
            /// Whether the work bench was successfully detected in this frame.
            bool valid = false;
            /// Global robot pose.
            PoseBase robotPose;


            /// Position of center.
            Vector3Base position;
            /**
             * @brief Orientation.
             *
             * x: long side, y: short side, z: normal (up)
             *
             * +---------^ y--------+
             * |         |          |
             * |         o--> x     |
             * |                    |
             * +--------------------+
             */
            QuaternionBase orientation;

            /// Rotation of normal around Z axis relative to x axis.
            float rotationAroundZ = 0;
            /// ... relative to global x axis.
            float rotationAroundZGlobal = 0;
        };
    }


    interface WorkbenchPoseTopic
    {
        void reportWorkbenchPose(data::WorkbenchPose Workbench);
    };


    interface WorkbenchPoseInterface
    {
        data::WorkbenchPose getLatestWorkbenchPose();
    };


    interface WorkbenchPoseEstimationInterface extends
            visionx::PointCloudProcessorInterface,
            WorkbenchPoseInterface,
            RequestableServiceListenerInterface
    {
    };


    interface WorkbenchPoseObserverInterface extends ObserverInterface, WorkbenchPoseTopic, WorkbenchPoseInterface
    {
    };

};
