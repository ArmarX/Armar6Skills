/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/interface/serialization/Shapes.ice>

#include <RobotAPI/interface/objectpose/object_pose_types.ice>

module armarx
{
    interface PlacementAreaProviderInterface
    {
        ["cpp:const"] idempotent
        void computeArea(objpose::data::ObjectPose objPose,
                         out simox::XYConstrainedOrientedBoxfSeq placementAreas);

        ["cpp:const"] idempotent
        void computeAreaPosition(objpose::data::ObjectPose objPose,
                                 Vector3Base targetPosition,
                                 out simox::XYConstrainedOrientedBoxfSeq placementAreas);

        ["cpp:const"] idempotent
        void computeAreaOrientation(objpose::data::ObjectPose objPose,
                                    QuaternionBase targetOrientation,
                                    out simox::XYConstrainedOrientedBoxfSeq placementAreas);

        ["cpp:const"] idempotent
        void computeAreaPose(objpose::data::ObjectPose objPose,
                             PoseBase targetPose,
                             out simox::XYConstrainedOrientedBoxfSeq placementAreas);

        ["cpp:const"] idempotent
        void computeAreaRegion(objpose::data::ObjectPose objPose,
                               simox::XYConstrainedOrientedBoxf region,
                               out simox::XYConstrainedOrientedBoxfSeq placementAreas);

        ["cpp:const"] idempotent
        void computeAreaRegionOrientation(objpose::data::ObjectPose objPose,
                                          simox::XYConstrainedOrientedBoxf region,
                                          QuaternionBase targetOrientation,
                                          out simox::XYConstrainedOrientedBoxfSeq placementAreas);
    };

    interface PlacementAreaTopicInterface
    {
        void reportNewPlacementAreas(out simox::XYConstrainedOrientedBoxfSeq placementAreas);
    }
}
