#pragma once

#include <ArmarXCore/interface/observers/ObserverInterface.ice>
#include <ArmarXCore/interface/observers/RequestableService.ice>

#include <RobotAPI/interface/core/PoseBase.ice>

#include <VisionX/interface/core/PointCloudProcessorInterface.ice>


module armarx
{

    module data
    {
        /**
         * @brief Estimated guard pose.
         *
         * All coordinates are given in robot frame.
         */
        struct GuardPose
        {
            /// Whether the guard was successfully detected in this frame.
            bool valid = false;

            /// Position of center.
            Vector3Base position;
            /// Orientation.
            /// x: short side, y: long side, z: normal
            QuaternionBase orientation;

            /// Rotation of normal around Z axis relative to x axis.
            float rotationAroundZ = 0;
            /// ... relative to global x axis.
            float rotationAroundZGlobal = 0;

            Vector3Base planeNormal;
            float planeOffset = 0;

            Vector3Base pTopRight;
            Vector3Base pTopLeft;
            Vector3Base pBottomRight;
            Vector3Base pBottomLeft;

            // Context

            /// Global robot pose.
            PoseBase robotPose;
        };
    }


    interface GuardPoseTopic
    {
        void reportGuardPose(data::GuardPose guard);
    };


    interface GuardPoseInterface
    {
        data::GuardPose getLatestGuardPose();
    };


    /// Allows PointCloudProcessors to implement the GuardPoseInterface.
    interface GuardPoseEstimationInterface extends visionx::PointCloudProcessorInterface,
            GuardPoseInterface,
            RequestableServiceListenerInterface
    {
    };


    interface GuardPoseObserverInterface extends ObserverInterface, GuardPoseTopic, GuardPoseInterface
    {
    };

};
