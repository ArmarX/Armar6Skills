/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    armar6_skills
* @author     Simon Ottenhaus
* @copyright  2020 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <RobotAPI/interface/core/PoseBase.ice>

module armarx
{
    module SearchAction
    {
        interface SearchActionReportTopic
        {
            // called when a search action starts
            void reportSearchStart(string actionName, long timestamp);

            // called when a search action finishes
            void reportSearchEnd(string actionName, long timestamp);

            // called periodically during the execution of the search action
            // phase describes the sub-phase of the action, e.g. 'lookLeft', 'lookCenter'
            // state can be 'driving', 'turningHead', 'waitingForHeadStable', 'waitingForResponse'
            void reportSearchState(string actionName, string phase, string state, PoseBase robotPose, PoseBase cameraPose, long timestamp);

            void reportImageCanBeCaputured(string actionName, string phase, long timestamp);
        };

        sequence<byte> ByteSeq;

        interface SearchActionResponse
        {
            // caleed when the action recognition has captured the image.
            // The parameters should have the values of the corresponding reportImageCanBeCaputured call and are passed back for sanity checking
            void notifyImageWasCaptured(string actionName, string phase, long timestamp);

            // request early termination of the search
            void notifyTerminateSearch(string reason);

            void reportSegmentedMask(int width, int height, ByteSeq mask, long timestamp);
        };
    };

};
