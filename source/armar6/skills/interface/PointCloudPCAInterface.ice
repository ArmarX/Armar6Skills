/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX
* @author     Simon Ottenhaus
* @copyright  2018 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <RobotAPI/interface/core/PoseBase.ice>
#include <VisionX/interface/core/PointCloudProcessorInterface.ice>

module armarx
{
    struct PCAResult3D
    {
        Vector3Base center;
        Vector3Base v1;
        Vector3Base v2;
        Vector3Base v3;
        int inputPointCount;
    };

    struct CapturedPoint
    {
        float x = 0;
        float y = 0;
        float z = 0;
    };
    sequence<CapturedPoint> CapturedPointSeq;

    interface PointCloudPCAInterface extends visionx::PointCloudProcessorInterface
    {
        PCAResult3D getResult();
        CapturedPointSeq getSegmentedPoints();
    };
};



