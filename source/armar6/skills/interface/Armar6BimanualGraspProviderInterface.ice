/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX
* @author     Simon Ottenhaus
* @copyright  2018 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <RobotAPI/interface/core/PoseBase.ice>
#include <VisionX/interface/core/PointCloudProcessorInterface.ice>
#include <RobotAPI/interface/units/GraspCandidateProviderInterface.ice>

module armarx
{

    /*module Armar6BimanualGraspProviderModule
    {
        struct Point
        {
            float x = 0;
            float y = 0;
            float z = 0;
        };
        sequence<Point> PointSeq;

        struct Extent
        {
            float min;
            float max;
            float p05;
            float p10;
            float p90;
            float p95;
        };

        enum GraspType {
            Top, Side
        };
        enum PreshapeType
        {
            Open, Preshaped
        };

        struct Result
        {
            Vector3Base center;
            Vector3Base v1;
            Vector3Base v2;
            Vector3Base v3;
            Extent extent1;
            Extent extent2;
            Extent extent3;
            int inputPointCount;
            PointSeq points;
            int labelNr;
            Vector3Base approach;
            GraspType graspDirection;
            PreshapeType preshape;
            string side;
            PoseBase graspPose;
        };

        sequence<Result> ResultSeq;

    };*/

    interface Armar6BimanualGraspProviderInterface extends visionx::PointCloudProcessorInterface, grasping::GraspCandidateProviderInterface
    {
        void setShowInvertedHands(bool value);
        void setShowLongAndShortSide(bool value);
        void setShowDiagonal(bool value);

        void setHandOffsetY(float value);
        void setHandDistanceOffsetX(float value);

        void setVisualization(bool showAllGrasps, string chosenGraspName);
    };
};



