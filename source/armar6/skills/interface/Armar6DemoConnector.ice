/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    armar6_skills
* @author     Simon Ottenhaus
* @copyright  2020 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <RobotAPI/interface/aron.ice>

module armarx
{
    interface Armar6DemoConnectorRequest
    {
        void startArmar6Skill(string skillName, armarx::aron::data::dto::Dict args);
        void stopArmar6Skill(string skillName);
    };

    interface Armar6DemoConnectorResponse
    {
        void notifySkillStarted(string skillName, string componentName);
        void notifySkillRunning(string skillName, string componentName, string phase, armarx::aron::data::dto::Dict args);
        void notifySkillCompleted(string skillName, string componentName);
    };

    struct Armar6DemoConnectorStatus
    {
        string status;
        string skillName;
        string componentName;
        string phase;
        long timestamp;
        armarx::aron::data::dto::Dict args;
    };

    interface Armar6DemoConnectorInterface extends Armar6DemoConnectorResponse
    {
        Armar6DemoConnectorStatus getStatus();
        void startArmar6Skill(string skillName, armarx::aron::data::dto::Dict args);
        void stopArmar6Skill(string skillName);
        void notifyKeepalive(string skillName);
    };


};
