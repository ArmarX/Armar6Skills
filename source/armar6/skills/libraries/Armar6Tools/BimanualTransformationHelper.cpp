/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Christoph Pohl (christoph dot pohl at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BimanualTransformationHelper.h"

using namespace armarx;

armarx::FramedPose
BimanualTransformationHelper::reflectHandPoseAboutYZAxis(const armarx::FramedPose& handPose,
        VirtualRobot::RobotPtr localRobotPtr)
{
    Eigen::Matrix4f hand_in_root = handPose.toRootEigen(localRobotPtr);
    return reflectHandPoseAboutYZAxis(hand_in_root, localRobotPtr->getName());
}

Armar6GraspTrajectoryPtr
BimanualTransformationHelper::transformGraspTrajectoryToOtherHand(Armar6GraspTrajectoryPtr trajectory)
{
    Eigen::Matrix3f flip_yz = Eigen::Matrix3f::Identity();
    flip_yz(0, 0) *= -1.0;
    Eigen::Matrix4f start_pose = trajectory->getStartPose();
    start_pose.block<3, 3>(0, 0) = flip_yz * start_pose.block<3, 3>(0, 0) * flip_yz;
    Armar6GraspTrajectoryPtr output_trajectory(
        new Armar6GraspTrajectory(start_pose, trajectory->getKeypoint(0)->handJointsTarget));
    for (size_t i = 1; i < trajectory->getKeypointCount(); i++)
    {
        Armar6GraspTrajectory::KeypointPtr& kp = trajectory->getKeypoint(i);
        Eigen::Matrix4f target_pose = kp->getTargetPose();
        target_pose.block<3, 3>(0, 0) = flip_yz * target_pose.block<3, 3>(0, 0) * flip_yz;
        output_trajectory->addKeypoint(target_pose, kp->handJointsTarget, kp->dt);
    }
    return output_trajectory;
}

armarx::FramedOrientation
BimanualTransformationHelper::transformOrientationToOtherHand(const armarx::FramedOrientation& orientation,
        VirtualRobot::RobotPtr localRobotPtr)
{
    Eigen::Matrix3f orientation_in_root = orientation.toRootEigen(localRobotPtr);
    return transformOrientationToOtherHand(orientation_in_root, localRobotPtr->getName());
}

FramedOrientation
BimanualTransformationHelper::transformOrientationToOtherHand(const Eigen::Matrix3f& orientationInRoot,
        const std::string& agentName)
{
    Eigen::Matrix3f flipped_ori = reflectOrientationAboutPlane(orientationInRoot, Eigen::Vector3f{1, 0, 0});
    return armarx::FramedOrientation(flipped_ori, "root", agentName);
}

FramedPose
BimanualTransformationHelper::reflectHandPoseAboutYZAxis(const Eigen::Matrix4f& poseInRoot, const std::string& agentName)
{
    Eigen::Matrix4f flipped_pose = reflectPoseAboutPlane(poseInRoot, Eigen::Vector4f{1, 0, 0, 0});
    return armarx::FramedPose(flipped_pose, "root", agentName);
}

Eigen::Matrix4f
BimanualTransformationHelper::reflectPoseAboutPlane(const Eigen::Matrix4f& pose, const Eigen::Vector4f& planeNormal)
{
    Eigen::Matrix4f householder_matrix = Eigen::Matrix4f::Identity() - 2 * planeNormal * planeNormal.transpose();
    return householder_matrix * pose * householder_matrix;
}

Eigen::Vector3f
BimanualTransformationHelper::reflectPointAboutPlane(const Eigen::Vector3f& point, const Eigen::Vector3f& planeNormal)
{
    Eigen::Matrix3f householder_matrix = Eigen::Matrix3f::Identity() - 2 * planeNormal * planeNormal.transpose();
    return householder_matrix * point;
}

Eigen::Matrix3f BimanualTransformationHelper::reflectOrientationAboutPlane(const Eigen::Matrix3f& orientation,
        const Eigen::Vector3f& planeNormal)
{
    Eigen::Matrix3f householder_matrix = Eigen::Matrix3f::Identity() - 2 * planeNormal * planeNormal.transpose();
    return householder_matrix * orientation * householder_matrix;
}

Eigen::Matrix4f
BimanualTransformationHelper::reflectPoseAboutPlane(const Eigen::Matrix4f& pose, const Eigen::Vector3f& planeNormal)
{
    Eigen::Vector4f normal{planeNormal.x(), planeNormal.y(), planeNormal.z(), 0};
    return reflectPoseAboutPlane(pose, normal);
}

Eigen::Matrix4f
BimanualTransformationHelper::switchHandForPose(const Eigen::Matrix4f& pose)
{
    Eigen::Matrix4f transformation_to_other_hand = Eigen::Matrix4f::Identity();
    transformation_to_other_hand.block<2, 2>(0, 0) *= -1;
    return pose * transformation_to_other_hand;
}

armarx::FramedPose
BimanualTransformationHelper::switchHandForPose(const armarx::FramedPose& pose)
{
    Eigen::Matrix4f transformed_pose = switchHandForPose(pose.toEigen());
    armarx::FramedPose other_hand(transformed_pose, pose.getFrame(), pose.agent);
    return other_hand;
}


