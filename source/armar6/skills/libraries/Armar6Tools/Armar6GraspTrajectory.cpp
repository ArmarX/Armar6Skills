/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6GraspTrajectory.h"

#include <RobotAPI/libraries/SimpleJsonLogger/SimpleJsonLogger.h>
#include <ArmarXGui/libraries/StructuralJson/JPathNavigator.h>
#include <ArmarXGui/libraries/StructuralJson/StructuralJsonParser.h>

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlWriter.h>

namespace armarx
{

    void Armar6GraspTrajectory::writeToFile(const std::string& filename)
    {
        RapidXmlWriter writer;
        RapidXmlWriterNode root = writer.createRootNode("Armar6GraspTrajectory");
        for (const KeypointPtr& keypoint : keypoints)
        {
            SimpleJsonLoggerEntry e;
            e.Add("dt", keypoint->dt);
            e.AddAsArr("Pose", keypoint->tcpTarget);
            e.AddAsArr("HandValues", keypoint->handJointsTarget);
            root.append_string_node("Keypoint", e.obj->toJsonString(0, "", true));
        }
        writer.saveToFile(filename, true);
    }

    Armar6GraspTrajectoryPtr Armar6GraspTrajectory::ReadFromFile(const grasping::GraspCandidatePtr& cnd)
    {
        std::string packageName = "armar6_skills";
        armarx::CMakePackageFinder finder(packageName);
        std::string dataDir = finder.getDataDir() + "/" + packageName;
        return Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/" + cnd->executionHints->graspTrajectoryName + ".xml");
    }

    Armar6GraspTrajectoryPtr Armar6GraspTrajectory::ReadFromReader(const RapidXmlReaderPtr& reader)
    {
        RapidXmlReaderNode root = reader->getRoot("Armar6GraspTrajectory");
        Armar6GraspTrajectoryPtr traj;
        for (const RapidXmlReaderNode& kpNode : root.nodes("Keypoint"))
        {
            StructuralJsonParser p(kpNode.value());
            p.parse();
            JPathNavigator nav(p.parsedJson);
            float dt = nav.selectSingleNode("dt").asFloat();

            Eigen::Matrix4f pose;
            std::vector<JPathNavigator> rows = nav.select("Pose/*");
            for (int i = 0; i < 4; i++)
            {
                std::vector<JPathNavigator> cells = rows.at(i).select("*");
                for (int j = 0; j < 4; j++)
                {
                    pose(i, j) = cells.at(j).asFloat();
                }
            }

            Eigen::Vector3f handValues;
            std::vector<JPathNavigator> cells = nav.select("HandValues/*");
            for (int j = 0; j < 3; j++)
            {
                handValues(j) = cells.at(j).asFloat();
            }

            if (!traj)
            {
                traj = Armar6GraspTrajectoryPtr(new Armar6GraspTrajectory(pose, handValues));
            }
            else
            {
                traj->addKeypoint(pose, handValues, dt);
            }
        }
        return traj;
    }

    Armar6GraspTrajectoryPtr Armar6GraspTrajectory::ReadFromFile(const std::string& filename)
    {
        return ReadFromReader(RapidXmlReader::FromFile(filename));
    }

    Armar6GraspTrajectoryPtr Armar6GraspTrajectory::ReadFromString(const std::string& xml)
    {
        return ReadFromReader(RapidXmlReader::FromXmlString(xml));
    }

}
