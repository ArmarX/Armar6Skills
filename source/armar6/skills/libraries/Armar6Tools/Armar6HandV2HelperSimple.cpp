/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister (fabian dot reister at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6HandV2HelperSimple.h"

#include "ArmarXCore/core/logging/Logging.h"
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/util/CPPUtility/trace.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::simple{

Armar6HandV2Joints::Armar6HandV2Joints(const std::string& side)
    : side(side)
{

    handRootNameMap =
    {
        {"Right", "Hand R Root"},
        {"Left", "Hand L Root"},
    };

    jointsThumbReal =
    {
        {"Right", "Thumb" },
        {"Left", "Thumb"},
    };

    jointsFingersReal =
    {
        {"Right", "Fingers"},
        {"Left", "Fingers"},
    };

    // jointsThumbSim =
    // {
    //     {"Right", {"Thumb R 1 Joint", "Thumb R 2 Joint"}},
    //     {"Left", {"Thumb L 1 Joint", "Thumb L 2 Joint"}},
    // };
    // jointsFingersSim =
    // {
    //     {
    //         "Right", {"Index R 1 Joint", "Index R 2 Joint", "Index R 3 Joint",
    //             "Middle R 1 Joint", "Middle R 2 Joint", "Middle R 3 Joint",
    //             "Ring R 1 Joint", "Ring R 2 Joint", "Ring R 3 Joint",
    //             "Pinky R 1 Joint", "Pinky R 2 Joint", "Pinky R 3 Joint"
    //         }
    //     },
    //     {
    //         "Left", {"Index L 1 Joint", "Index L 2 Joint", "Index L 3 Joint",
    //             "Middle L 1 Joint", "Middle L 2 Joint", "Middle L 3 Joint",
    //             "Ring L 1 Joint", "Ring L 2 Joint", "Ring L 3 Joint",
    //             "Pinky L 1 Joint", "Pinky L 2 Joint", "Pinky L 3 Joint"
    //         }
    //     },
    // };
}

std::map<std::string, float> Armar6HandV2Joints::getSimulationJointValues(float fingers, float thumb) const
{
    

    // float factor = M_PI / 2;

    std::map<std::string, float> jointValues;
    // for (const std::string& name : jointsFingersSim.at(side))
    // {
    //     jointValues[name] = fingers * factor;
    // }

    // for (const std::string& name : jointsThumbSim.at(side))
    // {
    //     jointValues[name] = thumb * factor;
    // }

    return jointValues;
}

std::map<std::string, float> Armar6HandV2Joints::getSimulationJointValues(const Eigen::Vector3f& jointValues) const
{
    return getSimulationJointValues(jointValues(0), jointValues(1));
}



std::string Armar6HandV2Joints::getThumbJointNames() const
{
    return jointsThumbReal.at(side);
}

std::string Armar6HandV2Joints::getFingersJointNames() const
{
    return jointsFingersReal.at(side);
}

void Armar6HandV2Joints::drawHand(DebugDrawerHelper& debugDrawerHelper, const std::string& name, const RobotNameHelper::RobotArm& arm, const Eigen::Matrix4f& pose, const Eigen::Vector3f& jointValues, DrawColor color)
{
    debugDrawerHelper.drawRobot(name, arm.getArm().getHandModelPath(), arm.getArm().getHandModelPackage(), pose * arm.getTcp2HandRootTransform(), color);
    debugDrawerHelper.setRobotConfig(name, getSimulationJointValues(jointValues));
}

void Armar6HandV2Joints::DrawHand(DebugDrawerHelper& debugDrawerHelper, const std::string& name, const RobotNameHelper::RobotArm& arm, const Eigen::Matrix4f& pose, const Eigen::Vector3f& jointValues, DrawColor color)
{
    Armar6HandV2Joints handJoints(arm.getSide());
    handJoints.drawHand(debugDrawerHelper, name, arm, pose, jointValues, color);
}


Armar6HandV2Helper::Armar6HandV2Helper(const HandUnitInterfacePrx& hUnit, const std::string& side)
    : Armar6HandV2Joints(side), handUnit{hUnit}
{
    ARMARX_CHECK_NOT_NULL(handUnit);
}

void Armar6HandV2Helper::shapeHand(float fingers, float thumb)
{
    ARMARX_TRACE;

    std::map<std::string, float> jointValues;

    jointValues[getFingersJointNames()] = fingers;
    jointValues[getThumbJointNames()] = thumb;

    ARMARX_CHECK_NOT_NULL(handUnit);
    handUnit->setJointAngles(jointValues);

    // kinUnitHelper.setJointAngles(jointValues);
}

void Armar6HandV2Helper::shapeHand(Eigen::Vector3f jointValues)
{
    shapeHand(jointValues(0), jointValues(1));
}

float Armar6HandV2Helper::getFingersActualValue() const
{
    const auto currentJointValues = handUnit->getCurrentJointValues();

    ARMARX_CHECK_EXPRESSION(currentJointValues.count(getFingersJointNames()))
            << VAROUT(currentJointValues) << '\n' << VAROUT(getFingersJointNames());

    return currentJointValues.at(getFingersJointNames());
}

float Armar6HandV2Helper::getThumbActualValue() const
{
    ARMARX_TRACE;

    const auto currentJointValues = handUnit->getCurrentJointValues();
    ARMARX_CHECK_EXPRESSION(currentJointValues.count(getThumbJointNames()))
            << VAROUT(currentJointValues) << '\n' << VAROUT(getThumbJointNames());

    return currentJointValues.at(getThumbJointNames());
}

float Armar6HandV2Helper::getFingersTargetValue() const
{
    return fingersTarget;
}

float Armar6HandV2Helper::getThumbTargetValue() const
{
    return thumbTarget;
}

bool Armar6HandV2Helper::targetReached(const float eps) const
{
    const float diffFingers = fingersTarget - getFingersActualValue();
    const float diffThumb = thumbTarget - getFingersActualValue();

    return (std::fabs(diffFingers) < eps) and (std::fabs(diffThumb) < eps);
}

}
