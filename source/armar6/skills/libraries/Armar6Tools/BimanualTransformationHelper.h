/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Christoph Pohl (christoph dot pohl at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef armar6_skills_BIMANUALTRANSFORMATIONHELPER_H
#define armar6_skills_BIMANUALTRANSFORMATIONHELPER_H


#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>

#include <VirtualRobot/Robot.h>

#include "Armar6GraspTrajectory.h"

namespace armarx
{
    class BimanualTransformationHelper
    {
    public:
        /**
         * Reflect the tcp pose of a hand along the yz-axis in the Robot Root Frame. This mirros e.g. the right hand
         * into the left hand or vice versa. The handedness of the input pose is preserved.
         *
         * @param handPose - Framed Pose of the Hand TCP (or any other pose that is to be mirrored along thx yz-axis)
         * @param localRobotPtr
         * @return
         */
        static armarx::FramedPose
        reflectHandPoseAboutYZAxis(const armarx::FramedPose& handPose, VirtualRobot::RobotPtr localRobotPtr);
        static FramedPose
        reflectHandPoseAboutYZAxis(const Eigen::Matrix4f& poseInRoot, const std::string& agentName);

        /**
         * Transforms a Armar6GraspTrajectory for one hand into that of the other hand. Internally, only the orientation
         * of the keypoints are mirrored, while the position and the joint values of the hand are kept. The handedness
         * of the keypoints is preserved.
         *
         * @param trajectory
         * @return
         */
        static Armar6GraspTrajectoryPtr transformGraspTrajectoryToOtherHand(Armar6GraspTrajectoryPtr trajectory);

        /**
         * Reflects an orientation along the yz-axis in the Robot Root Frame. This can be used i.e. for transforming
         * a left hand pose into a right hand pose (and vice versa) at the same position. The handedness of the input
         * orientation is preserved.
         *
         * @param orientation
         * @param localRobotPtr
         * @return reflected orientation (with implicit right-handedness, as a FramedOrientation is automatically
         *          transformed)
         */
        static armarx::FramedOrientation
        transformOrientationToOtherHand(const armarx::FramedOrientation& orientation, VirtualRobot::RobotPtr localRobotPtr);
        static FramedOrientation
        transformOrientationToOtherHand(const Eigen::Matrix3f& orientationInRoot,
                                        const std::string& agentName);

        /**
         * Reflect a pose about any plane. The reflection happens in the frame of the plane, i.e. the frame where the
         * plane goes through the origin. The handedness of the input pose is preserved.
         *
         * @param pose - 6D pose in the plane's frame
         * @param planeNormal - normal vector of the plane
         * @return reflected pose (of the same handedness as the input pose)
         */
        static Eigen::Matrix4f reflectPoseAboutPlane(const Eigen::Matrix4f& pose, const Eigen::Vector4f& planeNormal);
        static Eigen::Matrix4f reflectPoseAboutPlane(const Eigen::Matrix4f& pose, const Eigen::Vector3f& planeNormal);

        /**
         *  Reflect a point about any plane that goes through the origin. I.e. the reflection happens in the frame of
         *  the plane.
         *
         * @param point
         * @param planeNormal
         * @return
         */
        static Eigen::Vector3f reflectPointAboutPlane(const Eigen::Vector3f& point, const Eigen::Vector3f& planeNormal);

        /**
         * Reflect an orientation about any plane that goes through the origin. The handedness of the input orientation
         * is preserved.
         *
         * @param orientation
         * @param planeNormal
         * @return
         */
        static Eigen::Matrix3f reflectOrientationAboutPlane(const Eigen::Matrix3f& orientation, const Eigen::Vector3f& planeNormal);

        /**
         * Switch the hand for the current pose. This means that the other hand is oriented exactly (i.e. same grasping
         * orientation) as the current hand and is at the same position.
         *
         * @param pose
         * @return
         */
        static armarx::FramedPose switchHandForPose(const armarx::FramedPose& pose);
        static Eigen::Matrix4f switchHandForPose(const Eigen::Matrix4f& pose);
    };
}

#endif //armar6_skills_BIMANUALTRANSFORMATIONHELPER_H
