/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>

#include <Eigen/Core>

#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>
#include <RobotAPI/interface/units/HandUnitInterface.h>

/**
 * @brief This is a copy of Armar6HandV2Helper. In contrast to the original implementation, the interface is simpler as only one degree of freedom exists for the fingers and the thumb. 
 * 
 * Soon, this will replace the original implementation.
 * 
 */
namespace armarx::simple
{
    using Armar6HandV2HelperPtr = std::shared_ptr<class Armar6HandV2Helper>;


    class Armar6HandV2Joints
    {
    public:
        Armar6HandV2Joints(const std::string& side);

        std::map<std::string, float> getSimulationJointValues(float fingers, float thumb) const;
        std::map<std::string, float> getSimulationJointValues(const Eigen::Vector3f& jointValues) const;

        std::string getThumbJointNames() const;
        std::string getFingersJointNames() const;

        void drawHand(DebugDrawerHelper& debugDrawerHelper, const std::string& name, const RobotNameHelper::RobotArm& arm, const Eigen::Matrix4f& pose, const Eigen::Vector3f& jointValues, DrawColor color);
        static void DrawHand(DebugDrawerHelper& debugDrawerHelper, const std::string& name, const RobotNameHelper::RobotArm& arm, const Eigen::Matrix4f& pose, const Eigen::Vector3f& jointValues, DrawColor color);

    protected:
        const std::string side;

        std::map<std::string, std::string> handRootNameMap;
        std::map<std::string, std::string> jointsThumbReal;
        std::map<std::string, std::string> jointsFingersReal;

        // std::map<std::string, std::vector<std::string>> jointsThumbSim;
        // std::map<std::string, std::vector<std::string>> jointsFingersSim;

    };

    class Armar6HandV2Helper : public Armar6HandV2Joints
    {
    public:
        Armar6HandV2Helper(const HandUnitInterfacePrx& hUnit, const std::string& side);

        void shapeHand(float fingers, float thumb);
        void shapeHand(Eigen::Vector3f jointValues);

        float getFingersActualValue() const;
        float getThumbActualValue() const;

        float getFingersTargetValue() const;
        float getThumbTargetValue() const;

        Eigen::Vector3f getHandForwardVector() const
        {
            return Eigen::Vector3f::UnitZ();
        }

        static Eigen::Vector3f GetHandForwardVector()
        {
            return Eigen::Vector3f::UnitZ();
        }


        void closeHand()
        {
            shapeHand(1.0f, 1.0f);
        }

        void openHand()
        {
            shapeHand(0.0f, 0.0f);
        }

        /**
         * @brief Checks whether the desired hand shape has been reached.
         *
         * @param eps threshold
         * @return true if both hand and finger value are within the desired epsilon region.
         * @return false otherwise
         */
        bool targetReached(float eps = 0.01F) const;

    private:

        HandUnitInterfacePrx handUnit;

        float fingersTarget = 0;
        float thumbTarget = 0;

    };
}
