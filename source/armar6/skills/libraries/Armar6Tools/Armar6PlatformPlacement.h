/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "Armar6GraspTrajectory.h"

#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/diffik/SimpleDiffIK.h>
#include <RobotAPI/libraries/GraspingUtility/GraspCandidateHelper.h>
#include <RobotAPI/interface/units/GraspCandidateProviderInterface.h>

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <VirtualRobot/math/Grid3D.h>
#include <VirtualRobot/math/Helpers.h>

#include <Eigen/Dense>

#include <memory>

namespace armarx
{
    typedef std::shared_ptr<class Armar6PlatformPlacement> Armar6PlatformPlacementPtr;

    class Armar6PlatformPlacement
    {
    private:
        VirtualRobot::RobotNodeSetPtr rns;
        VirtualRobot::RobotNodePtr tcp;
        SimpleDiffIK::Parameters params;

    public:
        struct PlacementReachability
        {
            Eigen::Matrix4f relativePose;
            SimpleDiffIK::Reachability reachability;
        };

        Armar6PlatformPlacement(VirtualRobot::RobotNodeSetPtr rns, VirtualRobot::RobotNodePtr tcp = VirtualRobot::RobotNodePtr(), SimpleDiffIK::Parameters params = SimpleDiffIK::Parameters())
            : rns(rns), tcp(tcp), params(params)
        { }

        std::vector<PlacementReachability> calculateReachability(Armar6GraspTrajectoryPtr graspingTrajectory, const std::vector<Eigen::Matrix4f>& relativePoses)
        {
            std::vector<PlacementReachability> res;
            for (const Eigen::Matrix4f& relPose : relativePoses)
            {
                Armar6GraspTrajectoryPtr traj = graspingTrajectory->getTransformed(relPose.inverse());
                PlacementReachability pr;
                pr.relativePose = relPose;
                pr.reachability = traj->calculateReachability(rns, tcp, params);
                res.emplace_back(pr);
            }
            return res;
        }
        std::vector<PlacementReachability> calculateReachability(Armar6GraspTrajectoryPtr graspingTrajectory, const math::Grid3DPtr& grid)
        {
            std::vector<Eigen::Matrix4f> relativePoses = math::Helpers::CreatePoses(grid->AllGridPoints(), Eigen::Matrix3f::Identity());
            return calculateReachability(graspingTrajectory, relativePoses);
        }

        Eigen::Vector3f defrost(const Vector3BasePtr& base)
        {
            return Vector3Ptr::dynamicCast(base)->toEigen();
        }
        Eigen::Matrix4f defrost(const PoseBasePtr& base)
        {
            return PosePtr::dynamicCast(base)->toEigen();
        }

        std::vector<PlacementReachability> calculateReachability(const grasping::GraspCandidatePtr& graspCandidate, VirtualRobot::RobotPtr robot, const math::Grid3DPtr& grid)
        {
            std::string packageName = "armar6_skills";
            armarx::CMakePackageFinder finder(packageName);
            std::string dataDir = finder.getDataDir() + "/" + packageName;
            Armar6GraspTrajectoryPtr graspTrajectory = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/" + graspCandidate->executionHints->graspTrajectoryName + ".xml");
            Eigen::Matrix4f graspPose = GraspCandidateHelper(graspCandidate, robot).getGraspPoseInRobotRoot();
            graspTrajectory = graspTrajectory->getTransformedToGraspPose(graspPose);
            return calculateReachability(graspTrajectory, grid);
        }

        bool findBestPlacement(const std::vector<Armar6PlatformPlacement::PlacementReachability>& placements, size_t& maxIndex)
        {
            float maxMargin = -1;
            (void) maxMargin;

            bool found = false;
            float minPlatformMove = std::numeric_limits<float>::max();

            const float jointLimitMarginTh = M_PIf32 / 9.F;

            for (size_t i = 0; i < placements.size(); i++)
            {
                Armar6PlatformPlacement::PlacementReachability pr = placements.at(i);
                float diff = pr.relativePose.block<3, 1>(0, 3).norm();
                if (pr.reachability.reachable && diff < minPlatformMove && pr.reachability.minimumJointLimitMargin > jointLimitMarginTh)
                {
                    minPlatformMove = diff;
                    maxIndex = i;
                    found = true;
                }
                //                if (pr.reachability.reachable && pr.reachability.minimumJointLimitMargin > maxMargin)
                //                {
                //                    maxMargin = pr.reachability.minimumJointLimitMargin;
                //                    maxIndex = i;
                //                    found = true;
                //                }
            }

            ARMARX_IMPORTANT << VAROUT(placements.at(maxIndex).relativePose);
            return found;
        }


        bool findBestPlacement(const std::vector<Armar6PlatformPlacement::PlacementReachability>& placements, size_t& maxIndex, const Eigen::Vector3f& graspPosition)
        {
            float maxMargin = -1;
            (void) maxMargin;

            bool found = false;
            float minPlatformMove = std::numeric_limits<float>::max();

            const float jointLimitMarginTh = M_PIf32 / 9.F;

            for (size_t i = 0; i < placements.size(); i++)
            {
                Armar6PlatformPlacement::PlacementReachability pr = placements.at(i);
                float diff = pr.relativePose.block<3, 1>(0, 3).norm();

                float graspPoseDiff = (pr.relativePose.block<3, 1>(0, 3) - graspPosition).x();

                if (graspPoseDiff < 0)
                {
                    continue;
                }


                if (pr.reachability.reachable && diff < minPlatformMove && pr.reachability.minimumJointLimitMargin > jointLimitMarginTh)
                {
                    minPlatformMove = diff;
                    maxIndex = i;
                    found = true;
                }
                //                if (pr.reachability.reachable && pr.reachability.minimumJointLimitMargin > maxMargin)
                //                {
                //                    maxMargin = pr.reachability.minimumJointLimitMargin;
                //                    maxIndex = i;
                //                    found = true;
                //                }
            }

            ARMARX_IMPORTANT << VAROUT(placements.at(maxIndex).relativePose);
            return found;
        }

    };
}
