/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    armar6_skills::qt_plugins::Armar6DemoWidgetController
 * \author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * \date       2019
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "WidgetController.h"

#include <string>

namespace armar6::skills::qt_plugins::armar6_demo
{
    WidgetController::WidgetController()
    {
        widget.setupUi(getWidget());
        connect(
            widget.pushButtonRequestMaskRcnn, SIGNAL(clicked()), this, SLOT(onRequestMaskRncn()));
        connect(
            widget.pushButtonClearDebugDrawer, SIGNAL(clicked()), this, SLOT(onClearDebugDrawer()));
    }


    WidgetController::~WidgetController()
    {
    }


    void
    WidgetController::loadSettings(QSettings* settings)
    {
    }

    void
    WidgetController::saveSettings(QSettings* settings)
    {
    }


    void
    WidgetController::onInitComponent()
    {
        offeringTopic("ServiceRequests");
        offeringTopic("DebugDrawerUpdates");
    }


    void
    WidgetController::onConnectComponent()
    {
        getTopic(serviceRequestsPrx, "ServiceRequests");
        getTopic(debugDrawerPrx, "DebugDrawerUpdates");
    }


    void
    WidgetController::onRequestMaskRncn()
    {
        ARMARX_IMPORTANT << "onRequestMaskRncn";
        serviceRequestsPrx->requestService("MaskRCNNImageProcessor",
                                           widget.spinBoxRequestTimeout->value() * 1000);
    }
    void
    WidgetController::onClearDebugDrawer()
    {
        ARMARX_IMPORTANT << "onClearDebugDrawer";
        debugDrawerPrx->clearAll();
    }
} // namespace armar6::skills::qt_plugins::armar6_demo
