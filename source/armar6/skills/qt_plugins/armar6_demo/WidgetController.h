/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::qt_plugins::Armar6DemoWidgetController
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/interface/observers/RequestableService.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <armar6/skills/qt_plugins/armar6_demo/ui_Widget.h>

namespace armar6::skills::qt_plugins::armar6_demo
{
    /**
    \page armar6_skills-GuiPlugins-Armar6Demo Armar6Demo
    \brief The Armar6Demo allows visualizing ...

    \image html Armar6Demo.png
    The user can

    API Documentation \ref Armar6DemoWidgetController

    \see Armar6DemoGuiPlugin
    */

    /**
     * \class Armar6DemoWidgetController
     * \brief Armar6DemoWidgetController brief one line description
     *
     * Detailed description
     */
    class WidgetController :
        public armarx::ArmarXComponentWidgetControllerTemplate<WidgetController>
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit WidgetController();

        /**
         * Controller destructor
         */
        virtual ~WidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString
        GetWidgetName()
        {
            return "Armar6Demo";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;

    public slots:
        void onRequestMaskRncn();
        void onClearDebugDrawer();

    signals:
        /* QT signal declarations */

    private:
        /**
         * Widget Form
         */
        Ui::Armar6DemoWidget widget;

        armarx::RequestableServiceListenerInterfacePrx serviceRequestsPrx;
        armarx::DebugDrawerInterfacePrx debugDrawerPrx;
    };
} // namespace armar6::skills::qt_plugins::armar6_demo
