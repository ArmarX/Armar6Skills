/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    armar6_skills::qt_plugins::DemoGUIPluginWidgetController
 * \author     KHitzler ( kevin dot hitzler at kit dot edu )
 * \date       2019
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "WidgetController.h"

#include <string>


namespace armar6::skills::qt_plugins::demo
{

    WidgetController::WidgetController()
    {
        widget.setupUi(getWidget());
    }


    WidgetController::~WidgetController()
    {
    }


    void
    WidgetController::loadSettings(QSettings* settings)
    {
    }

    void
    WidgetController::saveSettings(QSettings* settings)
    {
    }


    void
    WidgetController::onInitComponent()
    {
    }


    void
    WidgetController::onConnectComponent()
    {
        // GUI elements
        connect(
            widget.btnShutdownArmar6, SIGNAL(clicked()), this, SLOT(onBtnShutdownArmar6Clicked()));
    }

    void
    WidgetController::onBtnShutdownArmar6Clicked()
    {
        // make sure the user wants to shutdown the demo
        QMessageBox messageBox;
        QMessageBox::StandardButton qShutdownArmar6 = messageBox.question(
            0,
            "Shutdown Armar-6",
            "Please make sure that the robot is in a safe state.\n\nAre you sure "
            "that you want to shutdown Armar-6?");
        messageBox.setFixedSize(500, 200);

        if (qShutdownArmar6 == QMessageBox::Yes)
        {
            // execute shutdown-armar6.sh script on rt
            ARMARX_IMPORTANT << "Armar-6 shutdown triggered. Bye.";
        }
    }
} // namespace armar6::skills::qt_plugins::demo
