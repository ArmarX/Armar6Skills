/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::qt_plugins::DemoGUIPluginWidgetController
 * @author     KHitzler ( kevin dot hitzler at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <QMessageBox>

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>

#include <armar6/skills/qt_plugins/demo/ui_Widget.h>


namespace armar6::skills::qt_plugins::demo
{
    /**
    \page ArmarXGui-GuiPlugins-DemoGUIPlugin DemoGUIPlugin
    \brief The DemoGUIPlugin allows visualizing ...

    \image html DemoGUIPlugin.png
    The user can

    API Documentation \ref DemoGUIPluginWidgetController

    \see DemoGUIPluginGuiPlugin
    */

    /**
     * \class DemoGUIPluginWidgetController
     * \brief DemoGUIPluginWidgetController brief one line description
     *
     * Detailed description
     */
    class WidgetController :
        public armarx::ArmarXComponentWidgetControllerTemplate<WidgetController>
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit WidgetController();

        /**
         * Controller destructor
         */
        virtual ~WidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString
        GetWidgetName()
        {
            return "DemoGUIPlugin";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;

    public slots:
        /* QT slot declarations */
        void onBtnShutdownArmar6Clicked();

    signals:
        /* QT signal declarations */

    private:
        /**
         * Widget Form
         */
        Ui::DemoGUIPluginWidget widget;
    };
} // namespace armar6::skills::qt_plugins::demo
