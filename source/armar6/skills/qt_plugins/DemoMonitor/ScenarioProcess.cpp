
#include "ScenarioProcess.h"

using namespace armarx;

ScenarioProcess::ScenarioProcess(const QString& name, const QString& pkg, const QString& host, const QString& user,
                                 QPushButton* b_start, QPushButton* b_stop, QPushButton* b_restart, QLabel* label, X11Config x11,
                                 QObject* parent) : QObject(parent),
    scenarioName(name), packageName(pkg), hostName(host), userName(user), x11(x11), startButton(b_start), stopButton(b_stop),
    restartButton(b_restart), statusLabel(label), status("Empty")
{
}

ScenarioProcess::~ScenarioProcess()
{
    this->stopListening();
    this->refreshCountdown.clear();
    this->scenarioController.clear();
    this->statusController.clear();
}

/*
 * Initializes the SshControllers and connects to the remote pcs
 * Must be called before starting any Scenario
 */
void ScenarioProcess::connectSsh(QString const& host, QString const& user)
{
    //connect Buttons
    connect(startButton, SIGNAL(clicked()), this, SLOT(startScenario()));
    connect(stopButton, SIGNAL(clicked()), this, SLOT(stopScenario()));
    connect(restartButton, SIGNAL(clicked()), this, SLOT(restartScenario()));
    if (host != "")
    {
        this->hostName = host;
    }
    this->userName = user;
    //controller for commands
    this->scenarioController = QSharedPointer<SshController>(new SshController(this->hostName, this->userName), &QObject::deleteLater);
    if (!this->scenarioController->isConnected())
    {
        sshError();
    }
    connect(this->scenarioController.data(), SIGNAL(sshError()), this, SLOT(sshError()));
    connect(this->scenarioController.data(), SIGNAL(applicationError()), this, SLOT(scenarioError()));
    this->scenarioController->moveToThread(qApp->thread());
    //controller for status updates
    this->statusController = QSharedPointer<SshController>(new SshController(this->hostName, this->userName), &QObject::deleteLater);
    if (!this->statusController->isConnected())
    {
        sshError();
    }
    connect(this->statusController.data(), SIGNAL(sshError()), this, SLOT(sshError()));
    connect(this->statusController.data(), SIGNAL(applicationError()), this, SLOT(scenarioError()));
    connect(this->statusController.data(), SIGNAL(readLine(QString)), this, SLOT(processOutput(QString)));
    this->statusController->moveToThread(qApp->thread());

    //a countdown to check if Gui updates delay too long
    refreshCountdown = QSharedPointer<QTimer>(new QTimer(), &QObject::deleteLater);
    refreshCountdown->setInterval(REFRESH_COUNTDOWN_INTERVAL);
    connect(refreshCountdown.data(), SIGNAL(timeout()), this, SLOT(timeoutError()));
    refreshCountdown->moveToThread(qApp->thread());

    //load the needed environment variables
    QString command = "source '$HOME/repos/.armarxrc'";
    QMetaObject::invokeMethod(scenarioController.data(), "startCommand", Qt::QueuedConnection, Q_ARG(QString, command));

    this->startListening();
}

void ScenarioProcess::startListening()
{
    QString program = "$HOME/repos/ArmarXCore/build/bin/armarx scenario periodic_status " + this->scenarioName + " -p " + this->packageName;
    QMetaObject::invokeMethod(statusController.data(), "startCommand", Q_ARG(QString, program));
    QMetaObject::invokeMethod(this->refreshCountdown.data(), "start", Qt::QueuedConnection);
}

void ScenarioProcess::stopListening()
{
    QMetaObject::invokeMethod(statusController.data(), "stopCommand");
    QString command = "pkill -9 ScenarioCliRun";
    QMetaObject::invokeMethod(statusController.data(), "startCommand", Q_ARG(QString, command));
}

void ScenarioProcess::startScenario()
{
    hasScenarioError = false;
    if (hasSSHError)
    {
        ARMARX_IMPORTANT << "Cannot start scenario because of an SSHError";
        return;
    }
    ARMARX_IMPORTANT << "starting scenario " << scenarioName;
    QString program = "$HOME/repos/ArmarXCore/build/bin/armarx scenario start " + this->scenarioName + " -p " + this->packageName;
    if (x11 == X11Config_disabled)
    {
        QMetaObject::invokeMethod(scenarioController.data(), "startCommand", Q_ARG(QString, program));
    }
    else
    {
        //need to source directly, as this will establish a different ssh connection
        program = "source '$HOME/repos/.armarxrc'; " + program;
        QMetaObject::invokeMethod(scenarioController.data(), "startX11Command", Q_ARG(QString, program));
    }
}

void ScenarioProcess::stopScenario()
{
    if (hasSSHError)
    {
        ARMARX_IMPORTANT << "Cannot stop scenario because of an SSHError";
        return;
    }
    ARMARX_IMPORTANT << "stopping scenario " << scenarioName;
    QString program = "$HOME/repos/ArmarXCore/build/bin/armarx scenario kill " + scenarioName + " -p " + this->packageName;
    //no need for x11 in kill
    QMetaObject::invokeMethod(scenarioController.data(), "startCommand", Qt::QueuedConnection, Q_ARG(QString, program));
}

void ScenarioProcess::restartScenario()
{
    ARMARX_IMPORTANT << "restarting scenario " << scenarioName;
    QString program = "$HOME/repos/ArmarXCore/build/bin/armarx scenario restart " + scenarioName + " -p " + this->packageName;
    if (x11 == X11Config_disabled)
    {
        QMetaObject::invokeMethod(scenarioController.data(), "startCommand", Qt::QueuedConnection, Q_ARG(QString, program));
    }
    else
    {
        //need to source directly, as this will establish a different ssh connection
        program = "source '$HOME/repos/.armarxrc'; " + program;
        QMetaObject::invokeMethod(scenarioController.data(), "startX11Command", Qt::QueuedConnection, Q_ARG(QString, program));
    }
}

void ScenarioProcess::processOutput(QString output)
{
    using namespace boost;
    std::string outstr = output.toStdString();
    std::size_t stopFound = outstr.find("Stopped");
    std::size_t runFound = outstr.find("Running");
    std::size_t mixFound = outstr.find("Mixed");

    if (stopFound != std::string::npos)
    {
        //                qDebug() << "stopped detected";
        status = "Stopped";
        this->statusLabel->setStyleSheet("QLabel {color: red; font-weight: normal}");
    }
    else if (runFound != std::string::npos)
    {
        //                qDebug() << "running detected";
        status = "Running";
        this->statusLabel->setStyleSheet("QLabel {color: green; font-weight: bold}");
    }
    else if (mixFound != std::string::npos)
    {
        //                qDebug() << "mixed detected";
        status = "Mixed";
        this->statusLabel->setStyleSheet("QLabel {color: orange; font-weight: normal}");
    }
    else
    {
        //                qDebug() << "unknown detected";
        status = "Unknown";
        this->statusLabel->setStyleSheet("QLabel {color: black; font-weight: normal}");
        if (!hasSSHError && !hasScenarioError)
            ARMARX_INFO << "Unknown output from scenario periodic_status:\n"
                        << outstr;
    }
    //Error handling
    if (hasSSHError)
    {
        this->statusLabel->setStyleSheet("QLabel {color: black; font-weight:bold}");
        status = "SSHError";
    }
    else if (hasScenarioError)
    {
        this->statusLabel->setStyleSheet("QLabel {color: black; font-weight:bold}");
        status = "scenarioError";
    }
    this->statusLabel->setText(status);
    QMetaObject::invokeMethod(this->refreshCountdown.data(), "stop", Qt::QueuedConnection);
    QMetaObject::invokeMethod(this->refreshCountdown.data(), "start", Qt::QueuedConnection);
}

void ScenarioProcess::sshError()
{
    status = "SSHError";
    hasSSHError = true;
    processOutput("SSHError");
}

void ScenarioProcess::scenarioError()
{
    status = "ScenarioError";
    hasScenarioError = true;
    processOutput("ScenarioError");
}

void ScenarioProcess::timeoutError()
{
    QMetaObject::invokeMethod(this->refreshCountdown.data(), "stop", Qt::QueuedConnection);
    //reinitialize values
    hasSSHError = false;
    hasScenarioError = false;
    //reinitialize the ssh controller
    ARMARX_INFO << "reconnecting to unresponsive Host: " << this->hostName;
    connectSsh();
}
