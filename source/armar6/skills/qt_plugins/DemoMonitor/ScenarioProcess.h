#pragma once

#include "SshController.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>

#include <QObject>
#include <qcoreapplication.h>
#include <QTimer>
#include <QString>
#include <QLabel>
#include <QPushButton>

namespace armarx
{
    enum X11Config
    {
        X11Config_disabled = 0,
        X11Config_enabled = 1,
    };
    // source '$HOME/repos/.armarxrc';
    struct ScenarioProcessConfig
    {
        QString scenarioName;
        QString scenarioPackage;
        QString hostPC;
        QString user;
        X11Config x11Needed = X11Config_disabled;
    };
    class ScenarioProcess: public QObject
    {
        Q_OBJECT
    private:
        QSharedPointer<QTimer> refreshCountdown;
        QSharedPointer<SshController> scenarioController;
        QSharedPointer<SshController> statusController;
        int statusChannelNr = -1;
        int runChannelNr = -1;

        const int REFRESH_COUNTDOWN_INTERVAL = 60000;
        void initController();
    public:
        QString scenarioName;
        QString packageName;
        QString hostName;
        QString userName;
        X11Config x11;

        QPushButton* startButton;
        QPushButton* stopButton;
        QPushButton* restartButton;
        QLabel* statusLabel;
        QString status = "";
        bool hasSSHError = false;
        bool hasScenarioError = false;

        explicit ScenarioProcess(const QString& name, const QString& pkg, const QString& host, const QString& user,
                                 QPushButton* b_start, QPushButton* b_stop, QPushButton* b_restart, QLabel* label,
                                 X11Config x11 = X11Config_disabled, QObject* parent = 0);
        virtual ~ScenarioProcess();
        void connectSsh(QString const& host = "", QString const& user = "");
        void startListening();
        void stopListening();

    public slots:
        void startScenario();
        void stopScenario();
        void restartScenario();
        void processOutput(QString output);
        void scenarioError();
        void sshError();
        void timeoutError();
    signals:
        void statusUpdate();
    };
}
