/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    GuiPluginSsh::qt_plugins::SshGuiPluginGuiPlugin
 * \author     Fabian Paus ( fabian dot paus at kit dot edu )
 * \date       2019
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SshController.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>

using namespace armarx;

SshController::SshController(QString const& hostName, QString const& user, QObject* parent)
    : QObject(parent), hostName(hostName), user(user)
{
    timer = new QTimer(this);
    timer->setInterval(500);
    connect(timer, SIGNAL(timeout()), this, SLOT(pollData()), Qt::DirectConnection);

    //this allows using localhost as well
    if (hostName == "" || hostName == "localhost" || hostName == "CONTROL")
    {
        this->hostName = "localhost";
    }
    ARMARX_INFO << "ssh-Connect: " << user << "@" << hostName;
    sshConnectPubkey();
    if (!connected)
    {
        sshCleanup();
    }
    else
    {
        ARMARX_INFO << "Connection successful!";
    }
}

SshController::~SshController()
{
    sshCleanup();
}

bool SshController::isConnected()
{
    return connected;
}

bool SshController::isRunning()
{
    return channel != nullptr;
}
/*
 * Connect to host using the SSH agent
 */
void SshController::sshConnectPubkey()
{
    connected = false;
    this->session = ssh_new();
    if (session == nullptr)
    {
        ARMARX_ERROR << "Could not create ssh session";
        return;
    }

    int port = 22;
    int returncode;
    int verbosity = SSH_LOG_NOLOG;
    long timeout = 3;

    ssh_options_set(session, SSH_OPTIONS_LOG_VERBOSITY, &verbosity);
    ssh_options_set(session, SSH_OPTIONS_PORT, &port);
    ssh_options_set(session, SSH_OPTIONS_HOST, hostName.toStdString().c_str());
    //smaller timeout if robot is not reachable
    ssh_options_set(session, SSH_OPTIONS_TIMEOUT, &timeout);
    if (!user.isEmpty())
    {
        ssh_options_set(session, SSH_OPTIONS_USER, user.toStdString().c_str());
    }
    returncode = ssh_connect(this->session);
    if (returncode != SSH_OK)
    {
        ARMARX_ERROR << "ssh connection error on " << this->hostName << ": " << ssh_get_error(this->session);
        return;
    }
    //ARMARX_INFO << "Authenticate for " << this->hostName;
    returncode = ssh_userauth_publickey_auto(this->session, NULL, NULL);
    switch (returncode)
    {
        case SSH_AUTH_ERROR:
            ARMARX_ERROR << "Ssh authentication! error on " << this->hostName << ": " << ssh_get_error(this->session);
            break;
        case SSH_AUTH_DENIED:
            ARMARX_ERROR << "Ssh key invalid on " << this->hostName;
            break;
        case SSH_AUTH_PARTIAL:
            ARMARX_ERROR << "Ssh authentication incomplete on " << this->hostName;
            break;
        default:
            connected = true;
    }
}
/*
 * disconnect and free resources
 */
void SshController::sshCleanup()
{
    stopCommand();
    //close connections
    if (session != nullptr)
    {
        if (connected)
        {
            ssh_disconnect(this->session);
        }
        ssh_free(this->session);
    }
    connected = false;
    session = nullptr;
}
/*
 * Start given command via libssh and associate it to controller by storing the corresponding channel.
 */
void SshController::startCommand(QString command)
{
    if (!connected)
    {
        ARMARX_INFO << "on host " << this->hostName << ": trying to ssh on disconnected session";
        return;
    }
    int returncode;
    //stop old command
    if (channel != nullptr)
    {
        stopCommand();
    }
    //open channel
    channel = ssh_channel_new(this->session);
    returncode = ssh_channel_open_session(channel);
    if (returncode != SSH_OK)
    {
        ARMARX_ERROR_S << "Ssh error on host " << this->hostName << ": " << returncode;
        ssh_channel_free(channel);
        channel = nullptr;
        return;
    }
    //run executable
    try
    {
        returncode = ssh_channel_request_exec(channel, command.toStdString().c_str());
    }
    catch (...)
    {
        ARMARX_ERROR << "Exception while trying to execute ssh command: " << command;
        ssh_channel_close(channel);
        ssh_channel_free(channel);
        channel = nullptr;
        emit(applicationError());
    }
    //Error handling
    if (returncode != SSH_OK)
    {
        ARMARX_ERROR << "Ssh error on host " << this->hostName << ": " << returncode;
        ssh_channel_close(channel);
        ssh_channel_free(channel);
        channel = nullptr;
        emit(sshError());
    }
    else
    {
        //start output listener
        QMetaObject::invokeMethod(timer, "start");
    }
}
/*
 * Close the channel of the associated remote command to stop its execution
 */
void SshController::stopCommand()
{
    if (channel != nullptr)
    {
        timer->stop();
        //does not Work with periodic_status!
        const char* signal = "HUP";
        ssh_channel_request_send_signal(channel, signal);
        if (!ssh_channel_is_eof(channel))
        {
            ssh_channel_send_eof(channel);
        }
        if (ssh_channel_is_open(channel))
        {
            ssh_channel_close(channel);
        }
        ssh_channel_free(channel);
        channel = nullptr;
    }
}
/*
 * Start a detached ssh command via system()
 */
void SshController::startX11Command(QString command)
{
    //build up shell command string
    QString sshCommand = "ssh -X ";
    if (user != "")
    {
        sshCommand += user + "@";
    }
    sshCommand += hostName + " " + command + " &";
    //execute command via "/bin/sh -c"
    ARMARX_INFO << "Executing:\n" << sshCommand;
    int rc = system(sshCommand.toStdString().c_str());
    if (rc != 0)
    {
        ARMARX_INFO << "Unexpected return code for command\n" << sshCommand << ":\n " << rc;
    }
}
/*
 * Read the output of the associated remote command
 */
void SshController::pollData()
{
    QByteArray buffer;
    buffer.resize(256);
    // read in command result
    int totalBytes = 0, newBytes = 0;
    //prevent accidental segfault
    if (channel == nullptr)
    {
        return;
    }
    int loopcnt = 0;
    do
    {
        newBytes = ssh_channel_read_nonblocking(this->channel, &(buffer.data()[totalBytes]), buffer.size() - totalBytes, 0);
        if (newBytes > 0)
        {
            totalBytes += newBytes;
        }
        //wait 1 second at most
        else if (loopcnt < 20)
        {
            usleep(50);
            loopcnt++;
        }
        else
        {
            break;
        }
    }
    //need to check for newlines in condition to get full messages
    while (!QString(buffer).contains("\n") && channel != nullptr);
    QString response = QString(buffer).mid(0, totalBytes);
    QStringList split = response.split("\n");
    split.removeAll("");
    if (split.size() > 0)
    {
        emit readLine(split.first());
    }
}

/*
 * This method is not recommended as it executes a blocking remote call
 */
QString SshController::execCommand(QString command)
{
    if (!this->connected)
    {
        ARMARX_ERROR << "on host " << this->hostName << ": trying to ssh on disconnected session";
        return "";
    }
    //for error handling
    int rc;
    //open channel
    auto channel = ssh_channel_new(this->session);
    rc = ssh_channel_open_session(channel);
    if (rc != SSH_OK)
    {
        ssh_channel_free(channel);
        ARMARX_ERROR << "Ssh error on host " << this->hostName << ": " << rc;
        return "";
    }
    //run executable
    rc = ssh_channel_request_exec(channel, command.toStdString().c_str());
    if (rc != SSH_OK)
    {
        ssh_channel_close(channel);
        ssh_channel_free(channel);
        ARMARX_ERROR << "Ssh error on host " << this->hostName << ": " << rc;
        return "";
    }
    //retrieve output
    char buffer[256];
    int nbytes;
    int loopCount = 0;
    QString result;

    nbytes = ssh_channel_read_timeout(channel, buffer, sizeof(buffer), 0, 1000);
    while (nbytes > 0 && loopCount <= 10)
    {
        std::string curr = std::string(buffer);
        QString qCurr = QString::fromStdString(curr);
        result.append(qCurr);
        result.append(QChar(' '));
        nbytes = ssh_channel_read_timeout(channel, buffer, sizeof(buffer), 0, 500);
        loopCount++;
    }
    //close channel
    ssh_channel_send_eof(channel);
    ssh_channel_close(channel);
    ssh_channel_free(channel);
    return result;
}
