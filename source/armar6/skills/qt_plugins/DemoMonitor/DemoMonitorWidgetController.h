/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::qt_plugins::DemoMonitorWidgetController
 * @author     KHitzler ( kevin dot hitzler at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <armar6/skills/qt_plugins/DemoMonitor/ui_DemoMonitorWidget.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/core/time/TimeUtil.h>

#include <RobotAPI/interface/components/RobotHealthInterface.h>
#include <IceUtil/UUID.h>

#include <QMessageBox>
#include <QTimer>
#include <QPushButton>
#include <QLabel>
#include <QDebug>
#include <QRegExp>
#include <qcoreapplication.h>
#include <QString>
#include <QList>
#include <qobjectdefs.h>

#include <vector>
#include <string>

#include "RemoteHost.h"
#include "ScenarioProcess.h"
#include "DemoMonitorConfigDialog.h"
namespace armarx
{
    class CustomCommand: public QObject
    {
        Q_OBJECT
    public:
        QString buttonCaption;
        QString hostPC;
        QString userName;
        QString command;
        X11Config x11;
        explicit CustomCommand(QString buttonCaption, QString hostPC, QString userName, QString command, X11Config x11 = X11Config_disabled, QObject* parent = 0): QObject(parent),
            buttonCaption(buttonCaption), hostPC(hostPC), userName(userName), command(command), x11(x11)
        {
        }
        void connectSsh(QString const& host = "", QString const& user = "")
        {
            if (host != "")
            {
                this->hostPC = host;
            }
            if (user != "")
            {
                this->userName = user;
            }
            controller = QSharedPointer<SshController>(new SshController(this->hostPC, this->userName), &QObject::deleteLater);
            connect(controller.data(), SIGNAL(readLine(QString)), this, SLOT(printOutput(QString)));
        }
    public slots:
        void execCommand()
        {
            if (x11 == X11Config_disabled)
            {
                QMetaObject::invokeMethod(controller.data(), "startCommand", Q_ARG(QString, command));
            }
            else
            {
                QMetaObject::invokeMethod(controller.data(), "startX11Command", Q_ARG(QString, command));
            }
        }
        void printOutput(QString output)
        {
            ARMARX_INFO << "Output of " << buttonCaption << ":\n" << output;
        }
    private:
        QSharedPointer<SshController> controller;
    };
    /**
    \page ArmarXGui-GuiPlugins-DemoMonitor DemoMonitor
    \brief The DemoMonitor allows visualizing ...

    \image html DemoMonitor.png
    The user can

    API Documentation \ref DemoMonitorWidgetController

    \see DemoMonitorGuiPlugin
    */

    /**
     * \class DemoMonitorWidgetController
     * \brief DemoMonitorWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        DemoMonitorWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < DemoMonitorWidgetController >
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit DemoMonitorWidgetController();

        /**
         * Controller destructor
         */
        virtual ~DemoMonitorWidgetController();

        QPointer<QDialog> getConfigDialog(QWidget* parent = 0) override;

        void configured() override;

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "DemoMonitor";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;

        void onDisconnectComponent() override;

    public slots:
        /* QT slot declarations */
        void onBtnShutdownArmar6Clicked();
        // From GUIHealthPlugin
        void healthTimerClb();
        void updateSummaryTimerClb();
        void unregisterSelf();

        //scenario part
        void updateScenarioStatus();
        void startScenario(int index);
        void stopScenario(int index);
        void restartScenario(int index);

        void startAllScenarios();
        void stopAllScenarios();
        void restartAllScenarios();

        void initPerformanceMonitorGui();

        static void updatePerformanceMonitorMaxRam(RemoteHost*);
        static void initPerformanceMonitorSession(RemoteHost*);

    signals:
        void getPerformanceMonitorValuesFinished(RemoteHost*);
        void maxRamUpdated(RemoteHost*);

    private:
        /**
         * Widget Form
         */
        Ui::DemoMonitorWidget widget;
        QPointer<DemoMonitorConfigDialog> dialog;

        // From GuiHealthPlugin
        RobotHealthInterfacePrx robotHealthTopicPrx;
        RobotHealthComponentInterfacePrx robotHealthComponentPrx;
        //        QTimer* healthTimer;
        QTimer* updateSummaryTimer;
        //Custom commands
        std::vector<CustomCommand*> commands;
        // scenarios
        QTimer* updateScenarioStatusTimer;
        QList<ScenarioProcess*> scenarios;
        //for dynamic GUI creation
        QLabel* scenario_all;
        QPushButton* startButton_all;
        QPushButton* stopButton_all;
        QPushButton* restartButton_all;
        QLabel* label_all;

        const QString alertHtml = "<font color=\"DeepPink\">";
        const QString notifyHtml = "<font color=\"Lime\">";
        const QString infoHtml = "<font color=\"Aqua\">";
        const QString endHtml = "</font><br>";

        const int PERFORMANCE_MONITOR_TIMER_INTERVAL = 5000;
        const int UPDATE_SCENARIO_STATUS_TIMER_INTERVAL = 1000;

        QString ARMAR6_USER = "armar-demo";

        const QString VMSTAT_ERROR_MSG = "vmstat could not be parsed";
        const QString SSH_ERROR_MSG = "Could not connect via SSH";

        RemoteHost* host1;
        RemoteHost* host2;
        RemoteHost* host3;
        RemoteHost* host4;
        RemoteHost* host5;
        RemoteHost* host6;
    };


}
