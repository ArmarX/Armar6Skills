/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    GuiPluginSsh::qt_plugins::SshGuiPlugin
 * \author     Fabian Paus ( fabian dot paus at kit dot edu )
 * \date       2019
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QObject>
#include <QProcess>
#include <QTimer>
#include <QSharedPointer>
#include <QString>
#include <QDebug>

// REQUIRES libssh-4 libssh-dev
#include <libssh/libssh.h>

namespace armarx
{
    /*
     * Enables to execute one remote command at a time on the specified host via libssh
     */
    class SshController : public QObject
    {
        Q_OBJECT
    public:
        explicit SshController(QString const& hostName, QString const& user, QObject* parent = 0);
        virtual ~SshController();
        bool isConnected();
        bool isRunning();
        void sshCleanup();
    public slots:
        void startCommand(QString command);
        void stopCommand();
        void startX11Command(QString command);
        void pollData();
        QString execCommand(QString command);
    signals:
        void readLine(QString line);
        void applicationError();
        void sshError();
    private:
        QString hostName;
        QString user;
        ssh_session session = nullptr;
        ssh_channel channel = nullptr;
        QStringList x11ProgramList;
        QTimer* timer;
        bool connected = false;
        void sshConnectPubkey();
    };
}
