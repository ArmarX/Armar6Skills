/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    armar6_skills::qt_plugins::DemoMonitorWidgetController
 * \author     KHitzler ( kevin dot hitzler at kit dot edu )
 * \date       2019
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DemoMonitorWidgetController.h"
#include <armar6/skills/qt_plugins/DemoMonitor/ui_DemoMonitorConfigDialog.h>

#include <cstdlib>
#include <cstdio>

#include <iostream>
#include <sstream>

#include <array>
#include <QtCore>
#include <QtConcurrent/QtConcurrent>

using namespace armarx;

DemoMonitorWidgetController::DemoMonitorWidgetController()
{
    widget.setupUi(getWidget());

    //init heartbeat
    //    robotHealthTopicPrx = getTopic<RobotHealthInterfacePrx>("RobotHealthTopic");
    //    robotHealthComponentPrx = getProxy<RobotHealthComponentInterfacePrx>("RobotHealth");
    //    QMetaObject::invokeMethod(this->updateSummaryTimer.data(), "start", Qt::QueuedConnection);
    QLabel* labelState = widget.labelState;
    labelState->setText("Sending heartbeats ...");

    updateSummaryTimer = new QTimer(this);
    updateSummaryTimer->setInterval(100);
    connect(updateSummaryTimer, SIGNAL(timeout()), this, SLOT(updateSummaryTimerClb()));

    //performance monitor
    host1 = new RemoteHost("Control PC", widget.Computer1, widget.Computer1CPUProgress, widget.Computer1MemoryProgress,
                           widget.Computer1Log, widget.Computer1Enable, this);
    host2 = new RemoteHost("Visualizing PC", widget.Computer2, widget.Computer2CPUProgress, widget.Computer2MemoryProgress,
                           widget.Computer2Log, widget.Computer2Enable, this);
    host3 = new RemoteHost("Realtime PC", widget.Computer3, widget.Computer3CPUProgress, widget.Computer3MemoryProgress,
                           widget.Computer3Log, widget.Computer3Enable, this);
    host4 = new RemoteHost("Planning PC", widget.Computer4, widget.Computer4CPUProgress, widget.Computer4MemoryProgress,
                           widget.Computer4Log, widget.Computer4Enable, this);
    host5 = new RemoteHost("Speech PC", widget.Computer5, widget.Computer5CPUProgress, widget.Computer5MemoryProgress,
                           widget.Computer5Log, widget.Computer5Enable, this);
    host6 = new RemoteHost("Vision PC", widget.Computer6, widget.Computer6CPUProgress, widget.Computer6MemoryProgress,
                           widget.Computer6Log, widget.Computer6Enable, this);
    //commands
    commands =
    {
        new CustomCommand("reconnect Roboception", "armar6a-3", ARMAR6_USER, "$HOME/reconnect_roboception.sh", X11Config_disabled, this),
        new CustomCommand("xclock", "i61a6ac002", "", "xclock", X11Config_enabled, this)
    };

    for (std::size_t i = 0; i < commands.size(); i++)
    {
        QPushButton* startButton = new QPushButton(widget.controlBox);
        startButton->setText(commands[i]->buttonCaption);
        startButton->setCursor(QCursor(Qt::PointingHandCursor));
        connect(startButton, SIGNAL(clicked()), commands[i], SLOT(execCommand()));
        widget.controlBoxLayout->addWidget(startButton);
    }

    //scenarios
    std::vector<ScenarioProcessConfig> configs =
    {
        { "Armar6UnitOnly", "armar6_rt", "armar6a-0", ARMAR6_USER, X11Config_disabled },
        { "Armar6LowLevelComponents", "armar6_rt", "armar6a-1", ARMAR6_USER, X11Config_disabled },
        { "Armar6HighlevelApps", "armar6_skills", "armar6a-1", ARMAR6_USER, X11Config_disabled },
        { "Armar6Cameras", "armar6_skills", "armar6a-3", ARMAR6_USER, X11Config_disabled },
        { "Armar6Vision", "armar6_skills", "armar6a-3", ARMAR6_USER, X11Config_enabled },
        { "CeBITTasks", "armar6_skills", "armar6a-1", ARMAR6_USER, X11Config_disabled },
        //  { "Armar6StreamDecoding", "armar6_skills", "localhost"}
    };

    QIcon icon0;
    icon0.addFile(QStringLiteral(":/icons/media-playback-start.ico"), QSize(), QIcon::Normal, QIcon::Off);
    QIcon icon1;
    icon1.addFile(QStringLiteral(":/icons/process-stop-7.ico"), QSize(), QIcon::Normal, QIcon::Off);
    QIcon icon2;
    icon2.addFile(QStringLiteral(":/icons/view-refresh-7.png"), QSize(), QIcon::Normal, QIcon::Off);

    //init scenario_All
    scenario_all = new QLabel(widget.scenarioAreaWidget);
    scenario_all->setObjectName(QStringLiteral("scenario_all"));
    scenario_all->setAlignment(Qt::AlignLeading | Qt::AlignLeft | Qt::AlignVCenter);
    scenario_all->setText("All Scenarios");
    startButton_all = new QPushButton(widget.scenarioAreaWidget);
    startButton_all->setObjectName(QStringLiteral("startButton_all"));
    startButton_all->setIcon(icon0);
    startButton_all->setCursor(QCursor(Qt::PointingHandCursor));
    stopButton_all = new QPushButton(widget.scenarioAreaWidget);
    stopButton_all->setObjectName(QStringLiteral("stopButton_all"));
    stopButton_all->setIcon(icon1);
    stopButton_all->setCursor(QCursor(Qt::PointingHandCursor));
    restartButton_all = new QPushButton(widget.scenarioAreaWidget);
    restartButton_all->setObjectName(QStringLiteral("restartButton_all"));
    restartButton_all->setIcon(icon2);
    restartButton_all->setCursor(QCursor(Qt::PointingHandCursor));
    label_all = new QLabel(widget.scenarioAreaWidget);
    label_all->setObjectName(QStringLiteral("label_all"));

    widget.scenarioGrid->addWidget(scenario_all, 0, 0, 1, 1);
    widget.scenarioGrid->addWidget(startButton_all, 0, 1, 1, 1);
    widget.scenarioGrid->addWidget(stopButton_all, 0, 2, 1, 1);
    widget.scenarioGrid->addWidget(restartButton_all, 0, 3, 1, 1);
    widget.scenarioGrid->addWidget(label_all, 0, 4, 1, 1);
    // scenario Summary
    updateScenarioStatusTimer = new QTimer(this);
    updateScenarioStatusTimer->setInterval(this->UPDATE_SCENARIO_STATUS_TIMER_INTERVAL);

    //dynamically add scenarios
    for (std::size_t i = 0; i < configs.size(); i++)
    {
        QLabel* scenarioName = new QLabel(widget.scenarioAreaWidget);
        scenarioName->setObjectName(QStringLiteral("scenarioName_%1").arg(i));
        scenarioName->setAlignment(Qt::AlignLeading | Qt::AlignLeft | Qt::AlignVCenter);
        scenarioName->setText(configs[i].scenarioName);
        QPushButton* startButton = new QPushButton(widget.scenarioAreaWidget);
        startButton->setObjectName(QStringLiteral("startButton_%1").arg(i));
        startButton->setIcon(icon0);
        startButton->setCursor(QCursor(Qt::PointingHandCursor));
        QPushButton* stopButton = new QPushButton(widget.scenarioAreaWidget);
        stopButton->setObjectName(QStringLiteral("stopButton_%1").arg(i));
        stopButton->setIcon(icon1);
        stopButton->setCursor(QCursor(Qt::PointingHandCursor));
        QPushButton* restartButton = new QPushButton(widget.scenarioAreaWidget);
        restartButton->setObjectName(QStringLiteral("restartButton_%1").arg(i));
        restartButton->setIcon(icon2);
        restartButton->setCursor(QCursor(Qt::PointingHandCursor));
        QLabel* scenarioStatus = new QLabel(widget.scenarioAreaWidget);
        scenarioStatus->setObjectName(QStringLiteral("scenarioStatus_%1").arg(i));

        widget.scenarioGrid->addWidget(scenarioName,  i + 1, 0, 1, 1);
        widget.scenarioGrid->addWidget(startButton,   i + 1, 1, 1, 1);
        widget.scenarioGrid->addWidget(stopButton,    i + 1, 2, 1, 1);
        widget.scenarioGrid->addWidget(restartButton, i + 1, 3, 1, 1);
        widget.scenarioGrid->addWidget(scenarioStatus, i + 1, 4, 1, 1);

        ScenarioProcess* scenario = new ScenarioProcess(configs[i].scenarioName, configs[i].scenarioPackage, configs[i].hostPC,
                configs[i].user, startButton, stopButton, restartButton, scenarioStatus, configs[i].x11Needed, this);
        scenarios.push_back(scenario);
    }
}

DemoMonitorWidgetController::~DemoMonitorWidgetController()
{

}

QPointer<QDialog> DemoMonitorWidgetController::getConfigDialog(QWidget* parent)
{
    if (!dialog)
    {
        dialog = new DemoMonitorConfigDialog(parent);
    }
    dialog->setUsername(ARMAR6_USER);
    return qobject_cast<DemoMonitorConfigDialog*>(dialog);
}

void DemoMonitorWidgetController::configured()
{
    ARMAR6_USER = dialog->getUsername();
}

void DemoMonitorWidgetController::loadSettings(QSettings* settings)
{

}

void DemoMonitorWidgetController::saveSettings(QSettings* settings)
{

}


void DemoMonitorWidgetController::onInitComponent()
{
    //usingProxy("RobotHealth");
}

void DemoMonitorWidgetController::onConnectComponent()
{
    //establish ssh connections for performance monitor
    host1->connectSsh("i61a6ac001", "");
    host2->connectSsh("i61a6ac002", "");
    host3->connectSsh("armar6a-0", ARMAR6_USER);
    host4->connectSsh("armar6a-1", ARMAR6_USER);
    host5->connectSsh("armar6a-2", ARMAR6_USER);
    host6->connectSsh("armar6a-3", ARMAR6_USER);
    //establish ssh connections for custom commands
    for (CustomCommand* command : commands)
    {
        command->connectSsh("", ARMAR6_USER);
    }
    //establish ssh connections for Scenarios
    for (ScenarioProcess* scenario : scenarios)
    {
        scenario->connectSsh("", ARMAR6_USER);
    }

    // GUI elements
    connect(widget.btnShutdownArmar6, SIGNAL(clicked()), this, SLOT(onBtnShutdownArmar6Clicked()));

    // init performance monitor gui
    QMetaObject::invokeMethod(this, "initPerformanceMonitorGui", Qt::QueuedConnection);

    // Scenario summary
    connect(startButton_all, SIGNAL(clicked()), this, SLOT(startAllScenarios()));
    connect(stopButton_all, SIGNAL(clicked()), this, SLOT(stopAllScenarios()));
    connect(restartButton_all, SIGNAL(clicked()), this, SLOT(restartAllScenarios()));
    connect(updateScenarioStatusTimer, SIGNAL(timeout()), this, SLOT(updateScenarioStatus()));
    QMetaObject::invokeMethod(updateScenarioStatusTimer, "start", Qt::QueuedConnection);
}

void DemoMonitorWidgetController::onDisconnectComponent()
{
    QMetaObject::invokeMethod(updateScenarioStatusTimer, "stop", Qt::QueuedConnection);
    QMetaObject::invokeMethod(updateSummaryTimer, "stop", Qt::QueuedConnection);
}

void DemoMonitorWidgetController::healthTimerClb()
{
    RobotHealthHeartbeatArgs rhha;
    rhha.maximumCycleTimeWarningMS = 250;
    rhha.maximumCycleTimeErrorMS = 500;
    robotHealthTopicPrx->heartbeat(getName(), RobotHealthHeartbeatArgs());
}
void DemoMonitorWidgetController::updateSummaryTimerClb()
{
    if (robotHealthComponentPrx)
    {
        try
        {
            widget.labelHealthState->setText(QString::fromUtf8(robotHealthComponentPrx->getSummary().c_str()));
        }
        catch (...)
        {}
    }
}

void DemoMonitorWidgetController::unregisterSelf()
{
    robotHealthTopicPrx->unregister(getName());
    widget.labelState->setText("inactive.");
    widget.pushButtonUnregister->setDisabled(true);
}

void DemoMonitorWidgetController::onBtnShutdownArmar6Clicked()
{
    // make sure the user wants to shutdown the demo
    QMessageBox messageBox;
    QMessageBox::StandardButton qShutdownArmar6 = messageBox.question(0, "Shutdown Armar-6",
            "Please make sure that the robot is in a safe state.\n\nAre you sure that you want to shutdown Armar-6?");
    messageBox.setFixedSize(500, 200);

    if (qShutdownArmar6 == QMessageBox::Yes)
    {
        // execute shutdown-armar6.sh script on rt
        SshController controller(host3->hostName, ARMAR6_USER, this);
        if (!controller.isConnected())
        {
            ARMARX_IMPORTANT << "No shutdown triggered because of ssh error";
            return;
        }
        QString command("$HOME/repos/armar6_rt/build/bin/shutdown-Armar6.sh");
        controller.startCommand(command);
        ARMARX_IMPORTANT << "Armar-6 shutdown triggered. Bye.";
    }
}

void DemoMonitorWidgetController::updateScenarioStatus()
{
    QString stati = "";
    QString str = scenarios.at(0)->status;
    bool all_equal = true;
    for (int i = 1; i < scenarios.size() && all_equal; i++)
    {
        all_equal = str == scenarios.at(i)->status;
    }
    if (all_equal)
    {
        stati = str;
        if (stati == "Running")
        {
            label_all->setStyleSheet("QLabel {color: green;}");
        }
        else if (stati.trimmed() == "Stopped")
        {
            label_all->setStyleSheet("QLabel {color: red;}");
        }
    }
    else
    {
        label_all->setStyleSheet("QLabel {color: orange;}");
        stati = "Mixed";
    }
    label_all->setText(stati);
}

void DemoMonitorWidgetController::startScenario(int index)
{
    QMetaObject::invokeMethod(scenarios.at(index), "startScenario", Qt::QueuedConnection);
}

void DemoMonitorWidgetController::stopScenario(int index)
{
    QMetaObject::invokeMethod(scenarios.at(index), "stopScenario", Qt::QueuedConnection);
}

void DemoMonitorWidgetController::restartScenario(int index)
{
    QMetaObject::invokeMethod(scenarios.at(index), "restartScenario", Qt::QueuedConnection);
}

void DemoMonitorWidgetController::startAllScenarios()
{
    ARMARX_IMPORTANT << "starting all scenarios";
    for (int i = 0; i < scenarios.size(); i++)
    {
        startScenario(i);
    }
}

void DemoMonitorWidgetController::stopAllScenarios()
{
    ARMARX_IMPORTANT << "stopping all scenarios";
    for (int i = 0; i < scenarios.size(); i++)
    {
        stopScenario(i);
    }
}

void DemoMonitorWidgetController::restartAllScenarios()
{
    ARMARX_IMPORTANT << "restarting all scenarios";
    for (int i = 0; i < scenarios.size(); i++)
    {
        restartScenario(i);
    }
}

void DemoMonitorWidgetController::updatePerformanceMonitorMaxRam(RemoteHost* h)
{
    QMetaObject::invokeMethod(h, "updateMaxRam", Qt::QueuedConnection);
}

void DemoMonitorWidgetController::initPerformanceMonitorSession(RemoteHost* h)
{
    //ARMARX_INFO << "Init session of host " << h->alias;
    QMetaObject::invokeMethod(h, "start", Qt::QueuedConnection);
}

void DemoMonitorWidgetController::initPerformanceMonitorGui()
{
    // init the displays
    auto threadHost1 = QtConcurrent::run(DemoMonitorWidgetController::updatePerformanceMonitorMaxRam, this->host1);
    auto threadHost2 = QtConcurrent::run(DemoMonitorWidgetController::updatePerformanceMonitorMaxRam, this->host2);
    auto threadHost3 = QtConcurrent::run(DemoMonitorWidgetController::updatePerformanceMonitorMaxRam, this->host3);
    auto threadHost4 = QtConcurrent::run(DemoMonitorWidgetController::updatePerformanceMonitorMaxRam, this->host4);
    auto threadHost5 = QtConcurrent::run(DemoMonitorWidgetController::updatePerformanceMonitorMaxRam, this->host5);
    auto threadHost6 = QtConcurrent::run(DemoMonitorWidgetController::updatePerformanceMonitorMaxRam, this->host6);

    threadHost1.waitForFinished();
    threadHost2.waitForFinished();
    threadHost3.waitForFinished();
    threadHost4.waitForFinished();
    threadHost5.waitForFinished();
    threadHost6.waitForFinished();

    //start commands
    QtConcurrent::run(DemoMonitorWidgetController::initPerformanceMonitorSession, this->host1);
    QtConcurrent::run(DemoMonitorWidgetController::initPerformanceMonitorSession, this->host2);
    QtConcurrent::run(DemoMonitorWidgetController::initPerformanceMonitorSession, this->host3);
    QtConcurrent::run(DemoMonitorWidgetController::initPerformanceMonitorSession, this->host4);
    QtConcurrent::run(DemoMonitorWidgetController::initPerformanceMonitorSession, this->host5);
    QtConcurrent::run(DemoMonitorWidgetController::initPerformanceMonitorSession, this->host6);
}
