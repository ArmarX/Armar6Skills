/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "DemoMonitorConfigDialog.h"
#include <armar6/skills/qt_plugins/DemoMonitor/ui_DemoMonitorConfigDialog.h>


#include <IceUtil/UUID.h>

using namespace armarx;

armarx::DemoMonitorConfigDialog::DemoMonitorConfigDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::DemoMonitorConfigDialog()),
    uuid(IceUtil::generateUUID())
{
    ui->setupUi(this);
}

armarx::DemoMonitorConfigDialog::~DemoMonitorConfigDialog()
{
    delete ui;
}

void armarx::DemoMonitorConfigDialog::onInitComponent()
{
}

void armarx::DemoMonitorConfigDialog::onConnectComponent()
{
}
std::string armarx::DemoMonitorConfigDialog::getDefaultName() const
{
    return "DemoMonitorConfigDialog" + uuid;
}
QString armarx::DemoMonitorConfigDialog::getUsername()
{
    return this->ui->editUserName->text();
}

void armarx::DemoMonitorConfigDialog::setUsername(QString username)
{
    this->ui->editUserName->setText(username);
}
