#include "RemoteHost.h"
using namespace armarx;

RemoteHost::RemoteHost(QString const& alias, QGroupBox* groupBox, QProgressBar* cpuProgressBar, QProgressBar* ramProgressBar,
                       QTextBrowser* textBrowser, QCheckBox* checkbox, QObject* parent):
    QObject(parent), alias(alias), group_box(groupBox), cpu_progress_bar(cpuProgressBar), ram_progress_bar(ramProgressBar),
    text_browser(textBrowser), checkbox(checkbox)
{
    //initialization
    RemoteHost::initGUI();
    //add a countdown to check if Gui updates delay too long
    refreshTimeoutCountdown = QSharedPointer<QTimer>(new QTimer(), &QObject::deleteLater);
    refreshTimeoutCountdown->setInterval(REFRESH_TIMEOUT_COUNTDOWN_INTERVAL);
    //connect the timer
    connect(refreshTimeoutCountdown.data(), SIGNAL(timeout()), this, SLOT(timeoutError()));
}

RemoteHost::~RemoteHost()
{
    QMetaObject::invokeMethod(this->refreshTimeoutCountdown.data(), "stop", Qt::QueuedConnection);
    refreshTimeoutCountdown.clear();
    this->controller->stopCommand();
    this->controller->startCommand("pkill -1 vmstat");
    controller.clear();
}

/*
 * Initialize the SshController and connect to the remote pc
 * Must be executed before using any functionality
 */
void RemoteHost::connectSsh(QString const& host, QString const& user)
{
    this->hostName = host;
    this->userName = user;
    //initialize the ssh controller
    controller = QSharedPointer<SshController>(new SshController(this->hostName, this->userName), &QObject::deleteLater);
    if (!this->controller->isConnected())
    {
        this->hasSSHError = true;
        this->error("ssh connection error");
        //reconnect after 60 seconds
        refreshTimeoutCountdown->setInterval(SSH_RECONNECT_COUNTDOWN_INTERVAL);
        QMetaObject::invokeMethod(refreshTimeoutCountdown.data(), "start", Qt::QueuedConnection);
    }
    //connect GUI updates
    connect(this->controller.data(), SIGNAL(readLine(QString)), this, SLOT(processOutput(QString)));
    connect(this, SIGNAL(statusUpdated()), this, SLOT(refreshGui()));
    //do not let ssh controller live in GUI thread
    this->controller->moveToThread(qApp->thread());
    refreshTimeoutCountdown->moveToThread(qApp->thread());
}

void RemoteHost::start()
{
    if (hasSSHError)
    {
        ARMARX_INFO_S << "SSH error on host " << alias;
        mutex.unlock();
        return;
    }
    QString command("vmstat -n 2");
    this->controller->startCommand(command);
    QMetaObject::invokeMethod(this->refreshTimeoutCountdown.data(), "start", Qt::QueuedConnection);
}

void RemoteHost::updateMaxRam()
{
    if (hasSSHError)
    {
        ARMARX_INFO_S << "SSH error on host " << alias;
        currentCPU = 0;
        currentRAM = 0;
        maxRam = 1;
        mutex.unlock();
        return;
    }

    //init maxRam values
    QString command("vmstat -s");
    QString out = controller->execCommand(command);

    QStringList split = out.split("\n");
    if (split.empty())
    {
        ARMARX_ERROR_S << "(1) Got wrong result from command vmstat -s for host " << alias;
        hasVmstatError = true;
        mutex.unlock();
        return;
    }

    QString totalMemory = split[0].trimmed();
    QStringList totalMemorySplit = totalMemory.split(" ");
    if (totalMemorySplit.empty() || totalMemorySplit.size() != 4 || totalMemorySplit[2] != "total" || totalMemorySplit[3] != "memory")
    {
        ARMARX_ERROR_S << "(2) Got wrong result from command vmstat -s for host " << alias;
        hasVmstatError = true;
        return;
    }

    this->maxRam = totalMemorySplit[0].toInt();
}

void RemoteHost::processOutput(QString output)
{
    if (this->hasSSHError || !this->checkbox->isChecked())
    {
        //ARMARX_INFO_S << "Ignoring host " << this->alias;
        this->currentCPU = 0;
        this->currentRAM = 0;
        emit(statusUpdated());
        return;
    }
    QStringList lastLineSplit = output.split(" ");
    lastLineSplit.removeAll("");

    if (lastLineSplit.empty() || lastLineSplit.size() < 15)
    {
        return;
    }

    int CPU_usage = 100 - lastLineSplit[14].toInt();
    int RAM_usage = ((this->maxRam - lastLineSplit[3].toLong() - lastLineSplit[5].toLong()) * 100) / this->maxRam;

    this->currentCPU = CPU_usage;
    this->currentRAM = RAM_usage;

    emit(statusUpdated());
}

void RemoteHost::refreshGui()
{
    if (hasSSHError)
    {
        error(this->SSH_ERROR_MSG);
        return;
    }
    if (hasVmstatError)
    {
        error(this->VMSTAT_ERROR_MSG);
        return;
    }

    int CPU_usage = currentCPU;
    int RAM_usage = currentRAM;

    QPalette cpu_p = cpu_progress_bar->palette();
    QPalette ram_p = ram_progress_bar->palette();
    if (CPU_usage < 50)
    {
        cpu_p.setColor(QPalette::Highlight, Qt::green);
    }
    else if (CPU_usage < 80)
    {
        cpu_p.setColor(QPalette::Highlight, Qt::yellow);
    }
    else
    {
        cpu_p.setColor(QPalette::Highlight, Qt::red);
    }

    if (RAM_usage < 50)
    {
        ram_p.setColor(QPalette::Highlight, Qt::green);
    }
    else if (RAM_usage < 80)
    {
        ram_p.setColor(QPalette::Highlight, Qt::yellow);
    }
    else
    {
        ram_p.setColor(QPalette::Highlight, Qt::red);
    }
    cpu_p.setColor(QPalette::Text, Qt::black);
    ram_p.setColor(QPalette::Text, Qt::black);

    cpu_progress_bar->setPalette(cpu_p);
    ram_progress_bar->setPalette(ram_p);


    cpu_progress_bar->setValue(CPU_usage);
    ram_progress_bar->setValue(RAM_usage);

    //ARMARX_INFO_S << "Updating finished for " << alias;

    if (!checkbox->isChecked())
    {
        //stop the refresh timeout
        QMetaObject::invokeMethod(this->refreshTimeoutCountdown.data(), "stop", Qt::QueuedConnection);
        offline();
    }
    else
    {
        //reset the refresh timeout
        QMetaObject::invokeMethod(this->refreshTimeoutCountdown.data(), "stop", Qt::QueuedConnection);
        QMetaObject::invokeMethod(this->refreshTimeoutCountdown.data(), "start", Qt::QueuedConnection);
        online();
    }
}

void RemoteHost::timeoutError()
{
    QMetaObject::invokeMethod(this->refreshTimeoutCountdown.data(), "stop", Qt::QueuedConnection);
    error(TIMEOUT_ERROR_MSG);
    //reinitialize values
    hasSSHError = false;
    hasVmstatError = false;
    channelId = -1;
    //reinitialize the ssh controller
    ARMARX_INFO << "reconnecting to unresponsive Host: " << this->alias;
    controller.reset(new SshController(this->hostName, this->userName));
    connect(this->controller.data(), SIGNAL(readLine(QString)), this, SLOT(processOutput(QString)));
    this->controller->moveToThread(qApp->thread());
    if (!this->controller->isConnected())
    {
        this->hasSSHError = true;
        this->error("ssh connection error");
        //reconnect after 60 seconds
        refreshTimeoutCountdown->setInterval(SSH_RECONNECT_COUNTDOWN_INTERVAL);
    }
    else
    {
        this->refreshTimeoutCountdown->setInterval(REFRESH_TIMEOUT_COUNTDOWN_INTERVAL);
        //restart vmstat
        QMetaObject::invokeMethod(this, "updateMaxRam", Qt::QueuedConnection);
        QMetaObject::invokeMethod(this, "start", Qt::QueuedConnection);
    }
    QMetaObject::invokeMethod(this->refreshTimeoutCountdown.data(), "start", Qt::QueuedConnection);
}

void RemoteHost::initGUI()
{
    this->group_box->setTitle(QString(this->alias));
    this->cpu_progress_bar->setRange(0, 100);
    this->ram_progress_bar->setRange(0, 100);
    this->checkbox->setChecked(true);
}

void RemoteHost::online()
{
    QString enabled("Online");
    QMetaObject::invokeMethod(this->text_browser, "setTextColor", Qt::QueuedConnection, Q_ARG(QColor, Qt::green));
    QMetaObject::invokeMethod(this->text_browser, "setText", Qt::QueuedConnection, Q_ARG(QString, enabled));
    this->hasVmstatError = false;
}

void RemoteHost::offline()
{
    QString disabled("Offline");
    QMetaObject::invokeMethod(this->text_browser, "setTextColor", Qt::QueuedConnection, Q_ARG(QColor, Qt::red));
    QMetaObject::invokeMethod(this->text_browser, "setText", Qt::QueuedConnection, Q_ARG(QString, disabled));
}

void RemoteHost::error(QString const& message)
{
    QMetaObject::invokeMethod(this->text_browser, "setTextColor", Qt::QueuedConnection, Q_ARG(QColor, Qt::red));
    QMetaObject::invokeMethod(this->text_browser, "setText", Qt::QueuedConnection, Q_ARG(QString, message));
    QMetaObject::invokeMethod(this->refreshTimeoutCountdown.data(), "stop", Qt::QueuedConnection);
}
