#pragma once

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>

#include <QMessageBox>
#include <QGroupBox>
#include <QCheckBox>
#include <QProgressBar>
#include <QTextBrowser>
#include <QPushButton>
#include <QLabel>

#include <QTimer>
#include <QString>
#include <QMutex>
#include <QSharedPointer>
#include <qcoreapplication.h>

#include "SshController.h"

namespace armarx
{
    class RemoteHost : public QObject
    {
        Q_OBJECT
    public:
        QString hostName;
        QString alias;
        QString userName;
        int maxRam = 1;

        int currentCPU = 0;
        int currentRAM = 0;

        const int REFRESH_TIMEOUT_COUNTDOWN_INTERVAL = 10000;
        const int SSH_RECONNECT_COUNTDOWN_INTERVAL = 60000;

        const QString VMSTAT_ERROR_MSG = "vmstat could not be parsed";
        const QString SSH_ERROR_MSG = "Could not connect via SSH";
        const QString TIMEOUT_ERROR_MSG = "no updates";

        explicit RemoteHost(QString const& alias, QGroupBox* groupBox, QProgressBar* cpuProgressBar, QProgressBar* ramProgressBar,
                            QTextBrowser* textBrowser, QCheckBox* checkbox, QObject* parent = 0);
        virtual ~RemoteHost();
        void connectSsh(QString const& host = "", QString const& user = "");
        void initGUI();
        void online();
        void offline();
        void error(QString const& message);
    public slots:
        void start();
        void updateMaxRam();
        void processOutput(QString output);
        void refreshGui();
        void timeoutError();
    signals:
        void statusUpdated();
    private:
        bool hasSSHError = false;
        bool hasVmstatError = false;

        QSharedPointer<QTimer> refreshTimeoutCountdown;
        QSharedPointer<SshController> controller;
        int channelId = -1;

        QGroupBox* group_box;
        QProgressBar* cpu_progress_bar;
        QProgressBar* ram_progress_bar;
        QTextBrowser* text_browser;
        QCheckBox* checkbox;

        QMutex mutex;
    };
}
