/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    armar6_skills::qt_plugins::TeachinGraspTrajectoryGuiPluginWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2019
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <string>

#include <QFileDialog>

#include <Ice/UUID.h>

#include <ArmarXCore/util/CPPUtility/trace.h>

#include <RobotAPI/libraries/GraspingUtility/GraspCandidateHelper.h>

#include <armar6/skills/libraries/Armar6Tools/Armar6HandV2Helper.h>

#include "TeachinGraspTrajectoryGuiPluginWidgetController.h"

#include <VirtualRobot/MathTools.h>

namespace armarx
{
    TeachinGraspTrajectoryGuiPluginWidgetController::TeachinGraspTrajectoryGuiPluginWidgetController() :
        _debugDrawer{Ice::generateUUID()}
    {
        ARMARX_TRACE;
        _ui.setupUi(getWidget());

        QFont font("Monospace");
        font.setStyleHint(QFont::TypeWriter);
        _ui.labelFingerJoints->setFont(font);
        _ui.labelThumbJoints->setFont(font);
        _ui.labelTCPPoseTarget->setFont(font);
        _ui.labelTCPPose->setFont(font);
        _ui.labelCandidatePose->setFont(font);

        _ui.labelTCPPose->setMaximumWidth(300);
        _ui.labelTCPPose->setMinimumWidth(300);
        _ui.labelTCPPoseTarget->setMaximumWidth(300);
        _ui.labelTCPPoseTarget->setMinimumWidth(300);

        _ui.pushButtonStop->setStyleSheet("QPushButton {background-color: red; color: white;}");

        connect(_ui.pushButtonGotoMoveDownUntilForce, SIGNAL(clicked()), this, SLOT(on_pushButtonGotoMoveDownUntilForce_clicked()));
        connect(_ui.pushButtonGotoGraspPose, SIGNAL(clicked()), this, SLOT(on_pushButtonGotoGraspPose_clicked()));
        connect(_ui.pushButtonGotoPrePose, SIGNAL(clicked()), this, SLOT(on_pushButtonGotoPrePose_clicked()));
        connect(_ui.pushButtonGotoInitPosition, SIGNAL(clicked()), this, SLOT(on_pushButtonGotoInitPosition_clicked()));
        connect(_ui.pushButtonRotZDec, SIGNAL(clicked()), this, SLOT(on_pushButtonRotZDec_clicked()));
        connect(_ui.pushButtonRotZInc, SIGNAL(clicked()), this, SLOT(on_pushButtonRotZInc_clicked()));
        connect(_ui.pushButtonPosZDec, SIGNAL(clicked()), this, SLOT(on_pushButtonPosZDec_clicked()));
        connect(_ui.pushButtonPosZInc, SIGNAL(clicked()), this, SLOT(on_pushButtonPosZInc_clicked()));
        connect(_ui.pushButtonRotYDec, SIGNAL(clicked()), this, SLOT(on_pushButtonRotYDec_clicked()));
        connect(_ui.pushButtonPosYDec, SIGNAL(clicked()), this, SLOT(on_pushButtonPosYDec_clicked()));
        connect(_ui.pushButtonRotYInc, SIGNAL(clicked()), this, SLOT(on_pushButtonRotYInc_clicked()));
        connect(_ui.pushButtonPosYInc, SIGNAL(clicked()), this, SLOT(on_pushButtonPosYInc_clicked()));
        connect(_ui.pushButtonRotXDec, SIGNAL(clicked()), this, SLOT(on_pushButtonRotXDec_clicked()));
        connect(_ui.pushButtonRotXInc, SIGNAL(clicked()), this, SLOT(on_pushButtonRotXInc_clicked()));
        connect(_ui.pushButtonPosXDec, SIGNAL(clicked()), this, SLOT(on_pushButtonPosXDec_clicked()));
        connect(_ui.pushButtonPosXInc, SIGNAL(clicked()), this, SLOT(on_pushButtonPosXInc_clicked()));
        connect(_ui.pushButtonGetCandidates, SIGNAL(clicked()), this, SLOT(on_pushButtonGetCandidates_clicked()));
        connect(_ui.comboBoxCandidates, SIGNAL(currentIndexChanged(int)), this, SLOT(on_comboBoxCandidates_currentIndexChanged(int)));
        connect(_ui.comboBoxProviders, SIGNAL(currentTextChanged(const QString&)), this, SLOT(on_comboBoxProviders_currentTextChanged(const QString&)));
        connect(_ui.pushButtonCloseHand, SIGNAL(clicked()), this, SLOT(on_pushButtonCloseHand_clicked()));
        connect(_ui.pushButtonOpenHand, SIGNAL(clicked()), this, SLOT(on_pushButtonOpenHand_clicked()));
        connect(_ui.horizontalSliderThumb, SIGNAL(valueChanged(int)), this, SLOT(on_horizontalSliderThumb_valueChanged(int)));
        connect(_ui.horizontalSliderFingers, SIGNAL(valueChanged(int)), this, SLOT(on_horizontalSliderFingers_valueChanged(int)));
        connect(_ui.pushButtonStop, SIGNAL(clicked()), this, SLOT(on_pushButtonStop_clicked()));
        connect(_ui.pushButtonSetInitPose, SIGNAL(clicked()), this, SLOT(on_pushButtonSetInitPose_clicked()));
        connect(_ui.pushButtonGotoLift, SIGNAL(clicked()), this, SLOT(on_pushButtonGotoLift_clicked()));

        connect(_ui.pushButtonTrajKPAddAfter, SIGNAL(clicked()), this, SLOT(on_pushButtonTrajKPAddAfter_clicked()));
        connect(_ui.pushButtonTrajKPGoto, SIGNAL(clicked()), this, SLOT(on_pushButtonTrajKPGoto_clicked()));
        connect(_ui.pushButtonTrajKPRemove, SIGNAL(clicked()), this, SLOT(on_pushButtonTrajKPRemove_clicked()));
        connect(_ui.pushButtonTrajKPReplace, SIGNAL(clicked()), this, SLOT(on_pushButtonTrajKPReplace_clicked()));
        connect(_ui.pushButtonTrajStartReset, SIGNAL(clicked()), this, SLOT(on_pushButtonTrajStartReset_clicked()));
        connect(_ui.pushButtonTrajExecVisu, SIGNAL(clicked()), this, SLOT(on_pushButtonTrajExecVisu_clicked()));
        connect(_ui.pushButtonTrajExecPreview, SIGNAL(clicked()), this, SLOT(on_pushButtonTrajExecPreview_clicked()));
        connect(_ui.pushButtonTrajExecStop, SIGNAL(clicked()), this, SLOT(on_pushButtonTrajExecStop_clicked()));
        connect(_ui.pushButtonTrajExecRun, SIGNAL(clicked()), this, SLOT(on_pushButtonTrajExecRun_clicked()));
        connect(_ui.pushButtonTrajExecGotoStart, SIGNAL(clicked()), this, SLOT(on_pushButtonTrajExecGotoStart_clicked()));
        connect(_ui.pushButtonTrajTransfToCurrentPose, SIGNAL(clicked()), this, SLOT(on_pushButtonTrajTransfToCurrentPose_clicked()));
        connect(_ui.pushButtonTrajLoad, SIGNAL(clicked()), this, SLOT(on_pushButtonTrajLoad_clicked()));
        connect(_ui.pushButtonTrajSave, SIGNAL(clicked()), this, SLOT(on_pushButtonTrajSave_clicked()));
        connect(_ui.pushButtonTrajApplyDT, SIGNAL(clicked()), this, SLOT(on_pushButtonTrajApplyDT_clicked()));


        _cfg.wpCfg.thresholdOrientationReached = 0.01;
        _cfg.wpCfg.thresholdPositionReached = 1;

        _timer = startTimer(10);
    }
    TeachinGraspTrajectoryGuiPluginWidgetController::~TeachinGraspTrajectoryGuiPluginWidgetController()
    {
        killTimer(_timer);
    }

    QPointer<QDialog> TeachinGraspTrajectoryGuiPluginWidgetController::getConfigDialog(QWidget* parent)
    {
        ARMARX_TRACE;
        std::lock_guard g{_allMutex};
        if (!_dialog)
        {
            _dialog = new SimpleConfigDialog(parent);
            _dialog->addProxyFinder<RobotUnitInterfacePrx>("ru", "Robot Unit", "*");
            _dialog->addProxyFinder<RobotStateComponentInterfacePrx>("rsc", "Robot State Component", "*");
            _dialog->addProxyFinder<HandUnitInterfacePrx>("hu", "Hand unit", "*");
            _dialog->addProxyFinder<grasping::GraspCandidateObserverInterfacePrx>("gco", "Grasp Candidate Observer", "*");

            _dialog->addLineEdit("rns", "Robot node set", "RightArm");
            _dialog->addLineEdit("ft", "Force-Torque sensor", "FT R");
            _dialog->addLineEdit("dbg", "Debug drawer topic", "DebugDrawerUpdates");
        }
        return qobject_cast<SimpleConfigDialog*>(_dialog);
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::configured()
    {
        ARMARX_TRACE;
        std::lock_guard g{_allMutex};
        _robotStateComponentName = _dialog->getProxyName("rsc");
        _robotUnitName = _dialog->getProxyName("ru");
        _handUnitName  = _dialog->getProxyName("hu");
        _graspCandidateObserverName = _dialog->getProxyName("gco");

        _rnsName = _dialog->getLineEditText("rns");
        _ftName = _dialog->getLineEditText("ft");
        _debugDrawerTopicName = _dialog->getLineEditText("dbg");
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::loadSettings(QSettings* settings)
    {
        _robotStateComponentName = settings->value("rsc", "Armar6StateComponent").toString().toStdString();
        _robotUnitName = settings->value("ru", "Armar6Unit").toString().toStdString();
        _handUnitName = settings->value("hu", "RightHandUnit").toString().toStdString();
        _graspCandidateObserverName = settings->value("gco", "GraspCandidateObserver").toString().toStdString();

        _debugDrawerTopicName = settings->value("ddt", "DebugDrawerUpdates").toString().toStdString();

        _rnsName = settings->value("rns", "RightArm").toString().toStdString();
        _ftName = settings->value("ft", "FT R").toString().toStdString();
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::saveSettings(QSettings* settings)
    {
        settings->setValue("rsc", QString::fromStdString(_robotStateComponentName));
        settings->setValue("ru", QString::fromStdString(_robotUnitName));
        settings->setValue("hu", QString::fromStdString(_handUnitName));
        settings->setValue("gco", QString::fromStdString(_graspCandidateObserverName));

        settings->setValue("ddt", QString::fromStdString(_debugDrawerTopicName));

        settings->setValue("rns", QString::fromStdString(_rnsName));
        settings->setValue("ft", QString::fromStdString(_ftName));
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::onInitComponent()
    {
        ARMARX_TRACE;
        usingProxy(_robotStateComponentName);
        usingProxy(_robotUnitName);
        usingProxy(_handUnitName);
        usingProxy(_graspCandidateObserverName);
        offeringTopic(_debugDrawerTopicName);
        ARMARX_INFO << "Proxies"
                    << '\n' << VAROUT(_robotStateComponentName)
                    << '\n' << VAROUT(_robotUnitName)
                    << '\n' << VAROUT(_handUnitName)
                    << '\n' << VAROUT(_graspCandidateObserverName)
                    << "\nOther"
                    << '\n' << VAROUT(_rnsName)
                    << '\n' << VAROUT(_ftName)
                    << '\n' << VAROUT(_debugDrawerTopicName);
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::onConnectComponent()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        //proxies
        {
            ARMARX_TRACE;
            getProxy(_robotStateComponent, _robotStateComponentName);
            getProxy(_robotUnit, _robotUnitName);
            getProxy(_handUnit, _handUnitName);
            getProxy(_graspCandidateObserver, _graspCandidateObserverName);
        }
        //ui bot
        {
            ARMARX_TRACE;
            _uiRobot = RemoteRobot::createLocalCloneFromFile(_robotStateComponent, VirtualRobot::RobotIO::eStructure);
            ARMARX_CHECK_NOT_NULL(_uiRobot);

            ARMARX_CHECK_EXPRESSION(_uiRobot->hasRobotNodeSet(_rnsName))
                    << VAROUT(_rnsName)
                    << '\n' << VAROUT(_uiRobot->getRobotNodeSetNames());
            const auto rns = _uiRobot->getRobotNodeSet(_rnsName);

            _uiRobotTCP = rns->getTCP();
            ARMARX_CHECK_NOT_NULL(_uiRobotTCP)
                    << VAROUT(_rnsName)
                    << '\n' << VAROUT(rns->getNodeNames());

            _tcpName = _uiRobotTCP->getName();
            ARMARX_INFO << "Determined tcp node name" << _tcpName;
        }
        //visu
        {
            ARMARX_TRACE;
            _debugDrawer.setDebugDrawer(getTopic<DebugDrawerInterfacePrx>(_debugDrawerTopicName));
            _debugDrawer.setRobot(_uiRobot);
        }
        //ctrl
        {
            ARMARX_TRACE;
            static const std::string njointControllerClassName = "NJointCartesianWaypointController";

            NJointCartesianWaypointControllerConfigPtr cfg = new NJointCartesianWaypointControllerConfig;
            cfg->rns = _rnsName;
            cfg->ftName = _ftName;

            _controllerName = getName() + "_" + njointControllerClassName;

            ARMARX_IMPORTANT << "Creating " << njointControllerClassName << " '"
                             << _controllerName << "'";

            _controller = NJointCartesianWaypointControllerInterfacePrx::checkedCast(
                              _robotUnit->createNJointController(
                                  njointControllerClassName,
                                  _controllerName,
                                  cfg));
            ARMARX_CHECK_NOT_NULL(_controller);
        }
        on_pushButtonSetInitPose_clicked();
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::onDisconnectComponent()
    {
        ARMARX_TRACE;
        std::lock_guard g{_allMutex};
        if (_graspTrajExec.joinable())
        {
            _graspTrajExecStop = true;
            _graspTrajExec.join();
        }
        if (_controller)
        {
            ARMARX_TRACE;
            ARMARX_IMPORTANT << "deleting old controller '" << _controllerName << "'";
            try
            {
                _controller->deactivateAndDeleteController();
            }
            catch (...) {}
            _controller = nullptr;
        }
        _uiRobotTCP = nullptr;
        _uiRobot    = nullptr;
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_horizontalSliderFingers_valueChanged(int action)
    {
        const std::lock_guard g{_allMutex};
        if (_handUnit)
        {
            ARMARX_TRACE;
            _handUnit->setJointAngles({{"Fingers", 1.f* action / _ui.horizontalSliderFingers->maximum()}});
        }
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_horizontalSliderThumb_valueChanged(int action)
    {
        const std::lock_guard g{_allMutex};
        if (_handUnit)
        {
            ARMARX_TRACE;
            _handUnit->setJointAngles({{"Thumb", 1.f* action / _ui.horizontalSliderThumb->maximum()}});
        }
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonOpenHand_clicked()
    {
        const std::lock_guard g{_allMutex};
        if (_handUnit)
        {
            ARMARX_TRACE;
            _handUnit->setShape("Open");
        }
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonCloseHand_clicked()
    {
        const std::lock_guard g{_allMutex};
        if (_handUnit)
        {
            ARMARX_TRACE;
            _handUnit->setShape("Close");
        }
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonPosXInc_clicked()
    {
        ARMARX_TRACE;
        movTCPInSteps(+1, 0, 0, 0, 0, 0);
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonPosXDec_clicked()
    {
        ARMARX_TRACE;
        movTCPInSteps(-1, 0, 0, 0, 0, 0);
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonPosYInc_clicked()
    {
        ARMARX_TRACE;
        movTCPInSteps(0, +1, 0, 0, 0, 0);
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonPosYDec_clicked()
    {
        ARMARX_TRACE;
        movTCPInSteps(0, -1, 0, 0, 0, 0);
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonPosZInc_clicked()
    {
        ARMARX_TRACE;
        movTCPInSteps(0, 0, +1, 0, 0, 0);
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonPosZDec_clicked()
    {
        ARMARX_TRACE;
        movTCPInSteps(0, 0, -1, 0, 0, 0);
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonRotXInc_clicked()
    {
        ARMARX_TRACE;
        movTCPInSteps(0, 0, 0, +1, 0, 0);
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonRotXDec_clicked()
    {
        ARMARX_TRACE;
        movTCPInSteps(0, 0, 0, -1, 0, 0);
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonRotYInc_clicked()
    {
        ARMARX_TRACE;
        movTCPInSteps(0, 0, 0, 0, +1, 0);
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonRotYDec_clicked()
    {
        ARMARX_TRACE;
        movTCPInSteps(0, 0, 0, 0, -1, 0);
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonRotZInc_clicked()
    {
        ARMARX_TRACE;
        movTCPInSteps(0, 0, 0, 0, 0, +1);
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonRotZDec_clicked()
    {
        ARMARX_TRACE;
        movTCPInSteps(0, 0, 0, 0, 0, -1);
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::movTCPInSteps(int tx, int ty, int tz, int rx, int ry, int rz)
    {
        const std::lock_guard g{_allMutex};
        if (!_uiRobot || !_robotStateComponent || !_controller)
        {
            return;
        }
        ARMARX_TRACE;
        const auto t = static_cast<float>(_ui.doubleSpinBoxCtrlPosDelta->value());
        const auto r = static_cast<float>(_ui.doubleSpinBoxCtrlOriDelta->value() / 180 * M_PI);
        RemoteRobot::synchronizeLocalClone(_uiRobot, _robotStateComponent);
        ARMARX_CHECK_EXPRESSION(_uiRobotTCP);
        const Eigen::Matrix4f mxr = VirtualRobot::MathTools::posrpy2eigen4f(0, 0, 0, rx * r, ry * r, rz * r);
        const Eigen::Matrix4f mxt = VirtualRobot::MathTools::posrpy2eigen4f(tx * t, ty * t, tz * t, 0, 0, 0);

        const Eigen::Matrix4f wpNoTrans =
            _ui.checkBoxMoveGlobalOri->isChecked() ?
            Eigen::Matrix4f{mxr* _uiRobotTCP->getPoseInRootFrame()} :
            Eigen::Matrix4f{_uiRobotTCP->getPoseInRootFrame()* mxr};

        const Eigen::Matrix4f wp =
            _ui.checkBoxMoveGlobalPos->isChecked() ?
            Eigen::Matrix4f{mxt * wpNoTrans} :
            Eigen::Matrix4f{wpNoTrans * mxt};
        _controller->setConfigAndWaypoint(_cfg, wp);
        _controller->activateController();
        _controller->setVisualizationRobotGlobalPose(_uiRobot->getGlobalPose());
        setTargetPoseInGui(wp);
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_comboBoxCandidates_currentIndexChanged(int arg1)
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (!_uiRobot || !_robotStateComponent)
        {
            return;
        }
        RemoteRobot::synchronizeLocalClone(_uiRobot, _robotStateComponent);

        _grasp = _graspCandidates.at(_graspComboBoxToGrasp.at(arg1));
        GraspCandidateHelper gh{_grasp, _uiRobot};
        const Eigen::Matrix4f pose = gh.getGraspPoseInRobotRoot();
        setPoseAsLabelText(pose, _ui.labelCandidatePose);

        _ui.labelCandidateSuccessProbability->setText(QString::number(_grasp->graspSuccessProbability));
        std::stringstream str;
        str << gh.getApproachVector().transpose();
        _ui.labelCandidateApproach->setText(QString::fromStdString(str.str()));

        _debugDrawer.drawPose("targ", pose);
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_comboBoxProviders_currentTextChanged(const QString& arg1)
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        const auto pnameSelected = arg1.toStdString();
        _graspComboBoxToGrasp.clear();
        _ui.comboBoxCandidates->blockSignals(true);
        _ui.comboBoxCandidates->clear();
        int idx = 0;
        for (const auto& c : _graspCandidates)
        {
            const auto& pname = c->providerName;
            if (pname == pnameSelected)
            {
                std::stringstream s;
                _graspComboBoxToGrasp.emplace_back(idx);
                s << _ui.comboBoxCandidates->count() << " - Group " << c->groupNr;
                _ui.comboBoxCandidates->addItem(QString::fromStdString(s.str()));
            }
            ++idx;
        }
        _ui.comboBoxCandidates->setCurrentIndex(0);
        _ui.comboBoxCandidates->blockSignals(false);
        on_comboBoxCandidates_currentIndexChanged(0);
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonGetCandidates_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (!_graspCandidateObserver)
        {
            return;
        }
        _graspCandidates = _graspCandidateObserver->getAllCandidates();
        const auto currentProvider = _ui.comboBoxProviders->currentText().toStdString();
        int idx = 0;
        _ui.comboBoxProviders->blockSignals(true);
        _ui.comboBoxProviders->clear();
        std::set<std::string> provs;
        QString first;
        for (const auto& c : _graspCandidates)
        {
            const auto& pname = c->providerName;
            if (!provs.count(pname))
            {
                if (provs.empty())
                {
                    first = QString::fromStdString(pname);
                }
                provs.emplace(pname);
                if (currentProvider == pname)
                {
                    idx = _ui.comboBoxProviders->count();
                }
                _ui.comboBoxProviders->addItem(QString::fromStdString(pname));
            }
        }
        _ui.comboBoxProviders->setCurrentIndex(idx);
        _ui.comboBoxProviders->blockSignals(false);
        if (!provs.empty())
        {
            on_comboBoxProviders_currentTextChanged(idx ? QString::fromStdString(currentProvider) : first);
        }
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonGotoInitPosition_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (!_uiRobot || !_robotStateComponent || !_controller)
        {
            return;
        }
        RemoteRobot::synchronizeLocalClone(_uiRobot, _robotStateComponent);
        setTargetPoseInGui(_initHandPose);
        _controller->setConfigAndWaypoint(_cfg, _initHandPose);
        _controller->activateController();
        _controller->setVisualizationRobotGlobalPose(_uiRobot->getGlobalPose());
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonGotoPrePose_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (!_uiRobot || !_robotStateComponent || !_controller || !_grasp)
        {
            return;
        }
        RemoteRobot::synchronizeLocalClone(_uiRobot, _robotStateComponent);
        Eigen::Matrix4f pose = GraspCandidateHelper{_grasp, _uiRobot}.getGraspPoseInRobotRoot();
        pose(2, 3) += _ui.doubleSpinBoxPrePoseZOffset->value();
        setTargetPoseInGui(pose);
        NJointCartesianWaypointControllerRuntimeConfig cfg;
        _controller->setConfigAndWaypoint(cfg, pose);
        _controller->activateController();
        _controller->setVisualizationRobotGlobalPose(_uiRobot->getGlobalPose());
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonGotoGraspPose_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (!_uiRobot || !_robotStateComponent || !_controller || !_grasp)
        {
            return;
        }
        RemoteRobot::synchronizeLocalClone(_uiRobot, _robotStateComponent);
        const Eigen::Matrix4f pose = GraspCandidateHelper{_grasp, _uiRobot}.getGraspPoseInRobotRoot();
        setTargetPoseInGui(pose);
        _controller->setConfigAndWaypoint(_cfg, pose);
        _controller->activateController();
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonGotoMoveDownUntilForce_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (!_uiRobot || !_robotStateComponent || !_controller)
        {
            return;
        }
        on_pushButtonStop_clicked();
        RemoteRobot::synchronizeLocalClone(_uiRobot, _robotStateComponent);
        NJointCartesianWaypointControllerRuntimeConfig cfg = _cfg;
        cfg.forceThresholdInRobotRootZ = true;
        cfg.forceThreshold = _ui.doubleSpinBoxForceThresh->value();

        Eigen::Matrix4f pose = _uiRobotTCP->getPoseInRootFrame();
        pose(2, 3) -= _ui.doubleSpinBoxGotoMoveDownZOffset->value();
        setTargetPoseInGui(pose);
        _controller->setConfigAndWaypoint(cfg, pose);
        _controller->setVisualizationRobotGlobalPose(_uiRobot->getGlobalPose());
        std::this_thread::sleep_for(std::chrono::milliseconds{500});
        _controller->setCurrentFTAsOffset();
        _controller->activateController();
    }
    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonGotoLift_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (!_uiRobot || !_robotStateComponent || !_controller || !_grasp)
        {
            return;
        }
        RemoteRobot::synchronizeLocalClone(_uiRobot, _robotStateComponent);
        Eigen::Matrix4f pose = _uiRobotTCP->getPoseInRootFrame();
        pose(2, 3) += _ui.doubleSpinBoxGotoLiftZOffset->value();
        setTargetPoseInGui(pose);
        _controller->setConfigAndWaypoint(_cfg, pose);
        _controller->activateController();
        _controller->setVisualizationRobotGlobalPose(_uiRobot->getGlobalPose());
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::timerEvent(QTimerEvent*)
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        //joints
        if (_handUnit)
        {
            ARMARX_TRACE;
            const auto j = _handUnit->getCurrentJointValues();
            if (!j.count("Fingers") || !j.count("Thumb"))
            {
                ARMARX_WARNING << deactivateSpam()
                               << "Some joints are missing. Available Joints: " << j;
            }
            else
            {
                ARMARX_INFO << deactivateSpam()
                            << "Updated joint values to " << j;
            }
            if (j.count("Fingers"))
            {
                const float v = j.at("Fingers");
                _ui.labelFingerJoints->setText(QString::number(v, 'g', 3));
                _handJointsValuesFTD(0) = v;
                _ui.horizontalSliderFingers->blockSignals(true);
                _ui.horizontalSliderFingers->setValue(v * _ui.horizontalSliderFingers->maximum());
                _ui.horizontalSliderFingers->blockSignals(false);
            }
            if (j.count("Thumb"))
            {
                const float v = j.at("Thumb");
                _ui.labelThumbJoints->setText(QString::number(v, 'g', 3));
                _handJointsValuesFTD(1) = v;
                _ui.horizontalSliderThumb->blockSignals(true);
                _ui.horizontalSliderThumb->setValue(v * _ui.horizontalSliderThumb->maximum());
                _ui.horizontalSliderThumb->blockSignals(false);
            }
        }
        //pose
        if (_uiRobot && _robotStateComponent)
        {
            ARMARX_TRACE;
            ARMARX_CHECK_NOT_NULL(_uiRobotTCP);
            RemoteRobot::synchronizeLocalClone(_uiRobot, _robotStateComponent);
            setPoseAsLabelText(_uiRobotTCP->getPoseInRootFrame(), _ui.labelTCPPose);
        }
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonStop_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (!_controller)
        {
            return;
        }
        try
        {
            ARMARX_TRACE;
            _controller->stopMovement();
            _controller->deactivateController();
        }
        catch (...) {}
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonSetInitPose_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (!_uiRobot || !_robotStateComponent)
        {
            return;
        }
        ARMARX_TRACE;
        ARMARX_CHECK_NOT_NULL(_uiRobotTCP);
        RemoteRobot::synchronizeLocalClone(_uiRobot, _robotStateComponent);
        _initHandPose = _uiRobotTCP->getPoseInRootFrame();
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonTrajStartReset_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (!_uiRobot || !_robotStateComponent)
        {
            return;
        }
        RemoteRobot::synchronizeLocalClone(_uiRobot, _robotStateComponent);
        _graspTrajectory = std::make_shared<Armar6GraspTrajectory>(_uiRobotTCP->getPoseInRootFrame(), _handJointsValuesFTD);
        updateGuiWPCount();
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonTrajKPReplace_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (!_uiRobot || !_robotStateComponent || !_graspTrajectory || !_graspTrajectory->getKeypointCount())
        {
            return;
        }
        RemoteRobot::synchronizeLocalClone(_uiRobot, _robotStateComponent);
        const int sel = _ui.spinBoxTrajKPIdx->value() - 1;
        const float dt = _graspTrajectory->getKeypoint(sel)->dt;
        _graspTrajectory->replaceKeypoint(sel, _uiRobotTCP->getPoseInRootFrame(), _handJointsValuesFTD, dt);
        updateGuiWPCount();
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonTrajKPRemove_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (!_uiRobot || !_robotStateComponent || !_graspTrajectory || !_graspTrajectory->getKeypointCount())
        {
            return;
        }
        RemoteRobot::synchronizeLocalClone(_uiRobot, _robotStateComponent);
        const int sel = _ui.spinBoxTrajKPIdx->value() - 1;
        _graspTrajectory->removeKeypoint(sel);
        updateGuiWPCount();
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonTrajKPGoto_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (!_uiRobot || !_robotStateComponent || !_graspTrajectory || _graspTrajectory->getKeypointCount() <= 1)
        {
            return;
        }
        const int sel = _ui.spinBoxTrajKPIdx->value() - 1;
        Armar6GraspTrajectory::KeypointPtr keypoint = _graspTrajectory->getKeypoint(sel);
        shapeHand(keypoint->handJointsTarget);
        setTargetPoseInGui(keypoint->getTargetPose());
        _controller->setConfigAndWaypoint(_cfg, keypoint->getTargetPose());
        _controller->activateController();
        _controller->setVisualizationRobotGlobalPose(_uiRobot->getGlobalPose());
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonTrajKPAddAfter_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (!_uiRobot || !_robotStateComponent || !_graspTrajectory)
        {
            return;
        }
        RemoteRobot::synchronizeLocalClone(_uiRobot, _robotStateComponent);
        _graspTrajectory->addKeypoint(_uiRobotTCP->getPoseInRootFrame(), _handJointsValuesFTD, dt());
        _ui.labelTrajWPMax->setText(QString::number(_graspTrajectory->getKeypointCount()));
        _ui.spinBoxTrajKPIdx->setRange(1, _graspTrajectory->getKeypointCount());
        _ui.spinBoxTrajKPIdx->setValue(1 + _ui.spinBoxTrajKPIdx->value());
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonTrajSave_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (!_graspTrajectory)
        {
            return;
        }

        const auto fileName = QFileDialog::getSaveFileName(
                                  getWidget(), tr("Save trajectory"),
                                  _lastFileSaveLoad, tr("XML files (*.xml);;All files (*)"));
        if (fileName.isEmpty())
        {
            return;
        }
        _lastFileSaveLoad = fileName;
        ARMARX_INFO << "saving to " << fileName.toStdString();
        _graspTrajectory->writeToFile(fileName.toStdString());
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonTrajLoad_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};

        const auto fileName = QFileDialog::getOpenFileName(
                                  getWidget(), tr("Open trajectory"),
                                  _lastFileSaveLoad, tr("XML files (*.xml);;All files (*)"));

        if (fileName.isEmpty())
        {
            return;
        }
        _lastFileSaveLoad = fileName;
        ARMARX_INFO << "loading from " << fileName.toStdString();
        _graspTrajectory = Armar6GraspTrajectory::ReadFromFile(fileName.toStdString());
        updateGuiWPCount();
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonTrajTransfToCurrentPose_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (!_uiRobot || !_robotStateComponent || !_graspTrajectory || !_graspTrajectory->getKeypointCount())
        {
            return;
        }
        _graspTrajectory = _graspTrajectory->getTransformedToGraspPose(
                               _uiRobotTCP->getPoseInRootFrame(),
                               Armar6HandV2Helper::GetHandForwardVector());
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonTrajExecGotoStart_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (!_uiRobot || !_robotStateComponent || !_graspTrajectory || !_graspTrajectory->getKeypointCount())
        {
            return;
        }
        Armar6GraspTrajectory::KeypointPtr keypoint = _graspTrajectory->getKeypoint(0);
        shapeHand(keypoint->handJointsTarget);
        setTargetPoseInGui(keypoint->getTargetPose());
        _controller->setConfigAndWaypoint(_cfg, keypoint->getTargetPose());
        _controller->activateController();
        _controller->setVisualizationRobotGlobalPose(_uiRobot->getGlobalPose());
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonTrajExecRun_clicked()
    {
        ARMARX_TRACE;
        std::lock_guard g{_allMutex};
        on_pushButtonTrajExecStop_clicked();
        if (!_graspTrajectory || !_controller || !_uiRobot || !_robotStateComponent || !_handUnit)
        {
            return;
        }
        RemoteRobot::synchronizeLocalClone(_uiRobot, _robotStateComponent);
        _controller->setVisualizationRobotGlobalPose(_uiRobot->getGlobalPose());

        _graspTrajExecStop = false;
        _graspTrajExec = std::thread
        {
            [traj = _graspTrajectory->getClone(), ctrl = _controller,  hu = _handUnit, this]{ // this for atomic

                const float endtime = traj->getDuration();
                ctrl->stopMovement();
                ctrl->activateController();

                const auto time = [start = std::chrono::high_resolution_clock::now()]
                {
                    return static_cast<float>(std::chrono::duration_cast<std::chrono::nanoseconds>(
                                                  std::chrono::high_resolution_clock::now() - start).count()) / 1e9f;
                };

                while (!_graspTrajExecStop)
                {
                    const float curtime = time();
                    const Eigen::Vector3f ftd = traj->GetHandValues(curtime);
                    hu->setJointAngles({{"Fingers", ftd(0)}, {"Thumb", ftd(1)}});
                    ctrl->setConfigAndWaypoint(_cfg, traj->GetPose(curtime));
                    if (curtime > endtime)
                    {
                        break;
                    }
                    std::this_thread::sleep_for(std::chrono::milliseconds{10});
                }
                ctrl->stopMovement();
                ctrl->deactivateController();
            }};
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonTrajExecStop_clicked()
    {
        on_pushButtonStop_clicked();
        if (_graspTrajExec.joinable())
        {
            _graspTrajExecStop = true;
            _graspTrajExec.join();
        }
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonTrajExecPreview_clicked()
    {
        ///TODO
        //    float t = gui.sldTrajectory->getValue() / 100.f * graspTrajectory->getDuration();
        //    currentTarget = graspTrajectory->GetPose(t);
        //    posHelper.setTarget(currentTarget);
        //    fingersTarget = graspTrajectory->GetHandValues(t);
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonTrajExecVisu_clicked()
    {
        ///TODO
        //    {
        //        float t = gui.sldTrajectory->getValue() / 100.f * graspTrajectory->getDuration();
        //        Eigen::Matrix4f target = graspTrajectory->GetPose(t);
        //        Eigen::Matrix4f tcp2handRoot = rnh->getRobotArm(RobotNameHelper::LocationRight, robot).getTCP()->getPoseInRootFrame().inverse() * robot->getRobotNode("Hand R Root")->getPoseInRootFrame();

        //        debugDrawerHelper.drawRobot("preview", "armar6_rt/robotmodel/Armar6-SH/Armar6-RightHand-v3.xml", "armar6_rt", target * tcp2handRoot, DrawColor {0, 1, 0, 1});
        //        debugDrawerHelper.setRobotConfig("preview", handHelper.getSimulationJointValues(graspTrajectory->GetHandValues(t)));
        //    }
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::on_pushButtonTrajApplyDT_clicked()
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (_graspTrajectory && _graspTrajectory->getKeypointCount())
        {
            const int sel = _ui.spinBoxTrajKPIdx->value() - 1;
            _graspTrajectory->setKeypointDt(sel, dt());
        }
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::setPoseAsLabelText(const Eigen::Matrix4f& mx, QLabel* l)
    {
        std::stringstream s;
        s << mx;
        l->setText(QString::fromStdString(s.str()).replace("\n", "<br/>"));
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::setTargetPoseInGui(const Eigen::Matrix4f& mx)
    {
        ARMARX_INFO << "New target pose\n" << mx;
        setPoseAsLabelText(mx, _ui.labelTCPPoseTarget);
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::updateGuiWPCount()
    {
        const auto cnt = _graspTrajectory ? _graspTrajectory->getKeypointCount() : 0;
        _ui.labelTrajWPMax->setText(QString::number(cnt));
        if (cnt)
        {
            _ui.spinBoxTrajKPIdx->setRange(1, cnt);
        }
        else
        {
            _ui.spinBoxTrajKPIdx->setRange(0, 0);
        }
    }

    float TeachinGraspTrajectoryGuiPluginWidgetController::dt()
    {
        return _ui.doubleSpinBoxTrajDt->value();
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::shapeHand(float fingers, float thumb)
    {
        ARMARX_TRACE;
        const std::lock_guard g{_allMutex};
        if (_handUnit)
        {
            ARMARX_INFO << deactivateSpam(1) << "Targets: Fingers = " << fingers
                        << ", Thumb = " << thumb;
            _handUnit->setJointAngles({{"Fingers", fingers}, {"Thumb", thumb}});
        }
    }

    void TeachinGraspTrajectoryGuiPluginWidgetController::shapeHand(const Eigen::Vector3f& v)
    {
        shapeHand(v(0), v(1));
    }
}
