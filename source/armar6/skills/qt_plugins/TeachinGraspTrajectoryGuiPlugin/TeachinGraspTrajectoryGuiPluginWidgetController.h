/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::qt_plugins::TeachinGraspTrajectoryGuiPluginWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <RobotAPI/interface/units/HandUnitInterface.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>
#include <RobotAPI/interface/observers/GraspCandidateObserverInterface.h>
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointCartesianWaypointController.h>

#include <armar6/skills/qt_plugins/TeachinGraspTrajectoryGuiPlugin/ui_TeachinGraspTrajectoryGuiPluginWidget.h>
#include <armar6/skills/libraries/Armar6Tools/Armar6GraspTrajectory.h>

namespace armarx
{
    /**
    \page armar6_skills-GuiPlugins-TeachinGraspTrajectoryGuiPlugin TeachinGraspTrajectoryGuiPlugin
    \brief The TeachinGraspTrajectoryGuiPlugin allows visualizing ...

    \image html TeachinGraspTrajectoryGuiPlugin.png
    The user can

    API Documentation \ref TeachinGraspTrajectoryGuiPluginWidgetController

    \see TeachinGraspTrajectoryGuiPluginGuiPlugin
    */

    /**
     * \class TeachinGraspTrajectoryGuiPluginWidgetController
     * \brief TeachinGraspTrajectoryGuiPluginWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        TeachinGraspTrajectoryGuiPluginWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < TeachinGraspTrajectoryGuiPluginWidgetController >
    {
        Q_OBJECT

    public:
        /// Controller Constructor
        explicit TeachinGraspTrajectoryGuiPluginWidgetController();

        /// Controller destructor
        virtual ~TeachinGraspTrajectoryGuiPluginWidgetController();

        /// @see ArmarXWidgetController::loadSettings()
        void loadSettings(QSettings* settings) override;

        ///@see ArmarXWidgetController::saveSettings()
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "RobotControl.TeachinGraspTrajectoryGuiPlugin";
        }

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;

        QPointer<QDialog> getConfigDialog(QWidget* parent) override;
        void configured() override;

    protected:
        void timerEvent(QTimerEvent*) override;

    private slots:
        void on_pushButtonGotoMoveDownUntilForce_clicked();
        void on_pushButtonGotoGraspPose_clicked();
        void on_pushButtonGotoPrePose_clicked();
        void on_pushButtonGotoInitPosition_clicked();
        void on_pushButtonRotZDec_clicked();
        void on_pushButtonRotZInc_clicked();
        void on_pushButtonPosZDec_clicked();
        void on_pushButtonPosZInc_clicked();
        void on_pushButtonRotYDec_clicked();
        void on_pushButtonPosYDec_clicked();
        void on_pushButtonRotYInc_clicked();
        void on_pushButtonPosYInc_clicked();
        void on_pushButtonRotXDec_clicked();
        void on_pushButtonRotXInc_clicked();
        void on_pushButtonPosXDec_clicked();
        void on_pushButtonPosXInc_clicked();
        void on_pushButtonGetCandidates_clicked();
        void on_comboBoxCandidates_currentIndexChanged(int);
        void on_comboBoxProviders_currentTextChanged(const QString& arg1);
        void on_pushButtonCloseHand_clicked();
        void on_pushButtonOpenHand_clicked();
        void on_horizontalSliderThumb_valueChanged(int action);
        void on_horizontalSliderFingers_valueChanged(int action);

        void movTCPInSteps(int tx, int ty, int tz, int rx, int ry, int rz);

        void on_pushButtonStop_clicked();
        void on_pushButtonSetInitPose_clicked();
        void on_pushButtonGotoLift_clicked();

        void on_pushButtonTrajKPAddAfter_clicked();
        void on_pushButtonTrajKPGoto_clicked();
        void on_pushButtonTrajKPRemove_clicked();
        void on_pushButtonTrajKPReplace_clicked();
        void on_pushButtonTrajStartReset_clicked();
        void on_pushButtonTrajExecVisu_clicked();
        void on_pushButtonTrajExecPreview_clicked();
        void on_pushButtonTrajExecStop_clicked();
        void on_pushButtonTrajExecRun_clicked();
        void on_pushButtonTrajExecGotoStart_clicked();
        void on_pushButtonTrajTransfToCurrentPose_clicked();
        void on_pushButtonTrajLoad_clicked();
        void on_pushButtonTrajSave_clicked();
        void on_pushButtonTrajApplyDT_clicked();

        //functions
    private:
        void setPoseAsLabelText(const Eigen::Matrix4f& mx, QLabel* l);
        void setTargetPoseInGui(const Eigen::Matrix4f& mx);

        void updateGuiWPCount();

        float dt();

        void shapeHand(float fingers, float thumb);
        void shapeHand(const Eigen::Vector3f& v);

        //data
    private:
        Ui::TeachinGraspTrajectoryGuiPluginWidget       _ui;
        QPointer<SimpleConfigDialog>                    _dialog;

        //names proxies
        std::string                                     _robotStateComponentName;
        std::string                                     _robotUnitName;
        std::string                                     _handUnitName;
        std::string                                     _graspCandidateObserverName;
        //names other
        std::string                                     _debugDrawerTopicName;
        std::string                                     _ftName;
        std::string                                     _rnsName;
        //names derived
        std::string                                     _tcpName;
        std::string                                     _controllerName;

        //guard
        mutable std::recursive_mutex                    _allMutex;

        //visu
        DebugDrawerHelper                               _debugDrawer;

        //ctrl + robot
        VirtualRobot::RobotPtr                          _uiRobot;
        VirtualRobot::RobotNodePtr                      _uiRobotTCP;
        RobotStateComponentInterfacePrx                 _robotStateComponent;
        RobotUnitInterfacePrx                           _robotUnit;
        NJointCartesianWaypointControllerInterfacePrx   _controller;
        HandUnitInterfacePrx                            _handUnit;
        Eigen::Vector3f                                 _handJointsValuesFTD{0, 0, 0};
        NJointCartesianWaypointControllerRuntimeConfig  _cfg;

        //grasps
        grasping::GraspCandidateObserverInterfacePrx    _graspCandidateObserver;
        grasping::GraspCandidateSeq                     _graspCandidates;
        std::vector<int>                                _graspComboBoxToGrasp;
        grasping::GraspCandidatePtr                     _grasp;


        //traj
        Armar6GraspTrajectoryPtr                        _graspTrajectory;
        std::atomic_bool                                _graspTrajExecStop{true};
        std::thread                                     _graspTrajExec;

        //other
        int                                             _timer;
        Eigen::Matrix4f                                 _initHandPose;
        QString                                         _lastFileSaveLoad{"~/"};
    };
}
