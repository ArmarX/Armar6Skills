armarx_add_qt_plugin(ui_teachin_grasp_trajectory_plugin
    MANUAL_QT_PLUGIN_SOURCES # TODO: Auto-gen Qt plugin.
    SOURCES
        TeachinGraspTrajectoryGuiPluginGuiPlugin.cpp
        TeachinGraspTrajectoryGuiPluginWidgetController.cpp
    HEADERS
        TeachinGraspTrajectoryGuiPluginGuiPlugin.h
        TeachinGraspTrajectoryGuiPluginWidgetController.h
    UI_FILES
        TeachinGraspTrajectoryGuiPluginWidget.ui
    DEPENDENCIES
        SimpleConfigDialog
        RobotUnit
        DebugDrawer
        Armar6Tools
)
