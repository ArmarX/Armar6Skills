/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::KITImageFilmGroup
 * @author     SecondHands Demo ( shdemo at armar6 )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "KITImageFilmDMP.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <armarx/control/deprecated_njoint_mp_controller/task_space/ControllerInterface.h>
#include "../Armar6DMPControlGroup/Helpers.h"
#include <armar6/skills/statecharts/KITImageFilmGroup/KITImageFilmGroupStatechartContext.generated.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>

#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>


using namespace armarx;
using namespace KITImageFilmGroup;

// DO NOT EDIT NEXT LINE
KITImageFilmDMP::SubClassRegistry KITImageFilmDMP::Registry(KITImageFilmDMP::GetName(), &KITImageFilmDMP::CreateInstance);



void KITImageFilmDMP::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void KITImageFilmDMP::run()
{
    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");

    KinematicUnitHelper kinUnitHelper(getKinematicUnit());
    kinUnitHelper.setJointAngles(in.getHeadJoints());


    NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController("CompliantTaskSpaceDMP");
    if (controller)
    {
        if (controller->isControllerActive())
        {
            controller->deactivateController();
            while (controller->isControllerActive())
            {
                TimeUtil::SleepMS(10);
            }
        }
        controller->deleteController();
        TimeUtil::SleepMS(10);
    }


    VirtualRobot::RobotPtr localRobot = getLocalRobot();
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
    double phaseL = 1000;
    double phaseK = 1000;
    double phaseDist0 = 10000;
    double phaseDist1 = 10000;
    double posToOriRatio = 10;


    std::vector<float> defaultJointValues;

    if (in.isDefaultJointValuesSet())
    {
        defaultJointValues = in.getDefaultJointValues();
    }
    else
    {
        defaultJointValues = localRobot->getRobotNodeSet(in.getKinematicChainName())->getJointValues();
    }

    bool useNullSpaceJointDMP;

    if (in.isNullSpaceJointDMPFileSet())
    {
        useNullSpaceJointDMP = true;
    }
    else
    {
        useNullSpaceJointDMP = false;
    }

    float torqueLimit = in.getTorqueLimit();

    NJointTaskSpaceImpedanceDMPControllerConfigPtr tsConfig = new NJointTaskSpaceImpedanceDMPControllerConfig(
        in.getDMPKernelSize(),
        in.getBaseMode(),
        in.getDMPStyle(),
        in.getTimeDuration(),
        in.getKinematicChainName(),
        phaseL,
        phaseK,
        phaseDist0,
        phaseDist1,
        posToOriRatio,
        in.getKposVec(),
        in.getDposVec(),
        in.getKoriVec(),
        in.getDoriVec(),
        in.getKnull(),
        in.getDnull(),
        useNullSpaceJointDMP,
        defaultJointValues,
        torqueLimit
    );

    std::string controllerName = "CompliantTaskSpaceDMP";
    if (in.isControllerNameSet())
    {
        controllerName = in.getControllerName();
    }

    NJointTaskSpaceImpedanceDMPControllerInterfacePrx dmpController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", controllerName, tsConfig));

    if (useNullSpaceJointDMP)
    {
        dmpController->learnJointDMPFromFiles(in.getNullSpaceJointDMPFile(), localRobot->getRobotNodeSet(in.getKinematicChainName())->getJointValues());
    }

    std::vector<std::string> fileNames = in.getArmMotionFile();
    ARMARX_IMPORTANT << VAROUT(in.getArmMotionFile());
    dmpController->learnDMPFromFiles(fileNames);
    std::vector<double> goals = Helpers::pose2dvec(in.getTcpGoalPose()->toEigen());

    if (in.isViaPoseListSet() && in.isViaPoseCanValSet() && in.getViaPoseList().size() == in.getViaPoseCanVal().size())
    {
        for (size_t i = 0; i < in.getViaPoseList().size(); ++i)
        {
            dmpController->setViaPoints(in.getViaPoseCanVal().at(i), Helpers::pose2dvec(in.getViaPoseList().at(i)->toEigen()));
        }
    }

    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx rightHandcontroller = StateBase::getContext<KITImageFilmGroupStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_R_EEF_NJointKITHandV2ShapeController");
    rightHandcontroller->activateController();


    dmpController->activateController();
    dmpController->runDMP(goals);

    //    bool handclosed = false;
    //    bool handopened = true;
    //    ARMARX_INFO << VAROUT(in.getIsCloseHand()) << VAROUT(in.getIsOpenHand());
    while (!isRunningTaskStopped() && dmpController->getVirtualTime() > in.getStopDMPCanValue() * in.getTimeDuration())
    {
        usleep(10000);
    }

    dmpController->stopDMP();

    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR8_Wri2"));
    Eigen::Vector3f initialForceRight;
    initialForceRight.setZero();
    Eigen::Vector3f forceRight;

    IceUtil::Time start = TimeUtil::GetTime();

    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

        if ((TimeUtil::GetTime() - start).toSecondsDouble() < in.getWaitBeforeOpenHand())
        {
            FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
            initialForceRight = forcePtr->toRootEigen(localRobot);
        }
        else
        {
            FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
            forceRight = forcePtr->toRootEigen(localRobot);
            forceRight -= initialForceRight;

            float forceMag = forceRight.norm();

            if (forceMag > in.getOpenHandForceThreshold())
            {
                break;
            }
        }


        usleep(10000);
    }

    rightHandcontroller->setTargets(0, 0);
    usleep(10000);
    if (in.getIsHandRetract())
    {
        dmpController->resumeDMP();
        while (!isRunningTaskStopped() && !dmpController->isFinished())
        {
            usleep(10000);
        }
    }

    dmpController->deactivateController();
    while (dmpController->isControllerActive())
    {
        usleep(10000);
    }
    dmpController->deleteController();

    emitSuccess();
}

//void KITImageFilmDMP::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void KITImageFilmDMP::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr KITImageFilmDMP::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new KITImageFilmDMP(stateData));
}
