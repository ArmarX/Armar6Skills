/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::KITImageFilmGroup
 * @author     SecondHands Demo ( shdemo at armar6 )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MovePlatformToHandOver.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/core/time/CycleUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <DynamicObstacleAvoidance/Modulation.hpp>
#include <DynamicObstacleAvoidance/Obstacle/Ellipsoid.hpp>
#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>
#include <RobotAPI/libraries/core/PIDController.h>
using namespace armarx;
using namespace KITImageFilmGroup;

// DO NOT EDIT NEXT LINE
MovePlatformToHandOver::SubClassRegistry MovePlatformToHandOver::Registry(MovePlatformToHandOver::GetName(), &MovePlatformToHandOver::CreateInstance);


float signedMin(float newValue, float minAbsValue)
{
    return std::copysign(std::min<float>(fabs(newValue), minAbsValue), newValue);
}


void MovePlatformToHandOver::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void MovePlatformToHandOver::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly armarxwhether isRunningTaskStopped() returns true

    // get a private kinematic instance for this state of the robot (tick "Robot State Component" proxy checkbox in statechart group)
    VirtualRobot::RobotPtr robot = getLocalRobot();

    KinematicUnitHelper kinUnitHelper(getKinematicUnit());
    kinUnitHelper.setJointAngles(in.getHeadJoints());

    // PID controller
    PIDController pidRot(in.getRotationPID()->x, in.getRotationPID()->y, in.getRotationPID()->z);
    DatafieldRefPtr platformRotAng = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformPose", "rotation"));

    //    DebugDrawerHelper dd(getDebugDrawerTopic(), "DynamicObstacleAvoidance", robot);
    float obstacleRadius = 0.5;
    double safetyMargin = in.getSafetyMargin();
    auto dd = getDebugDrawerTopic();
    getDebugDrawerTopic()->clearAll();

    usleep(100000);
    ARMARX_INFO << "started program ... ";
    // define obstacles

    // generate the list of obstacles
    // right side of frame
    // control console
    // left side of frame
    // guard stand 1, 3.75, 0
    // table
    // wall

    //    Eigen::Vector3d position_o1(3.0, 2.5, 0);
    Eigen::Vector3d position_o1(3.17, 2.32, 0);
    Eigen::Vector3d position_o2(4.4, 2, 0);
    Eigen::Vector3d position_o3(0.3, 3, 0);
    Eigen::Vector3d position_o4(1, 4, 0);
    Eigen::Vector3d position_o5(3.7, 4.7, 0);
    Eigen::Vector3d position_o6(4.6, 4.7, 0);
    Eigen::Vector3d position_o7(2, 5.7, 0);


    Eigen::Quaterniond orientation_o1(Eigen::AngleAxisd(0, Eigen::Vector3d::UnitZ()));
    Eigen::Quaterniond orientation_o2(Eigen::AngleAxisd(0, Eigen::Vector3d::UnitZ()));
    Eigen::Quaterniond orientation_o3(Eigen::AngleAxisd(0, Eigen::Vector3d::UnitZ()));
    Eigen::Quaterniond orientation_o4(Eigen::AngleAxisd(0.38, Eigen::Vector3d::UnitZ()));
    Eigen::Quaterniond orientation_o5(Eigen::AngleAxisd(0.75, Eigen::Vector3d::UnitZ()));
    Eigen::Quaterniond orientation_o6(Eigen::AngleAxisd(-0.75, Eigen::Vector3d::UnitZ()));
    Eigen::Quaterniond orientation_o7(Eigen::AngleAxisd(0, Eigen::Vector3d::UnitZ()));

    std::unique_ptr<Ellipsoid> ptrE1(std::unique_ptr<Ellipsoid>(new Ellipsoid(::State(position_o1, orientation_o1), safetyMargin)));
    std::unique_ptr<Ellipsoid> ptrE2(std::unique_ptr<Ellipsoid>(new Ellipsoid(::State(position_o2, orientation_o2), safetyMargin)));
    std::unique_ptr<Ellipsoid> ptrE3(std::unique_ptr<Ellipsoid>(new Ellipsoid(::State(position_o3, orientation_o3), safetyMargin)));
    std::unique_ptr<Ellipsoid> ptrE4(std::unique_ptr<Ellipsoid>(new Ellipsoid(::State(position_o4, orientation_o4), safetyMargin)));
    std::unique_ptr<Ellipsoid> ptrE5(std::unique_ptr<Ellipsoid>(new Ellipsoid(::State(position_o5, orientation_o5), safetyMargin)));
    std::unique_ptr<Ellipsoid> ptrE6(std::unique_ptr<Ellipsoid>(new Ellipsoid(::State(position_o6, orientation_o6), safetyMargin)));
    std::unique_ptr<Ellipsoid> ptrE7(std::unique_ptr<Ellipsoid>(new Ellipsoid(::State(position_o7, orientation_o7), safetyMargin)));


    ptrE1->set_axis_lengths(Eigen::Array3d(0.15, 0.75, 0));
    ptrE2->set_axis_lengths(Eigen::Array3d(1.5, 0.15, 0));
    ptrE3->set_axis_lengths(Eigen::Array3d(0.2, 2, 0));
    ptrE4->set_axis_lengths(Eigen::Array3d(0.7, 0.2, 0));
    ptrE5->set_axis_lengths(Eigen::Array3d(0.55, 0.3, 0));
    ptrE6->set_axis_lengths(Eigen::Array3d(0.55, 0.3, 0));
    ptrE7->set_axis_lengths(Eigen::Array3d(2, 0.2, 0));

    // set reference points for pairs of ellipses
    ptrE1->set_reference_position(Eigen::Vector3d(3, 2.05, 0));
    ptrE2->set_reference_position(Eigen::Vector3d(3, 2.05, 0));

    ptrE3->set_reference_position(Eigen::Vector3d(0.4, 3.55, 0));
    ptrE4->set_reference_position(Eigen::Vector3d(0.4, 3.55, 0));

    ptrE5->set_reference_position(Eigen::Vector3d(4.15, 5.1, 0));
    ptrE6->set_reference_position(Eigen::Vector3d(4.15, 5.1, 0));

    dd->setBoxVisu("DynamicObstacleAvoidance", "o1", new Pose(ptrE1->get_orientation().toRotationMatrix().cast<float>(), position_o1.cast<float>() * 1000), new Vector3(ptrE1->get_axis_lengths()(0) * 1000 * 2, ptrE1->get_axis_lengths()(1) * 1000 * 2, 1), DrawColor {1, 0, 0, 0.5});
    dd->setBoxVisu("DynamicObstacleAvoidance", "o2", new Pose(ptrE2->get_orientation().toRotationMatrix().cast<float>(), position_o2.cast<float>() * 1000), new Vector3(ptrE2->get_axis_lengths()(0) * 1000 * 2, ptrE2->get_axis_lengths()(1) * 1000 * 2, 1), DrawColor {1, 0, 0, 0.5});
    dd->setBoxVisu("DynamicObstacleAvoidance", "o3", new Pose(ptrE3->get_orientation().toRotationMatrix().cast<float>(), position_o3.cast<float>() * 1000), new Vector3(ptrE3->get_axis_lengths()(0) * 1000 * 2, ptrE3->get_axis_lengths()(1) * 1000 * 2, 1), DrawColor {1, 0, 0, 0.5});
    dd->setBoxVisu("DynamicObstacleAvoidance", "o4", new Pose(ptrE4->get_orientation().toRotationMatrix().cast<float>(), position_o4.cast<float>() * 1000), new Vector3(ptrE4->get_axis_lengths()(0) * 1000 * 2, ptrE4->get_axis_lengths()(1) * 1000 * 2, 1), DrawColor {1, 0, 0, 0.5});
    dd->setBoxVisu("DynamicObstacleAvoidance", "o5", new Pose(ptrE5->get_orientation().toRotationMatrix().cast<float>(), position_o5.cast<float>() * 1000), new Vector3(ptrE5->get_axis_lengths()(0) * 1000 * 2, ptrE5->get_axis_lengths()(1) * 1000 * 2, 1), DrawColor {1, 0, 0, 0.5});
    dd->setBoxVisu("DynamicObstacleAvoidance", "o6", new Pose(ptrE6->get_orientation().toRotationMatrix().cast<float>(), position_o6.cast<float>() * 1000), new Vector3(ptrE6->get_axis_lengths()(0) * 1000 * 2, ptrE6->get_axis_lengths()(1) * 1000 * 2, 1), DrawColor {1, 0, 0, 0.5});
    //    dd->setBoxVisu("DynamicObstacleAvoidance", "o7", new Pose(ptrE7->get_orientation().toRotationMatrix().cast<float>(), position_o7.cast<float>() * 1000), new Vector3(ptrE7->get_axis_lengths()(0) * 1000 * 2, ptrE7->get_axis_lengths()(1) * 1000 * 2, 1), DrawColor {1, 0, 0, 0.5});


    // add to the list
    std::deque<std::unique_ptr<Obstacle> > obstacle_list;
    //    obstacle_list.push_back(std::move(ptrE1));
    //    obstacle_list.push_back(std::move(ptrE2));
    //    obstacle_list.push_back(std::move(ptrE3));
    //    obstacle_list.push_back(std::move(ptrE4));
    obstacle_list.push_back(std::move(ptrE5));
    obstacle_list.push_back(std::move(ptrE6));
    //    obstacle_list.push_back(std::move(ptrE7));




    auto platformVelX = getPlatformUnitObserver()->getDataFieldRef(new DataFieldIdentifier(getPlatformUnitObserver()->getObserverName(),
                        "platformVelocity", "velocityX"));
    auto platformVelY = getPlatformUnitObserver()->getDataFieldRef(new DataFieldIdentifier(getPlatformUnitObserver()->getObserverName(),
                        "platformVelocity", "velocityY"));

    Eigen::Vector3f goal = in.getGoalList()[in.getGoalName()]->toEigen();
    dd->setSphereVisu("DynamicObstacleAvoidance", "goal", new Vector3(Eigen::Vector3f(goal * 1000)), DrawColor {0, 1, 0, 0.5}, 50);
    CycleUtil c(10);
    float maxVel = in.getMaxVelocity(); // m/s
    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    Eigen::Vector3f oldVisuPos = Eigen::Vector3f::Zero();
    auto startTime = TimeUtil::GetTime();
    int visuSphereCounter = 0;

    ::Agent agent;
    //    agent.set_safety_margin(0.55);
    agent.set_safety_margin(0.55);

    Eigen::Vector3f filteredPlatformTargetVelocity = Eigen::Vector3f::Zero();

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        // do your calculations
        // synchronize robot clone to most recent state
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        if ((oldVisuPos - robot->getGlobalPosition()).norm() > 50)
        {
            dd->setSphereVisu("DynamicObstacleAvoidance", "position" + std::to_string(visuSphereCounter), new Vector3(robot->getGlobalPosition()), DrawColor {1, 0, 1, 0.5}, 25);
            oldVisuPos = robot->getGlobalPosition();
            visuSphereCounter++;
        }

        Eigen::Vector3f agent_position(robot->getGlobalPosition() / 1000);
        agent_position(2) = 0;
        agent.set_position(agent_position.cast<double>());

        //        dd->setCircleDebugLayerVisu("robot_margin", new Vector3(Eigen::Vector3f(robot->getGlobalPosition())),
        //                                    new Vector3(Eigen::Vector3f(Eigen::Vector3f::UnitZ())), agent.get_safety_margin(), 1, 100, DrawColor{1,0,1,0.5});
        dd->setBoxVisu("DynamicObstacleAvoidance", "robot_margin", new Pose(robot->getGlobalPose()), new Vector3(agent.get_safety_margin() * 1000 * 2, agent.get_safety_margin() * 1000 * 2, 1), DrawColor {1, 0, 0, 0.5});

        //        agent.set_linear_velocity(Eigen::Vector3f(platformVelX->getFloat()/1000,platformVelY->getFloat()/1000,0));


        // platform controllers

        Eigen::Vector3f vel = (goal - agent_position) * in.getKp();
        vel(2) = 0;
        if (vel.norm() > maxVel)
        {
            vel = vel.normalized() * maxVel;
        }
        agent.set_linear_velocity(vel.cast<double>());
        //        double margin = std::max(0.1, 0.55);
        //        static_cast<Ellipsoid*>(agent.envelope.get())->set_axis_lengths(Eigen::Array3d((goal - agent_position).norm(), margin, margin));

        ARMARX_VERBOSE << "to goal distance: " << (goal - agent_position).norm();
        ARMARX_VERBOSE << "envelope size: " << static_cast<Ellipsoid*>(agent.envelope.get())->get_axis_lengths()(0);
        ARMARX_VERBOSE << deactivateSpam(0.3) << VAROUT(goal) << VAROUT(agent_position) << " desired vel: " << agent.get_linear_velocity();
        dd->setArrowVisu("DynamicObstacleAvoidance", "desiredvel", new Vector3(Eigen::Vector3f(robot->getGlobalPosition() + Eigen::Vector3f(0, 0, 2000))), new Vector3(vel), DrawColor {0, 1, 0, 0.5},  vel.norm() * 2000, 25);
        Eigen::Vector3f modulated_velocity = Modulation::modulate_velocity(agent, obstacle_list, true).cast<float>() * 1000; // first true means local modulation, seconds true push the robot away from obstacle
        dd->setArrowVisu("DynamicObstacleAvoidance", "modulated_velocity", new Vector3(Eigen::Vector3f(robot->getGlobalPosition() + Eigen::Vector3f(0, 0, 2200))), new Vector3(modulated_velocity), DrawColor {0, 1, 1, 0.5},  modulated_velocity.norm() * 2, 25);
        ARMARX_VERBOSE << deactivateSpam(0.3) << "target vel: " << modulated_velocity;
        modulated_velocity(2) = 0.0;


        float ori = platformRotAng->getDataField()->getFloat();

        if (ori > M_PI)
        {
            ori = - 2  * M_PI + ori;
        }

        float angleDelta = goal(2) - ori;
        //        Eigen::Vector2f velDir = modulated_velocity.head(2);

        //        float cos_theta = velDir.normalized().dot(Eigen::Vector2f::UnitX());
        //        float sin_theta = velDir.normalized().dot(Eigen::Vector2f::UnitY());
        //        float target_angle = -atan2(cos_theta, sin_theta);

        //        ARMARX_INFO << target_angle << target_angle;
        //        float angleDelta = target_angle - ori;
        ARMARX_VERBOSE << deactivateSpam(1) << VAROUT(angleDelta);

        // transform alpha to [-pi, pi)
        while (angleDelta < -M_PI)
        {
            angleDelta += 2 * M_PI;
        }

        while (angleDelta >= M_PI)
        {
            angleDelta -= 2 * M_PI;
        }

        pidRot.update(angleDelta, 0);
        float newVelocityRot = -signedMin(pidRot.getControlValue(), in.getMaxAngularVelocity());



        Eigen::Matrix3f m;
        m = Eigen::AngleAxisf(ori, Eigen::Vector3f::UnitZ());
        //        Eigen::Vector3f localVel = agent.get_pose().inverse() * modulated_velocity;
        Eigen::Vector3f localVel = m.inverse() * modulated_velocity;
        if (localVel.norm() > in.getMaxVelocity() * 1000)
        {
            localVel *= in.getMaxVelocity() * 1000 / localVel.norm();
        }

        if (std::isnan(localVel[0]) || std::isnan(localVel[1]) || std::isnan(newVelocityRot))
        {
            ARMARX_WARNING << deactivateSpam(1) << "Some velocity target is NaN - setting to zero";
            localVel.setZero();
        }
        float ratio = 0.7;
        filteredPlatformTargetVelocity = filteredPlatformTargetVelocity * ratio + (1.0 - ratio) * localVel;
        ARMARX_VERBOSE << deactivateSpam(0.1) << "filtered modulated target velocity: " << filteredPlatformTargetVelocity;

        //        getPlatformUnit()->move(filteredPlatformTargetVelocity(0), filteredPlatformTargetVelocity(1), newVelocityRot);
        //        c.waitForCycleDuration();
        ARMARX_VERBOSE << "current error: .... " << (goal.head(2) - agent_position.head(2)).norm() << " " << fabs(angleDelta);

        if ((goal.head(2) - agent_position.head(2)).norm() < in.getPositionReachedThreshold() /*|| modulated_velocity.norm() < 1*/)
        {
            filteredPlatformTargetVelocity(0) = 0.0;
            filteredPlatformTargetVelocity(1) = 0.0;
            ARMARX_VERBOSE << "position reached .... " << (goal.head(2) - agent_position.head(2)).norm() << " " << fabs(angleDelta);
            if (fabs(angleDelta) < in.getAngleReachedThreshold())
            {
                ARMARX_VERBOSE << "orientation reached .... " << fabs(angleDelta);
                break;
            }
        }


        getPlatformUnit()->move(filteredPlatformTargetVelocity(0), filteredPlatformTargetVelocity(1), newVelocityRot);
        c.waitForCycleDuration();

    }
    getPlatformUnit()->move(0, 0, 0);
    //    dd->clearLayer("DynamicObstacleAvoidance");
    getDebugDrawerTopic()->clearAll();
    emitSuccess();
}

//void MovePlatformToHandOver::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MovePlatformToHandOver::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MovePlatformToHandOver::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MovePlatformToHandOver(stateData));
}
