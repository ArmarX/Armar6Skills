/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::KITImageFilmGroup::KITImageFilmGroupRemoteStateOfferer
 * @author     SecondHands Demo ( shdemo at armar6 )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "KITImageFilmGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace KITImageFilmGroup;

// DO NOT EDIT NEXT LINE
KITImageFilmGroupRemoteStateOfferer::SubClassRegistry KITImageFilmGroupRemoteStateOfferer::Registry(KITImageFilmGroupRemoteStateOfferer::GetName(), &KITImageFilmGroupRemoteStateOfferer::CreateInstance);



KITImageFilmGroupRemoteStateOfferer::KITImageFilmGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < KITImageFilmGroupStatechartContext > (reader)
{
}

void KITImageFilmGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void KITImageFilmGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void KITImageFilmGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string KITImageFilmGroupRemoteStateOfferer::GetName()
{
    return "KITImageFilmGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr KITImageFilmGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new KITImageFilmGroupRemoteStateOfferer(reader));
}
