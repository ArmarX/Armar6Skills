/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6RecordContactGroup::Armar6RecordContactGroupRemoteStateOfferer
 * @author     armar6-user ( armar6-user at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6RecordContactGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6RecordContactGroup;

// DO NOT EDIT NEXT LINE
Armar6RecordContactGroupRemoteStateOfferer::SubClassRegistry Armar6RecordContactGroupRemoteStateOfferer::Registry(Armar6RecordContactGroupRemoteStateOfferer::GetName(), &Armar6RecordContactGroupRemoteStateOfferer::CreateInstance);



Armar6RecordContactGroupRemoteStateOfferer::Armar6RecordContactGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6RecordContactGroupStatechartContext > (reader)
{
}

void Armar6RecordContactGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6RecordContactGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6RecordContactGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6RecordContactGroupRemoteStateOfferer::GetName()
{
    return "Armar6RecordContactGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6RecordContactGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6RecordContactGroupRemoteStateOfferer(reader));
}
