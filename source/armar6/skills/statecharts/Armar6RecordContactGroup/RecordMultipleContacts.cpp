/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6RecordContactGroup
 * @author     armar6-user ( armar6-user at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RecordMultipleContacts.h"

#include <RobotAPI/libraries/SimpleJsonLogger/SimpleJsonLogger.h>
#include <RobotAPI/libraries/core/FramedPose.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6RecordContactGroup;

// DO NOT EDIT NEXT LINE
RecordMultipleContacts::SubClassRegistry RecordMultipleContacts::Registry(RecordMultipleContacts::GetName(), &RecordMultipleContacts::CreateInstance);



void RecordMultipleContacts::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

class SlidingMedianFilter
{
public:
    std::vector<float> history;
    size_t length;
    size_t index = 0;

    SlidingMedianFilter(size_t length)
    {
        history = std::vector<float>(length, 0);
        this->length = length;
    }

    void add(float value)
    {
        history.at(index) = value;
        index = (index + 1) % length;
    }

    float get()
    {
        std::vector<float> copy = history;
        std::sort(copy.begin(), copy.end());
        return copy.at(length / 2);
    }
};

class SlidingDiffFilter
{
    std::vector<float> history;
    size_t length;
    SlidingDiffFilter(size_t length)
    {

    }
};

void RecordMultipleContacts::run()
{
    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet(in.getNodeSetName());
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();

    SimpleJsonLogger logger("contacts.json", true);

    //Eigen::Matrix4f pose = tcp->getPoseInRootFrame();
    //ARMARX_IMPORTANT << "contact pose: " << pose;
    /*SimpleJsonLoggerEntry entry;
    entry.AddTimestamp();
    entry.AddAsArr("pose", pose);

    logger.log(entry);*/

    ChannelRegistry channels = getForceTorqueObserver()->getAvailableChannels(false);
    for (::std::pair< ::std::string, ::armarx::ChannelRegistryEntry> channel : channels)
    {
        ARMARX_IMPORTANT << channel.first;
    }

    std::string ftName = "FT L_ArmL_FT";

    DatafieldRefPtr forceDf = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield(ftName));

    SlidingMedianFilter filterX(100);
    SlidingMedianFilter filterY(100);
    SlidingMedianFilter filterZ(100);

    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        FramedDirectionPtr forcePtr = forceDf->getDataField()->get<FramedDirection>();
        Eigen::Vector3f force = forcePtr->toEigen();
        filterX.add(force(0));
        filterY.add(force(1));
        filterZ.add(force(2));

        Eigen::Vector3f forceMedian;
        forceMedian << filterX.get(), filterY.get(), filterZ.get();

        Eigen::Vector3f forceDiff = force - forceMedian;

        getDebugObserver()->setDebugDatafield("RecordMultipleContacts", "forceDiff_x", new Variant(forceDiff(0)));
        getDebugObserver()->setDebugDatafield("RecordMultipleContacts", "forceDiff_y", new Variant(forceDiff(1)));
        getDebugObserver()->setDebugDatafield("RecordMultipleContacts", "forceDiff_z", new Variant(forceDiff(2)));

        usleep(10000);
    }

}

//void RecordMultipleContacts::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void RecordMultipleContacts::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr RecordMultipleContacts::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new RecordMultipleContacts(stateData));
}
