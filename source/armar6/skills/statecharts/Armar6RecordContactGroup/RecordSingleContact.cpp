/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6RecordContactGroup
 * @author     armar6-user ( armar6-user at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RecordSingleContact.h"

#include <RobotAPI/libraries/SimpleJsonLogger/SimpleJsonLogger.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6RecordContactGroup;

// DO NOT EDIT NEXT LINE
RecordSingleContact::SubClassRegistry RecordSingleContact::Registry(RecordSingleContact::GetName(), &RecordSingleContact::CreateInstance);



void RecordSingleContact::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void RecordSingleContact::run()
{
    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
    VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet(in.getNodeSetName());
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();

    Eigen::Matrix4f pose = tcp->getPoseInRootFrame();

    ARMARX_IMPORTANT << "contact pose: " << pose;

    SimpleJsonLogger logger("contacts.json", true);

    SimpleJsonLoggerEntry entry;
    entry.AddTimestamp();
    entry.AddAsArr("pose", pose);

    logger.log(entry);

    emitSuccess();
}

//void RecordSingleContact::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void RecordSingleContact::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr RecordSingleContact::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new RecordSingleContact(stateData));
}
