/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::StaticGraspExample::StaticGraspExampleRemoteStateOfferer
 * @author     Kota Suzui ( s175908s at st dot go dot tuat dot ac dot jp )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "StaticGraspExampleRemoteStateOfferer.h"

using namespace armarx;
using namespace StaticGraspExample;

// DO NOT EDIT NEXT LINE
StaticGraspExampleRemoteStateOfferer::SubClassRegistry StaticGraspExampleRemoteStateOfferer::Registry(StaticGraspExampleRemoteStateOfferer::GetName(), &StaticGraspExampleRemoteStateOfferer::CreateInstance);



StaticGraspExampleRemoteStateOfferer::StaticGraspExampleRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < StaticGraspExampleStatechartContext > (reader)
{
}

void StaticGraspExampleRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void StaticGraspExampleRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void StaticGraspExampleRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string StaticGraspExampleRemoteStateOfferer::GetName()
{
    return "StaticGraspExampleRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr StaticGraspExampleRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new StaticGraspExampleRemoteStateOfferer(reader));
}
