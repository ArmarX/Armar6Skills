/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::StaticGraspExample
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GetGraspPose.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace StaticGraspExample;

// DO NOT EDIT NEXT LINE
GetGraspPose::SubClassRegistry GetGraspPose::Registry(GetGraspPose::GetName(), &GetGraspPose::CreateInstance);



void GetGraspPose::onEnter()
{

    const DebugDrawerInterfacePrx debugDrawer = getDebugDrawerTopic();

    PosePtr graspOffset = in.getGraspOffset();
    const RobotStateComponentInterfacePrx robotStateComponent = getRobotStateComponent();

    ChannelRefPtr objectInstance = in.getObjectInstanceChannel();
    ARMARX_VERBOSE << "getting Object pose from localization";
    FramedPositionPtr position = objectInstance->get<FramedPosition>("position");
    FramedOrientationPtr orientation = objectInstance->get<FramedOrientation>("orientation");
    float posUncertainty = objectInstance->get<float>("uncertaintyOfPosition");
    ARMARX_INFO << "position Uncertainty " << posUncertainty;
    FramedPosePtr objectPose = new FramedPose(orientation->toEigen(), position->toEigen(), position->getFrame(), position->agent);

    ARMARX_LOG << "ObjectPose: " << objectPose;

    Eigen::Matrix4f m = objectPose->toEigen() * graspOffset->toEigen();

    FramedPosePtr graspPose = new FramedPose(m, position->getFrame(), position->agent);

    ARMARX_LOG << "GraspPose: " << graspPose;

    debugDrawer->setPoseVisu(getName(), "graspPose", graspPose->toGlobal(robotStateComponent->getSynchronizedRobot()));

    out.setGraspPose(graspPose);

    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

//void GetGraspPose::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void GetGraspPose::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GetGraspPose::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GetGraspPose::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GetGraspPose(stateData));
}
