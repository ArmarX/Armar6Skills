/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::StaticGraspExample
 * @author     Kota Suzui ( s175908s at st dot go dot tuat dot ac dot jp )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveTCP.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityController.h>

#include <VirtualRobot/RobotNodeSet.h>

#include <ArmarXCore/core/time/TimeUtil.h>

#include <RobotAPI/libraries/core/CartesianPositionController.h>

#include <RobotAPI/libraries/core/math/MathUtils.h>
#include <VirtualRobot/math/Line.h>

//#include </home/armar6-user/armarx/HapticExplorationTools/HapticExplorationLibrary/math/Line.h>

using namespace armarx;
using namespace StaticGraspExample;

// DO NOT EDIT NEXT LINE
MoveTCP::SubClassRegistry MoveTCP::Registry(MoveTCP::GetName(), &MoveTCP::CreateInstance);



void MoveTCP::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)

}

void MoveTCP::run()
{
    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());

    const IceUtil::Time startTime = TimeUtil::GetTime();

    NJointCartesianVelocityControllerConfigPtr config = new NJointCartesianVelocityControllerConfig(in.getNodeSetName(), "",  armarx::NJointCartesianVelocityControllerMode::CartesianSelection::eAll);

    NJointCartesianVelocityControllerInterfacePrx extendedTCPController = NJointCartesianVelocityControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController(RobotUnitControllerNames::NJointCartesianVelocityController, "extendedTCPController", config));
    extendedTCPController->activateController();

    VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet(in.getNodeSetName());
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();

    CartesianPositionController posController(tcp);
    posController.KpOri = in.getKpOrientation();
    posController.KpPos = in.getKpPosition();
    posController.maxOriVel = in.getMaxOrientationalVelocity();

    float maxKpAvoidJointLimits = in.getKpAvoidJointLimits();
    float startRampDuration = in.getStartRampDuration();
    double maxPositionVelocity = in.getMaxPositionalVelocity();

    bool prePoseMode = in.isTargetPrePoseSet();
    FramedPosePtr targetPosePtr = in.getTargetPose();
    FramedPosePtr targetPrePosePtr = prePoseMode ? in.getTargetPrePose() : in.getTargetPose();
    Eigen::Matrix4f targetPose = targetPosePtr->toRootEigen(localRobot);
    Eigen::Matrix4f targetPrePose = targetPrePosePtr->toRootEigen(localRobot);

    ::math::Line line = ::math::Line::FromPoints(targetPrePose.col(3).head<3>(), targetPose.col(3).head<3>());

    ARMARX_IMPORTANT << VAROUT(targetPrePose);
    ARMARX_IMPORTANT << VAROUT(targetPose);

    getDebugDrawerTopic()->clearLayer(getName());

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

        float lineT = prePoseMode ? line.GetT(tcp->getPositionInRootFrame()) : 0;
        float lineTtarget = math::MathUtils::LimitMinMax(0.f, 1.f, lineT + 0.2f);
        ARMARX_IMPORTANT << VAROUT(lineT) << VAROUT(lineTtarget);
        Eigen::Vector3f targetPosition = line.Get(lineTtarget);
        Eigen::Matrix3f targetOrientation = targetPose.block<3, 3>(0, 0); // TODO: interpolate orientation

        Eigen::Matrix4f currentTargetPose = Eigen::Matrix4f::Identity();
        currentTargetPose.block<3, 3>(0, 0) = targetOrientation;
        currentTargetPose.col(3).head<3>() = targetPosition;

        IceUtil::Time now = TimeUtil::GetTime();
        float t = (now - startTime).toSecondsDouble();

        Eigen::VectorXf cartesianVelocity = posController.calculate(currentTargetPose, VirtualRobot::IKSolver::CartesianSelection::All);

        posController.maxPosVel = math::MathUtils::LimitMinMax(0.f, 1.f, t / startRampDuration) * maxPositionVelocity;
        float KpAvoidJointLimits = math::MathUtils::LimitMinMax(0.f, 1.f, t / startRampDuration) * maxKpAvoidJointLimits;

        FramedPosePtr tcpPosePtr = new FramedPose(tcp->getPoseInRootFrame(), localRobot->getRootNode()->getName(), localRobot->getName());
        FramedPosePtr currentTargetPosePtr = new FramedPose(currentTargetPose, localRobot->getRootNode()->getName(), localRobot->getName());
        getDebugDrawerTopic()->setPoseVisu(getName(), "currentTarget", currentTargetPosePtr->toGlobal(localRobot));
        getDebugDrawerTopic()->setPoseVisu(getName(), "tcp", tcpPosePtr->toGlobal(localRobot));

        extendedTCPController->setControllerTarget(
            cartesianVelocity(0), cartesianVelocity(1), cartesianVelocity(2),
            cartesianVelocity(3), cartesianVelocity(4), cartesianVelocity(5),
            KpAvoidJointLimits, ::armarx::NJointCartesianVelocityControllerMode::CartesianSelection::eAll);

        //extendedTCPController->setControllerTarget(dir(0), dir(1), dir(2), oriVel(0), oriVel(1), oriVel(2), 1, eAll);


        bool targetReached = true;
        targetReached &= (posController.getPositionError(targetPose) < in.getPositionalAccuracy());
        targetReached &= (posController.getOrientationError(targetPose) < in.getOrientationalAccuracyRad());

        if ((TimeUtil::GetTime() - startTime).toSecondsDouble() > in.getTimeout() || targetReached)
        {
            break;
        }

        usleep(10000);
    }

    ARMARX_WARNING << "done. timeout or target reached";

    extendedTCPController->deactivateController();
    while (extendedTCPController->isControllerActive())
    {
        usleep(10000);
    }

    extendedTCPController->deleteController();


    emitTCPTargetReached();
}

//void MoveTCP::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MoveTCP::onExit()
{
    ARMARX_WARNING << "MoveTCP::onExit 1 ";
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveTCP::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveTCP(stateData));
}
