/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::StaticGraspExample
 * @author     Kota Suzui ( s175908s at st dot go dot tuat dot ac dot jp )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LookAtTarget.h"

//#include <ArmarXCore/observers/variant/DatafieldRef.h>


#include <ArmarXCore/core/time/TimeUtil.h>

using namespace armarx;
using namespace StaticGraspExample;


// DO NOT EDIT NEXT LINE
LookAtTarget::SubClassRegistry LookAtTarget::Registry(LookAtTarget::GetName(), &LookAtTarget::CreateInstance);



void LookAtTarget::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void LookAtTarget::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    const ViewSelectionInterfacePrx viewSelection = getViewSelection();


    const IceUtil::Time startTime = TimeUtil::GetTime();


    bool isEnabled = viewSelection->isEnabledAutomaticViewSelection();

    viewSelection->deactivateAutomaticViewSelection();


    viewSelection->addManualViewTarget(in.getTarget());

    bool doStuff = true;

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    while (!isRunningTaskStopped() && doStuff) // stop run function if returning true
    {
        ARMARX_LOG << "elapsed time: " << (TimeUtil::GetTime() - startTime).toSecondsDouble();

        doStuff &= (TimeUtil::GetTime() - startTime).toSecondsDouble() < in.getDuration();

        usleep(10000);
    }

    ARMARX_LOG << "done";

    if (isEnabled)
    {
        viewSelection->activateAutomaticViewSelection();

    }

    emitDone();
}

//void LookAtTarget::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void LookAtTarget::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr LookAtTarget::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new LookAtTarget(stateData));
}
