/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6VisualGraspingGroup::Armar6VisualGraspingGroupRemoteStateOfferer
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6VisualGraspingGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6VisualGraspingGroup;

// DO NOT EDIT NEXT LINE
Armar6VisualGraspingGroupRemoteStateOfferer::SubClassRegistry Armar6VisualGraspingGroupRemoteStateOfferer::Registry(Armar6VisualGraspingGroupRemoteStateOfferer::GetName(), &Armar6VisualGraspingGroupRemoteStateOfferer::CreateInstance);



Armar6VisualGraspingGroupRemoteStateOfferer::Armar6VisualGraspingGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6VisualGraspingGroupStatechartContext > (reader)
{
}

void Armar6VisualGraspingGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6VisualGraspingGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6VisualGraspingGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6VisualGraspingGroupRemoteStateOfferer::GetName()
{
    return "Armar6VisualGraspingGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6VisualGraspingGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6VisualGraspingGroupRemoteStateOfferer(reader));
}
