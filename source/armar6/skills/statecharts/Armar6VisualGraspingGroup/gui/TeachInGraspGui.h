/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>

#include <Eigen/Core>

#include <memory>

namespace armarx
{
    typedef std::shared_ptr<class TeachInGraspGui> TeachInGraspGuiPtr;

    class TeachInGraspGui
    {
    public:

        class Widget;
        using WidgetPtr = std::shared_ptr<Widget>;

        class Widget
        {
        protected:
            bool dirty = false;

        public:
            virtual ~Widget() {}

            virtual void read(RemoteGui::TabProxy& guiTab) {}
            virtual void readInit(RemoteGui::TabProxy& guiTab)
            {
                read(guiTab);
            }
            virtual void write(RemoteGui::TabProxy& guiTab) {}
        };

        class Button;
        typedef std::shared_ptr<Button> ButtonPtr;
        class Button : public Widget
        {
        private:
            int lastClickValue;
            int currentClickValue;
            std::string name;
            std::vector<std::function<void()>> clickHandlers;
        public:
            Button(const std::string& name)
                : name(name)
            { }

            void read(RemoteGui::TabProxy& guiTab)
            {
                lastClickValue = currentClickValue;
                currentClickValue = guiTab.getValue<int>(name).get();
                if (wasClicked())
                {
                    //std::for_each(clickHandlers.begin(), clickHandlers.end(), [](std::function<void()> clb){clb();});
                    for (const std::function<void()>& clb : clickHandlers)
                    {
                        clb();
                    }
                }
            }
            void readInit(RemoteGui::TabProxy& guiTab)
            {
                currentClickValue = guiTab.getValue<int>(name).get();
                lastClickValue = currentClickValue;
            }

            bool wasClicked()
            {
                return currentClickValue > lastClickValue;
            }
            int clickDelta()
            {
                return currentClickValue - lastClickValue;
            }
            int clickValue()
            {
                return currentClickValue;
            }
            void onClick(const std::function<void()>& clb)
            {
                clickHandlers.emplace_back(clb);
            }
        };

        class ToggleButton;
        typedef std::shared_ptr<ToggleButton> ToggleButtonPtr;
        class ToggleButton : public Widget
        {
        private:
            std::string name;
            bool down = false;

        public:
            ToggleButton(const std::string& name)
                : name(name)
            { }

            void read(RemoteGui::TabProxy& guiTab)
            {
                down = guiTab.getValue<bool>(name).get();
            }

            bool isDown()
            {
                return down;
            }

        };

        class Label;
        typedef std::shared_ptr<Label> LabelPtr;
        class Label : public Widget
        {
        private:
            std::string name;
            std::string value;
        public:
            Label(const std::string& name, const std::string& value)
                : name(name), value(value)
            { }

            void write(RemoteGui::TabProxy& guiTab)
            {
                if (dirty)
                {
                    dirty = false;
                    guiTab.getValue<std::string>(name).set(value);
                }
            }

            void setValue(const std::string& value)
            {
                this->value = value;
                this->dirty = true;
            }
        };

        class Slider;
        typedef std::shared_ptr<Slider> SliderPtr;
        class Slider : public Widget
        {
        private:
            std::string name;
            float value;
        public:
            Slider(const std::string& name, float value)
                : name(name), value(value)
            { }
            void read(RemoteGui::TabProxy& guiTab)
            {
                this->value = guiTab.getValue<float>(name).get();
            }
            void write(RemoteGui::TabProxy& guiTab)
            {
                guiTab.getValue<std::string>(name + "_valuelabel").set(std::to_string(value));
                if (dirty)
                {
                    dirty = false;
                    guiTab.getValue<float>(name).set(value);
                }
            }

            float getValue()
            {
                return this->value;
            }
            void setValue(float value)
            {
                this->value = value;
                this->dirty = true;
            }
        };

        class ComboBox;
        typedef std::shared_ptr<ComboBox> ComboBoxPtr;
        class ComboBox : public Widget
        {
        private:
            std::string name;
            std::string value;
        public:
            ComboBox(const std::string& name, const std::string& value)
                : name(name), value(value)
            { }

            void read(RemoteGui::TabProxy& guiTab)
            {
                value = guiTab.getValue<std::string>(name).get();
            }

            std::string getValue()
            {
                return value;
            }
        };

        class LineEdit;
        typedef std::shared_ptr<LineEdit> LineEditPtr;
        class LineEdit : public Widget
        {
        private:
            std::string name;
            std::string value;
        public:
            LineEdit(const std::string& name, const std::string& value)
                : name(name), value(value)
            { }

            void read(RemoteGui::TabProxy& guiTab)
            {
                value = guiTab.getValue<std::string>(name).get();
            }

            std::string getValue()
            {
                return value;
            }
        };

        class FloatSpinBox;
        typedef std::shared_ptr<FloatSpinBox> FloatSpinBoxPtr;
        class FloatSpinBox : public Widget
        {
        private:
            std::string name;
            float value;
        public:
            FloatSpinBox(const std::string& name, float value)
                : name(name), value(value)
            { }
            void read(RemoteGui::TabProxy& guiTab)
            {
                this->value = guiTab.getValue<float>(name).get();
            }
            void write(RemoteGui::TabProxy& guiTab)
            {
                if (dirty)
                {
                    dirty = false;
                    guiTab.getValue<float>(name).set(value);
                }
            }

            float getValue()
            {
                return this->value;
            }
            void setValue(float value)
            {
                this->value = value;
                this->dirty = true;
            }

        };


        class Container;
        typedef std::shared_ptr<Container> ContainerPtr;
        class VBox;
        typedef std::shared_ptr<VBox> VBoxPtr;
        class HBox;
        typedef std::shared_ptr<HBox> HBoxPtr;

        class Container : public Widget
        {
        private:
            std::vector<WidgetPtr> children;
            bool addSpacerAtEnd = true;

        public:
            void readRecursive(RemoteGui::TabProxy& guiTab)
            {
                for (const WidgetPtr& c : this->children)
                {
                    ContainerPtr container = std::dynamic_pointer_cast<Container>(c);
                    if (container)
                    {
                        container->readRecursive(guiTab);
                    }
                    else
                    {
                        c->read(guiTab);
                    }
                }
            }
            void readInitRecursive(RemoteGui::TabProxy& guiTab)
            {
                for (const WidgetPtr& c : this->children)
                {
                    ContainerPtr container = std::dynamic_pointer_cast<Container>(c);
                    if (container)
                    {
                        container->readInitRecursive(guiTab);
                    }
                    else
                    {
                        c->readInit(guiTab);
                    }
                }
            }
            void writeRecursive(RemoteGui::TabProxy& guiTab)
            {
                for (const WidgetPtr& c : this->children)
                {
                    ContainerPtr container = std::dynamic_pointer_cast<Container>(c);
                    if (container)
                    {
                        container->writeRecursive(guiTab);
                    }
                    else
                    {
                        c->write(guiTab);
                    }
                }
            }
            void buildRecursive()
            {
                for (const WidgetPtr& c : this->children)
                {
                    ContainerPtr container = std::dynamic_pointer_cast<Container>(c);
                    if (container)
                    {
                        container->buildRecursive();
                    }
                }
                if (addSpacerAtEnd)
                {
                    addSpacer();
                }
            }

            virtual void addChild(RemoteGui::WidgetPtr const& child) = 0;
            virtual void addSpacer() = 0;
            ButtonPtr newButton(const std::string& name, const std::string& label)
            {
                addChild(RemoteGui::makeButton(name).label(label));
                ButtonPtr button(new Button(name));
                children.emplace_back(button);
                return button;
            }
            LabelPtr newLabel(const std::string& name, const std::string& value)
            {
                addChild(RemoteGui::makeLabel(name).value(value));
                LabelPtr label(new Label(name, value));
                children.emplace_back(label);
                return label;
            }
            SliderPtr newSlider(const std::string& name, const std::string label, float min, float max, float value)
            {
                addChild(RemoteGui::makeLabel(name + "_namelabel").value(label));
                addChild(RemoteGui::makeFloatSlider(name).min(min).max(max).value(value));
                addChild(RemoteGui::makeLabel(name + "_valuelabel").value(std::to_string(value)));
                SliderPtr slider(new Slider(name, value));
                children.emplace_back(slider);
                return slider;
            }
            ToggleButtonPtr newToggleButton(const std::string& name, const std::string& label)
            {
                addChild(RemoteGui::makeToggleButton(name).label(label));
                ToggleButtonPtr button(new ToggleButton(name));
                children.emplace_back(button);
                return button;
            }
            ComboBoxPtr newComboBox(const std::string& name, const std::string& label, const std::vector<std::string>& options)
            {
                addChild(RemoteGui::makeLabel(name + "_namelabel").value(label));
                addChild(RemoteGui::makeComboBox(name).options(options).value(options.at(0)));
                ComboBoxPtr comboBox(new ComboBox(name, options.at(0)));
                children.emplace_back(comboBox);
                return comboBox;
            }
            LineEditPtr newLineEdit(const std::string& name, const std::string& label, const std::string& value)
            {
                addChild(RemoteGui::makeLabel(name + "_namelabel").value(label));
                addChild(RemoteGui::makeLineEdit(name).value(value));
                LineEditPtr lineEdit(new LineEdit(name, value));
                children.emplace_back(lineEdit);
                return lineEdit;
            }
            FloatSpinBoxPtr newFloatSpinBox(const std::string& name, const std::string& label, float min, float max, float value, int decimals = 2, int steps = 100)
            {
                addChild(RemoteGui::makeLabel(name + "_namelabel").value(label));
                addChild(RemoteGui::makeFloatSpinBox(name).min(min).max(max).value(value).decimals(decimals).steps(steps));
                FloatSpinBoxPtr floatSpinBox(new FloatSpinBox(name, value));
                children.emplace_back(floatSpinBox);
                return floatSpinBox;
            }

            HBoxPtr newHBox(const std::string& name = "")
            {
                HBoxPtr hbox(new HBox(name));
                children.emplace_back(hbox);
                addChild(hbox->b);
                return hbox;
            }
            VBoxPtr newVBox(const std::string& name = "")
            {
                VBoxPtr vbox(new VBox(name));
                children.emplace_back(vbox);
                addChild(vbox->b);
                return vbox;
            }
            VBoxPtr newGroupVBox(const std::string& label, const std::string& groupBoxName = "", const std::string& vBoxName = "")
            {
                VBoxPtr vbox(new VBox(vBoxName));
                children.emplace_back(vbox);
                addChild(RemoteGui::makeGroupBox(groupBoxName).label(label).child(vbox->b));
                return vbox;
            }
            HBoxPtr newGroupHBox(const std::string& label, const std::string& groupBoxName = "", const std::string& hBoxName = "")
            {
                HBoxPtr hbox(new HBox(hBoxName));
                children.emplace_back(hbox);
                addChild(RemoteGui::makeGroupBox(groupBoxName).label(label).child(hbox->b));
                return hbox;
            }

            void newHSpacer()
            {
                addChild(new RemoteGui::HSpacer());
            }
            void newVSpacer()
            {
                addChild(new RemoteGui::VSpacer());
            }
        };


        class VBox : public Container
        {
        public:
            RemoteGui::detail::VBoxLayoutBuilder b;
            VBox(std::string const& name = "")
                : b(name)
            { }
            VBox(RemoteGui::detail::VBoxLayoutBuilder b)
                : b(b)
            { }
            void addChild(const RemoteGui::WidgetPtr& child) override
            {
                b.addChild(child);
            }
            void addSpacer() override
            {
                newVSpacer();
            }
        };
        class HBox : public Container
        {
        public:
            RemoteGui::detail::HBoxLayoutBuilder b;
            HBox(std::string const& name = "")
                : b(name)
            { }
            HBox(RemoteGui::detail::HBoxLayoutBuilder b)
                : b(b)
            { }
            void addChild(const RemoteGui::WidgetPtr& child) override
            {
                b.addChild(child);
            }
            void addSpacer() override
            {
                newHSpacer();
            }
        };

        RemoteGui::TabProxy guiTab;
        RemoteGuiInterfacePrx remoteGuiPrx;

        ButtonPtr btnGetGraspCandidates;
        ButtonPtr btnPrevCandidate;
        ButtonPtr btnNextCandidate;
        ButtonPtr btnGotoInitPosition;
        ButtonPtr btnGotoPrePose, btnGotoGraspPose;
        ButtonPtr btnMoveDownUntilForce;
        ButtonPtr btnDeltaXP, btnDeltaXN, btnDeltaYP, btnDeltaYN, btnDeltaZP, btnDeltaZN;
        ButtonPtr btnDeltaXrP, btnDeltaXrN, btnDeltaYrP, btnDeltaYrN, btnDeltaZrP, btnDeltaZrN;
        FloatSpinBoxPtr spnDeltaPos, spnDeltaOri;
        ButtonPtr btnDeltaFP, btnDeltaFN, btnDeltaTP, btnDeltaTN, btnOpenFingers, btnCloseFingers;
        ButtonPtr btnResetTrajectory;
        ButtonPtr btnAddKeypoint, btnReplaceKeypoint, btnNextKeypoint, btnGotoKeypoint, btnPrevKeypoint, btnRemoveKeypoint, btnLift;
        FloatSpinBoxPtr spnKeypointDt;
        ButtonPtr btnApplyKeypointDt;

        ButtonPtr btnTranslateTrajectory;
        ButtonPtr btnGotoTrajectoryStart, btnStartTrajectory, btnStopTrajectory;
        LabelPtr lblCandidateInfo, lblTrajectoryInfo, lblTrajectoryDescr, lblTrajectoryIO, lblTimer;
        SliderPtr sldTrajectory;
        ToggleButtonPtr btnPreviewTrajectory, btnVisuTrajectory;
        LineEditPtr edtFile;
        ButtonPtr btnLoadTrajectory, btnSaveTrajectory;
        ComboBoxPtr cmbTrajectoryFiles;


        RemoteGui::detail::VBoxLayoutBuilder rootLayoutBuilder = RemoteGui::makeVBoxLayout();
        VBoxPtr rootLayout;

        TeachInGraspGui(const RemoteGuiInterfacePrx& remoteGuiPrx)
            : remoteGuiPrx(remoteGuiPrx)
        { }

        void initTab()
        {
            createWidgets();
            initRead();
        }
        void createWidgets()
        {

            rootLayout.reset(new VBox(rootLayoutBuilder));

            HBoxPtr candidatesLayout = rootLayout->newGroupHBox("Grasp Candidates");

            btnGetGraspCandidates = candidatesLayout->newButton("GetGraspCandidates", "Get grasp candidates");
            lblCandidateInfo      = candidatesLayout->newLabel("CandidatesInfo", "no candidates");
            btnPrevCandidate      = candidatesLayout->newButton("PrevCandidate", "Select Previous Candidate");
            btnNextCandidate      = candidatesLayout->newButton("NextCandidate", "Select Next Candidate");
            lblTimer              = candidatesLayout->newLabel("Timer", "no update ...");
            //candidatesLayout->newHSpacer();

            HBoxPtr gotoLayout = rootLayout->newGroupHBox("Goto");
            btnGotoInitPosition   = gotoLayout->newButton("GotoInitPosition", "Init position");
            btnGotoPrePose        = gotoLayout->newButton("GotoPrePose", "Pre Pose");
            btnGotoGraspPose      = gotoLayout->newButton("GotoGraspPose", "Grasp Pose");
            btnMoveDownUntilForce = gotoLayout->newButton("MoveDownUntilForce", "Move Down Until Force");
            //gotoLayout->newHSpacer();


            VBoxPtr controlLayout = rootLayout->newGroupVBox("Control");
            HBoxPtr deltaPosButtonsLayout = controlLayout->newHBox();
            deltaPosButtonsLayout->newLabel("lblDeltaPosButtons", "Position:");
            btnDeltaXP = deltaPosButtonsLayout->newButton("DeltaXP", "X+");
            btnDeltaXN = deltaPosButtonsLayout->newButton("DeltaXN", "X-");
            btnDeltaYP = deltaPosButtonsLayout->newButton("DeltaYP", "Y+");
            btnDeltaYN = deltaPosButtonsLayout->newButton("DeltaYN", "Y-");
            btnDeltaZP = deltaPosButtonsLayout->newButton("DeltaZP", "Z+");
            btnDeltaZN = deltaPosButtonsLayout->newButton("DeltaZN", "Z-");
            spnDeltaPos = deltaPosButtonsLayout->newFloatSpinBox("DeltaPos", "dPos", 1, 10, 2, 1);
            //deltaPosButtonsLayout->newHSpacer();

            HBoxPtr deltaOriButtonsLayout = controlLayout->newHBox();
            deltaOriButtonsLayout->newLabel("lblDeltaOriButtons", "Orientation:");
            btnDeltaXrP = deltaOriButtonsLayout->newButton("DeltaXrP", "Xr+");
            btnDeltaXrN = deltaOriButtonsLayout->newButton("DeltaXrN", "Xr-");
            btnDeltaYrP = deltaOriButtonsLayout->newButton("DeltaYrP", "Yr+");
            btnDeltaYrN = deltaOriButtonsLayout->newButton("DeltaYrN", "Yr-");
            btnDeltaZrP = deltaOriButtonsLayout->newButton("DeltaZrP", "Zr+");
            btnDeltaZrN = deltaOriButtonsLayout->newButton("DeltaZrN", "Zr-");
            spnDeltaOri = deltaOriButtonsLayout->newFloatSpinBox("DeltaOri", "dOri", 1, 10, 1, 1);
            //deltaOriButtonsLayout->newHSpacer();


            HBoxPtr deltaFingersButtonsLayout = controlLayout->newHBox();
            deltaFingersButtonsLayout->newLabel("lblDeltaFingers", "Fingers:");
            btnDeltaFP = deltaFingersButtonsLayout->newButton("DeltaFP", "F+");
            btnDeltaFN = deltaFingersButtonsLayout->newButton("DeltaFN", "F-");
            btnDeltaTP = deltaFingersButtonsLayout->newButton("DeltaTP", "T+");
            btnDeltaTN = deltaFingersButtonsLayout->newButton("DeltaTN", "T-");
            btnOpenFingers  = deltaFingersButtonsLayout->newButton("OpenFingers", "Open");
            btnCloseFingers = deltaFingersButtonsLayout->newButton("CloseFingers", "Close");
            //deltaFingersButtonsLayout->newHSpacer();;

            VBoxPtr trajLayout = rootLayout->newGroupVBox("Trajectory");


            HBoxPtr trajRow1 = trajLayout->newHBox();
            btnResetTrajectory = trajRow1->newButton("ResetTrajectory", "Start/Reset");
            lblTrajectoryInfo  = trajRow1->newLabel("TrajectoryInfo", "no trajectory");
            btnAddKeypoint     = trajRow1->newButton("AddKeypoint", "Add KP");
            btnReplaceKeypoint = trajRow1->newButton("ReplaceKeypoint", "Replace KP");
            btnPrevKeypoint    = trajRow1->newButton("PrevKeypoint", "Prev KP");
            btnNextKeypoint    = trajRow1->newButton("NextKeypoint", "Next KP");
            btnGotoKeypoint    = trajRow1->newButton("GotoKeypoint", "Goto KP");
            btnRemoveKeypoint  = trajRow1->newButton("RemoveKeypoint", "Remove KP");
            //trajRow1->newHSpacer();

            {
                HBoxPtr row = trajLayout->newHBox();
                spnKeypointDt      = row->newFloatSpinBox("KeypointDt", "Dt", 0, 5, 0.5f, 2, 100);
                btnApplyKeypointDt = row->newButton("ApplyKeypointDt", "Apply");
            }
            {
                HBoxPtr row = trajLayout->newHBox();
                btnTranslateTrajectory = row->newButton("TranslateTrajectory", "Translate to current position");
                //row->addSpacer();
            }


            HBoxPtr trajRow2 = trajLayout->newHBox();
            lblTrajectoryDescr = trajRow2->newLabel("TrajectoryDescr", "Trajectory Description");
            //trajRow2->newHSpacer();

            HBoxPtr trajRow3 = trajLayout->newHBox();
            sldTrajectory = trajRow3->newSlider("TrajectoryProgress", "Progress", 0, 100, 0);
            //trajRow3->newHSpacer();

            HBoxPtr trajRow4 = trajLayout->newHBox();
            btnGotoTrajectoryStart = trajRow4->newButton("GotoTrajectoryStart", "Goto Start");
            btnStartTrajectory     = trajRow4->newButton("StartTrajectory", "Run");
            btnStopTrajectory      = trajRow4->newButton("StopTrajectory", "Stop");
            btnPreviewTrajectory   = trajRow4->newToggleButton("PreviewTrajectory", "Preview");
            btnVisuTrajectory      = trajRow4->newToggleButton("VisuTrajectory", "Visu");
            //trajRow4->newHSpacer();

            HBoxPtr trajRow5 = trajLayout->newHBox();
            edtFile           = trajRow5->newLineEdit("FileName", "File name", "Grasp");
            btnLoadTrajectory = trajRow5->newButton("LoadTrajectory", "Load");
            btnSaveTrajectory = trajRow5->newButton("SaveTrajectory", "Save");
            //trajRow5->newHSpacer();


            btnLift = rootLayout->newButton("Lift", "Lift");

            //rootLayout->newVSpacer();

            rootLayout->buildRecursive();


            RemoteGui::WidgetPtr rootWidget = rootLayout->b;
            remoteGuiPrx->createTab("TeachInGrasp", rootWidget);
            guiTab = RemoteGui::TabProxy(remoteGuiPrx, "TeachInGrasp");
        }

        void initRead()
        {
            guiTab.receiveUpdates();
            rootLayout->readInitRecursive(guiTab);
        }

        void updateRead()
        {
            guiTab.receiveUpdates();
            rootLayout->readRecursive(guiTab);
        }
        void updateWrite()
        {
            rootLayout->writeRecursive(guiTab);
            guiTab.sendUpdates();
        }

        Eigen::Vector3f getDeltaPos()
        {
            return Eigen::Vector3f(btnDeltaXP->clickDelta() - btnDeltaXN->clickDelta(),
                                   btnDeltaYP->clickDelta() - btnDeltaYN->clickDelta(),
                                   btnDeltaZP->clickDelta() - btnDeltaZN->clickDelta());
        }
        Eigen::Vector3f getDeltaOri()
        {
            return Eigen::Vector3f(btnDeltaXrP->clickDelta() - btnDeltaXrN->clickDelta(),
                                   btnDeltaYrP->clickDelta() - btnDeltaYrN->clickDelta(),
                                   btnDeltaZrP->clickDelta() - btnDeltaZrN->clickDelta());
        }
        Eigen::Vector3f getDeltaFingers()
        {
            return Eigen::Vector3f(btnDeltaFP->clickDelta() - btnDeltaFN->clickDelta(),
                                   btnDeltaTP->clickDelta() - btnDeltaTN->clickDelta(),
                                   0);
        }
    };
}
