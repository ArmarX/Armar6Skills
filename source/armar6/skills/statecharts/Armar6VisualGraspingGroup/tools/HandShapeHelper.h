/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>

#include <Eigen/Dense>

#include <memory>

namespace armarx
{
    typedef std::shared_ptr<class HandShapeHelper> HandShapeHelperPtr;

    class HandShapeHelper
    {
    public:
        HandShapeHelper(KinematicUnitInterfacePrx kinUnit, const std::string& handJointsPrefix)
            : kinUnitHelper(kinUnit)
        {
            fingersName = handJointsPrefix + "Fingers";
            thumbName = handJointsPrefix + "Thumb";
        }

        void shapeHand(float fingers, float thumb)
        {
            kinUnitHelper.setJointAngles({{fingersName, fingers}, {thumbName, thumb}});
        }
        void shapeHand(Eigen::Vector3f jointValues)
        {
            shapeHand(jointValues(0), jointValues(1));
        }

    private:
        KinematicUnitHelper kinUnitHelper;
        std::string handJointsPrefix;
        std::string fingersName;
        std::string thumbName;
    };
}
