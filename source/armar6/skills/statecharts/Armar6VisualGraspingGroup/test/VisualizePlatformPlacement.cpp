/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6VisualGraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "VisualizePlatformPlacement.h"

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <armar6/skills/libraries/Armar6Tools/Armar6GraspTrajectory.h>
#include <armar6/skills/libraries/Armar6Tools/Armar6PlatformPlacement.h>

#include <VirtualRobot/math/Grid3D.h>

#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6VisualGraspingGroup;

// DO NOT EDIT NEXT LINE
VisualizePlatformPlacement::SubClassRegistry VisualizePlatformPlacement::Registry(VisualizePlatformPlacement::GetName(), &VisualizePlatformPlacement::CreateInstance);



void VisualizePlatformPlacement::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}


void VisualizePlatformPlacement::run()
{
    VirtualRobot::RobotPtr robot = getLocalRobot();
    ::armarx::RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    RobotNameHelperPtr rnh = getRobotNameHelper();
    DebugDrawerHelper debugDrawerHelper(getDebugDrawerTopic(), "VisualizePlatformPlacement", robot);
    debugDrawerHelper.clearLayer();

    std::string packageName = "armar6_skills";
    armarx::CMakePackageFinder finder(packageName);
    std::string dataDir = finder.getDataDir() + "/" + packageName;
    std::map<std::string, Armar6GraspTrajectoryPtr> graspTrajectories;
    graspTrajectories["RightTopOpen"] = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/RightTopOpen.xml");
    graspTrajectories["RightSideOpen"] = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/RightSideOpen.xml");

    RobotNameHelper::RobotArm arm = rnh->getRobotArm("Right", robot);
    Armar6PlatformPlacement pp(arm.getKinematicChain(), arm.getTCP());

    math::Grid3DPtr grid = math::Grid3D::CreateFromCenterAndSize(Eigen::Vector3f::Zero(), Eigen::Vector3f(in.getGridSizeX(), 0, 0), in.getStepLength());

    DrawColor colorBlue = DrawColor {0, 0, 1, 1};
    DrawColor colorGreen = DrawColor {0, 1, 0, 1};

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        float visuOffsetZ = 1000;
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        grasping::GraspCandidateSeq candidates = getGraspCandidateObserver()->getAllCandidates();
        int index = 0;

        for (const auto& gc : candidates)
        {
            Armar6GraspTrajectoryPtr graspTrajectory = graspTrajectories.at(gc->executionHints->graspTrajectoryName);
            graspTrajectory = graspTrajectory->getTransformedToGraspPose(GraspCandidateHelper(gc, robot).getGraspPoseInRobotRoot());
            std::vector<Armar6PlatformPlacement::PlacementReachability> prs = pp.calculateReachability(graspTrajectory, grid);

            float maxMargin = -1;
            size_t maxIndex = 0;
            for (size_t i = 0; i < prs.size(); i++)
            {
                Armar6PlatformPlacement::PlacementReachability pr = prs.at(i);
                if (pr.reachability.reachable && pr.reachability.minimumJointLimitMargin > maxMargin)
                {
                    maxMargin = pr.reachability.minimumJointLimitMargin;
                    maxIndex = i;
                }
            }

            for (size_t i = 0; i < prs.size(); i++)
            {
                Armar6PlatformPlacement::PlacementReachability pr = prs.at(i);
                if (pr.reachability.reachable)
                {
                    Eigen::Vector3f p1 = GraspCandidateHelper(gc, robot).getGraspPositionInRobotRoot();
                    Eigen::Vector3f p2 = math::Helpers::Position(pr.relativePose) + Eigen::Vector3f(0, 0, visuOffsetZ);
                    DrawColor color = i == maxIndex ? colorGreen : colorBlue;
                    debugDrawerHelper.drawLine("line_" + std::to_string(index), p1, p2, 2, color);
                    index++;
                }
            }
        }

        debugDrawerHelper.cyclicCleanup();
        TimeUtil::SleepMS(10);
    }
    debugDrawerHelper.clearLayer();
}

//void VisualizePlatformPlacement::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void VisualizePlatformPlacement::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr VisualizePlatformPlacement::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new VisualizePlatformPlacement(stateData));
}
