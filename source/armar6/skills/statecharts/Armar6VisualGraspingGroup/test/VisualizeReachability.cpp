/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6VisualGraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "VisualizeReachability.h"

#include <ArmarXCore/core/time/TimeUtil.h>

#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>

#include <armar6/skills/libraries/Armar6Tools/Armar6HandV2Helper.h>
#include <RobotAPI/libraries/diffik/SimpleDiffIK.h>
#include <VirtualRobot/math/Grid3D.h>
#include <VirtualRobot/math/Helpers.h>

using namespace armarx;
using namespace Armar6VisualGraspingGroup;

// DO NOT EDIT NEXT LINE
VisualizeReachability::SubClassRegistry VisualizeReachability::Registry(VisualizeReachability::GetName(), &VisualizeReachability::CreateInstance);



void VisualizeReachability::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void VisualizeReachability::run()
{
    std::string side = in.getSide();
    VirtualRobot::RobotPtr robot = getLocalRobot();
    ::armarx::RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    RobotNameHelper::RobotArm arm = getRobotNameHelper()->getRobotArm(side, robot);
    VirtualRobot::RobotNodeSetPtr rns = arm.getKinematicChain();
    VirtualRobot::RobotNodePtr tcp = arm.getTCP();
    DebugDrawerHelper debugDrawerHelper(getDebugDrawerTopic(), "VisualizeReachability", robot);
    Armar6HandV2Helper handHelper(getKinematicUnit(), getRobotUnitObserver(), side, true);

    Eigen::Matrix3f oriOpen = in.getReferenceOrientationMap().at("RightTopOpen")->toEigen();
    Eigen::Matrix3f oriPreshaped = in.getReferenceOrientationMap().at("RightTopPreshaped")->toEigen();

    int stepsA = in.getStepsA();
    Eigen::Vector3f gridSize = in.getGridSize()->toEigen();
    Eigen::Vector3f referencePosition = in.getReferencePosition()->toEigen();

    math::Grid3DPtr grid = math::Grid3D::CreateFromBox(-gridSize + referencePosition, gridSize + referencePosition, in.getStepLength());

    Eigen::VectorXf initJV = Eigen::VectorXf::Zero(rns->getSize());

    debugDrawerHelper.clearLayer();


    int i = 0;
    for (const Eigen::Vector3f& p : grid->AllGridPoints())
    {
        for (int a = 0; a < stepsA; a++)
        {
            float angle = math::Helpers::Lerp(0, 2 * M_PI, 0, stepsA, a);
            Eigen::AngleAxisf aa(angle, Eigen::Vector3f::UnitZ());
            Eigen::Matrix4f targetOpen = math::Helpers::CreatePose(p, aa * oriOpen);
            Eigen::Matrix4f targetPreshaped = math::Helpers::CreatePose(p, aa * oriPreshaped);
            rns->setJointValues(initJV);
            SimpleDiffIK::Result resultOpen = SimpleDiffIK::CalculateDiffIK(targetOpen, rns, tcp);
            rns->setJointValues(initJV);
            SimpleDiffIK::Result resultPreshaped = SimpleDiffIK::CalculateDiffIK(targetPreshaped, rns, tcp);

            Eigen::Vector3f handForward = math::Helpers::TransformDirection(targetPreshaped, handHelper.getHandForwardVector());
            Eigen::Vector3f dir = math::Helpers::VectorRejection(handForward, Eigen::Vector3f::UnitZ()).normalized();

            float minimumJointLimitMargin = std::min(resultOpen.minimumJointLimitMargin, resultPreshaped.minimumJointLimitMargin);

            DrawColor color;
            if (resultOpen.reached && resultPreshaped.reached)
            {
                color = DrawColor {0, 1, minimumJointLimitMargin, 1};
            }
            else
            {
                color = DrawColor {1, 0, 0, 1};
            }
            debugDrawerHelper.drawLine("line_" + std::to_string(i), p, p + dir * (40 + minimumJointLimitMargin * 20), 5, color);
            i++;
        }
    }



    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());



        TimeUtil::SleepMS(10);
    }
}

//void VisualizeReachability::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void VisualizeReachability::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr VisualizeReachability::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new VisualizeReachability(stateData));
}
