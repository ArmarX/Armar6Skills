/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6VisualGraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LookAtSelectedCandidate.h"

#include <RobotAPI/libraries/GraspingUtility/GraspCandidateHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>
#include <VirtualRobot/math/Helpers.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6VisualGraspingGroup;

// DO NOT EDIT NEXT LINE
LookAtSelectedCandidate::SubClassRegistry LookAtSelectedCandidate::Registry(LookAtSelectedCandidate::GetName(), &LookAtSelectedCandidate::CreateInstance);



void LookAtSelectedCandidate::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void LookAtSelectedCandidate::run()
{
    VirtualRobot::RobotPtr robot = getLocalRobot();
    ::armarx::RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

    KinematicUnitHelper kinUnitHelper(getKinematicUnit());

    grasping::GraspCandidateSeq gcs = getGraspCandidateObserver()->getSelectedCandidates();
    if (gcs.size() == 0)
    {
        throw LocalException("No grasp candidate selected");
    }
    grasping::GraspCandidatePtr gc = gcs.at(0);

    Eigen::Matrix4f graspPose = GraspCandidateHelper(gc, robot).getGraspPoseInRobotRoot();

    Eigen::Vector3f graspPos = ::math::Helpers::GetPosition(graspPose);
    //Eigen::Vector3f fwd = Eigen::Vector3f::UnitY();
    //Eigen::Vector3f up = Eigen::Vector3f::UnitZ();
    //float angle = math::Helpers::Angle(graspPos, fwd, up);
    float angle = std::atan2(graspPos.y(), graspPos.x()) - M_PI / 2;
    kinUnitHelper.setJointAngles({{in.getHeadYawJoint(), angle}});

    //ARMARX_IMPORTANT << "oldGraspPose: " << math::Helpers::GetPosition(oldGraspPose).transpose();
    //ARMARX_IMPORTANT << "graspPose: " << math::Helpers::GetPosition(graspPose).transpose();
    ARMARX_IMPORTANT << "angle: " << angle;

    emitSuccess();

    //while (!isRunningTaskStopped()) // stop run function if returning true
    //{
    //    // do your calculations
    //    // synchronize robot clone to most recent state
    //    RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    //}
}

//void LookAtSelectedCandidate::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void LookAtSelectedCandidate::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr LookAtSelectedCandidate::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new LookAtSelectedCandidate(stateData));
}
