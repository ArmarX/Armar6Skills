/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6VisualGraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CalculatePlatformPose.h"

#include <armar6/skills/libraries/Armar6Tools/Armar6GraspTrajectory.h>
#include <armar6/skills/libraries/Armar6Tools/Armar6PlatformPlacement.h>
#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>
#include <VirtualRobot/MathTools.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6VisualGraspingGroup;

// DO NOT EDIT NEXT LINE
CalculatePlatformPose::SubClassRegistry CalculatePlatformPose::Registry(CalculatePlatformPose::GetName(), &CalculatePlatformPose::CreateInstance);



void CalculatePlatformPose::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

typedef std::shared_ptr<class GraspCandidatePlacement> GraspCandidatePlacementPtr;
struct GraspCandidatePlacement : public Armar6PlatformPlacement::PlacementReachability
{
    grasping::GraspCandidatePtr candiadte;
};

void CalculatePlatformPose::run()
{
    VirtualRobot::RobotPtr robot = getLocalRobot();
    ::armarx::RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

    grasping::GraspCandidateSeq gcs = getGraspCandidateObserver()->getSelectedCandidates();
    if (gcs.size() == 0)
    {
        std::vector<Vector3Ptr> targetPositions;
        out.settargetPositions(targetPositions);
        // throw LocalException("No grasp candidate selected");
        ARMARX_ERROR << "No grasp candidate selected according to CalculatePlatformPose!";
        emitFailure();
        return;
    }


    std::vector<GraspCandidatePlacementPtr> gcPlacements;

    for (grasping::GraspCandidatePtr gc : gcs)
    {

        ARMARX_IMPORTANT << "Using selected grasp candidate at " << GraspCandidateHelper(gc, robot).getGraspPoseInRobotRoot();
        RobotNameHelperPtr rnh = getRobotNameHelper();
        RobotNameHelper::RobotArm arm = rnh->getRobotArm(gc->side, robot);
        Armar6PlatformPlacement pp(arm.getKinematicChain(), arm.getTCP());

        math::Grid3DPtr grid = math::Grid3D::CreateFromCenterAndSize(Eigen::Vector3f::Zero(), Eigen::Vector3f(in.getGridSizeX(), 0, 0), in.getStepLength());

        std::vector<Armar6PlatformPlacement::PlacementReachability> prs = pp.calculateReachability(gc, robot, grid);
        size_t maxIndex = 0;
        bool found = pp.findBestPlacement(prs, maxIndex, GraspCandidateHelper(gc, robot).getGraspPositionInRobotRoot());
        //        bool found = pp.findBestPlacement(prs, maxIndex);

        if (found)
        {
            GraspCandidatePlacementPtr res(new GraspCandidatePlacement());
            res->candiadte = gc;
            Armar6PlatformPlacement::PlacementReachability maxPr = prs.at(maxIndex);
            res->reachability = maxPr.reachability;
            res->relativePose = maxPr.relativePose;
            gcPlacements.push_back(res);
        }

    }
    if (gcPlacements.size() == 0)
    {
        std::vector<Vector3Ptr> targetPositions;
        out.settargetPositions(targetPositions);
        // throw LocalException("No candidate is reachable");
        ARMARX_ERROR << "No candidate is reachable according to CalculatePlatformPose!";
        emitFailure();
        return;
    }

    GraspCandidatePlacementPtr best;
    float maxMargin = -1;
    for (GraspCandidatePlacementPtr gcp : gcPlacements)
    {
        if (gcp->reachability.minimumJointLimitMargin > maxMargin)
        {
            maxMargin = gcp->reachability.minimumJointLimitMargin;
            best = gcp;
        }
    }

    gcs.clear();
    gcs.push_back(best->candiadte);
    getGraspCandidateObserver()->setSelectedCandidates(gcs);

    Eigen::Matrix4f targetPose = robot->getGlobalPose() * best->relativePose;
    Eigen::Vector3f targetPos = math::Helpers::GetPosition(targetPose);
    Eigen::Vector3f rpy = VirtualRobot::MathTools::eigen4f2rpy(targetPose);

    Eigen::Vector3f platformTarget(targetPos(0), targetPos(1), rpy(2));

    std::vector<Vector3Ptr> targetPositions;
    targetPositions.push_back(new Vector3(platformTarget));
    out.settargetPositions(targetPositions);

    emitSuccess();

}

//void CalculatePlatformPose::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void CalculatePlatformPose::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CalculatePlatformPose::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CalculatePlatformPose(stateData));
}

