/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6VisualGraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <armar6/skills/statecharts/Armar6VisualGraspingGroup/SelectGraspNearLocation.generated.h>

namespace armarx::Armar6VisualGraspingGroup
{
    class SelectGraspNearLocation :
        public SelectGraspNearLocationGeneratedBase < SelectGraspNearLocation >
    {
    public:
        SelectGraspNearLocation(const XMLStateConstructorParams& stateData):
            XMLStateTemplate < SelectGraspNearLocation > (stateData), SelectGraspNearLocationGeneratedBase < SelectGraspNearLocation > (stateData)
        {
        }

        // inherited from StateBase
        void onEnter() override;
        void run() override;
        // void onBreak() override;
        void onExit() override;

        Eigen::Vector3f defrost(Vector3BasePtr p)
        {
            return Vector3Ptr::dynamicCast(p)->toEigen();
        }
        Eigen::Matrix4f defrost(PoseBasePtr p)
        {
            return PosePtr::dynamicCast(p)->toEigen();
        }

        bool getReachability(const grasping::GraspCandidatePtr& cnd);

        // static functions for AbstractFactory Method
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static SubClassRegistry Registry;

        // DO NOT INSERT ANY CLASS MEMBERS,
        // use stateparameters instead,
        // if classmember are neccessary nonetheless, reset them in onEnter
        static std::string ApertureTypeToString(grasping::ApertureType apertureType);
        static std::string ApproachTypeToString(grasping::ApproachType approachType);
        static std::string ObjectTypeToString(objpose::ObjectType objectType);
    private:
        float distanceXY(Eigen::Vector3f a, Eigen::Vector3f b);
        grasping::GraspCandidateSeq  filterCandidates(const grasping::GraspCandidateSeq& candidates, VirtualRobot::RobotPtr robot);
        void printCandidates(const grasping::GraspCandidateSeq& candidates, VirtualRobot::RobotPtr robot);
        grasping::GraspCandidateSeq selectCandidatesByLocation(const Eigen::Vector3f& pos, const grasping::GraspCandidateSeq& candidates);
        grasping::GraspCandidateSeq selectCandidatesByObjectName(const std::string& objectName, const grasping::GraspCandidateSeq& candidates);
        grasping::GraspCandidateSeq selectRandomCandidates(const grasping::GraspCandidateSeq& candidates);
        grasping::GraspCandidateSeq getAllCandidatesFromSameClass(const grasping::GraspCandidatePtr& cnd, const grasping::GraspCandidateSeq& candidates);
    };
}
