/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6VisualGraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ExecuteNaturalGrasp.h"

#include <RobotAPI/libraries/RobotStatechartHelpers/ForceTorqueHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/PositionControllerHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>

#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>
#include <RobotAPI/libraries/GraspingUtility/GraspCandidateHelper.h>

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <VirtualRobot/MathTools.h>
#include <VirtualRobot/math/Helpers.h>
#include <VirtualRobot/math/Line.h>
#include <VirtualRobot/math/LineStrip.h>
#include <VirtualRobot/math/LinearInterpolatedPose.h>

//#include <armar6/skills/statecharts/Armar6VisualGraspingGroup/tools/HandShapeHelper.h>

#include <armar6/skills/libraries/Armar6Tools/Armar6GraspTrajectory.h>
#include <armar6/skills/libraries/Armar6Tools/Armar6HandV2Helper.h>

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <RobotAPI/libraries/natik/CartesianNaturalPositionControllerProxy.h>


using namespace armarx;
using namespace Armar6VisualGraspingGroup;


// DO NOT EDIT NEXT LINE
ExecuteNaturalGrasp::SubClassRegistry ExecuteNaturalGrasp::Registry(ExecuteNaturalGrasp::GetName(), &ExecuteNaturalGrasp::CreateInstance);

void ExecuteNaturalGrasp::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}



void ExecuteNaturalGrasp::run()
{

    std::string side = in.getSide();
    RobotNameHelperPtr nameHelper = getRobotNameHelper();
    RobotNameHelper::Arm arm = nameHelper->getArm(side);
    VirtualRobot::RobotPtr robot = getLocalRobot();
    RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
    VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
    VirtualRobot::RobotNodePtr cla = rns->getNode(0);
    VirtualRobot::RobotNodePtr sho1 = rns->getNode(1);
    ARMARX_IMPORTANT << VAROUT(cla->getJointValue());
    cla->setJointValue(0);
    Eigen::Vector3f shoulder = sho1->getPositionInRootFrame();
    VirtualRobot::RobotNodePtr elb = rns->getNode(4);
    VirtualRobot::RobotNodePtr wri1 = rns->getNode(6);
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();
    NaturalIK::ArmJoints natArm;
    natArm.rns = rns;
    natArm.shoulder = sho1;
    natArm.elbow = elb;
    natArm.tcp = tcp;


    KinematicUnitHelper kinUnitHelper(getKinematicUnit());
    DebugDrawerHelper debugDrawerHelper(getDebugDrawerTopic(), "ExecuteNaturalGrasp", robot);
    Armar6HandV2Helper handHelper(getKinematicUnit(), getRobotUnitObserver(), side, in.getIsSimulation());

    grasping::GraspCandidateSeq candidates = getGraspCandidateObserver()->getSelectedCandidates();
    ARMARX_CHECK(candidates.size() == 1);

    grasping::GraspCandidatePtr cnd = candidates.at(0);
    GraspCandidateHelper gch(cnd, robot);

    std::string packageName = "armar6_skills";
    armarx::CMakePackageFinder finder(packageName);
    std::string dataDir = finder.getDataDir() + "/" + packageName;

    ARMARX_IMPORTANT << VAROUT(cnd->executionHints->graspTrajectoryName);
    Armar6GraspTrajectoryPtr graspTrajectory = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/" + cnd->executionHints->graspTrajectoryName + ".xml");
    ARMARX_IMPORTANT << "Read grasp trajectory, length: " << graspTrajectory->calculateLength().pos << ", duration: " << graspTrajectory->getDuration();
    handHelper.shapeHand(graspTrajectory->GetHandValues(0));


    float thumbOffset = in.getThumbOffset();


    std::vector<float> jointCosts = std::vector<float>(rns->getSize(), 1);
    jointCosts.at(0) = 4;

    RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    NaturalIK ik(side, shoulder, 1.3f);
    float upper_arm_length = (sho1->getPositionInRootFrame() - elb->getPositionInRootFrame()).norm();
    float lower_arm_length = (elb->getPositionInRootFrame() - wri1->getPositionInRootFrame()).norm()
                             + (wri1->getPositionInRootFrame() - tcp->getPositionInRootFrame()).norm();
    ik.setUpperArmLength(upper_arm_length);
    ik.setLowerArmLength(lower_arm_length);
    NJointCartesianNaturalPositionControllerConfigPtr config = CartesianNaturalPositionControllerProxy::MakeConfig(rns->getName(), elb->getName());
    config->ftName = in.getFTSensorNameMap().at(side);
    config->runCfg.jointCosts = jointCosts;
    CartesianNaturalPositionControllerProxy posCtrl(ik, natArm, getRobotUnit(), "ExecuteNaturalGrasp_" + side, config);
    posCtrl.init();

    CartesianNaturalPositionControllerProxy::WaypointConfig wpcApproach;
    wpcApproach.referencePosVel = in.getPosVelApproach();
    wpcApproach.thresholdTcpPosReached = 50;
    posCtrl.setDefaultWaypointConfig(wpcApproach);

    CartesianNaturalPositionControllerProxy::WaypointConfig wpcApproach2;
    wpcApproach2.referencePosVel = in.getPosVelApproach() / 2;
    wpcApproach2.thresholdTcpPosReached = 5;

    CartesianNaturalPositionControllerProxy::WaypointConfig wpcTop;
    wpcTop.referencePosVel = in.getPosVelGraspTop();
    wpcTop.thresholdTcpPosReached = 5;

    CartesianNaturalPositionControllerProxy::WaypointConfig wpcSide;
    wpcSide.referencePosVel = in.getPosVelGraspSide();
    wpcSide.thresholdTcpPosReached = 5;

    CartesianNaturalPositionControllerProxy::WaypointConfig wpcExecute;
    wpcExecute.referencePosVel = in.getPosVelGraspExecute();
    wpcExecute.thresholdTcpPosReached = 5;

    CartesianNaturalPositionControllerProxy::WaypointConfig wpcPlace;
    wpcPlace.referencePosVel = in.getPosVelGraspTop();
    wpcPlace.thresholdTcpPosReached = 5;


    float approachDistance = in.getApproachDistance();
    Eigen::Matrix4f graspPose = gch.getGraspPoseInRobotRoot();
    Eigen::Matrix4f prePose1 = gch.getPrePoseInRobotRoot(approachDistance);
    Eigen::Matrix4f prePose2 = gch.getPrePoseInRobotRoot(approachDistance / 2);

    ARMARX_IMPORTANT << VAROUT(graspPose(2, 3)) << VAROUT(in.getSideGraspMinHeight());

    if (gch.isSideGrasp() && graspPose(2, 3) < in.getSideGraspMinHeight())
    {
        float zOffset = in.getSideGraspMinHeight() - graspPose(2, 3);
        Eigen::Vector3f zOffsetVector = Eigen::Vector3f(0, 0, zOffset);
        ARMARX_WARNING << "Grasp z value is too low for side grasp! Z=" << graspPose(2, 3) << "; setting z = " << in.getSideGraspMinHeight();

        graspPose = math::Helpers::TranslatePose(graspPose, zOffsetVector);
        prePose1 = math::Helpers::TranslatePose(prePose1, zOffsetVector);
        prePose2 = math::Helpers::TranslatePose(prePose2, zOffsetVector);
        ARMARX_IMPORTANT << "new grasp position: " << math::Helpers::GetPosition(graspPose.transpose());
    }


    Eigen::Matrix4f placePose = in.getPlacePose()->toEigen();

    Eigen::Vector3f TopGraspingVector = in.getTopGraspingVector()->toEigen();
    Eigen::Matrix4f graspPoseTop = math::Helpers::TranslatePose(graspPose, TopGraspingVector);

    std::vector<float> nullspaceTargets = std::vector<float>(rns->getSize(), std::nanf(""));
    nullspaceTargets.at(6) = 0;
    nullspaceTargets.at(7) = 0;

    Eigen::Vector3f initPos = tcp->getPositionInRootFrame();
    if (tcp->getPositionInRootFrame().z() < in.getInitMinHeight())
    {
        initPos = in.getInitPositionMap().at(side)->toEigen();
        posCtrl.addWaypoint(posCtrl.createWaypoint(initPos, nullspaceTargets));
    }

    //math::LinearInterpolatedPose linPose(tcp->getPoseInRootFrame(), prePose1, 0, 1, true);
    //waypoints.push_back(linPose.Get(0.5f));


    math::Line l = math::Line::FromPoints(initPos, math::Helpers::Position(prePose1));
    posCtrl.addWaypoint(posCtrl.createWaypoint(l.Get(0.5f)).setNullspaceTargets(nullspaceTargets));
    posCtrl.addWaypoint(posCtrl.createWaypoint(prePose1));
    posCtrl.addWaypoint(posCtrl.createWaypoint(prePose2).setConfig(wpcApproach2));

    //IceUtil::Time start = TimeUtil::GetTime();

    PhaseType currentPhase = PhaseType::ApproachPrePose;

    IceUtil::Time graspStartTime;

    bool fakeFTwasSet = false;

    IceUtil::Time waypointReachedTime;
    bool waypointReachedTimeSet = false;

    IceUtil::Time replaceStartTime;

    Eigen::Matrix4f PlacePoseDropObjectGlobal = Eigen::Matrix4f::Identity();

    TimerHelper dropInBoxTimer;

    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        bool ikSolvable = posCtrl.update();
        if (!ikSolvable)
        {
            ARMARX_ERROR << "Natural IK could not be solved";
            emitFailure();
            break;
        }

        debugDrawerHelper.drawLine("tcpToTarget", tcp->getPositionInRootFrame(), posCtrl.getCurrentTargetPosition(), 5, DrawColor {0, 1, 0, 0});

        {
            StringVariantBaseMap cfg;
            cfg["DisableVisuOverrideTimeoutSeconds"] = new Variant(1.0f);
            getRequestableServices()->setServiceConfig("__any__GraspProvider__", cfg);
        }

        float tcpPosError = posCtrl.getTcpPositionError();
        float tcpOriError = posCtrl.getTcpOrientationError();
        float elbPosError = posCtrl.getElbPositionError();
        //ARMARX_INFO << deactivateSpam(1) << VAROUT(positionError) << " " << VAROUT(orientationError) << " " << VAROUT(cv.transpose());

        getDebugObserver()->setDebugDatafield("CartesianPositionControl", "tcpPosError", new Variant(tcpPosError));
        getDebugObserver()->setDebugDatafield("CartesianPositionControl", "tcpOriError", new Variant(tcpOriError));
        getDebugObserver()->setDebugDatafield("CartesianPositionControl", "elbPosError", new Variant(elbPosError));

        PhaseType nextPhase = currentPhase;
        bool waypointReached = posCtrl.isFinalWaypointReached();
        //bool targetNear = posHelper.isFinalTargetNear();

        FTSensorValue ftVal = posCtrl.getCurrentFTValue(true);
        FTSensorValue avgFT = posCtrl.getAverageFTValue(true);


        //const Eigen::Vector3f currentForce = ftHelper.getForce();
        //const Eigen::Vector3f currentTorque = ftHelper.getTorque();


        getDebugObserver()->setDebugDatafield("ExecuteNaturalGrasp", "force_x", new Variant(ftVal.force(0)));
        getDebugObserver()->setDebugDatafield("ExecuteNaturalGrasp", "force_y", new Variant(ftVal.force(1)));
        getDebugObserver()->setDebugDatafield("ExecuteNaturalGrasp", "force_z", new Variant(ftVal.force(2)));
        getDebugObserver()->setDebugDatafield("ExecuteNaturalGrasp", "torque_x", new Variant(ftVal.torque(0)));
        getDebugObserver()->setDebugDatafield("ExecuteNaturalGrasp", "torque_y", new Variant(ftVal.torque(1)));
        getDebugObserver()->setDebugDatafield("ExecuteNaturalGrasp", "torque_z", new Variant(ftVal.torque(2)));

        getDebugObserver()->setDebugDatafield("ExecuteNaturalGrasp", "force_avg_x", new Variant(avgFT.force(0)));
        getDebugObserver()->setDebugDatafield("ExecuteNaturalGrasp", "force_avg_y", new Variant(avgFT.force(1)));
        getDebugObserver()->setDebugDatafield("ExecuteNaturalGrasp", "force_avg_z", new Variant(avgFT.force(2)));

        Eigen::Vector3f tcpPos = tcp->getPositionInRootFrame();
        Eigen::Matrix4f tcpPose = tcp->getPoseInRootFrame();
        if (in.getIsSimulation() &&
            (currentPhase == PhaseType::TrajectoryGrasp || currentPhase == PhaseType::Place))
        {
            float diff = tcpPos(2) - in.getSimulationForceHeightThreshold();
            if (diff < 0)
            {
                ARMARX_IMPORTANT << "Simulating force exceeded";
                FTSensorValue fakeFT;
                fakeFT.force = Eigen::Vector3f(1, 1, 1) * diff;
                fakeFT.torque = Eigen::Vector3f::Zero();
                posCtrl.getInternalController()->setFakeFTValue(fakeFT);
                fakeFTwasSet = true;
            }
            else if (fakeFTwasSet)
            {
                fakeFTwasSet = false;
                posCtrl.getInternalController()->clearFakeFTValue();
            }
        }


        //bool forceExceeded = posCtrl.getInternalController()->isAtForceLimit();

        bool forceExceeded = ftVal.force.norm() > in.getForceThreshold();

        if (forceExceeded)
        {
            ARMARX_IMPORTANT << "Force exceeded. current force: " << posCtrl.getInternalController()->getCurrentFTValue().force.transpose();
        }

        if (currentPhase == PhaseType::ApproachPrePose)
        {
            getMessageDisplay()->setMessage("Approaching Pre Pose", posCtrl.getStatusText());
            if (waypointReached && !waypointReachedTimeSet)
            {
                waypointReachedTime = TimeUtil::GetTime();
                waypointReachedTimeSet = true;
            }
            if (waypointReached)
            {
                posCtrl.useCurrentFTasOffset();
            }
            if (waypointReached && (TimeUtil::GetTime() - waypointReachedTime).toSecondsDouble() > 0.5)
            {
                waypointReachedTimeSet = false;
                posCtrl.clearWaypoints();
                ARMARX_IMPORTANT << "PrePose reached";
                if (gch.isTopGrasp())
                {
                    ARMARX_IMPORTANT << "graspType = Top: Adding top grasping vector";
                    //posCtrl.enableFTLimit(in.getForceThreshold(), -1, true);
                    posCtrl.addWaypoint(posCtrl.createWaypoint(graspPoseTop).setConfig(wpcTop));
                }
                else
                {
                    posCtrl.addWaypoint(posCtrl.createWaypoint(graspPose).setConfig(wpcSide));
                }
                posCtrl.getInternalController()->setNullspaceControlEnabled(false);
                nextPhase = PhaseType::ApproachObject;
            }
        }
        if (currentPhase == PhaseType::ApproachObject)
        {
            getMessageDisplay()->setMessage("Approaching Object", posCtrl.getStatusText());
            if (forceExceeded || waypointReached)
            {
                posCtrl.stopClear();
                posCtrl.addWaypoint(posCtrl.createWaypoint(tcpPose).setConfig(wpcExecute));
                posCtrl.update();

                graspStartTime = TimeUtil::GetTime();

                nextPhase = PhaseType::TrajectoryGrasp;

                Eigen::Matrix4f startPose = graspTrajectory->getStartPose();
                Eigen::Vector3f currentHandForward = ::math::Helpers::TransformDirection(tcpPose, handHelper.getHandForwardVector());
                Eigen::Vector3f trajHandForward = ::math::Helpers::TransformDirection(startPose, handHelper.getHandForwardVector());
                Eigen::Vector3f up(0, 0, 1);

                float angle = ::math::Helpers::Angle(currentHandForward, trajHandForward, up);
                Eigen::AngleAxisf aa(angle, up);

                Eigen::Matrix4f transform = ::math::Helpers::CreateTranslationRotationTranslationPose(-math::Helpers::GetPosition(startPose), aa.toRotationMatrix(), math::Helpers::GetPosition(tcpPose));
                graspTrajectory = graspTrajectory->getTransformed(transform);

            }
        }
        if (currentPhase == PhaseType::TrajectoryGrasp)
        {
            IceUtil::Time now = TimeUtil::GetTime();
            float t = (now - graspStartTime).toSecondsDouble();
            float duration = graspTrajectory->getDuration();
            {
                std::stringstream ss;
                ss.precision(2);
                ss << "Force: " << ftVal.force.norm() << ", " << t;
                getMessageDisplay()->setMessage("Grasping", ss.str());
            }
            if (t > duration)
            {
                t = graspTrajectory->getDuration();
                nextPhase = PhaseType::Lift;
                Eigen::Matrix4f target = tcp->getPoseInRootFrame();
                posCtrl.getInternalController()->clearFeedForwardVelocity();
                posCtrl.getInternalController()->setNullspaceControlEnabled(true);
                posCtrl.clearWaypoints();
                posCtrl.addWaypoint(posCtrl.createWaypoint(math::Helpers::TranslatePose(target, in.getLiftVector()->toEigen())));
            }
            else
            {
                Eigen::Matrix4f target = graspTrajectory->GetPose(t);
                Eigen::Vector6f ffVel = graspTrajectory->GetTcpDerivative(t);
                posCtrl.getInternalController()->setTargetFeedForward(target, posCtrl.getCurrentElbowTargetPosition(), true, ffVel);
                Eigen::Vector3f handShape = graspTrajectory->GetHandValues(t);
                handShape(1) = handShape(1) + thumbOffset;
                handHelper.shapeHand(handShape);
            }
        }
        if (currentPhase != PhaseType::Lift && nextPhase == PhaseType::Lift)
        {
            DebugDrawerHelper ddhSelectedGrasps(getDebugDrawerTopic(), "SelectedGrasps", robot);
            ddhSelectedGrasps.clearLayer();
        }
        if (currentPhase == PhaseType::Lift)
        {
            {
                std::stringstream ss;
                ss << "Force: " << ftVal.force.norm() << ", " << posCtrl.getStatusText();
                getMessageDisplay()->setMessage("Lifting", ss.str());
            }
            if (in.getPlaceAfterLift() && waypointReached)
            {
                if (in.getPlaceStrategy() == "DropIntoBox")
                {


                    {
                        Eigen::Matrix4f platformTargetPose = robot->getGlobalPose() * math::Helpers::CreatePose(Eigen::Vector3f(0, -500, 0), Eigen::Matrix3f::Identity());
                        Eigen::Vector3f platformTargetPos = math::Helpers::GetPosition(platformTargetPose);
                        Eigen::Vector3f rpy = VirtualRobot::MathTools::eigen4f2rpy(platformTargetPose);
                        Eigen::Vector3f platformTarget(platformTargetPos(0), platformTargetPos(1), rpy(2));
                        getPlatformUnit()->moveTo(platformTarget(0), platformTarget(1), platformTarget(2), 200, 0.01f);
                    }


                    Eigen::Vector3f placePlatformVec = in.getPlacePlatformPose()->toEigen();
                    Eigen::Matrix3f platPlatformOri = Eigen::AngleAxisf(placePlatformVec(2), Eigen::Vector3f::UnitZ()).toRotationMatrix();
                    Eigen::Matrix4f placePlatformPose = math::Helpers::CreatePose(Eigen::Vector3f(placePlatformVec(0), placePlatformVec(1), 0), platPlatformOri);
                    Eigen::Matrix4f PlacePoseDropObject = in.getPlacePoseDropObjectPrePose()->toEigen();
                    PlacePoseDropObjectGlobal = placePlatformPose * PlacePoseDropObject;
                    nextPhase = PhaseType::MovePlatformForDrop;
                    replaceStartTime = TimeUtil::GetTime();
                }
                else
                {
                    posCtrl.addWaypoint(posCtrl.createWaypoint(placePose));
                    nextPhase = PhaseType::Replace;
                }

            }
            if (!in.getPlaceAfterLift() && waypointReached)
            {
                posCtrl.stopClear();
                emitSuccess();
                getMessageDisplay()->setMessage("", "");
                break;
            }
        }
        if (currentPhase == PhaseType::MovePlatformForDrop)
        {
            if ((TimeUtil::GetTime() - replaceStartTime).toSecondsDouble() > 3)
            {
                Eigen::Vector3f placePlatformPose = in.getPlacePlatformPose()->toEigen();
                getPlatformUnit()->setMaxVelocities(300, 0.5f);
                getPlatformUnit()->moveTo(placePlatformPose(0), placePlatformPose(1), placePlatformPose(2), 200, 0.01f);

                Eigen::Matrix4f PlacePoseDropObject = in.getPlacePoseDropObjectPrePose()->toEigen();
                posCtrl.clearWaypoints();
                posCtrl.addWaypoint(posCtrl.createWaypoint(PlacePoseDropObject));

                replaceStartTime = TimeUtil::GetTime();
                nextPhase = PhaseType::ReplaceForDropPrepose;
            }
        }
        if (currentPhase == PhaseType::ReplaceForDropPrepose)
        {
            Eigen::Vector3f diff = math::Helpers::GetPosition(PlacePoseDropObjectGlobal) - tcp->getGlobalPosition();
            ARMARX_IMPORTANT << VAROUT(diff.norm()) << VAROUT(diff.transpose());
            //if (waypointReached && (TimeUtil::GetTime() - replaceStartTime).toSecondsDouble() > 9)
            if (diff.norm() < 30)
            {
                dropInBoxTimer.start();
            }
            if (diff.norm() < 30 && dropInBoxTimer.elapsed() > 1)
            {
                getPlatformUnit()->move(0, 0, 0);
            }
            if (diff.norm() < 30 && dropInBoxTimer.elapsed() > 2)
            {
                nextPhase = PhaseType::DropObjectIntoBox;
                posCtrl.clearWaypoints();
                posCtrl.addWaypoint(posCtrl.createWaypoint(in.getPlacePoseDropObjectDownPose()->toEigen()));
                posCtrl.useCurrentFTasOffset();
            }
        }
        if (currentPhase == PhaseType::DropObjectIntoBox)
        {
            if (waypointReached || forceExceeded)
            {
                handHelper.shapeHand(0, 0);
                posCtrl.stopClear();
                posCtrl.addWaypoint(posCtrl.createWaypoint(math::Helpers::TranslatePose(tcp->getPoseInRootFrame(), Eigen::Vector3f(0, 0, 100))));
                posCtrl.addWaypoint(posCtrl.createWaypoint(in.getPlaceFinalPose()->toEigen()));
                nextPhase = PhaseType::RetractAfterPlace;
            }
        }
        if (currentPhase == PhaseType::Replace)
        {
            getMessageDisplay()->setMessage("Replacing", "");
            if (waypointReached)
            {
                posCtrl.useCurrentFTasOffset();
                //posCtrl.enableFTLimit(in.getForceThreshold(), -1, true);
                posCtrl.getInternalController()->setNullspaceControlEnabled(false);
                posCtrl.addWaypoint(posCtrl.createWaypoint(math::Helpers::TranslatePose(placePose, in.getPlaceVector()->toEigen())).setConfig(wpcPlace));
                nextPhase = PhaseType::Place;
            }
        }
        if (currentPhase == PhaseType::Place)
        {
            {
                std::stringstream ss;
                ss << "Force: " << ftVal.force.norm();
                getMessageDisplay()->setMessage("Placing", ss.str());
            }
            if (forceExceeded)
            {
                posCtrl.stopClear();
                posCtrl.getInternalController()->setNullspaceControlEnabled(true);
                handHelper.shapeHand(0, 0);
                posCtrl.addWaypoint(posCtrl.createWaypoint(math::Helpers::TranslatePose(placePose, Eigen::Vector3f(0, 0, 100))));
                //Eigen::Vector3f RetractAfterPlacePos = math::Helpers::Position(in.getRetractAfterPlacePose()->toEigen());
                posCtrl.addWaypoint(posCtrl.createWaypoint(in.getRetractAfterPlacePose()->toEigen()));
                //posHelper.addWaypoint(in.getFinalPrePose()->toEigen());
                //posHelper.addWaypoint(in.getFinalPose()->toEigen());
                nextPhase = PhaseType::RetractAfterPlace;
                kinUnitHelper.setJointAngles(in.getLookAtObjectJointAngles());

            }
            if (waypointReached)
            {
                posCtrl.stopClear();
                emitFailure();
                break;
            }
        }
        if (currentPhase == PhaseType::RetractAfterPlace)
        {
            getMessageDisplay()->setMessage("Move back", posCtrl.getStatusText());
            if (waypointReached)
            {
                posCtrl.stopClear();
                emitSuccess();
                getMessageDisplay()->setMessage("", "");
                break;
            }
        }

        currentPhase = nextPhase;

        TimeUtil::SleepMS(10);
    }

    if (in.getStopOnExit())
    {
        posCtrl.stopClear();
    }
    posCtrl.cleanup();
    debugDrawerHelper.clearLayer();
    getDebugDrawerTopic()->clearLayer(in.getGraspProviderVisuName());
}

//void ExecuteNaturalGrasp::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ExecuteNaturalGrasp::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ExecuteNaturalGrasp::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ExecuteNaturalGrasp(stateData));
}


void ExecuteNaturalGrasp::TimerHelper::start()
{
    if (!started)
    {
        started = true;
        startTime = TimeUtil::GetTime();
    }
}

float ExecuteNaturalGrasp::TimerHelper::elapsed()
{
    if (!started)
    {
        return -1;
    }
    return (TimeUtil::GetTime() - startTime).toSecondsDouble();
}

void ExecuteNaturalGrasp::TimerHelper::reset()
{
    started = false;
}
