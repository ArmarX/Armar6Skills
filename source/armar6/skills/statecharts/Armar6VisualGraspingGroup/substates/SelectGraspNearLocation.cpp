/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6VisualGraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SelectGraspNearLocation.h"

#include <armar6/skills/libraries/Armar6Tools/Armar6GraspTrajectory.h>
#include <armar6/skills/libraries/Armar6Tools/Armar6HandV2Helper.h>

#include <RobotAPI/components/ArViz/Client/Client.h>
#include <RobotAPI/components/ArViz/Client/ScopedClient.h>
#include <RobotAPI/components/ArViz/Client/elements/RobotHand.h>
#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>

#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/PositionControllerHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>
#include <RobotAPI/libraries/GraspingUtility/GraspCandidateHelper.h>

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/time/TimeUtil.h>

#include <SimoxUtility/math/pose/pose.h>
#include <SimoxUtility/math/pose/invert.h>

#include <VirtualRobot/math/Helpers.h>
#include <VirtualRobot/MathTools.h>

#include <SimoxUtility/algorithm/string/string_tools.h>
#include <SimoxUtility/math/pose/transform.h>

#include <algorithm>
#include <random>

//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6VisualGraspingGroup;

// DO NOT EDIT NEXT LINE
SelectGraspNearLocation::SubClassRegistry SelectGraspNearLocation::Registry(SelectGraspNearLocation::GetName(), &SelectGraspNearLocation::CreateInstance);



void SelectGraspNearLocation::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

float SelectGraspNearLocation::distanceXY(Eigen::Vector3f a, Eigen::Vector3f b)
{
    return (Eigen::Vector2f(a(0), a(1)) - Eigen::Vector2f(b(0), b(1))).norm();
}



std::string SelectGraspNearLocation::ApproachTypeToString(grasping::ApproachType approachType)
{
    switch (approachType)
    {
        case grasping::ApproachType::TopApproach:
            return "Top";
        case grasping::ApproachType::SideApproach:
            return "Side";
        default:
            throw LocalException("Unknown GraspType");
    }
}

std::string SelectGraspNearLocation::ObjectTypeToString(objpose::ObjectType objectType)
{
    switch (objectType)
    {
        case objpose::ObjectType::KnownObject:
            return "KnownObject";
        case objpose::ObjectType::UnknownObject:
            return "UnknownObject";
        case objpose::ObjectType::AnyObject:
            return "AnyObject";
    }
    throw LocalException("Unsupported object type.");
}

std::string SelectGraspNearLocation::ApertureTypeToString(grasping::ApertureType apertureType)
{
    switch (apertureType)
    {
        case grasping::ApertureType::OpenAperture:
            return "Open";
        case grasping::ApertureType::PreshapedAperture:
            return "Preshaped";
        default:
            throw LocalException("Unknown GraspType");
    }
}

void SelectGraspNearLocation::printCandidates(const grasping::GraspCandidateSeq& candidates, VirtualRobot::RobotPtr robot)
{
    std::stringstream ss;
    for (const grasping::GraspCandidatePtr& cnd : candidates)
    {
        ss << "GraspPosition: (" << GraspCandidateHelper(cnd, robot).getGraspPositionInRobotRoot() << ")";
        if (cnd->sourceInfo)
        {
            ss << " ObjectName: " << cnd->sourceInfo->referenceObjectName;
        }
        ss << " Type: " << cnd->side;
        if (cnd->executionHints)
        {
            std::string preshapeStr = ApertureTypeToString(cnd->executionHints->preshape);
            std::string graspTypeStr = ApproachTypeToString(cnd->executionHints->approach);
            ss << graspTypeStr << preshapeStr;
        }
        if (cnd->reachabilityInfo)
        {
            ss << " Reachable: "  <<  cnd->reachabilityInfo->reachable;

        }
        ss << "\n";
    }
    ARMARX_IMPORTANT << "Grasp Candidates (" << candidates.size() << ")\n" << ss.str();
}

grasping::GraspCandidateSeq SelectGraspNearLocation::selectCandidatesByLocation(const Eigen::Vector3f& pos, const grasping::GraspCandidateSeq& candidates)
{
    float minDist = 1e10f;
    grasping::GraspCandidatePtr selectedCandidate;
    ARMARX_IMPORTANT << "Looking for object near " << pos.transpose();
    for (const grasping::GraspCandidatePtr& cnd : candidates)
    {
        Eigen::Vector3f center = defrost(cnd->sourceInfo->bbox->center);
        ARMARX_IMPORTANT << VAROUT(center);
        float dist = distanceXY(center, pos);
        bool acceptable = getReachability(cnd) || !in.getRequireReachable();
        if (dist < minDist && acceptable)
        {
            minDist = dist;
            selectedCandidate = cnd;
        }
    }
    if (!selectedCandidate)
    {
        return grasping::GraspCandidateSeq();
    }
    if (in.getRequireReachable() || in.getSelectSingleGrasp())
    {
        return grasping::GraspCandidateSeq {selectedCandidate};
    }
    else
    {
        return getAllCandidatesFromSameClass(selectedCandidate, candidates);
    }
}

grasping::GraspCandidateSeq SelectGraspNearLocation::selectCandidatesByObjectName(const std::string& objectName, const grasping::GraspCandidateSeq& candidates)
{
    ARMARX_IMPORTANT << "Looking for " << objectName;
    grasping::GraspCandidateSeq res;
    for (const grasping::GraspCandidatePtr& cnd : candidates)
    {
        std::string cndObjName = cnd->sourceInfo->referenceObjectName;
        bool acceptable = getReachability(cnd) || !in.getRequireReachable();
        if (simox::alg::to_lower(cndObjName) == simox::alg::to_lower(objectName) && acceptable)
        {
            if (in.getSelectSingleGrasp())
            {
                return {cnd};
            }
            else
            {
                res.push_back(cnd);
            }
        }
    }
    return res;
}

grasping::GraspCandidateSeq SelectGraspNearLocation::filterCandidates(const grasping::GraspCandidateSeq& candidates, VirtualRobot::RobotPtr robot)
{
    grasping::GraspCandidateSeq filteredCandidates;
    std::copy_if(candidates.begin(), candidates.end(), std::back_inserter(filteredCandidates), [this, &robot](const grasping::GraspCandidatePtr & c)
    {
        // ToDo: This either returns true, or segfaults. The if should probably check whether execution hints
        // are NOT there, then do some maths. As this does not seem to occur currently, we leave it until further testing.
        // This should be thought about and fixed accordingly.
        if (c->executionHints)
        {
            return true;
        }
        ARMARX_CHECK(c->executionHints) << "The next line would cause a segfault! (Ask Markus, who wrote this code.)";
        switch (c->executionHints->approach)
        {
            case grasping::ApproachType::AnyApproach:
                return true;
            case grasping::ApproachType::TopApproach:
                return true;
            case grasping::ApproachType::SideApproach:
                Eigen::Matrix4f graspPose = GraspCandidateHelper(c, robot).getGraspPoseInRobotRoot();
                Eigen::Vector3f rpy = VirtualRobot::MathTools::eigen4f2rpy(graspPose);
                return std::fabs(rpy.y()) < 1.2f;
        }
        return true;
    });

    return filteredCandidates;
}

grasping::GraspCandidateSeq SelectGraspNearLocation::selectRandomCandidates(const grasping::GraspCandidateSeq& candidates)
{
    grasping::GraspCandidateSeq filteredCandidates;
    if (in.getRequireReachable())
    {
        std::copy_if(candidates.begin(), candidates.end(), std::back_inserter(filteredCandidates), [this](const grasping::GraspCandidatePtr & c)
        {
            return getReachability(c);
        });
    }
    else
    {
        filteredCandidates = candidates;
    }

    grasping::GraspCandidateSeq selectedCandidates;
    std::sample(
        filteredCandidates.begin(),
        filteredCandidates.end(),
        std::back_inserter(selectedCandidates),
        1,
        std::mt19937{std::random_device{}()}
    );

    if (selectedCandidates.empty())
    {
        return grasping::GraspCandidateSeq();
    }
    if (in.getRequireReachable() || in.getSelectSingleGrasp())
    {
        return selectedCandidates;
    }
    else
    {
        return getAllCandidatesFromSameClass(selectedCandidates.front(), candidates);
    }
}

grasping::GraspCandidateSeq SelectGraspNearLocation::getAllCandidatesFromSameClass(const grasping::GraspCandidatePtr& cnd, const grasping::GraspCandidateSeq& candidates)
{
    grasping::GraspCandidateSeq res;
    for (const  grasping::GraspCandidatePtr& c : candidates)
    {
        bool matches = (c->groupNr >= 0 && c->groupNr == cnd->groupNr && c->providerName != "" && c->providerName == cnd->providerName);
        if (matches)
        {
            res.push_back(c);
        }
    }
    return res;
}

#define ENABLE_DEBUG_DRAWER 0

void SelectGraspNearLocation::run()
{
    RobotNameHelperPtr nameHelper = getRobotNameHelper();
    VirtualRobot::RobotPtr robot = getLocalRobot();
    RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    const Eigen::Matrix4f currentGlobalRobotPose = robot->getGlobalPose();

#if ENABLE_DEBUG_DRAWER
    DebugDrawerHelper debugDrawerHelper(getDebugDrawerTopic(), "SelectGraspNearLocation", robot);
    DebugDrawerHelper ddhSelectedGrasps(getDebugDrawerTopic(), "SelectedGrasps", robot);
    ddhSelectedGrasps.clearLayer();
#endif

    viz::Client arviz = viz::Client::createFromTopic("Armar6 Grasping Statecharts", getArvizTopic());
    viz::Layer layerSelectedGrasps = arviz.layer("SelectedGrasps");

    viz::ScopedClient arvizDebug = viz::Client::createFromTopic("Armar6 Grasping Statecharts | Debug", getArvizTopic());
    viz::Layer layerDebug = arvizDebug.layer("Debug SelectGraspNearLocation");
    viz::Layer layerDebugGraspPose = arvizDebug.layer("GraspPose");
    viz::Layer layerDebugTcpToTarget = arvizDebug.layer("TCP To Target");

    arviz.commit({layerSelectedGrasps});
    arvizDebug.commit({layerDebug, layerDebugGraspPose, layerDebugTcpToTarget});

    KinematicUnitHelper kinUnitHelper(getKinematicUnit());
    if (in.getMoveHead())
    {
        kinUnitHelper.setJointAngles(in.getHeadJointAngles());
    }
    TimeUtil::Sleep(1);

    grasping::GraspCandidateSeq candidates;
    std::string selectionMode = in.getSelectionMode();

    if (selectionMode != "UseSelectedCandidate")
    {
        objpose::ObjectType expectedObjectType = selectionMode == "ObjectName" ? objpose::KnownObject : objpose::UnknownObject;
        grasping::InfoMap infoMap = getGraspCandidateObserver()->getAvailableProvidersWithInfo();

        std::vector<std::string> providerNames;
        ARMARX_INFO << "Looking for providers with object type = " << ObjectTypeToString(expectedObjectType) << ", available providers: " << getGraspCandidateObserver()->getAvailableProviderNames();
        for (const auto& pair : infoMap)
        {
            ARMARX_INFO << "found provider '" << pair.first << "' with object type = " << ObjectTypeToString(pair.second->objectType);
            if (pair.second->objectType == expectedObjectType)
            {
                providerNames.push_back(pair.first);
            }
        }
        //std::vector<std::string> providerNames; // = getGraspCandidateObserver()->getAvailableProviderNames();

        if (providerNames.size() == 0)
        {
            throw LocalException("No providers available!");
        }
        if (providerNames.size() != 1)
        {
            throw LocalException("Only one provider is suppoted so far! running providers: ") << providerNames;
        }

        std::string name = providerNames.at(0);

        int initialPorcessCount = getGraspCandidateObserver()->getUpdateCounterByProvider(name);
        int requiredProcessCount = initialPorcessCount + 2;

        IceUtil::Time start = TimeUtil::GetTime();
        while (true)
        {
            int currentPorcessCount = getGraspCandidateObserver()->getUpdateCounterByProvider(name);
            if (currentPorcessCount >= requiredProcessCount)
            {
                break;
            }
            if (isRunningTaskStopped())
            {
                throw LocalException("Stopped");
            }
            IceUtil::Time now = TimeUtil::GetTime();
            if ((now - start).toSecondsDouble() > in.getGraspProviderTimeout())
            {
                throw LocalException("Timeout exceeded while waiting for updates from grasp provider: ") << name;
            }

            ARMARX_INFO << "Waiting for fresh localization. Current count: " << currentPorcessCount << ", required: " << requiredProcessCount;
            TimeUtil::SleepMS(100);

        }

        //grasping::CandidateFilterConditionPtr filter = new grasping::CandidateFilterCondition();
        candidates = getGraspCandidateObserver()->getCandidatesByProvider(name);
    }


#if ENABLE_DEBUG_DRAWER
    debugDrawerHelper.clearLayer();
#endif

    printCandidates(candidates, robot);

    candidates = filterCandidates(candidates, robot);

    printCandidates(candidates, robot);


    grasping::GraspCandidateSeq selectedCandidates;
    if (selectionMode == "Position")
    {
        Eigen::Vector3f ReferenceLocation = in.getReferenceLocation()->toEigen();
        selectedCandidates = selectCandidatesByLocation(ReferenceLocation, candidates);
    }
    else if (selectionMode == "ObjectName")
    {
        std::string objectName = in.getObjectName();
        selectedCandidates = selectCandidatesByObjectName(objectName, candidates);
    }
    else if (selectionMode == "UseSelectedCandidate")
    {
        selectedCandidates = getGraspCandidateObserver()->getSelectedCandidates();
    }
    else if (selectionMode == "Random")
    {
        selectedCandidates = selectRandomCandidates(candidates);
    }
    else
    {
        throw LocalException("Unknwon selection mode ") << selectionMode;
    }

    if (selectedCandidates.size() == 0)
    {
        getGraspCandidateObserver()->setSelectedCandidates(selectedCandidates);
        emitNoCandidatesFound();
        return;
    }


    TimeUtil::SleepMS(100);

    for (size_t i = 0; i < selectedCandidates.size(); i++)
    {
        grasping::GraspCandidatePtr gc = selectedCandidates.at(i);
        std::string side = gc->side;
        RobotNameHelper::RobotArm arm = getRobotNameHelper()->getRobotArm(side, robot);
        Eigen::Matrix4f graspPose = GraspCandidateHelper(gc, robot).getGraspPoseInRobotRoot();

        // Choose appropriate colors depending on reachibility (default is gray).
        // DrawColor debugDrawerColor { 128, 128, 128, 96 };
        simox::Color simoxColor = simox::Color::gray(128, 96);
        if (gc->reachabilityInfo)
        {
            if (gc->reachabilityInfo->reachable)
            {
                simoxColor = simox::Color::green();
                // debugDrawerColor = DebugDrawerHelper::Colors::Green;
            }
        }
        else
        {
            ARMARX_WARNING << "Reachability information not available in grasp candidate " << i << ".";
        }

#if ENABLE_DEBUG_DRAWER
        Armar6GraspTrajectoryPtr graspTrajectory = Armar6GraspTrajectory::ReadFromFile(gc);
        Armar6HandV2Joints handJoints(side, true);
        handJoints.drawHand(debugDrawerHelper, "SelectedGrasp_" + std::to_string(i),
                            arm, graspPose, graspTrajectory->GetHandValues(0), debugDrawerColor);
#endif
        layerSelectedGrasps.add(viz::RobotHand("SelectedGrasp_" + std::to_string(i))
                                .fileByArm(arm).pose(fromIce(gc->robotPose) * graspPose * arm.getTcp2HandRootTransform())
                                .overrideColor(simoxColor));
    }
    arviz.commit(layerSelectedGrasps);


    getGraspCandidateObserver()->setSelectedCandidates(selectedCandidates);
    if (selectedCandidates.size() == 1)
    {
        grasping::GraspCandidatePtr selectedCandidate = selectedCandidates.at(0);


        std::string side = selectedCandidate->side;

        std::string initPositionMode = in.getInitPositionMode();

        if (initPositionMode == "Joints")
        {
            std::map<std::string, float> initialJointAngles = in.getInitialJointAnglesMap().at(side)->toStdMap<float>();
            kinUnitHelper.setJointAngles(initialJointAngles);
            TimeUtil::Sleep(3);
        }
        else if (initPositionMode == "Cartesian")
        {
            RobotNameHelper::Arm arm = nameHelper->getArm(side);
            RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
            VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();
            VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(getRobotUnit(), arm.getKinematicChain(), in.getCartesianVelocityControllerNameMap().at(side)));
            Eigen::Matrix4f target = in.getInitialPoseMap().at(side)->toEigen();
            PositionControllerHelper posHelper(tcp, velHelper, target);
            posHelper.readConfig(in.getControllerConfig());

            while (!isRunningTaskStopped())
            {
                RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
                posHelper.update();

#if ENABLE_DEBUG_DRAWER
                debugDrawerHelper.drawLine("tcpToTarget", tcp->getPositionInRootFrame(), posHelper.getCurrentTargetPosition(), 5, DrawColor {0, 1, 0, 0});
#endif
                {
                    layerDebugTcpToTarget.clear();
                    layerDebugTcpToTarget.add(viz::Cylinder("tcpToTarget").fromTo(
                                                  simox::math::transform_position(currentGlobalRobotPose, tcp->getPositionInRootFrame()),
                                                  simox::math::transform_position(currentGlobalRobotPose, posHelper.getCurrentTargetPosition()))
                                              .radius(5).color(simox::Color::green()));
                    arvizDebug.commit(layerDebugTcpToTarget);
                }

                if (posHelper.isFinalTargetNear())
                {
                    break;
                }
                TimeUtil::SleepMS(10);
            }

#if ENABLE_DEBUG_DRAWER
            debugDrawerHelper.removeElement("tcpToTarget", DebugDrawerHelper::DrawElementType::Line);
#endif
            {
                layerDebugTcpToTarget.clear();
                arvizDebug.commit(layerDebugTcpToTarget);
            }

            posHelper.immediateHardStop();
            velHelper->cleanup();
        }
        else if (initPositionMode == "None")
        {
            ARMARX_IMPORTANT << "Skipping goto init position, initPositionMode = None";
        }
        else
        {
            throw LocalException("Unsupported initPositionMode: ") << initPositionMode;
        }

        //debugDrawerHelper.clearLayer();
        //TimeUtil::SleepMS(10);

        // This internally applies the original robot pose, then substracts the current robot pose.
        const Eigen::Matrix4f currentGraspPoseRobot = GraspCandidateHelper(selectedCandidate, robot).getGraspPoseInRobotRoot();

        const std::string preshapeStr = ApertureTypeToString(selectedCandidate->executionHints->preshape);
        const std::string graspTypeStr = ApproachTypeToString(selectedCandidate->executionHints->approach);
        out.setGraspType(graspTypeStr);
        out.setApproachVector(selectedCandidate->approachVector);
        out.setFingerPreshape(in.getFingerPreshapeMap().at(preshapeStr));
        out.setGraspPose(new Pose(currentGraspPoseRobot));
        out.setOffsetDuringGrasp(in.getOffsetDuringGraspMap().at(graspTypeStr + preshapeStr));
        out.setSide(side);
        out.setCartesianVelocityControllerName(in.getCartesianVelocityControllerNameMap().at(side));
        out.setPreApproachPosition(in.getPreApproachPositionMap().at(side));
        out.setGraspTrajectoryName(side + graspTypeStr + preshapeStr);
        if (selectedCandidate->sourceInfo)
        {
            out.setReferenceObjectName(selectedCandidate->sourceInfo->referenceObjectName);
        }
        else
        {
            out.setReferenceObjectName("");
        }

        {
            // For debug visu and output.
            const Eigen::Matrix4f originalGraspPoseRobot = fromIce(selectedCandidate->graspPose);
            const Eigen::Matrix4f originalRobotPose = fromIce(selectedCandidate->robotPose);
            const Eigen::Matrix4f originalGraspPoseGlobal = GraspCandidateHelper(selectedCandidate, robot).getGraspPoseInGlobal();
            const Eigen::Matrix4f currentGraspPoseGlobal = currentGlobalRobotPose * currentGraspPoseRobot;

            ARMARX_INFO << "currentGraspPoseRobot: \n" << currentGraspPoseRobot << "\n"
                        << "originalGraspPoseRobot: \n" << currentGraspPoseRobot << "\n\n"
                        << "currentGraspPoseGlobal: \n" << currentGraspPoseGlobal << "\n"
                        << "originalGraspPoseGlobal: \n" << currentGraspPoseGlobal << "\n"
                        ;

#if ENABLE_DEBUG_DRAWER
            debugDrawerHelper.drawBox("graspPose", currentGraspPoseRobot, 50, DebugDrawerHelper::Colors::Blue);
            //        debugDrawerHelper.drawLine("approachvector", math::Helpers::GetPosition(graspPoseInRobot), math::Helpers::GetPosition(graspPoseInRobot) + defrost(selectedCandidate->approachVector) * 200);
#endif

            layerDebugGraspPose.add(viz::Box("Grasp Position").pose(currentGraspPoseGlobal).size(50).color(simox::Color::blue()));
            layerDebugGraspPose.add(viz::Pose("Grasp Pose").pose(currentGraspPoseGlobal).scale(2));

            RobotNameHelper::RobotArm arm = getRobotNameHelper()->getRobotArm(selectedCandidate->side, robot);
            layerDebug.add(
                viz::RobotHand("Selected grasp with updated pose")
                .fileByArm(arm).pose(currentGraspPoseGlobal * arm.getTcp2HandRootTransform())
                .overrideColor(simox::Color::blue(255)));

            layerDebug.add(
                viz::Arrow("OriginalRobotPose").fromTo(simox::math::position(originalRobotPose) + 3000 * Eigen::Vector3f::UnitZ(),
                        simox::math::position(originalRobotPose) + 2000 * Eigen::Vector3f::UnitZ())
                .width(50).color(simox::Color::green()));
            layerDebug.add(
                viz::Arrow("CurrentRobotPose").fromTo(simox::math::position(currentGlobalRobotPose) + 3000 * Eigen::Vector3f::UnitZ(),
                        simox::math::position(currentGlobalRobotPose) + 2000 * Eigen::Vector3f::UnitZ())
                .width(50).color(simox::Color::blue()));

            layerDebug.add(
                viz::Arrow("OriginalGraspPoseGlobal").fromTo(simox::math::position(originalGraspPoseGlobal) + 3500 * Eigen::Vector3f::UnitZ(),
                        simox::math::position(originalGraspPoseGlobal) + 1500 * Eigen::Vector3f::UnitZ())
                .width(12).color(simox::Color::green(128)));
            layerDebug.add(
                viz::Arrow("CurrentGraspPoseGlobal").fromTo(simox::math::position(currentGraspPoseGlobal) + 3000 * Eigen::Vector3f::UnitZ(),
                        simox::math::position(currentGraspPoseGlobal) + 2000 * Eigen::Vector3f::UnitZ())
                .width(25).color(simox::Color::blue(128)));


            layerDebug.add(
                viz::RobotHand("OriginalGraspPoseRobot")
                .fileByArm(arm).pose(originalGraspPoseRobot * arm.getTcp2HandRootTransform())
                .overrideColor(simox::Color::green(128)));
            layerDebug.add(
                viz::RobotHand("CurrentGraspPoseRobot")
                .fileByArm(arm).pose(currentGraspPoseRobot * arm.getTcp2HandRootTransform())
                .overrideColor(simox::Color::blue(128)));
            layerDebug.add(
                viz::Arrow("Origin").fromTo(4000 * Eigen::Vector3f::UnitZ(), 2000 * Eigen::Vector3f::UnitZ()).width(100).color(simox::Color::red()));


            arvizDebug.commit({layerDebug, layerDebugGraspPose});
        }

        getGraspCandidateObserver()->setSelectedCandidates(selectedCandidates);
        emitSingleGraspSelected();
    }
    else
    {
        out.setReferenceObjectName("");
        emitMultipleGraspsSelected();
    }
}


//void SelectGraspNearLocation::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void SelectGraspNearLocation::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}

bool SelectGraspNearLocation::getReachability(const grasping::GraspCandidatePtr& cnd)
{
    if (!cnd->reachabilityInfo)
    {
        ARMARX_WARNING << "GraspCandidate has no reachabilityInfo";
        return true;
    }
    return cnd->reachabilityInfo->reachable;
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SelectGraspNearLocation::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SelectGraspNearLocation(stateData));
}

