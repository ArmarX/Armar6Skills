/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6VisualGraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ExecuteGrasp.h"

#include <armar6/skills/libraries/Armar6Tools/Armar6GraspTrajectory.h>
#include <armar6/skills/libraries/Armar6Tools/Armar6HandV2Helper.h>

#include <MemoryX/libraries/motionmodels/MotionModelAttachedToOtherObject.h>

#include <RobotAPI/libraries/RobotStatechartHelpers/ForceTorqueHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/PositionControllerHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>
#include <RobotAPI/libraries/ArmarXObjects/ice_conversions.h>

#include <RobotAPI/components/ArViz/Client/Client.h>
#include <RobotAPI/components/ArViz/Client/ScopedClient.h>
#include <RobotAPI/components/ArViz/Client/elements/RobotHand.h>
#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <VirtualRobot/math/Helpers.h>
#include <VirtualRobot/math/LineStrip.h>
#include <VirtualRobot/math/LinearInterpolatedPose.h>

#include <SimoxUtility/math/pose/transform.h>


using namespace armarx;
using namespace Armar6VisualGraspingGroup;
using namespace ::math;

// DO NOT EDIT NEXT LINE
ExecuteGrasp::SubClassRegistry ExecuteGrasp::Registry(ExecuteGrasp::GetName(), &ExecuteGrasp::CreateInstance);


#define ENABLE_DEBUG_DRAWER 0


void ExecuteGrasp::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

ExecuteGrasp::GraspProcedureType ExecuteGrasp::GetGraspProcedureTypeFromName(const std::string& name)
{
    if (name == "Simple")
    {
        return ExecuteGrasp::GraspProcedureType::Simple;
    }
    if (name == "Trajectory")
    {
        return ExecuteGrasp::GraspProcedureType::Trajectory;
    }
    throw LocalException("Unsupported GraspProcedureType: ") << name;
}

void ExecuteGrasp::run()
{
    const std::string side = in.getSide();
    RobotNameHelperPtr nameHelper = getRobotNameHelper();
    RobotNameHelper::Arm arm = nameHelper->getArm(side);
    VirtualRobot::RobotPtr robot = getLocalRobot();
    RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
    VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

    KinematicUnitHelper kinUnitHelper(getKinematicUnit());

#if ENABLE_DEBUG_DRAWER
    DebugDrawerHelper debugDrawkerHelper(getDebugDrawerTopic(), "ExecuteGrasp", robot);
#endif


    viz::Client arviz = viz::Client::createFromTopic("Armar6 Grasping Statecharts", getArvizTopic());
    viz::Layer layerSelectedGrasps = arviz.layer("SelectedGrasps");
    // Don't cpmmit yet, just clear the layer at the end.

    viz::ScopedClient arvizDebug = viz::Client::createFromTopic("Armar6 Grasping Statecharts | Debug", getArvizTopic());
    viz::Layer layerDebug = arvizDebug.layer("Debug ExecuteGrasp");
    viz::Layer layerDebugGraspPose = arvizDebug.layer("GraspPose");
    viz::Layer layerDebugTcpToTarget = arvizDebug.layer("TCP To Target");
    arvizDebug.commit({layerDebug, layerDebugGraspPose});


    Armar6HandV2Helper handHelper(getKinematicUnit(), getRobotUnitObserver(), side, in.getIsSimulation());

    Eigen::Vector3f fingerPreshape = in.getFingerPreshape()->toEigen();

    std::string graspProcedureName = in.getGraspProcedure();
    GraspProcedureType graspProcedure = GetGraspProcedureTypeFromName(graspProcedureName);

    std::string packageName = "armar6_skills";
    armarx::CMakePackageFinder finder(packageName);
    std::string dataDir = finder.getDataDir() + "/" + packageName;

    Armar6GraspTrajectoryPtr graspTrajectory;

    if (graspProcedure == GraspProcedureType::Trajectory)
    {
        graspTrajectory = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/" + in.getGraspTrajectoryName() + ".xml");
        ARMARX_IMPORTANT << "Read grasp trajectory, length: " << graspTrajectory->calculateLength().pos << ", duration: " << graspTrajectory->getDuration();

        handHelper.shapeHand(graspTrajectory->GetHandValues(0));
    }
    else
    {
        handHelper.shapeHand(fingerPreshape);
    }


    std::string graspType = in.getGraspType();
    if (graspType != "Top" && graspType != "Side")
    {
        throw LocalException("Unsupported grasp type ") << graspType;
    }

    std::vector<Eigen::Vector3f> handClosing;
    if (in.getFingerClosingSequence() == "JustClose")
    {
        handClosing.push_back(Eigen::Vector3f(1.00f, 1.00f, 0));
        handClosing.push_back(Eigen::Vector3f(1.00f, 1.00f, 0));
    }
    else if (in.getFingerClosingSequence() == "Controlled")
    {
        handClosing.push_back(Eigen::Vector3f(0.16f, 0.28f, 0));
        handClosing.push_back(Eigen::Vector3f(0.30f, 0.36f, 0));
        handClosing.push_back(Eigen::Vector3f(0.39f, 0.44f, 0));
        handClosing.push_back(Eigen::Vector3f(0.47f, 0.47f, 0));
        handClosing.push_back(Eigen::Vector3f(1.00f, 1.00f, 0));
        for (size_t i = 0; i < handClosing.size(); i++)
        {
            handClosing.at(i) = ::math::Helpers::CwiseMax(fingerPreshape, handClosing.at(i));
        }
    }
    else
    {
        throw LocalException("Unknown FingerClosingSequence ") << in.getFingerClosingSequence();
    }
    ARMARX_IMPORTANT << VAROUT(fingerPreshape);
    LineStrip handClosingSequence(handClosing, 0, in.getGraspTime());
    float handClosingSequenceStartOffset = 0;
    for (float t = 0; t < in.getGraspTime(); t += 0.1f)
    {
        if (handClosingSequence.Get(t)(0) > fingerPreshape(0) || handClosingSequence.Get(t)(1) > fingerPreshape(1))
        {
            handClosingSequenceStartOffset = t - 0.1f;
            break;
        }
    }
    ARMARX_IMPORTANT << VAROUT(handClosingSequenceStartOffset);

    //NJointCartesianVelocityControllerWithRampInterfacePrx controller = NJointCartesianVelocityControllerWithRampInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getCartesianVelocityControllerName()));
    //controller->activateController();

    VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(getRobotUnit(), arm.getKinematicChain(), in.getCartesianVelocityControllerName()));

    ForceTorqueHelper ftHelper(getForceTorqueObserver(), arm.getForceTorqueSensor());

    Eigen::Vector3f approachVector = in.getApproachVector()->toEigen().normalized();
    float approachDistance = in.getApproachDistance();
    const Eigen::Matrix4f graspPose = in.getGraspPose()->toEigen();
    Eigen::Matrix4f prePose1 = Helpers::TranslatePose(graspPose, approachVector * approachDistance);
    Eigen::Matrix4f prePose2 = Helpers::TranslatePose(graspPose, approachVector * approachDistance / 2);
    Eigen::Matrix4f placePose = in.getPlacePose()->toEigen();

    layerDebug.add(viz::Pose("GraspPose").pose(graspPose));
    layerDebug.add(viz::Arrow("GraspPoseArrow").fromTo(simox::math::position(graspPose) + 2500 * Eigen::Vector3f::UnitZ(),
                   simox::math::position(graspPose) + 500 * Eigen::Vector3f::UnitZ())
                   .width(50).color(simox::Color::orange()));
    arvizDebug.commit(layerDebug);


    std::vector<Eigen::Matrix4f> waypoints;
    if (in.getGoToPreApproachPose())
    {
        Eigen::Vector3f preApproachPos = in.getPreApproachPosition()->toEigen();
        Eigen::Matrix3f preApproachOri = in.getPreApproachOrientationMap().at(side)->toEigen();
        Eigen::Matrix4f preApproachPose = Helpers::CreatePose(preApproachPos, preApproachOri);
        waypoints.push_back(preApproachPose);
        math::LinearInterpolatedPose linPose(preApproachPose, prePose1, 0, 1, true);
        waypoints.push_back(linPose.Get(0.5f));
    }
    else
    {
        math::LinearInterpolatedPose linPose(tcp->getPoseInRootFrame(), prePose1, 0, 1, true);
        waypoints.push_back(linPose.Get(0.5f));
    }
    waypoints.push_back(prePose1);
    waypoints.push_back(prePose2);
    //debugDrawerHelper.drawPose("prePose1", prePose1);
    //debugDrawerHelper.drawPose("prePose2", prePose2);
    //debugDrawerHelper.drawPose("graspPose", graspPose);
    ARMARX_INFO << VAROUT(waypoints);
    ARMARX_INFO << VAROUT(graspPose);
    //waypoints.push_back(graspPose);
    PositionControllerHelper posHelper(tcp, velHelper, waypoints);
    posHelper.readConfig(in.getControllerConfig());
    float defaultMaxPosVel = posHelper.posController.maxPosVel;

    //IceUtil::Time start = TimeUtil::GetTime();

    memoryx::ObjectInstanceMemorySegmentBasePrx objectInstancesSegment = getWorkingMemory()->getObjectInstancesSegment();
    std::string objectInstanceID;
    if (in.isObjectInstanceChannelSet() && in.getObjectInstanceChannel())
    {
        TimedVariantPtr idField = ChannelRefPtr::dynamicCast(in.getObjectInstanceChannel())->getDataField("id");
        objectInstanceID = idField->getString();
        ARMARX_IMPORTANT << "Found object instance ID: " << objectInstanceID;
    }
    else
    {
        ARMARX_IMPORTANT << "No object instance ID found";
    }


    PhaseType currentPhase = PhaseType::ApproachPrePose;
    IceUtil::Time graspStartTime;


    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        posHelper.update();

#if ENABLE_DEBUG_DRAWER
        debugDrawerHelper.drawLine("tcpToTarget", tcp->getPositionInRootFrame(),
                                   posHelper.getCurrentTargetPosition(), 5, DrawColor {0, 1, 0, 0});
#endif
        {
            layerDebugTcpToTarget.clear();
            layerDebugTcpToTarget.add(viz::Cylinder("tcpToTarget").fromTo(
                                          simox::math::transform_position(robot->getGlobalPose(), tcp->getPositionInRootFrame()),
                                          simox::math::transform_position(robot->getGlobalPose(), posHelper.getCurrentTargetPosition()))
                                      .radius(5).color(simox::Color::green()));
            arvizDebug.commit(layerDebugTcpToTarget);
        }


        float positionError = posHelper.getPositionError();
        float orientationError = posHelper.getOrientationError();
        //ARMARX_INFO << deactivateSpam(1) << VAROUT(positionError) << " " << VAROUT(orientationError) << " " << VAROUT(cv.transpose());

        getDebugObserver()->setDebugDatafield("CartesianPositionControl", "positionError", new Variant(positionError));
        getDebugObserver()->setDebugDatafield("CartesianPositionControl", "orientationError", new Variant(orientationError));
        //getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_py", new Variant(cv(1)));
        //getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_pz", new Variant(cv(2)));
        //getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_rx", new Variant(cv(3)));
        //getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_ry", new Variant(cv(4)));
        //getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_px", new Variant(cv(0)));
        //getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_rz", new Variant(cv(5)));


        PhaseType nextPhase = currentPhase;
        bool targetReached = posHelper.isFinalTargetReached();
        bool targetNear = posHelper.isFinalTargetNear();

        const Eigen::Vector3f currentForce = ftHelper.getForce();
        const Eigen::Vector3f currentTorque = ftHelper.getTorque();

        getDebugObserver()->setDebugDatafield("ExecuteGrasp", "force_x", new Variant(currentForce(0)));
        getDebugObserver()->setDebugDatafield("ExecuteGrasp", "force_y", new Variant(currentForce(1)));
        getDebugObserver()->setDebugDatafield("ExecuteGrasp", "force_z", new Variant(currentForce(2)));
        getDebugObserver()->setDebugDatafield("ExecuteGrasp", "torque_x", new Variant(currentTorque(0)));
        getDebugObserver()->setDebugDatafield("ExecuteGrasp", "torque_y", new Variant(currentTorque(1)));
        getDebugObserver()->setDebugDatafield("ExecuteGrasp", "torque_z", new Variant(currentTorque(2)));


        bool forceExceeded = currentForce.norm() > in.getForceThreshold();
        Eigen::Vector3f tcpPos = tcp->getPositionInRootFrame();
        Eigen::Matrix4f tcpPose = tcp->getPoseInRootFrame();
        if (in.getIsSimulation() && tcpPos(2) < in.getSimulationForceHeightThreshold())
        {
            ARMARX_IMPORTANT << "Simulating force exceeded";
            forceExceeded = true;
        }

        if (currentPhase == PhaseType::ApproachPrePose)
        {
            getMessageDisplay()->setMessage("Approaching Pre Pose", posHelper.getStatusText());
            if (targetReached)
            {
                ARMARX_IMPORTANT << "PrePose reached";
                if (graspType == "Top")
                {
                    Eigen::Vector3f TopGraspingVector = in.getTopGraspingVector()->toEigen();
                    ARMARX_IMPORTANT << "graspType = Top: Adding top grasping vector";
                    posHelper.setTarget(math::Helpers::TranslatePose(graspPose, TopGraspingVector));
                    posHelper.posController.maxPosVel = defaultMaxPosVel / 2;
                    ARMARX_IMPORTANT << "setting posController.maxPosVel = " << posHelper.posController.maxPosVel;
                }
                else
                {
                    posHelper.setTarget(graspPose);
                }
                velHelper->controller->setKpJointLimitAvoidance(0);
                velHelper->controller->setJointLimitAvoidanceScale(0);
                ftHelper.setZero();
                nextPhase = PhaseType::ApproachObject;
            }
        }
        if (currentPhase == PhaseType::ApproachObject)
        {
            getMessageDisplay()->setMessage("Approaching Object", posHelper.getStatusText());

            //if (targetReached && graspType == "Top" && !in.getIsSimulation())
            //{
            //    ARMARX_IMPORTANT << "targetReached";
            //    posHelper.immediateHardStop();
            //    emitFailure();
            //    break;
            //}
            //if (forceExceeded || (graspType == "Side" && targetReached) || (in.getIsSimulation() && targetReached))
            if (forceExceeded || targetReached)
            {
                posHelper.immediateHardStop();
                posHelper.posController.maxPosVel = defaultMaxPosVel;
                graspStartTime = TimeUtil::GetTime();

                if (graspProcedure == GraspProcedureType::Simple)
                {
                    nextPhase = PhaseType::SimpleGrasp;
                    Eigen::Matrix4f target = tcp->getPoseInRootFrame();
                    //target(2, 3) = fmax(target(2, 3), in.getGraspMinZHeight());
                    target = ::math::Helpers::TranslatePose(target, in.getOffsetDuringGrasp()->toEigen());
                    posHelper.setTarget(target);

                }
                else if (graspProcedure == GraspProcedureType::Trajectory)
                {
                    nextPhase = PhaseType::TrajectoryGrasp;

                    Eigen::Matrix4f startPose = graspTrajectory->getStartPose();
                    Eigen::Vector3f currentHandForward = ::math::Helpers::TransformDirection(tcpPose, handHelper.getHandForwardVector());
                    Eigen::Vector3f trajHandForward = ::math::Helpers::TransformDirection(startPose, handHelper.getHandForwardVector());
                    Eigen::Vector3f up(0, 0, 1);

                    float angle = ::math::Helpers::Angle(currentHandForward, trajHandForward, up);
                    Eigen::AngleAxisf aa(angle, up);

                    Eigen::Matrix4f transform = ::math::Helpers::CreateTranslationRotationTranslationPose(-math::Helpers::GetPosition(startPose), aa.toRotationMatrix(), math::Helpers::GetPosition(tcpPose));
                    graspTrajectory = graspTrajectory->getTransformed(transform);
                }
                else
                {
                    throw LocalException("Unsupported GraspProcedureType");
                }

            }
        }

        // When grasping the object, clear the grasp candidates
        if (in.getGraspVisuClearBeforeGrasp()
            && (currentPhase == PhaseType::TrajectoryGrasp or currentPhase == PhaseType::SimpleGrasp))
        {
            getDebugDrawerTopic()->clearLayer(in.getGraspProviderVisuName());

            layerSelectedGrasps.clear();
            layerDebugGraspPose.clear();
            arviz.commit({layerSelectedGrasps});
            arvizDebug.commit({layerDebugGraspPose});
        }

        if (currentPhase == PhaseType::TrajectoryGrasp)
        {
            IceUtil::Time now = TimeUtil::GetTime();
            float t = (now - graspStartTime).toSecondsDouble();
            float duration = graspTrajectory->getDuration();
            if (t > duration)
            {
                t = graspTrajectory->getDuration();
                nextPhase = PhaseType::Lift;
                Eigen::Matrix4f target = tcp->getPoseInRootFrame();
                target.block<3, 1>(0, 3) += in.getLiftVector()->toEigen();
                posHelper.setTarget(target);

                // Attach object to hand.
                if (in.isHandInstanceChannelSet() && objectInstanceID.size() > 0)
                {
                    ARMARX_IMPORTANT << "Attaching object to the robot's hand\n"
                                     << "objectInstanceID: " << objectInstanceID;
                    memoryx::MotionModelAttachedToOtherObjectPtr newMotionModel(
                        new memoryx::MotionModelAttachedToOtherObject(getRobotStateComponent(), in.getHandInstanceChannel()));
                    objectInstancesSegment->begin_setNewMotionModel(objectInstanceID, newMotionModel);
                }
                else if (in.getReferenceObjectName().size() > 0)
                {
                    const std::string objectName = in.getReferenceObjectName();
                    if (objectName.find("/") != std::string::npos)
                    {
                        armarx::objpose::AttachObjectToRobotNodeInput input;
                        input.objectID = toIce(armarx::ObjectID(objectName));
                        input.providerName = ""; // Optional
                        input.frameName = "Hand R TCP";  // ToDo: Make parameterizable.
                        input.agentName = "Armar6";
                        input.poseInFrame = nullptr;  // Derived automatically.
                        armarx::objpose::AttachObjectToRobotNodeOutput result = getObjectPoseStorage()->attachObjectToRobotNode(input);
                        if (!result.success)
                        {
                            ARMARX_WARNING << "Failed to attach object " << input.objectID << " to frame '" << input.frameName
                                           << "' of agent '" << input.agentName << "'.";
                        }
                    }
                }
                else
                {
                    ARMARX_IMPORTANT << "Not attaching object to the robot's hand since one channel is not set.\n"
                                     << VAROUT(in.isHandInstanceChannelSet()) << "\n"
                                     << VAROUT(in.isObjectInstanceChannelSet());
                }
            }
            else
            {
                Eigen::Matrix4f target = graspTrajectory->GetPose(t);
                posHelper.setTarget(target);
                posHelper.setFeedForwardVelocity(graspTrajectory->GetPositionDerivative(t), graspTrajectory->GetOrientationDerivative(t));
                Eigen::Vector3f handShape = graspTrajectory->GetHandValues(t);
                handHelper.shapeHand(handShape);
            }
        }
        if (currentPhase == PhaseType::SimpleGrasp)
        {
            IceUtil::Time now = TimeUtil::GetTime();
            float elapsedGraspTime = (now - graspStartTime).toSecondsDouble() + handClosingSequenceStartOffset;
            Eigen::Vector3f handShape = handClosingSequence.Get(Helpers::Clamp(0, in.getGraspTime(), elapsedGraspTime));
            handHelper.shapeHand(handShape);

            {
                std::stringstream ss;
                ss.precision(1);
                ss << elapsedGraspTime << "s Hand shape: (" << handShape(0) << ", " << handShape(1) << ")";
                getMessageDisplay()->setMessage("Grasping", ss.str());
            }

            if (elapsedGraspTime > in.getGraspTime())
            {

                nextPhase = PhaseType::Lift;
                Eigen::Matrix4f target = tcp->getPoseInRootFrame();
                target.block<3, 1>(0, 3) += in.getLiftVector()->toEigen();
                posHelper.setTarget(target);

            }
        }
        if (currentPhase != PhaseType::Lift && nextPhase == PhaseType::Lift)
        {
#if ENABLE_DEBUG_DRAWER
            DebugDrawerHelper ddhSelectedGrasps(getDebugDrawerTopic(), "SelectedGrasps", robot);
            ddhSelectedGrasps.clearLayer();
#endif
        }
        if (currentPhase == PhaseType::Lift)
        {
            {
                std::stringstream ss;
                ss << "Force: " << currentForce.norm();
                getMessageDisplay()->setMessage("Lifting", ss.str());
            }
            if (in.getPlaceAfterLift() && targetNear)
            {
                ftHelper.setZero();
                nextPhase = PhaseType::Replace;
                posHelper.setTarget(placePose);

            }
            if (!in.getPlaceAfterLift() && targetReached)
            {
                posHelper.immediateHardStop();
                emitSuccess();
                getMessageDisplay()->setMessage("", "");
                break;
            }
        }
        if (currentPhase == PhaseType::Replace)
        {
            getMessageDisplay()->setMessage("Replacing", "");
            if (targetNear)
            {
                ftHelper.setZero();
                posHelper.setTarget(Helpers::TranslatePose(placePose, in.getPlaceVector()->toEigen()));
                nextPhase = PhaseType::Place;
            }
        }
        if (currentPhase == PhaseType::Place)
        {
            {
                std::stringstream ss;
                ss << "Force: " << currentForce.norm();
                getMessageDisplay()->setMessage("Placing", ss.str());
            }
            if (forceExceeded)
            {
                posHelper.immediateHardStop();
                handHelper.shapeHand(0, 0);
                posHelper.setTarget(Helpers::TranslatePose(placePose, Eigen::Vector3f(0, 0, 100)));
                posHelper.addWaypoint(in.getRetractAfterPlacePose()->toEigen());
                //posHelper.addWaypoint(in.getFinalPrePose()->toEigen());
                //posHelper.addWaypoint(in.getFinalPose()->toEigen());
                velHelper->controller->setJointLimitAvoidanceScale(2);
                velHelper->controller->setKpJointLimitAvoidance(1);
                nextPhase = PhaseType::RetractAfterPlace;
                kinUnitHelper.setJointAngles(in.getLookAtObjectJointAngles());

            }
            if (targetReached)
            {
                posHelper.immediateHardStop();
                emitFailure();
                break;
            }
        }
        if (currentPhase == PhaseType::RetractAfterPlace)
        {
            getMessageDisplay()->setMessage("Move back", posHelper.getStatusText());
            if (targetReached)
            {
                posHelper.immediateHardStop();
                emitSuccess();
                getMessageDisplay()->setMessage("", "");
                break;
            }
        }

        currentPhase = nextPhase;

        TimeUtil::SleepMS(10);
    }

    if (in.getStopOnExit())
    {
        posHelper.immediateHardStop();
    }
    velHelper->cleanup();

#if ENABLE_DEBUG_DRAWER
    debugDrawerHelper.clearLayer();
    getDebugDrawerTopic()->clearLayer(in.getGraspProviderVisuName());
#endif

    layerSelectedGrasps.clear();
    layerDebugGraspPose.clear();
    arviz.commit({layerSelectedGrasps});
    arvizDebug.commit({layerDebugGraspPose});
}

//void ExecuteGrasp::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ExecuteGrasp::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ExecuteGrasp::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ExecuteGrasp(stateData));
}
