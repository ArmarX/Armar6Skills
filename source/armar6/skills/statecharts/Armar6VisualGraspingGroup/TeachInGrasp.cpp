/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6VisualGraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TeachInGrasp.h"

#include <ArmarXCore/core/time/TimeUtil.h>

#include <RobotAPI/libraries/RobotStatechartHelpers/ForceTorqueHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/PositionControllerHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>

#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>

#include <VirtualRobot/math/Helpers.h>

#include <armar6/skills/libraries/Armar6Tools/Armar6GraspTrajectory.h>
#include <armar6/skills/libraries/Armar6Tools/Armar6HandV2Helper.h>

#include <RobotAPI/libraries/SimpleJsonLogger/SimpleJsonLogger.h>

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <armar6/skills/statecharts/Armar6VisualGraspingGroup/gui/TeachInGraspGui.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6VisualGraspingGroup;

// DO NOT EDIT NEXT LINE
TeachInGrasp::SubClassRegistry TeachInGrasp::Registry(TeachInGrasp::GetName(), &TeachInGrasp::CreateInstance);



void TeachInGrasp::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

Eigen::Vector3f defrost(const Vector3BasePtr& base)
{
    return Vector3Ptr::dynamicCast(base)->toEigen();
}
Eigen::Matrix4f defrost(const PoseBasePtr& base)
{
    return PosePtr::dynamicCast(base)->toEigen();
}


void TeachInGrasp::run()
{
    std::string side = in.getSide();
    Eigen::Matrix4f initPose = in.getInitPoseMap().at(side)->toEigen();
    Eigen::Vector3f moveDownVector = in.getMoveDownVector()->toEigen();
    Eigen::Vector3f liftVector = in.getLiftVector()->toEigen();
    //float deltaPosScalingValue = in.getDeltaPosScalingValue();
    //float deltaOriScalingValue = math::Helpers::deg2rad(1.f); /// @@@ add in.get...
    float defaultKeypointDt = 0.5f; /// @@@ add in.get...
    float deltaFingersScalingValue = in.getDeltaFingersScalingValue();
    float approachDistance = in.getApproachDistance();

    RobotNameHelperPtr nameHelper = getRobotNameHelper();
    RobotNameHelper::Arm arm = nameHelper->getArm(side);
    VirtualRobot::RobotPtr robot = getLocalRobot();
    ::armarx::RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
    VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
    VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

    KinematicUnitHelper kinUnitHelper(getKinematicUnit());
    DebugDrawerHelper debugDrawerHelper(getDebugDrawerTopic(), "TeachInGrasp", robot);
    Armar6HandV2Helper handHelper(getKinematicUnit(), getRobotUnitObserver(), side, in.getIsSimulation());
    VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(getRobotUnit(), arm.getKinematicChain(), in.getCartesianVelocityControllerName()));
    ForceTorqueHelper ftHelper(getForceTorqueObserver(), arm.getForceTorqueSensor());
    TeachInGraspGui gui(getRemoteGui());
    gui.initTab();
    RobotNameHelperPtr rnh = getRobotNameHelper();

    debugDrawerHelper.clearLayer();

    PositionControllerHelper posHelper(tcp, velHelper, tcp->getPoseInRootFrame());
    posHelper.readConfig(in.getControllerConfig());
    ARMARX_IMPORTANT << in.getControllerConfig();
    //posHelper.thresholdOrientationNear = in.getThresholdOrientationNear();
    //posHelper.thresholdOrientationReached = in.getThresholdOrientationReached();
    //posHelper.thresholdPositionNear = in.getThresholdPositionNear();
    //posHelper.thresholdPositionReached = in.getThresholdPositionReached();
    //
    //posHelper.posController.maxOriVel = in.getMaxOriVel();
    //posHelper.posController.maxPosVel = in.getMaxPosVel();
    //posHelper.posController.KpOri = in.getKpOrientation();
    //posHelper.posController.KpPos = in.getKpPosition();

    PhaseType currentPhase = PhaseType::Init;

    Eigen::Vector3f fingersTarget(0, 0, 0);

    grasping::GraspCandidateSeq candidates = getGraspCandidateObserver()->getAllCandidates();
    int selectedCandidateIndex = 0;

    int selectedKeypointIndex = 0;

    Eigen::Matrix4f graspPose;
    Eigen::Vector3f approachVector;

    std::string packageName = "armar6_skills";
    armarx::CMakePackageFinder finder(packageName);
    std::string dataDir = finder.getDataDir() + "/" + packageName;
    std::string logfilename = dataDir +  "/logs/TeachInGrasp.json";

    SimpleJsonLogger logger(logfilename, true);
    {
        SimpleJsonLoggerEntry e;
        e.Add("Event", "Start");
        e.AddTimestamp();
        logger.log(e);
    }

    Armar6GraspTrajectoryPtr graspTrajectory(new Armar6GraspTrajectory(tcp->getPoseInRootFrame(), fingersTarget));
    IceUtil::Time trajectoryStartTime = TimeUtil::GetTime();

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        posHelper.updateRead();
        gui.updateRead();
        debugDrawerHelper.cyclicCleanup();

        PhaseType nextPhase = currentPhase;
        //bool targetReached = posHelper.isFinalTargetReached();
        //bool targetNear = posHelper.isFinalTargetNear();

        IceUtil::Time now = TimeUtil::GetTime();

        SimpleJsonLoggerEntry e;
        std::string currentEvent = "None";

        bool updateSelectedCandidate = false;

        const Eigen::Vector3f currentForce = ftHelper.getForce();
        const Eigen::Vector3f currentTorque = ftHelper.getTorque();
        bool forceExceeded = currentForce.norm() > in.getForceThreshold();
        Eigen::Vector3f tcpPos = tcp->getPositionInRootFrame();
        Eigen::Matrix4f tcpPose = tcp->getPoseInRootFrame();
        if (in.getIsSimulation() && tcpPos(2) < in.getSimulationForceHeightThreshold())
        {
            ARMARX_IMPORTANT << "Simulating force exceeded";
            forceExceeded = true;
        }

        getDebugObserver()->setDebugDatafield("TeachInGrasp", "force_x", new Variant(currentForce(0)));
        getDebugObserver()->setDebugDatafield("TeachInGrasp", "force_y", new Variant(currentForce(1)));
        getDebugObserver()->setDebugDatafield("TeachInGrasp", "force_z", new Variant(currentForce(2)));

        gui.lblTimer->setValue(TimeUtil::GetTime().toDateTime());

        if (gui.btnGotoInitPosition->wasClicked())
        {
            posHelper.setTarget(initPose);
        }
        if (gui.btnMoveDownUntilForce->wasClicked())
        {
            ftHelper.setZero();
            posHelper.setTarget(::math::Helpers::TranslatePose(tcpPose, moveDownVector));
            nextPhase = PhaseType::MoveDown;
        }
        if (gui.btnLift->wasClicked())
        {
            posHelper.setTarget(::math::Helpers::TranslatePose(tcpPose, liftVector));
        }
        if (gui.btnGetGraspCandidates->wasClicked())
        {
            candidates = getGraspCandidateObserver()->getAllCandidates();
            selectedCandidateIndex = 0;
            updateSelectedCandidate = true;
        }
        int candidateDelta = gui.btnNextCandidate->clickDelta() - gui.btnPrevCandidate->clickDelta();
        selectedCandidateIndex = (selectedCandidateIndex + candidates.size() + candidateDelta) % candidates.size();
        if (candidateDelta != 0)
        {
            updateSelectedCandidate = true;
        }

        if (updateSelectedCandidate)
        {
            gui.lblCandidateInfo->setValue("Selected candidate: " + std::to_string(selectedCandidateIndex) + "/" + std::to_string(candidates.size()));

        }

        bool hasValidGraspCandidate = (int)candidates.size() > selectedCandidateIndex;
        grasping::GraspCandidatePtr selectedCandidate;
        if (hasValidGraspCandidate)
        {
            selectedCandidate = candidates.at(selectedCandidateIndex);
            graspPose = defrost(selectedCandidate->graspPose);
            approachVector = defrost(selectedCandidate->approachVector);
            debugDrawerHelper.drawLine("ApproachVector", math::Helpers::GetPosition(graspPose), math::Helpers::GetPosition(graspPose) + approachVector * approachDistance, 5, DrawColor {0, 0, 1, 1});
        }

        if (gui.btnGotoPrePose->wasClicked() && hasValidGraspCandidate)
        {
            posHelper.setTarget(::math::Helpers::TranslatePose(graspPose, approachVector * approachDistance));
        }
        if (gui.btnGotoGraspPose->wasClicked() && hasValidGraspCandidate)
        {
            posHelper.setTarget(graspPose);
        }

        float lastKeypointDt = graspTrajectory->getKeypoint(selectedKeypointIndex)->dt;

        if (gui.btnSaveTrajectory->wasClicked())
        {
            graspTrajectory->writeToFile(dataDir + "/motions/grasps/" + gui.edtFile->getValue() + ".xml");
        }
        if (gui.btnLoadTrajectory->wasClicked())
        {
            graspTrajectory = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/" + gui.edtFile->getValue() + ".xml");
        }
        if (gui.btnResetTrajectory->wasClicked())
        {
            selectedKeypointIndex = 0;
            graspTrajectory.reset(new Armar6GraspTrajectory(tcp->getPoseInRootFrame(), fingersTarget));
        }
        if (gui.btnAddKeypoint->wasClicked())
        {
            currentEvent = "Keypoint";
            graspTrajectory->addKeypoint(tcp->getPoseInRootFrame(), fingersTarget, defaultKeypointDt);
        }
        if (gui.btnReplaceKeypoint->wasClicked())
        {
            if (selectedKeypointIndex > 0)
            {
                float dt = graspTrajectory->getKeypoint(selectedKeypointIndex)->dt;
                graspTrajectory->replaceKeypoint(selectedKeypointIndex, tcp->getPoseInRootFrame(), fingersTarget, dt);
            }
        }
        if (gui.btnRemoveKeypoint->wasClicked())
        {
            if (selectedKeypointIndex > 0)
            {
                graspTrajectory->removeKeypoint(selectedKeypointIndex);
            }
        }
        if (gui.btnNextKeypoint->wasClicked())
        {
            selectedKeypointIndex++;
        }
        if (gui.btnPrevKeypoint->wasClicked())
        {
            selectedKeypointIndex--;
        }
        selectedKeypointIndex = math::Helpers::Clampi(0, graspTrajectory->getKeypointCount() - 1, selectedKeypointIndex);
        Eigen::Matrix4f currentTarget = posHelper.getCurrentTarget();

        if (gui.btnGotoKeypoint->wasClicked())
        {
            if (graspTrajectory->getKeypointCount() <= 1)
            {
                ARMARX_WARNING << "Cannot go to keypoint, no keypoints set";
            }
            else
            {
                Armar6GraspTrajectory::KeypointPtr keypoint = graspTrajectory->getKeypoint(selectedKeypointIndex);
                currentTarget = keypoint->getTargetPose();
                posHelper.setTarget(currentTarget);
                fingersTarget = keypoint->handJointsTarget;
            }
        }

        float currentKeypointDt = graspTrajectory->getKeypoint(selectedKeypointIndex)->dt;
        if (currentKeypointDt != lastKeypointDt)
        {
            gui.spnKeypointDt->setValue(currentKeypointDt);
        }
        if (gui.btnApplyKeypointDt->wasClicked())
        {
            graspTrajectory->setKeypointDt(selectedKeypointIndex, gui.spnKeypointDt->getValue());
        }



        gui.lblTrajectoryInfo->setValue("Trajectory: " + std::to_string(selectedKeypointIndex) + "/" + std::to_string(graspTrajectory->getKeypointCount()));
        Armar6GraspTrajectory::Length trajLen = graspTrajectory->calculateLength();
        gui.lblTrajectoryDescr->setValue("Length: " + std::to_string(trajLen.pos) + ", Duration: " + std::to_string(graspTrajectory->getDuration()));

        if (gui.btnGotoTrajectoryStart->wasClicked())
        {
            nextPhase = PhaseType::Init;
            gui.sldTrajectory->setValue(0);
            currentTarget = graspTrajectory->GetPose(0);
            posHelper.setTarget(currentTarget);
            fingersTarget = graspTrajectory->GetHandValues(0);
        }
        if (gui.btnStartTrajectory->wasClicked())
        {
            trajectoryStartTime = TimeUtil::GetTime();
            nextPhase = PhaseType::RunTrajectory;
        }
        if (gui.btnStopTrajectory->wasClicked())
        {
            nextPhase = PhaseType::Init;
        }

        if (gui.btnPreviewTrajectory->isDown())
        {
            float t = gui.sldTrajectory->getValue() / 100.f * graspTrajectory->getDuration();
            currentTarget = graspTrajectory->GetPose(t);
            posHelper.setTarget(currentTarget);
            fingersTarget = graspTrajectory->GetHandValues(t);
        }
        if (gui.btnVisuTrajectory->isDown())
        {
            float t = gui.sldTrajectory->getValue() / 100.f * graspTrajectory->getDuration();
            Eigen::Matrix4f target = graspTrajectory->GetPose(t);
            Eigen::Matrix4f tcp2handRoot = rnh->getRobotArm(RobotNameHelper::LocationRight, robot).getTCP()->getPoseInRootFrame().inverse() * robot->getRobotNode("Hand R Root")->getPoseInRootFrame();

            debugDrawerHelper.drawRobot("preview", "armar6_rt/robotmodel/Armar6-SH/Armar6-RightHand-v3.xml", "armar6_rt", target * tcp2handRoot, DrawColor {0, 1, 0, 1});
            debugDrawerHelper.setRobotConfig("preview", handHelper.getSimulationJointValues(graspTrajectory->GetHandValues(t)));
        }

        if (gui.btnTranslateTrajectory->wasClicked())
        {
            Eigen::Matrix4f startPose = graspTrajectory->getStartPose();
            Eigen::Vector3f currentHandForward = math::Helpers::TransformDirection(tcpPose, handHelper.getHandForwardVector());
            Eigen::Vector3f trajHandForward = math::Helpers::TransformDirection(startPose, handHelper.getHandForwardVector());
            Eigen::Vector3f up(0, 0, 1);

            float angle = math::Helpers::Angle(currentHandForward, trajHandForward, up);
            Eigen::AngleAxisf aa(angle, up);

            Eigen::Matrix4f transform = math::Helpers::CreateTranslationRotationTranslationPose(-math::Helpers::GetPosition(startPose), aa.toRotationMatrix(), math::Helpers::GetPosition(tcpPose));

            graspTrajectory = graspTrajectory->getTransformed(transform);
        }


        Eigen::Vector3f deltaPos = gui.getDeltaPos();
        if (deltaPos.norm() > 0)
        {
            float deltaPosScalingValue = gui.spnDeltaPos->getValue();
            currentTarget = ::math::Helpers::TranslatePose(currentTarget, deltaPos * deltaPosScalingValue);
            posHelper.setTarget(currentTarget);
        }
        Eigen::Vector3f deltaOri = gui.getDeltaOri();
        if (deltaOri.norm() > 0)
        {
            //ARMARX_IMPORTANT << VAROUT(deltaOri.transpose());
            float deltaOriScalingValue = math::Helpers::deg2rad(gui.spnDeltaOri->getValue());
            ARMARX_IMPORTANT << "deltaOriScalingValue: " << deltaOriScalingValue;
            Eigen::AngleAxisf aaPitch(deltaOri(0) * deltaOriScalingValue, Eigen::Vector3f::UnitY()); // Pitch using tcp Y axis
            Eigen::AngleAxisf aaYaw(deltaOri(2) * deltaOriScalingValue, Eigen::Vector3f::UnitZ()); // Yaw using global Z axis

            Eigen::Matrix3f currentTargetOri = aaYaw.toRotationMatrix() * math::Helpers::GetOrientation(currentTarget) * aaPitch.toRotationMatrix();
            currentTargetOri = math::Helpers::Orthogonalize(currentTargetOri);
            math::Helpers::Orientation(currentTarget) = currentTargetOri;
            posHelper.setTarget(currentTarget);
        }
        Eigen::Vector3f deltaFingers = gui.getDeltaFingers();
        if (deltaFingers.norm() > 0)
        {
            fingersTarget += deltaFingers * deltaFingersScalingValue;
            fingersTarget = math::Helpers::CwiseClamp(0, 1, fingersTarget);
        }
        if (gui.btnOpenFingers->wasClicked())
        {
            fingersTarget = Eigen::Vector3f(0, 0, 0);
        }
        if (gui.btnCloseFingers->wasClicked())
        {
            fingersTarget = Eigen::Vector3f(1, 1, 0);
        }


        if (currentPhase == PhaseType::MoveDown)
        {
            if (forceExceeded)
            {
                posHelper.immediateHardStop();
                nextPhase = PhaseType::Init;
            }
        }
        if (currentPhase == PhaseType::RunTrajectory)
        {
            float t = (now - trajectoryStartTime).toSecondsDouble();
            float duration = graspTrajectory->getDuration();
            if (t > duration)
            {
                t = graspTrajectory->getDuration();
                nextPhase = PhaseType::Init;
            }
            currentTarget = graspTrajectory->GetPose(t);
            posHelper.setTarget(currentTarget);
            posHelper.setFeedForwardVelocity(graspTrajectory->GetPositionDerivative(t), graspTrajectory->GetOrientationDerivative(t));
            fingersTarget = graspTrajectory->GetHandValues(t);
            if (duration > 0)
            {
                gui.sldTrajectory->setValue(t / duration * 100.f);
            }
        }


        handHelper.shapeHand(fingersTarget(0), fingersTarget(1));


        e.Add("Event", currentEvent);
        e.AddTimestamp();
        e.Add("JointValues", rns->getJointValueMap());
        e.AddMatrix("tcpTarget", posHelper.getCurrentTarget());
        e.AddMatrix("tcpActual", tcpPose);
        e.Add("fingersTarget", handHelper.getFingersTargetValue());
        e.Add("fingersActual", handHelper.getFingersActualValue());
        e.Add("thumbTarget", handHelper.getThumbTargetValue());
        e.Add("thumbActual", handHelper.getThumbActualValue());
        e.AddAsArr("currentForce", currentForce);
        e.AddAsArr("currentTorque", currentTorque);
        logger.log(e);

        posHelper.updateWrite();
        gui.updateWrite();

        currentPhase = nextPhase;
        TimeUtil::SleepMS(10);
    }

    velHelper->cleanup();

    logger.close();
}

//void TeachInGrasp::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void TeachInGrasp::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TeachInGrasp::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TeachInGrasp(stateData));
}
