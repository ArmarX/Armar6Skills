/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6TestrigGroup::Armar6TestrigGroupRemoteStateOfferer
 * @author     Armar6 Test ( armar6test )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6TestrigGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6TestrigGroup;

// DO NOT EDIT NEXT LINE
Armar6TestrigGroupRemoteStateOfferer::SubClassRegistry Armar6TestrigGroupRemoteStateOfferer::Registry(Armar6TestrigGroupRemoteStateOfferer::GetName(), &Armar6TestrigGroupRemoteStateOfferer::CreateInstance);



Armar6TestrigGroupRemoteStateOfferer::Armar6TestrigGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6TestrigGroupStatechartContext > (reader)
{
}

void Armar6TestrigGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6TestrigGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6TestrigGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6TestrigGroupRemoteStateOfferer::GetName()
{
    return "Armar6TestrigGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6TestrigGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6TestrigGroupRemoteStateOfferer(reader));
}
