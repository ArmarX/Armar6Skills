/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6TestrigGroup
 * @author     Armar6 Test ( armar6test )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TestNominalTorque.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6TestrigGroup;

// DO NOT EDIT NEXT LINE
TestNominalTorque::SubClassRegistry TestNominalTorque::Registry(TestNominalTorque::GetName(), &TestNominalTorque::CreateInstance);



void TestNominalTorque::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void TestNominalTorque::run()
{
    getKinematicUnit()->switchControlMode({{"Test1", eTorqueControl}, {"Test2", eTorqueControl}});
    getKinematicUnit()->setJointTorques({{"Test1", in.getTorque1()}, {"Test2", in.getTorque2()}});
    //getKinematicUnit()->setJointVelocities({{"Test1", in.getVelocity()}});

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        TimeUtil::SleepMS(10);
    }

    getKinematicUnit()->setJointTorques({{"Test1", 0}, {"Test2", 0}});

    //getKinematicUnit()->switchControlMode({{"Test1", eTorqueControl}, {"Test2", eVelocityControl}});
    //getKinematicUnit()->setJointTorques({{"Test1", 0}});
    //getKinematicUnit()->setJointVelocities({{"Test2", 0}});


}

//void TestNominalTorque::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void TestNominalTorque::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TestNominalTorque::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TestNominalTorque(stateData));
}
