/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::HapticObjectExplorationGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveJoint.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace HapticObjectExplorationGroup;

// DO NOT EDIT NEXT LINE
MoveJoint::SubClassRegistry MoveJoint::Registry(MoveJoint::GetName(), &MoveJoint::CreateInstance);



void MoveJoint::onEnter()
{
    KinematicUnitInterfacePrx kinUnit = getKinematicUnit();
    std::string jointName = in.getJointName();
    float velocity = in.getVelocity();
    kinUnit->switchControlMode({{jointName, eVelocityControl}});
    kinUnit->setJointVelocities({{jointName, velocity}});
    float target = in.getTarget();
    installConditionForTargetReached(Literal(DataFieldIdentifier(getKinematicUnitObserver()->getObserverName(), "jointangles", jointName), velocity < 0 ? checks::smaller : checks::larger, {target}));

}

//void MoveJoint::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void MoveJoint::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MoveJoint::onExit()
{
    KinematicUnitInterfacePrx kinUnit = getKinematicUnit();
    std::string jointName = in.getJointName();
    kinUnit->setJointVelocities({{jointName, 0}});
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveJoint::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveJoint(stateData));
}
