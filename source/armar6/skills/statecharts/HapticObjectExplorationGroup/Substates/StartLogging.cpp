/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::HapticObjectExplorationGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "StartLogging.h"
#include <boost/algorithm/string/replace.hpp>
//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <termios.h>

using namespace armarx;
using namespace HapticObjectExplorationGroup;

// DO NOT EDIT NEXT LINE
StartLogging::SubClassRegistry StartLogging::Registry(StartLogging::GetName(), &StartLogging::CreateInstance);



void StartLogging::onEnter()
{
    IceUtil::Time now = IceUtil::Time::now();
    time_t time = now.toSeconds();
    struct tm* ts;
    char buffer[80];
    ts = localtime(&time);
    strftime(buffer, 80, "%Y-%m-%d-%H-%M-%S", ts);

    ARMARX_ERROR << "fix call to getRobotUnit()->stopLogging()!!!";
    //getRobotUnit()->stopLogging();
    //getRobotUnit()->startLogging("log-" + in.getTrialName() + "-" + buffer + ".csv", {});

    emitSuccess();
}

//void StartLogging::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void StartLogging::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void StartLogging::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr StartLogging::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new StartLogging(stateData));
}
