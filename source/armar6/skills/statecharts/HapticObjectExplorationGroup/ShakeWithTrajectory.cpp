/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::HapticObjectExplorationGroup
 * @author     armar6-user ( armar6-user at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ShakeWithTrajectory.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/core/util/StringHelpers.h>

using namespace armarx;
using namespace HapticObjectExplorationGroup;

// DO NOT EDIT NEXT LINE
ShakeWithTrajectory::SubClassRegistry ShakeWithTrajectory::Registry(ShakeWithTrajectory::GetName(), &ShakeWithTrajectory::CreateInstance);



void ShakeWithTrajectory::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

bool ShakeWithTrajectory::anyFilterMatches(const std::vector<std::vector<std::string>>& filters, const std::string& str)
{
    for (const std::vector<std::string>& f : filters)
    {
        bool matches = true;
        for (const std::string& s : f)
        {
            if (!Contains(str, s, true))
            {
                matches = false;
                break;
            }
        }
        if (matches)
        {
            return true;
        }
    }
    return false;
}

void ShakeWithTrajectory::run()
{

    std::vector<std::string> allLoggingNames = getRobotUnit()->getLoggingNames();
    std::vector<std::string> loggingNames;
    std::string filter = "sens ArmL .torque$, sens.FT L.tx, sens.FT L.ty, sens.FT L.tz, sens.FT L.fx, sens.FT L.fy, sens.FT L.fz";

    std::vector<std::vector<std::string>> filters;
    for (const std::string& p : Split(filter, ",", true))
    {
        filters.push_back(Split(p, " ", true, true));
    }
    for (const std::string& name  : allLoggingNames)
    {
        if (anyFilterMatches(filters, name + "$"))
        {
            loggingNames.push_back(name + "$");
        }
    }
    ARMARX_IMPORTANT << "Start Logging of " << loggingNames;

    SimpleRemoteReferenceCounterBasePtr loggingPrx = getRobotUnit()->startRtLogging("{DataDir:armar6_rt}/armar6_rt/logs/ft-torque-{DateTime}.csv", loggingNames);
    getRobotUnit()->addMarkerToRtLog(loggingPrx, "start");

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        // do your calculations
        usleep(10000);
    }
    getRobotUnit()->addMarkerToRtLog(loggingPrx, "stop");
    //usleep(10000);
    getRobotUnit()->stopRtLogging(loggingPrx);
    //loggingPrx = nullptr;
}

//void ShakeWithTrajectory::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ShakeWithTrajectory::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ShakeWithTrajectory::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ShakeWithTrajectory(stateData));
}
