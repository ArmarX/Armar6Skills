/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::HapticObjectExplorationGroup::HapticObjectExplorationGroupRemoteStateOfferer
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "HapticObjectExplorationGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace HapticObjectExplorationGroup;

// DO NOT EDIT NEXT LINE
HapticObjectExplorationGroupRemoteStateOfferer::SubClassRegistry HapticObjectExplorationGroupRemoteStateOfferer::Registry(HapticObjectExplorationGroupRemoteStateOfferer::GetName(), &HapticObjectExplorationGroupRemoteStateOfferer::CreateInstance);



HapticObjectExplorationGroupRemoteStateOfferer::HapticObjectExplorationGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < HapticObjectExplorationGroupStatechartContext > (reader)
{
}

void HapticObjectExplorationGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void HapticObjectExplorationGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void HapticObjectExplorationGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string HapticObjectExplorationGroupRemoteStateOfferer::GetName()
{
    return "HapticObjectExplorationGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr HapticObjectExplorationGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new HapticObjectExplorationGroupRemoteStateOfferer(reader));
}
