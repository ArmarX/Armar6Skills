/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArMarkerTestGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LocalizeAllMarkers.h"

#include <ArmarXCore/core/time/TimeUtil.h>

#include <VirtualRobot/math/Helpers.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace ArMarkerTestGroup;

// DO NOT EDIT NEXT LINE
LocalizeAllMarkers::SubClassRegistry LocalizeAllMarkers::Registry(LocalizeAllMarkers::GetName(), &LocalizeAllMarkers::CreateInstance);



void LocalizeAllMarkers::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void LocalizeAllMarkers::run()
{
    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    std::string rootName = robot->getRootNode()->getName();
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

        visionx::ArMarkerLocalizationResultList resultList = getArMarkerLocalizer()->LocalizeAllMarkersNow();
        ARMARX_IMPORTANT << "found " << resultList.size() << " markers";
        for (const visionx::ArMarkerLocalizationResult& r : resultList)
        {
            ARMARX_IMPORTANT << r.id << " " << r.pose;
            //FramedOrientationPtr oriPtr = FramedOrientationPtr::dynamicCast(r.orientation);
            //FramedPositionPtr posPtr = FramedPositionPtr::dynamicCast(r.position);
            FramedPosePtr posePtr = FramedPosePtr::dynamicCast(r.pose);
            //Eigen::Matrix4f pose = math::Helpers::CreatePose(posPtr->toRootEigen(robot), oriPtr->toRootEigen(robot));
            Eigen::Matrix4f pose = posePtr->toRootEigen(robot);
            Eigen::Matrix4f globalPose = robot->getGlobalPose() * pose;
            //ARMARX_IMPORTANT << VAROUT(pose) << VAROUT(globalPose);
            //ARMARX_IMPORTANT << VAROUT(r.instanceName) << VAROUT(r.objectClassName);
            getDebugDrawerTopic()->setPoseVisu("LocalizeAllMarkers", "marker_" + std::to_string(r.id), new Pose(globalPose));
            //getDebugDrawerTopic()->setSphereVisu("LocalizeAllMarkers", "Test", new Vector3(0, 0, 0), DrawColor {1, 1, 1, 1}, 5);
        }

        TimeUtil::SleepMS(10);
    }
}

//void LocalizeAllMarkers::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void LocalizeAllMarkers::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr LocalizeAllMarkers::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new LocalizeAllMarkers(stateData));
}
