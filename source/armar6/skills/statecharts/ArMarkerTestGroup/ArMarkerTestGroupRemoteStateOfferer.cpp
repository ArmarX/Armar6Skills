/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArMarkerTestGroup::ArMarkerTestGroupRemoteStateOfferer
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ArMarkerTestGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace ArMarkerTestGroup;

// DO NOT EDIT NEXT LINE
ArMarkerTestGroupRemoteStateOfferer::SubClassRegistry ArMarkerTestGroupRemoteStateOfferer::Registry(ArMarkerTestGroupRemoteStateOfferer::GetName(), &ArMarkerTestGroupRemoteStateOfferer::CreateInstance);



ArMarkerTestGroupRemoteStateOfferer::ArMarkerTestGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < ArMarkerTestGroupStatechartContext > (reader)
{
}

void ArMarkerTestGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void ArMarkerTestGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void ArMarkerTestGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string ArMarkerTestGroupRemoteStateOfferer::GetName()
{
    return "ArMarkerTestGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr ArMarkerTestGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new ArMarkerTestGroupRemoteStateOfferer(reader));
}
