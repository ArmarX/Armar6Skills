/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6FollowMeDemo
 * @author     Sonja Marahrens ( sonja dot marahrens )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RightArmTorqueDemo.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6FollowMeDemo;

// DO NOT EDIT NEXT LINE
RightArmTorqueDemo::SubClassRegistry RightArmTorqueDemo::Registry(RightArmTorqueDemo::GetName(), &RightArmTorqueDemo::CreateInstance);



void RightArmTorqueDemo::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void RightArmTorqueDemo::run()
{
    NameControlModeMap controlModes;
    for (const std::string& name : in.getTorqueControlledJoints())
    {
        controlModes[name] = eTorqueControl;
    }
    getKinematicUnit()->switchControlMode(controlModes);

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        // do your calculations
        usleep(10000);
    }

    for (const std::string& name : in.getTorqueControlledJoints())
    {
        controlModes[name] = ePositionControl;
    }
    getKinematicUnit()->switchControlMode(controlModes);
}

void RightArmTorqueDemo::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr RightArmTorqueDemo::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new RightArmTorqueDemo(stateData));
}
