/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6FollowMeDemo
 * @author     [Author Name] ( [Author Email] )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "FollowMeAndBalance.h"

#include <armar6/rt/units/Armar6Unit/NJointController/NJointGlobalTCPController.h>

#include <RobotAPI/interface/units/RobotUnit/NJointTrajectoryController.h>

#include <IceUtil/UUID.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6FollowMeDemo;

// DO NOT EDIT NEXT LINE
FollowMeAndBalance::SubClassRegistry FollowMeAndBalance::Registry(FollowMeAndBalance::GetName(), &FollowMeAndBalance::CreateInstance);



void FollowMeAndBalance::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void FollowMeAndBalance::run()
{

    std::string name = "GlocalTCPController_" + IceUtil::generateUUID();


    if (in.getIsSimulation())
    {
        getRobotUnit()->loadLibFromPackage("armar6_rt", "Armar6Unit");
    }


    StringVariantBaseMap config;
    config["nodeSetName"] = new Variant(in.getBalanceNodeSetName());
    config["tcpName"] = new Variant("");
    config["K p Position"] = new Variant(in.getKpPosition());
    config["K p Orientation"] = new Variant(in.getKpOrientation());

    NJointControllerInterfacePrx globalTCPController;
    globalTCPController = getRobotUnit()->createNJointControllerFromVariantConfig(RobotUnitControllerNames::NJointGlobalTCPController, name, config);
    globalTCPController->activateController();


    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        // do your calculations
        usleep(10000);
    }

    globalTCPController->deactivateController();
    while (globalTCPController->isControllerActive())
    {
        ARMARX_IMPORTANT << "waiting for controller to deactivate";
        usleep(10000);
    }
    globalTCPController->deleteController();
    ARMARX_IMPORTANT << "controller deleted";

}

//void FollowMeAndBalance::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void FollowMeAndBalance::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr FollowMeAndBalance::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new FollowMeAndBalance(stateData));
}
