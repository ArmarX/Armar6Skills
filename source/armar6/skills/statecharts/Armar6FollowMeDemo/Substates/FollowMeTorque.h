/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6Demo
 * @author     Armar6Demo ( Armar6Demo at h2t dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <armar6/skills/statecharts/Armar6FollowMeDemo/FollowMeTorque.generated.h>

namespace armarx::Armar6FollowMeDemo
{
    class FollowMeTorque :
        public FollowMeTorqueGeneratedBase < FollowMeTorque >
    {
    public:
        FollowMeTorque(const XMLStateConstructorParams& stateData):
            XMLStateTemplate < FollowMeTorque > (stateData), FollowMeTorqueGeneratedBase < FollowMeTorque > (stateData)
        {
        }

        // inherited from StateBase
        void onEnter() override;
        void run() override;
        // void onBreak() override;
        void onExit() override;

        // static functions for AbstractFactory Method
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static SubClassRegistry Registry;

        // DO NOT INSERT ANY CLASS MEMBERS,
        // use stateparameters instead,
        // if classmember are neccessary nonetheless, reset them in onEnter
    };
}
