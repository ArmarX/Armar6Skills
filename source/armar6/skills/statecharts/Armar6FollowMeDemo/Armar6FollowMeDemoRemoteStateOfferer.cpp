/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6FollowMeDemo::Armar6FollowMeDemoRemoteStateOfferer
 * @author     [Author Name] ( [Author Email] )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6FollowMeDemoRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6FollowMeDemo;

// DO NOT EDIT NEXT LINE
Armar6FollowMeDemoRemoteStateOfferer::SubClassRegistry Armar6FollowMeDemoRemoteStateOfferer::Registry(Armar6FollowMeDemoRemoteStateOfferer::GetName(), &Armar6FollowMeDemoRemoteStateOfferer::CreateInstance);



Armar6FollowMeDemoRemoteStateOfferer::Armar6FollowMeDemoRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6FollowMeDemoStatechartContext > (reader)
{
}

void Armar6FollowMeDemoRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6FollowMeDemoRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6FollowMeDemoRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6FollowMeDemoRemoteStateOfferer::GetName()
{
    return "Armar6FollowMeDemoRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6FollowMeDemoRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6FollowMeDemoRemoteStateOfferer(reader));
}
