/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HapticGraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LookAtObjectAndGetPose.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <VirtualRobot/math/Helpers.h>

#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>

using namespace armarx;
using namespace Armar6HapticGraspingGroup;

// DO NOT EDIT NEXT LINE
LookAtObjectAndGetPose::SubClassRegistry LookAtObjectAndGetPose::Registry(LookAtObjectAndGetPose::GetName(), &LookAtObjectAndGetPose::CreateInstance);

class KinematicUnitHelper
{
public:
    KinematicUnitHelper(const KinematicUnitInterfacePrx& kinUnit)
        : kinUnit(kinUnit)
    { }

    void setJointAngles(const std::map<std::string, float> jointAngles)
    {
        NameControlModeMap controlModes;
        for (const auto& pair : jointAngles)
        {
            controlModes[pair.first] = ePositionControl;
        }
        kinUnit->switchControlMode(controlModes);
        kinUnit->setJointAngles(jointAngles);
    }

    KinematicUnitInterfacePrx kinUnit;
};

void LookAtObjectAndGetPose::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

/*class MapResolver
{
public:
    MapResolver(const std::string& key)
        : key(key)
    {  }

    template<typename T>
    void checkKey(const std::map<std::string, T>& map)
    {
        if (map.count(key) == 0)
        {
            //for(const std::pair<)
        }
    }

    Eigen::Vector3f get(const std::map<std::string, ::armarx::QuaternionPtr>& map)
    {

    }

private:
    std::string key;
};*/

void LookAtObjectAndGetPose::run()
{
    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());

    getDebugDrawerTopic()->clearLayer("TryTopGrasp");
    getDebugDrawerTopic()->clearLayer("LookAtObjectAndGetPose");
    getMessageDisplay()->setMessage("Looking at object", "");

    KinematicUnitHelper kinUnitHelper(getKinematicUnit());
    kinUnitHelper.setJointAngles(in.getJointAngles());
    TimeUtil::Sleep(3);

    DebugDrawerHelper debugDrawerHelper(getDebugDrawerTopic(), "LookAtObjectAndGetPose", robot);

    auto pcaResult = getPointCloudPCA()->getResult();
    Eigen::Vector3f center = Vector3Ptr::dynamicCast(pcaResult.center)->toEigen();
    Eigen::Vector3f v1 = Vector3Ptr::dynamicCast(pcaResult.v1)->toEigen();
    Eigen::Vector3f v2 = Vector3Ptr::dynamicCast(pcaResult.v2)->toEigen();

    debugDrawerHelper.drawArrow("v1", center, v1, DrawColor {1, 0, 0, 1}, 50, 3);
    debugDrawerHelper.drawArrow("v1_neg", center, Eigen::Vector3f(-v1), DrawColor {1, 0, 0, 1}, 50, 3);
    debugDrawerHelper.drawArrow("v2", center, v2, DrawColor {0, 1, 0, 1}, 50, 3);
    debugDrawerHelper.drawArrow("v2_neg", center, Eigen::Vector3f(-v2), DrawColor {0, 1, 0, 1}, 50, 3);

    //getDebugDrawerTopic()->setArrowVisu("LookAtObjectAndGetPose", "v1", pcaResult.center, new Vector3(v1), DrawColor {1, 0, 0, 1}, 50, 3);
    //getDebugDrawerTopic()->setArrowVisu("LookAtObjectAndGetPose", "v1_neg", pcaResult.center, new Vector3(Eigen::Vector3f(-v1)), DrawColor {1, 0, 0, 1}, 50, 3);
    //getDebugDrawerTopic()->setArrowVisu("LookAtObjectAndGetPose", "v2", pcaResult.center, new Vector3(v2), DrawColor {0, 1, 0, 1}, 50, 3);
    //getDebugDrawerTopic()->setArrowVisu("LookAtObjectAndGetPose", "v2_neg", pcaResult.center, new Vector3(Eigen::Vector3f(-v2)), DrawColor {0, 1, 0, 1}, 50, 3);


    auto capturedPoints = getPointCloudPCA()->getSegmentedPoints();
    DebugDrawerPointCloud debugPointCloud;
    for (const auto& p : capturedPoints)
    {
        Eigen::Vector3f globP = math::Helpers::TransformPosition(robot->getGlobalPose(), Eigen::Vector3f(p.x, p.y, p.z));
        debugPointCloud.points.push_back(DebugDrawerPointCloudElement {globP.x(), globP.y(), globP.z()});
    }
    debugPointCloud.pointSize = 5;

    getDebugDrawerTopic()->setPointCloudVisu("LookAtObjectAndGetPose", "capturedPoints", debugPointCloud);
    //getTextToSpeech()->reportText();


    v2 = v2.normalized();
    Eigen::Vector3f v2ref = in.getReferenceV2()->toEigen().normalized();

    // v2 is shorter vector => should be aligned with hand.
    if (v2.dot(v2ref) < 0)
    {
        v2 = -v2;
    }
    float angleV2 = math::Helpers::Angle(Eigen::Vector2f(v2(0), v2(1)));

    {
        std::stringstream ss;
        ss.precision(1);
        ss << "Found " << capturedPoints.size() << " points. Center at " << std::fixed << center(0) << " " << center(1) << " " << center(2);
        ss << " Angle: " << math::Helpers::rad2deg(angleV2);
        getMessageDisplay()->setSubCaption(ss.str());
    }

    // reference grasp ori is aligned with Y axis
    float angleGraspOri = math::Helpers::Angle(Eigen::Vector2f(0, 1));
    float rotationAngle = math::Helpers::AngleModPI(angleV2 - angleGraspOri);

    ARMARX_IMPORTANT << VAROUT(angleV2);
    ARMARX_IMPORTANT << VAROUT(angleGraspOri);
    ARMARX_IMPORTANT << VAROUT(rotationAngle);

    Eigen::Matrix3f referenceOrientation = in.getReferenceOrientationMap().at(in.getPreshapeType())->toEigen();
    out.setFingerPreshape(in.getFingerPreshapeMap().at(in.getPreshapeType()));
    out.setOffsetDuringGrasp(in.getOffsetDuringGraspMap().at(in.getPreshapeType())->toEigen());

    Eigen::AngleAxisf aa = Eigen::AngleAxisf(rotationAngle, Eigen::Vector3f::UnitZ());
    Eigen::Matrix3f prePoseOri = aa.toRotationMatrix() * referenceOrientation;

    Eigen::Matrix4f prePose = math::Helpers::CreatePose(center + in.getGraspOffsetInRoot()->toEigen() + in.getPrePoseOffset()->toEigen(), prePoseOri);
    Eigen::Matrix4f preTarget = math::Helpers::CreatePose(center + in.getGraspOffsetInRoot()->toEigen() + in.getPrePoseOffset()->toEigen() + in.getPreTargetOffset()->toEigen(), referenceOrientation);

    getDebugDrawerTopic()->setPoseVisu("LookAtObjectAndGetPose", "PrePose_noOffset", new Pose(getRobot()->getGlobalPose() * prePose));


    Eigen::Matrix4f prePoseOffset = math::Helpers::CreatePose(in.getGraspOffsetInHandCoords()->toEigen(), Eigen::Matrix3f::Identity());
    ARMARX_IMPORTANT << VAROUT(prePoseOffset);
    prePose = prePose * prePoseOffset;


    getDebugDrawerTopic()->setPoseVisu("LookAtObjectAndGetPose", "PrePose", new Pose(getRobot()->getGlobalPose() * prePose));

    kinUnitHelper.setJointAngles(in.getLookAwayJointAngles());

    out.setPreGraspPose(new Pose(prePose));
    out.setPreTarget(new Pose(preTarget));

    emitSuccess();
}

//void LookAtObjectAndGetPose::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void LookAtObjectAndGetPose::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr LookAtObjectAndGetPose::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new LookAtObjectAndGetPose(stateData));
}
