/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HapticGraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "FindTable.h"

#include <ArmarXCore/core/time/TimeUtil.h>

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>

#include <RobotAPI/libraries/core/CartesianPositionController.h>

#include <VirtualRobot/math/CompositeFunctionR1R6.h>
#include <VirtualRobot/math/Helpers.h>
#include <VirtualRobot/math/Line.h>

#include <RobotAPI/libraries/core/math/MathUtils.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace math;
using namespace armarx;
using namespace armarx::math;
using namespace Armar6HapticGraspingGroup;

// DO NOT EDIT NEXT LINE
FindTable::SubClassRegistry FindTable::Registry(FindTable::GetName(), &FindTable::CreateInstance);



void FindTable::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void FindTable::run()
{
    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(in.getNodeSetName());
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();

    DatafieldRefPtr forceDf = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield(in.getFTDatefieldName()));
    const Eigen::Vector3f initialForce = forceDf->getDataField()->get<FramedDirection>()->toEigen();

    NJointCartesianVelocityControllerWithRampInterfacePrx controller = NJointCartesianVelocityControllerWithRampInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getCartesianVelocityControllerName()));
    controller->activateController();

    CartesianPositionController posController(tcp);
    posController.maxOriVel = 1;
    posController.maxPosVel = 40;
    posController.KpOri = 10;
    posController.KpPos = 10;

    Line line(tcp->getPositionInRootFrame(), in.getVelocity()->toEigen());

    IceUtil::Time start = TimeUtil::GetTime();
    Eigen::Vector3f startPosition = tcp->getPositionInRootFrame();

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        IceUtil::Time now = TimeUtil::GetTime();
        float t = (now - start).toSecondsDouble();

        Eigen::Vector3f force = forceDf->getDataField()->get<FramedDirection>()->toEigen();
        force = force - initialForce;

        Eigen::Matrix4f targetPose = Helpers::CreatePose(line.Get(t), in.getTargetOrientation()->toEigen());

        Eigen::VectorXf cv = posController.calculate(targetPose, VirtualRobot::IKSolver::All);
        controller->setTargetVelocity(cv(0), cv(1), cv(2), cv(3), cv(4), cv(5));


        if (force.norm() > in.getForceThreshold())
        {
            controller->immediateHardStop();
            emitForceDetected();
            break;
        }

        if ((tcp->getPositionInRootFrame() - startPosition).norm() > in.getMaxDistance())
        {
            controller->immediateHardStop();
            emitMaxDistanceReached();
            break;
        }

        TimeUtil::SleepMS(10);
    }
}

//void FindTable::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void FindTable::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr FindTable::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new FindTable(stateData));
}
