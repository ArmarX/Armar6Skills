/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HapticGraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ScanForObject.h"

#include <ArmarXCore/core/time/TimeUtil.h>

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>

#include <RobotAPI/libraries/core/CartesianFeedForwardPositionController.h>

#include <VirtualRobot/math/CompositeFunctionR1R6.h>
#include <VirtualRobot/math/Helpers.h>
#include <VirtualRobot/math/Line.h>
#include <VirtualRobot/math/Line.h>
#include <VirtualRobot/math/LinearInterpolatedOrientation.h>

#include <RobotAPI/libraries/core/math/MathUtils.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace math;
using namespace armarx;
using namespace Armar6HapticGraspingGroup;

// DO NOT EDIT NEXT LINE
ScanForObject::SubClassRegistry ScanForObject::Registry(ScanForObject::GetName(), &ScanForObject::CreateInstance);



void ScanForObject::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void ScanForObject::run()
{
    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(in.getNodeSetName());
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();

    DatafieldRefPtr forceDf = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield(in.getFTDatefieldName()));
    const Eigen::Vector3f initialForce = forceDf->getDataField()->get<FramedDirection>()->toEigen();

    NJointCartesianVelocityControllerWithRampInterfacePrx controller = NJointCartesianVelocityControllerWithRampInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getCartesianVelocityControllerName()));
    controller->activateController();

    CartesianFeedForwardPositionController posController(tcp);
    posController.maxOriVel = 1;
    posController.maxPosVel = in.getPositionVelocity();
    posController.KpOri = 1;
    posController.KpPos = 10;
    posController.enableFeedForward = true;

    Eigen::Vector3f scanStart = in.getScanStart()->toEigen();
    Eigen::Vector3f positionOffset = in.getPositionOffset()->toEigen();
    Eigen::Quaternionf oriQuat = in.getOrientation()->toEigenQuaternion();

    CompositeFunctionR1R6Ptr trajectory = CompositeFunctionR1R6::CreateLine(scanStart, scanStart + positionOffset, oriQuat, oriQuat, 0, in.getScanTime());

    /*LinearInterpolatedOrientation linOri(oriQuat, oriQuat, 0, 1, true);
    ARMARX_IMPORTANT << linOri.Get(0).toRotationMatrix();
    ARMARX_IMPORTANT << linOri.GetDerivative(0);

    ARMARX_IMPORTANT << linOri.Get(0.5).toRotationMatrix();
    ARMARX_IMPORTANT << linOri.GetDerivative(1);

    ARMARX_IMPORTANT << "PositionDerivative: " << trajectory->GetPositionDerivative(0);*/


    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        Eigen::VectorXf cv = posController.calculate(trajectory, 0, VirtualRobot::IKSolver::All);
        controller->setTargetVelocity(cv(0), cv(1), cv(2), cv(3), cv(4), cv(5));

        if (posController.getPositionError(trajectory, 0) < in.getPositionReachedThreshold())
        {
            posController.enableFeedForward = true;
            break;
        }
        TimeUtil::SleepMS(10);
    }

    IceUtil::Time start = TimeUtil::GetTime();
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        IceUtil::Time now = TimeUtil::GetTime();
        float t = (now - start).toSecondsDouble();

        Eigen::VectorXf cv = posController.calculate(trajectory, t, VirtualRobot::IKSolver::All);
        controller->setTargetVelocity(cv(0), cv(1), cv(2), cv(3), cv(4), cv(5));

        ARMARX_IMPORTANT << "t: " << t;

        //ARMARX_IMPORTANT << cv.transpose();
        ARMARX_IMPORTANT << "PositionDiff: " << posController.getPositionDiff(trajectory, t).transpose();

        if (t > in.getScanTime())
        {
            emitFailure();
            controller->immediateHardStop();
            break;
        }

        TimeUtil::SleepMS(10);
    }
}

//void ScanForObject::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ScanForObject::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ScanForObject::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ScanForObject(stateData));
}
