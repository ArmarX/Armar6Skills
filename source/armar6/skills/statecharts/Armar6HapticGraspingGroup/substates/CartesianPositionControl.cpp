/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HapticGraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CartesianPositionControl.h"

#include <RobotAPI/libraries/core/CartesianPositionController.h>

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6HapticGraspingGroup;

// DO NOT EDIT NEXT LINE
CartesianPositionControl::SubClassRegistry CartesianPositionControl::Registry(CartesianPositionControl::GetName(), &CartesianPositionControl::CreateInstance);



void CartesianPositionControl::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void CartesianPositionControl::run()
{
    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(in.getNodeSetName());
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();

    NJointCartesianVelocityControllerWithRampInterfacePrx controller = NJointCartesianVelocityControllerWithRampInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getCartesianVelocityControllerName()));
    controller->activateController();

    CartesianPositionController posController(tcp);
    posController.maxOriVel = in.getMaxOriVel();
    posController.maxPosVel = in.getMaxPosVel();
    posController.KpOri = in.getKpOrientation();
    posController.KpPos = in.getKpPosition();

    Eigen::Matrix4f target = in.getTargetPose()->toEigen();

    bool targetReached = false;

    IceUtil::Time start = TimeUtil::GetTime();

    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        Eigen::VectorXf cv = posController.calculate(target, VirtualRobot::IKSolver::All);
        controller->setTargetVelocity(cv(0), cv(1), cv(2), cv(3), cv(4), cv(5));

        float positionError = posController.getPositionError(target);
        float orientationError = posController.getOrientationError(target);
        ARMARX_INFO << deactivateSpam(1) << VAROUT(positionError) << " " << VAROUT(orientationError) << " " << VAROUT(cv.transpose());

        getDebugObserver()->setDebugDatafield("CartesianPositionControl", "positionError", new Variant(positionError));
        getDebugObserver()->setDebugDatafield("CartesianPositionControl", "orientationError", new Variant(orientationError));
        getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_px", new Variant(cv(0)));
        getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_py", new Variant(cv(1)));
        getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_pz", new Variant(cv(2)));
        getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_rx", new Variant(cv(3)));
        getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_ry", new Variant(cv(4)));
        getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_rz", new Variant(cv(5)));

        if (!targetReached && positionError < in.getPositionThreshold() && orientationError < in.getOrientationThreshold())
        {
            start = TimeUtil::GetTime();
            ARMARX_IMPORTANT << "target threshold reached. " << VAROUT(positionError) << " " << VAROUT(orientationError);;
            targetReached = true;
        }
        if (targetReached)
        {
            IceUtil::Time now = TimeUtil::GetTime();
            float t = (now - start).toSecondsDouble();
            if (t > in.getAdditionalControlTime())
            {
                ARMARX_IMPORTANT << "AdditionalControlTime elapsed. " << VAROUT(positionError) << " " << VAROUT(orientationError);
                if (in.getStopOnReached())
                {
                    ARMARX_IMPORTANT << "Setting velocity to zero";
                    controller->setTargetVelocity(0, 0, 0, 0, 0, 0);
                }
                emitTargetReached();
                break;
            }
        }
        TimeUtil::SleepMS(10);
    }

}

//void CartesianPositionControl::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void CartesianPositionControl::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CartesianPositionControl::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CartesianPositionControl(stateData));
}
