/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HapticGraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TryTopGrasp.h"

#include "CartesianPositionControl.h"

#include <RobotAPI/libraries/core/CartesianPositionController.h>

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/ForceTorqueHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/PositionControllerHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>
#include <VirtualRobot/math/Line.h>
#include <VirtualRobot/math/Helpers.h>
#include <VirtualRobot/math/LineStrip.h>
#include <VirtualRobot/math/LinearInterpolatedOrientation.h>
#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>

using namespace math;
using namespace armarx;
using namespace Armar6HapticGraspingGroup;

// DO NOT EDIT NEXT LINE
TryTopGrasp::SubClassRegistry TryTopGrasp::Registry(TryTopGrasp::GetName(), &TryTopGrasp::CreateInstance);



void TryTopGrasp::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}


std::string TryTopGrasp::ContactTypeToString(ContactType contactType)
{
    switch (contactType)
    {
        case ContactType::None:
            return "None";
        case ContactType::Thumb:
            return "Thumb";
        case ContactType::Fingers:
            return "Fingers";
        default:
            return "-";
    }
}

void TryTopGrasp::shapeHand(float fingers, float thumb)
{
    std::string fingersName = in.getHandJointsPrefix() + "Fingers";
    std::string thumbName = in.getHandJointsPrefix() + "Thumb";

    getKinematicUnit()->switchControlMode({{fingersName, ePositionControl}, {thumbName, ePositionControl}});
    getKinematicUnit()->setJointAngles({{fingersName, fingers}, {thumbName, thumb}});
}



void TryTopGrasp::run()
{
    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(in.getNodeSetName());
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();

    KinematicUnitHelper kinUnitHelper(getKinematicUnit());
    DebugDrawerHelper debugDrawerHelper(getDebugDrawerTopic(), "TryTopGrasp", robot);


    Eigen::Vector3f fingerPreshape = in.getFingerPreshape()->toEigen();
    shapeHand(fingerPreshape(0), fingerPreshape(1));

    std::vector<Eigen::Vector3f> handClosing;
    if (in.getFingerClosingSequence() == "JustClose")
    {
        handClosing.push_back(Eigen::Vector3f(1.00f, 1.00f, 0));
        handClosing.push_back(Eigen::Vector3f(1.00f, 1.00f, 0));
    }
    else if (in.getFingerClosingSequence() == "Controlled")
    {
        handClosing.push_back(Eigen::Vector3f(0.16f, 0.28f, 0));
        handClosing.push_back(Eigen::Vector3f(0.30f, 0.36f, 0));
        handClosing.push_back(Eigen::Vector3f(0.39f, 0.44f, 0));
        handClosing.push_back(Eigen::Vector3f(0.47f, 0.47f, 0));
        handClosing.push_back(Eigen::Vector3f(1.00f, 1.00f, 0));
        for (size_t i = 0; i < handClosing.size(); i++)
        {
            handClosing.at(i) = math::Helpers::CwiseMax(fingerPreshape, handClosing.at(i));
        }
    }
    else
    {
        throw LocalException("Unknown FingerClosingSequence ") << in.getFingerClosingSequence();
    }
    ARMARX_IMPORTANT << VAROUT(fingerPreshape);
    LineStrip handClosingSequence(handClosing, 0, in.getGraspTime());
    float handClosingSequenceStartOffset = 0;
    for (float t = 0; t < in.getGraspTime(); t += 0.1f)
    {
        if (handClosingSequence.Get(t)(0) > fingerPreshape(0) || handClosingSequence.Get(t)(1) > fingerPreshape(1))
        {
            handClosingSequenceStartOffset = t - 0.1f;
            break;
        }
    }
    ARMARX_IMPORTANT << VAROUT(handClosingSequenceStartOffset);

    //NJointCartesianVelocityControllerWithRampInterfacePrx controller = NJointCartesianVelocityControllerWithRampInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getCartesianVelocityControllerName()));
    //controller->activateController();

    VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(getRobotUnit(), in.getNodeSetName(), in.getCartesianVelocityControllerName()));

    ForceTorqueHelper ftHelper(getForceTorqueObserver(), in.getFTDatefieldName());


    Eigen::Matrix4f preTarget = in.getAbsolutePreTarget()->toEigen();
    Eigen::Matrix4f preGraspPose = in.getPreGraspPose()->toEigen();
    Eigen::Matrix4f placePose = in.getPlacePose()->toEigen();

    Eigen::Matrix3f graspOrientation = in.getGraspOrientation()->toEigen();

    const Eigen::Vector3f sourceLocalUp = graspOrientation.inverse() * Eigen::Vector3f::UnitZ();
    const Eigen::Matrix3f grapOri = math::Helpers::RotateOrientationToFitVector(preGraspPose.block<3, 3>(0, 0), sourceLocalUp, Eigen::Vector3f::UnitZ());

    Eigen::Matrix4f graspTarget = preGraspPose;
    graspTarget.block<3, 3>(0, 0) = grapOri;
    debugDrawerHelper.drawPose("RotatedGraspPose", graspTarget);
    //getDebugDrawerTopic()->setPoseVisu("TryTopGrasp", "RotatedGraspPose", new Pose(robot->getGlobalPose() * graspTarget));

    /*LinearInterpolatedOrientation graspOrientationFunc(preGraspPose.block<3, 3>(0, 0), grapOri, 0, in.getGraspTime(), true);

    for (float f = 0; f < 1.1; f += 0.1f)
    {
        Eigen::Matrix4f tmp = preGraspPose;
        tmp.block<3, 3>(0, 0) = graspOrientationFunc.Get(f * in.getGraspTime()).toRotationMatrix();
        getDebugDrawerTopic()->setPoseVisu("TryTopGrasp", "RotatedGraspPose_" + std::to_string(f), new Pose(robot->getGlobalPose() * tmp));
    }*/

    //Eigen::Matrix4f target = preGraspPose;

    std::vector<Eigen::Matrix4f> waypoints;
    waypoints.push_back(preTarget);
    waypoints.push_back(preGraspPose);
    PositionControllerHelper posHelper(tcp, velHelper, waypoints);
    posHelper.thresholdOrientationNear = in.getThresholdOrientationNear();
    posHelper.thresholdOrientationReached = in.getThresholdOrientationReached();
    posHelper.thresholdPositionNear = in.getThresholdPositionNear();
    posHelper.thresholdPositionReached = in.getThresholdPositionReached();

    posHelper.posController.maxOriVel = in.getMaxOriVel();
    posHelper.posController.maxPosVel = in.getMaxPosVel();
    posHelper.posController.KpOri = in.getKpOrientation();
    posHelper.posController.KpPos = in.getKpPosition();


    PhaseType currentPhase = PhaseType::ApproachPrePose;
    int graspTryCount = 0;

    float graspAdjustmentFactor = 1;

    ContactType lastContactType = ContactType::None;
    float lastNormalizedTorque = 0;
    int directionChangeCounter = 0;

    IceUtil::Time graspStartTime;

    std::string adjustmentDirectionStr;

    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        posHelper.update();
        //Eigen::VectorXf cv = posController.calculate(target, VirtualRobot::IKSolver::All);
        //controller->setTargetVelocity(cv(0), cv(1), cv(2), cv(3), cv(4), cv(5));

        debugDrawerHelper.drawLine("tcpToTarget", tcp->getPositionInRootFrame(), posHelper.getCurrentTargetPosition(), 5, DrawColor {0, 1, 0, 0});

        float positionError = posHelper.getPositionError();
        float orientationError = posHelper.getOrientationError();
        //ARMARX_INFO << deactivateSpam(1) << VAROUT(positionError) << " " << VAROUT(orientationError) << " " << VAROUT(cv.transpose());

        getDebugObserver()->setDebugDatafield("CartesianPositionControl", "positionError", new Variant(positionError));
        getDebugObserver()->setDebugDatafield("CartesianPositionControl", "orientationError", new Variant(orientationError));
        //getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_py", new Variant(cv(1)));
        //getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_pz", new Variant(cv(2)));
        //getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_rx", new Variant(cv(3)));
        //getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_ry", new Variant(cv(4)));
        //getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_px", new Variant(cv(0)));
        //getDebugObserver()->setDebugDatafield("CartesianPositionControl", "vel_rz", new Variant(cv(5)));


        //FramedDirectionPtr currentForcePtr = forceDf->getDataField()->get<FramedDirection>();
        //const Eigen::Vector3f currentForce = initialForce - currentForcePtr->toEigen();
        //const Eigen::Vector3f currentTorque = initialTorque - torqueDf->getDataField()->get<FramedDirection>()->toEigen();
        //bool targetReached = positionError < in.getPositionThreshold() && orientationError < in.getOrientationThreshold();
        //
        //Eigen::Matrix4f ftPose = robot->getGlobalPose().inverse() * robot->getSensor("FT R")->getGlobalPose();
        //
        //Eigen::Matrix3f forceCrossProduct;
        //forceCrossProduct <<
        //                  0, currentForce(2), -currentForce(1),
        //                  -currentForce(2), 0, currentForce(0),
        //                  currentForce(1), -currentForce(0), 0;
        //
        //Line forceLine(Eigen::Vector3f::Zero(), Eigen::Vector3f::Zero());
        //
        //forceLine = forceLine.Transform(ftPose);
        //
        //Line forceLineGlobal = forceLine.Transform(robot->getGlobalPose());
        //
        //DrawColor color;
        //color.r = 1;
        //color.g = 1;
        //color.b = 0;
        //color.a = 0.5;
        //getDebugDrawerTopic()->setLineVisu("TryTopGrasp", "forceLine", new Vector3(forceLineGlobal.Get(-100)), new Vector3(forceLineGlobal.Get(100)), 5, color);
        //
        //bool forceExceeded = currentForce.norm() > in.getForceThreshold();

        PhaseType nextPhase = currentPhase;

        //bool targetReached = positionError < in.getPositionThreshold() && orientationError < in.getOrientationThreshold();
        bool targetReached = posHelper.isFinalTargetReached();

        //FramedDirectionPtr currentForcePtr = forceDf->getDataField()->get<FramedDirection>();
        //const Eigen::Vector3f currentForce = initialForce - currentForcePtr->toEigen();
        //const Eigen::Vector3f currentTorque = initialTorque - torqueDf->getDataField()->get<FramedDirection>()->toEigen();
        const Eigen::Vector3f currentForce = ftHelper.getForce();
        const Eigen::Vector3f currentTorque = ftHelper.getTorque();

        Eigen::Vector3f force = currentForce * in.getForceAxis();
        Eigen::Vector3f torque = currentTorque * in.getTorqueAxis();

        float normalizedTorque = force.norm() < 0.01f ? 0 : torque.norm() / force.norm();

        getDebugObserver()->setDebugDatafield("TryTopGrasp", "force_x", new Variant(currentForce(0)));
        getDebugObserver()->setDebugDatafield("TryTopGrasp", "force_y", new Variant(currentForce(1)));
        getDebugObserver()->setDebugDatafield("TryTopGrasp", "force_z", new Variant(currentForce(2)));
        getDebugObserver()->setDebugDatafield("TryTopGrasp", "torque_x", new Variant(currentTorque(0)));
        getDebugObserver()->setDebugDatafield("TryTopGrasp", "torque_y", new Variant(currentTorque(1)));
        getDebugObserver()->setDebugDatafield("TryTopGrasp", "torque_z", new Variant(currentTorque(2)));

        getDebugObserver()->setDebugDatafield("TryTopGrasp", "force", new Variant(force.norm()));
        getDebugObserver()->setDebugDatafield("TryTopGrasp", "torque", new Variant(torque.norm()));

        getDebugObserver()->setDebugDatafield("TryTopGrasp", "normalizedTorque", new Variant(normalizedTorque));
        getDebugObserver()->setDebugDatafield("TryTopGrasp", "lastNormalizedTorque", new Variant(lastNormalizedTorque));
        getDebugObserver()->setDebugDatafield("TryTopGrasp", "graspAdjustmentFactor", new Variant(graspAdjustmentFactor));
        getDebugObserver()->setDebugDatafield("TryTopGrasp", "directionChangeCounter", new Variant(directionChangeCounter));

        bool forceExceeded = currentForce.norm() > in.getForceThreshold();
        ContactType contactType = ContactType::None;
        if (force.norm() > in.getForceThreshold())
        {
            contactType = normalizedTorque < in.getThumbFingerThreshold() ? ContactType::Thumb : ContactType::Fingers;
        }

        getDebugObserver()->setDebugDatafield("TryTopGrasp", "contactType", new Variant(ContactTypeToString(contactType)));
        getDebugObserver()->setDebugDatafield("TryTopGrasp", "lastContactType", new Variant(ContactTypeToString(lastContactType)));


        if (currentPhase == PhaseType::ApproachPrePose)
        {
            getMessageDisplay()->setMessage("Approaching Pre Pose", posHelper.getStatusText());
            if (targetReached)
            {
                ARMARX_IMPORTANT << "PrePose reached";
                posHelper.setTarget(Helpers::TranslatePose(preGraspPose, in.getApprochVector()->toEigen()));
                velHelper->controller->setKpJointLimitAvoidance(0);
                velHelper->controller->setJointLimitAvoidanceScale(0);
                ftHelper.setZero();
                nextPhase = PhaseType::ApproachObject;
            }
        }
        if (currentPhase == PhaseType::ApproachObject)
        {
            getMessageDisplay()->setMessage("Approaching Object", posHelper.getStatusText());
            Eigen::Vector3f tcpPos = tcp->getPositionInRootFrame();

            if (in.getIsSimulation() && graspTryCount == 1 && tcpPos(2) < in.getHeightThreshold() + 20)
            {
                ARMARX_IMPORTANT << "Simulating force exceeded";
                forceExceeded = true;
            }

            if (targetReached)
            {
                ARMARX_IMPORTANT << "targetReached";
                velHelper->controller->immediateHardStop();
                emitFailure();
                break;
            }
            if (forceExceeded)
            {
                ContactType previousContactType = lastContactType;
                lastContactType = contactType;

                if (previousContactType != ContactType::None && previousContactType != lastContactType)
                {
                    directionChangeCounter++;
                    graspAdjustmentFactor = pow(in.getGraspAdjustmentFactor(), directionChangeCounter);
                    ARMARX_IMPORTANT << "rdirectionChangeCounter: " << directionChangeCounter;
                    ARMARX_IMPORTANT << "graspAdjustmentFactor: " << graspAdjustmentFactor;
                }

                lastNormalizedTorque = normalizedTorque;


                velHelper->controller->immediateHardStop();
                if (directionChangeCounter >= in.getRequiredDirectionChanges())
                {
                    nextPhase = PhaseType::Grasp;
                    Eigen::Matrix4f target = tcp->getPoseInRootFrame();
                    //target(2, 3) = fmax(target(2, 3), in.getGraspMinZHeight());
                    target = ::math::Helpers::TranslatePose(target, in.getOffsetDuringGrasp()->toEigen());
                    posHelper.setTarget(target);
                    graspStartTime = TimeUtil::GetTime();
                }
                else
                {

                    float adjustmentDirection = contactType == ContactType::Thumb ? -1 : 1;
                    adjustmentDirectionStr = adjustmentDirection > 0 ? "forward" : "back";
                    preGraspPose.block<3, 1>(0, 3) += graspAdjustmentFactor * adjustmentDirection * in.getGraspAdjustment()->toEigen();
                    posHelper.setTarget(preGraspPose);

                    ARMARX_IMPORTANT << "switching to retract and adjust";

                    nextPhase = PhaseType::Retract;
                }
            }
        }
        if (currentPhase == PhaseType::Retract)
        {
            getMessageDisplay()->setMessage("Adjusting " + adjustmentDirectionStr, posHelper.getStatusText());
            if (targetReached)
            {
                graspTryCount++;
                ARMARX_IMPORTANT << "switching to approach, Try nr. " << graspTryCount;
                nextPhase = PhaseType::ApproachObject;
                graspTarget = preGraspPose;
                graspTarget.block<3, 1>(0, 3) += in.getApprochVector()->toEigen();
                posHelper.setTarget(graspTarget);
            }
        }
        if (currentPhase == PhaseType::Grasp)
        {
            IceUtil::Time now = TimeUtil::GetTime();
            float elapsedGraspTime = (now - graspStartTime).toSecondsDouble() + handClosingSequenceStartOffset;
            Eigen::Vector3f handShape = handClosingSequence.Get(Helpers::Clamp(0, in.getGraspTime(), elapsedGraspTime));
            shapeHand(handShape(0), handShape(1));
            //graspTarget.block<3, 3>(0, 0) = graspOrientationFunc.Get(elapsedGraspTime).toRotationMatrix();

            // TODO: adjust ori based on height!
            //posHelper.setTarget(graspTarget);

            {
                std::stringstream ss;
                ss.precision(1);
                ss << elapsedGraspTime << "s Hand shape: (" << handShape(0) << ", " << handShape(1) << ")";
                getMessageDisplay()->setMessage("Grasping", ss.str());
            }

            if (elapsedGraspTime > in.getGraspTime())
            {
                nextPhase = PhaseType::Lift;
                Eigen::Matrix4f target = tcp->getPoseInRootFrame();
                target.block<3, 1>(0, 3) += in.getLiftVector()->toEigen();
                posHelper.setTarget(target);

                posHelper.addWaypoint(placePose);
                posHelper.addWaypoint(Helpers::TranslatePose(placePose, in.getPlaceVector()->toEigen()));
            }
        }
        if (currentPhase == PhaseType::Lift)
        {
            {
                std::stringstream ss;
                ss << "Force: " << currentForce.norm();
                getMessageDisplay()->setMessage("Lifting", ss.str());
            }
            if (posHelper.currentWaypointIndex > 0)
            {
                ftHelper.setZero();
                nextPhase = PhaseType::Replace;
            }
        }
        if (currentPhase == PhaseType::Replace)
        {
            if (!posHelper.isLastWaypoint())
            {
                ftHelper.setZero();
            }
            {
                std::stringstream ss;
                ss << "Force: " << currentForce.norm();
                getMessageDisplay()->setMessage("Replacing", ss.str());
            }
            if (forceExceeded)
            {
                velHelper->controller->immediateHardStop();
                shapeHand(0, 0);
                posHelper.setTarget(Helpers::TranslatePose(placePose, Eigen::Vector3f(0, 0, 100)));
                posHelper.addWaypoint(in.getRetractAfterPlacePose()->toEigen());
                posHelper.addWaypoint(in.getFinalPrePose()->toEigen());
                posHelper.addWaypoint(in.getFinalPose()->toEigen());
                velHelper->controller->setJointLimitAvoidanceScale(2);
                velHelper->controller->setKpJointLimitAvoidance(1);
                nextPhase = PhaseType::RetractAfterPlace;
                kinUnitHelper.setJointAngles(in.getLookAtObjectJointAngles());

            }
            if (targetReached)
            {
                velHelper->controller->immediateHardStop();
                emitFailure();
                return;
            }
        }
        if (currentPhase == PhaseType::RetractAfterPlace)
        {
            getMessageDisplay()->setMessage("Move back", posHelper.getStatusText());
            if (targetReached)
            {
                velHelper->controller->immediateHardStop();
                emitSuccess();
                getMessageDisplay()->setMessage("", "");
                return;
            }
        }

        currentPhase = nextPhase;

        TimeUtil::SleepMS(10);
    }
}

//void TryTopGrasp::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void TryTopGrasp::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TryTopGrasp::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TryTopGrasp(stateData));
}
