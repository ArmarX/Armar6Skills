/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HapticGraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ReactiveGraspingWithVision.h"

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>

#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6HapticGraspingGroup;

// DO NOT EDIT NEXT LINE
ReactiveGraspingWithVision::SubClassRegistry ReactiveGraspingWithVision::Registry(ReactiveGraspingWithVision::GetName(), &ReactiveGraspingWithVision::CreateInstance);



void ReactiveGraspingWithVision::onEnter()
{
    NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController(in.getCartesianVelocityControllerName());
    if (controller)
    {
        controller->deactivateController();
        while (controller->isControllerActive())
        {
            TimeUtil::SleepMS(1);
        }
        controller->deleteController();

        TimeUtil::SleepMS(50);
    }
}

void ReactiveGraspingWithVision::run()
{
    NJointCartesianVelocityControllerWithRampConfigPtr config = new NJointCartesianVelocityControllerWithRampConfig(in.getNodeSetName(), "", CartesianSelectionMode::eAll,
            500, 1, 2, in.getKpAvoidJointLimits(), in.getJointLimitAvoidanceScale());
    NJointControllerInterfacePrx ctrl = getRobotUnit()->createNJointController("NJointCartesianVelocityControllerWithRamp", in.getCartesianVelocityControllerName(), config);

    if (!in.getIsSimulation())
    {
        devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx handController = devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getHandControllerName()));
        ARMARX_CHECK_EXPRESSION(handController);
        handController->activateController();
        handController->setTargetsWithPwm(1, 1, 1, 1);
    }

    while (!isRunningTaskStopped())
    {
        TimeUtil::SleepMS(1);
    }

    ctrl->deactivateController();
    while (ctrl->isControllerActive())
    {
        TimeUtil::SleepMS(1);
    }
    ctrl->deleteController();
}

//void ReactiveGraspingWithVision::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ReactiveGraspingWithVision::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ReactiveGraspingWithVision::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ReactiveGraspingWithVision(stateData));
}
