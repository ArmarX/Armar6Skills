/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HapticGraspingGroup::Armar6HapticGraspingGroupRemoteStateOfferer
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6HapticGraspingGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6HapticGraspingGroup;

// DO NOT EDIT NEXT LINE
Armar6HapticGraspingGroupRemoteStateOfferer::SubClassRegistry Armar6HapticGraspingGroupRemoteStateOfferer::Registry(Armar6HapticGraspingGroupRemoteStateOfferer::GetName(), &Armar6HapticGraspingGroupRemoteStateOfferer::CreateInstance);



Armar6HapticGraspingGroupRemoteStateOfferer::Armar6HapticGraspingGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6HapticGraspingGroupStatechartContext > (reader)
{
}

void Armar6HapticGraspingGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6HapticGraspingGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6HapticGraspingGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6HapticGraspingGroupRemoteStateOfferer::GetName()
{
    return "Armar6HapticGraspingGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6HapticGraspingGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6HapticGraspingGroupRemoteStateOfferer(reader));
}
