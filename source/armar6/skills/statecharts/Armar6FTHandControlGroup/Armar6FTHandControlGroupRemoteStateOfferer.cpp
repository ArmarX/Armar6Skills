/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6FTHandControlGroup::Armar6FTHandControlGroupRemoteStateOfferer
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6FTHandControlGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6FTHandControlGroup;

// DO NOT EDIT NEXT LINE
Armar6FTHandControlGroupRemoteStateOfferer::SubClassRegistry Armar6FTHandControlGroupRemoteStateOfferer::Registry(Armar6FTHandControlGroupRemoteStateOfferer::GetName(), &Armar6FTHandControlGroupRemoteStateOfferer::CreateInstance);



Armar6FTHandControlGroupRemoteStateOfferer::Armar6FTHandControlGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6FTHandControlGroupStatechartContext > (reader)
{
}

void Armar6FTHandControlGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6FTHandControlGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6FTHandControlGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6FTHandControlGroupRemoteStateOfferer::GetName()
{
    return "Armar6FTHandControlGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6FTHandControlGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6FTHandControlGroupRemoteStateOfferer(reader));
}
