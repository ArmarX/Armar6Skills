/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BimanualManipulationGroup
 * @author     armar-user ( armar-user at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SwitchDMPWithForceStop.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <armarx/control/deprecated_njoint_mp_controller/bimanual/ObjLevelControllerInterface.h>
#include "Helpers.h"
#include <RobotAPI/libraries/core/FramedPose.h>

namespace armarx::Armar6BimanualManipulationGroup
{
    // DO NOT EDIT NEXT LINE
    SwitchDMPWithForceStop::SubClassRegistry SwitchDMPWithForceStop::Registry(SwitchDMPWithForceStop::GetName(), &SwitchDMPWithForceStop::CreateInstance);

    void SwitchDMPWithForceStop::onEnter()
    {
        // put your user code for the enter-point here
        // execution time should be short (<100ms)
    }

    void SwitchDMPWithForceStop::run()
    {
        NJointBimanualObjLevelControllerInterfacePrx bimanualObjLevelController;
        NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController("BimanualObjLevelController");
        if (controller)
        {
            bimanualObjLevelController = NJointBimanualObjLevelControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("BimanualObjLevelController"));
        }
        else
        {
            emitFailure();
        }


        if (in.isDMPFileSet())
        {
            bimanualObjLevelController->learnDMPFromFiles(in.getDMPFile());
        }

        if (in.isMPWeightsSet())
        {
            std::vector<std::vector<double> > weights = Helpers::getWeightsFromFlatVector(in.getMPWeights(), 7);
            bimanualObjLevelController->setMPWeights(weights);
        }

        if (in.isMPRotWeightsSet())
        {
            std::vector<std::vector<double> > weights = Helpers::getWeightsFromFlatVector(in.getMPRotWeights(), 4);
            bimanualObjLevelController->setMPRotWeights(weights);
        }

        bimanualObjLevelController->removeAllViaPoints();
        if (in.isViaPointsSet())
        {
            std::map<std::string, armarx::PosePtr> viaPoseMap = in.getViaPoints();

            for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
                 it != viaPoseMap.end(); ++it)
            {
                double canVal = std::stod(it->first);
                bimanualObjLevelController->setViaPoints(canVal, Helpers::pose2dvec(it->second->toEigen()));

            }
        }


        std::vector<double> dmpGoals = Helpers::pose2dvec(in.getDMPGoals()->toEigen());

        if (in.isDMPStartsSet())
        {
            std::vector<double> dmpStarts = Helpers::pose2dvec(in.getDMPStarts()->toEigen());
            bimanualObjLevelController->runDMPWithVirtualStart(dmpStarts, dmpGoals, in.getTimeDuration());

        }
        else
        {
            bimanualObjLevelController->runDMP(dmpGoals, in.getTimeDuration());
        }

        // get force torque sensor
        VirtualRobot::RobotPtr localRobot = getLocalRobot();
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT"));
        DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT"));
        Eigen::Vector3f initialForceLeft;
        Eigen::Vector3f initialForceRight;
        {
            FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
            initialForceRight = forcePtr->toRootEigen(localRobot);
        }
        {
            FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
            initialForceLeft = forcePtr->toRootEigen(localRobot);
        }


        float forceIntegral = 0;
        Eigen::Vector3f forceLeft(0, 0, 0);
        Eigen::Vector3f forceRight(0, 0, 0);

        float filterWeight = in.getForceFilterWeight();
        while (!isRunningTaskStopped()) // stop run function if returning true
        {
            FramedDirectionPtr forceRightPtr = forceDfRight->getDataField()->get<FramedDirection>();
            forceRight = (1 - filterWeight) * forceRight + filterWeight * (forceRightPtr->toRootEigen(localRobot) - initialForceRight);

            FramedDirectionPtr forceLeftPtr = forceDfLeft->getDataField()->get<FramedDirection>();
            forceLeft = (1 - filterWeight) * forceLeft + filterWeight * (forceLeftPtr->toRootEigen(localRobot) - initialForceLeft);


            float forceMag = fabs(forceLeft(2) + forceRight(2));
            if (forceMag > 2)
            {
                forceIntegral += forceMag * 0.01;
            }
            else
            {
                forceIntegral = 0;
            }

            if (bimanualObjLevelController->isFinished() or forceIntegral > in.getForceThreshold())
            {
                break;
            }

        }



        emitSuccess();

    }





    //void SwitchDMPWithForceStop::onBreak()
    //{
    //    // put your user code for the breaking point here
    //    // execution time should be short (<100ms)
    //}

    void SwitchDMPWithForceStop::onExit()
    {
        // put your user code for the exit point here
        // execution time should be short (<100ms)
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr SwitchDMPWithForceStop::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new SwitchDMPWithForceStop(stateData));
    }
}
