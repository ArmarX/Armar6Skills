/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BimanualManipulationGroup::Armar6BimanualManipulationGroupRemoteStateOfferer
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6BimanualManipulationGroupRemoteStateOfferer.h"

namespace armarx::Armar6BimanualManipulationGroup
{

    // DO NOT EDIT NEXT LINE
    Armar6BimanualManipulationGroupRemoteStateOfferer::SubClassRegistry Armar6BimanualManipulationGroupRemoteStateOfferer::Registry(Armar6BimanualManipulationGroupRemoteStateOfferer::GetName(), &Armar6BimanualManipulationGroupRemoteStateOfferer::CreateInstance);



    Armar6BimanualManipulationGroupRemoteStateOfferer::Armar6BimanualManipulationGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
        XMLRemoteStateOfferer < Armar6BimanualManipulationGroupStatechartContext > (reader)
    {
    }

    void Armar6BimanualManipulationGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
    {

    }

    void Armar6BimanualManipulationGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
    {

    }

    void Armar6BimanualManipulationGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
    {

    }

    // DO NOT EDIT NEXT FUNCTION
    std::string Armar6BimanualManipulationGroupRemoteStateOfferer::GetName()
    {
        return "Armar6BimanualManipulationGroupRemoteStateOfferer";
    }

    // DO NOT EDIT NEXT FUNCTION
    XMLStateOffererFactoryBasePtr Armar6BimanualManipulationGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
    {
        return XMLStateOffererFactoryBasePtr(new Armar6BimanualManipulationGroupRemoteStateOfferer(reader));
    }
}
