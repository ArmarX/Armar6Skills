/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BimanualManipulationGroup
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BimanualObjectLevelVelControl.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <armarx/control/deprecated_njoint_mp_controller/bimanual/ObjLevelControllerInterface.h>
#include "Helpers.h"
#include <ArmarXCore/core/time/TimeUtil.h>

namespace armarx::Armar6BimanualManipulationGroup
{
    // DO NOT EDIT NEXT LINE
    BimanualObjectLevelVelControl::SubClassRegistry BimanualObjectLevelVelControl::Registry(BimanualObjectLevelVelControl::GetName(), &BimanualObjectLevelVelControl::CreateInstance);

    void BimanualObjectLevelVelControl::onEnter()
    {
        // put your user code for the enter-point here
        // execution time should be short (<100ms)
    }

    void BimanualObjectLevelVelControl::run()
    {

        std::string controllerName;
        if (in.isControllerNameSet())
        {
            controllerName = in.getControllerName();
        }
        else
        {
            controllerName = "BimanualObjLevelVelController";
        }
        NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController(controllerName);
        if (controller)
        {
            controller->deactivateAndDeleteController();
            TimeUtil::SleepMS(2000);
        }

        int kernelSize = 10;
        std::string dmpMode = "Linear";
        std::string dmpType = "Discrete";
        float jointLimitAvoidanceKp = 0.0;
        NJointBimanualObjLevelVelControllerConfigPtr config = new NJointBimanualObjLevelVelControllerConfig
        (
            kernelSize,
            dmpMode,
            dmpType,
            in.getTimeDuration(),
            in.getLeftDesiredJointValues(),
            in.getRightDesiredJointValues(),
            in.getKpImpedance(),
            in.getKdImpedance(),
            in.getKnull(),
            in.getDnull(),
            in.getJointVelLimit(),
            jointLimitAvoidanceKp
        );


        out.setControllerName(controllerName);


        NJointBimanualObjLevelVelControllerInterfacePrx bimanualObjLevelController
            = NJointBimanualObjLevelVelControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointBimanualObjLevelVelController", controllerName, config));

        std::vector<std::string> fileNames = in.getArmMotionFileName();
        bimanualObjLevelController->learnDMPFromFiles(fileNames);
        std::vector<double> dmpGoals = Helpers::pose2dvec(in.getDMPGoals()->toEigen());

        if (in.isViaPointsSet())
        {
            std::map<std::string, armarx::PosePtr> viaPoseMap = in.getViaPoints();

            for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
                 it != viaPoseMap.end(); ++it)
            {
                double canVal = std::stod(it->first);
                bimanualObjLevelController->setViaPoints(canVal, Helpers::pose2dvec(it->second->toEigen()));

            }
        }

        bimanualObjLevelController->activateController();
        bimanualObjLevelController->runDMP(dmpGoals, in.getTimeDuration());

        while (!isRunningTaskStopped()) // stop run function if returning true
        {
            if (bimanualObjLevelController->isFinished())
            {
                break;
            }
        }

        if (in.getIsStopAfterDMPFinished())
        {
            bimanualObjLevelController->deactivateController();
            while (bimanualObjLevelController->isControllerActive())
            {
                usleep(10000);
            }
            bimanualObjLevelController->deleteController();
        }

        emitSuccess();

    }

    //void BimanualObjectLevelVelControl::onBreak()
    //{
    //    // put your user code for the breaking point here
    //    // execution time should be short (<100ms)
    //}

    void BimanualObjectLevelVelControl::onExit()
    {
        // put your user code for the exit point here
        // execution time should be short (<100ms)
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr BimanualObjectLevelVelControl::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new BimanualObjectLevelVelControl(stateData));
    }
}
