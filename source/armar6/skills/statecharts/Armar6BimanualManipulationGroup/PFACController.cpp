/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BimanualManipulationGroup
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PFACController.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <armarx/control/deprecated_njoint_mp_controller/bimanual/ForceMPControllerInterface.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include "Helpers.h"

namespace armarx::Armar6BimanualManipulationGroup
{
    // DO NOT EDIT NEXT LINE
    PFACController::SubClassRegistry PFACController::Registry(PFACController::GetName(), &PFACController::CreateInstance);



    void PFACController::onEnter()
    {
        // put your user code for the enter-point here
        // execution time should be short (<100ms)
    }

    void PFACController::run()
    {
        //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");

        NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController("BimanualForceController");
        if (controller)
        {
            if (controller->isControllerActive())
            {
                controller->deactivateController();
                while (controller->isControllerActive())
                {
                    TimeUtil::SleepMS(10);
                }
            }
            controller->deleteController();
            TimeUtil::SleepMS(10);
        }

        // DMP parameters
        int kernelSize = 50;
        std::string dmpMode = "Linear";
        std::string dmpType = "Discrete";
        double phaseL = 100;
        double phaseK = 1000;
        double phaseDist0 = 1000;
        double phaseDist1 = 1000;
        double phaseKpPos = 2;
        double phaseKpOri = 0.1;
        double posToOriRatio = 10;
        double timeDuration = in.getTimeDuration();


        std::vector<double> boxInitialPose = in.getBoxInitialPoseVec();
        if (in.getIsBoxInHand())
        {
            VirtualRobot::RobotPtr localRobot = getLocalRobot();
            RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

            Eigen::Matrix4f leftPose = localRobot->getRobotNodeSet("LeftArm")->getTCP()->getPoseInRootFrame();
            Eigen::Matrix4f rightPose = localRobot->getRobotNodeSet("RightArm")->getTCP()->getPoseInRootFrame();

            VirtualRobot::MathTools::Quaternion boxOri = VirtualRobot::MathTools::eigen4f2quat(leftPose);
            Eigen::Vector3f boxPosi = (leftPose.block<3, 1>(0, 3) + rightPose.block<3, 1>(0, 3)) / 2;

            for (size_t i = 0; i < 3; ++i)
            {
                boxInitialPose[i] = boxPosi(i) * 0.001;
            }
            boxInitialPose[3] = boxOri.w;
            boxInitialPose[4] = boxOri.x;
            boxInitialPose[5] = boxOri.y;
            boxInitialPose[6] = boxOri.z;
        }

        float boxWidth = in.getBoxWidth();
        std::vector<float> leftDesiredJointValues = in.getLeftDesiredJointValues();
        std::vector<float> rightDesiredJointValues  = in.getRightDesiredJointValues();

        std::vector<float> KpImpedance = in.getKpImpedance();
        std::vector<float> KdImpedance = in.getKdImpedance();
        std::vector<float> KpAdmittance = in.getKpAdmittance();
        std::vector<float> KdAdmittance = in.getKdAdmittance();
        std::vector<float> KmAdmittance = in.getKmAdmittance();
        std::vector<float> KmPID = in.getKmPID();

        std::vector<float> targetWrench = in.getTargetWrench();
        std::vector<float> forceP = in.getForceP();
        std::vector<float> forceI = in.getForceI();
        std::vector<float> forceD = in.getForceD();
        std::vector<float> forcePIDLimits = in.getForcePIDLimits();

        //    float filterCoeff = exp(-1.0f / in.getSamplingFrequency() * (2.0f * M_PI * in.getDampingFrequency()) / (pow(10.0f, in.getDampingIntensity() / -10.0f)));
        //    ARMARX_IMPORTANT << "filter coeff. " << filterCoeff;
        float filterCoeff = in.getFilterCoefficient();

        float massLeft = in.getMassLeft();
        std::vector<float> CoMVecLeft = in.getCoMVecLeft();
        std::vector<float> offsetLeft = in.getOffsetVecLeft();
        std::vector<float> forceOffsetLeft { offsetLeft[0], offsetLeft[1], offsetLeft[2]};
        std::vector<float> torqueOffsetLeft { offsetLeft[3], offsetLeft[4], offsetLeft[5]};

        float massRight = in.getMassRight();
        std::vector<float> CoMVecRight = in.getCoMVecRight();
        std::vector<float> offsetRight = in.getOffsetVecRight();
        std::vector<float> forceOffsetRight { offsetRight[0], offsetRight[1], offsetRight[2]};
        std::vector<float> torqueOffsetRight { offsetRight[3], offsetRight[4], offsetRight[5]};

        float knull = in.getKnull();
        float dnull = in.getDnull();
        float torqueLimit = in.getTorqueLimit();

        float forceThreshold = in.getForceThreshold();

        NJointBimanualForceControllerConfigPtr config = new NJointBimanualForceControllerConfig
        (
            kernelSize,
            dmpMode,
            dmpType,
            phaseL,
            phaseK,
            phaseDist0,
            phaseDist1,
            phaseKpPos,
            phaseKpOri,
            posToOriRatio,
            timeDuration,
            boxInitialPose,
            boxWidth,
            leftDesiredJointValues,
            rightDesiredJointValues,
            KpImpedance,
            KdImpedance,
            KpAdmittance,
            KdAdmittance,
            KmAdmittance,
            KmPID,
            targetWrench,
            forceP,
            forceI,
            forceD,
            forcePIDLimits,
            filterCoeff,
            massLeft,
            CoMVecLeft,
            forceOffsetLeft,
            torqueOffsetLeft,
            massRight,
            CoMVecRight,
            forceOffsetRight,
            torqueOffsetRight,
            knull,
            dnull,
            torqueLimit,
            forceThreshold
        );


        NJointBimanualForceControllerInterfacePrx bimanualForceController
            = NJointBimanualForceControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointBimanualForceController", "BimanualForceController", config));



        std::vector<std::string> fileNames = in.getArmMotionFileName();
        bimanualForceController->learnDMPFromFiles(fileNames);
        std::vector<double> dmpGoals = Helpers::pose2dvec(in.getDMPGoals()->toEigen());

        if (in.isViaPointsSet())
        {
            std::map<std::string, armarx::PosePtr> viaPoseMap = in.getViaPoints();

            for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
                 it != viaPoseMap.end(); ++it)
            {
                double canVal = std::stod(it->first);
                bimanualForceController->setViaPoints(canVal, Helpers::pose2dvec(it->second->toEigen()));

            }
        }


        bimanualForceController->activateController();


        usleep(in.getWaitTime() * 1000000);

        bimanualForceController->runDMP(dmpGoals);

        while (!isRunningTaskStopped()) // stop run function if returning true
        {
            if (bimanualForceController->isFinished())
            {
                break;
            }
        }

        if (in.getIsStopAfterDMPFinished())
        {
            bimanualForceController->deactivateController();
            while (bimanualForceController->isControllerActive())
            {
                usleep(10000);
            }
            bimanualForceController->deleteController();
        }
        emitSuccess();

    }

    //void PFACController::onBreak()
    //{
    //    // put your user code for the breaking point here
    //    // execution time should be short (<100ms)
    //}

    void PFACController::onExit()
    {
        // put your user code for the exit point here
        // execution time should be short (<100ms)
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr PFACController::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new PFACController(stateData));
    }
}
