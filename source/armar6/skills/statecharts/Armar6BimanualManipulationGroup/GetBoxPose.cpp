/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BimanualManipulationGroup
 * @author     armar-user ( armar-user at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// #include <pcl/visualization/pcl_visualizer.h>

#include "GetBoxPose.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>



#include <pcl/features/moment_of_inertia_estimation.h>
#include <VisionX/interface/core/PointCloudProviderInterface.h>

#include <pcl/segmentation/sac_segmentation.h>

#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>

#include <VisionX/components/pointcloud_core/PointCloudConversions.h>

#include "Armar6BimanualManipulationGroupStatechartContext.generated.h"


#include <RobotAPI/components/ArViz/Client/Client.h>

#include <pcl/common/transforms.h>
#include <armar6/skills/components/Armar6GraspProvider/FitRectangle.h>

#include <pcl/filters/statistical_outlier_removal.h>

namespace armarx
{

    namespace Armar6BimanualManipulationGroup
    {
        // DO NOT EDIT NEXT LINE
        GetBoxPose::SubClassRegistry GetBoxPose::Registry(GetBoxPose::GetName(), &GetBoxPose::CreateInstance);

        void GetBoxPose::onEnter()
        {

        }

        void GetBoxPose::run()
        {


            std::string providerName = in.getProviderName();
            Armar6BimanualManipulationGroupStatechartContext* context = getContext<Armar6BimanualManipulationGroupStatechartContext>();


            viz::Client arviz(*context);


            ARMARX_INFO << "Getting point cloud from: " <<  VAROUT(providerName);
            visionx::PointCloudProviderInterfacePrx providerPrx;
            try
            {
                providerPrx = context->getIceManager()->getProxy<visionx::PointCloudProviderInterfacePrx>(providerName);
            }
            catch (...)
            {

            }

            if (!providerPrx)
            {
                emitFailure();
            }

            // retrieve point cloud
            pcl::PointCloud<PointT>::Ptr inputCloudPtr(new pcl::PointCloud<PointT>());

            visionx::MetaPointCloudFormatPtr pointCloudFormat = new visionx::MetaPointCloudFormat() ;
            std::vector<Ice::Byte> blob = providerPrx->getPointCloud(pointCloudFormat);

            pointCloudFormat = providerPrx->getPointCloudFormat();

            void** bufferPtr = reinterpret_cast<void**>(&blob);
            visionx::tools::convertToPCL(bufferPtr, pointCloudFormat, inputCloudPtr);

            VirtualRobot::RobotPtr robot = getLocalRobot();
            RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());


            pcl::PassThrough<PointT> filter;
            filter.setInputCloud(inputCloudPtr);
            filter.setFilterFieldName("z");
            filter.setFilterLimits(in.getTableHeight(), 1500);
            filter.filter(*inputCloudPtr);

            pcl::SACSegmentation<PointT> seg;
            seg.setOptimizeCoefficients(true);
            seg.setModelType(pcl::SACMODEL_PERPENDICULAR_PLANE);
            seg.setMethodType(pcl::SAC_RANSAC);
            seg.setMaxIterations(1000);
            seg.setDistanceThreshold(30.0);
            seg.setAxis(Eigen::Vector3f::UnitZ());
            seg.setEpsAngle(10.0 / 180.0 * M_PI);
            seg.setProbability(0.99);

            if (isRunningTaskStopped())
            {
                emitFailure();
                return;
            }


            pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
            pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
            seg.setInputCloud(inputCloudPtr);
            seg.segment(*inliers, *coefficients);

            pcl::PointCloud<PointT>::Ptr plane(new pcl::PointCloud<PointT>());
            pcl::ExtractIndices<PointT> extract;
            extract.setInputCloud(inputCloudPtr);
            extract.setIndices(inliers);
            extract.setNegative(false);
            extract.filter(*plane);


            Eigen::Matrix4f robotGlobalPose = robot->getGlobalPose();
            pcl::PointCloud<PointT>::Ptr global(new pcl::PointCloud<PointT>());
            pcl::transformPointCloud(*plane, *global, robotGlobalPose);

            viz::PointCloud pc = viz::PointCloud("points");
            pc.pointCloud(*global);
            viz::Layer layerPlannedInGlobal = arviz.layer("planned_in_global");
            layerPlannedInGlobal.add(pc);


            pcl::PointCloud<PointT>::Ptr cloud_filtered(new pcl::PointCloud<PointT>());
            pcl::StatisticalOutlierRemoval<PointT> sor;
            sor.setInputCloud(plane);
            sor.setMeanK(50);
            sor.setStddevMulThresh(1.0);
            sor.filter(*cloud_filtered);

            pc = viz::PointCloud("points");
            pc.pointCloud(*cloud_filtered).pose(robotGlobalPose);
            viz::Layer layerFilteredInGlobal = arviz.layer("filtered_in_global");
            layerFilteredInGlobal.add(pc);

            arviz.commit({layerPlannedInGlobal, layerFilteredInGlobal});


            if (isRunningTaskStopped())
            {
                emitFailure();
                return;
            }

            std::vector < Eigen::Vector3f > allPoints;
            for (const PointT& p : plane->points)
            {
                allPoints.push_back(p.getVector3fMap());
            }
            math::FitRectangle fitBox(90);
            math::Box3D box = fitBox.Fit(allPoints);
            const Eigen::Matrix4f boxPose = box.GetPose();
            const Eigen::Vector3f boxDimensions = box.SizeVec();
            viz::Layer layer = arviz.layer("box");
            layer.add(viz::Pose("pose").pose(robotGlobalPose * boxPose));
            layer.add(viz::Box("box").pose(robotGlobalPose *  boxPose).size(boxDimensions).color(viz::Color::red(255, 128)));
            arviz.commit(layer);

            //
            float theta = boxPose.block<3, 3>(0, 0).eulerAngles(0, 1, 2)[2];
            ARMARX_INFO << VAROUT(theta);
            if (theta > M_PI / 2)
            {
                theta -= M_PI;
            }
            else if (theta < -M_PI / 2)
            {
                theta += M_PI;
            }
            ARMARX_INFO << VAROUT(theta);

            ARMARX_INFO << VAROUT(boxDimensions);

            if (boxDimensions[0] / boxDimensions[1] < in.getLengthRatioThreshold())
            {

                if (theta > M_PI / 4)
                {
                    theta = theta - M_PI / 2;
                }
                else if (theta < - M_PI / 4)
                {
                    theta = M_PI / 2 + theta;
                }
            }
            ARMARX_INFO << VAROUT(theta);

            if (abs(theta) > in.getAngleTolerance())
            {

                Eigen::Vector3f tablePos = in.getTablePos()->toEigen();
                DatafieldRefPtr platformPosX = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformPose", "positionX"));
                DatafieldRefPtr platformPosY = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformPose", "positionY"));
                DatafieldRefPtr platformRot = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformPose", "rotation"));
                float px = platformPosX->getDataField()->getFloat();
                float py = platformPosY->getDataField()->getFloat();
                float pr = platformRot->getDataField()->getFloat();


                // go back 100 mm
                float pxp = (px - tablePos[0]) * (1 + in.getBackStep()) + tablePos[0];
                float pyp = (py - tablePos[1]) * (1 + in.getBackStep()) + tablePos[1];

                armarx::Vector3Ptr prePose(new Vector3(pxp, pyp, pr));
                out.setPlatformRrePose(prePose);


                float npx = tablePos[0] + cos(theta) * (px - tablePos[0]) - sin(theta) * (py - tablePos[1]);
                float npy = tablePos[1] + sin(theta) * (px - tablePos[0]) + cos(theta) * (py - tablePos[1]);
                pr += theta;
                armarx::Vector3Ptr vecPose(new Vector3(npx, npy, pr));
                out.setPlatformGoalPose(vecPose);



                ARMARX_INFO << VAROUT(vecPose->toEigen());
                emitMovePlatform();

            }


            //
            for (size_t i = 0; i < 3; ++i)
            {
                if (boxDimensions[i] > 1000)
                {
                    emitFailure();
                }

            }

            Eigen::Vector3f boxPosition = boxPose.col(3).head<3>();


            bool criterion = fabs(boxPosition[0]) < 300 && boxPosition[1] < 1200 && boxPosition[2] < 1500 && boxPosition[2] > 800;

            out.setBoxDimensions(boxDimensions);
            arviz.commit(arviz.layer("box"));

            if (criterion)
            {
                if (boxPosition[1] > 1000 && boxPosition[1] < 1200)
                {
                    boxPosition(1) -= 100;
                    out.setBoxPose(new FramedPose(boxPose.block<3, 3>(0, 0), boxPosition, robot->getRootNode()->getName(), robot->getName()));
                    emitMoveForwardAndSuccess();
                }
                else
                {
                    out.setBoxPose(new FramedPose(boxPose.block<3, 3>(0, 0), boxPosition, robot->getRootNode()->getName(), robot->getName()));
                    emitSuccess();
                }
            }
            else
            {
                emitFailure();
            }


        }

        //void GetBoxPose::onBreak()
        //{
        //    // put your user code for the breaking point here
        //    // execution time should be short (<100ms)
        //}s

        void GetBoxPose::onExit()
        {
            // put your user code for the exit point here
            // execution time should be short (<100ms)
        }


        // DO NOT EDIT NEXT FUNCTION
        XMLStateFactoryBasePtr GetBoxPose::CreateInstance(XMLStateConstructorParams stateData)
        {
            return XMLStateFactoryBasePtr(new GetBoxPose(stateData));
        }
    }
}
