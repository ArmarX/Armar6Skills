/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BimanualManipulationGroup
 * @author     JeffGao ( jianfenggaobit at gmail dot com )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BimanualObjectLevelMultiMPControl.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <armarx/control/deprecated_njoint_mp_controller/bimanual/ObjLevelControllerInterface.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include "Helpers.h"
#include <armar6/skills/statecharts/Armar6BimanualManipulationGroup/Armar6BimanualManipulationGroupStatechartContext.generated.h>

namespace armarx::Armar6BimanualManipulationGroup
{
    // DO NOT EDIT NEXT LINE
    BimanualObjectLevelMultiMPControl::SubClassRegistry BimanualObjectLevelMultiMPControl::Registry(BimanualObjectLevelMultiMPControl::GetName(), &BimanualObjectLevelMultiMPControl::CreateInstance);

    void BimanualObjectLevelMultiMPControl::onEnter()
    {
        // put your user code for the enter-point here
        // execution time should be short (<100ms)
    }

    void BimanualObjectLevelMultiMPControl::run()
    {
        // hand controller
        devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx leftHandcontroller;
        if (!getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"))
        {
            leftHandcontroller = StateBase::getContext<Armar6BimanualManipulationGroupStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_L_EEF_NJointKITHandV2ShapeController");
        }
        else
        {
            leftHandcontroller =  devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"));
        }
        leftHandcontroller->activateController();

        devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx rightHandcontroller;
        if (!getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"))
        {
            rightHandcontroller = StateBase::getContext<Armar6BimanualManipulationGroupStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_R_EEF_NJointKITHandV2ShapeController");
        }
        else
        {
            rightHandcontroller = devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"));
        }
        rightHandcontroller->activateController();

        if (in.getIsCloseHand())
        {
            leftHandcontroller->setTargetsWithPwm(2, 2, 1, 1);
            rightHandcontroller->setTargetsWithPwm(2, 2, 1, 1);
        }

        // DMP controller check
        NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController("BimanualObjLevelController");
        if (controller)
        {
            controller->deactivateAndDeleteController();
            TimeUtil::SleepMS(2000);
        }

        // DMP parameters
        int kernelSize = 10;
        std::string dmpMode = "Linear";
        std::string dmpObjType = in.getDMPStyleObj();
        std::string dmpLeftType = in.getDMPStyleLeft();
        std::string dmpRightType = in.getDMPStyleRight();

        float dmpAmplitude = in.getDMPAmplitude();
        double phaseL = 100;
        double phaseK = 1000;
        double phaseDist0 = 1000;
        double phaseDist1 = 1000;
        double phaseKpPos = 2;
        double phaseKpOri = 0.1;
        double posToOriRatio = 10;
        double timeDuration = in.getTimeDuration();


        std::vector<double> objInitialPose = in.getObjInitialPoseVec();
        if (in.getIsBoxInHand())
        {
            // bimanually grasp object, thus object in the center of the two hands
            VirtualRobot::RobotPtr localRobot = getLocalRobot();
            RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

            Eigen::Matrix4f leftPose = localRobot->getRobotNodeSet("LeftArm")->getTCP()->getPoseInRootFrame();
            Eigen::Matrix4f rightPose = localRobot->getRobotNodeSet("RightArm")->getTCP()->getPoseInRootFrame();

            VirtualRobot::MathTools::Quaternion boxOri = VirtualRobot::MathTools::eigen4f2quat(leftPose);
            Eigen::Vector3f boxPosi = (leftPose.block<3, 1>(0, 3) + rightPose.block<3, 1>(0, 3)) / 2;

            for (size_t i = 0; i < 3; ++i)
            {
                objInitialPose[i] = boxPosi(i) * 0.001;
            }
            objInitialPose[3] = boxOri.w;
            objInitialPose[4] = boxOri.x;
            objInitialPose[5] = boxOri.y;
            objInitialPose[6] = boxOri.z;
        }

        ARMARX_INFO << "state: " << VAROUT(objInitialPose);

        std::vector<float> leftDesiredJointValues = in.getLeftDesiredJointValues();
        std::vector<float> rightDesiredJointValues  = in.getRightDesiredJointValues();

        std::vector<float> KpImpedance = in.getKpImpedance();
        std::vector<float> KdImpedance = in.getKdImpedance();
        std::vector<float> KpAdmittance = in.getKpAdmittance();
        std::vector<float> KdAdmittance = in.getKdAdmittance();
        std::vector<float> KmAdmittance = in.getKmAdmittance();
        std::vector<float> KmPID = in.getKmPID();

        std::vector<float> targetWrench = in.getTargetWrench();
        std::vector<float> forceP = in.getForceP();
        std::vector<float> forceI = in.getForceI();
        std::vector<float> forceD = in.getForceD();
        std::vector<float> forcePIDLimits = in.getForcePIDLimits();

        //    float filterCoeff = exp(-1.0f / in.getSamplingFrequency() * (2.0f * M_PI * in.getDampingFrequency()) / (pow(10.0f, in.getDampingIntensity() / -10.0f)));
        //    ARMARX_IMPORTANT << "filter coeff. " << filterCoeff;
        float filterCoeff = in.getFilterCoefficient();

        float massLeft = in.getMassLeft();
        std::vector<float> CoMVecLeft = in.getCoMVecLeft();
        std::vector<float> offsetLeft = in.getOffsetVecLeft();
        std::vector<float> forceOffsetLeft { offsetLeft[0], offsetLeft[1], offsetLeft[2]};
        std::vector<float> torqueOffsetLeft { offsetLeft[3], offsetLeft[4], offsetLeft[5]};

        float massRight = in.getMassRight();
        std::vector<float> CoMVecRight = in.getCoMVecRight();
        std::vector<float> offsetRight = in.getOffsetVecRight();
        std::vector<float> forceOffsetRight { offsetRight[0], offsetRight[1], offsetRight[2]};
        std::vector<float> torqueOffsetRight { offsetRight[3], offsetRight[4], offsetRight[5]};

        float knull = in.getKnull();
        float dnull = in.getDnull();
        float torqueLimit = in.getTorqueLimit();

        std::vector<float> forceThreshold = in.getForceThreshold();

        double ftCalibrationTime = in.getFTCalibrationTime();
        NJointBimanualObjLevelMultiMPControllerConfigPtr config = new NJointBimanualObjLevelMultiMPControllerConfig
        (
            kernelSize,
            dmpMode,
            dmpObjType, dmpLeftType, dmpRightType,
            dmpAmplitude,
            phaseL,
            phaseK,
            phaseDist0,
            phaseDist1,
            phaseKpPos,
            phaseKpOri,
            posToOriRatio,
            timeDuration,
            objInitialPose,
            leftDesiredJointValues,
            rightDesiredJointValues,
            KpImpedance,
            KdImpedance,
            KpAdmittance,
            KdAdmittance,
            KmAdmittance,
            KmPID,
            targetWrench,
            forceP,
            forceI,
            forceD,
            forcePIDLimits,
            filterCoeff,
            massLeft,
            CoMVecLeft,
            forceOffsetLeft,
            torqueOffsetLeft,
            massRight,
            CoMVecRight,
            forceOffsetRight,
            torqueOffsetRight,
            knull,
            dnull,
            torqueLimit,
            forceThreshold,
            ftCalibrationTime
        );

        NJointBimanualObjLevelMultiMPControllerInterfacePrx bimanualObjLevelController
            = NJointBimanualObjLevelMultiMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointBimanualObjLevelMultiMPController", "BimanualObjLevelController", config));

        // learn dmps from file
        std::vector<std::string> fileNamesObj = in.getFileNameObj();
        std::vector<std::string> fileNamesLeft = in.getFileNameLeft();
        std::vector<std::string> fileNamesRight = in.getFileNameRight();
        bimanualObjLevelController->learnMultiDMPFromFiles(fileNamesObj, fileNamesLeft, fileNamesRight);

        // set dmp via points
        if (in.isViaPointsSet())
        {
            std::map<std::string, armarx::PosePtr> viaPoseMap = in.getViaPoints();

            for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
                 it != viaPoseMap.end(); ++it)
            {
                double canVal = std::stod(it->first);
                bimanualObjLevelController->setViaPoints(canVal, Helpers::pose2dvec(it->second->toEigen()));

            }
        }

        // activate controller
        bimanualObjLevelController->activateController();
        usleep(in.getFTCalibrationTime() * 1000000);

        // set dmp goals
        std::vector<double> dmpGoalsObj = Helpers::pose2dvec(in.getDMPGoalsObj()->toEigen());
        std::vector<double> dmpGoalsLeft = Helpers::pose2dvec(in.getDMPGoalsLeft()->toEigen());
        std::vector<double> dmpGoalsRight = Helpers::pose2dvec(in.getDMPGoalsRight()->toEigen());
        bimanualObjLevelController->runDMP(dmpGoalsObj, dmpGoalsLeft, dmpGoalsRight, timeDuration);

        // loop
        while (!isRunningTaskStopped()) // stop run function if returning true
        {
            if (bimanualObjLevelController->isFinished())
            {
                break;
            }
        }

        if (in.getIsStopAfterDMPFinished())
        {
            bimanualObjLevelController->deactivateController();
            while (bimanualObjLevelController->isControllerActive())
            {
                usleep(10000);
            }
            bimanualObjLevelController->deleteController();
        }
        emitSuccess();
    }

    //void BimanualObjectLevelMultiMPControl::onBreak()
    //{
    //    // put your user code for the breaking point here
    //    // execution time should be short (<100ms)
    //}

    void BimanualObjectLevelMultiMPControl::onExit()
    {
        // put your user code for the exit point here
        // execution time should be short (<100ms)
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr BimanualObjectLevelMultiMPControl::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new BimanualObjectLevelMultiMPControl(stateData));
    }
}
