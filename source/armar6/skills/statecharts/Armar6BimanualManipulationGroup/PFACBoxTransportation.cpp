/*
* This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BimanualManipulationGroup
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PFACBoxTransportation.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
// #include <RobotAPI/components/DSObstacleAvoidance/arviz_agent.h>
#include <RobotAPI/interface/components/ObstacleAvoidance/ObstacleAvoidanceInterface.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/XML/RobotIO.h>
#include <VirtualRobot/math/Helpers.h>
#include <SimoxUtility/math/convert/mat3f_to_rpy.h>
#include <ArmarXCore/core/time/CycleUtil.h>
#include <RobotAPI/libraries/core/PIDController.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <ArmarXCore/core/time/TimeUtil.h>
#include <armarx/control/deprecated_njoint_mp_controller/bimanual/ForceMPControllerInterface.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include "Helpers.h"

namespace armarx::Armar6BimanualManipulationGroup
{
    // DO NOT EDIT NEXT LINE
    PFACBoxTransportation::SubClassRegistry PFACBoxTransportation::Registry(PFACBoxTransportation::GetName(), &PFACBoxTransportation::CreateInstance);

    namespace
    {
        float
        signed_min(float newValue, float minAbsValue)
        {
            return std::copysign(std::min<float>(fabs(newValue), minAbsValue), newValue);
        }
    }


    void PFACBoxTransportation::onEnter()
    {
        // put your user code for the enter-point here
        // execution time should be short (<100ms)
    }

    void PFACBoxTransportation::run()
    {
        //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");
        VirtualRobot::RobotPtr robot = getLocalRobot();

        NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController("BimanualForceController");
        if (controller)
        {
            if (controller->isControllerActive())
            {
                controller->deactivateController();
                while (controller->isControllerActive())
                {
                    TimeUtil::SleepMS(10);
                }
            }
            controller->deleteController();
            TimeUtil::SleepMS(10);
        }

        // DMP parameters
        int kernelSize = 50;
        std::string dmpMode = "Linear";
        std::string dmpType = "Discrete";
        double phaseL = 100;
        double phaseK = 1000;
        double phaseDist0 = 1000;
        double phaseDist1 = 1000;
        double phaseKpPos = 2;
        double phaseKpOri = 0.1;
        double posToOriRatio = 10;
        double timeDuration = in.getTimeDuration();


        std::vector<double> boxInitialPose = in.getBoxInitialPoseVec();
        float boxWidth = in.getBoxWidth();
        std::vector<float> leftDesiredJointValues = in.getLeftDesiredJointValues();
        std::vector<float> rightDesiredJointValues  = in.getRightDesiredJointValues();

        std::vector<float> KpImpedance = in.getKpImpedance();
        std::vector<float> KdImpedance = in.getKdImpedance();
        std::vector<float> KpAdmittance = in.getKpAdmittance();
        std::vector<float> KdAdmittance = in.getKdAdmittance();
        std::vector<float> KmAdmittance = in.getKmAdmittance();
        std::vector<float> KmPID = in.getKmPID();

        std::vector<float> targetWrench = in.getTargetWrench();
        std::vector<float> forceP = in.getForceP();
        std::vector<float> forceI = in.getForceI();
        std::vector<float> forceD = in.getForceD();
        std::vector<float> forcePIDLimits = in.getForcePIDLimits();

        float filterCoeff = in.getFilterCoefficient();

        float massLeft = 0;// in.getMassLeft();
        std::vector<float> CoMVecLeft = in.getCoMVecLeft();
        //    std::vector<float> offsetLeft = in.getOffsetVecLeft();
        //    std::vector<float> forceOffsetLeft { offsetLeft[0], offsetLeft[1], offsetLeft[2]};
        //    std::vector<float> torqueOffsetLeft { offsetLeft[3], offsetLeft[4], offsetLeft[5]};

        float massRight = 0;// in.getMassRight();
        std::vector<float> CoMVecRight = in.getCoMVecRight();
        //    std::vector<float> offsetRight = in.getOffsetVecRight();
        //    std::vector<float> forceOffsetRight { offsetRight[0], offsetRight[1], offsetRight[2]};
        //    std::vector<float> torqueOffsetRight { offsetRight[3], offsetRight[4], offsetRight[5]};

        IceUtil::Time startFTCalibration = TimeUtil::GetTime();
        DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT"));
        DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT"));
        DatafieldRefPtr torqueDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getTorqueDatafield("FT L_ArmL_FT"));
        DatafieldRefPtr torqueDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getTorqueDatafield("FT R_ArmR_FT"));

        Eigen::Vector3f forceLeftOffset;
        Eigen::Vector3f torqueLeftOffset;
        forceLeftOffset.setZero();
        torqueLeftOffset.setZero();

        Eigen::Vector3f forceRightOffset;
        Eigen::Vector3f torqueRightOffset;
        forceRightOffset.setZero();
        torqueRightOffset.setZero();

        while ((TimeUtil::GetTime() - startFTCalibration).toSecondsDouble() < in.getFTCalibrationTime()) // Stop updating if moving task was stopped.
        {
            RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

            forceLeftOffset = 0.5 * (forceLeftOffset + forceDfLeft->getDataField()->get<FramedDirection>()->toEigen());
            torqueLeftOffset = 0.5 * (torqueLeftOffset + torqueDfLeft->getDataField()->get<FramedDirection>()->toEigen());
            forceRightOffset = 0.5 * (forceRightOffset + forceDfRight->getDataField()->get<FramedDirection>()->toEigen());
            torqueRightOffset = 0.5 * (torqueRightOffset + torqueDfRight->getDataField()->get<FramedDirection>()->toEigen());
        }

        ARMARX_INFO << VAROUT(forceLeftOffset);
        ARMARX_INFO << VAROUT(torqueLeftOffset);
        ARMARX_INFO << VAROUT(forceRightOffset);
        ARMARX_INFO << VAROUT(torqueRightOffset);


        std::vector<float> forceOffsetLeft { forceLeftOffset[0], forceLeftOffset[1], forceLeftOffset[2]};
        std::vector<float> torqueOffsetLeft { torqueLeftOffset[0], torqueLeftOffset[1], torqueLeftOffset[2]};

        std::vector<float> forceOffsetRight { forceRightOffset[0], forceRightOffset[1], forceRightOffset[2]};
        std::vector<float> torqueOffsetRight { torqueRightOffset[0], torqueRightOffset[1], torqueRightOffset[2]};

        float knull = in.getKnull();
        float dnull = in.getDnull();
        float torqueLimit = in.getTorqueLimit();

        float forceThreshold = in.getForceThreshold();

        NJointBimanualForceControllerConfigPtr config = new NJointBimanualForceControllerConfig
        (
            kernelSize,
            dmpMode,
            dmpType,
            phaseL,
            phaseK,
            phaseDist0,
            phaseDist1,
            phaseKpPos,
            phaseKpOri,
            posToOriRatio,
            timeDuration,
            boxInitialPose,
            boxWidth,
            leftDesiredJointValues,
            rightDesiredJointValues,
            KpImpedance,
            KdImpedance,
            KpAdmittance,
            KdAdmittance,
            KmAdmittance,
            KmPID,
            targetWrench,
            forceP,
            forceI,
            forceD,
            forcePIDLimits,
            filterCoeff,
            massLeft,
            CoMVecLeft,
            forceOffsetLeft,
            torqueOffsetLeft,
            massRight,
            CoMVecRight,
            forceOffsetRight,
            torqueOffsetRight,
            knull,
            dnull,
            torqueLimit,
            forceThreshold
        );


        NJointBimanualForceControllerInterfacePrx bimanualForceController
            = NJointBimanualForceControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointBimanualForceController", "BimanualForceController", config));

        std::vector<std::string> fileNames = in.getArmMotionFileName();
        bimanualForceController->learnDMPFromFiles(fileNames);
        bimanualForceController->activateController();

        if (in.isDMPViaPointsGraspingSet())
        {
            std::map<std::string, armarx::PosePtr> viaPoseMap = in.getDMPViaPointsGrasping();

            for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
                 it != viaPoseMap.end(); ++it)
            {
                double canVal = std::stod(it->first);
                bimanualForceController->setViaPoints(canVal, Helpers::pose2dvec(it->second->toEigen()));

            }
        }


        usleep(in.getWaitTime() * 1000000);

        bimanualForceController->runDMP(Helpers::pose2dvec(in.getDMPGoalsGrasping()->toEigen()));
        while (!bimanualForceController->isFinished())
        {

        }

        ARMARX_IMPORTANT << "first dmp finished ... ";

        PIDController pid_rot{in.getRotationPID()->x, in.getRotationPID()->y, in.getRotationPID()->z};


        const double kp = in.getKp();
        const float max_velocity = in.getMaxVelocity();
        const float max_angular_velocity = in.getMaxAngularVelocity();
        const float position_reached_threshold = in.getPositionReachedThreshold();
        const float angle_reached_threshold = in.getAngleReachedThreshold();

        // Proxies.
        ObstacleAvoidanceInterfacePrx obstacle_avoidance = getObstacleAvoidance();
        DebugDrawerInterfacePrx debug = getDebugDrawerTopic();
        PlatformUnitInterfacePrx platform = getPlatformUnit();

        obstacleavoidance::Agent agent;
        agent.safety_margin = 0;
        agent.pos = robot->getGlobalPosition();
        agent.desired_vel = Eigen::Vector3f::Zero();


        // Draw static objects.
        //clear_drawings(debug);
        //draw_goal(debug, goal);

        // Variables for path visualization.
        Eigen::Vector3f last_visu_pos = Eigen::Vector3f::Zero();
        const int path_draw_distance = 50;  // Min. distance between two path vis. spheres (in [mm]).
        int visu_sphere_counter = 0;

        Eigen::Vector3f filtered_platform_target_vel = Eigen::Vector3f::Zero();

        CycleUtil c{10};

        size_t goalID = 0;

        std::vector<Eigen::Vector3f > goalList;
        Eigen::Vector3f goal;

        std::vector< ::armarx::Vector3Ptr> goalListInput = in.getGoalList();

        for (size_t i = 0; i < goalListInput.size(); ++i)
        {
            goalList.push_back(goalListInput[i]->toEigen());
        }



        while (not isRunningTaskStopped())  // Stop updating if moving task was stopped.
        {
            goal = goalList[goalID];
            RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
            if ((last_visu_pos - robot->getGlobalPosition()).norm() > path_draw_distance)
            {
                //            draw_agent_path(debug, robot, visu_sphere_counter);
                last_visu_pos = robot->getGlobalPosition();
                visu_sphere_counter++;
            }

            Eigen::Vector3f agent_position{robot->getGlobalPosition()};
            agent_position(2) = 0;
            //        draw_agent_margin(debug, robot, obstacle_avoidance->getConfig().agent_safety_margin);

            // Platform controllers.

            Eigen::Vector3f vel = (goal - agent_position) * kp;
            vel(2) = 0;
            if (vel.norm() > max_velocity)
            {
                vel = vel.normalized() * max_velocity;
            }

            agent.goal = goal;
            agent.pos = agent_position;
            agent.desired_vel = vel;

            ARMARX_DEBUG << "To goal distance:  " << (goal - agent_position).norm();
            //        ARMARX_DEBUG << deactivateSpam(0.3) << VAROUT(goal) << VAROUT(agent_position)
            //                     << " desired vel: " << agent.get_linear_velocity();
            // draw_agent_desired_velocity(debug, robot->getGlobalPosition(), vel);
            Eigen::Vector3f modulated_vel = obstacle_avoidance->modulateVelocity(agent);
            // draw_agent_modulated_velocity(debug, robot->getGlobalPosition(), modulated_vel);

            auto rot_matrix = ::math::Helpers::Orientation(robot->getGlobalPose());
            float ori = simox::math::mat3f_to_rpy(rot_matrix).z();
            if (ori > M_PI)
            {
                ori = - 2 * M_PI + ori;
            }

            float angle_delta = goal(2) - ori;
            // transform alpha to [-pi, pi)
            while (angle_delta < -M_PI)
            {
                angle_delta +=  2 * M_PI;
            }
            while (angle_delta >= M_PI)
            {
                angle_delta -=  2 * M_PI;
            }

            pid_rot.update(angle_delta, 0);
            float new_vel_rot = -signed_min(pid_rot.getControlValue(), max_angular_velocity);

            Eigen::Matrix3f m;
            m = Eigen::AngleAxisf(ori, Eigen::Vector3f::UnitZ());
            Eigen::Vector3f local_vel = m.inverse() * modulated_vel;
            if (local_vel.norm() > max_velocity)
            {
                local_vel *= max_velocity / local_vel.norm();
            }

            if (not std::isfinite(local_vel[0]) or not std::isfinite(local_vel[1])
                or not std::isfinite(new_vel_rot))
            {
                ARMARX_WARNING << deactivateSpam(1) << "Some velocity target is not finite.  "
                               << "Setting to zero.";
                local_vel.setZero();
            }
            const float ratio = 0.7;
            filtered_platform_target_vel = filtered_platform_target_vel * ratio
                                           + (1.0 - ratio) * local_vel;

            ARMARX_DEBUG << deactivateSpam(0.1) << "Filtered modulated target velocity:  "
                         << filtered_platform_target_vel;
            ARMARX_DEBUG << "Current error:  " << (goal.head(2) - agent_position.head(2)).norm()
                         << " " << fabs(angle_delta);

            // Determine whether the goal position and orientation was reached.
            if ((goal.head(2) - agent_position.head(2)).norm() < position_reached_threshold)
            {
                ARMARX_VERBOSE << "Goal position reached.";

                filtered_platform_target_vel(0) = 0.0;
                filtered_platform_target_vel(1) = 0.0;

                if (fabs(angle_delta) < angle_reached_threshold)
                {
                    ARMARX_VERBOSE << "Goal orientation reached.";
                    goalID++;
                    if (goalID >= goalList.size())
                    {
                        break;
                    }
                }
            }

            platform->move(filtered_platform_target_vel(0), filtered_platform_target_vel(1),
                           new_vel_rot);
            c.waitForCycleDuration();
        }

        ARMARX_IMPORTANT << "Stopping platform now.";
        platform->move(0, 0, 0);


        if (in.isDMPViaPointsPlacingSet())
        {
            std::map<std::string, armarx::PosePtr> viaPoseMap = in.getDMPViaPointsPlacing();

            for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
                 it != viaPoseMap.end(); ++it)
            {
                double canVal = std::stod(it->first);
                bimanualForceController->setViaPoints(canVal, Helpers::pose2dvec(it->second->toEigen()));

            }
        }

        bimanualForceController->runDMP(Helpers::pose2dvec(in.getDMPGoalsPlacing()->toEigen()));

        while (!bimanualForceController->isFinished())
        {

        }


        bimanualForceController->deactivateController();
        while (bimanualForceController->isControllerActive())
        {
            usleep(10000);
        }
        bimanualForceController->deleteController();
        emitSuccess();

    }

    //void PFACBoxTransportation::onBreak()
    //{
    //    // put your user code for the breaking point here
    //    // execution time should be short (<100ms)
    //}

    void PFACBoxTransportation::onExit()
    {
        // put your user code for the exit point here
        // execution time should be short (<100ms)
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr PFACBoxTransportation::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new PFACBoxTransportation(stateData));
    }
}
