/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BimanualManipulationGroup
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SwitchBimanualObjLevelVelDMP.h"


#include <ArmarXCore/core/time/TimeUtil.h>
#include <armarx/control/deprecated_njoint_mp_controller/bimanual/ObjLevelControllerInterface.h>
#include "Helpers.h"

namespace armarx::Armar6BimanualManipulationGroup
{
    // DO NOT EDIT NEXT LINE
    SwitchBimanualObjLevelVelDMP::SubClassRegistry SwitchBimanualObjLevelVelDMP::Registry(SwitchBimanualObjLevelVelDMP::GetName(), &SwitchBimanualObjLevelVelDMP::CreateInstance);

    void SwitchBimanualObjLevelVelDMP::onEnter()
    {
        // put your user code for the enter-point here
        // execution time should be short (<100ms)
    }

    void SwitchBimanualObjLevelVelDMP::run()
    {
        NJointBimanualObjLevelVelControllerInterfacePrx bimanualObjLevelController;
        NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController(in.getControllerName());
        if (controller)
        {
            bimanualObjLevelController = NJointBimanualObjLevelVelControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getControllerName()));
        }
        else
        {
            emitFailure();
        }


        if (in.isDMPFileSet())
        {
            bimanualObjLevelController->learnDMPFromFiles(in.getDMPFile());
        }

        if (in.isMPWeightsSet())
        {
            std::vector<std::vector<double> > weights = Helpers::getWeightsFromFlatVector(in.getMPWeights(), 7);
            bimanualObjLevelController->setMPWeights(weights);
        }

        if (in.isMPRotWeightsSet())
        {
            std::vector<std::vector<double> > weights = Helpers::getWeightsFromFlatVector(in.getMPRotWeights(), 4);
            bimanualObjLevelController->setMPRotWeights(weights);
        }


        std::vector<double> dmpGoals = Helpers::pose2dvec(in.getDMPGoals()->toEigen());

        if (in.isDMPStartsSet())
        {
            std::vector<double> dmpStarts = Helpers::pose2dvec(in.getDMPStarts()->toEigen());
            bimanualObjLevelController->runDMPWithVirtualStart(dmpStarts, dmpGoals, in.getTimeDuration());

        }
        else
        {
            bimanualObjLevelController->runDMP(dmpGoals, in.getTimeDuration());
        }
        if (in.getWaitForDMPFinished())
        {

            while (!isRunningTaskStopped()) // stop run function if returning true
            {
                if (bimanualObjLevelController->isFinished())
                {
                    break;
                }
            }
        }

        if (in.isMPRotWeightsOutFileSet() && in.getIsSave())
        {
            Helpers::writeMPWeightsToFile(bimanualObjLevelController->getMPRotWeights(), in.getMPRotWeightsOutFile());
        }

        emitSuccess();
    }

    //void SwitchBimanualObjLevelVelDMP::onBreak()
    //{
    //    // put your user code for the breaking point here
    //    // execution time should be short (<100ms)
    //}

    void SwitchBimanualObjLevelVelDMP::onExit()
    {
        // put your user code for the exit point here
        // execution time should be short (<100ms)
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr SwitchBimanualObjLevelVelDMP::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new SwitchBimanualObjLevelVelDMP(stateData));
    }
}
