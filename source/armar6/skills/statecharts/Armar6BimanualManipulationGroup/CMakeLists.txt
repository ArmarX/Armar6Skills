armarx_add_statechart_group(
    Armar6BimanualManipulationGroup
    SOURCES
        Armar6BimanualManipulationGroupRemoteStateOfferer.cpp
    HEADERS
        Armar6BimanualManipulationGroupRemoteStateOfferer.h
        Helpers.h
    DEPENDENCIES
        VisionXPointCloud
        RobotStatechartHelpers
        ArViz
        devices_ethercat::hand_armar6_v2
        armarx_control::deprecated_njoint_mp_controller_interface
    DEPENDENCIES_LEGACY
        PCL
        DMP
    GROUP_FILE
        Armar6BimanualManipulationGroup.scgxml
)
