/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BimanualManipulationGroup
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "IndependentBimanualDMPWithForceStop.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <armarx/control/deprecated_njoint_mp_controller/task_space/TaskSpaceImpedanceDMPControllerInterface.h>
#include "Helpers.h"

namespace armarx::Armar6BimanualManipulationGroup
{
    // DO NOT EDIT NEXT LINE
    IndependentBimanualDMPWithForceStop::SubClassRegistry IndependentBimanualDMPWithForceStop::Registry(IndependentBimanualDMPWithForceStop::GetName(), &IndependentBimanualDMPWithForceStop::CreateInstance);

    void IndependentBimanualDMPWithForceStop::onEnter()
    {
        // put your user code for the enter-point here
        // execution time should be short (<100ms)
    }

    void IndependentBimanualDMPWithForceStop::run()
    {
        //        getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");

        NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController(in.getLeftControllerName());
        if (controller)
        {
            if (controller->isControllerActive())
            {
                controller->deactivateController();
                while (controller->isControllerActive())
                {
                    TimeUtil::SleepMS(10);
                }
            }
            controller->deleteController();
            TimeUtil::SleepMS(10);
        }

        controller = getRobotUnit()->getNJointController(in.getRightControllerName());
        if (controller)
        {
            if (controller->isControllerActive())
            {
                controller->deactivateController();
                while (controller->isControllerActive())
                {
                    TimeUtil::SleepMS(10);
                }
            }
            controller->deleteController();
            TimeUtil::SleepMS(10);
        }

        double phaseL = 100;
        double phaseK = 1000;
        double phaseDist0 = 100;
        double phaseDist1 = 50;
        double posToOriRatio = 10;

        std::vector<float> defaultLeftJointValues;
        if (in.isDesiredLeftJointValuesSet())
        {
            defaultLeftJointValues = in.getDesiredLeftJointValues();
        }
        else
        {
            defaultLeftJointValues = getRobot()->getRobotNodeSet("LeftArm")->getJointValues();
        }

        std::vector<float> defaultRightJointValues;
        if (in.isDesiredRightJointValuesSet())
        {
            defaultRightJointValues = in.getDesiredRightJointValues();
        }
        else
        {
            defaultRightJointValues = getRobot()->getRobotNodeSet("RightArm")->getJointValues();
        }

        float torqueLimit = in.getTorqueLimit();
        std::string forceSensorName = "FT L";
        float waitTimeForCalibration = 1;
        float forceFilter = 0.5;
        float forceDeadZone = 2;
        Eigen::Vector3f forceThreshold{100, 100, 100};
        std::string forceFrameName = "ArmL8_Wri2";

        NJointTaskSpaceImpedanceDMPControllerConfigPtr tsConfig = new NJointTaskSpaceImpedanceDMPControllerConfig(
            in.getDMPKernelSize(),
            in.getBaseMode(),
            in.getDMPStyle(),
            in.getTimeDuration(),
            "LeftArm",
            phaseL,
            phaseK,
            phaseDist0,
            phaseDist1,
            posToOriRatio,
            in.getKposVec(),
            in.getDposVec(),
            in.getKoriVec(),
            in.getDoriVec(),
            in.getKnull(),
            in.getDnull(),
            false,
            defaultLeftJointValues,
            torqueLimit,
            forceSensorName,
            waitTimeForCalibration,
            forceFilter,
            forceDeadZone,
            forceThreshold,
            forceFrameName
        );

        NJointTaskSpaceImpedanceDMPControllerInterfacePrx leftController
            = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", in.getLeftControllerName(), tsConfig));


        tsConfig->nodeSetName = "RightArm";
        tsConfig->defaultNullSpaceJointValues = defaultRightJointValues;
        tsConfig->forceFrameName = "ArmR8_Wri2";
        tsConfig->forceSensorName = "FT R";

        NJointTaskSpaceImpedanceDMPControllerInterfacePrx rightController
            = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", in.getRightControllerName(), tsConfig));


        std::vector<std::string> fileNames = in.getLeftArmMotionFile();
        leftController->learnDMPFromFiles(fileNames);
        fileNames = in.getRightArmMotionFile();
        rightController->learnDMPFromFiles(fileNames);

        std::vector<double> leftGoals = Helpers::pose2dvec(in.getLeftTcpGoalPose()->toEigen());

        if (in.isLeftViaPoseSet())
        {
            std::map<std::string, armarx::PosePtr> viaPoseMap = in.getLeftViaPose();

            for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
                 it != viaPoseMap.end(); ++it)
            {
                double canVal = std::stod(it->first);
                leftController->setViaPoints(canVal, Helpers::pose2dvec(it->second->toEigen()));

            }
        }

        std::vector<double> rightGoals = Helpers::pose2dvec(in.getRightTcpGoalPose()->toEigen());

        if (in.isRightViaPoseSet())
        {
            std::map<std::string, armarx::PosePtr> viaPoseMap = in.getRightViaPose();

            for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
                 it != viaPoseMap.end(); ++it)
            {
                double canVal = std::stod(it->first);
                rightController->setViaPoints(canVal, Helpers::pose2dvec(it->second->toEigen()));

            }
        }

        leftController->activateController();
        rightController->activateController();
        leftController->runDMP(leftGoals);
        rightController->runDMP(rightGoals);

        //############### //
        VirtualRobot::RobotPtr localRobot = getLocalRobot();
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT"));
        DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT"));
        Eigen::Vector3f initialForceLeft;
        Eigen::Vector3f initialForceRight;
        {
            FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
            initialForceRight = forcePtr->toRootEigen(localRobot);
        }
        {
            FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
            initialForceLeft = forcePtr->toRootEigen(localRobot);
        }

        IceUtil::Time forceTimerLeft = IceUtil::Time::now();
        IceUtil::Time forceTimerRight = IceUtil::Time::now();

        float forceIntegralLeft = 0;
        float forceIntegralRight = 0;

        Eigen::Vector3f forceLeft;
        Eigen::Vector3f forceRight;

        bool isLeftStopped = false;
        bool isRightStopped = false;
        float filterWeight = in.getForceFilterWeight();
        while (!isRunningTaskStopped())
        {
            RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

            FramedDirectionPtr forceRightPtr = forceDfRight->getDataField()->get<FramedDirection>();
            forceRight = (1 - filterWeight) * forceRight + filterWeight * (forceRightPtr->toRootEigen(localRobot) - initialForceRight);

            FramedDirectionPtr forceLeftPtr = forceDfLeft->getDataField()->get<FramedDirection>();
            forceLeft = (1 - filterWeight) * forceLeft + filterWeight * (forceLeftPtr->toRootEigen(localRobot) - initialForceLeft);

            double canVal = leftController->getVirtualTime();

            if (canVal < in.getForceStopTriggeredCanVal() * in.getTimeDuration())
            {
                {
                    Eigen::Vector3f fdir = in.getForceThreshouldDirLeft()->toEigen();
                    fdir = fdir.cwiseAbs();
                    forceLeft = forceLeft.cwiseAbs();
                    float forceMag = forceLeft.dot(fdir);
                    double deltaT = 0.01; //(IceUtil::Time::now() - forceTimerLeft).toSecondsDouble();
                    if (forceMag > 2)
                    {
                        forceIntegralLeft += forceMag * deltaT;
                    }
                    else
                    {
                        forceIntegralLeft = 0;
                    }

                    if (forceIntegralLeft > in.getForceThreshold())
                    {
                        leftController->stopDMP();
                        isLeftStopped = true;
                    }
                }
                {
                    Eigen::Vector3f fdir = in.getForceThreshouldDirRight()->toEigen();
                    fdir = fdir.cwiseAbs();
                    forceRight = forceRight.cwiseAbs();

                    float forceMag = forceRight.dot(fdir);
                    double deltaT = 0.01; //(IceUtil::Time::now() - forceTimerRight).toSecondsDouble();
                    if (forceMag > 2)
                    {
                        forceIntegralRight += forceMag * deltaT;
                    }
                    else
                    {
                        forceIntegralRight = 0;
                    }

                    if (forceIntegralRight >  in.getForceThreshold())
                    {
                        rightController->stopDMP();
                        isRightStopped = true;
                    }
                }
            }
            else
            {
                forceTimerLeft = IceUtil::Time::now();
                forceTimerRight = IceUtil::Time::now();
                {
                    FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
                    initialForceRight = forcePtr->toRootEigen(localRobot);
                }
                {
                    FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
                    initialForceLeft = forcePtr->toRootEigen(localRobot);
                }
            }


            if (leftController->isFinished())
            {
                isLeftStopped = true;
            }

            if (rightController->isFinished())
            {
                isRightStopped = true;
            }
            if (isLeftStopped && isRightStopped)
            {
                ARMARX_INFO << "reach force threshold";
                break;
            }
            usleep(1000);
        }

        leftController->deactivateController();
        rightController->deactivateController();

        while (leftController->isControllerActive() || rightController->isControllerActive())
        {
            usleep(10000);
        }

        leftController->deleteController();
        rightController->deleteController();

        usleep(100000);
        emitSuccess();
    }

    //void IndependentBimanualDMPWithForceStop::onBreak()
    //{
    //    // put your user code for the breaking point here
    //    // execution time should be short (<100ms)
    //}

    void IndependentBimanualDMPWithForceStop::onExit()
    {
        // put your user code for the exit point here
        // execution time should be short (<100ms)
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr IndependentBimanualDMPWithForceStop::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new IndependentBimanualDMPWithForceStop(stateData));
    }
}
