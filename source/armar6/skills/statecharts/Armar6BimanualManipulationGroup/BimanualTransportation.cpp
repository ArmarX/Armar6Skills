/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BimanualManipulationGroup
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BimanualTransportation.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <VirtualRobot/MathTools.h>

namespace armarx::Armar6BimanualManipulationGroup
{

    // DO NOT EDIT NEXT LINE
    BimanualTransportation::SubClassRegistry BimanualTransportation::Registry(BimanualTransportation::GetName(), &BimanualTransportation::CreateInstance);



    void BimanualTransportation::onEnter()
    {
        // put your user code for the enter-point here
        // execution time should be short (<100ms)

        Eigen::Matrix4f boxGraspingLeft = in.getBoxGraspingLeft()->toEigen();
        Eigen::Matrix4f boxGraspingRight = in.getBoxGraspingRight()->toEigen();


        Eigen::Matrix4f leftApproachingGoal = boxGraspingLeft;
        local.setLeftApproachingGoal(new Pose(leftApproachingGoal));

        std::map<std::string, PosePtr> leftApproachingViaPoseMap;
        Eigen::Matrix4f leftApproachingViaPose = leftApproachingGoal;
        leftApproachingViaPose(0, 3) -= 100;
        leftApproachingViaPoseMap["0.2"] = new Pose(leftApproachingViaPose);
        local.setLeftApproachingViaPose(leftApproachingViaPoseMap);

        Eigen::Matrix4f rightApproachingGoal = boxGraspingRight;
        local.setRightApproachingGoal(new Pose(rightApproachingGoal));

        std::map<std::string, PosePtr> rightApproachingViaPoseMap;
        Eigen::Matrix4f rightApproachingViaPose = rightApproachingGoal;
        rightApproachingViaPose(0, 3) += 100;
        rightApproachingViaPoseMap["0.2"] = new Pose(rightApproachingViaPose);
        local.setRightApproachingViaPose(rightApproachingViaPoseMap);

        // PFAC is in m ..
        Eigen::Matrix4f pfacGraspingGoal = boxGraspingLeft;
        pfacGraspingGoal.block<3, 1>(0, 3) = 0.001 * (boxGraspingLeft.block<3, 1>(0, 3) + boxGraspingRight.block<3, 1>(0, 3)) / 2;
        pfacGraspingGoal(2, 3) = in.getTransportationHeight();
        local.setPFACGraspingGoal(pfacGraspingGoal);

        Eigen::Matrix4f pfacPlacingGoal = pfacGraspingGoal;
        pfacPlacingGoal(1, 3) = in.getTargetTableYInMeter();
        pfacPlacingGoal(2, 3) = in.getTargetTableHeightInMeter();
        local.setPFACPlacingGoal(pfacPlacingGoal);


        std::map<std::string, PosePtr> pfacPlacingViaPoseMap;
        Eigen::Matrix4f pfacPlacingViaPose = pfacPlacingGoal;
        pfacPlacingViaPose(2, 3) += 0.2;
        pfacPlacingViaPoseMap["0.5"] = new Pose(pfacPlacingViaPose);
        local.setPFACPlacingViaPose(pfacPlacingViaPoseMap);

        std::vector<double> boxInitialPoseVec;
        VirtualRobot::MathTools::Quaternion boxOri = VirtualRobot::MathTools::eigen4f2quat(boxGraspingLeft);
        Eigen::Vector3f boxPosi = (boxGraspingLeft.block<3, 1>(0, 3) + boxGraspingRight.block<3, 1>(0, 3)) / 2;
        for (size_t i = 0; i < 3; ++i)
        {
            boxInitialPoseVec.push_back(boxPosi(i) * 0.001);
        }
        boxInitialPoseVec.push_back(boxOri.w);
        boxInitialPoseVec.push_back(boxOri.x);
        boxInitialPoseVec.push_back(boxOri.y);
        boxInitialPoseVec.push_back(boxOri.z);
        local.setBoxInitialPoseVec(boxInitialPoseVec);


    }

    //void BimanualTransportation::run()
    //{
    //    // put your user code for the execution-phase here
    //    // runs in seperate thread, thus can do complex operations
    //    // should check constantly whether isRunningTaskStopped() returns true
    //
    //    // get a private kinematic instance for this state of the robot (tick "Robot State Component" proxy checkbox in statechart group)
    //    VirtualRobot::RobotPtr robot = getLocalRobot();
    //
    //// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //        // synchronize robot clone to most recent state
    //        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    //    }
    //}

    //void BimanualTransportation::onBreak()
    //{
    //    // put your user code for the breaking point here
    //    // execution time should be short (<100ms)
    //}

    void BimanualTransportation::onExit()
    {
        // put your user code for the exit point here
        // execution time should be short (<100ms)
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr BimanualTransportation::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new BimanualTransportation(stateData));
    }
}
