/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BimanualManipulationGroup
 * @author     armar-user ( armar6 at kit )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "AddObstacle.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <RobotAPI/interface/components/ObstacleAvoidance/ObstacleDetectionInterface.h>

namespace armarx::Armar6BimanualManipulationGroup
{
    // DO NOT EDIT NEXT LINE
    AddObstacle::SubClassRegistry AddObstacle::Registry(AddObstacle::GetName(), &AddObstacle::CreateInstance);

    void AddObstacle::onEnter()
    {
        // put your user code for the enter-point here
        // execution time should be short (<100ms)

        if (in.isObsNameSet())
        {
            std::map<std::string, float> obsParams = in.getObstacleParams();
            obstacledetection::Obstacle obstacle;
            obstacle.name = in.getObsName();
            obstacle.posX = obsParams["posX"];
            obstacle.posY = obsParams["posY"];
            obstacle.posZ = 0;
            obstacle.yaw =  obsParams["yaw"];
            obstacle.axisLengthX = obsParams["axisLengthX"];
            obstacle.axisLengthY = obsParams["axisLengthY"];
            obstacle.axisLengthZ = 0;
            obstacle.refPosX = obstacle.posX;
            obstacle.refPosY = obstacle.posY;
            obstacle.refPosZ = 0;
            obstacle.safetyMarginX = obsParams["safetyMargin"];
            obstacle.safetyMarginY = obsParams["safetyMargin"];
            obstacle.safetyMarginZ = 0;
            obstacle.curvatureX = 2;
            obstacle.curvatureY = 2;
            obstacle.curvatureZ = 1;

            ObstacleDetectionInterfacePrx obstacle_detection = getObstacleDetection();

            obstacle_detection->updateObstacle(obstacle);
        }

        emitSuccess();
    }
    //void AddObstacle::run()
    //{
    //    // put your user code for the execution-phase here
    //    // runs in seperate thread, thus can do complex operations
    //    // should check constantly whether isRunningTaskStopped() returns true
    //
    //    // get a private kinematic instance for this state of the robot (tick "Robot State Component" proxy checkbox in statechart group)
    //    VirtualRobot::RobotPtr robot = getLocalRobot();
    //
    //// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //        // synchronize robot clone to most recent state
    //        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    //    }
    //}

    //void AddObstacle::onBreak()
    //{
    //    // put your user code for the breaking point here
    //    // execution time should be short (<100ms)
    //}

    void AddObstacle::onExit()
    {
        // put your user code for the exit point here
        // execution time should be short (<100ms)
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr AddObstacle::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new AddObstacle(stateData));
    }
}
