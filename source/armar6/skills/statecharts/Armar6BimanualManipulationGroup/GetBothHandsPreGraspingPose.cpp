/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BimanualManipulationGroup
 * @author     armar-user ( armar-user at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GetBothHandsPreGraspingPose.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include "Armar6BimanualManipulationGroupStatechartContext.generated.h"

namespace armarx
{
    namespace Armar6BimanualManipulationGroup
    {
        // DO NOT EDIT NEXT LINE
        GetBothHandsPreGraspingPose::SubClassRegistry GetBothHandsPreGraspingPose::Registry(GetBothHandsPreGraspingPose::GetName(), &GetBothHandsPreGraspingPose::CreateInstance);

        void GetBothHandsPreGraspingPose::onEnter()
        {
            // put your user code for the enter-point here
            // execution time should be short (<100ms)
            Eigen::Vector3f boxDims = in.getBoxDimensions()->toEigen();

            armarx::FramedPosePtr boxFramedPose = in.getBoxPose();
            Eigen::Matrix4f  boxPose = boxFramedPose->toEigen();
            Eigen::Matrix4f leftPose = in.getDefaultLeftPose()->toEigen();
            Eigen::Matrix4f rightPose = in.getDefaultRightPose()->toEigen();

            float width = boxDims[0];


            // set target pose for grasping box
            for (size_t i = 0; i < 3 ; ++i)
            {
                leftPose(i, 3) = boxPose(i, 3);
                rightPose(i, 3) = boxPose(i, 3);
            }

            leftPose(0, 3) = boxPose(0, 3) - width / 2;
            rightPose(0, 3) = boxPose(0, 3) + width / 2;


            // set via pose for grasping box
            Eigen::Matrix4f leftViaPose = leftPose;
            Eigen::Matrix4f rightViaPose = rightPose;
            double viaPointCanVal = in.getViaPointCanVal();
            leftViaPose.block<3, 1>(0, 3) += in.getLeftViaPoseOffset()->toEigen();
            rightViaPose.block<3, 1>(0, 3) += in.getRightViaPoseOffset()->toEigen();
            std::map<std::string, armarx::PosePtr > leftViaPoseMap;
            leftViaPoseMap[std::to_string(viaPointCanVal)] = new armarx::Pose(leftViaPose);
            out.setLeftViaPose(leftViaPoseMap);
            std::map<std::string, armarx::PosePtr > rightViaPoseMap;
            rightViaPoseMap[std::to_string(viaPointCanVal)] = new armarx::Pose(rightViaPose);
            out.setRightViaPose(rightViaPoseMap);

            ARMARX_INFO << VAROUT(leftViaPose);
            ARMARX_INFO << VAROUT(rightViaPose);


            // set lift and placing poses
            Eigen::Matrix4f liftPose = leftPose;
            // transform from mm to m
            liftPose.block<3, 1>(0, 3) = 0.5 * (leftPose.block<3, 1>(0, 3) + rightPose.block<3, 1>(0, 3)) * 0.001;
            Eigen::Matrix4f placePose = liftPose;
            liftPose.block<3, 1>(0, 3) += in.getLiftPoseOffsetInMeter()->toEigen();
            placePose.block<3, 1>(0, 3) += in.getPlacePoseOffsetInMeter()->toEigen();
            Eigen::Matrix4f transportPose = liftPose;
            transportPose(2, 3) = in.getTransportHeightInMeter();

            out.setDMPGoalsTransport(new Pose(transportPose));
            out.setDMPGoalsLifting(new Pose(liftPose));
            out.setDMPGoalsPlacing(new Pose(placePose));
            ARMARX_INFO << VAROUT(liftPose);
            ARMARX_INFO << VAROUT(placePose);


            // adjust left and right hand poses
            leftPose(0, 3) += in.getXOffset();
            rightPose(0, 3) -= in.getXOffset();
            leftPose(1, 3) = leftPose(1, 3) - 100;
            rightPose(1, 3) = rightPose(1, 3) - 100;
            leftPose(2, 3) = leftPose(2, 3) + in.getZOffset();
            rightPose(2, 3) = rightPose(2, 3) + in.getZOffset();
            out.setLeftPose(new armarx::Pose(leftPose));
            out.setRightPose(new armarx::Pose(rightPose));

            ARMARX_INFO << VAROUT(leftPose);
            ARMARX_INFO << VAROUT(rightPose);

            Eigen::Vector3f origGoalPose = in.getGoalPosePlatformForPlacing()->toEigen();
            float length = boxDims[1];
            float alpha = origGoalPose(2);
            Eigen::Vector3f dir(-sin(alpha), cos(alpha), 0);
            origGoalPose = origGoalPose - 1.5 * length * dir;
            out.setAdjustedGoalPosePlatformForPlacing(new Vector3(origGoalPose));
            out.setMoveToTableOffset(new Vector3(0, 1.5 * length + 100, 0));

            ARMARX_INFO << VAROUT(origGoalPose);

            emitSuccess();

        }

        //        void GetBothHandsPreGraspingPose::run()
        //        {

        //            VirtualRobot::RobotPtr robot = getLocalRobot();



        //        }

        //void GetBothHandsPreGraspingPose::onBreak()
        //{
        //    // put your user code for the breaking point here
        //    // execution time should be short (<100ms)
        //}

        void GetBothHandsPreGraspingPose::onExit()
        {
            // put your user code for the exit point here
            // execution time should be short (<100ms)
        }


        // DO NOT EDIT NEXT FUNCTION
        XMLStateFactoryBasePtr GetBothHandsPreGraspingPose::CreateInstance(XMLStateConstructorParams stateData)
        {
            return XMLStateFactoryBasePtr(new GetBothHandsPreGraspingPose(stateData));
        }
    }
}
