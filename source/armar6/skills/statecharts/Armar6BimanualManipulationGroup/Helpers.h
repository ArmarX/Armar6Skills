#ifndef _ARMARX_XMLUSERCODE_BIMANUALMANIPULATION_HELPERS_H
#define _ARMARX_XMLUSERCODE_BIMANUALMANIPULATION_HELPERS_H
#include <VirtualRobot/MathTools.h>

#include <boost/tokenizer.hpp>
#include <fstream>
class Helpers
{
public:

    static std::vector<float> pose2vec(const Eigen::Matrix4f& pose)
    {

        std::vector<float> result;
        result.resize(7);

        for (size_t i = 0; i < 3; ++i)
        {
            result.at(i) = pose(i, 3);
        }

        VirtualRobot::MathTools::Quaternion quat = VirtualRobot::MathTools::eigen4f2quat(pose);

        result.at(3) = quat.w;
        result.at(4) = quat.x;
        result.at(5) = quat.y;
        result.at(6) = quat.z;

        return result;

        return result;
    }

    static std::vector<double> pose2dvec(const Eigen::Matrix4f& pose)
    {

        std::vector<double> result;
        result.resize(7);

        for (size_t i = 0; i < 3; ++i)
        {
            result.at(i) = pose(i, 3);
        }

        VirtualRobot::MathTools::Quaternion quat = VirtualRobot::MathTools::eigen4f2quat(pose);

        result.at(3) = quat.w;
        result.at(4) = quat.x;
        result.at(5) = quat.y;
        result.at(6) = quat.z;

        return result;

        return result;
    }

    static void writeMPWeightsToFile(const std::vector<std::vector<double> >& weights, const std::string& filename)
    {
        std::ofstream outfile;
        outfile.open(filename, std::ios_base::app);

        for (size_t i = 0; i < weights.size(); ++i)
        {
            for (size_t j = 0; j < weights[i].size(); ++j)
            {
                outfile << weights[i][j];

                if (i != weights.size() - 1 || j != weights[i].size() - 1)
                {
                    outfile << ",";
                }
            }
        }
        outfile << "\n";
        outfile.close();
    }

    static std::vector<std::vector<double> > loadAllWeightsFromFile(const std::string& filename)
    {
        std::ifstream infile(filename.c_str());
        if (!infile.is_open())
        {
            std::cerr << "Could not open file: " << filename;
        }

        std::vector<std::vector<double> > weights;

        using Tokenizer = boost::tokenizer< boost::escaped_list_separator<char> >;

        std::vector<std::string> stringVec;
        std::string line;

        while (getline(infile, line))
        {
            Tokenizer tok(line);
            stringVec.assign(tok.begin(), tok.end());

            std::vector<double> weight;

            for (size_t i = 0; i < stringVec.size(); ++i)
            {
                std::istringstream stream(stringVec[i]);
                double cw;
                stream >> cw;
                weight.push_back(cw);
            }

            weights.push_back(weight);
        }
        infile.close();
        return weights;
    }

    static std::vector<std::vector<double> > getWeightsFromFlatVector(const std::vector<double>& weightsVec, int dim)
    {
        std::vector<std::vector<double> > weights;

        int kerSize = (int)weightsVec.size() / dim;
        int cid = 0;
        for (int i = 0; i < dim; ++i)
        {
            std::vector<double> weight;
            for (int j = 0; j < kerSize; ++j)
            {
                weight.push_back(weightsVec[cid]);
                cid++;
            }
            weights.push_back(weight);
        }

        return weights;
    }

};

#endif
