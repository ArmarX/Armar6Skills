/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GraspScrewdiverGroup::Armar6GraspScrewdiverGroupRemoteStateOfferer
 * @author     Julia Starke ( julia dot starke at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6GraspScrewdiverGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6GraspScrewdiverGroup;

// DO NOT EDIT NEXT LINE
Armar6GraspScrewdiverGroupRemoteStateOfferer::SubClassRegistry Armar6GraspScrewdiverGroupRemoteStateOfferer::Registry(Armar6GraspScrewdiverGroupRemoteStateOfferer::GetName(), &Armar6GraspScrewdiverGroupRemoteStateOfferer::CreateInstance);



Armar6GraspScrewdiverGroupRemoteStateOfferer::Armar6GraspScrewdiverGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6GraspScrewdiverGroupStatechartContext > (reader)
{
}

void Armar6GraspScrewdiverGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6GraspScrewdiverGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6GraspScrewdiverGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6GraspScrewdiverGroupRemoteStateOfferer::GetName()
{
    return "Armar6GraspScrewdiverGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6GraspScrewdiverGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6GraspScrewdiverGroupRemoteStateOfferer(reader));
}
