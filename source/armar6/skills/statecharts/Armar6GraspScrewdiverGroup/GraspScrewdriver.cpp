/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GraspScrewdiverGroup
 * @author     Julia Starke ( julia dot starke at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspScrewdriver.h"

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityController.h>

#include <ArmarXCore/core/time/TimeUtil.h>

#include <RobotAPI/libraries/core/CartesianPositionController.h>

#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <armar6/skills/statecharts/Armar6GraspScrewdiverGroup/Armar6GraspScrewdiverGroupStatechartContext.generated.h>

#include <RobotAPI/libraries/core/math/MathUtils.h>
#include <RobotAPI/libraries/core/FramedPose.h>


using namespace armarx;
using namespace Armar6GraspScrewdiverGroup;

// DO NOT EDIT NEXT LINE
GraspScrewdriver::SubClassRegistry GraspScrewdriver::Registry(GraspScrewdriver::GetName(), &GraspScrewdriver::CreateInstance);



void GraspScrewdriver::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void GraspScrewdriver::run()
{
    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());

    NJointCartesianVelocityControllerConfigPtr config = new NJointCartesianVelocityControllerConfig(in.getNodeSetName(), "", NJointCartesianVelocityControllerMode::eAll);
    NJointCartesianVelocityControllerInterfacePrx tcpController = NJointCartesianVelocityControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController(RobotUnitControllerNames::NJointCartesianVelocityController, "extendedTCPController", config));
    tcpController->activateController();

    std::string handControllerName = in.getHandControllerName();
    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx controller = StateBase::getContext<Armar6GraspScrewdiverGroupStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>(handControllerName);
    if (!controller->isControllerActive())
    {
        controller->activateController();
    }

    controller->setTargetsWithPwm(in.getPreshapeFingers(), in.getPreshapeThumb(), 1, 0.5);

    IceUtil::Time start = TimeUtil::GetTime();
    VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet(in.getNodeSetName());
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();
    float KpAvoidJointLimits = in.getKpAvoidJointLimits();

    Eigen::Matrix4f targetPose = tcp->getPoseInRootFrame();

    targetPose.block<3, 1>(0, 3) += in.getRelativeTarget()->toEigen();

    DatafieldRefPtr forceDf = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield(in.getFTDatefieldName()));

    Eigen::Vector3f initialForce;
    {
        FramedDirectionPtr forcePtr = forceDf->getDataField()->get<FramedDirection>();
        initialForce = forcePtr->toEigen();
    }

    CartesianPositionController posController(tcp);

    posController.maxPosVel = in.getMaxPositionVelocity();

    float minExecutionTime = in.getMinExecutionTime();
    float forceThreshold = in.getForceThreshold();

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        IceUtil::Time now = TimeUtil::GetTime();
        float t = (now - start).toSecondsDouble();

        FramedDirectionPtr forcePtr = forceDf->getDataField()->get<FramedDirection>();
        Eigen::Vector3f force = forcePtr->toEigen();
        force -= initialForce;

        if (t > minExecutionTime && force.norm() > forceThreshold)
        {
            tcpController->deactivateController();
            controller->setTargetsWithPwm(in.getPrecloseFingers(), in.getPreshapeThumb(), 1, 1);
            usleep(1000000);
            controller->setTargetsWithPwm(in.getPrecloseFingers(), in.getPrecloseThumb(), 1, 1);
            usleep(1000000);
            //controller->setTargetsWithPwm(in.getPrecloseFingers_2(), in.getPrecloseThumb_2(), 1, 1);
            //usleep(1000000);
            controller->setTargetsWithPwm(2, 1.2, 1, 1);
            break;
        }

        getDebugObserver()->setDebugDatafield("GraspScrewdriver", "force_x", new Variant(force(0)));
        getDebugObserver()->setDebugDatafield("GraspScrewdriver", "force_y", new Variant(force(1)));
        getDebugObserver()->setDebugDatafield("GraspScrewdriver", "force_z", new Variant(force(2)));


        Eigen::VectorXf cartesianVelocity = posController.calculate(targetPose, VirtualRobot::IKSolver::CartesianSelection::All);
        tcpController->setControllerTarget(
            cartesianVelocity(0), cartesianVelocity(1), cartesianVelocity(2),
            cartesianVelocity(3), cartesianVelocity(4), cartesianVelocity(5),
            KpAvoidJointLimits, NJointCartesianVelocityControllerMode::eAll);
        usleep(10000);

    }


    while (tcpController->isControllerActive())
    {
        usleep(10000);
    }

    tcpController->deleteController();
}

//void GraspScrewdriver::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GraspScrewdriver::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GraspScrewdriver::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GraspScrewdriver(stateData));
}
