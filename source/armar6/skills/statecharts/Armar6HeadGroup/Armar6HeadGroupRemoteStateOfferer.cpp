/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HeadGroup::Armar6HeadGroupRemoteStateOfferer
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6HeadGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6HeadGroup;

// DO NOT EDIT NEXT LINE
Armar6HeadGroupRemoteStateOfferer::SubClassRegistry Armar6HeadGroupRemoteStateOfferer::Registry(Armar6HeadGroupRemoteStateOfferer::GetName(), &Armar6HeadGroupRemoteStateOfferer::CreateInstance);



Armar6HeadGroupRemoteStateOfferer::Armar6HeadGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6HeadGroupStatechartContext > (reader)
{
}

void Armar6HeadGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6HeadGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6HeadGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6HeadGroupRemoteStateOfferer::GetName()
{
    return "Armar6HeadGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6HeadGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6HeadGroupRemoteStateOfferer(reader));
}
