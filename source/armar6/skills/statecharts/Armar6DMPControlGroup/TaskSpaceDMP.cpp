/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DMPControlGroup
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TaskSpaceDMP.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <armarx/control/deprecated_njoint_mp_controller/task_space/ControllerInterface.h>
#include "Helpers.h"

using namespace armarx;
using namespace Armar6DMPControlGroup;

// DO NOT EDIT NEXT LINE
TaskSpaceDMP::SubClassRegistry TaskSpaceDMP::Registry(TaskSpaceDMP::GetName(), &TaskSpaceDMP::CreateInstance);



void TaskSpaceDMP::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void TaskSpaceDMP::run()
{
    //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");


    double phaseL = 100;
    double phaseK = 1000;
    double phaseDist0 = 50;
    double phaseDist1 = 10;
    double phaseKp = 2;
    double phaseKd = 1;
    double posToOriRatio = 10;
    double amp = 1;
    double maxLinearVel = 1000;
    double maxAngularVel = 10;


    NJointTaskSpaceDMPControllerConfigPtr tsConfig = new NJointTaskSpaceDMPControllerConfig(
        50,
        in.getDMPKernelSize(),
        1,
        in.getBaseMode(),
        in.getDMPStyle(),
        amp,
        phaseL,
        phaseK,
        phaseDist0,
        phaseDist1,
        phaseKp,
        phaseKd,
        in.getTimeDuration(),
        posToOriRatio,
        in.getKinematicChainName(),
        "",
        "",
        NJointTaskSpaceDMPControllerMode::eAll,
        maxLinearVel,
        maxAngularVel,
        50.0,
        "DMP",
        in.getKpLinearVel(),
        in.getKdLinearVel(),
        in.getKpAngularVel(),
        in.getKdAngularVel(),
        1.0,
        1.0
    );

    NJointTaskSpaceDMPControllerInterfacePrx dmpController
        = NJointTaskSpaceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTSDMPController", "dmpController", tsConfig));


    std::vector<std::string> fileNames = in.getArmMotionFile();
    dmpController->learnDMPFromFiles(fileNames);

    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
    std::vector<double> goals ;

    if (in.getDMPStyle() == "Periodic")
    {
        goals = Helpers::pose2dvec(localRobot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPoseInRootFrame());
    }
    else
    {
        goals = Helpers::pose2dvec(in.getTcpGoalPose()->toEigen());
    }


    if (in.isViaPoseListSet() && in.isViaPoseCanValSet() && in.getViaPoseList().size() == in.getViaPoseCanVal().size())
    {
        for (size_t i = 0; i < in.getViaPoseList().size(); ++i)
        {
            dmpController->setViaPoints(in.getViaPoseCanVal().at(i), Helpers::pose2dvec(in.getViaPoseList().at(i)->toEigen()));
        }
    }

    dmpController->activateController();

    dmpController->runDMP(goals, 1.0);

    while (!isRunningTaskStopped() && !dmpController->isFinished())
    {

    }

    dmpController->deactivateController();
    while (dmpController->isControllerActive())
    {
        usleep(10000);
    }
    dmpController->deleteController();

    emitSuccess();

}

//void TaskSpaceDMP::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void TaskSpaceDMP::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TaskSpaceDMP::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TaskSpaceDMP(stateData));
}
