/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DMPControlGroup
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PeriodicTSDMP.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <armarx/control/deprecated_njoint_mp_controller/task_space/ControllerInterface.h>
#include "Helpers.h"
#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>

using namespace armarx;
using namespace Armar6DMPControlGroup;

// DO NOT EDIT NEXT LINE
PeriodicTSDMP::SubClassRegistry PeriodicTSDMP::Registry(PeriodicTSDMP::GetName(), &PeriodicTSDMP::CreateInstance);



void PeriodicTSDMP::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void PeriodicTSDMP::run()
{
    //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");


    double phaseL = 100;
    double phaseK = 1000;
    double phaseDist0 = 50000;
    double phaseDist1 = 1000;
    double phaseKpPos = 2;
    double phaseKpOri = 0.1;
    double posToOriRatio = 10;
    double maxLinearVel = 1000;
    double maxAngularVel = 10;
    float maxJointVel = 10;
    float avoidJointLimitsKp = 0;


    int kernelSize = in.getKernelSize();
    double timeDuration = in.getTimeDuration();
    double dmpAmplitude = in.getAmplitude();
    std::string nodeSetName = in.getKinematicChainName();
    float kpos = in.getKpos();
    float kori = in.getKori();

    std::string forceSensorName = in.getForceSensorName();
    std::string forceFrameName = in.getForceFrameName();
    float forceFilter = in.getForceFilterCoeff();
    float waitTimeForCalibration = in.getCalibrationTime();
    float kpf = in.getKpForce();
    float minimumReactForce = in.getMinimumReactForce();

    NJointPeriodicTSDMPControllerConfigPtr tsConfig = new NJointPeriodicTSDMPControllerConfig(
        kernelSize,
        dmpAmplitude,
        timeDuration,
        phaseL,
        phaseK,
        phaseDist0,
        phaseDist1,
        phaseKpPos,
        phaseKpOri,
        posToOriRatio,
        nodeSetName,
        maxLinearVel,
        maxAngularVel,
        maxJointVel,
        avoidJointLimitsKp,
        kpos,
        kori, forceSensorName, forceFrameName, forceFilter, waitTimeForCalibration, kpf, minimumReactForce
    );

    NJointPeriodicTSDMPControllerInterfacePrx dmpController
        = NJointPeriodicTSDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointPeriodicTSDMPForwardVelController", "dmpController", tsConfig));


    std::vector<std::string> fileNames = in.getArmMotionFileList();
    dmpController->learnDMPFromFiles(fileNames);


    std::vector<double> goals;
    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

    if (in.isAnchorPointSet())
    {
        goals = Helpers::pose2dvec(in.getAnchorPoint()->toEigen());
    }
    else
    {
        goals = Helpers::pose2dvec(localRobot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPoseInRootFrame());
    }

    dmpController->activateController();
    dmpController->runDMP(goals, in.getSpeed());


    //    RemoteGui::detail::VBoxLayoutBuilder rootLayoutBuilder = RemoteGui::makeVBoxLayout();
    //    auto makeSlider = [&](std::string name, float min, float max, float val)
    //    {
    //        rootLayoutBuilder.addChild(
    //            RemoteGui::makeHBoxLayout()
    //            .addChild(RemoteGui::makeTextLabel(name))
    //            .addChild(RemoteGui::makeFloatSlider(name).min(min).max(max).value(val))
    //        );
    //    };
    //    RemoteGui::WidgetPtr rootLayout = rootLayoutBuilder;
    //    getRemoteGui()->createTab("PeriodicVelDMP", rootLayout);
    //    RemoteGui::TabProxy guiTab = RemoteGui::TabProxy(getRemoteGui(), "PeriodicVelDMP");

    //    makeSlider("Speed", 0.5, 2, 1);
    //    makeSlider("Amplitude", 0.5, 2, 1);

    //    IceUtil::Time start = TimeUtil::GetTime();
    while (!isRunningTaskStopped() && !dmpController->isFinished())
    {
        //        IceUtil::Time now = TimeUtil::GetTime();
        //        float silent = (now - start).toSecondsDouble();

        //        if(silent > 1)
        //        {
        //            guiTab.receiveUpdates();
        //            float speed = guiTab.getValue<float>("Speed").get();
        //            float amplitude = guiTab.getValue<float>("Amplitude").get();
        //            dmpController->setSpeed(speed);
        //            dmpController->setAmplitude(amplitude);

        //            start = TimeUtil::GetTime();
        //        }

    }



    dmpController->deactivateController();
    while (dmpController->isControllerActive())
    {
        usleep(10000);
    }
    dmpController->deleteController();

    //    emitSuccess();
}

//void PeriodicTSDMP::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void PeriodicTSDMP::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr PeriodicTSDMP::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new PeriodicTSDMP(stateData));
}
