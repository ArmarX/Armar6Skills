/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DMPControlGroup
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CompliantTaskSpaceDMPWithForceStop.h"

//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <armarx/control/deprecated_njoint_mp_controller/task_space/TaskSpaceImpedanceDMPControllerInterface.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include "Helpers.h"
#include <armar6/skills/statecharts/Armar6DMPControlGroup/Armar6DMPControlGroupStatechartContext.generated.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <ArmarXCore/core/time/TimeUtil.h>

using namespace armarx;
using namespace Armar6DMPControlGroup;

// DO NOT EDIT NEXT LINE
CompliantTaskSpaceDMPWithForceStop::SubClassRegistry CompliantTaskSpaceDMPWithForceStop::Registry(CompliantTaskSpaceDMPWithForceStop::GetName(), &CompliantTaskSpaceDMPWithForceStop::CreateInstance);



void CompliantTaskSpaceDMPWithForceStop::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void CompliantTaskSpaceDMPWithForceStop::run()
{
    //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");

    NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController("CompliantTaskSpaceDMP");
    if (controller)
    {
        if (controller->isControllerActive())
        {
            controller->deactivateController();
            while (controller->isControllerActive())
            {
                TimeUtil::SleepMS(10);
            }
        }
        controller->deleteController();
        TimeUtil::SleepMS(10);
    }


    VirtualRobot::RobotPtr localRobot = getLocalRobot();
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
    double phaseL = 1000;
    double phaseK = 1000;
    double phaseDist0 = 10000;
    double phaseDist1 = 10000;
    double posToOriRatio = 10;


    std::vector<float> defaultJointValues;

    if (in.isDefaultJointValuesSet())
    {
        defaultJointValues = in.getDefaultJointValues();
    }
    else
    {
        defaultJointValues = localRobot->getRobotNodeSet(in.getKinematicChainName())->getJointValues();
    }

    bool useNullSpaceJointDMP;

    if (in.isNullSpaceJointDMPFileSet())
    {
        useNullSpaceJointDMP = true;
    }
    else
    {
        useNullSpaceJointDMP = false;
    }

    float torqueLimit = in.getTorqueLimit();

    float waitTimeForCalibration = 1;
    float forceFilter = 0.5;
    float forceDeadZone = 2;
    Eigen::Vector3f forceThreshold{100, 100, 100};
    std::string forceSensorName;
    std::string forceFrameName;
    if (in.getKinematicChainName() == "LeftArm")
    {
        forceSensorName = "FT L";
        forceFrameName = "ArmL8_Wri2";
    }
    else
    {
        forceSensorName = "FT R";
        forceFrameName = "ArmR8_Wri2";
    }



    NJointTaskSpaceImpedanceDMPControllerConfigPtr tsConfig = new NJointTaskSpaceImpedanceDMPControllerConfig(
        in.getDMPKernelSize(),
        in.getBaseMode(),
        in.getDMPStyle(),
        in.getTimeDuration(),
        in.getKinematicChainName(),
        phaseL,
        phaseK,
        phaseDist0,
        phaseDist1,
        posToOriRatio,
        in.getKposVec(),
        in.getDposVec(),
        in.getKoriVec(),
        in.getDoriVec(),
        in.getKnull(),
        in.getDnull(),
        useNullSpaceJointDMP,
        defaultJointValues,
        torqueLimit,
        forceSensorName,
        waitTimeForCalibration,
        forceFilter,
        forceDeadZone,
        forceThreshold,
        forceFrameName
    );

    std::string controllerName = "CompliantTaskSpaceDMPWithForceStop";
    if (in.isControllerNameSet())
    {
        controllerName = in.getControllerName();
    }

    NJointTaskSpaceImpedanceDMPControllerInterfacePrx dmpController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", controllerName, tsConfig));

    if (useNullSpaceJointDMP)
    {
        dmpController->learnJointDMPFromFiles(in.getNullSpaceJointDMPFile(), localRobot->getRobotNodeSet(in.getKinematicChainName())->getJointValues());
    }

    std::vector<std::string> fileNames = in.getArmMotionFile();
    ARMARX_IMPORTANT << VAROUT(in.getArmMotionFile());
    dmpController->learnDMPFromFiles(fileNames);
    std::vector<double> goals = Helpers::pose2dvec(in.getTcpGoalPose()->toEigen());

    if (in.isViaPoseListSet() && in.isViaPoseCanValSet() && in.getViaPoseList().size() == in.getViaPoseCanVal().size())
    {
        for (size_t i = 0; i < in.getViaPoseList().size(); ++i)
        {
            dmpController->setViaPoints(in.getViaPoseCanVal().at(i), Helpers::pose2dvec(in.getViaPoseList().at(i)->toEigen()));
        }
    }


    std::string handControllerName;
    std::string forceFieldName;
    if (in.getKinematicChainName() == "RightArm")
    {
        handControllerName = "Hand_R_EEF_NJointKITHandV2ShapeController";
        forceFieldName = "FT R_ArmR_FT";
    }
    else
    {
        handControllerName = "Hand_L_EEF_NJointKITHandV2ShapeController";
        forceFieldName = "FT L_ArmL_FT";
    }

    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx handcontroller = StateBase::getContext<Armar6DMPControlGroupStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>(handControllerName);
    handcontroller->activateController();
    DatafieldRefPtr forceDf = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield(forceFieldName));
    Eigen::Vector3f initialForce;
    initialForce.setZero();
    Eigen::Vector3f filteredForce;
    filteredForce.setZero();

    dmpController->activateController();
    dmpController->runDMP(goals);

    while (!isRunningTaskStopped() && !dmpController->isFinished())
    {
        FramedDirectionPtr forcePtr = forceDf->getDataField()->get<FramedDirection>();
        filteredForce = (1 - in.getForceFilterCoeff()) * filteredForce + in.getForceFilterCoeff() * forcePtr->toRootEigen(localRobot);

        if (dmpController->getVirtualTime() > in.getForceTimeOffset() * in.getTimeDuration())
        {
            initialForce = (1 - in.getForceFilterCoeff()) * initialForce + in.getForceFilterCoeff() * forcePtr->toRootEigen(localRobot);
        }
        else
        {
            Eigen::Vector3f normalizedForce = filteredForce - initialForce;

            if (normalizedForce.norm() > in.getForceThreshold())
            {
                if (in.getIsCloseHand())
                {
                    handcontroller->setTargetsWithPwm(1, 2, 1, 1);
                }
                else if (in.getIsOpenHand())
                {
                    handcontroller->setTargets(0, 0);
                }
                dmpController->stopDMP();
                break;
            }
        }

        usleep(10000);
    }

    dmpController->deactivateController();
    while (dmpController->isControllerActive())
    {
        usleep(10000);
    }
    dmpController->deleteController();
    emitSuccess();
}

//void CompliantTaskSpaceDMPWithForceStop::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void CompliantTaskSpaceDMPWithForceStop::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CompliantTaskSpaceDMPWithForceStop::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CompliantTaskSpaceDMPWithForceStop(stateData));
}
