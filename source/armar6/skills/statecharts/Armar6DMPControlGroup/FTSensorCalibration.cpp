/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DMPControlGroup
 * @author     JeffGao ( jianfenggaobit at gmail dot com )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "FTSensorCalibration.h"

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <RobotAPI/libraries/core/FramedPose.h>

#include <fstream>

using namespace armarx;
using namespace Armar6DMPControlGroup;

// DO NOT EDIT NEXT LINE
FTSensorCalibration::SubClassRegistry FTSensorCalibration::Registry(FTSensorCalibration::GetName(), &FTSensorCalibration::CreateInstance);



void FTSensorCalibration::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

Eigen::Matrix3f skew(const Eigen::Vector3f& vec)
{
    Eigen::Matrix3f mat;
    mat.setZero();
    mat(0, 1) = -vec(2);
    mat(0, 2) = vec(1);
    mat(1, 2) = -vec(0);
    mat(1, 0) = vec(2);
    mat(2, 0) = -vec(1);
    mat(2, 1) = vec(0);
    return mat;
}

NameControlModeMap setControlMode(const std::vector<std::string>& jointNameList, armarx::ControlMode ctrlMode)
{
    NameControlModeMap jvm;

    for (size_t i = 0; i < jointNameList.size(); ++i)
    {
        jvm[jointNameList.at(i)] = ctrlMode;
    }

    return jvm;
}


void FTSensorCalibration::run()
{
    VirtualRobot::RobotPtr robot = getLocalRobot();
    std::vector<std::string> jname = {"ArmL1_Cla1", "ArmL2_Sho1", "ArmL3_Sho2", "ArmL4_Sho3", "ArmL5_Elb1", "ArmL6_Elb2", "ArmL7_Wri1", "ArmL8_Wri2",
                                      "ArmR1_Cla1", "ArmR2_Sho1", "ArmR3_Sho2", "ArmR4_Sho3", "ArmR5_Elb1", "ArmR6_Elb2", "ArmR7_Wri1", "ArmR8_Wri2"
                                     };

    NameControlModeMap posCtrl = setControlMode(jname, ePositionControl);
    NameControlModeMap velCtrl = setControlMode(jname, eVelocityControl);

    NameValueMap jzerovel;
    for (size_t i = 0; i < jname.size(); ++i)
    {
        jzerovel[jname[i]] = 0;
    }

    NameValueMap jmaxvel;
    for (size_t i = 0; i < jname.size(); ++i)
    {
        jmaxvel[jname[i]] = 10;
    }


    DatafieldRefPtr forceLeftDf = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT"));
    DatafieldRefPtr torqueLeftDf = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getTorqueDatafield("FT L_ArmL_FT"));
    DatafieldRefPtr forceRightDf = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT"));
    DatafieldRefPtr torqueRightDf = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getTorqueDatafield("FT R_ArmR_FT"));

    std::vector<NameValueMap > jointTargetList;
    std::vector<bool> record;
    NameValueMap jvs =
    {
        {"ArmL1_Cla1", 0},
        {"ArmL2_Sho1", 0},
        {"ArmL3_Sho2", 0},
        {"ArmL4_Sho3", 0},
        {"ArmL5_Elb1", 1.57},
        {"ArmL6_Elb2", 0},
        {"ArmL7_Wri1", 0},
        {"ArmL8_Wri2", 0},
        {"ArmR1_Cla1", 0},
        {"ArmR2_Sho1", 0},
        {"ArmR3_Sho2", 0},
        {"ArmR4_Sho3", 0},
        {"ArmR5_Elb1", 1.57},
        {"ArmR6_Elb2", 0},
        {"ArmR7_Wri1", 0},
        {"ArmR8_Wri2", 0}
    };
    NameValueMap jzerov = jvs;
    record.push_back(false);
    jointTargetList.push_back(jvs);
    jvs["ArmL6_Elb2"] += 1.57;
    jvs["ArmR6_Elb2"] += 1.57;
    record.push_back(true);
    jointTargetList.push_back(jvs);
    jvs["ArmL6_Elb2"] += 1.57;
    jvs["ArmR6_Elb2"] += 1.57;
    record.push_back(true);
    jointTargetList.push_back(jvs);
    jvs["ArmL6_Elb2"] += 1.57;
    jvs["ArmR6_Elb2"] += 1.57;
    record.push_back(true);
    jointTargetList.push_back(jvs);
    jvs["ArmL6_Elb2"] += 1.57;
    jvs["ArmR6_Elb2"] += 1.57;
    jointTargetList.push_back(jvs);
    jvs["ArmL8_Wri2"] -= 1.5;
    jvs["ArmR8_Wri2"] -= 1.5;
    record.push_back(true);
    jointTargetList.push_back(jvs);
    jvs["ArmL8_Wri2"] += 3;
    jvs["ArmR8_Wri2"] += 3;
    record.push_back(true);
    jointTargetList.push_back(jvs);
    jvs["ArmL6_Elb2"] = -0.785;
    jvs["ArmR6_Elb2"] = -0.785;
    record.push_back(false);
    jointTargetList.push_back(jvs);
    jvs["ArmL6_Elb2"] = 0.785;
    jvs["ArmR6_Elb2"] = 0.785;
    record.push_back(false);
    jointTargetList.push_back(jvs);
    jvs["ArmL6_Elb2"] = 0;
    jvs["ArmR6_Elb2"] = 0;
    jvs["ArmL5_Elb1"] = 2.09;
    jvs["ArmR5_Elb1"] = 2.09;
    jvs["ArmL8_Wri2"] = 0.698;
    jvs["ArmR8_Wri2"] = 0.698;
    record.push_back(false);
    jointTargetList.push_back(jvs);
    jvs["ArmL8_Wri2"] = 1.396;
    jvs["ArmR8_Wri2"] = 1.396;
    record.push_back(false);
    jointTargetList.push_back(jvs);
    record.push_back(false);
    jointTargetList.push_back(jzerov);


    getKinematicUnit()->switchControlMode(posCtrl);

    std::vector<float> lforceVec;
    std::vector<float> ltorqueVec;
    std::vector<float> lgravityVec;
    std::vector<float> rforceVec;
    std::vector<float> rtorqueVec;
    std::vector<float> rgravityVec;
    Eigen::Vector3f gravity;
    gravity << 0.0, 0.0, -9.8;

    Eigen::VectorXf lftOffset;
    lftOffset.setZero(6);
    Eigen::VectorXf rftOffset;
    rftOffset.setZero(6);
    std::vector<int> foInd = {0, 1, 0, 1, -1, 2, 2, -1, -1, -1, -1, -1};
    std::vector<int> toInd = {5, -1, 5, -1, -1, -1, -1, 3, 3, 4, 4, -1};

    ARMARX_CHECK_EQUAL(toInd.size(), jointTargetList.size());
    ARMARX_CHECK_EQUAL(foInd.size(), jointTargetList.size());

    Eigen::VectorXf lftFilter;
    lftFilter.setZero(6);
    Eigen::VectorXf rftFilter;
    rftFilter.setZero(6);
    float filterCoeff = 0.8;

    for (size_t i = 0; i < jointTargetList.size(); ++i)
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

        getKinematicUnit()->setJointAngles(jointTargetList[i]);

        while (1)
        {
            RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

            bool targetReached = true;
            for (auto& value : jointTargetList[i])
            {
                float currentJv = robot->getRobotNode(value.first)->getJointValue();
                float targetJv = value.second;
                targetReached = targetReached && (fmod(fabs(targetJv - currentJv), 2 * M_PI) < 0.02) ;
            }

            FramedDirectionPtr lforcePtr = forceLeftDf->getDataField()->get<FramedDirection>();
            FramedDirectionPtr ltorquePtr = torqueLeftDf->getDataField()->get<FramedDirection>();
            lftFilter.head(3) = lftFilter.head(3) * (1 - filterCoeff) + lforcePtr->toEigen() * filterCoeff;
            lftFilter.tail(3) = lftFilter.tail(3) * (1 - filterCoeff) + ltorquePtr->toEigen() * filterCoeff;


            FramedDirectionPtr rforcePtr = forceRightDf->getDataField()->get<FramedDirection>();
            FramedDirectionPtr rtorquePtr = torqueRightDf->getDataField()->get<FramedDirection>();
            rftFilter.head(3) = rftFilter.head(3) * (1 - filterCoeff) + rforcePtr->toEigen() * filterCoeff;
            rftFilter.tail(3) = rftFilter.tail(3) * (1 - filterCoeff) + rtorquePtr->toEigen() * filterCoeff;

            if (record[i])
            {
                {
                    Eigen::Vector3f gravityInLocalFrame = robot->getRobotNode("ArmL8_Wri2")->getPoseInRootFrame().block<3, 3>(0, 0).transpose() * gravity;
                    lgravityVec.push_back(gravityInLocalFrame(0));
                    lgravityVec.push_back(gravityInLocalFrame(1));
                    lgravityVec.push_back(gravityInLocalFrame(2));
                    lforceVec.push_back(lftFilter(0));
                    lforceVec.push_back(lftFilter(1));
                    lforceVec.push_back(lftFilter(2));
                    ltorqueVec.push_back(lftFilter(3));
                    ltorqueVec.push_back(lftFilter(4));
                    ltorqueVec.push_back(lftFilter(5));
                }

                {
                    Eigen::Vector3f gravityInLocalFrame = robot->getRobotNode("ArmR8_Wri2")->getPoseInRootFrame().block<3, 3>(0, 0).transpose() * gravity;
                    rgravityVec.push_back(gravityInLocalFrame(0));
                    rgravityVec.push_back(gravityInLocalFrame(1));
                    rgravityVec.push_back(gravityInLocalFrame(2));
                    rforceVec.push_back(rftFilter(0));
                    rforceVec.push_back(rftFilter(1));
                    rforceVec.push_back(rftFilter(2));
                    rtorqueVec.push_back(rftFilter(3));
                    rtorqueVec.push_back(rftFilter(4));
                    rtorqueVec.push_back(rftFilter(5));
                }

            }

            if (targetReached)
            {
                if (foInd[i] != -1)
                {
                    lftOffset(foInd[i]) += lftFilter(foInd[i]);
                    rftOffset(foInd[i]) += rftFilter(foInd[i]);
                }
                if (toInd[i] != -1)
                {
                    lftOffset(toInd[i]) += lftFilter(toInd[i]);
                    rftOffset(toInd[i]) += rftFilter(toInd[i]);
                }

                break;
            }
            getKinematicUnit()->setJointAngles(jointTargetList[i]);

        }
    }

    lftOffset = lftOffset / 2.0;
    rftOffset = rftOffset / 2.0;

    getKinematicUnit()->setJointVelocities(jzerovel);

    for (size_t i = 0; i < lforceVec.size(); i = i + 3)
    {
        lforceVec[i] -= lftOffset[0];
        lforceVec[i + 1] -= lftOffset[1];
        lforceVec[i + 2] -= lftOffset[2];
        ltorqueVec[i] -= lftOffset[3];
        ltorqueVec[i + 1] -= lftOffset[4];
        ltorqueVec[i + 2] -= lftOffset[5];

        rforceVec[i] -= rftOffset[0];
        rforceVec[i + 1] -= rftOffset[1];
        rforceVec[i + 2] -= rftOffset[2];
        rtorqueVec[i] -= rftOffset[3];
        rtorqueVec[i + 1] -= rftOffset[4];
        rtorqueVec[i + 2] -= rftOffset[5];
    }


    Eigen::Vector3f leftCoM;
    float leftMass;
    // calculate mass;
    {
        Eigen::VectorXf force = Eigen::Map<Eigen::VectorXf, Eigen::Unaligned>(lforceVec.data(), lforceVec.size());
        Eigen::VectorXf torque = Eigen::Map<Eigen::VectorXf, Eigen::Unaligned>(ltorqueVec.data(), ltorqueVec.size());
        Eigen::VectorXf gravity = Eigen::Map<Eigen::VectorXf, Eigen::Unaligned>(lgravityVec.data(), lgravityVec.size());

        ARMARX_INFO << "Left Force: " << force;
        ARMARX_INFO << "Left Torque: " << torque;
        ARMARX_INFO << "Left Gravity: " << gravity;

        leftMass = force.dot(gravity) / gravity.dot(gravity);

        Eigen::MatrixXf SkewG;
        SkewG.setZero(gravity.rows(), 3);
        for (size_t i = 0; i < gravity.rows() / 3.0f; i++)
        {
            SkewG.block<3, 3>(i * 3, 0) = - skew(gravity.block<3, 1>(i * 3, 0));
        }

        ARMARX_INFO << "SkewG: " << SkewG;

        Eigen::MatrixXf inverseSkewG = (SkewG.transpose() * SkewG).inverse() * SkewG.transpose();
        ARMARX_INFO << "inverseSkewG: " << inverseSkewG;

        leftCoM =  inverseSkewG * torque * (1 / leftMass);
    }

    Eigen::Vector3f rightCoM;
    float rightMass;
    // calculate mass;
    {
        Eigen::VectorXf force = Eigen::Map<Eigen::VectorXf, Eigen::Unaligned>(rforceVec.data(), rforceVec.size());
        Eigen::VectorXf torque = Eigen::Map<Eigen::VectorXf, Eigen::Unaligned>(rtorqueVec.data(), rtorqueVec.size());
        Eigen::VectorXf gravity = Eigen::Map<Eigen::VectorXf, Eigen::Unaligned>(rgravityVec.data(), rgravityVec.size());

        ARMARX_INFO << "Right Force: " << force;
        ARMARX_INFO << "Right Torque: " << torque;
        ARMARX_INFO << "Right Gravity: " << gravity;

        rightMass = force.dot(gravity) / gravity.dot(gravity);


        Eigen::MatrixXf SkewG;
        SkewG.setZero(gravity.rows(), 3);
        for (size_t i = 0; i < gravity.rows() / 3.0f; i++)
        {
            SkewG.block<3, 3>(i * 3, 0) = - skew(gravity.block<3, 1>(i * 3, 0));
        }
        ARMARX_INFO << "SkewG: " << SkewG;

        Eigen::MatrixXf inverseSkewG = (SkewG.transpose() * SkewG).inverse() * SkewG.transpose();
        ARMARX_INFO << "inverseSkewG: " << inverseSkewG;

        rightCoM =  inverseSkewG * torque * (1 / rightMass);
    }

    out.setLeftCoM(new Vector3(leftCoM));
    out.setLeftMass(leftMass);
    out.setRightCoM(new Vector3(rightCoM));
    out.setRightMass(rightMass);

    ARMARX_INFO << "left com: " << leftCoM;
    ARMARX_INFO << "left mass: " << leftMass;
    ARMARX_INFO << "right com: " << rightCoM;
    ARMARX_INFO << "right mass: " << rightMass;

    std::string packageName = "armar6_skills";
    armarx::CMakePackageFinder finder(packageName);
    std::string dataDir = finder.getDataDir() + "/" + packageName + "/ftcalibration/";
    std::string filepath = dataDir + "ftCalibration";
    ARMARX_INFO << "filepath: " << filepath;
    std::ofstream ofs(filepath);

    ofs << "leftCOM: " << leftCoM[0] << "," << leftCoM[1] << "," << leftCoM[2] << "\n";
    ofs << "leftForceOffset: " << lftOffset[0] << "," << lftOffset[1] << "," << lftOffset[2] << "," << lftOffset[3] << "," << lftOffset[4] << "," << lftOffset[5] << "\n";
    ofs << "leftMass: " << leftMass << "\n";
    ofs << "rightCOM: " << rightCoM[0] << "," << rightCoM[1] << "," << rightCoM[2] << "\n";
    ofs << "rightForceOffset: " << rftOffset[0] << "," << rftOffset[1] << "," << rftOffset[2] << "," << rftOffset[3] << "," << rftOffset[4] << "," << rftOffset[5] << "\n";
    ofs << "rightMass: " << rightMass << "\n";

    ofs.close();


}

//void FTSensorCalibration::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void FTSensorCalibration::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr FTSensorCalibration::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new FTSensorCalibration(stateData));
}
