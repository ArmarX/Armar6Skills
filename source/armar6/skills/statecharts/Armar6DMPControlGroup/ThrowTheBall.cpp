/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DMPControlGroup
 * @author     JeffGao ( jianfenggaobit at gmail dot com )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ThrowTheBall.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <armarx/control/deprecated_njoint_mp_controller/task_space/TaskSpaceImpedanceDMPControllerInterface.h>
#include "Helpers.h"
#include <armar6/skills/statecharts/Armar6DMPControlGroup/Armar6DMPControlGroupStatechartContext.generated.h>

#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <boost/tokenizer.hpp>
#include <boost/serialization/vector.hpp>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>
#include <ArmarXGui/libraries/RemoteGui/RemoteGui.h>


using namespace armarx;
using namespace Armar6DMPControlGroup;

// DO NOT EDIT NEXT LINE
ThrowTheBall::SubClassRegistry ThrowTheBall::Registry(ThrowTheBall::GetName(), &ThrowTheBall::CreateInstance);



void ThrowTheBall::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void ThrowTheBall::run()
{


    //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");

    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx handcontroller;

    if (in.getHandName() == "RightHand")
    {
        if (!getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"))
        {
            handcontroller = StateBase::getContext<Armar6DMPControlGroupStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_R_EEF_NJointKITHandV2ShapeController");
        }
        else
        {
            handcontroller = devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"));
        }
        handcontroller->activateController();
    }
    else
    {
        if (!getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"))
        {
            handcontroller = StateBase::getContext<Armar6DMPControlGroupStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_L_EEF_NJointKITHandV2ShapeController");
        }
        else
        {
            handcontroller = devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"));
        }
        handcontroller->activateController();
    }


    NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController("CompliantTaskSpaceDMP");
    if (controller)
    {
        if (controller->isControllerActive())
        {
            controller->deactivateController();
            while (controller->isControllerActive())
            {
                TimeUtil::SleepMS(10);
            }
        }
        controller->deleteController();
        TimeUtil::SleepMS(10);
    }


    VirtualRobot::RobotPtr localRobot = getLocalRobot();
    double phaseL = 1000;
    double phaseK = 1000;
    double phaseDist0 = 10000;
    double phaseDist1 = 10000;
    double posToOriRatio = 10;


    std::vector<float> defaultJointValues;

    if (in.isDefaultJointValuesSet())
    {
        defaultJointValues = in.getDefaultJointValues();
    }
    else
    {
        defaultJointValues = localRobot->getRobotNodeSet(in.getKinematicChainName())->getJointValues();
    }

    bool useNullSpaceJointDMP;

    if (in.isNullSpaceJointDMPFileSet())
    {
        useNullSpaceJointDMP = true;
    }
    else
    {
        useNullSpaceJointDMP = false;
    }

    float torqueLimit = in.getTorqueLimit();

    float defaultOpenHand = (float)in.getOpenHandTime();

    RemoteGui::detail::VBoxLayoutBuilder rootLayoutBuilder = RemoteGui::makeVBoxLayout();

    float minOpen = 0;
    float maxOpen = 2.0;
    float minFactor = 0;
    float maxFactor = 2.0;
    float defualtFactor = 1.0;
    rootLayoutBuilder
    .addChild(RemoteGui::makeHBoxLayout()
              .addChild(RemoteGui::makeTextLabel("OpenHand: "))
              .addChild(RemoteGui::makeTextLabel(std::to_string(minOpen)))
              .addChild(RemoteGui::makeFloatSlider("OpenHand").min(minOpen).max(maxOpen).value(defaultOpenHand))
              .addChild(RemoteGui::makeTextLabel(std::to_string(maxOpen))))
    .addChild(RemoteGui::makeHBoxLayout()
              .addChild(RemoteGui::makeTextLabel("SpeedUpFactor: "))
              .addChild(RemoteGui::makeTextLabel(std::to_string(minFactor)))
              .addChild(RemoteGui::makeFloatSlider("SpeedUpFactor").min(minFactor).max(maxFactor).value(defualtFactor))
              .addChild(RemoteGui::makeTextLabel(std::to_string(maxFactor))))
    .addChild(RemoteGui::makeHBoxLayout()
              .addChild(RemoteGui::makeButton("Start").label("Start")));

    getRemoteGui()->createTab("CollectData", rootLayoutBuilder);
    RemoteGui::TabProxy guiTab = RemoteGui::TabProxy(getRemoteGui(), "CollectData");

    float timeToOpen;
    float speedUpFactor;
    while (1)
    {
        guiTab.receiveUpdates();
        timeToOpen = guiTab.getValue<float>("OpenHand").get();
        speedUpFactor = guiTab.getValue<float>("SpeedUpFactor").get();
        ARMARX_IMPORTANT << "TimeToOpen: " << timeToOpen << "SpeedUpFactor: " << speedUpFactor;

        RemoteGui::ButtonProxy button = guiTab.getButton("Start");

        if (button.clicked())
        {
            break;
        }

    }

    ARMARX_INFO << "throwtheball- timeduration : " << in.getTimeDuration();
    float waitTimeForCalibration = 1;
    float forceFilter = 0.5;
    float forceDeadZone = 2;
    Eigen::Vector3f forceThreshold{100, 100, 100};
    std::string forceSensorName;
    std::string forceFrameName;
    if (in.getKinematicChainName() == "LeftArm")
    {
        forceSensorName = "FT L";
        forceFrameName = "ArmL8_Wri2";
    }
    else
    {
        forceSensorName = "FT R";
        forceFrameName = "ArmR8_Wri2";
    }

    NJointTaskSpaceImpedanceDMPControllerConfigPtr tsConfig = new NJointTaskSpaceImpedanceDMPControllerConfig(
        in.getDMPKernelSize(),
        in.getBaseMode(),
        in.getDMPStyle(),
        in.getTimeDuration() * speedUpFactor,
        in.getKinematicChainName(),
        phaseL,
        phaseK,
        phaseDist0,
        phaseDist1,
        posToOriRatio,
        in.getKposVec(),
        in.getDposVec(),
        in.getKoriVec(),
        in.getDoriVec(),
        in.getKnull(),
        in.getDnull(),
        useNullSpaceJointDMP,
        defaultJointValues,
        torqueLimit,
        forceSensorName,
        waitTimeForCalibration,
        forceFilter,
        forceDeadZone,
        forceThreshold,
        forceFrameName
    );

    std::string controllerName = "CompliantTaskSpaceDMP";
    if (in.isControllerNameSet())
    {
        controllerName = in.getControllerName();
    }

    NJointTaskSpaceImpedanceDMPControllerInterfacePrx dmpController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", controllerName, tsConfig));

    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

    if (useNullSpaceJointDMP)
    {
        dmpController->learnJointDMPFromFiles(in.getNullSpaceJointDMPFile(), localRobot->getRobotNodeSet(in.getKinematicChainName())->getJointValues());
    }

    std::vector<std::string> fileNames = in.getArmMotionFile();
    ARMARX_IMPORTANT << VAROUT(in.getArmMotionFile());
    dmpController->learnDMPFromFiles(fileNames);

    if (in.isViaPoseListSet() && in.isViaPoseCanValSet() && in.getViaPoseList().size() == in.getViaPoseCanVal().size())
    {
        for (size_t i = 0; i < in.getViaPoseList().size(); ++i)
        {
            dmpController->setViaPoints(in.getViaPoseCanVal().at(i), Helpers::pose2dvec(in.getViaPoseList().at(i)->toEigen()));
        }
    }


    std::vector<double> goals;
    if (in.isTcpGoalPoseSet())
    {
        goals = Helpers::pose2dvec(in.getTcpGoalPose()->toEigen());
    }
    else
    {
        std::ifstream f(fileNames.at(0).c_str());

        if (!f.is_open())
        {
            ARMARX_ERROR << "Could not open file: " << fileNames.at(0);
        }

        using namespace boost;
        typedef tokenizer< escaped_list_separator<char> > Tokenizer;

        std::string line;
        std::string lastLine;
        while (getline(f, line))
        {
            lastLine = line;
        }
        Tokenizer tok(lastLine);
        std::vector< std::string > strVec;
        strVec.assign(tok.begin(), tok.end());
        if (strVec.size() == 0)
        {
            std::cout << "invalid reading of the last line" << lastLine << std::endl;
        }


        unsigned int j = 1;
        for (unsigned int sd = 0; j < strVec.size(); sd++, ++j)
        {
            goals.push_back(std::stod(strVec[j]));
        }

    }

    ARMARX_INFO << "Goal: \n{" << "\"qw\":" << goals[3] << ","
                << "\"qx\":" << goals[4] << ","
                << "\"qy\":" << goals[5] << ","
                << "\"qz\":" << goals[6] << ","
                << "\"x\":" << goals[0] << ","
                << "\"y\":" << goals[1] << ","
                << "\"z\":" << goals[2] << "}";




    dmpController->activateController();
    dmpController->runDMP(goals);
    IceUtil::Time start = TimeUtil::GetTime();

    while (!isRunningTaskStopped() && !dmpController->isFinished())
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        IceUtil::Time now = TimeUtil::GetTime();
        double t = (now - start).toSecondsDouble();

        if (t > timeToOpen * speedUpFactor)
        {
            handcontroller->setTargetsWithPwm(0, 0, 1, 1);
            Eigen::Matrix4f pose = localRobot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPoseInRootFrame();
            ARMARX_IMPORTANT << "open hand pose: " << pose;
        }
        //        if (dmpController->getVirtualTime() < in.getOpenHandCanVal() * in.getTimeDuration())
        //        {
        //            handcontroller->setTargetsWithPwm(0, 0, 1, 1);
        //        }
    }

    dmpController->deactivateController();
    while (dmpController->isControllerActive())
    {
        usleep(10000);
    }
    dmpController->deleteController();

    emitSuccess();

}

//void ThrowTheBall::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ThrowTheBall::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ThrowTheBall::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ThrowTheBall(stateData));
}
