/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DMPControlGroup
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BimanualVelCCDMP.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <armarx/control/deprecated_njoint_mp_controller/bimanual/ControllerInterface.h>

#include <ArmarXCore/core/time/TimeUtil.h>
#include "Helpers.h"

using namespace armarx;
using namespace Armar6DMPControlGroup;

// DO NOT EDIT NEXT LINE
BimanualVelCCDMP::SubClassRegistry BimanualVelCCDMP::Registry(BimanualVelCCDMP::GetName(), &BimanualVelCCDMP::CreateInstance);



void BimanualVelCCDMP::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void BimanualVelCCDMP::run()
{
    //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");

    NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController("BiCCDMPVelController");
    if (controller)
    {
        if (controller->isControllerActive())
        {
            controller->deactivateController();
            while (controller->isControllerActive())
            {
                TimeUtil::SleepMS(10);
            }
        }
        controller->deleteController();
        TimeUtil::SleepMS(10);
    }

    int kernelSize = 100;
    std::string dmpMode = "Linear";
    std::string dmpType = "Discrete";
    double timeDuration = in.getTimeDuration();
    std::string defaultLeader = in.getDefaultLeader();

    float maxLinearVel = in.getMaxLinearVel();
    float maxAngularVel = in.getMaxAngularVel();

    std::vector<float> leftKpos = in.getleftKposVec();
    std::vector<float> leftDpos = in.getleftDposVec();
    std::vector<float> leftKori = in.getleftKoriVec();
    std::vector<float> leftDori = in.getleftDoriVec();

    std::vector<float> rightKpos = in.getrightKposVec();
    std::vector<float> rightDpos = in.getrightDposVec();
    std::vector<float> rightKori = in.getrightKoriVec();
    std::vector<float> rightDori = in.getrightDoriVec();

    float knull = in.getKnull();
    float dnull = in.getDnull();

    float maxJointVel = in.getVelLimit();

    std::vector<float> leftDesiredJointValues;

    if (in.isDesiredLeftJointValuesSet())
    {
        leftDesiredJointValues = in.getDesiredLeftJointValues();
    }
    else
    {
        leftDesiredJointValues = getRobot()->getRobotNodeSet("LeftArm")->getJointValues();
    }

    std::vector<float> rightDesiredJointValues;
    if (in.isDesiredRightJointValuesSet())
    {
        rightDesiredJointValues = in.getDesiredRightJointValues();
    }
    else
    {
        rightDesiredJointValues = getRobot()->getRobotNodeSet("RightArm")->getJointValues();

    }


    NJointBimanualCCDMPVelocityControllerConfigPtr config = new NJointBimanualCCDMPVelocityControllerConfig(
        kernelSize,
        dmpMode,
        dmpType,
        timeDuration,
        defaultLeader,
        leftDesiredJointValues,
        rightDesiredJointValues,
        maxLinearVel,
        maxAngularVel,
        leftKpos,
        leftDpos,
        leftKori,
        leftDori,
        rightKpos,
        rightDpos,
        rightKori,
        rightDori,
        knull,
        dnull,
        maxJointVel, in.getInitRatio());


    NJointBimanualCCDMPVelocityControllerInterfacePrx dmpController
        = NJointBimanualCCDMPVelocityControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointBimanualCCDMPVelocityController", "BiCCDMPVelController", config));

    dmpController->learnDMPFromBothFiles(in.getLeftLeaderFileNames(), in.getRightLeaderFileNames());
    std::vector<double> leftGoals = Helpers::pose2dvec(in.getLeftGoals()->toEigen());
    std::vector<double> rightGoals = Helpers::pose2dvec(in.getRightGoals()->toEigen());


    dmpController->activateController();

    if (in.isViaPoseSet())
    {
        dmpController->setViaPoints(0.5, Helpers::pose2dvec(in.getViaPose()->toEigen()));
    }

    //    DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL8_Wri2"));
    //    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR8_Wri2"));
    //    Eigen::Vector3f initialForceLeft;
    //    Eigen::Vector3f initialForceRight;
    //    VirtualRobot::RobotPtr localRobot = getLocalRobot();
    //    {
    //        FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
    //        initialForceRight = forcePtr->toRootEigen(localRobot);
    //    }
    //    {
    //        FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
    //        initialForceLeft = forcePtr->toRootEigen(localRobot);
    //    }

    dmpController->runDMP(leftGoals, rightGoals);
    VirtualRobot::RobotPtr localRobot = getLocalRobot();

    while (!isRunningTaskStopped() /*&& !dmpController->isFinished()*/) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
    }

    dmpController->deactivateController();
    while (dmpController->isControllerActive())
    {
        usleep(10000);
    }
    dmpController->deleteController();

}

//void BimanualVelCCDMP::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void BimanualVelCCDMP::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr BimanualVelCCDMP::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new BimanualVelCCDMP(stateData));
}
