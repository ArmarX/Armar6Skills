/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DMPControlGroup
 * @author     JeffGao ( jianfenggaobit at gmail dot com )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "AnomalyDetectionAdaptiveWiping.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <armarx/control/deprecated_njoint_mp_controller/adaptive/ControllerInterface.h>

#include <Helpers.h>

using namespace armarx;
using namespace Armar6DMPControlGroup;

// DO NOT EDIT NEXT LINE
AnomalyDetectionAdaptiveWiping::SubClassRegistry AnomalyDetectionAdaptiveWiping::Registry(AnomalyDetectionAdaptiveWiping::GetName(), &AnomalyDetectionAdaptiveWiping::CreateInstance);



void AnomalyDetectionAdaptiveWiping::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void AnomalyDetectionAdaptiveWiping::run()
{
    getDebugDrawerTopic()->clearAll();
    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");
    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

    /* -------------------- create dmp controller config data -------------------- */

    double phaseL = 100;
    double phaseK = 1000;
    float phaseDist0 = in.getPhaseDist();
    float phaseDist1 = in.getTooFarDist();
    double phaseKpPos = in.getPhaseKpos();
    double phaseKpOri = 0.1;
    double posToOriRatio = 10;

    int kernelSize = in.getKernelSize();
    double timeDuration = in.getTimeDuration();
    double dmpAmplitude = in.getAmplitude();
    std::string nodeSetName = in.getKinematicChainName();
    std::vector<float> Kpos = in.getKpos();
    std::vector<float> Dpos = in.getDpos();
    std::vector<float> Kori = in.getKori();
    std::vector<float> Dori = in.getDori();
    std::vector<float> desiredNullSpaceJointValues = in.getNullSpaceJoints();

    std::vector<float> pidForce = in.getPidForce();
    std::vector<float> pidRot = in.getPidRot();
    std::vector<float> pidTorque = in.getPidTorque();
    std::vector<float> pidLCR = in.getPidLCR();

    std::string forceSensorName = in.getForceSensorName();
    std::string forceFrameName = in.getForceFrameName();
    float forceFilter = in.getForceFilterCoeff();
    float waitTimeForCalibration = in.getCalibrationTime();
    //float kpf = in.getKpForce();
    float minimumReactForce = in.getMinimumReactForce();
    float maxJointTorque = in.getMaxJointTorque();
    std::vector<float> Knull = in.getKnull();
    std::vector<float> Dnull = in.getDnull();
    float maxLinearVel = in.getMaxControlValue();
    float maxAngularVel = 10;

    float adaptCoeff = in.getAdaptCoeff();
    float reactThreshold = in.getReactThreshold();

    float dragForceDeadZone = in.getDragForceDeadZone();
    float adaptForceCoeff = in.getAdaptForceCoeff();

    float changePositionTolerance = in.getChangePositionTolerance();
    float changeTimerThreshold = in.getChangeTimerThreshold();


    std::vector<float> ws_x;
    ws_x.push_back(-500);
    ws_x.push_back(1200);
    std::vector<float> ws_y;
    ws_y.push_back(0);
    ws_y.push_back(1200);
    std::vector<float> ws_z;
    ws_z.push_back(100);
    ws_z.push_back(2200);

    if (in.isHardWorkSpaceSet())
    {
        StringFloatDictionary ws = in.getHardWorkSpace();
        ws_x[0] = ws["x_min"];
        ws_x[1] = ws["x_max"];
        ws_y[0] = ws["y_min"];
        ws_y[1] = ws["y_max"];
        ws_z[0] = ws["z_min"];
        ws_z[1] = ws["z_max"];

        // draw hard workspace
        if (in.getDrawDebug())
        {
            Eigen::Vector3f poly_left_bottom;
            poly_left_bottom << ws["x_min"], ws["y_min"], 1020;
            poly_left_bottom = localRobot->toGlobalCoordinateSystemVec(poly_left_bottom);
            Eigen::Vector3f poly_right_bottom;
            poly_right_bottom << ws["x_max"], ws["y_min"], 1020;
            poly_right_bottom = localRobot->toGlobalCoordinateSystemVec(poly_right_bottom);
            Eigen::Vector3f poly_left_up;
            poly_left_up << ws["x_min"], ws["y_max"], 1020;
            poly_left_up = localRobot->toGlobalCoordinateSystemVec(poly_left_up);
            Eigen::Vector3f poly_right_up;
            poly_right_up << ws["x_max"], ws["y_max"], 1020;
            poly_right_up = localRobot->toGlobalCoordinateSystemVec(poly_right_up);

            getDebugDrawerTopic()->setLineVisu("hardws", "polygon_left", new Vector3(poly_left_bottom), new Vector3(poly_left_up), 10, DrawColor {0, 0, 0, 1});
            getDebugDrawerTopic()->setLineVisu("hardws", "polygon_up", new Vector3(poly_left_up), new Vector3(poly_right_up), 10, DrawColor {0, 0, 0, 1});
            getDebugDrawerTopic()->setLineVisu("hardws", "polygon_right", new Vector3(poly_right_up), new Vector3(poly_right_bottom), 10, DrawColor {0, 0, 0, 1});
            getDebugDrawerTopic()->setLineVisu("hardws", "polygon_bottom", new Vector3(poly_right_bottom), new Vector3(poly_left_bottom), 10, DrawColor {0, 0, 0, 1});
        }
    }

    NJointAnomalyDetectionAdaptiveWipingControllerConfigPtr tsConfig = new NJointAnomalyDetectionAdaptiveWipingControllerConfig(
        kernelSize,
        dmpAmplitude,
        timeDuration,
        phaseL,
        phaseK,
        phaseDist0,
        phaseDist1,
        phaseKpPos,
        phaseKpOri,
        posToOriRatio,
        nodeSetName,
        maxJointTorque,
        desiredNullSpaceJointValues,
        Kpos,
        Dpos,
        Kori,
        Dori,
        Knull,
        Dnull,
        forceSensorName,
        forceFrameName,
        forceFilter,
        waitTimeForCalibration,
        in.getVelocityHorizon(),
        in.getFrictionHorizon(),
        in.getIsForceCtrlInForceDir(),
        in.getIsForceControlEnabled(),
        in.getIsRotControlEnabled(),
        in.getIsTorqueControlEnabled(),
        in.getIsLCRControlEnabled(),
        pidForce,
        pidRot,
        pidTorque,
        pidLCR,
        minimumReactForce,
        in.getForceDeadZone(),
        in.getVelFilter(),
        maxLinearVel,
        maxAngularVel,
        ws_x,
        ws_y,
        ws_z,
        adaptCoeff,
        reactThreshold,
        dragForceDeadZone, adaptForceCoeff, changePositionTolerance, changeTimerThreshold,   in.getZFToffset(), in.getZHandCOM(), in.getZHandMass(),
        in.getFTCommandFilter(),
        in.getFrictionCone(),
        in.getFricEstiFilter(),
        in.getVelNormThreshold(),
        in.getMaxInteractionForce()
    );

    /* -------------------- create dmp controller -------------------- */

    NJointAnomalyDetectionAdaptiveWipingControllerInterfacePrx dmpController
        = NJointAnomalyDetectionAdaptiveWipingControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointAnomalyDetectionAdaptiveWipingController", "dmpController", tsConfig));
    if (in.isObservedTrajectorySet())
    {
        dmpController->learnDMPFromTrajectory(in.getObservedTrajectory());
        ARMARX_IMPORTANT << "Learning dmp with trajectory";
    }
    else
    {
        std::vector<std::string> fileNames = in.getArmMotionFileList();
        dmpController->learnDMPFromFiles(fileNames);
    }

    std::vector<double> goals;
    if (in.isAnchorPointSet())
    {
        goals = Helpers::pose2dvec(in.getAnchorPoint()->toEigen());
    }
    else
    {
        goals = Helpers::pose2dvec(localRobot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPoseInRootFrame());
    }

    dmpController->activateController();
    dmpController->runDMP(goals, in.getSpeed());
    dmpController->setTargetForceInRootFrame(in.getTargetForce());


    /* -------------------- remote gui for speed and amplitude control -------------------- */
    StringFloatDictionary ampSetting = in.getAmpiltudeSetting();
    StringFloatDictionary speedSetting = in.getSpeedSetting();

    float minSpeed = speedSetting["MinSpeed"];
    float maxSpeed = speedSetting["MaxSpeed"];
    float defaultSpeed = speedSetting["DefaultSpeed"];
    float minAmp  = ampSetting["MinAmplitude"];
    float maxAmp = ampSetting["MaxAmplitude"];
    float defaultAmp = ampSetting["DefaultAmplitude"];

    if (maxSpeed > 3)
    {
        maxSpeed = 3;
    }
    if (maxAmp > 3)
    {
        maxAmp = 3;
    }

    RemoteGui::detail::VBoxLayoutBuilder rootLayoutBuilder = RemoteGui::makeVBoxLayout();
    rootLayoutBuilder
    .addChild(RemoteGui::makeHBoxLayout()
              .addChild(RemoteGui::makeTextLabel("Speed: "))
              .addChild(RemoteGui::makeTextLabel(std::to_string(minSpeed)))
              .addChild(RemoteGui::makeFloatSlider("Speed").min(minSpeed).max(maxSpeed).value(defaultSpeed))
              .addChild(RemoteGui::makeTextLabel(std::to_string(maxSpeed))))
    .addChild(RemoteGui::makeHBoxLayout()
              .addChild(RemoteGui::makeTextLabel("Amplitude"))
              .addChild(RemoteGui::makeTextLabel(std::to_string(minAmp)))
              .addChild(RemoteGui::makeFloatSlider("Amplitude").min(minAmp).max(maxAmp).value(defaultAmp))
              .addChild(RemoteGui::makeTextLabel(std::to_string(maxAmp))));

    getRemoteGui()->createTab("PeriodicCompliantDMP", rootLayoutBuilder);
    RemoteGui::TabProxy guiTab = RemoteGui::TabProxy(getRemoteGui(), "PeriodicCompliantDMP");
    IceUtil::Time sliderReactionStart = TimeUtil::GetTime();
    getDebugDrawerTopic()->clearLayer("trajLayer");
    int num = 1;

    /* -------------------- Loop -------------------- */

    while (!isRunningTaskStopped() && !dmpController->isFinished())
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        // force reading
        IceUtil::Time now = TimeUtil::GetTime();


        float sliderTD = (now - sliderReactionStart).toSecondsDouble();

        if (sliderTD > 2)
        {
            guiTab.receiveUpdates();
            float speed = guiTab.getValue<float>("Speed").get();
            dmpController->setSpeed(speed);
            float amplitude = guiTab.getValue<float>("Amplitude").get();
            dmpController->setAmplitude(amplitude);
            sliderReactionStart = TimeUtil::GetTime();
        }

        /* -------------------- anomaly detection -------------------- */


        // debug drawer (comment out during the real demo)
        if (in.getDrawDebug())
        {
            Eigen::Vector3f globalPosition = localRobot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getGlobalPosition();
            std::string trajPointName = "traj_" + std::to_string(num);
            getDebugDrawerTopic()->setSphereVisu("trajLayer", trajPointName, new Vector3(globalPosition), DrawColor {0, 1, 0, 1}, 5);
            num++;
            if (num > 5000)
            {
                num = 1;
            }
        }


        //        auto recognizedTextResponse = getSpeechObserver()->getDatafieldByName("SpeechToText", "RecognizedText");
        //        long recognizedTimestamp = recognizedTextResponse->getTimestamp();
        //        std::string response = recognizedTextResponse->getString();
        //        if (Contains(response, "thank", true) && (TimeUtil::GetTime() - IceUtil::Time::microSeconds(recognizedTimestamp)).toSecondsDouble() < 5)
        //        {
        //            break;
        //        }


    }

    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
    Eigen::Matrix4f targetPose =  localRobot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPoseInRootFrame();

    targetPose(2, 3) += 30;
    out.setTargetPose(new Pose(targetPose));

    dmpController->deactivateController();
    while (dmpController->isControllerActive())
    {
        usleep(10000);
    }
    dmpController->deleteController();
    emitSuccess();
}

//void AnomalyDetectionAdaptiveWiping::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void AnomalyDetectionAdaptiveWiping::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr AnomalyDetectionAdaptiveWiping::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new AnomalyDetectionAdaptiveWiping(stateData));
}
