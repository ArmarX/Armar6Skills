/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DMPControlGroup
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "KinetheticLearning.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <ArmarXCore/core/csv/CsvWriter.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/interface/core/BasicTypes.h>
#include <RobotAPI/interface/units/RobotUnit/NjointZeroTorqueController.h>

#include <RobotAPI/libraries/core/math/LinearizeAngularTrajectory.h>
#include <RobotAPI/libraries/core/math/TimeSeriesUtils.h>
#include <iostream>
#include <ctime>
#include <cstdio>

#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <armar6/skills/statecharts/Armar6DMPControlGroup/Armar6DMPControlGroupStatechartContext.generated.h>

#include <VirtualRobot/MathTools.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

using namespace armarx;
using namespace Armar6DMPControlGroup;

// DO NOT EDIT NEXT LINE
KinetheticLearning::SubClassRegistry KinetheticLearning::Registry(KinetheticLearning::GetName(), &KinetheticLearning::CreateInstance);



void KinetheticLearning::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void KinetheticLearning::run()
{
    //    NameControlModeMap controlModes;
    //    for (const std::string& name : in.getTorqueControlledJoints())
    //    {
    //        controlModes[name] = eTorqueControl;
    //    }
    //    getKinematicUnit()->switchControlMode(controlModes);

    NJointZeroTorqueControllerConfigPtr config(new NJointZeroTorqueControllerConfig());
    config->maxTorque = 0;
    std::string controllerName;

    if (in.isTorqueControlledJointsSet())
    {
        config->jointNames = in.getTorqueControlledJoints();
    }
    else if (in.isKinematicChainNameSet())
    {
        controllerName = in.getKinematicChainName();
        ARMARX_CHECK_EXPRESSION(getLocalRobot()->hasRobotNodeSet(in.getKinematicChainName())) << in.getKinematicChainName();
        auto nodeSet = getLocalRobot()->getRobotNodeSet(in.getKinematicChainName());
        for (auto& name : nodeSet->getNodeNames())
        {
            config->jointNames.push_back(name);
        }
    }
    ARMARX_CHECK_GREATER(config->jointNames.size(), 0) << "Either JointNames or KinematicChainName must bet set!";
    if (controllerName.empty())
    {
        controllerName = "ZeroTorqueCtrl" + simox::alg::join(config->jointNames, "-");
    }
    auto ctrlPrx = getRobotUnit()->getNJointController(controllerName);
    if (!ctrlPrx)
    {
        ctrlPrx = getRobotUnit()->createNJointController(RobotUnitControllerNames::NJointZeroTorqueController, controllerName, config);
    }
    getRobotUnit()->activateNJointController(controllerName);




    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    float motionThresholdRadian = in.getMotionThresholdRadian();
    int noMotionTimeoutMs = in.getNoMotionTimeoutMs();

    VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet(in.getKinematicChainName());
    std::vector<std::string> jointNames = rns->getNodeNames();
    IceUtil::Time start = TimeUtil::GetTime();
    IceUtil::Time lastMotionTime = start;
    bool initialMotionDetected = false;


    Eigen::VectorXf jve = rns->getJointValuesEigen();

    std::vector<std::vector<float>> jointData;
    for (size_t i = 0; i < jointNames.size(); i++)
    {
        std::vector<float> cjoint;
        cjoint.push_back(jve(i));
        jointData.push_back(cjoint);
    }


    std::vector<std::vector<float>> cartData;
    Eigen::Matrix4f initPose = rns->getTCP()->getPoseInRootFrame();
    VirtualRobot::MathTools::Quaternion initQuat = VirtualRobot::MathTools::eigen4f2quat(initPose);
    VirtualRobot::MathTools::Quaternion oldquatE = initQuat;
    float quatvec[] = {initPose(0, 3), initPose(1, 3), initPose(2, 3), initQuat.w, initQuat.x, initQuat.y,  initQuat.z};
    for (size_t i = 0; i < 7; ++i)
    {
        std::vector<float> cpos;
        cpos.push_back(quatvec[i]);
        cartData.push_back(cpos);
    }

    std::vector<float> timestamps;

    int motionStartIndex = 0;
    int motionEndIndex = 0;

    int sigmaMs = in.getFilterSigmaMs();
    int sampleMs = 10;

    //    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx handcontroller;

    //    if (!getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"))
    //    {
    //        handcontroller = StateBase::getContext<Armar6DMPControlGroupStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_R_EEF_NJointKITHandV2ShapeController");
    //    }
    //    else
    //    {
    //        handcontroller = devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"));
    //    }
    //    handcontroller->activateController();

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

        IceUtil::Time now = TimeUtil::GetTime();


        Eigen::VectorXf jve2 = rns->getJointValuesEigen();
        if ((jve2 - jve).norm() > motionThresholdRadian)
        {
            if (!initialMotionDetected)
            {
                ARMARX_IMPORTANT << "Start recording";
                motionStartIndex = 0;// timestamps.size();
                //start = now;
            }
            jve = jve2;
            initialMotionDetected = true;
            lastMotionTime = now;

        }

        double t = (now - start).toSecondsDouble();


        std::vector<float> jvs = rns->getJointValues();
        for (size_t i = 0; i < jvs.size(); i++)
        {
            jointData.at(i).push_back(jvs.at(i));
        }

        Eigen::Matrix4f tcpPose = rns->getTCP()->getPoseInRootFrame();
        cartData.at(0).push_back(tcpPose(0, 3));
        cartData.at(1).push_back(tcpPose(1, 3));
        cartData.at(2).push_back(tcpPose(2, 3));
        VirtualRobot::MathTools::Quaternion quat = VirtualRobot::MathTools::eigen4f2quat(tcpPose);
        double dotProduct = quat.w * oldquatE.w + quat.x * oldquatE.x + quat.y * oldquatE.y + quat.z * oldquatE.z;

        if (dotProduct < 0)
        {
            cartData.at(3).push_back(-quat.w);
            cartData.at(4).push_back(-quat.x);
            cartData.at(5).push_back(-quat.y);
            cartData.at(6).push_back(-quat.z);
            oldquatE.w = -quat.w;
            oldquatE.x = -quat.x;
            oldquatE.y = -quat.y;
            oldquatE.z = -quat.z;

        }
        else
        {
            cartData.at(3).push_back(quat.w);
            cartData.at(4).push_back(quat.x);
            cartData.at(5).push_back(quat.y);
            cartData.at(6).push_back(quat.z);

            oldquatE.w = quat.w;
            oldquatE.x = quat.x;
            oldquatE.y = quat.y;
            oldquatE.z = quat.z;
        }

        //        Eigen::Matrix4f pose = localRobot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPoseInRootFrame();

        //        if (pose(2, 3) > in.getOpenHandHeight())
        //        {
        //            handcontroller->setTargetsWithPwm(0, 0, 1, 1);
        //            ARMARX_IMPORTANT << "teaching open hand pose: " << pose;
        //        }
        //        if (in.isOpenHandTimeSet() && t > in.getOpenHandTime())
        //        {
        //            handcontroller-F>setTargetsWithPwm(0, 0, 1, 1);
        //            Eigen::Matrix4f pose = localRobot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPoseInRootFrame();
        //            ARMARX_IMPORTANT << "teaching==open hand pose: " << pose;
        //        }


        timestamps.push_back(t);


        if (initialMotionDetected && (now - lastMotionTime).toMilliSeconds() > noMotionTimeoutMs)
        {
            ARMARX_IMPORTANT << "Stop recording";
            motionEndIndex = timestamps.size();// - noMotionTimeoutMs / sampleMs;
            break;
        }

        TimeUtil::USleep(sampleMs * 1000);
    }

    motionStartIndex -= sigmaMs / sampleMs * 2;
    if (motionStartIndex < 0)
    {
        motionStartIndex = 0;
    }
    //motionEndIndex += sigmaMs / sampleMs * 2;
    //if (motionEndIndex >= data.size())
    //{
    //    motionEndIndex = data.size() - 1;
    //}


    timestamps = std::vector<float>(timestamps.begin() + motionStartIndex, timestamps.begin() + motionEndIndex);
    for (size_t n = 1; n < timestamps.size(); n++)
    {
        timestamps.at(n) = timestamps.at(n) - timestamps.at(0);
    }
    timestamps.at(0) = 0;

    std::vector<float> newT = math::TimeSeriesUtils::MakeTimestamps(timestamps.at(0), timestamps.at(timestamps.size() - 1), timestamps.size());

    {
        StringFloatSeqDict plotFiltered;
        StringFloatSeqDict plotOrig;
        StringFloatSeqDict plotResampled;
        for (size_t i = 0; i < jointData.size(); i++)
        {
            jointData.at(i) = std::vector<float>(jointData.at(i).begin() + motionStartIndex, jointData.at(i).begin() + motionEndIndex);
            if (rns->getNode(i)->isLimitless())
            {
                math::LinearizeAngularTrajectory::LinearizeRef(jointData.at(i));
            }
            plotOrig[jointNames.at(i)] = jointData.at(i);
            jointData.at(i) = math::TimeSeriesUtils::Resample(timestamps, jointData.at(i), newT);
            plotResampled[jointNames.at(i)] = jointData.at(i);
            jointData.at(i) = math::TimeSeriesUtils::ApplyGaussianFilter(jointData.at(i), sigmaMs * 0.001f, sampleMs * 0.001f, math::TimeSeriesUtils::BorderMode::Nearest);
            plotFiltered[jointNames.at(i)] = jointData.at(i);
        }
        getStaticPlotter()->addPlotWithTimestampVector("original joint motion", timestamps, plotOrig);
        getStaticPlotter()->addPlotWithTimestampVector("resampled joint motion", newT, plotResampled);
        getStaticPlotter()->addPlotWithTimestampVector("filtered joint motion", newT, plotFiltered);
    }

    std::vector<std::string> cartName = {"x", "y", "z", "qw", "qx", "qy", "qz"};

    //    {
    //        StringFloatSeqDict plotFiltered;
    //        StringFloatSeqDict plotOrig;
    //        StringFloatSeqDict plotResampled;


    //        for (size_t i = 0; i < cartData.size(); i++)
    //        {
    //            cartData.at(i) = std::vector<float>(cartData.at(i).begin() + motionStartIndex, cartData.at(i).begin() + motionEndIndex);
    //            plotOrig[cartName.at(i)] = cartData.at(i);
    //            jointData.at(i) = math::TimeSeriesUtils::Resample(timestamps, cartData.at(i), newT);
    //            plotResampled[cartName.at(i)] = cartData.at(i);
    //            jointData.at(i) = math::TimeSeriesUtils::ApplyGaussianFilter(cartData.at(i), sigmaMs * 0.001f, sampleMs * 0.001f, math::TimeSeriesUtils::BorderMode::Nearest);
    //            plotFiltered[cartName.at(i)] = cartData.at(i);
    //        }
    //        getStaticPlotter()->addPlotWithTimestampVector("original task space motion", timestamps, plotOrig);
    //        getStaticPlotter()->addPlotWithTimestampVector("resampled task space motion", newT, plotResampled);
    //        getStaticPlotter()->addPlotWithTimestampVector("filtered task space motion", newT, plotFiltered);
    //    }

    timestamps = newT;

    std::string trajName;
    if (in.isTrajNameSet())
    {
        trajName = in.getTrajName();
    }
    else
    {
        IceUtil::Time now = IceUtil::Time::now();
        time_t timer = now.toSeconds();
        struct tm* ts;
        char buffer[80];
        ts = localtime(&timer);
        strftime(buffer, 80, "%Y-%m-%d-%H-%M-%S", ts);
        std::string str(buffer);
        trajName = "temp-" + str;
    }

    double cutoutTime = (in.getNoMotionTimeoutMs() / 1000.0f) * 0.8;
    std::string jointSpacePath;

    std::vector<int> recordedIdx = in.getJointRecordIdx();


    std::string packageName = in.getPackageName();
    armarx::CMakePackageFinder finder(packageName);

    std::string jointbaseDir = "armar6_motion";
    std::string taskbaseDir = "armar6_motion";
    if (in.isJointBaseDirSet())
    {
        jointbaseDir = in.getJointBaseDir();
    }
    if (in.isTaskBaseDirSet())
    {
        taskbaseDir = in.getTaskBaseDir();
    }


    // write joint data
    {
        std::vector<std::string> header;
        header.push_back("timestamp");
        for (size_t i = 0; i < recordedIdx.size(); i++)
        {
            header.push_back(jointNames[recordedIdx[i]]);
        }

        std::string dataDir = finder.getDataDir() + "/" + jointbaseDir + "/";
        std::string fileName = "jointTraj_" + trajName + ".csv";
        std::string filepath = dataDir + fileName;
        jointSpacePath = filepath;
        ARMARX_IMPORTANT << "writing trajectory to " << filepath;
        CsvWriter writer(filepath, header, false);

        double endTime = timestamps.at(timestamps.size() - 1);

        std::vector<double> jointTarget;
        for (size_t n = 0; n < timestamps.size(); n++)
        {
            if (endTime - timestamps.at(n) < cutoutTime)
            {
                for (size_t i = 0; i < recordedIdx.size(); i++)
                {
                    jointTarget.push_back(jointData.at(recordedIdx[i]).at(n));
                }
                break;
            }

            std::vector<float> row;
            row.push_back(timestamps.at(n));
            for (size_t i = 0; i < recordedIdx.size(); i++)
            {
                row.push_back(jointData.at(recordedIdx[i]).at(n));
            }
            writer.write(row);


        }
        writer.close();

        std::string remote_dir_name = in.getRemoteJointTrajDir();
        std::string remote_file_path = remote_dir_name + fileName;
        std::vector<std::string> jointfiles;
        jointfiles.push_back(remote_file_path);
        out.setJointSpaceDMPFile(jointfiles);

        ARMARX_INFO << "jointTarget: " << jointTarget;
        out.setJointTargets(jointTarget);

    }

    // write cartesian data
    std::string taskSpaceDMPFile;
    {
        std::vector<std::string> header;
        header.push_back("timestamp");
        for (size_t i = 0; i < cartName.size(); ++i)
        {
            header.push_back(cartName.at(i));
        }

        std::string dataDir = finder.getDataDir() + "/" + taskbaseDir + "/";
        std::string fileName = "taskTraj_" + trajName + ".csv";
        std::string filepath = dataDir + fileName;
        taskSpaceDMPFile = filepath;
        CsvWriter writer(filepath, header, false);

        ARMARX_IMPORTANT << "writing trajectory to " << filepath;

        double endTime = timestamps.at(timestamps.size() - 1);
        for (size_t n = 0; n < timestamps.size(); n++)
        {


            std::vector<float> row;
            row.push_back(timestamps.at(n));
            for (size_t i = 0; i < cartData.size(); i++)
            {
                row.push_back(cartData.at(i).at(n));
            }
            writer.write(row);


            if (endTime - timestamps.at(n) < cutoutTime)
            {
                Eigen::Matrix4f endPose = VirtualRobot::MathTools::quat2eigen4f(row.at(5), row.at(6), row.at(7), row.at(4));
                endPose(0, 3) = row.at(1);
                endPose(1, 3) = row.at(2);
                endPose(2, 3) = row.at(3);
                out.setEndPose(endPose);

                double openHandCanVal = 1 - 0.8 / timestamps.at(n);

                out.setOpenHandCanVal(openHandCanVal);
                ARMARX_INFO << "kinethetic-timeduration : " << timestamps.at(n);
                out.setTimeDuration(timestamps.at(n) * in.getSlowerDownTimes());
                break;
            }

            //            if (n == timestamps.size() - 1)
            //            {


            //                ARMARX_INFO << "endPose: " << row.at(0) << " "
            //                            << row.at(1) << " "
            //                            << row.at(2) << " "
            //                            << row.at(3) << " "
            //                            << row.at(4) << " "
            //                            << row.at(5) << " "
            //                            << row.at(6) << " " << row.at(7);

            //            }
        }
        writer.close();

        std::string remote_dir_name = "/home/armar-user/repos/armarx_integration/robots/armar6/rt/data/armar6_motion/";
        std::string remote_file_path = remote_dir_name + fileName;
        std::vector<std::string> fileList;
        fileList.push_back(remote_file_path);
        out.setTaskSpaceDMPFile(fileList);

    }


    ctrlPrx->deactivateController();
    while (ctrlPrx->isControllerActive())
    {
        usleep(10000);
    }

    //    handcontroller->deactivateController();
    //    for (const std::string& name : in.getTorqueControlledJoints())
    //    {
    //        controlModes[name] = ePositionControl;
    //    }
    //    getKinematicUnit()->switchControlMode(controlModes);


    //    std::string command = "scp " + taskSpaceDMPFile + " armar-user@armar6a-0:/home/armar-user/repos/armarx_integration/robots/armar6/rt/data/armar6_motion/GraspDataCollection";
    //    auto r1 = system(command.c_str());
    std::string jointcmd = "scp " + jointSpacePath + " armar-user@armar6a-0:" + in.getRemoteJointTrajDir(); //" armar-user@armar6a-0:/home/armar-user/repos/armarx_integration/robots/armar6/rt/data/armar6_motion/GraspDataCollection";
    ARMARX_INFO << "copy to remote dir: " << in.getRemoteJointTrajDir();
    auto r2 = system(jointcmd.c_str());
    emitSuccess();



}

//void KinetheticLearning::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void KinetheticLearning::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr KinetheticLearning::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new KinetheticLearning(stateData));
}
