/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DMPControlGroup
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "JointSpaceDMP.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <armar6/skills/statecharts/Armar6DMPControlGroup/Armar6DMPControlGroupStatechartContext.generated.h>

#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <armarx/control/deprecated_njoint_mp_controller/joint_space/ControllerInterface.h>

#include "Helpers.h"

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
using namespace armarx;
using namespace Armar6DMPControlGroup;

// DO NOT EDIT NEXT LINE
JointSpaceDMP::SubClassRegistry JointSpaceDMP::Registry(JointSpaceDMP::GetName(), &JointSpaceDMP::CreateInstance);



void JointSpaceDMP::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void JointSpaceDMP::run()
{
    //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");
    //    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx handcontroller;


    //    if (!getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"))
    //    {
    //        handcontroller = StateBase::getContext<Armar6DMPControlGroupStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_R_EEF_NJointKITHandV2ShapeController");
    //    }
    //    else
    //    {
    //        handcontroller = devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"));
    //    }
    //    handcontroller->activateController();

    NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController("JSDMP");
    if (controller)
    {
        if (controller->isControllerActive())
        {
            controller->deactivateController();
            while (controller->isControllerActive())
            {
                TimeUtil::SleepMS(10);
            }
        }
        controller->deleteController();
        TimeUtil::SleepMS(10);
    }

    double phaseL = 1000;
    double phaseK = 1000;
    double phaseDist0 = 10000;
    double phaseDist1 = 10000;
    double phaseKp = in.getJointKp();

    int baseMode = 1;
    if (in.getBaseMode() == "MinimumJerk")
    {
        baseMode = 2;
    }

    NJointJointSpaceDMPControllerConfigPtr jsConfig = new NJointJointSpaceDMPControllerConfig(
        in.getControlJointNames(),
        in.getDMPKernelSize(),
        baseMode,
        phaseL,
        phaseK,
        phaseDist0,
        phaseDist1,
        phaseKp,
        in.getTimeDuration(), 10.0, false
    );

    std::string controllerName = "JSDMP";
    if (in.isControllerNameSet())
    {
        controllerName = in.getControllerName();
    }

    NJointJointSpaceDMPControllerInterfacePrx dmpController
        = NJointJointSpaceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointJSDMPController", controllerName, jsConfig));

    VirtualRobot::RobotPtr robot = getLocalRobot();
    RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

    std::vector<std::string> fileNames = in.getJointMotionFiles();
    ARMARX_IMPORTANT << VAROUT(in.getJointMotionFiles());
    dmpController->learnDMPFromFiles(fileNames);

    std::vector<double> goals = in.getJointGoals();
    dmpController->activateController();

    dmpController->runDMP(goals, 1);

    while (!isRunningTaskStopped() && !dmpController->isFinished()) // stop run function if returning true
    {
    }

    dmpController->deactivateController();
    while (dmpController->isControllerActive())
    {
        usleep(10000);
    }
    dmpController->deleteController();

    emitSuccess();
}

//void JointSpaceDMP::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void JointSpaceDMP::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr JointSpaceDMP::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new JointSpaceDMP(stateData));
}
