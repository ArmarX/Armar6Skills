/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DMPControlGroup
 * @author     JeffGao ( jianfenggaobit at gmail dot com )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TestWippingDirection.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6DMPControlGroup;

// DO NOT EDIT NEXT LINE
TestWippingDirection::SubClassRegistry TestWippingDirection::Registry(TestWippingDirection::GetName(), &TestWippingDirection::CreateInstance);



void TestWippingDirection::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

//void TestWippingDirection::run()
//{
//    VirtualRobot::RobotPtr localRobot = getLocalRobot();

//    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR8_Wri2"));

//    Eigen::Vector3f initialForceRight;

//    Eigen::Matrix4f currentRightPose;
//    VirtualRobot::RobotNodePtr tcpRight;
//    VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet("RightArm");

//    float forceFilterCoeff = 0.5;//in.getForceFilterCoeff();
//    Eigen::Vector3f filteredForceInRoot = Eigen::Vector3f::Zero();


//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

//        IceUtil::Time now = TimeUtil::GetTime();
//        float t = (now - start).toSecondsDouble();

//        if (t < 5)
//        {
//            {
//                FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
//                initialForceRight = forcePtr->toRootEigen(localRobot);
//            }

//            continue;
//        }

//        Eigen::Vector3f forceRight;
//        {
//            FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
//            forceRight = forcePtr->toRootEigen(localRobot);
//            forceRight -= initialForceRight;
//        }

//        filteredForceInRoot = (1 - forceFilterCoeff) * filteredForceInRoot + forceFilterCoeff * forceRight;

//        //         rotation changes

//        if (filteredForceInRoot.norm() > fabs(cfg->minimumReactForce))
//        {
//            Eigen::Vector3f desiredToolDir = filteredForceInRoot / filteredForceInRoot.norm();
//            currentToolDir = currentToolDir / currentToolDir.norm();
//            Eigen::Vector3f axis = currentToolDir.cross(desiredToolDir);
//            float angle = acosf(currentToolDir.dot(desiredToolDir));

//            if (fabs(angle) < M_PI / 2)
//            {
//                Eigen::AngleAxisf desiredToolRot(angle, axis);
//                Eigen::Matrix3f desiredRotMat = desiredToolRot * Eigen::Matrix3f::Identity();

//                targetPose.block<3, 3>(0, 0) = desiredRotMat * currentHandOri;

//                Eigen::Vector3f desiredRPY = VirtualRobot::MathTools::eigen3f2rpy(desiredRotMat);
//                Eigen::Vector3f checkedToolDir =  desiredRotMat * currentToolDir;
//                ARMARX_IMPORTANT << "axis: " << axis;
//                ARMARX_IMPORTANT << "angle: " << angle * 180 / 3.1415;
//                ARMARX_IMPORTANT << "desiredRotMat: " << desiredRotMat;

//                ARMARX_IMPORTANT << "desiredToolDir: " << desiredToolDir;
//                ARMARX_IMPORTANT << "currentToolDir: " << currentToolDir;
//                ARMARX_IMPORTANT << "checkedToolDir: " << checkedToolDir;
//            }

//        }
//    }
//}

//void TestWippingDirection::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void TestWippingDirection::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TestWippingDirection::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TestWippingDirection(stateData));
}
