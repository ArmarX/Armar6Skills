/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DMPControlGroup
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspingOverTheTable.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <armarx/control/deprecated_njoint_mp_controller/task_space/TaskSpaceImpedanceDMPControllerInterface.h>
#include "Helpers.h"
#include <armar6/skills/statecharts/Armar6DMPControlGroup/Armar6DMPControlGroupStatechartContext.generated.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>

using namespace armarx;
using namespace Armar6DMPControlGroup;

// DO NOT EDIT NEXT LINE
GraspingOverTheTable::SubClassRegistry GraspingOverTheTable::Registry(GraspingOverTheTable::GetName(), &GraspingOverTheTable::CreateInstance);



void GraspingOverTheTable::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void GraspingOverTheTable::run()
{
    //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");

    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

    double phaseL = 100;
    double phaseK = 1000;
    double phaseDist0 = 100;
    double phaseDist1 = 50;
    double posToOriRatio = 10;


    std::vector<float> defaultJointValues;

    if (in.isDefaultJointValuesSet())
    {
        defaultJointValues = in.getDefaultJointValues();
    }
    else
    {
        defaultJointValues = localRobot->getRobotNodeSet(in.getKinematicChainName())->getJointValues();
    }

    bool useNullSpaceJointDMP;

    if (in.isNullSpaceJointDMPFileSet())
    {
        useNullSpaceJointDMP = true;
    }
    else
    {
        useNullSpaceJointDMP = false;
    }

    float torqueLimit = in.getTorqueLimit();

    float waitTimeForCalibration = 1;
    float forceFilter = 0.5;
    float forceDeadZone = 2;
    Eigen::Vector3f forceThreshold{100, 100, 100};
    std::string forceSensorName;
    std::string forceFrameName;
    if (in.getKinematicChainName() == "LeftArm")
    {
        forceSensorName = "FT L";
        forceFrameName = "ArmL8_Wri2";
    }
    else
    {
        forceSensorName = "FT R";
        forceFrameName = "ArmR8_Wri2";
    }



    NJointTaskSpaceImpedanceDMPControllerConfigPtr tsConfig = new NJointTaskSpaceImpedanceDMPControllerConfig(
        in.getDMPKernelSize(),
        in.getBaseMode(),
        in.getDMPStyle(),
        in.getTimeDuration(),
        in.getKinematicChainName(),
        phaseL,
        phaseK,
        phaseDist0,
        phaseDist1,
        posToOriRatio,
        in.getKposVec(),
        in.getDposVec(),
        in.getKoriVec(),
        in.getDoriVec(),
        in.getKnull(),
        in.getDnull(),
        useNullSpaceJointDMP,
        defaultJointValues,
        torqueLimit,

        forceSensorName,
        waitTimeForCalibration,
        forceFilter,
        forceDeadZone,
        forceThreshold,
        forceFrameName
    );

    NJointTaskSpaceImpedanceDMPControllerInterfacePrx dmpController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", "dmpController", tsConfig));

    if (useNullSpaceJointDMP)
    {
        dmpController->learnJointDMPFromFiles(in.getNullSpaceJointDMPFile(), localRobot->getRobotNodeSet(in.getKinematicChainName())->getJointValues());
    }

    std::vector<std::string> fileNames = in.getArmMotionFile();
    dmpController->learnDMPFromFiles(fileNames);
    std::vector<double> goals = Helpers::pose2dvec(in.getTargetPose()->toEigen());


    Eigen::Matrix4f targetPose = in.getTargetPose()->toEigen();
    Eigen::Matrix4f viaPose = localRobot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPoseInRootFrame();

    viaPose(0, 3) = (viaPose(0, 3) + targetPose(0, 3)) / 2;
    viaPose(1, 3) = (viaPose(1, 3) + targetPose(1, 3)) / 2;

    if (viaPose(2, 3) > targetPose(2, 3))
    {
        viaPose(2, 3) = viaPose(2, 3) + in.getOffsetZ();
    }
    else
    {
        viaPose(2, 3) = targetPose(2, 3) + in.getOffsetZ();
    }
    dmpController->setViaPoints(0.5, Helpers::pose2dvec(viaPose));



    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx rightHandcontroller = StateBase::getContext<Armar6DMPControlGroupStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_R_EEF_NJointKITHandV2ShapeController");
    rightHandcontroller->activateController();

    dmpController->activateController();

    dmpController->runDMP(goals);

    bool handclosed = false;
    bool handopened = true;
    while (!isRunningTaskStopped() && !dmpController->isFinished())
    {
        if (dmpController->getVirtualTime() < 0.58 * in.getTimeDuration() && !handclosed && in.getIsCloseHand())
        {
            rightHandcontroller->setTargetsWithPwm(1, 2, 1, 1);
            handclosed = true;
        }
        if (handclosed && dmpController->getVirtualTime() < 0.6 * in.getTimeDuration() && !handopened && in.getIsOpenHand())
        {
            rightHandcontroller->setTargets(0, 0);
            handopened = true;
        }
    }

    dmpController->deactivateController();
    while (dmpController->isControllerActive())
    {
        usleep(10000);
    }
    dmpController->deleteController();

    emitSuccess();
}

//void GraspingOverTheTable::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GraspingOverTheTable::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GraspingOverTheTable::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GraspingOverTheTable(stateData));
}
