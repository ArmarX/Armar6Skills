/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DMPControlGroup::Armar6DMPControlGroupRemoteStateOfferer
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6DMPControlGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6DMPControlGroup;

// DO NOT EDIT NEXT LINE
Armar6DMPControlGroupRemoteStateOfferer::SubClassRegistry Armar6DMPControlGroupRemoteStateOfferer::Registry(Armar6DMPControlGroupRemoteStateOfferer::GetName(), &Armar6DMPControlGroupRemoteStateOfferer::CreateInstance);



Armar6DMPControlGroupRemoteStateOfferer::Armar6DMPControlGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6DMPControlGroupStatechartContext > (reader)
{
}

void Armar6DMPControlGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6DMPControlGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6DMPControlGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6DMPControlGroupRemoteStateOfferer::GetName()
{
    return "Armar6DMPControlGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6DMPControlGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6DMPControlGroupRemoteStateOfferer(reader));
}
