/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DMPControlGroup
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TaskSpaceDMPWithForceStop.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <armarx/control/deprecated_njoint_mp_controller/task_space/ControllerInterface.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include "Helpers.h"
#include <armar6/skills/statecharts/Armar6DMPControlGroup/Armar6DMPControlGroupStatechartContext.generated.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <ArmarXCore/core/time/TimeUtil.h>

using namespace armarx;
using namespace Armar6DMPControlGroup;

// DO NOT EDIT NEXT LINE
TaskSpaceDMPWithForceStop::SubClassRegistry TaskSpaceDMPWithForceStop::Registry(TaskSpaceDMPWithForceStop::GetName(), &TaskSpaceDMPWithForceStop::CreateInstance);



void TaskSpaceDMPWithForceStop::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void TaskSpaceDMPWithForceStop::run()
{
    //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");


    double phaseL = 100;
    double phaseK = 1000;
    double phaseDist0 = 50;
    double phaseDist1 = 10;
    double phaseKp = 2;
    double phaseKd = 1;
    double posToOriRatio = 10;
    double amp = 1;
    double maxLinearVel = 1000;
    double maxAngularVel = 10;


    NJointTaskSpaceDMPControllerConfigPtr tsConfig = new NJointTaskSpaceDMPControllerConfig(
        50,
        in.getDMPKernelSize(),
        1,
        in.getBaseMode(),
        in.getDMPStyle(),
        amp,
        phaseL,
        phaseK,
        phaseDist0,
        phaseDist1,
        phaseKp,
        phaseKd,
        in.getTimeDuration(),
        posToOriRatio,
        in.getKinematicChainName(),
        "",
        "",
        NJointTaskSpaceDMPControllerMode::eAll,
        maxLinearVel,
        maxAngularVel,
        50.0,
        "DMP",
        in.getKpLinearVel(),
        in.getKdLinearVel(),
        in.getKpAngularVel(),
        in.getKdAngularVel(),
        1.0,
        1.0
    );

    NJointTaskSpaceDMPControllerInterfacePrx dmpController
        = NJointTaskSpaceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTSDMPController", "dmpController", tsConfig));


    std::vector<std::string> fileNames = in.getArmMotionFile();
    dmpController->learnDMPFromFiles(fileNames);

    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
    std::vector<double> goals ;

    if (in.getDMPStyle() == "Periodic")
    {
        goals = Helpers::pose2dvec(localRobot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPoseInRootFrame());
    }
    else
    {
        goals = Helpers::pose2dvec(in.getTcpGoalPose()->toEigen());
    }


    if (in.isViaPoseListSet() && in.isViaPoseCanValSet() && in.getViaPoseList().size() == in.getViaPoseCanVal().size())
    {
        for (size_t i = 0; i < in.getViaPoseList().size(); ++i)
        {
            dmpController->setViaPoints(in.getViaPoseCanVal().at(i), Helpers::pose2dvec(in.getViaPoseList().at(i)->toEigen()));
        }
    }
    std::string handControllerName;
    std::string forceFieldName;
    if (in.getKinematicChainName() == "RightArm")
    {
        handControllerName = "Hand_R_EEF_NJointKITHandV2ShapeController";
        forceFieldName = "FT R_ArmR_FT";
    }
    else
    {
        handControllerName = "Hand_L_EEF_NJointKITHandV2ShapeController";
        forceFieldName = "FT L_ArmL_FT";
    }

    //    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx handcontroller = StateBase::getContext<Armar6DMPControlGroupStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>(handControllerName);
    //    handcontroller->activateController();
    DatafieldRefPtr forceDf = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield(forceFieldName));
    Eigen::Vector3f initialForce;
    initialForce.setZero();
    Eigen::Vector3f filteredForce;
    filteredForce.setZero();

    dmpController->activateController();
    dmpController->runDMP(goals, 1);

    while (!isRunningTaskStopped() && !dmpController->isFinished())
    {
        FramedDirectionPtr forcePtr = forceDf->getDataField()->get<FramedDirection>();
        filteredForce = (1 - in.getForceFilterCoeff()) * filteredForce + in.getForceFilterCoeff() * forcePtr->toRootEigen(localRobot);

        if (dmpController->getCanVal() > in.getForceTimeOffset() * in.getTimeDuration())
        {
            initialForce = (1 - in.getForceFilterCoeff()) * initialForce + in.getForceFilterCoeff() * forcePtr->toRootEigen(localRobot);
        }
        else
        {
            Eigen::Vector3f normalizedForce = filteredForce - initialForce;

            if (normalizedForce.norm() > in.getForceThreshold())
            {
                if (in.getIsCloseHand())
                {
                    //                    handcontroller->setTargetsWithPwm(1, 2, 1, 1);
                    ARMARX_INFO << "closing hand";
                }
                else if (in.getIsOpenHand())
                {
                    //                    handcontroller->setTargets(0, 0);
                    ARMARX_INFO << "opening hand";
                }
                break;
            }
        }

        usleep(10000);
    }

    dmpController->deactivateController();
    while (dmpController->isControllerActive())
    {
        usleep(10000);
    }
    dmpController->deleteController();

    emitSuccess();
}

//void TaskSpaceDMPWithForceStop::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void TaskSpaceDMPWithForceStop::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TaskSpaceDMPWithForceStop::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TaskSpaceDMPWithForceStop(stateData));
}
