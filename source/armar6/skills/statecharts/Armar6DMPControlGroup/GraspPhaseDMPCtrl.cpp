/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DMPControlGroup
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspPhaseDMPCtrl.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <armarx/control/deprecated_njoint_mp_controller/task_space/ControllerInterface.h>
#include "Helpers.h"
#include <armar6/skills/statecharts/Armar6DMPControlGroup/Armar6DMPControlGroupStatechartContext.generated.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>
#include <dmp/representation/trajectory.h>

namespace armarx
{
    namespace Armar6DMPControlGroup
    {
        // DO NOT EDIT NEXT LINE
        GraspPhaseDMPCtrl::SubClassRegistry GraspPhaseDMPCtrl::Registry(GraspPhaseDMPCtrl::GetName(), &GraspPhaseDMPCtrl::CreateInstance);

        void GraspPhaseDMPCtrl::onEnter()
        {
            // put your user code for the enter-point here
            // execution time should be short (<100ms)
        }

        void GraspPhaseDMPCtrl::run()
        {
            NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController("GraspPhaseDMPCtrl");
            if (controller)
            {
                if (controller->isControllerActive())
                {
                    controller->deactivateController();
                    while (controller->isControllerActive())
                    {
                        TimeUtil::SleepMS(10);
                    }
                }
                controller->deleteController();
                TimeUtil::SleepMS(10);
            }


            VirtualRobot::RobotPtr localRobot = getLocalRobot();
            RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
            double phaseL = 1000;
            double phaseK = 1000;
            double phaseDist0 = 10000;
            double phaseDist1 = 10000;
            double posToOriRatio = 10;


            std::vector<float> defaultJointValues;

            if (in.isDefaultJointValuesSet())
            {
                defaultJointValues = in.getDefaultJointValues();
            }
            else
            {
                defaultJointValues = localRobot->getRobotNodeSet(in.getKinematicChainName())->getJointValues();
            }

            bool useNullSpaceJointDMP;

            if (in.isNullSpaceJointDMPFileSet())
            {
                useNullSpaceJointDMP = true;
            }
            else
            {
                useNullSpaceJointDMP = false;
            }

            float torqueLimit = in.getTorqueLimit();

            float waitTimeForCalibration = 1;
            float forceFilter = 0.5;
            float forceDeadZone = 2;
            Eigen::Vector3f forceThreshold{100, 100, 100};
            std::string forceSensorName;
            std::string forceFrameName;
            if (in.getKinematicChainName() == "LeftArm")
            {
                forceSensorName = "FT L";
                forceFrameName = "ArmL8_Wri2";
            }
            else
            {
                forceSensorName = "FT R";
                forceFrameName = "ArmR8_Wri2";
            }

            NJointTaskSpaceImpedanceDMPControllerConfigPtr tsConfig = new NJointTaskSpaceImpedanceDMPControllerConfig(
                in.getDMPKernelSize(),
                in.getBaseMode(),
                in.getDMPStyle(),
                in.getTimeDuration(),
                in.getKinematicChainName(),
                phaseL,
                phaseK,
                phaseDist0,
                phaseDist1,
                posToOriRatio,
                in.getKposVec(),
                in.getDposVec(),
                in.getKoriVec(),
                in.getDoriVec(),
                in.getKnull(),
                in.getDnull(),
                useNullSpaceJointDMP,
                defaultJointValues,
                torqueLimit,
                forceSensorName,
                waitTimeForCalibration,
                forceFilter,
                forceDeadZone,
                forceThreshold,
                forceFrameName
            );

            std::string controllerName = "GraspPhaseDMPCtrl";
            if (in.isControllerNameSet())
            {
                controllerName = in.getControllerName();
            }

            NJointTaskSpaceImpedanceDMPControllerInterfacePrx dmpController
                = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", controllerName, tsConfig));

            if (useNullSpaceJointDMP)
            {
                dmpController->learnJointDMPFromFiles(in.getNullSpaceJointDMPFile(), localRobot->getRobotNodeSet(in.getKinematicChainName())->getJointValues());
            }

            std::vector<std::string> fileNames = in.getArmMotionFile();
            ARMARX_IMPORTANT << VAROUT(in.getArmMotionFile());
            dmpController->learnDMPFromFiles(fileNames);
            std::vector<double> goals = Helpers::pose2dvec(in.getTcpGoalPose()->toEigen());

            if (in.isViaPoseListSet() && in.isViaPoseCanValSet() && in.getViaPoseList().size() == in.getViaPoseCanVal().size())
            {
                for (size_t i = 0; i < in.getViaPoseList().size(); ++i)
                {
                    dmpController->setViaPoints(in.getViaPoseCanVal().at(i), Helpers::pose2dvec(in.getViaPoseList().at(i)->toEigen()));
                }
            }


            /* SETUP HAND TRAJECTORY */
            DMP::SampledTrajectoryV2 traj;
            if (in.isHandTrajFileSet())
            {
                traj.readFromCSVFile(in.getHandTrajFile());
            }


            devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx rightHandcontroller = StateBase::getContext<Armar6DMPControlGroupStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_R_EEF_NJointKITHandV2ShapeController");
            rightHandcontroller->activateController();
            ARMARX_INFO << "Opening hand at start";
            rightHandcontroller->setTargets(0, 0);


            dmpController->activateController();
            dmpController->runDMP(goals);

            //    bool handclosed = false;
            //    bool handopened = true;
            //    ARMARX_INFO << VAROUT(in.getIsCloseHand()) << VAROUT(in.getIsOpenHand());
            while (!isRunningTaskStopped() && !dmpController->isFinished())
            {
                if (in.isHandTrajFileSet())
                {
                    double t = dmpController->getVirtualTime() / in.getTimeDuration();
                    DMP::DVec states = traj.getStates(t, 0);
                    rightHandcontroller->setTargets(states[0], states[1]);
                }

                //        if (dmpController->getVirtualTime() < 0.58 * in.getTimeDuration() && !handclosed && in.getIsCloseHand())
                //        {
                //            ARMARX_INFO << deactivateSpam(1) << "Closing hand!";
                //            rightHandcontroller->setTargetsWithPwm(1, 2, 1, 1);
                //            handclosed = true;
                //        }
                //        if (handclosed && dmpController->getVirtualTime() < 0.6 * in.getTimeDuration() && !handopened && in.getIsOpenHand())
                //        {
                //            ARMARX_INFO << deactivateSpam(1) << "Opening hand!";
                //            rightHandcontroller->setTargets(0, 0);
                //            handopened = true;
                //        }
                usleep(10000);

            }

            dmpController->deactivateController();
            while (dmpController->isControllerActive())
            {
                usleep(10000);
            }
            dmpController->deleteController();

            emitSuccess();

        }

        //void GraspPhaseDMPCtrl::onBreak()
        //{
        //    // put your user code for the breaking point here
        //    // execution time should be short (<100ms)
        //}

        void GraspPhaseDMPCtrl::onExit()
        {
            // put your user code for the exit point here
            // execution time should be short (<100ms)
        }


        // DO NOT EDIT NEXT FUNCTION
        XMLStateFactoryBasePtr GraspPhaseDMPCtrl::CreateInstance(XMLStateConstructorParams stateData)
        {
            return XMLStateFactoryBasePtr(new GraspPhaseDMPCtrl(stateData));
        }
    }
}
