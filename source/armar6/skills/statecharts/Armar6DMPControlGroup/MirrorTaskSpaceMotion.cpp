/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DMPControlGroup
 * @author     armar-user ( armar6 at kit )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MirrorTaskSpaceMotion.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <dmp/representation/trajectory.h>

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <ArmarXCore/core/csv/CsvWriter.h>
#include "Helpers.h"
#include <armar6/skills/libraries/Armar6Tools/BimanualTransformationHelper.h>
namespace armarx
{
    namespace Armar6DMPControlGroup
    {
        // DO NOT EDIT NEXT LINE
        MirrorTaskSpaceMotion::SubClassRegistry MirrorTaskSpaceMotion::Registry(MirrorTaskSpaceMotion::GetName(), &MirrorTaskSpaceMotion::CreateInstance);

        void MirrorTaskSpaceMotion::onEnter()
        {
            // put your user code for the enter-point here
            // execution time should be short (<100ms)
        }

        void MirrorTaskSpaceMotion::run()
        {
            VirtualRobot::RobotPtr robot = getLocalRobot();

            std::string filepath = in.getTaskTrajFile();
            DMP::SampledTrajectoryV2 traj;
            traj.readFromCSVFile(filepath);

            std::string packageName = "armar6_rt";
            armarx::CMakePackageFinder finder(packageName);
            std::string dataDir = finder.getDataDir() + "/armar6_motion/";
            std::string fileName = in.getOutFile();
            std::string outfile = dataDir + fileName;

            std::vector<std::string> header({"timestamp", "x", "y", "z", "qw", "qx", "qy", "qz"});
            CsvWriter writer(outfile, header, false);
            VirtualRobot::MathTools::Quaternion oldq;

            DMP::DVec timesteps = traj.getTimestamps();

            for (size_t i = 0; i < timesteps.size(); ++i)
            {
                double t = timesteps[i];
                DMP::DVec trajPoint = traj.getStates(t, 0);
                Eigen::Matrix4f pose = Helpers::dvec2pose(trajPoint);
                Eigen::Matrix4f targetPose = BimanualTransformationHelper::reflectHandPoseAboutYZAxis(pose, "Armar6").toEigen();

                VirtualRobot::MathTools::Quaternion quat = VirtualRobot::MathTools::eigen4f2quat(targetPose);

                if (i != 0)
                {
                    float halfTheta = quat.w * oldq.w + quat.x * oldq.x + quat.y * oldq.y + quat.z * oldq.z;
                    if (halfTheta < 0)
                    {
                        quat.w = - quat.w;
                        quat.x = - quat.x;
                        quat.y = - quat.y;
                        quat.z = - quat.z;
                    }
                }

                oldq = quat;

                std::vector<float> row;
                row.push_back(t);
                row.push_back(targetPose(0, 3));
                row.push_back(targetPose(1, 3));
                row.push_back(targetPose(2, 3));
                row.push_back(quat.w);
                row.push_back(quat.x);
                row.push_back(quat.y);
                row.push_back(quat.z);
                writer.write(row);
            }
            writer.close();
        }

        //void MirrorTaskSpaceMotion::onBreak()
        //{
        //    // put your user code for the breaking point here
        //    // execution time should be short (<100ms)
        //}

        void MirrorTaskSpaceMotion::onExit()
        {
            // put your user code for the exit point here
            // execution time should be short (<100ms)
        }


        // DO NOT EDIT NEXT FUNCTION
        XMLStateFactoryBasePtr MirrorTaskSpaceMotion::CreateInstance(XMLStateConstructorParams stateData)
        {
            return XMLStateFactoryBasePtr(new MirrorTaskSpaceMotion(stateData));
        }
    }
}
