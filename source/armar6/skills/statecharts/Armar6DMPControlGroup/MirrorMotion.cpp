/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DMPControlGroup
 * @author     armar-user ( armar6 at kit )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MirrorMotion.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <dmp/representation/trajectory.h>
#include <ArmarXCore/core/csv/CsvWriter.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

namespace armarx
{
    namespace Armar6DMPControlGroup
    {
        // DO NOT EDIT NEXT LINE
        MirrorMotion::SubClassRegistry MirrorMotion::Registry(MirrorMotion::GetName(), &MirrorMotion::CreateInstance);

        void MirrorMotion::onEnter()
        {
            // put your user code for the enter-point here
            // execution time should be short (<100ms)
        }

        void MirrorMotion::run()
        {
            // put your user code for the execution-phase here
            // runs in seperate thread, thus can do complex operations
            // should check constantly whether isRunningTaskStopped() returns true

            // get a private kinematic instance for this state of the robot (tick "Robot State Component" proxy checkbox in statechart group)
            VirtualRobot::RobotPtr robot = getLocalRobot();
            std::string filepath = in.getJointTrajectoryFile();
            DMP::SampledTrajectoryV2 traj;
            traj.readFromCSVFile(filepath);

            VirtualRobot::RobotNodeSetPtr nodeSet = robot->getRobotNodeSet(in.getTargetArm());
            std::vector<std::string> jointNames = nodeSet->getNodeNames();
            DMP::DVec timesteps = traj.getTimestamps();
            std::vector<int> signs = in.getRotationSigns();
            ARMARX_IMPORTANT << signs;

            std::vector<std::string> header;
            header.push_back("timestamp");
            for (size_t i = 0; i < jointNames.size(); i++)
            {
                header.push_back(jointNames[i]);
            }
            std::string packageName = "armar6_rt";
            armarx::CMakePackageFinder finder(packageName);
            std::string dataDir = finder.getDataDir() + "/armar6_motion/";
            std::string fileName = in.getOutFile();
            std::string outfile = dataDir + fileName;
            std::string outJointFile = dataDir + in.getOutJointFile();
            ARMARX_IMPORTANT << "writing trajectory to " << outfile;
            CsvWriter writer(outfile, header, false);
            CsvWriter jointWriter(outJointFile, header, false);
            Eigen::Quaternionf oldq;
            bool firstTime = true;
            for (auto t : timesteps)
            {
                DMP::DVec trajPoint = traj.getStates(t, 0);
                ARMARX_CHECK_EQUAL(trajPoint.size(), signs.size());


                std::vector<float> trajPointf(trajPoint.size());
                for (size_t i = 0; i < trajPointf.size(); ++i)
                {
                    trajPointf[i] = trajPoint[i] * (float) signs[i];
                }
                ARMARX_INFO << trajPointf;

                //std::copy(trajPoint.begin(), trajPoint.end(), std::back_inserter(trajPointf));

                nodeSet->setJointValues(trajPointf);
                Eigen::Affine3f pose(nodeSet->getTCP()->getPoseInRootFrame());

                trajPointf.insert(trajPointf.begin(), t);
                jointWriter.write(trajPointf);

                std::vector<float> row;
                row.push_back(t);
                row.push_back(pose.translation().x());
                row.push_back(pose.translation().y());
                row.push_back(pose.translation().z());

                Eigen::Quaternionf q(pose.linear());
                if (firstTime)
                {
                    oldq = q;
                    firstTime = false;
                }

                double cosHalfTheta = q.w() * oldq.w() + q.x() * oldq.x() + q.y() * oldq.y() + q.z() * oldq.z();
                if (cosHalfTheta < 0)
                {
                    q.w() = -q.w();
                    q.x() = -q.x();
                    q.y() = -q.y();
                    q.z() = -q.z();
                }
                row.push_back(q.w());
                row.push_back(q.x());
                row.push_back(q.y());
                row.push_back(q.z());

                oldq = q;

                writer.write(row);



            }
            writer.close();
            jointWriter.close();

        }

        //void MirrorMotion::onBreak()
        //{
        //    // put your user code for the breaking point here
        //    // execution time should be short (<100ms)
        //}

        void MirrorMotion::onExit()
        {
            // put your user code for the exit point here
            // execution time should be short (<100ms)
        }


        // DO NOT EDIT NEXT FUNCTION
        XMLStateFactoryBasePtr MirrorMotion::CreateInstance(XMLStateConstructorParams stateData)
        {
            return XMLStateFactoryBasePtr(new MirrorMotion(stateData));
        }
    }
}
