/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DMPControlGroup
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PeriodicTSCompliantDMP.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <armarx/control/deprecated_njoint_mp_controller/task_space/ControllerInterface.h>
#include "Helpers.h"
#include <ArmarXGui/libraries/RemoteGui/RemoteGui.h>
#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>
#include <RobotAPI/components/ArViz/Client/Client.h>
#include <armar6/skills/statecharts/Armar6DMPControlGroup/Armar6DMPControlGroupStatechartContext.generated.h>
#include <VirtualRobot/Manipulability/SingleChainManipulability.h>

using namespace armarx;
using namespace Armar6DMPControlGroup;

// DO NOT EDIT NEXT LINE
PeriodicTSCompliantDMP::SubClassRegistry PeriodicTSCompliantDMP::Registry(PeriodicTSCompliantDMP::GetName(), &PeriodicTSCompliantDMP::CreateInstance);



void PeriodicTSCompliantDMP::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void PeriodicTSCompliantDMP::run()
{
    Armar6DMPControlGroupStatechartContext* context = getContext<Armar6DMPControlGroupStatechartContext>();
    viz::Client arviz(*context);


    //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");
    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
    VirtualRobot::RobotNodeSetPtr nodeSet = localRobot->getRobotNodeSet(in.getKinematicChainName());

    double phaseL = 100;
    double phaseK = 1000;
    float phaseDist0 = in.getPhaseDist();
    float phaseDist1 = in.getTooFarDist();
    double phaseKpPos = in.getPhaseKpos();
    double phaseKpOri = 0.1;
    double posToOriRatio = 10;



    int kernelSize = in.getKernelSize();
    double timeDuration = in.getTimeDuration();
    double dmpAmplitude = in.getAmplitude();
    std::string nodeSetName = in.getKinematicChainName();
    std::vector<float> Kpos = in.getKpos();
    std::vector<float> Dpos = in.getDpos();
    std::vector<float> Kori = in.getKori();
    std::vector<float> Dori = in.getDori();
    std::vector<float> desiredNullSpaceJointValues = in.getNullSpaceJoints();

    std::string forceSensorName = in.getForceSensorName();
    std::string forceFrameName = in.getForceFrameName();
    float forceFilter = in.getForceFilterCoeff();
    float waitTimeForCalibration = in.getCalibrationTime();
    float kpf = in.getKpForce();
    float minimumReactForce = in.getMinimumReactForce();
    float maxJointTorque = in.getMaxJointTorque();
    float Knull = in.getKnull();
    float Dnull = in.getDnull();
    float maxLinearVel = in.getMaxControlValue();
    float maxAngularVel = 10;

    float adaptCoeff = in.getAdaptCoeff();
    float reactThreshold = in.getReactThreshold();

    float dragForceDeadZone = in.getDragForceDeadZone();
    float adaptForceCoeff = in.getAdaptForceCoeff();

    float changePositionTolerance = in.getChangePositionTolerance();
    float changeTimerThreshold = in.getChangeTimerThreshold();

    bool stopAfter = false;
    int stopAfterMS = -1;
    if (in.isStopAfterMSSet())
    {
        stopAfter = true;
        stopAfterMS = in.getStopAfterMS();
    }

    std::vector<float> ws_x;
    ws_x.push_back(-500);
    ws_x.push_back(1200);
    std::vector<float> ws_y;
    ws_y.push_back(0);
    ws_y.push_back(1200);
    std::vector<float> ws_z;
    ws_z.push_back(100);
    ws_z.push_back(2200);

    if (in.isHardWorkSpaceSet())
    {
        StringFloatDictionary ws = in.getHardWorkSpace();
        ws_x[0] = ws["x_min"];
        ws_x[1] = ws["x_max"];
        ws_y[0] = ws["y_min"];
        ws_y[1] = ws["y_max"];
        ws_z[0] = ws["z_min"];
        ws_z[1] = ws["z_max"];

        // draw hard workspace
        if (in.getDrawDebug())
        {
            Eigen::Vector3f poly_left_bottom;
            poly_left_bottom << ws["x_min"], ws["y_min"], 1020;
            poly_left_bottom = localRobot->toGlobalCoordinateSystemVec(poly_left_bottom);
            Eigen::Vector3f poly_right_bottom;
            poly_right_bottom << ws["x_max"], ws["y_min"], 1020;
            poly_right_bottom = localRobot->toGlobalCoordinateSystemVec(poly_right_bottom);
            Eigen::Vector3f poly_left_up;
            poly_left_up << ws["x_min"], ws["y_max"], 1020;
            poly_left_up = localRobot->toGlobalCoordinateSystemVec(poly_left_up);
            Eigen::Vector3f poly_right_up;
            poly_right_up << ws["x_max"], ws["y_max"], 1020;
            poly_right_up = localRobot->toGlobalCoordinateSystemVec(poly_right_up);

            getDebugDrawerTopic()->setLineVisu("hardws", "polygon_left", new Vector3(poly_left_bottom), new Vector3(poly_left_up), 10, DrawColor {0, 0, 0, 1});
            getDebugDrawerTopic()->setLineVisu("hardws", "polygon_up", new Vector3(poly_left_up), new Vector3(poly_right_up), 10, DrawColor {0, 0, 0, 1});
            getDebugDrawerTopic()->setLineVisu("hardws", "polygon_right", new Vector3(poly_right_up), new Vector3(poly_right_bottom), 10, DrawColor {0, 0, 0, 1});
            getDebugDrawerTopic()->setLineVisu("hardws", "polygon_bottom", new Vector3(poly_right_bottom), new Vector3(poly_left_bottom), 10, DrawColor {0, 0, 0, 1});
        }
    }

    bool ignoreWSLimitChecks = in.getIgnoreWSLimitChecks();


    bool isManipulability = in.getIsManipulability();
    std::vector<double> kmani(3, 0);
    std::vector<double> positionManipulability(9, 1);
    std::vector<double> maniWeightMat(nodeSet->getNodeNames().size(), 1);
    Eigen::MatrixXd desiredMani(3, 3);

    if (in.isPositionManipulabilitySet())
    {
        positionManipulability = in.getPositionManipulability();
        desiredMani << positionManipulability[0],  positionManipulability[1], positionManipulability[2],
                    positionManipulability[3],  positionManipulability[4], positionManipulability[5],
                    positionManipulability[6],  positionManipulability[7], positionManipulability[8];
    }

    if (in.isKmaniSet())
    {
        kmani = in.getKmani();
    }

    if (in.isManipulabilityWeightVecSet())
    {
        maniWeightMat = in.getManipulabilityWeightVec();
    }

    if (!(in.isKmaniSet() && in.isPositionManipulabilitySet()))
    {
        isManipulability = false;
    }

    NJointPeriodicTSDMPCompliantControllerConfigPtr tsConfig = new NJointPeriodicTSDMPCompliantControllerConfig(
        kernelSize,
        dmpAmplitude,
        timeDuration,
        phaseL,
        phaseK,
        phaseDist0,
        phaseDist1,
        phaseKpPos,
        phaseKpOri,
        posToOriRatio,
        nodeSetName,
        maxJointTorque,
        Kpos,
        Dpos,
        Kori,
        Dori,
        desiredNullSpaceJointValues,
        Knull,
        Dnull,
        forceSensorName,
        forceFrameName,
        forceFilter,
        waitTimeForCalibration,
        kpf,
        minimumReactForce,
        in.getForceDeadZone(),
        in.getVelFilter(),
        maxLinearVel,
        maxAngularVel,
        ws_x,
        ws_y,
        ws_z,
        ignoreWSLimitChecks,
        adaptCoeff,
        reactThreshold,
        dragForceDeadZone,
        adaptForceCoeff,
        changePositionTolerance,
        changeTimerThreshold,
        isManipulability,
        kmani,
        positionManipulability,
        maniWeightMat
    );


    NJointPeriodicTSDMPCompliantControllerInterfacePrx dmpController
        = NJointPeriodicTSDMPCompliantControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointPeriodicTSDMPCompliantController", "dmpController", tsConfig));



    if (in.isObservedTrajectorySet())
    {
        dmpController->learnDMPFromTrajectory(in.getObservedTrajectory());
        ARMARX_IMPORTANT << "Learning dmp with trajectory";
    }
    else
    {
        std::vector<std::string> fileNames = in.getArmMotionFileList();
        ARMARX_IMPORTANT << "Learning dmp with fileNames";
        dmpController->learnDMPFromFiles(fileNames);
        ARMARX_IMPORTANT << "Learning dmp done";

    }

    std::vector<double> goals;


    if (in.isAnchorPointSet())
    {
        goals = Helpers::pose2dvec(in.getAnchorPoint()->toEigen());
    }
    else
    {
        goals = Helpers::pose2dvec(localRobot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPoseInRootFrame());
    }

    dmpController->activateController();
    dmpController->runDMP(goals, in.getSpeed());


    dmpController->setTargetForceInRootFrame(in.getTargetForce());


    StringFloatDictionary ampSetting = in.getAmpiltudeSetting();
    StringFloatDictionary speedSetting = in.getSpeedSetting();


    float minSpeed = speedSetting["MinSpeed"];
    float maxSpeed = speedSetting["MaxSpeed"];
    float defaultSpeed = speedSetting["DefaultSpeed"];
    float minAmp  = ampSetting["MinAmplitude"];
    float maxAmp = ampSetting["MaxAmplitude"];
    float defaultAmp = ampSetting["DefaultAmplitude"];


    if (maxSpeed > 3)
    {
        maxSpeed = 3;
    }

    if (maxAmp > 3)
    {
        maxAmp = 3;
    }


    RemoteGui::detail::VBoxLayoutBuilder rootLayoutBuilder = RemoteGui::makeVBoxLayout();
    rootLayoutBuilder
    .addChild(RemoteGui::makeHBoxLayout()
              .addChild(RemoteGui::makeTextLabel("Speed: "))
              .addChild(RemoteGui::makeTextLabel(std::to_string(minSpeed)))
              .addChild(RemoteGui::makeFloatSlider("Speed").min(minSpeed).max(maxSpeed).value(defaultSpeed))
              .addChild(RemoteGui::makeTextLabel(std::to_string(maxSpeed))))
    .addChild(RemoteGui::makeHBoxLayout()
              .addChild(RemoteGui::makeTextLabel("Amplitude"))
              .addChild(RemoteGui::makeTextLabel(std::to_string(minAmp)))
              .addChild(RemoteGui::makeFloatSlider("Amplitude").min(minAmp).max(maxAmp).value(defaultAmp))
              .addChild(RemoteGui::makeTextLabel(std::to_string(maxAmp))));

    getRemoteGui()->createTab("PeriodicCompliantDMP", rootLayoutBuilder);
    RemoteGui::TabProxy guiTab = RemoteGui::TabProxy(getRemoteGui(), "PeriodicCompliantDMP");


    IceUtil::Time sliderReactionStart = TimeUtil::GetTime();
    IceUtil::Time startTime = TimeUtil::GetTime();

    getDebugDrawerTopic()->clearLayer("trajLayer");
    int num = 1;

    //
    viz::Layer layer = arviz.layer("manipulability");
    VirtualRobot::SingleRobotNodeSetManipulabilityPtr mani(new VirtualRobot::SingleRobotNodeSetManipulability(nodeSet, nodeSet->getTCP(), VirtualRobot::AbstractManipulability::Mode::Position, VirtualRobot::AbstractManipulability::Type::Velocity, localRobot->getRootNode()));

    while (!isRunningTaskStopped() && !dmpController->isFinished())
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        // force reading
        IceUtil::Time now = TimeUtil::GetTime();
        int runTime = (now - startTime).toMilliSeconds();
        if (stopAfter && runTime >= stopAfterMS)
        {
            break;
        }

        float sliderTD = (now - sliderReactionStart).toSecondsDouble();
        if (sliderTD > 2)
        {
            guiTab.receiveUpdates();
            float speed = guiTab.getValue<float>("Speed").get();
            dmpController->setSpeed(speed);
            float amplitude = guiTab.getValue<float>("Amplitude").get();
            dmpController->setAmplitude(amplitude);
            sliderReactionStart = TimeUtil::GetTime();

        }

        // debug drawer (comment out during the real demo)
        if (in.getDrawDebug())
        {
            Eigen::Vector3f globalPosition = localRobot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getGlobalPosition();
            std::string trajPointName = "traj_" + std::to_string(num);
            getDebugDrawerTopic()->setSphereVisu("trajLayer", trajPointName, new Vector3(globalPosition), DrawColor {0, 1, 0, 1}, 5);
            num++;
            if (num > 5000)
            {
                num = 1;
            }
        }


        auto recognizedTextResponse = getSpeechObserver()->getDatafieldByName("SpeechToText", "RecognizedText");
        long recognizedTimestamp = recognizedTextResponse->getTimestamp();
        std::string response = recognizedTextResponse->getString();
        if (Contains(response, "thank", true) && (TimeUtil::GetTime() - IceUtil::Time::microSeconds(recognizedTimestamp)).toSecondsDouble() < 5)
        {
            break;
        }


        Eigen::Matrix4f robotGlobalPose = localRobot->getGlobalPose();
        //deal with arviz manipulability ...
        {
            Eigen::Quaternionf orientation;
            Eigen::Vector3d scale;
            mani->getEllipsoidOrientationAndScale(orientation, scale);
            Eigen::Matrix4f maniPose = VirtualRobot::MathTools::quat2eigen4f(orientation.x(), orientation.y(), orientation.z(), orientation.w());
            maniPose.block<3, 1>(0, 3) = localRobot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPositionInRootFrame();
            Eigen::Matrix4f maniGlobalPose = robotGlobalPose * maniPose;
            layer.add(viz::Ellipsoid{"current ellipsoid"}
                      .pose(maniGlobalPose)
                      .color(viz::Color::fromRGBA(0, 0, 255, 100))
                      .axisLengths(1000 * scale.cast<float>()));

            ARMARX_INFO << VAROUT(mani->computeManipulability());
        }

        if (in.isPositionManipulabilitySet())
        {
            //deal with arviz manipulability ...
            {
                Eigen::Quaternionf orientation;
                Eigen::Vector3d scale;
                mani->getEllipsoidOrientationAndScale(desiredMani, orientation, scale);
                Eigen::Matrix4f maniPose = VirtualRobot::MathTools::quat2eigen4f(orientation.x(), orientation.y(), orientation.z(), orientation.w());
                maniPose.block<3, 1>(0, 3) = localRobot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPositionInRootFrame();
                Eigen::Matrix4f desiredManiGlobalPose = robotGlobalPose * maniPose;
                layer.add(viz::Ellipsoid{"desired ellipsoid"}
                          .pose(desiredManiGlobalPose)
                          .color(viz::Color::fromRGBA(255, 0, 0, 100))
                          .axisLengths(1000 * scale.cast<float>()));
            }

        }

        arviz.commit(layer);

        usleep(100000);
    }

    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
    Eigen::Matrix4f targetPose =  localRobot->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPoseInRootFrame();

    targetPose(2, 3) += 30;
    out.setTargetPose(new Pose(targetPose));

    dmpController->deactivateController();
    while (dmpController->isControllerActive())
    {
        usleep(10000);
    }
    dmpController->deleteController();
    arviz.commit(arviz.layer("manipulability"));
    emitSuccess();
}

//void PeriodicTSCompliantDMP::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void PeriodicTSCompliantDMP::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr PeriodicTSCompliantDMP::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new PeriodicTSCompliantDMP(stateData));
}
