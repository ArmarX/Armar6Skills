/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DHParameterOptimizationGroup
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RunDataCollector.h"
#include <ArmarXSimulation/interface/simulator/SimulatorInterface.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6DHParameterOptimizationGroup;

// DO NOT EDIT NEXT LINE
RunDataCollector::SubClassRegistry RunDataCollector::Registry(RunDataCollector::GetName(), &RunDataCollector::CreateInstance);



void RunDataCollector::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    DHParameterOptimizationLoggerInterfacePrx logger = getDhParameterOptimizationLogger();
    logger->init(in.getKinematicChainName(),
                 in.getNeckMarkerMapping(),
                 in.getHandMarkerMapping(),
                 in.getLoggingFileName(),
                 false);

    // Trying to access simulator if it is running

    //    Ice::ObjectPrx base = getDhParameterOptimizationLogger()->ice_getCommunicator()->stringToProxy("Simulator");
    //    SimulatorInterfacePrx simulatorPrx = SimulatorInterfacePrx::checkedCast(base);
    //    if (simulatorPrx)
    //    {
    //        ARMARX_INFO << "Is in simulation: viconLogging deactivated";
    //        logger->stopViconLogging();
    //    }
}

//void RunDataCollector::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void RunDataCollector::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void RunDataCollector::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr RunDataCollector::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new RunDataCollector(stateData));
}
