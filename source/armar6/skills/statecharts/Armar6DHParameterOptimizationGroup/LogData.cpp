/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DHParameterOptimizationGroup
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LogData.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6DHParameterOptimizationGroup;

// DO NOT EDIT NEXT LINE
LogData::SubClassRegistry LogData::Registry(LogData::GetName(), &LogData::CreateInstance);



void LogData::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void LogData::run()
{
    DHParameterOptimizationLoggerInterfacePrx logger = getDhParameterOptimizationLogger();
    IceUtil::Time startTime = TimeUtil::GetTime();
    Ice::AsyncResultPtr resultPtr;
    if (in.getLogRepeatAccuracy())
    {
        resultPtr = logger->begin_logDataWithRepeatAccuracy();
    }
    else
    {
        resultPtr = logger->begin_logData();
    }


    int elapsedMS = 0;
    while (!isRunningTaskStopped() && elapsedMS <= in.getTimeoutMS() && !resultPtr->isCompleted())
    {
        TimeUtil::MSSleep(10);
        elapsedMS = (TimeUtil::GetTime() - startTime).toMilliSeconds();
    }

    if (resultPtr->isCompleted())
    {
        emitLoggingDone();
        return;
    }
    else if (elapsedMS > in.getTimeoutMS())
    {
        emitTimeout();
        return;
    }
    emitFailure();
}

//void LogData::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void LogData::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr LogData::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new LogData(stateData));
}
