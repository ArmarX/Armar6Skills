/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6DHParameterOptimizationGroup::Armar6DHParameterOptimizationGroupRemoteStateOfferer
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6DHParameterOptimizationGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6DHParameterOptimizationGroup;

// DO NOT EDIT NEXT LINE
Armar6DHParameterOptimizationGroupRemoteStateOfferer::SubClassRegistry Armar6DHParameterOptimizationGroupRemoteStateOfferer::Registry(Armar6DHParameterOptimizationGroupRemoteStateOfferer::GetName(), &Armar6DHParameterOptimizationGroupRemoteStateOfferer::CreateInstance);



Armar6DHParameterOptimizationGroupRemoteStateOfferer::Armar6DHParameterOptimizationGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6DHParameterOptimizationGroupStatechartContext > (reader)
{
}

void Armar6DHParameterOptimizationGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6DHParameterOptimizationGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6DHParameterOptimizationGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6DHParameterOptimizationGroupRemoteStateOfferer::GetName()
{
    return "Armar6DHParameterOptimizationGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6DHParameterOptimizationGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6DHParameterOptimizationGroupRemoteStateOfferer(reader));
}
