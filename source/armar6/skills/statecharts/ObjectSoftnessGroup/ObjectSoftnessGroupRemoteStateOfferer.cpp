/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ObjectSoftnessGroup::ObjectSoftnessGroupRemoteStateOfferer
 * @author     JuliaStarke ( julia dot starke at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ObjectSoftnessGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace ObjectSoftnessGroup;

// DO NOT EDIT NEXT LINE
ObjectSoftnessGroupRemoteStateOfferer::SubClassRegistry ObjectSoftnessGroupRemoteStateOfferer::Registry(ObjectSoftnessGroupRemoteStateOfferer::GetName(), &ObjectSoftnessGroupRemoteStateOfferer::CreateInstance);



ObjectSoftnessGroupRemoteStateOfferer::ObjectSoftnessGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < ObjectSoftnessGroupStatechartContext > (reader)
{
}

void ObjectSoftnessGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void ObjectSoftnessGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void ObjectSoftnessGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string ObjectSoftnessGroupRemoteStateOfferer::GetName()
{
    return "ObjectSoftnessGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr ObjectSoftnessGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new ObjectSoftnessGroupRemoteStateOfferer(reader));
}
