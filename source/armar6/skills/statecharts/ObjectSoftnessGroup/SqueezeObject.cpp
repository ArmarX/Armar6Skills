/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ObjectSoftnessGroup
 * @author     JuliaStarke ( julia dot starke at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SqueezeObject.h"

#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <armar6/skills/statecharts/ObjectSoftnessGroup/ObjectSoftnessGroupStatechartContext.generated.h>

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace ObjectSoftnessGroup;

// DO NOT EDIT NEXT LINE
SqueezeObject::SubClassRegistry SqueezeObject::Registry(SqueezeObject::GetName(), &SqueezeObject::CreateInstance);



void SqueezeObject::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void SqueezeObject::run()
{

    std::string controllerName = in.getNJointControllerName();
    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx controller = StateBase::getContext<ObjectSoftnessGroupStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>(controllerName);
    if (!controller->isControllerActive())
    {
        controller->activateController();
    }

    controller->setTargetsWithPwm(1, 1, in.getGraspPwmFingers(), in.getGraspPwmThumb());

    IceUtil::Time start = TimeUtil::GetTime();
    while (!isRunningTaskStopped())
    {
        IceUtil::Time now = TimeUtil::GetTime();
        float t = (now - start).toSecondsDouble();

        usleep(10000); // 10ms

        if (t > 2)
        {
            break;
        }
    }

    auto initialValues = controller->getJointValues();
    controller->setTargetsWithPwm(1, 1, in.getSqueezePwmFingers(), in.getSqueezePwmThumb());

    ARMARX_IMPORTANT << "intial hand values: " << initialValues.fingersJointValue << ", " << initialValues.thumbJointValue;


    start = TimeUtil::GetTime();
    while (!isRunningTaskStopped())
    {
        IceUtil::Time now = TimeUtil::GetTime();
        float t = (now - start).toSecondsDouble();

        usleep(10000); // 10ms

        if (t > 2)
        {
            break;
        }
    }

    auto finalValues = controller->getJointValues();

    ARMARX_IMPORTANT << "final hand values: " << finalValues.fingersJointValue << ", " << finalValues.thumbJointValue;

}

//void SqueezeObject::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void SqueezeObject::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SqueezeObject::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SqueezeObject(stateData));
}
