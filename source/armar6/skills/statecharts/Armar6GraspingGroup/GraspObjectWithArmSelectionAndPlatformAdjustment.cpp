/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GraspingGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <thread>
#include "GraspObjectWithArmSelectionAndPlatformAdjustment.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

//#include <MemoryX/interface/observers/ObjectMemoryObserverInterface.h>
#include <RobotAPI/interface/components/ViewSelectionInterface.h>

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>

using namespace armarx;
using namespace Armar6GraspingGroup;

// DO NOT EDIT NEXT LINE
GraspObjectWithArmSelectionAndPlatformAdjustment::SubClassRegistry GraspObjectWithArmSelectionAndPlatformAdjustment::Registry(GraspObjectWithArmSelectionAndPlatformAdjustment::GetName(), &GraspObjectWithArmSelectionAndPlatformAdjustment::CreateInstance);



void GraspObjectWithArmSelectionAndPlatformAdjustment::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)

}

void GraspObjectWithArmSelectionAndPlatformAdjustment::run()
{
    NJointCartesianVelocityControllerWithRampConfigPtr configRight = new NJointCartesianVelocityControllerWithRampConfig(in.getRightNodeSetName(), "", CartesianSelectionMode::eAll, 500, 1, 2, 0, 2);
    NJointControllerInterfacePrx ctrlRight = getRobotUnit()->createNJointController("NJointCartesianVelocityControllerWithRamp", in.getRightCartesianVelocityControllerName(), configRight);

    NJointCartesianVelocityControllerWithRampConfigPtr configLeft = new NJointCartesianVelocityControllerWithRampConfig(in.getLeftNodeSetName(), "", CartesianSelectionMode::eAll, 500, 1, 2, 0, 2);
    NJointControllerInterfacePrx ctrlLeft = getRobotUnit()->createNJointController("NJointCartesianVelocityControllerWithRamp", in.getLeftCartesianVelocityControllerName(), configLeft);

    //    {
    //        devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx rightHandController =
    //            devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getRightHandControllerName()));
    //        ARMARX_CHECK_EXPRESSION(rightHandController);
    //        rightHandController->activateController();
    //        rightHandController->setTargetsWithPwm(0, 0, 1, 1);
    //    }
    //    {
    //        devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx leftHandController =
    //            devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getLeftHandControllerName()));
    //        ARMARX_CHECK_EXPRESSION(leftHandController);
    //        leftHandController->activateController();
    //        leftHandController->setTargetsWithPwm(0, 0, 1, 1);
    //    }

    while (!isRunningTaskStopped())
    {
        std::this_thread::sleep_for(std::chrono::milliseconds {1});
    }

    ctrlRight->deactivateController();
    ctrlLeft->deactivateController();

    while (ctrlRight->isControllerActive() || ctrlLeft->isControllerActive())
    {
        std::this_thread::sleep_for(std::chrono::milliseconds {1});
    }
    ctrlRight->deleteController();
    ctrlLeft->deleteController();
}

//void GraspObjectWithArmSelectionAndPlatformAdjustment::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GraspObjectWithArmSelectionAndPlatformAdjustment::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GraspObjectWithArmSelectionAndPlatformAdjustment::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GraspObjectWithArmSelectionAndPlatformAdjustment(stateData));
}

void GraspObjectWithArmSelectionAndPlatformAdjustment::transitionSuccessFromMovePlatformAndSelectArm(Armar6CleanUpTaskGroup::SetArmParametersIn& next, const Armar6CleanUpTaskGroup::MovePlatformAndSelectArmOut& prev)
{

    memoryx::ObjectMemoryObserverInterfacePrx objectMemoryObserver = getObjectMemoryObserver();
    ARMARX_INFO << "Hand memory names:" << in.getLeftHandMemoryName() << " " << in.getRightHandMemoryName();
    int priority = armarx::DEFAULT_VIEWTARGET_PRIORITY;

    int cycleTime = static_cast<int>(in.getCycleTime() / 1000.0);
    // todo test if object exists
    ChannelRefBasePtr leftHandChannelRef = objectMemoryObserver->requestObjectClassRepeated(in.getLeftHandMemoryName(), cycleTime, priority);
    if (leftHandChannelRef)
    {
        local.setLeftHandChannel(leftHandChannelRef);
        next.setLeftHandChannel(leftHandChannelRef);
    }
    else
    {
        ARMARX_ERROR << "Failed to get hand channel ref";
    }



    // todo test if object exists
    ChannelRefBasePtr rightHandChannelRef = objectMemoryObserver->requestObjectClassRepeated(in.getRightHandMemoryName(), cycleTime, priority);
    if (rightHandChannelRef)
    {
        local.setRightHandChannel(rightHandChannelRef);
        next.setRightHandChannel(rightHandChannelRef);
    }
    else
    {
        ARMARX_ERROR << "Failed to get hand channel ref";
    }
    IceUtil::Time start = TimeUtil::GetTime();
    ChannelRefBasePtr leftHandInstanceChannelRef, rightHandInstanceChannelRef;
    do
    {
        leftHandInstanceChannelRef = objectMemoryObserver->getFirstObjectInstance(leftHandChannelRef);
        rightHandInstanceChannelRef = objectMemoryObserver->getFirstObjectInstance(rightHandChannelRef);
        if ((TimeUtil::GetTime() - start).toSecondsDouble() > 10)
        {
            ARMARX_ERROR << "Failed to get hand channel ref";
            return;
        }
    }
    while (!leftHandInstanceChannelRef || !rightHandInstanceChannelRef);

    local.setRightHandChannel(rightHandInstanceChannelRef);
    next.setRightHandChannel(rightHandInstanceChannelRef);

    local.setLeftHandChannel(leftHandInstanceChannelRef);
    next.setLeftHandChannel(leftHandInstanceChannelRef);



}
