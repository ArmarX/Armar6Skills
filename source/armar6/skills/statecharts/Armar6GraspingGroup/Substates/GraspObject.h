/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <armar6/skills/statecharts/Armar6GraspingGroup/GraspObject.generated.h>

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>

#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>


namespace armarx::Armar6GraspingGroup
{
    struct HandShape
    {
        float fingers = 0;
        float thumb   = 0;
        float fingersRelativeMaxPwm = 1;
        float thumbRelativeMaxPwm   = 1;
    };


    class GraspObject :
        public GraspObjectGeneratedBase < GraspObject >
    {
    public:
        GraspObject(const XMLStateConstructorParams& stateData):
            XMLStateTemplate < GraspObject > (stateData), GraspObjectGeneratedBase < GraspObject > (stateData)
        {
        }

        // inherited from StateBase
        void onEnter() override {}
        void run() override;
        void onExit() override {}

        // static functions for AbstractFactory Method
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static SubClassRegistry Registry;

        // DO NOT INSERT ANY CLASS MEMBERS,
        // use stateparameters instead,
        // if classmember are neccessary nonetheless, reset them in onEnter
    private:
        void testGraspedAndEmitGraspedAndAttachObject(const devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx& handCtrl);

        //grasping strategies
        void graspTopForceContact(
            const VirtualRobot::RobotPtr& localRobot,
            const devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx& handCtrl,
            const NJointCartesianVelocityControllerWithRampInterfacePrx& tcpCtrl,
            const HandShape& preshape = {});
        void graspObjectDirectionForceContact(
            const VirtualRobot::RobotPtr& localRobot,
            const devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx& handCtrl,
            const NJointCartesianVelocityControllerWithRampInterfacePrx& tcpCtrl,
            const HandShape& preshape = {});

        void graspForceContactOrMinDistance(
            const VirtualRobot::RobotPtr& localRobot,
            const devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx& handCtrl,
            const NJointCartesianVelocityControllerWithRampInterfacePrx& tcpCtrl,
            const Eigen::Vector3f& approachVector,
            float minDistance,
            const HandShape& preshape = {});
        void graspForceContactOrGraspPose(
            const VirtualRobot::RobotPtr& localRobot,
            const devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx& handCtrl,
            const NJointCartesianVelocityControllerWithRampInterfacePrx& tcpCtrl,
            const Eigen::Matrix4f&  graspPose,
            const HandShape& preshape = {});

        void graspForceContact(
            const VirtualRobot::RobotPtr& localRobot,
            const devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx& handCtrl,
            const NJointCartesianVelocityControllerWithRampInterfacePrx& tcpCtrl,
            const Eigen::Vector3f& approachVector,
            const HandShape& preshape = {})
        {
            graspForceContactOrMinDistance(localRobot, handCtrl, tcpCtrl, approachVector, -1, preshape);
        }


    };
}
