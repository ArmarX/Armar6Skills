/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspObject.h"

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityController.h>

#include <ArmarXCore/core/time/TimeUtil.h>

#include <RobotAPI/libraries/core/CartesianPositionController.h>
#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>

#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <MemoryX/libraries/motionmodels/MotionModelAttachedToOtherObject.h>

#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <armar6/skills/statecharts/Armar6GraspingGroup/Armar6GraspingGroupStatechartContext.generated.h>

#include <RobotAPI/libraries/core/math/MathUtils.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6GraspingGroup;

// DO NOT EDIT NEXT LINE
GraspObject::SubClassRegistry GraspObject::Registry(GraspObject::GetName(), &GraspObject::CreateInstance);


void GraspObject::run()
{
    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx handController =
        devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getHandControllerName()));
    ARMARX_CHECK_EXPRESSION(handController);
    handController->activateController();

    NJointCartesianVelocityControllerWithRampInterfacePrx tcpController =
        NJointCartesianVelocityControllerWithRampInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getCartesianVelocityControllerName()));
    ARMARX_CHECK_EXPRESSION(tcpController);
    tcpController->activateController();

    //memory segments / structures
    ARMARX_CHECK_NOT_NULL(getWorkingMemory());
    memoryx::ObjectInstanceMemorySegmentBasePrx objInstanceSeg = getWorkingMemory()->getObjectInstancesSegment();
    memoryx::PersistentObjectClassSegmentBasePrx objClassesSeg = getPriorKnowledge()->getObjectClassesSegment();
    memoryx::CommonStorageInterfacePrx commonStorage = getPriorKnowledge()->getCommonStorage();
    memoryx::GridFileManagerPtr fileManager(new memoryx::GridFileManager(commonStorage));

    ARMARX_CHECK_NOT_NULL(objInstanceSeg);
    ARMARX_CHECK_NOT_NULL(objClassesSeg);
    ARMARX_CHECK_NOT_NULL(commonStorage);
    ARMARX_CHECK_NOT_NULL(fileManager);

    //instance
    memoryx::ObjectInstancePtr objectInstance =  memoryx::ObjectInstancePtr::dynamicCast(objInstanceSeg->getObjectInstanceById(in.getObjectInstanceId()));
    ARMARX_IMPORTANT << "Object instance id '" << in.getObjectInstanceId() << "' = (@" << objectInstance.get() << ")\n" << objectInstance;
    ARMARX_CHECK_NOT_NULL(objectInstance);

    const std::string className = objectInstance->getMostProbableClass();
    memoryx::ObjectClassPtr objectClass = memoryx::ObjectClassPtr::dynamicCast(objClassesSeg->getEntityByName(className));
    ARMARX_IMPORTANT << "Object instance class '" << className << "' = (@" << objectClass.get() << ")";
    ARMARX_CHECK_NOT_NULL(objectClass);



    ///TODO use object class / is child of to determine grasping class
    //special cases with max priority
    if (className == "cleaning_cloth")
    {
        //preshape
        HandShape preshape;
        preshape.fingers = 0.15;
        preshape.thumb = 0;
        graspTopForceContact(localRobot, handController, tcpController, preshape);
    }
    //general case
    else if (in.isGraspPoseSet())
    {
        const Eigen::Matrix4f graspPose = in.getGraspPose()->toEigen();
        ARMARX_IMPORTANT << "GraspPose was Set\n" << graspPose;
        graspForceContactOrGraspPose(localRobot, handController, tcpController, graspPose);
    }
    //special cases with min priority
    else if (className == "sponge")
    {
        graspTopForceContact(localRobot, handController, tcpController);
    }
    else
    {
        ARMARX_ERROR << "I can only grasp sponge. sorry. Object was: '" << className << "'";
    }
}

void GraspObject::graspForceContactOrGraspPose(
    const VirtualRobot::RobotPtr& localRobot,
    const devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx& handCtrl,
    const NJointCartesianVelocityControllerWithRampInterfacePrx& tcpCtrl,
    const Eigen::Matrix4f&  targetPose,
    const HandShape& preshape)
{
    ARMARX_DEBUG << "preshape";
    handCtrl->setTargetsWithPwm(preshape.fingers, preshape.thumb, preshape.fingersRelativeMaxPwm, preshape.thumbRelativeMaxPwm);
    {
        IceUtil::Time start = TimeUtil::GetTime();
        const float minExecutionTime = in.getMinExecutionTime();
        //force
        const float forceThreshold = in.getForceThreshold();
        DatafieldRefPtr forceDf = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield(in.getFTDatafieldName()));
        const Eigen::Vector3f initialForce = forceDf->getDataField()->get<FramedDirection>()->toEigen();
        //robot sync
        VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet(in.getNodeSetName());
        VirtualRobot::RobotNodePtr tcp = rns->getTCP();
        //ctrl
        CartesianPositionController posController(tcp);
        posController.maxPosVel = in.getMaxPositionVelocity();

        while (!isRunningTaskStopped()) // stop run function if returning true
        {
            RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
            const float posDelta = (targetPose.block<3, 1>(0, 3) - tcp->getPoseInRootFrame().block<3, 1>(0, 3)).norm();


            IceUtil::Time now = TimeUtil::GetTime();
            float t = (now - start).toSecondsDouble();

            Eigen::Vector3f force = forceDf->getDataField()->get<FramedDirection>()->toEigen();
            force -= initialForce;

            getDebugObserver()->setDebugDatafield("GraspObject", "force_x", new Variant(force(0)));
            getDebugObserver()->setDebugDatafield("GraspObject", "force_y", new Variant(force(1)));
            getDebugObserver()->setDebugDatafield("GraspObject", "force_z", new Variant(force(2)));
            getDebugObserver()->setDebugDatafield("GraspObject", "force", new Variant(force.norm()));
            getDebugObserver()->setDebugDatafield("GraspObject", "posDelta", new Variant(posDelta));

            ARMARX_INFO << deactivateSpam(1) << VAROUT(posDelta) << " " << VAROUT(force.norm()) << " " << VAROUT(forceThreshold);
            const bool forceThreshReached = (force.norm() > forceThreshold);

            if ((t > minExecutionTime && forceThreshReached) || posDelta < 2.5)
            {
                //                if (forceThreshReached)
                //                {
                //                    tcpCtrl->setTargetVelocity(0, 0, 5, 0, 0, 0);
                //                    sleep(4);
                //                }
                ARMARX_INFO << deactivateSpam(1) << "final: " << VAROUT(posDelta) << " " << VAROUT(force.norm()) << " " << VAROUT(forceThreshold);
                ///TODO check if adjust
                if (true) //done
                {
                    tcpCtrl->setTargetVelocity(0, 0, 0, 0, 0, 0);
                    ARMARX_IMPORTANT << "close";
                    handCtrl->setTargetsWithPwm(2, 2, 1, 1);
                    testGraspedAndEmitGraspedAndAttachObject(handCtrl);
                    break;
                }
                ///TODO adjust
            }

            Eigen::VectorXf cartesianVelocity = posController.calculate(targetPose, VirtualRobot::IKSolver::CartesianSelection::All);
            tcpCtrl->setTargetVelocity(
                cartesianVelocity(0), cartesianVelocity(1), cartesianVelocity(2),
                cartesianVelocity(3), cartesianVelocity(4), cartesianVelocity(5)
            );
            usleep(10000);
        }
    }
}

void GraspObject::graspForceContactOrMinDistance(
    const VirtualRobot::RobotPtr& localRobot,
    const devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx& handCtrl,
    const NJointCartesianVelocityControllerWithRampInterfacePrx& tcpCtrl,
    const Eigen::Vector3f& approachVector,
    float minDistance,
    const HandShape& preshape)
{
    ARMARX_DEBUG << "preshape";
    handCtrl->setTargetsWithPwm(preshape.fingers, preshape.thumb, preshape.fingersRelativeMaxPwm, preshape.thumbRelativeMaxPwm);
    ARMARX_DEBUG << "approach vector " << approachVector;

    {
        IceUtil::Time start = TimeUtil::GetTime();
        const float minExecutionTime = in.getMinExecutionTime();
        //force
        const float forceThreshold = in.getForceThreshold();
        DatafieldRefPtr forceDf = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield(in.getFTDatafieldName()));
        const Eigen::Vector3f initialForce = forceDf->getDataField()->get<FramedDirection>()->toEigen();
        //robot sync
        VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet(in.getNodeSetName());
        VirtualRobot::RobotNodePtr tcp = rns->getTCP();
        //target
        Eigen::Matrix4f targetPose = tcp->getPoseInRootFrame();
        ARMARX_CHECK_GREATER(in.getRelativeTargetDistanceInApproachDirection(), 0);
        targetPose.block<3, 1>(0, 3) += approachVector.normalized() * in.getRelativeTargetDistanceInApproachDirection();
        //ctrl
        CartesianPositionController posController(tcp);
        posController.maxPosVel = in.getMaxPositionVelocity();

        while (!isRunningTaskStopped()) // stop run function if returning true
        {
            bool minDistReached = false;
            if (minDistance > 0)
            {
                RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
                Eigen::Vector3f approachVector = in.getObjectPose()->toEigen().block<3, 1>(0, 3) - tcp->getPoseInRootFrame().block<3, 1>(0, 3);
                minDistReached = approachVector.norm() < minDistance;
            }

            IceUtil::Time now = TimeUtil::GetTime();
            float t = (now - start).toSecondsDouble();

            Eigen::Vector3f force = forceDf->getDataField()->get<FramedDirection>()->toEigen();
            force -= initialForce;

            getDebugObserver()->setDebugDatafield("GraspObject", "force_x", new Variant(force(0)));
            getDebugObserver()->setDebugDatafield("GraspObject", "force_y", new Variant(force(1)));
            getDebugObserver()->setDebugDatafield("GraspObject", "force_z", new Variant(force(2)));

            const bool forceThreshReached = (force.norm() > forceThreshold);

            if ((t > minExecutionTime && forceThreshReached) || minDistReached)
            {
                ///TODO check if adjust
                if (true) //done
                {
                    if (forceThreshReached)
                    {
                        tcpCtrl->setTargetVelocity(0, 0, 5, 0, 0, 0);
                        sleep(4);
                    }

                    if (minDistReached)
                    {
                        ARMARX_IMPORTANT << "minimal distance reached";
                    }
                    else
                    {
                        ARMARX_IMPORTANT << "force threshold reached";
                    }
                    tcpCtrl->setTargetVelocity(0, 0, 0, 0, 0, 0);
                    ARMARX_IMPORTANT << "close";
                    handCtrl->setTargetsWithPwm(2, 2, 1, 1);
                    testGraspedAndEmitGraspedAndAttachObject(handCtrl);
                    break;
                }
                ///TODO adjust

            }


            Eigen::VectorXf cartesianVelocity = posController.calculate(targetPose, VirtualRobot::IKSolver::CartesianSelection::All);
            tcpCtrl->setTargetVelocity(
                cartesianVelocity(0), cartesianVelocity(1), cartesianVelocity(2),
                cartesianVelocity(3), cartesianVelocity(4), cartesianVelocity(5)
            );
            usleep(10000);
        }
    }

}

void GraspObject::graspObjectDirectionForceContact(
    const VirtualRobot::RobotPtr& localRobot,
    const devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx& handCtrl,
    const NJointCartesianVelocityControllerWithRampInterfacePrx& tcpCtrl,
    const HandShape& preshape)
{
    ARMARX_DEBUG << "calculating approach vector";

    VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet(in.getNodeSetName());
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();
    Eigen::Matrix4f targetPose = tcp->getPoseInRootFrame();
    Eigen::Vector3f approachVector = in.getObjectPose()->toEigen().block<3, 1>(0, 3) - targetPose.block<3, 1>(0, 3);

    graspForceContact(localRobot, handCtrl, tcpCtrl, approachVector, preshape);
}

void GraspObject::graspTopForceContact(const VirtualRobot::RobotPtr& localRobot,
                                       const devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx& handCtrl,
                                       const NJointCartesianVelocityControllerWithRampInterfacePrx& tcpCtrl,
                                       const HandShape& preshape)
{
    graspForceContact(localRobot, handCtrl, tcpCtrl, {0, 0, -1}, preshape);
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GraspObject::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GraspObject(stateData));
}

void GraspObject::testGraspedAndEmitGraspedAndAttachObject(const devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx& handCtrl)
{
    //check if object is in hand
    const auto now = []
    {
        return std::chrono::duration_cast<std::chrono::nanoseconds>(
            std::chrono::high_resolution_clock::now().time_since_epoch()
        ).count() * 1e-9f;
    };
    const auto isClosed = [&]
    {
        const auto vals = handCtrl->getJointValues();
        return vals.thumbJointValue > in.getCloseThresholdThumb() &&
        vals.fingersJointValue > in.getCloseThresholdFingers();
    };
    const auto start = now();
    while (in.getCloseSeconds() > now() - start && !isClosed())
    {
        std::this_thread::sleep_for(std::chrono::milliseconds{10});
    }
    if (isClosed())
    {
        emitFailure();
        return;
    }
    memoryx::MotionModelAttachedToOtherObjectPtr newMotionModel = new memoryx::MotionModelAttachedToOtherObject(getRobotStateComponent(), in.getHandChannel());
    getWorkingMemory()->getObjectInstancesSegment()->setNewMotionModel(in.getObjectInstanceId(), newMotionModel);
    emitGrasped();
}
