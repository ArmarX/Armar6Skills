/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BezierMotion.h"

#include <VirtualRobot/math/Helpers.h>
#include <VirtualRobot/math/Line.h>
#include <VirtualRobot/math/LinearContinuedBezier.h>

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <Eigen/Eigen>

#include <RobotAPI/libraries/core/EigenHelpers.h>

#include <RobotAPI/libraries/core/CartesianPositionController.h>
#include <RobotAPI/libraries/core/CartesianVelocityController.h>
#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>
#include <RobotAPI/libraries/core/math/MathUtils.h>


using namespace armarx;
using namespace Armar6GraspingGroup;
using namespace ::math;
using namespace armarx::math;

// DO NOT EDIT NEXT LINE
BezierMotion::SubClassRegistry BezierMotion::Registry(BezierMotion::GetName(), &BezierMotion::CreateInstance);



void BezierMotion::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void BezierMotion::run()
{
    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(in.getNodeSetName());
    ARMARX_CHECK_EXPRESSION(rns);
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();
    ARMARX_CHECK_EXPRESSION(tcp);


    Eigen::Matrix4f startPose = in.isStartPoseSet() ? in.getStartPose()->toEigen() : tcp->getPoseInRootFrame();
    Eigen::Matrix4f targetPose = startPose;
    if (in.isTargetPoseSet())
    {
        targetPose = in.getTargetPose()->toEigen();
    }
    else if (in.isRelativeTargetPositionSet())
    {
        targetPose.block<3, 1>(0, 3) += in.getRelativeTargetPosition()->toEigen();
    }
    else
    {
        throw LocalException("Either TargetPose or RelativeTargetPosition have to be set");
    }


    Eigen::Quaternionf startQuat = Eigen::Quaternionf(startPose.block<3, 3>(0, 0));
    Eigen::Quaternionf targetQuat = Eigen::Quaternionf(targetPose.block<3, 3>(0, 0));

    Eigen::Vector3f p0 = startPose.block<3, 1>(0, 3);
    Eigen::Vector3f p1 = in.getP1()->toEigen() + startPose.block<3, 1>(0, 3);
    Eigen::Vector3f p2 = in.getP2()->toEigen() + targetPose.block<3, 1>(0, 3);
    Eigen::Vector3f p3 = targetPose.block<3, 1>(0, 3);

    float KpPosition = in.getKpPosition();
    float KpOrientation = in.getKpOrientation();

    LinearContinuedBezier bezier(p0, p1, p2, p3);

    //float jointLimitAvoidanceKp = in.getJointLimitAvoidanceKp();

    NJointCartesianVelocityControllerWithRampInterfacePrx controller =
        NJointCartesianVelocityControllerWithRampInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getCartesianVelocityControllerName()));
    ARMARX_CHECK_EXPRESSION(controller);

    controller->activateController();

    Eigen::AngleAxisf aa(targetQuat * startQuat.inverse());
    Eigen::Vector3f oriDelta = aa.axis() * MathUtils::angleModPI(aa.angle());
    ARMARX_IMPORTANT << VAROUT(oriDelta.transpose());

    float duration = in.getDuration();
    IceUtil::Time start = TimeUtil::GetTime();
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        IceUtil::Time now = TimeUtil::GetTime();

        float t = (now - start).toSecondsDouble();
        float f = t / duration;

        if (f > 1)
        {
            if (in.getStopOnFinish())
            {
                controller->setTargetVelocity(0, 0, 0, 0, 0, 0);
            }
            emitTargetReached();
            break;
        }

        Eigen::Vector3f posVelocity = bezier.GetDerivative(f) / duration;
        Eigen::Vector3f posError = bezier.Get(f) - tcp->getPositionInRootFrame();
        posVelocity += KpPosition * posError;

        Eigen::Quaternionf quat = Helpers::Lerp(startQuat, targetQuat, f);
        Eigen::AngleAxisf aaOriErr(quat * Eigen::Quaternionf(tcp->getPoseInRootFrame().block<3, 3>(0, 0)).inverse());
        Eigen::Vector3f oriError = aaOriErr.axis() * MathUtils::angleModPI(aaOriErr.angle());
        ARMARX_IMPORTANT << deactivateSpam(1) << VAROUT(oriError.transpose());

        Eigen::Vector3f oriVelocity = oriDelta / duration;
        oriVelocity += oriError * KpOrientation;

        getDebugObserver()->setDebugDatafield("BezierMotion", "err_x", new Variant(posError(0)));
        getDebugObserver()->setDebugDatafield("BezierMotion", "err_y", new Variant(posError(1)));
        getDebugObserver()->setDebugDatafield("BezierMotion", "err_z", new Variant(posError(2)));


        controller->setTargetVelocity(posVelocity(0), posVelocity(1), posVelocity(2), oriVelocity(0), oriVelocity(1), oriVelocity(2));


        usleep(10000); // 10ms

    }
}

//void BezierMotion::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void BezierMotion::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr BezierMotion::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new BezierMotion(stateData));
}
