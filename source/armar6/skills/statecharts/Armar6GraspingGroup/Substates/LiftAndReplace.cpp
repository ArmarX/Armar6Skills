/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LiftAndReplace.h"

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>

#include <RobotAPI/libraries/core/CartesianPositionController.h>

#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6GraspingGroup;

// DO NOT EDIT NEXT LINE
LiftAndReplace::SubClassRegistry LiftAndReplace::Registry(LiftAndReplace::GetName(), &LiftAndReplace::CreateInstance);


class VelocityControllerHelper
{
public:
    VelocityControllerHelper(const RobotUnitInterfacePrx& robotUnit, const std::string& nodeSetName, const std::string& controllerName)
        : robotUnit(robotUnit), controllerName(controllerName)
    {
        config = new NJointCartesianVelocityControllerWithRampConfig(nodeSetName, "", CartesianSelectionMode::eAll, 500, 1, 2, 0, 2);
        init();
    }

    void init()
    {
        NJointControllerInterfacePrx ctrl = robotUnit->getNJointController(controllerName);
        if (ctrl)
        {
            controllerCreated = false;
        }
        else
        {
            ctrl = robotUnit->createNJointController("NJointCartesianVelocityControllerWithRamp", controllerName, config);
            controllerCreated = true;
        }
        controller = NJointCartesianVelocityControllerWithRampInterfacePrx::checkedCast(ctrl);
        controller->activateController();
    }

    void cleanup()
    {
        controller->deactivateController();
        if (controllerCreated)
        {
            while (controller->isControllerActive())
            {
                TimeUtil::SleepMS(1);
            }
            controller->deleteController();
        }
    }

    NJointCartesianVelocityControllerWithRampConfigPtr config;
    NJointCartesianVelocityControllerWithRampInterfacePrx controller;
    RobotUnitInterfacePrx robotUnit;
    std::string controllerName;
    bool controllerCreated = false;
};

void LiftAndReplace::shapeHand(float fingers, float thumb)
{
    std::string handJointPrefix = in.getHandJointsPrefix().at(in.getSelectedArm());
    std::string fingersName = handJointPrefix + "Fingers";
    std::string thumbName = handJointPrefix + "Thumb";

    getKinematicUnit()->switchControlMode({{fingersName, ePositionControl}, {thumbName, ePositionControl}});
    getKinematicUnit()->setJointAngles({{fingersName, fingers}, {thumbName, thumb}});
}

void LiftAndReplace::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

Eigen::Matrix4f LiftAndReplace::mirrorPose(Eigen::Matrix4f oldPose)
{
    Eigen::Quaternionf oriOld(oldPose.block<3, 3>(0, 0));
    Eigen::Quaternionf ori;

    ori.w() = oriOld.z();
    ori.x() = oriOld.y() * -1;
    ori.y() = oriOld.x() * -1;
    ori.z() = oriOld.w();

    Eigen::Matrix4f pose = Eigen::Matrix4f::Identity();
    pose.block<3, 3>(0, 0) = (ori * Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitZ())).toRotationMatrix();
    pose(0, 3) = -oldPose(0, 3);
    pose(1, 3) = oldPose(1, 3);
    pose(2, 3) = oldPose(2, 3);

    return pose;
}

std::vector<PosePtr> LiftAndReplace::mirrorPoses(const std::vector<PosePtr>& poses)
{
    std::vector<PosePtr> result;
    for (const PosePtr& pose : poses)
    {
        result.push_back(new Pose(mirrorPose(pose->toEigen())));
    }
    return result;
}

void LiftAndReplace::run()
{
    std::string selectedArm = in.getSelectedArm();
    if (selectedArm != "Right" && selectedArm != "Left")
    {
        throw LocalException("SelectedArm has to be either 'Right' or 'Left'.");
    }


    std::string nodeSetName = in.getNodeSetName().at(selectedArm);
    Eigen::Vector3f liftVector = in.getLiftVector()->toEigen();

    std::vector<PosePtr> replaceWaypoints = in.getReplaceWaypoints().at("Right")->toStdVector<PosePtr>();
    std::vector<PosePtr> returnWaypoints = in.getReturnWaypoints().at("Right")->toStdVector<PosePtr>();
    PosePtr replaceTargetPtr = in.getReplaceTarget().at("Right");
    Eigen::Matrix4f replaceTarget = replaceTargetPtr->toEigen();
    replaceTarget.block<3, 1>(0, 3) += in.getReplaceTargetOffset()->toEigen();
    replaceTargetPtr = new Pose(replaceTarget);
    replaceWaypoints.push_back(replaceTargetPtr);
    returnWaypoints.push_back(in.getReturnTarget().at("Right"));


    if (selectedArm == "Left")
    {
        replaceWaypoints = mirrorPoses(replaceWaypoints);
        returnWaypoints = mirrorPoses(returnWaypoints);
    }


    std::vector<PosePtr> currentWaypoints;


    VelocityControllerHelper ctrlHelper(getRobotUnit(), nodeSetName, in.getCartesianVelocityControllerName());

    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(nodeSetName);
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();

    /*controller->setKpJointLimitAvoidance(0);
    controller->setJointLimitAvoidanceScale(0);*/


    CartesianPositionController posController(tcp);
    posController.maxOriVel = in.getMaxOriVel();
    posController.maxPosVel = in.getMaxPosVel();
    posController.KpOri = in.getKpOrientation();
    posController.KpPos = in.getKpPosition();

    Eigen::Matrix4f target = tcp->getPoseInRootFrame();
    target.block<3, 1>(0, 3) += liftVector;


    PhaseType currentPhase = PhaseType::Lift;
    IceUtil::Time unloadStart;
    size_t waypointIndex = 0;
    currentWaypoints = replaceWaypoints;


    RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

    if (in.getDrawDebugInfo())
    {
        auto drawWps = [&](const std::vector<PosePtr>& wps, std::string layer)
        {
            PolygonPointList points;
            DrawColor c1 {0, 0, 0, 0};
            DrawColor c2 {0, 0, 1, 1};
            for (std::size_t i = 0; i < wps.size(); ++i)
            {
                const auto& wp = wps.at(i);
                PoseBasePtr p = new Pose {Eigen::Matrix4f{robot->getGlobalPose()* wp->toEigen()}};
                getDebugDrawerTopic()->setPoseVisu(layer, std::to_string(i), p);
                points.emplace_back(p->position);
            }
            getDebugDrawerTopic()->setPolygonVisu(layer, "line", points, c1, c2, 5);
            ARMARX_INFO << "drawing waypoints to layer " << layer << " " << points;
        };
        drawWps(replaceWaypoints, "LiftAndRplace_replaceWaypoints");
        drawWps(returnWaypoints, "LiftAndRplace_returnWaypoints");
    }

    CycleUtil c(10);
    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        Eigen::VectorXf cv = posController.calculate(target, VirtualRobot::IKSolver::All);
        ctrlHelper.controller->setTargetVelocity(cv(0), cv(1), cv(2), cv(3), cv(4), cv(5));

        float positionError = posController.getPositionError(target);
        float orientationError = posController.getOrientationError(target);
        ARMARX_INFO << deactivateSpam(1) << VAROUT(positionError) << " " << VAROUT(orientationError) << " " << VAROUT(cv.transpose());

        PhaseType nextPhase = currentPhase;

        bool targetReached = positionError < in.getPositionThreshold() && orientationError < in.getOrientationThreshold();
        bool targetNear = positionError < in.getPositionNearThreshold() && orientationError < in.getOrientationNearThreshold();

        getDebugObserver()->setDebugDatafield("LiftAndReplace", "positionError", new Variant(positionError));
        getDebugObserver()->setDebugDatafield("LiftAndReplace", "orientationError", new Variant(orientationError));
        if (in.getDrawDebugInfo())
        {
            getDebugDrawerTopic()->setPoseVisu("LiftAndReplace", "target", new Pose(robot->getGlobalPose() * target));
            getDebugDrawerTopic()->setPoseVisu("LiftAndReplace", "tcp", new Pose(robot->getGlobalPose() * tcp->getPoseInRootFrame()));
        }

        bool isLastWaypoint = waypointIndex >= currentWaypoints.size() - 1;

        if (currentPhase == PhaseType::Lift)
        {
            if (targetNear)
            {
                ARMARX_IMPORTANT << "Lift completed";
                nextPhase = PhaseType::Replace;
                waypointIndex = 0;
                target = currentWaypoints.at(waypointIndex)->toEigen();
            }
        }
        if (currentPhase == PhaseType::Replace)
        {
            if ((targetNear && !isLastWaypoint) || targetReached)
            {
                ARMARX_IMPORTANT << "waypoint reached. waypointIndex=" << waypointIndex;
                waypointIndex++;
                if (waypointIndex < currentWaypoints.size())
                {
                    target = currentWaypoints.at(waypointIndex)->toEigen();
                    ARMARX_IMPORTANT << "going to next waypoint. target=\n" << target;
                }
                else
                {
                    ARMARX_IMPORTANT << "last waypoint reached. next phase=unload";
                    nextPhase = PhaseType::Unload;
                    unloadStart = TimeUtil::GetTime();
                    shapeHand(0, 0);
                }
            }
        }
        if (currentPhase == PhaseType::Unload)
        {
            if ((TimeUtil::GetTime() - unloadStart).toSecondsDouble() > in.getUnloadTime())
            {
                nextPhase = PhaseType::Return;
                waypointIndex = 0;
                currentWaypoints = returnWaypoints;
                target = currentWaypoints.at(waypointIndex)->toEigen();
                ARMARX_IMPORTANT << "unload complete. next phase=return, target=\n" << target;
            }
        }
        if (currentPhase == PhaseType::Return)
        {
            if ((targetNear && !isLastWaypoint) || targetReached)
            {
                ARMARX_IMPORTANT << "waypoint reached. waypointIndex=" << waypointIndex;
                waypointIndex++;


                if (waypointIndex < currentWaypoints.size())
                {
                    target = currentWaypoints.at(waypointIndex)->toEigen();
                }
                else
                {
                    ctrlHelper.controller->immediateHardStop();
                    emitSuccess();
                    ctrlHelper.cleanup();
                    return;
                }
            }

        }

        currentPhase = nextPhase;
        c.waitForCycleDuration();
    }

    ctrlHelper.cleanup();
}

//void LiftAndReplace::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void LiftAndReplace::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr LiftAndReplace::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new LiftAndReplace(stateData));
}
