/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GraspingGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GenerateApproachMotion.h"

#include <RobotAPI/libraries/ArmarXObjects/ice_conversions.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>

#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>

#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>

#include <VirtualRobot/Grasping/GraspSet.h>
#include <VirtualRobot/math/Helpers.h>
#include <VirtualRobot/XML/ObjectIO.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6GraspingGroup;

// DO NOT EDIT NEXT LINE
GenerateApproachMotion::SubClassRegistry GenerateApproachMotion::Registry(GenerateApproachMotion::GetName(), &GenerateApproachMotion::CreateInstance);


Eigen::Matrix3f GenerateApproachMotion::mirrorOri(Eigen::Matrix3f oldOri)
{
    Eigen::Quaternionf oriOld(oldOri);
    Eigen::Quaternionf oriQuat;

    oriQuat.w() = oriOld.z();
    oriQuat.x() = oriOld.y() * -1;
    oriQuat.y() = oriOld.x() * -1;
    oriQuat.z() = oriOld.w();

    return (oriQuat * Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitZ())).toRotationMatrix();
}

void GenerateApproachMotion::onEnter()
{
    out.setGraspPrePose(PoseBasePtr {new Pose{}});
    ARMARX_IMPORTANT << "generating approach motion";
    //robot
    VirtualRobot::RobotPtr localRobot = getLocalRobot();
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());


    //memory segments / structures
    ARMARX_CHECK_NOT_NULL(getWorkingMemory());
    memoryx::ObjectInstanceMemorySegmentBasePrx objInstanceSeg = getWorkingMemory()->getObjectInstancesSegment();
    memoryx::PersistentObjectClassSegmentBasePrx objClassesSeg = getPriorKnowledge()->getObjectClassesSegment();
    memoryx::CommonStorageInterfacePrx commonStorage = getWorkingMemory()->getCommonStorage();
    memoryx::GridFileManagerPtr fileManager(new memoryx::GridFileManager(commonStorage));

    ARMARX_CHECK_NOT_NULL(objInstanceSeg);
    ARMARX_CHECK_NOT_NULL(objClassesSeg);
    ARMARX_CHECK_NOT_NULL(commonStorage);
    ARMARX_CHECK_NOT_NULL(fileManager);

    //instance
    Eigen::Matrix4f objInRoot = Eigen::Matrix4f::Identity();
    Eigen::Matrix4f globalObj = Eigen::Matrix4f::Identity();
    std::string className;
    VirtualRobot::ManipulationObjectPtr manipulationObject;
    bool found = false;

    if (in.getObjectInstanceId().find("/") != std::string::npos)
    {
        // It's an ObjectID.
        armarx::ObjectID id(in.getObjectInstanceId());
        ARMARX_IMPORTANT << "Interpreting ObjectInstanceId '" << in.getObjectInstanceId() << "' as armarx::ObjectID " << id << "!";
        objpose::ObjectPoseSeq objectPoses = objpose::fromIce(getObjectPoseStorage()->getObjectPoses());
        for (const auto& p : objectPoses)
        {
            if (p.objectID == id)
            {
                armarx::ObjectFinder finder;
                if (std::optional<ObjectInfo> info = finder.findObject(p.objectID))
                {
                    try
                    {
                        manipulationObject = VirtualRobot::ObjectIO::loadManipulationObject(info->simoxXML().absolutePath);

                        globalObj = p.objectPoseGlobal;
                        objInRoot = p.objectPoseRobot;
                        className = p.objectID.className();

                        found = true;
                    }
                    catch (const VirtualRobot::VirtualRobotException& e)
                    {
                        ARMARX_ERROR << "Could not load simox object model from " << info->simoxXML().absolutePath << ":\n" << e.what();
                    }
                }
                break;
            }
        }
        if (!found)
        {
            ARMARX_WARNING << "Object instance ID '" << in.getObjectInstanceId() << "' looks like an armarx::ObjectID, "
                           << "but no such object was found in ObjectPoseStorage (offering " << objectPoses.size() << " objects)."
                           << "Trying to interpret it as memory instance ID next.";
        }
    }
    if (!found)
    {
        memoryx::ObjectInstancePtr objectInstance =  memoryx::ObjectInstancePtr::dynamicCast(objInstanceSeg->getObjectInstanceById(in.getObjectInstanceId()));
        ARMARX_IMPORTANT << "Object instance id '" << in.getObjectInstanceId() << "' = (@" << objectInstance.get() << ")\n" << objectInstance;
        ARMARX_CHECK_NOT_NULL(objectInstance);

        objInRoot = objectInstance->getPose()->toRootEigen(localRobot);
        globalObj = objectInstance->getPose()->toGlobalEigen(localRobot);

        //class
        const std::string className = objectInstance->getMostProbableClass();
        memoryx::ObjectClassPtr objectClass = memoryx::ObjectClassPtr::dynamicCast(objClassesSeg->getEntityByName(className));
        ARMARX_IMPORTANT << "Object instance class '" << className << "' = (@" << objectClass.get() << ")";
        ARMARX_CHECK_NOT_NULL(objectClass);

        ARMARX_IMPORTANT << "adding simox wrapper";
        memoryx::EntityWrappers::SimoxObjectWrapperPtr simoxWrapper = objectClass->addWrapper(new memoryx::EntityWrappers::SimoxObjectWrapper(fileManager));
        ARMARX_CHECK_NOT_NULL(simoxWrapper);
        ARMARX_IMPORTANT << "creating manipulation object";
        VirtualRobot::ManipulationObjectPtr manipulationObject = simoxWrapper->getManipulationObject();
    }
    ARMARX_CHECK_NOT_NULL(manipulationObject);
    VirtualRobot::GraspSetPtr graspSet = manipulationObject->getGraspSet(in.getGraspSetName());
    ARMARX_INFO << manipulationObject->getAllGraspSets();
    ARMARX_IMPORTANT << "Grasp set name '" << in.getGraspSetName() << "' = (@" << graspSet.get() << ")\n";
    ARMARX_CHECK_NOT_NULL(graspSet);


    //visu
    if (in.getDrawDebugInfo())
    {
        getDebugDrawerTopic()->setPoseVisu("GenerateAppoachMotion", "objectPose", new Pose {globalObj});
    }


    ///TODO select grasp
    //    static const std::map<std::string, std::string> classToGrasp
    //    {
    //        {"sponge", "front_top_grasp"},
    //        {"bauhaus-screwbox-small", "right_side_grasp"},
    //    };

    /*
    static const float offTopZ = 50;

    static const std::map<std::string, Eigen::Vector3f> graspToPreOffsetLeft
    {
        {"sponge", {0, 0, offTopZ}},
        {"cleaning_paste", {0, 0, offTopZ}},
        {"cleaning_cloth", {0, 0, offTopZ}},
        {"cable-ties", {0, 0, offTopZ}},
        {"bauhaus-screwbox-large", {0, 0, offTopZ}},
        {"bauhaus-screw-box-large-top", {0, 0, offTopZ}},
        {"bauhaus-screwbox-small", { -70, 0, 0}},
        {"isoalcohol", { -70, 0, 0}},
        {"spraybottle", { -70, 0, 0}},
    };

    static const std::map<std::string, Eigen::Vector3f> graspToPreOffsetRight
    {
        {"sponge", {0, 0, offTopZ}},
        {"cleaning_paste", {0, 0, offTopZ}},
        {"cleaning_cloth", {0, 0, offTopZ}},
        {"cable-ties", {0, 0, offTopZ}},
        {"bauhaus-screwbox-large", {0, 0, offTopZ}},
        {"bauhaus-screw-box-large-top", {0, 0, offTopZ}},
        {"bauhaus-screwbox-small", { 70, 0, 0}},
        {"isoalcohol", { 70, 0, 0}},
        {"spraybottle", { 70, 0, 0}},
    };*/

    const bool useRight = in.getNodeSetName() == "RightArm";
    const std::string graspName = useRight ? "Right_1_Grasp" : "Left_1_Grasp";
    std::string selectedSide = useRight ? "Right" : "Left";
    auto helper = getRobotNameHelper()->getArm(selectedSide);
    ARMARX_INFO << "Using hand: " << helper.getEndEffector();
    out.setHandName(helper.getEndEffector());
    std::map<std::string, bool> isTopGrasp = in.getIsTopGrasp();

    ARMARX_CHECK_EXPRESSION(in.getPrePoseRelativeOffsetsRight().count(className)) << className;
    Eigen::Vector3f prePoseRelativeOffset = in.getPrePoseRelativeOffsetsRight().at(className)->toEigen();

    if (!useRight)
    {
        prePoseRelativeOffset(0) = -prePoseRelativeOffset(0);
    }

    //const std::map<std::string, Eigen::Vector3f>& graspToPreOffset = useRight ? graspToPreOffsetRight : graspToPreOffsetLeft;


    const auto grasp = graspSet->getGrasp(graspName);
    ARMARX_IMPORTANT << "Grasp name '" << graspName << "' = (@" << grasp.get() << ")\n";
    ARMARX_CHECK_NOT_NULL(grasp);
    const Eigen::Matrix4f graspInObj = grasp->getTransformation().inverse();
    out.setRelativeGraspPose(graspInObj);

    Eigen::Matrix4f graspInRoot =  objInRoot * graspInObj;

    bool isTopGraspNow = isTopGrasp.count(className) && isTopGrasp.at(className);
    if (isTopGraspNow)
    {
        const Eigen::Matrix3f graspOri = graspInRoot.block<3, 3>(0, 0);
        Eigen::Matrix3f referenceOrientation = in.getReferenceOrientationTopGrasp()->toEigen();
        if (!useRight)
        {
            referenceOrientation = mirrorOri(referenceOrientation);
        }

        const Eigen::Vector3f sourceLocalUp = referenceOrientation.inverse() * Eigen::Vector3f::UnitZ();
        const Eigen::Matrix3f targetOri = ::math::Helpers::RotateOrientationToFitVector(graspOri, sourceLocalUp, Eigen::Vector3f::UnitZ());
        graspInRoot.block<3, 3>(0, 0) = targetOri;
    }




    if (className == "cleaning_paste" && useRight)
    {

        PosePtr p {new Pose};

        p->orientation->qw = -0.4721967577934265;
        p->orientation->qx = 0.3264586031436920;
        p->orientation->qy = 0.7991034388542175;
        p->orientation->qz = 0.1785744726657867f;

        graspInRoot.block<3, 3>(0, 0) = p->toEigen().block<3, 3>(0, 0);
    }



    out.setGraspPose(new Pose {graspInRoot});

    if (in.getDrawDebugInfo())
    {
        getDebugDrawerTopic()->setPoseVisu("GenerateAppoachMotion", "graspInRoot", new Pose {localRobot->getGlobalPose() * graspInRoot});
    }

    Eigen::Matrix4f targetPoseInRoot = graspInRoot;
    ARMARX_INFO << VAROUT(targetPoseInRoot) << VAROUT(objInRoot) << VAROUT(prePoseRelativeOffset);
    targetPoseInRoot.block<3, 1>(0, 3) += prePoseRelativeOffset;
    Eigen::Matrix4f pregraspPoseInObj =  objInRoot.inverse() * targetPoseInRoot;
    ARMARX_INFO << VAROUT(targetPoseInRoot) << VAROUT(pregraspPoseInObj);
    out.setRelativeGraspPrePose(pregraspPoseInObj);

    if (in.getDrawDebugInfo())
    {
        getDebugDrawerTopic()->setPoseVisu("GenerateAppoachMotion", "targetPose", new Pose {localRobot->getGlobalPose() * targetPoseInRoot});
    }


    Eigen::Matrix4f targetPoseDMP = targetPoseInRoot;

    if (isTopGrasp.count(className) && isTopGrasp.at(className))
    {
        targetPoseDMP.block<3, 1>(0, 3) += in.getApproachOffsetSide().at(selectedSide)->toEigen();
    }
    else
    {
        targetPoseDMP.block<3, 1>(0, 3) += in.getApproachOffsetTop().at(selectedSide)->toEigen();
    }

    PosePtr graspPoseDMP {new Pose{targetPoseDMP}};

    targetPoseDMP = graspPoseDMP->toEigen();
    if (in.getDrawDebugInfo())
    {
        getDebugDrawerTopic()->setPoseVisu("GenerateAppoachMotion", "targetPoseDMP", new Pose {localRobot->getGlobalPose() * targetPoseDMP});
    }


    //    if(in.isGraspPoseOffsetSet())
    //    {
    //        targetPose.block<3,1>(0,3) += in.getGraspPoseOffset()->toEigen();
    //    }


    std::string graspType = isTopGraspNow ? "Top" : "Side";
    std::map<std::string, Vector3Ptr> bezierSupportPoints = in.getBezierSupportPoint().at(graspType)->toStdMap<Vector3Ptr>();
    out.setP1(bezierSupportPoints.at("P1"));
    out.setP2(bezierSupportPoints.at("P2"));

    //visu
    if (in.getDrawDebugInfo())
    {
        VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet(in.getNodeSetName());
        VirtualRobot::RobotNodePtr tcp = rns->getTCP();

        const Eigen::Matrix4f globalStartPose = localRobot->getGlobalPose() * tcp->getPoseInRootFrame();
        Eigen::Matrix4f globalP1 = globalStartPose;
        globalP1.block<3, 1>(0, 3) += out.getP1()->toEigen();

        Eigen::Matrix4f globalP2 = localRobot->getGlobalPose() * targetPoseInRoot;
        globalP2.block<3, 1>(0, 3) += out.getP2()->toEigen();

        getDebugDrawerTopic()->setPoseVisu("GenerateAppoachMotion", "startPose", new Pose {globalStartPose});
        getDebugDrawerTopic()->setPoseVisu("GenerateAppoachMotion", "P1", new Pose {globalP1});
        getDebugDrawerTopic()->setPoseVisu("GenerateAppoachMotion", "P2", new Pose {globalP2});
    }



    ARMARX_IMPORTANT << "setting target approack pose to:\n" << targetPoseInRoot;
    out.setGraspPrePose(PoseBasePtr {new Pose{targetPoseInRoot}});
    out.setObjectPose(new Pose {objInRoot});



    //    out.setGraspPoseDMP(graspPoseDMP);

    if (true || isTopGraspNow)
    {
        ARMARX_CHECK_EXPRESSION(in.isViaPoseCanValGraspSet());
        ARMARX_CHECK_EXPRESSION(in.isViaPoseGraspSet());
        ARMARX_CHECK_EXPRESSION(in.isDMPTrajectoryFileForFirstGraspSet());
        ARMARX_CHECK_EXPRESSION(in.isViaPoseCanValGraspSet());
        if (in.isViaPoseCanValGraspSet())
        {
            out.setViaPoseCanValGrasp({});
        }
        if (in.isViaPoseGraspSet())
        {
            out.setViaPoseGrasp({});
        }
        if (in.isDMPTrajectoryFileForFirstGraspSet())
        {
            out.setDMPTrajectoryFileForFirstGrasp({in.getDMPTrajectoryFileForFirstGrasp().at(0).substr(0, in.getDMPTrajectoryFileForFirstGrasp().at(0).size() - 4) + "PrePose.csv"});
            ARMARX_IMPORTANT << VAROUT(out.getDMPTrajectoryFileForFirstGrasp());
        }
        if (useRight)
        {
            graspPoseDMP->orientation->qw = 0.80185f;
            graspPoseDMP->orientation->qx = -0.519819f;
            graspPoseDMP->orientation->qy = -0.01836f;
            graspPoseDMP->orientation->qz = 0.294088f;
            graspPoseDMP->position->x = 478.885f;
            graspPoseDMP->position->y = 435.757f;
            graspPoseDMP->position->z = 1280.83f;
        }
        else
        {
            graspPoseDMP->orientation->qw = -0.649957f;
            graspPoseDMP->orientation->qx = 0.722493f;
            graspPoseDMP->orientation->qy = -0.0764362f;
            graspPoseDMP->orientation->qz = 0.222973f;
            graspPoseDMP->position->x = -412.217f;
            graspPoseDMP->position->y = 649.287f;
            graspPoseDMP->position->z = 1270.91f;
        }
    }
    else
    {
        if (in.isViaPoseCanValGraspSet())
        {
            out.setViaPoseCanValGrasp(in.getViaPoseCanValGrasp());
        }
        if (in.isViaPoseGraspSet())
        {
            out.setViaPoseGrasp(in.getViaPoseGrasp());
        }
        if (in.isDMPTrajectoryFileForFirstGraspSet())
        {
            out.setDMPTrajectoryFileForFirstGrasp(in.getDMPTrajectoryFileForFirstGrasp());
        }
    }
    out.setGraspPoseDMP(graspPoseDMP);
    emitGeneratedBezierMotion();
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GenerateApproachMotion::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GenerateApproachMotion(stateData));
}
