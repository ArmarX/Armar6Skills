/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CheckObjectExistence.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <RobotAPI/libraries/ArmarXObjects/ObjectID.h>
#include <RobotAPI/libraries/ArmarXObjects/ice_conversions.h>

using namespace armarx;
using namespace Armar6GraspingGroup;

// DO NOT EDIT NEXT LINE
CheckObjectExistence::SubClassRegistry CheckObjectExistence::Registry(CheckObjectExistence::GetName(), &CheckObjectExistence::CreateInstance);



void CheckObjectExistence::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    if (in.getObjectName().find("/") != std::string::npos)
    {
        // It's an ObjectID.
        ObjectID id(in.getObjectName());
        objpose::ObjectPoseSeq objectPoses = objpose::fromIce(getObjectPoseStorage()->getObjectPoses());
        for (const auto& p : objectPoses)
        {
            if (p.objectID == id)
            {
                ARMARX_IMPORTANT << "Found object in ObjectPoseStorage: " << id;
                // out.setObjectChannel(nullptr); // Optional
                // Miss-use instance ID for our ObjectID.
                out.setObjectInstanceId(id.str());
                emitSuccess();
                return;
            }
        }
    }
    else
    {
        std::vector<::armarx::ChannelRefPtr> channels = in.getObjectInstanceChannels();
        for (::armarx::ChannelRefPtr channel : channels)
        {
            std::string className = channel->getDataField("className")->getString();
            if (className == in.getObjectName())
            {
                std::string instanceId = channel->getDataField("id")->getString();
                ARMARX_IMPORTANT << "Found object in working memory: class '" << className << "' with id '" << instanceId << "'";
                out.setObjectChannel(channel);
                out.setObjectInstanceId(instanceId);
                emitSuccess();
                return;
            }
        }
    }

    ARMARX_WARNING << "No object instance of class " << in.getObjectName() << " found";
    emitFailure();
}

//void CheckObjectExistence::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void CheckObjectExistence::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void CheckObjectExistence::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CheckObjectExistence::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CheckObjectExistence(stateData));
}
