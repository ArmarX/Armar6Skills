/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GraspingGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BringObjectToLandmark.h"

#include <filesystem>

#include <SimoxUtility/json.h>
#include <SimoxUtility/algorithm/contains.h>
#include <SimoxUtility/algorithm/string.h>
//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <RobotAPI/libraries/ArmarXObjects/ice_conversions.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>

#include <regex>

using namespace armarx;
using namespace Armar6GraspingGroup;
namespace fs = std::filesystem;

// DO NOT EDIT NEXT LINE
BringObjectToLandmark::SubClassRegistry BringObjectToLandmark::Registry(BringObjectToLandmark::GetName(), &BringObjectToLandmark::CreateInstance);



void BringObjectToLandmark::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)

    std::string givenObjectName = in.getGivenObjectName();
    const std::map<std::string, std::string> map = in.getObjectNameMapping();



    givenObjectName = std::regex_replace(givenObjectName, std::regex("the "), "");
    givenObjectName = std::regex_replace(givenObjectName, std::regex("!"), "");
    givenObjectName = std::regex_replace(givenObjectName, std::regex(" "), "");



    std::string groundedObjectName;

    if (map.count(givenObjectName) > 0)
    {
        groundedObjectName = map.at(givenObjectName);
        ARMARX_INFO << "Mapping '" << givenObjectName << "' to  '" << groundedObjectName << "'.";
    }
    else
    {
        auto handleMatch = [this, &groundedObjectName](const std::string & givenObjectName, const ObjectID & objectID)
        {
            ARMARX_IMPORTANT << "Matched givenObjectName '" << givenObjectName << "'"
                             << " to PriorKnowledgeData object with ID " << objectID << ".";
            groundedObjectName = objectID.str();
        };

        ObjectFinder finder;

        if (groundedObjectName.empty())
        {
            const std::vector<objpose::ObjectPose> objectPoses = objpose::fromIce(getObjectPoseStorage()->getObjectPoses());
            ARMARX_INFO << "Searching '" << givenObjectName << "' in ObjectPoseStorage (" << objectPoses.size() << " objects).";
            for (const objpose::ObjectPose& objectPose : objectPoses)
            {
                static const bool includeClassName = true;
                const std::vector<std::string> names = finder.loadRecognizedNames(objectPose.objectID, includeClassName);
                if (simox::alg::contains(names, givenObjectName))
                {
                    handleMatch(givenObjectName, objectPose.objectID);
                    break;
                }
            }
        }

        if (groundedObjectName.empty())
        {
            const std::vector<std::string> objectIDWhitelist = in.getObjectIDWhitelist();
            ARMARX_INFO << "Did not find '" << givenObjectName << "' in ObjectPoseStorage.\n"
                        << "Searching ObjectID whitelist: " << objectIDWhitelist;
            for (const std::string& objectID : objectIDWhitelist)
            {
                static const bool includeClassName = true;
                const std::vector<std::string> names = finder.loadRecognizedNames(ObjectID(objectID), includeClassName);
                if (simox::alg::contains(names, givenObjectName))
                {
                    handleMatch(givenObjectName, objectID);
                    break;
                }
            }
        }

        if (groundedObjectName.empty())
        {
            ARMARX_INFO << "Using name from speech directly: '" << givenObjectName << "'";
            local.setObjectName(givenObjectName);
        }
    }

    local.setObjectName(groundedObjectName);
    out.setObjectName(groundedObjectName);
}

//void BringObjectToLandmark::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void BringObjectToLandmark::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void BringObjectToLandmark::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr BringObjectToLandmark::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new BringObjectToLandmark(stateData));
}
