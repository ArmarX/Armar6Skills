/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <thread>

#include "SimpleGraspNoVision.h"

using namespace armarx;
using namespace Armar6GraspingGroup;

// DO NOT EDIT NEXT LINE
SimpleGraspNoVision::SubClassRegistry SimpleGraspNoVision::Registry(SimpleGraspNoVision::GetName(), &SimpleGraspNoVision::CreateInstance);

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>


void SimpleGraspNoVision::run()
{
    NJointCartesianVelocityControllerWithRampConfigPtr config = new NJointCartesianVelocityControllerWithRampConfig(in.getNodeSetName(), "", CartesianSelectionMode::eAll, 500, 1, 2, 0, 2);
    NJointControllerInterfacePrx ctrl = getRobotUnit()->createNJointController("NJointCartesianVelocityControllerWithRamp", in.getCartesianVelocityControllerName(), config);

    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx handController =
        devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getHandControllerName()));
    ARMARX_CHECK_EXPRESSION(handController);
    handController->activateController();
    handController->setTargetsWithPwm(0, 0, 1, 1);

    while (!isRunningTaskStopped())
    {
        std::this_thread::sleep_for(std::chrono::milliseconds{1});
    }

    ctrl->deactivateController();
    while (ctrl->isControllerActive())
    {
        std::this_thread::sleep_for(std::chrono::milliseconds{1});
    }
    ctrl->deleteController();
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SimpleGraspNoVision::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SimpleGraspNoVision(stateData));
}
