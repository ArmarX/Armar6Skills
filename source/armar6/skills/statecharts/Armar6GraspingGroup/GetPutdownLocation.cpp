/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GraspingGroup
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GetPutdownLocation.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <armar6/skills/interface/FindPutdownLocation.h>

#include <armar6/skills/statecharts/Armar6GraspingGroup/Armar6GraspingGroupStatechartContext.generated.h>

using namespace armarx;
using namespace Armar6GraspingGroup;

// DO NOT EDIT NEXT LINE
GetPutdownLocation::SubClassRegistry GetPutdownLocation::Registry(GetPutdownLocation::GetName(), &GetPutdownLocation::CreateInstance);



void GetPutdownLocation::onEnter()
{


    FindPutdownLocationInterfacePrx locationProvider;

    try
    {
        locationProvider = StateBase::getContext<Armar6GraspingGroup::Armar6GraspingGroupStatechartContext>()->getIceManager()->getProxy<FindPutdownLocationInterfacePrx>("FindPutdownLocation");
    }
    catch (...)
    {

    }

    if (locationProvider)
    {
        Vector3BaseList putdownLocations = locationProvider->getPutdownLocations();

        if (putdownLocations.empty())
        {
            emitFailure();
            return;
        }

        Eigen::Vector3f v = Vector3Ptr::dynamicCast(putdownLocations.front())->toEigen();
        VirtualRobot::RobotPtr robot = getLocalRobot();
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());


        //FramedPositionPtr position = new FramedPosition(v, armarx::GlobalFrame, robot->getName());
        //Eigen::Vector3f d = position->toRootEigen(robot);

        Eigen::Vector3f globalPosition = robot->getGlobalPosition();
        Eigen::Vector3f d = v - globalPosition;

        // v(1) = globalPosition(1);
        // getDebugDrawerTopic()->setLineVisu(getName(), "distance", new Vector3(v), new Vector3(globalPosition), 5.0, {1.0, 0, 1.0 , 1.0});


        out.setPlatformRelativePosition(new Vector3(d(0) - 300.0, 150.0, 0.0));
        out.setPutdownLocation(putdownLocations.front());

        emitSuccess();
    }
    else
    {
        emitFailure();
    }

    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

//void GetPutdownLocation::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void GetPutdownLocation::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GetPutdownLocation::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GetPutdownLocation::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GetPutdownLocation(stateData));
}
