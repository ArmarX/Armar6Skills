/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GraspingGroup
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PlaceObject.h"

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>

#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>

#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <VirtualRobot/math/Helpers.h>
#include <MemoryX/libraries/motionmodels/MotionModelStaticObject.h>

#include <RobotAPI/libraries/core/CartesianPositionController.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectID.h>
#include <RobotAPI/libraries/ArmarXObjects/ice_conversions.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6GraspingGroup;

enum class PhaseType
{
    ApproachPrePose,
    Place,
    Unload,
    Retract
};

// DO NOT EDIT NEXT LINE
PlaceObject::SubClassRegistry PlaceObject::Registry(PlaceObject::GetName(), &PlaceObject::CreateInstance);



void PlaceObject::onEnter()
{
    NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController(in.getCartesianVelocityControllerName());
    if (controller)
    {
        controller->deactivateController();
        while (controller->isControllerActive())
        {
            TimeUtil::SleepMS(1);
        }
        controller->deleteController();

        TimeUtil::SleepMS(200);
    }
}

void PlaceObject::shapeHand(float fingers, float thumb)
{
    std::string handJointPrefix = in.getHandJointsPrefix().at(in.getSelectedArm());
    std::string fingersName = handJointPrefix + "Fingers";
    std::string thumbName = handJointPrefix + "Thumb";

    getKinematicUnit()->switchControlMode({{fingersName, ePositionControl}, {thumbName, ePositionControl}});
    getKinematicUnit()->setJointAngles({{fingersName, fingers}, {thumbName, thumb}});
}

void PlaceObject::run()
{
    std::string selectedArm = in.getSelectedArm();
    std::string nodeSetName = in.getNodeSetName().at(selectedArm);
    std::string FTDatefieldName = in.getFTDatefieldName().at(selectedArm);
    const std::string objectName = in.getObjectName();
    const std::string graspType = [this, &objectName]() -> std::string
    {
        std::map<std::string, std::string> types = in.getGraspTypes();
        if (auto it = types.find(objectName); it != types.end())
        {
            return it->second;
        }
        else if (auto it = types.find("default"); it != types.end())
        {
            return it->second;
        }
        else
        {
            return "SideGrasp";
        }
    }();
    Eigen::Matrix4f prePlacePose = in.getPrePlacePose().at(selectedArm + "_" + graspType)->toEigen();
    Eigen::Vector3f retractVector = in.getRetractVector().at(selectedArm + "_" + graspType)->toEigen();


    NJointCartesianVelocityControllerWithRampConfigPtr config = new NJointCartesianVelocityControllerWithRampConfig(nodeSetName, "", CartesianSelectionMode::eAll, 500, 1, 2, 0, 2);
    NJointControllerInterfacePrx ctrl = getRobotUnit()->createNJointController("NJointCartesianVelocityControllerWithRamp", in.getCartesianVelocityControllerName(), config);

    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(nodeSetName);
    out.setUsedNodeSet(rns->getName());
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();

    shapeHand(2, 2);
    NJointCartesianVelocityControllerWithRampInterfacePrx controller = NJointCartesianVelocityControllerWithRampInterfacePrx::checkedCast(ctrl);
    controller->activateController();

    controller->setKpJointLimitAvoidance(0);
    controller->setJointLimitAvoidanceScale(0);


    DatafieldRefPtr forceDf = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield(FTDatefieldName));
    DatafieldRefPtr torqueDf = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getTorqueDatafield(FTDatefieldName));
    Eigen::Vector3f forceOffset = forceDf->getDataField()->get<FramedDirection>()->toEigen();
    Eigen::Vector3f torqueOffset = torqueDf->getDataField()->get<FramedDirection>()->toEigen();

    CartesianPositionController posController(tcp);
    posController.maxOriVel = in.getMaxOriVel();
    posController.maxPosVel = in.getMaxPosVel();
    posController.KpOri = in.getKpOrientation();
    posController.KpPos = in.getKpPosition();

    Eigen::Matrix4f target = prePlacePose;


    PhaseType currentPhase = PhaseType::ApproachPrePose;
    IceUtil::Time unloadStart;

    CycleUtil c(10);
    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        Eigen::VectorXf cv = posController.calculate(target, VirtualRobot::IKSolver::All);
        controller->setTargetVelocity(cv(0), cv(1), cv(2), cv(3), cv(4), cv(5));

        float positionError = posController.getPositionError(target);
        float orientationError = posController.getOrientationError(target);
        ARMARX_INFO << deactivateSpam(1) << VAROUT(positionError) << " " << VAROUT(orientationError) << " " << VAROUT(cv.transpose());

        PhaseType nextPhase = currentPhase;

        bool targetReached = positionError < in.getPositionThreshold() && orientationError < in.getOrientationThreshold();

        FramedDirectionPtr currentForcePtr = forceDf->getDataField()->get<FramedDirection>();
        const Eigen::Vector3f currentForce = currentForcePtr->toEigen() - forceOffset;
        const Eigen::Vector3f currentTorque = torqueDf->getDataField()->get<FramedDirection>()->toEigen() - torqueOffset;

        getDebugObserver()->setDebugDatafield("TryTopGrasp", "force_x", new Variant(currentForce(0)));
        getDebugObserver()->setDebugDatafield("TryTopGrasp", "force_y", new Variant(currentForce(1)));
        getDebugObserver()->setDebugDatafield("TryTopGrasp", "force_z", new Variant(currentForce(2)));
        getDebugObserver()->setDebugDatafield("TryTopGrasp", "torque_x", new Variant(currentTorque(0)));
        getDebugObserver()->setDebugDatafield("TryTopGrasp", "torque_y", new Variant(currentTorque(1)));
        getDebugObserver()->setDebugDatafield("TryTopGrasp", "torque_z", new Variant(currentTorque(2)));

        getDebugObserver()->setDebugDatafield("TryTopGrasp", "force", new Variant(currentForce.norm()));
        getDebugObserver()->setDebugDatafield("TryTopGrasp", "torque", new Variant(currentTorque.norm()));

        bool forceExceeded = currentForce.norm() > in.getForceThreshold();
        ARMARX_INFO << deactivateSpam(0.5) << "currentForce: " << currentForce.norm();
        if (currentPhase == PhaseType::ApproachPrePose)
        {
            if (targetReached)
            {
                ARMARX_IMPORTANT << "PrePose reached";
                forceOffset = forceDf->getDataField()->get<FramedDirection>()->toEigen();
                torqueOffset = torqueDf->getDataField()->get<FramedDirection>()->toEigen();

                target = prePlacePose;
                target.block<3, 1>(0, 3) += in.getPlaceVector()->toEigen();
                nextPhase = PhaseType::Place;
                posController.maxPosVel = in.getMaxPosVelPlaceDown();
                if (graspType == "TopGrasp")
                {
                    posController.maxPosVel *= 0.5;
                }
            }
        }
        if (currentPhase == PhaseType::Place)
        {
            if (targetReached)
            {
                ARMARX_IMPORTANT << "targetReached";
                controller->immediateHardStop();
                emitFailure();
                break;
            }
            if (forceExceeded)
            {
                ARMARX_IMPORTANT << "Force exceeded: " << currentForce.norm();
                controller->immediateHardStop();
                //                shapeHand(0, 0);
                nextPhase = PhaseType::Unload;
                target = tcp->getPoseInRootFrame();
                if (graspType == "TopGrasp")
                {
                    target = ::math::Helpers::TranslatePose(target, Eigen::Vector3f(0, 0, 20));
                }
                unloadStart = TimeUtil::GetTime();
            }
        }
        if (currentPhase == PhaseType::Unload)
        {
            float t = (TimeUtil::GetTime() - unloadStart).toSecondsDouble();
            float handJointsVal = 1.f - ::math::Helpers::ILerp(0, in.getUnloadTime(), t);
            shapeHand(handJointsVal, handJointsVal);
            if (t > in.getUnloadTime())
            {
                nextPhase = PhaseType::Retract;
                posController.maxPosVel = in.getMaxPosVel();
                target = tcp->getPoseInRootFrame();
                target.block<3, 1>(0, 3) += retractVector;
                shapeHand(0, 0);
                if (objectName.find("/") != std::string::npos)
                {
                    ObjectID objectID(objectName);
                    ARMARX_INFO << "Detaching object with ID " << objectID << " in ObjectPoseStorage.";
                    objpose::DetachObjectFromRobotNodeInput input;
                    toIce(input.objectID, objectID);
                    getObjectPoseStorage()->detachObjectFromRobotNode(input);
                }
                if (in.isObjectInstanceChannelSet() and in.getObjectInstanceChannel())
                {
                    try
                    {
                        TimedVariantPtr idField = ChannelRefPtr::dynamicCast(in.getObjectInstanceChannel())->getDataField("id");
                        std::string attachedObjectId = idField->getString();
                        ARMARX_INFO << "Detaching object " << attachedObjectId;
                        memoryx::MotionModelStaticObjectPtr newMotionModel(new memoryx::MotionModelStaticObject(getRobotStateComponent()));
                        getWorkingMemory()->getObjectInstancesSegment()->setNewMotionModel(attachedObjectId, newMotionModel);
                    }
                    catch (std::exception const& ex)
                    {
                        ARMARX_WARNING << "Could not get ID of attached object from channel. Reason:\n"
                                       << ex.what();
                    }
                }
                else
                {
                    ARMARX_INFO << "No object instance channel set - not detaching it";
                }
            }
        }
        if (currentPhase == PhaseType::Retract)
        {
            if (targetReached)
            {
                controller->immediateHardStop();
                emitSuccess();
                return;
            }
        }

        currentPhase = nextPhase;


        c.waitForCycleDuration();
    }

    ctrl->deactivateController();

    while (ctrl->isControllerActive())
    {
        TimeUtil::SleepMS(1);
    }
    ctrl->deleteController();
}

//void PlaceObject::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void PlaceObject::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr PlaceObject::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new PlaceObject(stateData));
}
