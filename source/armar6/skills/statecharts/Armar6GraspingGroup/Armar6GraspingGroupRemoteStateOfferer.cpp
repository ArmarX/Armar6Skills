/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GraspingGroup::Armar6GraspingGroupRemoteStateOfferer
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6GraspingGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6GraspingGroup;

// DO NOT EDIT NEXT LINE
Armar6GraspingGroupRemoteStateOfferer::SubClassRegistry Armar6GraspingGroupRemoteStateOfferer::Registry(Armar6GraspingGroupRemoteStateOfferer::GetName(), &Armar6GraspingGroupRemoteStateOfferer::CreateInstance);



Armar6GraspingGroupRemoteStateOfferer::Armar6GraspingGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6GraspingGroupStatechartContext > (reader)
{
}

void Armar6GraspingGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6GraspingGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6GraspingGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6GraspingGroupRemoteStateOfferer::GetName()
{
    return "Armar6GraspingGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6GraspingGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6GraspingGroupRemoteStateOfferer(reader));
}
