/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GraspingGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ApproachDynamicObjectPose.h"

#include <RobotAPI/libraries/core/CartesianPositionController.h>

#include <ArmarXCore/core/time/CycleUtil.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>

using namespace armarx;
using namespace Armar6GraspingGroup;

// DO NOT EDIT NEXT LINE
ApproachDynamicObjectPose::SubClassRegistry ApproachDynamicObjectPose::Registry(ApproachDynamicObjectPose::GetName(), &ApproachDynamicObjectPose::CreateInstance);



void ApproachDynamicObjectPose::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void ApproachDynamicObjectPose::run()
{
    auto robot = this->getLocalRobot();
    RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    auto objRobot = this->getLocalRobot();
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true
    auto tcp = robot->getRobotNodeSet(in.getKinematicChainName())->getTCP();
    NJointCartesianVelocityControllerWithRampConfigPtr ctrlCfg = new NJointCartesianVelocityControllerWithRampConfig();
    ctrlCfg->KpJointLimitAvoidance = 0;
    ctrlCfg->jointLimitAvoidanceScale = 1;
    ctrlCfg->mode = CartesianSelectionMode::eAll;
    ctrlCfg->nodeSetName = in.getKinematicChainName();
    ctrlCfg->tcpName = tcp->getName();
    ctrlCfg->maxNullspaceAcceleration = 2;
    ctrlCfg->maxPositionAcceleration = in.getTCPAcceleration();
    ctrlCfg->maxOrientationAcceleration = 1;

    CartesianPositionController posController(tcp);
    posController.maxOriVel = in.getMaxOrientationVelocity();
    posController.maxPosVel = in.getMaxTCPVelocity();
    posController.KpOri = in.getKpOrientation();
    posController.KpPos = in.getKp();
    CycleUtil cycle(10);

    auto objectInstanceChannel = in.getObjectInstanceChannel();

    auto ctrlName = in.isCartesianVelocityControllerNameSet() ? in.getCartesianVelocityControllerName() : "";

    auto baseCtrl = getRobotUnit()->getNJointController(ctrlName);
    bool createdNJointCtrl = false;
    if (ctrlName.empty() || !baseCtrl)
    {
        baseCtrl = getRobotUnit()->createNJointController("NJointCartesianVelocityControllerWithRamp", ctrlName, ctrlCfg);
        createdNJointCtrl = true;
    }
    ARMARX_CHECK_EXPRESSION(baseCtrl);
    NJointCartesianVelocityControllerWithRampInterfacePrx ctrl = NJointCartesianVelocityControllerWithRampInterfacePrx::checkedCast(baseCtrl);
    ARMARX_CHECK_EXPRESSION(ctrl) << baseCtrl->ice_ids();
    ctrl->activateController();
    ARMARX_INFO << "target pose offset: " << in.getTargetPoseOffset()->toEigen();
    auto dbgObs = getDebugObserver()->ice_batchOneway();
    std::string ddLayer = "ApproachDynamicObjectPose";
    FramedPosePtr initialTCPPose = new FramedPose(tcp->getPoseInRootFrame(), robot->getRootNode()->getName(), robot->getName());
    out.setInitialTCPPose(initialTCPPose);
    out.setInitialTCPPosition(new FramedPosition(tcp->getPositionInRootFrame(), robot->getRootNode()->getName(), robot->getName()));
    out.setInitialTCPOrientation(new FramedOrientation(tcp->getPoseInRootFrame(), robot->getRootNode()->getName(), robot->getName()));
    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    auto dd = getDebugDrawerTopic()->ice_batchOneway();
    bool deactivateController = in.getDeactivateControllerOnExit();

    auto objPose = objectInstanceChannel->getDataField("pose")->get<FramedPose>();
    auto objPoseTimestamp = objectInstanceChannel->getDataField("timestamp")->get<TimestampVariant>();
    auto objExistenceCertainty = objectInstanceChannel->getDataField("existenceCertainty")->getFloat();
    auto updateObjectPose = in.getUpdateObjectPose();
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        // do your calculations
        if (updateObjectPose)
        {
            objPose = objectInstanceChannel->getDataField("pose")->get<FramedPose>();
            objPoseTimestamp = objectInstanceChannel->getDataField("timestamp")->get<TimestampVariant>();
            objExistenceCertainty = objectInstanceChannel->getDataField("existenceCertainty")->getFloat();
        }
        RemoteRobot::synchronizeLocalCloneToTimestamp(objRobot, getRobotStateComponent(), objPoseTimestamp->getTimestamp());
        getHeadIKUnit()->setHeadTarget(in.getVirtualGazeNodeSet(), new FramedPosition(objPose->toGlobal(robot)->toEigen(), GlobalFrame, ""));
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

        Eigen::Matrix4f globalTargetPose = objPose->toGlobalEigen(robot) * in.getTargetPoseOffset()->toEigen();
        Eigen::Matrix4f rootTargetPose = FramedPose(globalTargetPose, GlobalFrame, "").toRootEigen(robot);
        dd->setPoseVisu(ddLayer, "TargetPose", new Pose(globalTargetPose));

        auto posError = posController.getPositionError(rootTargetPose);
        auto oriError = posController.getOrientationError(rootTargetPose);
        auto globalObjPose = objPose->toGlobal(robot);
        dd->setPoseVisu(ddLayer, "ObjectPose", globalObjPose);
        Eigen::Vector3f textPos = globalObjPose->toEigen().block<3, 1>(0, 3);
        textPos(2) += 300;
        std::stringstream distanceStr;
        distanceStr.precision(3);
        distanceStr << "Distance: " << posController.getPositionError(rootTargetPose) << " mm, " <<
                    (posController.getOrientationError(rootTargetPose) * 180 / M_PI) << " deg";
        dd->setTextVisu(ddLayer, "DistanceText",
                        distanceStr.str(),
                        new Vector3(textPos), DrawColor {0, 0, 1, 0.5}, 15);
        ARMARX_INFO << deactivateSpam(1) << VAROUT(*objPose) << VAROUT(globalTargetPose)  << " time: " << objPoseTimestamp->getTimestamp() << VAROUT(textPos);
        Eigen::VectorXf cv = posController.calculate(rootTargetPose, VirtualRobot::IKSolver::All);
        ctrl->setTargetVelocity(cv(0), cv(1), cv(2), cv(3), cv(4), cv(5));

        dbgObs->setDebugDatafield(ddLayer, "positionError", new Variant(posError));
        dbgObs->setDebugDatafield(ddLayer, "orientationError", new Variant(oriError));
        dbgObs->ice_flushBatchRequests();
        dd->ice_flushBatchRequests();
        if (posError < in.getPositionAccuracy() &&
            oriError < in.getOrientationAccuracy())
        {
            emitSuccess();
            break;
        }
        if (updateObjectPose && objExistenceCertainty < in.getMinimumExistenceCertainty())
        {
            ctrl->setTargetVelocity(0, 0, 0,
                                    0, 0, 0);
            deactivateController = true;
            emitObjectLost();
            break;
        }

        cycle.waitForCycleDuration();

    }

    if (deactivateController)
    {
        if (createdNJointCtrl) // only delete if we created it
        {
            baseCtrl->deactivateAndDeleteController();
        }
        else
        {
            baseCtrl->deactivateController();
        }
    }
    getDebugDrawerTopic()->clearLayer(ddLayer);
}

//void ApproachDynamicObjectPose::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ApproachDynamicObjectPose::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ApproachDynamicObjectPose::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ApproachDynamicObjectPose(stateData));
}
