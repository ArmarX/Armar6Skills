/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TestHandUprightOrientation.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <VirtualRobot/math/Helpers.h>

using namespace armarx;
using namespace Armar6GraspingGroup;

// DO NOT EDIT NEXT LINE
TestHandUprightOrientation::SubClassRegistry TestHandUprightOrientation::Registry(TestHandUprightOrientation::GetName(), &TestHandUprightOrientation::CreateInstance);



void TestHandUprightOrientation::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void TestHandUprightOrientation::run()
{
    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(in.getNodeSetName());
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();

    ARMARX_IMPORTANT << "Hello!";

    Eigen::Matrix3f referenceOrientation = in.getReferenceOrientation()->toEigen();




    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        Eigen::Matrix4f tcpPose = tcp->getPoseInRootFrame();
        Eigen::Matrix3f tcpOri = tcpPose.block<3, 3>(0, 0);

        Eigen::Vector3f sourceLocalUp = referenceOrientation.inverse() * Eigen::Vector3f::UnitZ();
        Eigen::Matrix4f sourceLocalUpPose = Eigen::Matrix4f::Identity();
        sourceLocalUpPose.block<3, 1>(0, 3) = sourceLocalUp * 100;

        Eigen::Matrix3f targetOri = ::math::Helpers::RotateOrientationToFitVector(tcpOri, sourceLocalUp, Eigen::Vector3f::UnitZ());
        Eigen::Matrix4f targetPose = tcpPose;
        targetPose.block<3, 3>(0, 0) = targetOri;
        targetPose.block<3, 1>(0, 3) += Eigen::Vector3f(0, 0, -50);

        Eigen::Matrix4f referencePose = tcpPose;
        referencePose.block<3, 3>(0, 0) = referenceOrientation;
        referencePose.block<3, 1>(0, 3) += Eigen::Vector3f(0, 0, -100);

        if (in.getDrawDebugInfo())
        {
            getDebugDrawerTopic()->setPoseVisu("TestHandUprightOrientation", "tcp", new Pose(robot->getGlobalPose() * tcpPose));
            getDebugDrawerTopic()->setPoseVisu("TestHandUprightOrientation", "targetPose", new Pose(robot->getGlobalPose() * targetPose));
            getDebugDrawerTopic()->setPoseVisu("TestHandUprightOrientation", "referencePose", new Pose(robot->getGlobalPose() * referencePose));
        }

        DrawColor color;
        color.r = 1;
        color.g = 0;
        color.b = 0;
        color.a = 1;

        if (in.getDrawDebugInfo())
        {
            getDebugDrawerTopic()->setLineVisu("TestHandUprightOrientation", "upVectorTarget",
                                               new Vector3(Eigen::Matrix4f(robot->getGlobalPose() * targetPose)),
                                               new Vector3(Eigen::Matrix4f(robot->getGlobalPose() * targetPose * sourceLocalUpPose)), 5, color);
        }

        color.r = 0;
        color.g = 0;
        color.b = 1;
        color.a = 1;

        if (in.getDrawDebugInfo())
        {
            getDebugDrawerTopic()->setLineVisu("TestHandUprightOrientation", "upVectorReference",
                                               new Vector3(Eigen::Matrix4f(robot->getGlobalPose() * referencePose)),
                                               new Vector3(Eigen::Matrix4f(robot->getGlobalPose() * referencePose * sourceLocalUpPose)), 5, color);
        }


        color.r = 0;
        color.g = 1;
        color.b = 0;
        color.a = 1;

        if (in.getDrawDebugInfo())
        {
            getDebugDrawerTopic()->setLineVisu("TestHandUprightOrientation", "upVectorTcp",
                                               new Vector3(Eigen::Matrix4f(robot->getGlobalPose() * tcpPose)),
                                               new Vector3(Eigen::Matrix4f(robot->getGlobalPose() * tcpPose * sourceLocalUpPose)), 5, color);
        }


        TimeUtil::SleepMS(10);

    }
}

//void TestHandUprightOrientation::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void TestHandUprightOrientation::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TestHandUprightOrientation::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TestHandUprightOrientation(stateData));
}
