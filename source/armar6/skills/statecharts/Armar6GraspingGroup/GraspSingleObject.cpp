/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspSingleObject.h"

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>

#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <thread>

using namespace armarx;
using namespace Armar6GraspingGroup;

// DO NOT EDIT NEXT LINE
GraspSingleObject::SubClassRegistry GraspSingleObject::Registry(GraspSingleObject::GetName(), &GraspSingleObject::CreateInstance);



void GraspSingleObject::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void GraspSingleObject::run()
{
    NJointCartesianVelocityControllerWithRampConfigPtr config = new NJointCartesianVelocityControllerWithRampConfig(in.getNodeSetName(), "", CartesianSelectionMode::eAll, 500, 1, 2, 0, 2);
    NJointControllerInterfacePrx ctrl = getRobotUnit()->createNJointController("NJointCartesianVelocityControllerWithRamp", in.getCartesianVelocityControllerName(), config);

    //    {
    //        devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx HandController =
    //            devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getHandControllerName()));
    //        ARMARX_CHECK_EXPRESSION(HandController);
    //        HandController->activateController();
    //        HandController->setTargetsWithPwm(0, 0, 1, 1);
    //    }

    while (!isRunningTaskStopped())
    {
        std::this_thread::sleep_for(std::chrono::milliseconds {1});
    }

    ctrl->deactivateController();

    while (ctrl->isControllerActive())
    {
        std::this_thread::sleep_for(std::chrono::milliseconds {1});
    }
    ctrl->deleteController();
}

//void GraspSingleObject::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GraspSingleObject::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}

//void GraspSingleObject::transitionSuccessFromLocalizeObject(ScanLocationGroup::ScanLocationIn& next, const ObjectLocalization::LocalizeObjectOut& prev)
//{
//    std::string id = prev.getObjectInstanceChannel()->getDataField("id")->getString();
//    ARMARX_IMPORTANT << "Object instance id: " << id;

//    local.setObjectInstanceId(id);
//}

//void GraspSingleObject::transitionTimeoutFromLocalizeObject(ScanLocationGroup::ScanLocationIn& next, const ObjectLocalization::LocalizeObjectOut& prev)
//{
//    transitionSuccessFromLocalizeObject(next, prev);
//}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GraspSingleObject::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GraspSingleObject(stateData));
}



void armarx::Armar6GraspingGroup::GraspSingleObject::transitionSuccessFromRequestObjectLocalization(ScanLocationGroup::ScanLocationIn& next, const ObjectLocalization::RequestObjectLocalizationOut& prev)
{
    ARMARX_INFO << "Setting object name to " << in.getObjectName();
    next.setObjectclassesToLocalize({in.getObjectName()});
}

void GraspSingleObject::transitionSuccessFromScanLocation(CheckObjectExistenceIn& next, const ScanLocationGroup::ScanLocationOut& prev)
{
    ChannelRefBasePtr handInstanceChannel = getObjectMemoryObserver()->getFirstObjectInstance(local.getHandChannel());
    ARMARX_IMPORTANT << "Hand Instance Channel: " << handInstanceChannel;
    local.setHandInstanceChannel(handInstanceChannel);
}
