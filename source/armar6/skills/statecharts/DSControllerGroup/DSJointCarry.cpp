/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::DSControllerGroup
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DSJointCarry.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
//#include <armarx/control/ds_controller/DSControllerInterface.h>
#include <RobotAPI/interface/units/RobotUnit/DSControllerBase.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>

#include <armar6/skills/statecharts/DSControllerGroup/DSControllerGroupStatechartContext.generated.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <RobotAPI/libraries/core/math/MathUtils.h>

#include "Helpers.h"

//#include "DynamicObstacleAvoidance/Modulation.hpp"
//#include "DynamicObstacleAvoidance/Obstacle/Ellipsoid.hpp"
//#include "DynamicObstacleAvoidance/Agent.hpp"
#include <RobotAPI/libraries/core/FramedPose.h>

#include <RobotAPI/interface/components/ObstacleAvoidance/ObstacleAvoidanceInterface.h>
#include <RobotAPI/interface/components/ObstacleAvoidance/ObstacleDetectionInterface.h>
#include <VirtualRobot/math/Helpers.h>
#include <SimoxUtility/math/convert/mat3f_to_rpy.h>
using namespace armarx;
using namespace DSControllerGroup;

// DO NOT EDIT NEXT LINE
DSJointCarry::SubClassRegistry DSJointCarry::Registry(DSJointCarry::GetName(), &DSJointCarry::CreateInstance);



void DSJointCarry::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void DSJointCarry::run()
{
    getRobotUnit()->loadLibFromPackage("RobotAPI", "DSControllers");

    float posiKp = in.getPositionKp();
    float v_max = in.getDSMaxVelocity();
    std::vector<float> posiDamping = in.getDamping();
    float oriDamping = in.getOriDamping();
    float oriKp = in.getOriKp();
    float filterTimeConstant = in.getFilterTimeConstant();
    float torqueLimit = in.getTorqueLimit();
    float nullTorqueLimit = in.getNullTorqueLimit();

    std::vector<float> left_desiredQuaternion;
    left_desiredQuaternion.push_back(in.getLeftDesiredQuaternion()->qw);
    left_desiredQuaternion.push_back(in.getLeftDesiredQuaternion()->qx);
    left_desiredQuaternion.push_back(in.getLeftDesiredQuaternion()->qy);
    left_desiredQuaternion.push_back(in.getLeftDesiredQuaternion()->qz);
    std::vector<float> right_desiredQuaternion;
    right_desiredQuaternion.push_back(in.getRightDesiredQuaternion()->qw);
    right_desiredQuaternion.push_back(in.getRightDesiredQuaternion()->qx);
    right_desiredQuaternion.push_back(in.getRightDesiredQuaternion()->qy);
    right_desiredQuaternion.push_back(in.getRightDesiredQuaternion()->qz);

    std::vector<float> leftarm_qnullspaceVec = in.getLeftnullspaceVec();
    std::vector<float> rightarm_qnullspaceVec = in.getRightnullspaceVec();
    float nullspaceKp = in.getNullSpaceKp();
    float nullspaceDamping = in.getNullSpaceDamping();
    std::string gmmParamsFile = in.getGmmfile();
    float positionErrorTolerance = in.getPositionErrorTolerance();
    float desiredTorqueDisturbance = in.getTorqueDisturbance();
    float torqueFilterConstant = in.getTorqueFilterConstant();
    float guardLength = in.getGuardLength();
    float guardGravity = in.getGuardGravity();

    std::vector<float> defaultGuardOri;
    defaultGuardOri.push_back(in.getDefaultGuardOri()->qw);
    defaultGuardOri.push_back(in.getDefaultGuardOri()->qx);
    defaultGuardOri.push_back(in.getDefaultGuardOri()->qy);
    defaultGuardOri.push_back(in.getDefaultGuardOri()->qz);
    float handVelLimit = in.getHandVelLimit();
    std::vector<float> defaultRotationStiffness = in.getDefaultRotationStiffness();

    DSJointCarryControllerConfigPtr config = new DSJointCarryControllerConfig(posiKp,
            v_max,
            posiDamping,
            oriDamping,
            oriKp,
            filterTimeConstant,
            torqueLimit,
            nullTorqueLimit,
            left_desiredQuaternion,
            right_desiredQuaternion,
            leftarm_qnullspaceVec,
            rightarm_qnullspaceVec,
            nullspaceKp,
            nullspaceDamping,
            gmmParamsFile,
            positionErrorTolerance,
            desiredTorqueDisturbance,
            torqueFilterConstant,
            guardLength,
            guardGravity,
            defaultGuardOri,
            handVelLimit,
            defaultRotationStiffness);



    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    DSJointCarryControllerInterfacePrx dsController
        = DSJointCarryControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("DSJointCarryController", "dsController", config));

    ARMARX_INFO << "Finished crearing dsController";
    dsController->activateController();

    float minPosiTolerance = in.getMinPosiTolerance();


    obstacledetection::Obstacle obstacle = {"box", 3000, 4000, 0.0, 0, 520 / 2 + 100, 850 / 2 + 100, 1120, 3000, 4000, 0.0, 200, 200, 200, 1, 1, 1};

    obstacle.curvatureX = 2;
    obstacle.curvatureY = 2;
    obstacle.curvatureZ = 2;

    getObstacleDetection()->updateObstacle(obstacle);
    getObstacleDetection()->updateEnvironment();
    DebugDrawerInterfacePrx debug = getDebugDrawerTopic();
    usleep(100000);

    //    obstacleavoidance::Obstacle obs2d = {"box2d", 3.0, 4.0, 0, 0, 0.52 / 2, 0.85 / 2, 3.0, 4.0, 0.1};
    //    getObstacleAvoidance2D()->updateObstacle(obs2d);
    //    getObstacleAvoidance2D()->updateEnvironment();

    DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT"));
    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT"));
    Eigen::Vector3f initialForceLeft;
    Eigen::Vector3f initialForceRight;
    Eigen::Vector3f filteredForceLeft;
    filteredForceLeft.setZero();
    Eigen::Vector3f filteredForceRight;
    filteredForceRight.setZero();

    Eigen::Vector3f initialPositionRight;
    Eigen::Vector3f initialPositionLeft;
    Eigen::Vector3f initVec;
    IceUtil::Time start = TimeUtil::GetTime();
    double timeDuration = 0;
    while (timeDuration < in.getMinExecutionTime())
    {
        timeDuration = (TimeUtil::GetTime() - start).toSecondsDouble();
        initialPositionLeft = localRobot->getRobotNodeSet("LeftArm")->getTCP()->getPositionInRootFrame();
        initialPositionRight = localRobot->getRobotNodeSet("RightArm")->getTCP()->getPositionInRootFrame();
        initVec = initialPositionRight - initialPositionLeft;
        initVec.normalize();
    }

    DatafieldRefPtr platformVelX = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformVelocity", "velocityX"));
    DatafieldRefPtr platformVelY = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformVelocity", "velocityY"));
    DatafieldRefPtr platformRotVel = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformVelocity", "velocityRotation"));
    DatafieldRefPtr platformPosX = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformPose", "positionX"));
    DatafieldRefPtr platformPosY = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformPose", "positionY"));
    DatafieldRefPtr platformRotAng = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformPose", "rotation"));
    Eigen::Vector3f filteredPlatformTargetVelocity = Eigen::Vector3f::Zero();
    Eigen::Vector3f Kplatform = in.getKplatform()->toEigen();
    float KplatformAngle = in.getKplatformAngle();
    float maxPlatformLinearVelocity = in.getMaxPlatformLinearVelocity();
    float maxPlatformAngularVelocity = in.getMaxPlatformAngularVelocity();
    Eigen::Vector3f filteredLinearVel;
    filteredLinearVel.setZero();
    float filteredAngularVel = 0;

    Eigen::Vector3f filteredModulatedGuardVel;
    filteredModulatedGuardVel.setZero();

    IceUtil::Time last = TimeUtil::GetTime();

    Eigen::Vector3f forceVelFactor = in.getForceVelFactor()->toEigen();
    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        Eigen::Vector3f currentPositionLeft = localRobot->getRobotNodeSet("LeftArm")->getTCP()->getPositionInRootFrame();
        Eigen::Vector3f currentPositionRight = localRobot->getRobotNodeSet("RightArm")->getTCP()->getPositionInRootFrame();
        Eigen::Vector3f currentVec = currentPositionRight - currentPositionLeft;
        currentVec.normalize();

        // platform control
        Eigen::Vector3f positionDeltaRight = currentPositionRight - initialPositionRight;
        Eigen::Vector3f positionDeltaLeft = currentPositionLeft - initialPositionLeft;
        Eigen::Vector3f positionDelta = (positionDeltaLeft + positionDeltaRight) / 2;
        Eigen::Vector3f currentPlatformPosi;
        currentPlatformPosi << platformPosX->getDataField()->getFloat(), platformPosY->getDataField()->getFloat(), 0;
        Eigen::Vector3f currentVel;
        currentVel << platformVelX->getDataField()->getFloat(), platformVelY->getDataField()->getFloat(), 0;

        auto rot_matrix = ::math::Helpers::Orientation(localRobot->getGlobalPose());
        float ori = simox::math::mat3f_to_rpy(rot_matrix).z();
        if (ori > M_PI)
        {
            ori = - 2 * M_PI + ori;
        }
        Eigen::Matrix3f m;
        m = Eigen::AngleAxisf(ori, Eigen::Vector3f::UnitZ());



        float guardGMMZVel = dsController->getGMMVel();
        IceUtil::Time now = TimeUtil::GetTime();
        double deltaT = (now - last).toSecondsDouble();
        last = now;
        float Tstar = 1 / ((filterTimeConstant / deltaT) + 1);


        Eigen::Vector3f platformLinearVelocity;
        filteredAngularVel = 0;
        float angularVelocity = 0;
        float zHeight = (currentPositionLeft(2) + currentPositionRight(2)) / 2;
        if (zHeight < in.getForceActivatedZ())
        {
            if (positionDelta.norm() < minPosiTolerance)
            {
                platformLinearVelocity.setZero();
            }
            else
            {
                positionDelta = positionDelta - minPosiTolerance * positionDelta / positionDelta.norm();
                platformLinearVelocity = Kplatform.cwiseProduct(positionDelta);
            }

            if (deltaT == 0)
            {
                continue;
            }

            float cosAlpha = currentVec.dot(initVec);
            float alpha = 0;
            Eigen::Vector3f directionDiff = currentVec - initVec;
            if (directionDiff(1) < 0)
            {
                alpha = -acos(cosAlpha);
            }
            else
            {
                alpha = acos(cosAlpha);
            }
            float minAngleTolerance = in.getMinAngleTolerance();
            if (fabs(alpha) < minAngleTolerance)
            {
                angularVelocity = 0;
            }
            else
            {
                if (alpha > 0)
                {
                    alpha -= minAngleTolerance;
                }
                else
                {
                    alpha += minAngleTolerance;
                }
                angularVelocity =  KplatformAngle * alpha;
            }

            filteredAngularVel = Tstar * (angularVelocity - filteredAngularVel) + filteredAngularVel;
            if (!std::isfinite(filteredAngularVel))
            {
                filteredAngularVel = 0;
            }
            angularVelocity = math::MathUtils::LimitTo(filteredAngularVel, maxPlatformAngularVelocity);


            {
                FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
                initialForceRight = forcePtr->toRootEigen(localRobot);
            }
            {
                FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
                initialForceLeft = forcePtr->toRootEigen(localRobot);
            }

            filteredForceLeft.setZero();
            filteredForceRight.setZero();

        }
        else
        {
            Eigen::Vector3f forceLeft, forceRight;

            {
                FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
                forceRight = forcePtr->toRootEigen(localRobot) - initialForceRight;
                filteredForceRight = (1 - in.getForceFilterFactor()) * forceRight + in.getForceFilterFactor() * filteredForceRight;
            }
            {
                FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
                forceLeft = forcePtr->toRootEigen(localRobot) - initialForceLeft;
                filteredForceLeft = (1 - in.getForceFilterFactor()) * forceLeft + in.getForceFilterFactor() * filteredForceLeft;
            }

            Eigen::Vector3f filteredForce = filteredForceRight + filteredForceLeft;
            filteredForce(2) = 0;

            if (filteredForce.norm() < in.getForceDeadZone())
            {
                filteredForce.setZero();
            }
            else
            {
                filteredForce = filteredForce - in.getForceDeadZone() * filteredForce.normalized();
            }
            getDebugObserver()->setDebugDatafield("filteredForce_x", "fx", new Variant(filteredForce(0)));
            getDebugObserver()->setDebugDatafield("filteredForce_y", "fy", new Variant(filteredForce(1)));

            platformLinearVelocity = forceVelFactor.cwiseProduct(filteredForce);
            platformLinearVelocity(1) = 0;
            platformLinearVelocity(2) = 0;
        }

        float platformVelNorm = platformLinearVelocity.norm();
        Eigen::Vector3f platformGlobalLinearVelocity = m * platformLinearVelocity;
        obstacleavoidance::Agent p_agent;
        p_agent.pos = currentPlatformPosi;
        p_agent.desired_vel = platformGlobalLinearVelocity;
        p_agent.safety_margin = 0;
        Eigen::Vector3f platformModulatedLinearVel = getObstacleAvoidance()->modulateVelocity(p_agent);
        platformModulatedLinearVel = platformVelNorm * platformModulatedLinearVel.normalized();
        platformLinearVelocity = m.inverse() * platformModulatedLinearVel;

        filteredLinearVel = Tstar * (platformLinearVelocity - filteredLinearVel) + filteredLinearVel;
        if (!std::isfinite(filteredLinearVel(0)))
        {
            filteredLinearVel(0) = 0;
        }
        if (!std::isfinite(filteredLinearVel(1)))
        {
            filteredLinearVel(1) = 0;
        }
        platformLinearVelocity = math::MathUtils::LimitTo(filteredLinearVel, maxPlatformLinearVelocity);
        Eigen::Vector3f localVel = platformLinearVelocity; // m.inverse() * platformModulatedLinearVel;
        localVel(2) = angularVelocity;
        float ratio = 0.95;
        if (not std::isfinite(localVel[0]) or not std::isfinite(localVel[1])
            or not std::isfinite(angularVelocity))
        {
            ARMARX_WARNING << deactivateSpam(1) << "Some velocity target is NaN - setting to zero";
            localVel.setZero();
        }
        filteredPlatformTargetVelocity = filteredPlatformTargetVelocity * ratio + (1.0 - ratio) * localVel;
        getPlatformUnit()->move(filteredPlatformTargetVelocity(0), filteredPlatformTargetVelocity(1), filteredPlatformTargetVelocity(2));



        // obstacle avoidance for guard z direction
        Eigen::Vector3f currentGuardLocalPosi = (currentPositionLeft + currentPositionRight) / 2;
        currentGuardLocalPosi(1) += 0.6;
        Eigen::Vector3f currentGuardGlobalPosi = localRobot->toGlobalCoordinateSystemVec(currentGuardLocalPosi);
        Eigen::Vector3f currentGuardGlobalVel = m * platformLinearVelocity;
        currentGuardGlobalVel(2) = 1000 * guardGMMZVel;
        obstacleavoidance::Agent g_agent;
        g_agent.pos = currentGuardGlobalPosi;
        g_agent.desired_vel = currentGuardGlobalVel;
        g_agent.safety_margin = 0;
        Eigen::Vector3f guardModulatedLinearVel = getObstacleAvoidance()->modulateVelocity(g_agent);
        filteredModulatedGuardVel = in.getGuardVelFilter() * filteredModulatedGuardVel + (1 - in.getGuardVelFilter()) * guardModulatedLinearVel;
        ARMARX_INFO << "currentGuardGlobalVel: " << currentGuardGlobalVel << " guardModulatedLinearVel: " << filteredModulatedGuardVel;
        std::vector<float> guardObsAvoidVel;
        guardObsAvoidVel.push_back(0);
        guardObsAvoidVel.push_back(0);
        guardObsAvoidVel.push_back(filteredModulatedGuardVel(2) * 0.001 - guardGMMZVel);
        dsController->setGuardObsAvoidVel(guardObsAvoidVel);


        getDebugObserver()->setDebugDatafield("DSJointCarry_PlatformVx", "vx", new Variant(filteredPlatformTargetVelocity(0)));
        getDebugObserver()->setDebugDatafield("DSJointCarry_PlatformVy", "vy", new Variant(filteredPlatformTargetVelocity(1)));
        getDebugObserver()->setDebugDatafield("DSJointCarry_PlatformVa", "vangle", new Variant(filteredPlatformTargetVelocity(2)));

        // calculate the guard orientation (todo: replace with vision)
        Eigen::Vector3f rotAxis = initVec.cross(currentVec);
        float qw  = 1 + initVec.dot(currentVec);
        Eigen::Quaternionf quat(qw, rotAxis[0], rotAxis[1], rotAxis[2]);
        quat.normalize();
        std::vector<float> guardOri;
        guardOri.push_back(quat.w());
        guardOri.push_back(quat.x());
        guardOri.push_back(quat.y());
        guardOri.push_back(quat.z());
        dsController->setGuardOrientation(guardOri);


    }
    dsController->deactivateAndDeleteController();
    emitSuccess();
}

//void DSJointCarry::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void DSJointCarry::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr DSJointCarry::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new DSJointCarry(stateData));
}
