/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::DSControllerGroup
 * @author     JeffGao ( jianfenggaobit at gmail dot com )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ForceOffsetDetection.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <RobotAPI/libraries/core/FramedPose.h>

using namespace armarx;
using namespace DSControllerGroup;

// DO NOT EDIT NEXT LINE
ForceOffsetDetection::SubClassRegistry ForceOffsetDetection::Registry(ForceOffsetDetection::GetName(), &ForceOffsetDetection::CreateInstance);



void ForceOffsetDetection::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void ForceOffsetDetection::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    // get a private kinematic instance for this state of the robot (tick "Robot State Component" proxy checkbox in statechart group)

    DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT")); // FT L_ArmL8_Wri2
    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT")); // FT R_ArmR8_Wri2


    Eigen::Vector3f leftForceOffset;
    Eigen::Vector3f rightForceOffset;


    float forceFilterCoeff  = in.getForceFilterCoeff();
    IceUtil::Time start = TimeUtil::GetTime();


    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        FramedDirectionPtr leftForcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
        leftForceOffset = (1 - forceFilterCoeff) * leftForceOffset + forceFilterCoeff * leftForcePtr->toEigen();
        FramedDirectionPtr rightForcePtr = forceDfRight->getDataField()->get<FramedDirection>();
        rightForceOffset = (1 - forceFilterCoeff) * rightForceOffset + forceFilterCoeff * rightForcePtr->toEigen();


        IceUtil::Time now = TimeUtil::GetTime();
        if ((now - start).toSecondsDouble() > in.getTimeDuration())
        {
            break;
        }
    }

    ARMARX_IMPORTANT << "left force offset: " << leftForceOffset;
    ARMARX_IMPORTANT << "right force offset: " << rightForceOffset;


    out.setLeftForceOffset(new Vector3(leftForceOffset));
    out.setRightForceOffset(new Vector3(rightForceOffset));

    emitSuccess();
}

//void ForceOffsetDetection::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ForceOffsetDetection::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ForceOffsetDetection::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ForceOffsetDetection(stateData));
}
