#ifndef _ARMARX_XMLUSERCODE_BORARDSUPPORT_HELPERS_H
#define _ARMARX_XMLUSERCODE_BORARDSUPPORT_HELPERS_H
#include <VirtualRobot/MathTools.h>

class Helpers
{
public:

    static std::vector<float> pose2vec(const Eigen::Matrix4f& pose)
    {

        std::vector<float> result;
        result.resize(7);

        for (size_t i = 0; i < 3; ++i)
        {
            result.at(i) = pose(i, 3);
        }

        VirtualRobot::MathTools::Quaternion quat = VirtualRobot::MathTools::eigen4f2quat(pose);

        result.at(3) = quat.w;
        result.at(4) = quat.x;
        result.at(5) = quat.y;
        result.at(6) = quat.z;

        return result;

        return result;
    }

    static std::vector<double> pose2dvec(const Eigen::Matrix4f& pose)
    {

        std::vector<double> result;
        result.resize(7);

        for (size_t i = 0; i < 3; ++i)
        {
            result.at(i) = pose(i, 3);
        }

        VirtualRobot::MathTools::Quaternion quat = VirtualRobot::MathTools::eigen4f2quat(pose);

        result.at(3) = quat.w;
        result.at(4) = quat.x;
        result.at(5) = quat.y;
        result.at(6) = quat.z;

        return result;

        return result;
    }

};

#endif
