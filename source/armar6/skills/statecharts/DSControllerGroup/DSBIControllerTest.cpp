/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    DSController::DSControllerGroup
 * @author     JeffGao ( jianfenggaobit at gmail dot com )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DSBIControllerTest.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
//#include <armarx/control/ds_controller/DSControllerInterface.h>
#include <RobotAPI/interface/units/RobotUnit/DSControllerBase.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <armar6/skills/statecharts/DSControllerGroup/DSControllerGroupStatechartContext.generated.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>

using namespace armarx;
using namespace DSControllerGroup;

// DO NOT EDIT NEXT LINE
DSBIControllerTest::SubClassRegistry DSBIControllerTest::Registry(DSBIControllerTest::GetName(), &DSBIControllerTest::CreateInstance);



void DSBIControllerTest::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void DSBIControllerTest::run()
{
    //    getRobotUnit()->loadLibFromPackage("RobotAPI", "DSControllers");

    float posiKp = 5;
    float v_max = 0.15;
    std::vector<float> posiDamping = in.getDamping();
    float couplingStiffness = in.getCouplingStiffness();
    float couplingForceLimit = in.getCouplingForceLimit();
    float forwardGain = in.getForwardGain();
    float oriDamping = in.getOriDamping();
    float oriKp = in.getOriKp();
    float filterTimeConstant = in.getFilterTimeConstant();
    float torqueLimit = 20;
    float nullTorqueLimit = in.getNullTorqueLimit();
    float contactForce = in.getContactForce();
    float guardTargetZDown = in.getGuardTargetZDown();
    float guardTargetZUp = in.getGuardTargetZUp();
    float loweringForce = in.getLoweringForce();
    float liftingForce = in.getLiftingForce();
    bool guardDesiredDirection = in.getIsGuardLifting();
    float highPassFilterFactor = in.getHighPassFilterFactor();
    std::vector<float> left_desiredQuaternion = in.getLeftDesiredQuaternion();
    std::vector<float> right_desiredQuaternion =  in.getRightDesiredQuaternion();;
    std::vector<float> leftarm_qnullspaceVec = in.getLeftnullspaceVec();
    std::vector<float> rightarm_qnullspaceVec = in.getRightnullspaceVec();
    float nullspaceKp = in.getNullSpaceKp();
    float nullspaceDamping = in.getNullSpaceDamping();
    std::string gmmParamsFile = in.getGmmfile();
    float positionErrorTolerance = in.getPositionErrorTolerance();
    std::vector<float> left_oriUp;
    std::vector<float> right_oriUp;
    std::vector<float> left_oriDown;
    std::vector<float> right_oriDown;

    {
        QuaternionPtr q = in.getLeftOriUp();
        left_oriUp.push_back(q->qw);
        left_oriUp.push_back(q->qx);
        left_oriUp.push_back(q->qy);
        left_oriUp.push_back(q->qz);
    }

    {
        QuaternionPtr q = in.getLeftOriDown();
        left_oriDown.push_back(q->qw);
        left_oriDown.push_back(q->qx);
        left_oriDown.push_back(q->qy);
        left_oriDown.push_back(q->qz);
    }
    {
        QuaternionPtr q = in.getRightOriUp();
        right_oriUp.push_back(q->qw);
        right_oriUp.push_back(q->qx);
        right_oriUp.push_back(q->qy);
        right_oriUp.push_back(q->qz);
    }
    {
        QuaternionPtr q = in.getRightOriDown();
        right_oriDown.push_back(q->qw);
        right_oriDown.push_back(q->qx);
        right_oriDown.push_back(q->qy);
        right_oriDown.push_back(q->qz);
    }

    float forceFilterCoeff = in.getForceFilterCoeff();

    //    // add remote gui
    //    RemoteGui::detail::VBoxLayoutBuilder rootLayoutBuilder = RemoteGui::makeVBoxLayout();
    //    rootLayoutBuilder
    //            .addChild(RemoteGui::makeLabel("message"))
    //            .addChild(RemoteGui::makeLabel("status").value("status"))
    //            .addChild(RemoteGui::makeButton("ok").label("Ok"))
    //            .addChild(new RemoteGui::VSpacer());

    //    getRemoteGui()->createTab("RemoteTeachIn", rootLayoutBuilder);
    //    RemoteGui::TabProxy guiTab = RemoteGui::TabProxy(getRemoteGui(), "RemoteTeachIn");


    std::vector<float> forceLeftOffset;
    Eigen::Vector3f flv = in.getForceLeftOffset()->toEigen();
    forceLeftOffset.push_back(flv(0));
    forceLeftOffset.push_back(flv(1));
    forceLeftOffset.push_back(flv(2));

    std::vector<float> forceRightOffset;
    Eigen::Vector3f frv = in.getForceRightOffset()->toEigen();
    forceRightOffset.push_back(frv(0));
    forceRightOffset.push_back(frv(1));
    forceRightOffset.push_back(frv(2));

    DSRTBimanualControllerConfigPtr config = new DSRTBimanualControllerConfig(posiKp,
            v_max,
            posiDamping,
            couplingStiffness,
            couplingForceLimit,
            forwardGain,
            oriDamping,
            oriKp,
            filterTimeConstant,
            torqueLimit,
            nullTorqueLimit,
            left_desiredQuaternion,
            right_desiredQuaternion,
            leftarm_qnullspaceVec,
            rightarm_qnullspaceVec,
            nullspaceKp,
            nullspaceDamping,
            gmmParamsFile,
            positionErrorTolerance,
            contactForce,
            guardTargetZUp,
            guardTargetZDown,
            loweringForce,
            liftingForce,
            guardDesiredDirection,
            highPassFilterFactor,
            left_oriUp,
            left_oriDown,
            right_oriUp,
            right_oriDown, forceFilterCoeff,
            in.getForceErrorZDisturbance(),
            in.getForceErrorZThreshold(),
            in.getForceErrorZGain(),
            in.getTorqueDisturbance(),
            in.getTorqueFilterConstant(),
            in.getLeftForceOffsetZ(),
            in.getRightForceOffsetZ(),
            in.getContactDistanceTolerance(),
            in.getMountingDistanceTolerance(),
            in.getMountingCorrectionFilterFactor(),
            forceLeftOffset, forceRightOffset);

    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx leftHandcontroller = StateBase::getContext<DSControllerGroupStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_L_EEF_NJointKITHandV2ShapeController");
    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx rightHandcontroller = StateBase::getContext<DSControllerGroupStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_R_EEF_NJointKITHandV2ShapeController");

    //    leftHandcontroller->setTargets(0, 0);
    //    rightHandcontroller->setTargets(0, 0);


    DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT")); // FT L_ArmL8_Wri2
    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT")); // FTRL_ArmR8_Wri2
    Eigen::Vector3f initialForceLeft;
    Eigen::Vector3f initialForceRight;

    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());

    DSBimanualControllerInterfacePrx dsController
        = DSBimanualControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("DSRTBimanualController", "dsController", config));

    dsController->activateController();
    IceUtil::Time start = TimeUtil::GetTime();

    bool isLiftingTargetReached = false;
    //    bool isAnswered = false;

    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

        // force detection
        IceUtil::Time now = TimeUtil::GetTime();
        float t = (now - start).toSecondsDouble();

        if (t < in.getMinExecutionTime())
        {
            {
                FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
                initialForceRight = forcePtr->toRootEigen(localRobot);
            }
            {
                FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
                initialForceLeft = forcePtr->toRootEigen(localRobot);
            }
            continue;
        }
        Eigen::Vector3f forceLeft;
        Eigen::Vector3f origForceLeft;
        {
            FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
            origForceLeft = forcePtr->toRootEigen(localRobot);
            forceLeft = origForceLeft - initialForceLeft;
        }

        Eigen::Vector3f forceRight;
        Eigen::Vector3f origForceRight;
        {
            FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
            origForceRight = forcePtr->toRootEigen(localRobot);
            forceRight = origForceRight - initialForceRight;
        }

//        float forcelefty = forceLeft(1);
//        float forcerighty = forceRight(1);
//        bool closeHandTrigger = (forcelefty < in.getCloseHandForceThreshold()) && (forcerighty < in.getCloseHandForceThreshold());
        float zright = localRobot->getRobotNodeSet("RightArm")->getTCP()->getPositionInRootFrame()[2];
        float zleft = localRobot->getRobotNodeSet("LeftArm")->getTCP()->getPositionInRootFrame()[2];
        bool isLoweringTargetReached = ((zright + zleft) / 2) <= in.getGuardTargetZDown() * 1000;

        if (!in.getIsGuardLifting() &&  isLoweringTargetReached)
        {
            leftHandcontroller->activateController();
            rightHandcontroller->activateController();

            leftHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
            rightHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
            break;
        }

        if ((zright + zleft) / 2 >= (in.getGuardTargetZUp() - 0.25) * 1000)
        {
            isLiftingTargetReached = true;
        }

        //        ARMARX_IMPORTANT << "isLiftingTargetReached: " << isLiftingTargetReached;
        if (in.getIsGuardLifting() && isLiftingTargetReached)
        {
            //            ARMARX_IMPORTANT << "current: " << (zright + zleft) / 2;
            //            ARMARX_IMPORTANT << "target: " << (in.getGuardTargetZDown() + in.getGuardTargetZUp()) / 2;

            auto recognizedTextResponse = getSpeechObserver()->getDatafieldByName("SpeechToText", "RecognizedText");
            long recognizedTimestamp = recognizedTextResponse->getTimestamp();
            std::string response = recognizedTextResponse->getString();
            if (Contains(response, "thank", true) && (TimeUtil::GetTime() - IceUtil::Time::microSeconds(recognizedTimestamp)).toSecondsDouble() < 5)
            {
                //                if (!isAnswered)
                //                {
                //                    getTextToSpeech()->reportText(in.getTextForFinishGuardDemo());
                //                    isAnswered = true;
                //                }
                dsController->setToDefaultTarget();
            }

            //            if ((zright + zleft) / 2 < 1000 * (in.getGuardTargetZUp() - in.getMountingDistanceTolerance()))
            //            {
            //                dsController->setToDefaultTarget();
            //            }
            if (isLoweringTargetReached)
            {
                break;
            }
        }


    }
    dsController->deactivateAndDeleteController();
    emitSuccess();
    //    dsController->deactivateController();
    //    while (dsController->isControllerActive())
    //    {
    //        usleep(10000);
    //    }
    //    dsController->deleteController();
}

//void DSBIControllerTest::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void DSBIControllerTest::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr DSBIControllerTest::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new DSBIControllerTest(stateData));
}
