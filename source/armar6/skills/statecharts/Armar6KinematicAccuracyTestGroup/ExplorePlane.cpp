/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6KinematicAccuracyTestGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ExplorePlane.h"

#include <RobotAPI/libraries/RobotStatechartHelpers/ForceTorqueHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/PositionControllerHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>

#include <ArmarXCore/core/time/CycleUtil.h>

#include <VirtualRobot/math/FitPlane.h>
#include <VirtualRobot/math/Helpers.h>

#include <ArmarXCore/core/csv/CsvWriter.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6KinematicAccuracyTestGroup;

// DO NOT EDIT NEXT LINE
ExplorePlane::SubClassRegistry ExplorePlane::Registry(ExplorePlane::GetName(), &ExplorePlane::CreateInstance);

enum class PhaseType
{
    ApproachPrePose,
    InitialApproach,
    Approach,
    MinimizeForceRetract,
    Retract,
    FinalRetract
};

void ExplorePlane::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void ExplorePlane::run()
{

    std::string selectedArm = in.getSelectedArm();
    if (selectedArm != "Right" && selectedArm != "Left")
    {
        throw LocalException("SelectedArm has to be either 'Right' or 'Left'.");
    }





    std::string nodeSetName = in.getNodeSetName().at(selectedArm);
    std::string FTDatefieldName = in.getFTDatefieldName().at(selectedArm);

    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(nodeSetName);
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();

    VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(getRobotUnit(), nodeSetName, in.getCartesianVelocityControllerName()));

    ForceTorqueHelper ftHelper(getForceTorqueObserver(), FTDatefieldName);
    PositionControllerHelper posHelper(tcp, velHelper, Eigen::Matrix4f::Zero());
    posHelper.thresholdOrientationNear = in.getThresholdOrientationNear();
    posHelper.thresholdOrientationReached = in.getThresholdOrientationReached();
    posHelper.thresholdPositionNear = in.getThresholdPositionNear();
    posHelper.thresholdPositionReached = in.getThresholdPositionReached();

    posHelper.posController.maxOriVel = in.getMaxOriVel();
    posHelper.posController.maxPosVel = in.getMaxPosVel();
    posHelper.posController.KpOri = in.getKpOrientation();
    posHelper.posController.KpPos = in.getKpPosition();

    PhaseType currentPhase = PhaseType::ApproachPrePose;

    Eigen::Vector3f GridReferencePos = in.getGridReferencePos()->toEigen();

    std::vector<std::string> csvHeader = {"x", "y", "z", "qw", "qx", "qy", "qz"};
    for (const VirtualRobot::RobotNodePtr& rn : rns->getAllRobotNodes())
    {
        csvHeader.push_back(rn->getName());
    }
    CsvWriter writer("contacts.csv", csvHeader, false);

    Eigen::Matrix3f referenceOri = in.getGridReferenceOri()->toEigen();
    std::vector<Eigen::Matrix3f> orientations;
    orientations.push_back(referenceOri);
    for (Vector3Ptr relOriPtr : in.getGridRelativeOrientations())
    {
        Eigen::Vector3f relOri = relOriPtr->toEigen();
        Eigen::AngleAxisf aa(relOri.norm(), relOri.normalized());
        orientations.push_back(Eigen::Matrix3f(aa.toRotationMatrix() * referenceOri));
    }

    int gridIndex1 = 0;
    int gridIndex2 = 0;
    unsigned int oriIndex = 0;

    auto calcTarget = [&](Eigen::Vector3f offset)
    {
        Eigen::Vector3f pos = GridReferencePos + gridIndex1 * in.getGridVec1()->toEigen() + gridIndex2 * in.getGridVec2()->toEigen() + offset;
        Eigen::Matrix3f ori = orientations.at(oriIndex);

        return math::Helpers::CreatePose(pos, ori);
    };
    auto nextTarget = [&]()
    {
        oriIndex++;
        if (oriIndex >= orientations.size())
        {
            oriIndex = 0;
            gridIndex1++;
        }
        if (gridIndex1 > in.getGridSteps1())
        {
            gridIndex1 = 0;
            gridIndex2++;
        }
    };

    auto isLastTarget = [&]()
    {
        return gridIndex1 == in.getGridSteps1() && gridIndex2 == in.getGridSteps2() && oriIndex + 1 == (orientations.size());
    };

    auto setJointLimitAvoidanceEnabled = [&](bool enabled)
    {
        velHelper->controller->setKpJointLimitAvoidance(enabled ? 1 : 0);
        velHelper->controller->setJointLimitAvoidanceScale(enabled ? 2 : 1);
    };

    posHelper.setNewWaypoints(in.getApproachWaypoints());
    posHelper.addWaypoint(calcTarget(Eigen::Vector3f::Zero()));

    std::vector<Eigen::Vector3f> contactPositions;

    ARMARX_IMPORTANT << "before loop";

    CycleUtil c(10);
    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        PhaseType nextPhase = currentPhase;

        posHelper.update();
        bool targetReached = posHelper.isFinalTargetReached();

        const Eigen::Vector3f currentForce = ftHelper.getForce();
        const Eigen::Vector3f currentTorque = ftHelper.getTorque();

        getDebugObserver()->setDebugChannel("ExploreNextTarget",
        {
            { "force_x", new Variant(currentForce(0))},
            { "force_y", new Variant(currentForce(1))},
            { "force_z", new Variant(currentForce(2))},
            { "torque_x", new Variant(currentTorque(0))},
            { "torque_y", new Variant(currentTorque(1))},
            { "torque_z", new Variant(currentTorque(2))},
            { "force", new Variant(currentForce.norm())},
            { "torque", new Variant(currentTorque.norm())},
            { "positionError", new Variant(posHelper.getPositionError())},
            { "orientationError", new Variant(posHelper.getOrientationError())},
        });


        bool forceExceeded = currentForce.norm() > in.getForceThreshold();
        bool retractForceExceeded = currentForce.norm() > in.getRetractForceThreshold();

        ARMARX_INFO << deactivateSpam(0.5) << "currentForce: " << currentForce.norm();

        if (in.getIsSimulation() && (currentPhase == PhaseType::Approach || currentPhase == PhaseType::InitialApproach || currentPhase == PhaseType::MinimizeForceRetract))
        {
            float dist = (tcp->getPositionInRootFrame() - calcTarget(Eigen::Vector3f::Zero()).block<3, 1>(0, 3)).norm();
            forceExceeded = dist > 50;
            retractForceExceeded = dist > 48;
        }


        if (currentPhase == PhaseType::ApproachPrePose)
        {
            if (targetReached)
            {
                ARMARX_IMPORTANT << "PrePose reached";
                posHelper.setTarget(calcTarget(in.getApproachVec()->toEigen()));
                nextPhase = PhaseType::InitialApproach;
                ftHelper.setZero();
                setJointLimitAvoidanceEnabled(false);
            }
        }
        if (currentPhase == PhaseType::Approach || currentPhase == PhaseType::InitialApproach)
        {
            if (targetReached)
            {
                ARMARX_IMPORTANT << "targetReached, expecting contact";
                velHelper->controller->immediateHardStop();
                emitFailure();
                break;
            }
            if (forceExceeded)
            {
                if (currentPhase == PhaseType::InitialApproach)
                {
                    GridReferencePos = tcp->getPositionInRootFrame() + in.getRetractVec()->toEigen();
                }


                posHelper.setTarget(calcTarget(Eigen::Vector3f::Zero()));
                if (currentPhase == PhaseType::InitialApproach)
                {
                    nextPhase = PhaseType::Retract;
                    posHelper.posController.maxPosVel = in.getMaxPosVel();
                }
                if (currentPhase == PhaseType::Approach)
                {
                    nextPhase = PhaseType::MinimizeForceRetract;
                    posHelper.posController.maxPosVel = in.getMaxPosVelMinimizeForce();
                }

            }
        }
        if (currentPhase == PhaseType::MinimizeForceRetract)
        {
            if (!retractForceExceeded)
            {
                ARMARX_INFO << "Force Minimize threshold deceeded: " << currentForce.norm();

                std::stringstream ss;
                ss <<  "contact_" << oriIndex << "_" << gridIndex1 << "_" << gridIndex2;
                getDebugDrawerTopic()->setSphereVisu("ExploreNextTarget", ss.str(), new Vector3(tcp->getGlobalPose()), DrawColor {1, 0, 0, 1}, 5);

                Eigen::Vector3f tcpPos = tcp->getPositionInRootFrame();
                contactPositions.push_back(tcpPos);
                Eigen::Quaternionf tcpQuat(tcp->getPoseInRootFrame().block<3, 3>(0, 0));
                std::vector<float> csvRow = {tcpPos(0), tcpPos(1), tcpPos(2), tcpQuat.w(), tcpQuat.x(), tcpQuat.y(), tcpQuat.z()};
                for (const VirtualRobot::RobotNodePtr& rn : rns->getAllRobotNodes())
                {
                    csvRow.push_back(rn->getJointValue());
                }
                writer.write(csvRow);

                if (gridIndex2 > 0)
                {
                    auto plane = math::FitPlane::Fit(contactPositions);
                    Eigen::VectorXf distances(contactPositions.size());
                    int i = 0;
                    for (auto& p : contactPositions)
                    {
                        distances(i) = plane.GetDistance(p, math::AbstractFunctionR2R3::SimpleProjection);
                        ARMARX_INFO << "distance: " << distances(i) << " " << VAROUT(p);
                        i++;
                    }
                    ARMARX_INFO << "Average distance: " << distances.mean();
                    auto rootName = getRobot()->getRootNode()->getName();
                    plane = plane.Transform(getRobot()->getGlobalPose());
                    PolygonPointList points;
                    float size = 1000;
                    points.emplace_back(new Vector3 {plane.GetPoint(size, size)});
                    points.emplace_back(new Vector3 {plane.GetPoint(-size, size)});
                    points.emplace_back(new Vector3 {plane.GetPoint(-size, -size)});
                    points.emplace_back(new Vector3 {plane.GetPoint(size, -size)});
                    DrawColor clr {0, 150.f / 255.f, 130.f / 255.f, 0.3};
                    getDebugDrawerTopic()->setPolygonVisu("ExploreNextTarget", "plane", points, clr, clr, 1);

                }

                if (isLastTarget())
                {
                    posHelper.setTarget(calcTarget(Eigen::Vector3f::Zero()));
                    nextPhase = PhaseType::FinalRetract;
                }
                else
                {
                    nextTarget();
                    posHelper.setTarget(calcTarget(Eigen::Vector3f::Zero()));

                    nextPhase = PhaseType::Retract;
                    posHelper.posController.maxPosVel = in.getMaxPosVel();
                    setJointLimitAvoidanceEnabled(true);
                }

            }
        }
        if (currentPhase == PhaseType::Retract)
        {
            if (targetReached)
            {
                posHelper.setTarget(calcTarget(in.getApproachVec()->toEigen()));
                nextPhase = PhaseType::Approach;
                posHelper.posController.maxPosVel = in.getMaxPosVelApproach();
                ftHelper.setZero();
                setJointLimitAvoidanceEnabled(false);
            }
        }
        if (currentPhase == PhaseType::FinalRetract)
        {
            if (targetReached)
            {
                emitSuccess();
                break;
            }
        }

        currentPhase = nextPhase;
        c.waitForCycleDuration();
    }

    writer.close();

    velHelper->cleanup();
}

//void ExplorePlane::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ExplorePlane::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ExplorePlane::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ExplorePlane(stateData));
}
