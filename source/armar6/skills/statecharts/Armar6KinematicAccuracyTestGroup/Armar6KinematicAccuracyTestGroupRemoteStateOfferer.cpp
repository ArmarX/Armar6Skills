/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6KinematicAccuracyTestGroup::Armar6KinematicAccuracyTestGroupRemoteStateOfferer
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6KinematicAccuracyTestGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6KinematicAccuracyTestGroup;

// DO NOT EDIT NEXT LINE
Armar6KinematicAccuracyTestGroupRemoteStateOfferer::SubClassRegistry Armar6KinematicAccuracyTestGroupRemoteStateOfferer::Registry(Armar6KinematicAccuracyTestGroupRemoteStateOfferer::GetName(), &Armar6KinematicAccuracyTestGroupRemoteStateOfferer::CreateInstance);



Armar6KinematicAccuracyTestGroupRemoteStateOfferer::Armar6KinematicAccuracyTestGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6KinematicAccuracyTestGroupStatechartContext > (reader)
{
}

void Armar6KinematicAccuracyTestGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6KinematicAccuracyTestGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6KinematicAccuracyTestGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6KinematicAccuracyTestGroupRemoteStateOfferer::GetName()
{
    return "Armar6KinematicAccuracyTestGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6KinematicAccuracyTestGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6KinematicAccuracyTestGroupRemoteStateOfferer(reader));
}
