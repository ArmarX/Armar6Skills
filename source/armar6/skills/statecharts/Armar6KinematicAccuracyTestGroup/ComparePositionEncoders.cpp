/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6KinematicAccuracyTestGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ComparePositionEncoders.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/core/time/CycleUtil.h>

#include <VirtualRobot/math/Helpers.h>

#include <ArmarXCore/observers/filters/rtfilters/MedianFilter.h>

using namespace armarx;
using namespace Armar6KinematicAccuracyTestGroup;

// DO NOT EDIT NEXT LINE
ComparePositionEncoders::SubClassRegistry ComparePositionEncoders::Registry(ComparePositionEncoders::GetName(), &ComparePositionEncoders::CreateInstance);



void ComparePositionEncoders::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void ComparePositionEncoders::run()
{
    std::string jointName = in.getJointName();
    DatafieldRefPtr posDF = DatafieldRefPtr::dynamicCast(getRobotUnitObserver()->getDatafieldRefByName("SensorDevices", jointName + "_SensorValueArmar6Actuator_position"));
    DatafieldRefPtr relPosDF = DatafieldRefPtr::dynamicCast(getRobotUnitObserver()->getDatafieldRefByName("SensorDevices", jointName + "_SensorValueArmar6Actuator_relativePosition"));
    DatafieldRefPtr torqueDF = DatafieldRefPtr::dynamicCast(getRobotUnitObserver()->getDatafieldRefByName("SensorDevices", jointName + "_SensorValueArmar6Actuator_torque"));



    int rotationSign = in.getRotationSign();

    float relPosOffset = rotationSign * relPosDF->get<float>() - posDF->get<float>();

    rtfilters::MedianFilter diffFiltered(3);

    CycleUtil c(10);
    while (!isRunningTaskStopped())
    {
        float pos = posDF->get<float>();
        float relPos = rotationSign * relPosDF->get<float>() - relPosOffset;
        float diff = math::Helpers::AngleModPI(pos - relPos);
        float diffFiltVal = diffFiltered.update(TimeUtil::GetTime(), diff);

        getDebugObserver()->setDebugDatafield("ComparePositionEncoders", "pos", new Variant(pos));
        getDebugObserver()->setDebugDatafield("ComparePositionEncoders", "relPos", new Variant(relPos));
        getDebugObserver()->setDebugDatafield("ComparePositionEncoders", "diff", new Variant(diff));
        getDebugObserver()->setDebugDatafield("ComparePositionEncoders", "diffFilt", new Variant(diffFiltVal));
        getDebugObserver()->setDebugDatafield("ComparePositionEncoders", "torqueDF", new Variant(torqueDF->get<float>()));



        c.waitForCycleDuration();
    }
}

//void ComparePositionEncoders::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ComparePositionEncoders::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ComparePositionEncoders::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ComparePositionEncoders(stateData));
}
