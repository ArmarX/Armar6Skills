/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6Demo
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DoubleCircleMotion.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/core/util/StringHelpers.h>

#include <VirtualRobot/MathTools.h>

using namespace armarx;
using namespace Armar6Demo;

// DO NOT EDIT NEXT LINE
DoubleCircleMotion::SubClassRegistry DoubleCircleMotion::Registry(DoubleCircleMotion::GetName(), &DoubleCircleMotion::CreateInstance);



void DoubleCircleMotion::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void DoubleCircleMotion::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true
    getTcpControlUnit()->request();
    float phaseStartLeft = in.getPhaseStartLeft();
    float phaseStartRight = in.getPhaseStartRight();
    float secondsPerCircle = in.getSecondsPerCircle();
    float circleSize = in.getCircleSize();
    auto leftCircleCenter = in.getStartPositionLeft()->toEigen();
    auto rightCircleCenter = in.getStartPositionRight()->toEigen();
    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    auto tcpLeft = localRobot->getRobotNodeSet(in.getNodeSetLeft())->getTCP();
    auto tcpRight = localRobot->getRobotNodeSet(in.getNodeSetRight())->getTCP();
    float p = in.getP();
    IceUtil::Time startTime = TimeUtil::GetTime();
    int currentAngle = phaseStartLeft * 180.0 / M_PI;
    armarx::CycleUtil c(10);
    Eigen::Matrix4f goalOri = Eigen::Matrix4f::Identity();
    goalOri.block<3, 3>(0, 0) = in.getLeftTCPOrientation()->toRootEigen(localRobot);
    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        {
            double currentPhaseStateLeft = phaseStartLeft + (TimeUtil::GetTime() - startTime).toSecondsDouble() / secondsPerCircle * M_PI * 2;
            // do your calculations
            double x = cos(currentPhaseStateLeft) * circleSize;
            double z = sin(currentPhaseStateLeft) * circleSize;
            ARMARX_INFO << deactivateSpam(1) << VAROUT(currentPhaseStateLeft);
            Eigen::Vector3f currentGoalEigen = leftCircleCenter + Eigen::Vector3f(x, 0, z);
            Eigen::Vector3f currentDeltaToGoal = (currentGoalEigen - tcpLeft->getPositionInRootFrame()) * p;
            Eigen::Matrix4f deltaOri = goalOri * tcpLeft->getGlobalPose().inverse();
            Eigen::Vector3f orientationDelta;
            VirtualRobot::MathTools::eigen4f2rpy(deltaOri, orientationDelta);
            FramedDirectionPtr orientationVel = new FramedDirection(orientationDelta * p, localRobot->getRootNode()->getName(), localRobot->getName());


            currentAngle = currentPhaseStateLeft * 180.0 / M_PI;
            //        FramedPositionPtr currentGoal = new FramedPosition(, in.getStartPositionLeft()->getFrame(), in.getStartPositionLeft()->agent);
            getDebugDrawerTopic()->setSphereVisu("Armar6Demo", "GoalSphere" + to_string(currentAngle % 180), FramedPosition(currentGoalEigen, in.getStartPositionLeft()->getFrame(), in.getStartPositionLeft()->agent).toGlobal(localRobot), DrawColor{1, 0, 0, 0.5}, 10);
            getDebugDrawerTopic()->setSphereVisu("Armar6Demo", "CurrentPos", new Vector3(Eigen::Vector3f(tcpLeft->getGlobalPose().block<3, 1>(0, 3))), DrawColor{0, 1, 0, 0.5}, 13);
            getTcpControlUnit()->setTCPVelocity(in.getNodeSetLeft(), tcpLeft->getName(), new FramedDirection(currentDeltaToGoal, in.getStartPositionLeft()->getFrame(), in.getStartPositionLeft()->agent), orientationVel);
        }
        if (in.getUseRightArm())
        {
            double currentPhaseStateRight = phaseStartRight + (TimeUtil::GetTime() - startTime).toSecondsDouble() / secondsPerCircle * M_PI * 2;
            double x = cos(currentPhaseStateRight) * circleSize;
            double z = sin(currentPhaseStateRight) * circleSize;
            //            ARMARX_INFO << deactivateSpam(1) << VAROUT(currentPhaseStateRight);
            Eigen::Vector3f currentGoalEigen = rightCircleCenter + Eigen::Vector3f(x, 0, z);
            Eigen::Vector3f currentDeltaToGoal = (currentGoalEigen - tcpRight->getPositionInRootFrame()) * p;

            Eigen::Matrix4f deltaOri = goalOri * tcpRight->getGlobalPose().inverse();
            Eigen::Vector3f orientationDelta;
            VirtualRobot::MathTools::eigen4f2rpy(deltaOri, orientationDelta);
            FramedDirectionPtr orientationVel = new FramedDirection(orientationDelta * p, localRobot->getRootNode()->getName(), localRobot->getName());

            currentAngle = currentPhaseStateRight * 180.0 / M_PI;
            //        FramedPositionPtr currentGoal = new FramedPosition(, in.getStartPositionRight()->getFrame(), in.getStartPositionRight()->agent);
            getDebugDrawerTopic()->setSphereVisu("Armar6Demo", "GoalSphereRight" + to_string(currentAngle % 180), FramedPosition(currentGoalEigen, in.getStartPositionRight()->getFrame(), in.getStartPositionRight()->agent).toGlobal(localRobot), DrawColor{1, 0, 0, 0.5}, 10);
            getDebugDrawerTopic()->setSphereVisu("Armar6Demo", "CurrentPosRight", new Vector3(Eigen::Vector3f(tcpRight->getGlobalPose().block<3, 1>(0, 3))), DrawColor{0, 1, 0, 0.5}, 13);
            getTcpControlUnit()->setTCPVelocity(in.getNodeSetRight(), tcpRight->getName(), new FramedDirection(currentDeltaToGoal, in.getStartPositionRight()->getFrame(), in.getStartPositionRight()->agent), orientationVel);
        }
        c.waitForCycleDuration();

    }
}

//void DoubleCircleMotion::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void DoubleCircleMotion::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
    getTcpControlUnit()->release();
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr DoubleCircleMotion::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new DoubleCircleMotion(stateData));
}
