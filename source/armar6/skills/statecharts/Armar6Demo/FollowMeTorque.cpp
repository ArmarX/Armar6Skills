/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6Demo
 * @author     Armar6Demo ( Armar6Demo at h2t dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "FollowMeTorque.h"
#include <RobotAPI/libraries/core/math/MathUtils.h>


//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6Demo;

// DO NOT EDIT NEXT LINE
FollowMeTorque::SubClassRegistry FollowMeTorque::Registry(FollowMeTorque::GetName(), &FollowMeTorque::CreateInstance);



void FollowMeTorque::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void FollowMeTorque::run()
{
    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    float Kp = in.getKp();
    float maxVelocity = in.getMaxVelocity();
    float KpAngle = in.getKp();
    float maxAngularVelocity = in.getMaxAngularVelocity();
    VirtualRobot::RobotNodePtr tcp = localRobot->getRobotNodeSet(in.getNodeSetName())->getTCP();

    Eigen::Vector3f initialPosition = tcp->getPositionInRootFrame();
    Eigen::Matrix3f initialOrientation = tcp->getPoseInRootFrame().block<3, 3>(0, 0);



    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        Eigen::Vector3f currentPosition = tcp->getPositionInRootFrame();
        Eigen::Matrix3f currentOrientation = tcp->getPoseInRootFrame().block<3, 3>(0, 0);
        Eigen::Vector3f positionDelta = currentPosition - initialPosition;
        Eigen::Vector3f velocity = positionDelta * Kp;
        velocity = math::MathUtils::LimitTo(velocity, maxVelocity);

        Eigen::Matrix3f orientationDelta = initialOrientation.inverse() * currentOrientation;
        Eigen::AngleAxisf aa = Eigen::AngleAxisf(orientationDelta);
        float rotationZ = aa.angle() * aa.axis()(2);
        ARMARX_IMPORTANT << VAROUT(rotationZ) << VAROUT(aa.axis()) << VAROUT(aa.angle());

        getDebugObserver()->setDebugDatafield("FollowMeTorque", "axis_x", new Variant(aa.axis()(0)));
        getDebugObserver()->setDebugDatafield("FollowMeTorque", "axis_y", new Variant(aa.axis()(1)));
        getDebugObserver()->setDebugDatafield("FollowMeTorque", "axis_z", new Variant(aa.axis()(2)));
        getDebugObserver()->setDebugDatafield("FollowMeTorque", "angle", new Variant(aa.angle()));

        float angularVelocity = rotationZ * KpAngle;
        angularVelocity = math::MathUtils::LimitTo(angularVelocity, maxAngularVelocity);

        getPlatformUnit()->move(velocity(0), velocity(1), angularVelocity);
        usleep(10);
    }
    getPlatformUnit()->move(0, 0, 0);
}

//void FollowMeTorque::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void FollowMeTorque::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr FollowMeTorque::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new FollowMeTorque(stateData));
}
