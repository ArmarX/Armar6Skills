/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6Demo::Armar6DemoRemoteStateOfferer
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6DemoRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6Demo;

// DO NOT EDIT NEXT LINE
Armar6DemoRemoteStateOfferer::SubClassRegistry Armar6DemoRemoteStateOfferer::Registry(Armar6DemoRemoteStateOfferer::GetName(), &Armar6DemoRemoteStateOfferer::CreateInstance);



Armar6DemoRemoteStateOfferer::Armar6DemoRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6DemoStatechartContext > (reader)
{
}

void Armar6DemoRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6DemoRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6DemoRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6DemoRemoteStateOfferer::GetName()
{
    return "Armar6DemoRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6DemoRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6DemoRemoteStateOfferer(reader));
}
