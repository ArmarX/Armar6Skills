/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GuardSupportDemoGroup::Armar6GuardSupportDemoGroupRemoteStateOfferer
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6GuardSupportDemoGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6GuardSupportDemoGroup;

// DO NOT EDIT NEXT LINE
Armar6GuardSupportDemoGroupRemoteStateOfferer::SubClassRegistry Armar6GuardSupportDemoGroupRemoteStateOfferer::Registry(Armar6GuardSupportDemoGroupRemoteStateOfferer::GetName(), &Armar6GuardSupportDemoGroupRemoteStateOfferer::CreateInstance);



Armar6GuardSupportDemoGroupRemoteStateOfferer::Armar6GuardSupportDemoGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6GuardSupportDemoGroupStatechartContext > (reader)
{
}

void Armar6GuardSupportDemoGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6GuardSupportDemoGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6GuardSupportDemoGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6GuardSupportDemoGroupRemoteStateOfferer::GetName()
{
    return "Armar6GuardSupportDemoGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6GuardSupportDemoGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6GuardSupportDemoGroupRemoteStateOfferer(reader));
}
