/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GuardSupportDemoGroup
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "WaitForGuardSupportGesture.h"

#include <ArmarXCore/core/time/CycleUtil.h>


using namespace armarx;
using namespace Armar6GuardSupportDemoGroup;

// DO NOT EDIT NEXT LINE
WaitForGuardSupportGesture::SubClassRegistry WaitForGuardSupportGesture::Registry(WaitForGuardSupportGesture::GetName(), &WaitForGuardSupportGesture::CreateInstance);

static const std::string GUARD_SUPPORT = "guardsupport";

void WaitForGuardSupportGesture::onEnter()
{
    setTimeoutEvent(in.getTimeout(), createEventTimeout());
}

void WaitForGuardSupportGesture::run()
{
    //armarx::PoseBasedActionRecognitionInterfacePrx const& actionRecognition = getPoseBasedActionRecognition();

#if 0
    int detectionBufferSize = in.getDetectionBufferSize();
    boost::circular_buffer<bool> buffer(detectionBufferSize);
    float detectionThreshold = in.getDetectionThreshold();

    CycleUtil c(in.getCycleTimeMs());
    long lastTimestamp = 0;
    while (!isRunningTaskStopped())
    {
        c.waitForCycleDuration();
        //ActionRecognitionResult result = actionRecognition->getLatestRecognition();
        // 22.07.2020 (Fabian):
        // The pose based action recognition is broken (crashes after 2 seconds).
        // In the demo, the timeout is always used.
        // We reduce the dependency chain a bit by not using it
        auto age = (TimeUtil::GetTime() - IceUtil::Time::microSeconds(result.timestamp)).toSecondsDouble();
        if (age > 10)
        {
            ARMARX_INFO << deactivateSpam(2) << "Data is too old: " << age << " label: " << result.label;
            continue;
        }
        if (result.timestamp != lastTimestamp)
        {
            buffer.push_back(result.label == GUARD_SUPPORT);

            int detected = 0;
            for (bool support : buffer)
            {
                if (support)
                {
                    ++detected;
                }
            }
            float supportRatio = 1.0f * detected / buffer.size();

            ARMARX_INFO << "Before Support ratio: " << supportRatio << " " << VAROUT(detected) << VAROUT(buffer.size());

            if ((int)buffer.size() < detectionBufferSize / 2)
            {
                continue;
            }

            ARMARX_INFO << "After Support ratio: " << supportRatio;

            if (supportRatio >= detectionThreshold)
            {
                ARMARX_IMPORTANT << "Detected guard support";
                getTextToSpeech()->reportText(in.getTextAfterHelpRecognition());

                auto display = in.getDisplay_NeedOfHelpRecognized();
                if (display.size() == 2)
                {
                    getMessageDisplay()->setMessage(display[0], display[1]);
                }
                else
                {
                    ARMARX_WARNING << "Display text not set (wrong size): " << display.size();
                }


                emitGuardSupportDetected();
                break;
            }

            lastTimestamp = result.timestamp;
        }
        else
        {
            ARMARX_INFO << deactivateSpam(1) << "Timestamp is the same: " << lastTimestamp;
        }


    }
#endif
}

//void WaitForGuardSupportGesture::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void WaitForGuardSupportGesture::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr WaitForGuardSupportGesture::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new WaitForGuardSupportGesture(stateData));
}

