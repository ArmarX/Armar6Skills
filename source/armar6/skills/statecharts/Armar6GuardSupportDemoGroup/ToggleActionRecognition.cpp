/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6GuardSupportDemoGroup
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ToggleActionRecognition.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6GuardSupportDemoGroup;

// DO NOT EDIT NEXT LINE
ToggleActionRecognition::SubClassRegistry ToggleActionRecognition::Registry(ToggleActionRecognition::GetName(), &ToggleActionRecognition::CreateInstance);



void ToggleActionRecognition::onEnter()
{
    armarx::OpenPoseEstimationInterfacePrx const& openPose = getOpenPoseEstimation();
    //armarx::PoseBasedActionRecognitionInterfacePrx const& actionRecognition = getPoseBasedActionRecognition();
    if (in.getActivate())
    {
        ARMARX_INFO << "Starting open pose tracking";
        //        openPose->start();
        openPose->start3DPoseEstimation();

        //ARMARX_INFO << "Starting action recognition";
        //actionRecognition->startRecognition();
    }
    else
    {
        if (in.getStopOpenPoseAfterRun())
        {
            ARMARX_INFO << "Stopping open pose tracking";
            openPose->stop();
        }

        //ARMARX_INFO << "Stopping action recognition";
        //actionRecognition->stopRecognition();
    }
    emitSuccess();
}

//void ToggleActionRecognition::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void ToggleActionRecognition::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ToggleActionRecognition::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ToggleActionRecognition::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ToggleActionRecognition(stateData));
}
