/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6TorqueCalibrationGroup
 * @author     Lukas Kaul ( lukas dot s dot kaul at gmail dot com )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MeasureTorques.h"

#include <ArmarXCore/core/time/TimeUtil.h>

#include <ArmarXCore/observers/variant/TimedVariant.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6TorqueCalibrationGroup;

// DO NOT EDIT NEXT LINE
MeasureTorques::SubClassRegistry MeasureTorques::Registry(MeasureTorques::GetName(), &MeasureTorques::CreateInstance);



void MeasureTorques::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void MeasureTorques::run()
{
    NameControlModeMap positionControl;

    //VirtualRobot::RobotPtr robot = getRobot();
    //robot->getRobotNode("bla")->get


    for (const StringValueMapPtr& configurationPtr : in.getConfigurations())
    {
        std::map<std::string, float> configuration = configurationPtr->toStdMap<float>();
        for (std::pair<std::string, float> pair : configuration)
        {
            positionControl[pair.first] = ePositionControl;
        }
        getKinematicUnit()->switchControlMode(positionControl);
        getKinematicUnit()->setJointAngles(configuration);

        TimeUtil::Sleep(in.getSleepTime());

        for (std::pair<std::string, float> pair : configuration)
        {
            TimedVariantPtr positionDF = TimedVariantPtr::dynamicCast(getRobotUnitObserver()->getDatafieldByName("SensorDevices", pair.first + "_SensorValue1DoFActuator_position"));
            float position = positionDF->getFloat();
            ARMARX_IMPORTANT << pair.first << " position: " << position;

            TimedVariantPtr torqueDF = TimedVariantPtr::dynamicCast(getRobotUnitObserver()->getDatafieldByName("SensorDevices", pair.first + "_SensorValue1DoFActuator_torque"));
            float torque = torqueDF->getFloat();
            ARMARX_IMPORTANT << pair.first << " torque: " << torque;
        }


        if (isRunningTaskStopped())
        {
            break;
        }
    }

    emitDone();

    /*while (!isRunningTaskStopped()) // stop run function if returning true
    {
        // do your calculations
    }*/
}

//void MeasureTorques::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MeasureTorques::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MeasureTorques::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MeasureTorques(stateData));
}
