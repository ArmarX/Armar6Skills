/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6TorqueCalibrationGroup::Armar6TorqueCalibrationGroupRemoteStateOfferer
 * @author     Lukas Kaul ( lukas dot s dot kaul at gmail dot com )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6TorqueCalibrationGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6TorqueCalibrationGroup;

// DO NOT EDIT NEXT LINE
Armar6TorqueCalibrationGroupRemoteStateOfferer::SubClassRegistry Armar6TorqueCalibrationGroupRemoteStateOfferer::Registry(Armar6TorqueCalibrationGroupRemoteStateOfferer::GetName(), &Armar6TorqueCalibrationGroupRemoteStateOfferer::CreateInstance);



Armar6TorqueCalibrationGroupRemoteStateOfferer::Armar6TorqueCalibrationGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6TorqueCalibrationGroupStatechartContext > (reader)
{
}

void Armar6TorqueCalibrationGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6TorqueCalibrationGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6TorqueCalibrationGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6TorqueCalibrationGroupRemoteStateOfferer::GetName()
{
    return "Armar6TorqueCalibrationGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6TorqueCalibrationGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6TorqueCalibrationGroupRemoteStateOfferer(reader));
}
