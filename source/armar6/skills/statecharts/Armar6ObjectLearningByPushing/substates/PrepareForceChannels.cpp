/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ObjectLearningByPushing
 * @author     Sarah Puch ( sarah dot puch at domain dot de )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PrepareForceChannels.h"

#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <RobotAPI/libraries/core/observerfilters/PoseMedianFilter.h>
#include <RobotAPI/libraries/core/observerfilters/OffsetFilter.h>
#include <ArmarXCore/observers/filters/DatafieldFilter.h>
#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>

using namespace armarx;
using namespace Armar6ObjectLearningByPushing;

// DO NOT EDIT NEXT LINE
PrepareForceChannels::SubClassRegistry PrepareForceChannels::Registry(PrepareForceChannels::GetName(), &PrepareForceChannels::CreateInstance);



void PrepareForceChannels::onEnter()
{
    // Readout the forces and convert the datatype
    // In the next step we filter the datafields with a median and an offset filter

    DatafieldRefBasePtr datafieldBaseRight;
    if (getForceTorqueObserver()->existsChannel(in.getForceTorqueChannelNameRight()))
    {
        datafieldBaseRight = getForceTorqueObserver()->getForceDatafield(in.getForceTorqueChannelNameRight());
        ARMARX_INFO << "Using FT channel (right): " << in.getForceTorqueChannelNameRight() << armarx::flush;
    }
    /*
    else if (getForceTorqueObserver()->existsChannel("Wrist 2 R")) // for simulation
    {
        datafieldBaseRight = context->getForceTorqueObserver()->getForceDatafield("Wrist 2 R");
    }
    else if (context->getForceTorqueObserver()->existsChannel("TCP R")) // for real armar
    {
        datafieldBaseRight = context->getForceTorqueObserver()->getForceDatafield("TCP R");
    }
    */
    if (!datafieldBaseRight)
    {
        ARMARX_ERROR << "datafieldBaseRight is NULL";
        emitFailure();
    }

    DatafieldRefBasePtr datafieldBaseLeft;
    if (getForceTorqueObserver()->existsChannel(in.getForceTorqueChannelNameLeft()))
    {
        datafieldBaseLeft = getForceTorqueObserver()->getForceDatafield(in.getForceTorqueChannelNameLeft());
        ARMARX_INFO << "Using FT channel (left): " << in.getForceTorqueChannelNameLeft() << armarx::flush;
    }
    /*
    else if (context->getForceTorqueObserver()->existsChannel("Wrist 2 L")) // for simulation Armar3
    {
        datafieldBaseLeft = getForceTorqueObserver()->getForceDatafield("Wrist 2 L");
    }
    else if (getForceTorqueObserver()->existsChannel("TCP L")) // for real Armar3
    {
        datafieldBaseLeft = getForceTorqueObserver()->getForceDatafield("TCP L");
    }
    */
    if (!datafieldBaseLeft)
    {
        ARMARX_ERROR << "datafieldBaseLeft is NULL";
        emitFailure();
    }

    DatafieldRefPtr datafieldRightFilteredOffset = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->createFilteredDatafield(DatafieldFilterBasePtr(new filters::OffsetFilter()), datafieldBaseRight));
    DatafieldRefPtr datafieldRightFiltered = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->createFilteredDatafield(DatafieldFilterBasePtr(new filters::PoseMedianFilter), datafieldRightFilteredOffset));

    DatafieldRefPtr datafieldLeftFilteredOffset = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->createFilteredDatafield(DatafieldFilterBasePtr(new filters::OffsetFilter()), datafieldBaseLeft));
    DatafieldRefPtr datafieldLeftFiltered = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->createFilteredDatafield(DatafieldFilterBasePtr(new filters::PoseMedianFilter), datafieldLeftFilteredOffset));

    out.setDatafieldFilteredRight(datafieldRightFiltered);
    out.setDatafieldFilteredLeft(datafieldLeftFiltered);

    emitSuccess();
}

//void PrepareForceChannels::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void PrepareForceChannels::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void PrepareForceChannels::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr PrepareForceChannels::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new PrepareForceChannels(stateData));
}
