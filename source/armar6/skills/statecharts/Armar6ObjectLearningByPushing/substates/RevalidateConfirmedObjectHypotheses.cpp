/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ObjectLearningByPushing
 * @author     Sarah Puch ( sarah dot puch at domain dot de )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RevalidateConfirmedObjectHypotheses.h"

using namespace armarx;
using namespace Armar6ObjectLearningByPushing;

// DO NOT EDIT NEXT LINE
RevalidateConfirmedObjectHypotheses::SubClassRegistry RevalidateConfirmedObjectHypotheses::Registry(RevalidateConfirmedObjectHypotheses::GetName(), &RevalidateConfirmedObjectHypotheses::CreateInstance);



void RevalidateConfirmedObjectHypotheses::onEnter()
{
    ARMARX_VERBOSE << "Entering RevalidateConfirmedObjectHypotheses";

    getObjectLearningByPushingProxy()->RevalidateConfirmedObjectHypotheses();

    ARMARX_VERBOSE << "Finished entering RevalidateConfirmedObjectHypotheses";
}

void RevalidateConfirmedObjectHypotheses::run()
{
    // fix all the races
    sleep(5);

    Literal checkHypothesesValidated("ObjectLearningByPushingObserver.objectHypotheses.hypothesesValidated", "equals", Literal::createParameterList(1));
    installConditionForHypothesesValidated(checkHypothesesValidated);

    Literal checkHypothesesValidationFailed("ObjectLearningByPushingObserver.objectHypotheses.hypothesesValidated", "equals", Literal::createParameterList(-1));
    installConditionForHypothesesValidationFailed(checkHypothesesValidationFailed);
}

//void RevalidateConfirmedObjectHypotheses::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void RevalidateConfirmedObjectHypotheses::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr RevalidateConfirmedObjectHypotheses::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new RevalidateConfirmedObjectHypotheses(stateData));
}
