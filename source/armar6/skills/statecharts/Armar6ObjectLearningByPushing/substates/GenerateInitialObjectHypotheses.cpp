/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ObjectLearningByPushing
 * @author     Sarah Puch ( sarah dot puch at domain dot de )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GenerateInitialObjectHypotheses.h"
#include <RobotAPI/libraries/core/FramedPose.h>

using namespace armarx;
using namespace Armar6ObjectLearningByPushing;

// DO NOT EDIT NEXT LINE
GenerateInitialObjectHypotheses::SubClassRegistry GenerateInitialObjectHypotheses::Registry(GenerateInitialObjectHypotheses::GetName(), &GenerateInitialObjectHypotheses::CreateInstance);



void GenerateInitialObjectHypotheses::onEnter()
{
    ARMARX_VERBOSE << "Entering GenerateInitialObjectHypotheses";

    getObjectLearningByPushingProxy()->CreateInitialObjectHypotheses();

    /*
    context->lines.startPoints.clear();
    context->lines.pushDirections.clear();
    context->lines.sides.clear();
    out.setExistLastLine(false);
    out.setNoTurn(true);
    */

    /*
    ChannelRefPtr counterId = in.getCounterId();
    getSystemObserver()->resetCounter(counterId);
    */

    Literal checkHypothesisCreated("ObjectLearningByPushingObserver.objectHypotheses.initialHypothesesCreated", "equals", Literal::createParameterList(1));
    installConditionForInitialObjectHypothesesGenerated(checkHypothesisCreated);

    Literal checkHypothesisCreationFailed("ObjectLearningByPushingObserver.objectHypotheses.initialHypothesesCreated", "equals", Literal::createParameterList(-1));
    installConditionForInitialObjectHypothesesGenerationFailed(checkHypothesisCreationFailed);

    ARMARX_VERBOSE << "Finished entering GenerateInitialObjectHypotheses";
}

//void GenerateInitialObjectHypotheses::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void GenerateInitialObjectHypotheses::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GenerateInitialObjectHypotheses::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

    DataFieldIdentifierPtr dataFieldId = new DataFieldIdentifier("ObjectLearningByPushingObserver.objectHypothesisPosition.objectPosition");
    FramedPositionPtr objectPositionFramed = armarx::VariantPtr::dynamicCast(getObjectLearningByPushingObserver()->getDataField(dataFieldId))->getClass<FramedPosition>();
    ARMARX_VERBOSE << "Object position: " << *objectPositionFramed;

    ARMARX_IMPORTANT << "PushObjectHypothesis::onEnter()";
    DebugDrawerInterfacePrx const& debugDrawer = getDebugDrawerTopic();
    SharedRobotInterfacePrx sharedRobot = getRobotStateComponent()->getSynchronizedRobot();
    VirtualRobot::RobotPtr robot = getLocalRobot();
    RemoteRobot::synchronizeLocalClone(robot, sharedRobot);
    Eigen::Affine3f robotToGlobal;
    robotToGlobal.matrix() = robot->getGlobalPose();

    Vector3Ptr globalObjPos = Vector3Ptr {new Vector3(objectPositionFramed->toGlobalEigen(robot))};

    debugDrawer->setSphereVisu("HypoGen", "GlobalObj", globalObjPos, DrawColor {0, 0, 1, 1}, 50);
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GenerateInitialObjectHypotheses::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GenerateInitialObjectHypotheses(stateData));
}
