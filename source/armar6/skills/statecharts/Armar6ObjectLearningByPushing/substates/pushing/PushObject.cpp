/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ObjectLearningByPushing
 * @author     Sarah Puch ( sarah dot puch at domain dot de )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PushObject.h"

#include <VirtualRobot/LinkedCoordinate.h>

#include <RobotAPI/libraries/core/LinkedPose.h>

#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

using namespace armarx;
using namespace Armar6ObjectLearningByPushing;

// DO NOT EDIT NEXT LINE
PushObject::SubClassRegistry PushObject::Registry(PushObject::GetName(), &PushObject::CreateInstance);



void PushObject::onEnter()
{
    // getViewSelection()->activateAutomaticViewSelection();
    // getViewSelection()->deactivateAutomaticViewSelection();

    getTcpControlUnit()->request();

    // store keypoints:
    local.setPushingKeypoint1_2(in.getPushingKeypoint1());
    local.setPushingKeypoint2_2(in.getPushingKeypoint2());
    local.setPushingKeypoint3_2(in.getPushingKeypoint3());
    local.setPushingKeypoint4_2(in.getPushingKeypoint4());

    local.setIncrement(45.0);
    float forceLimit = 30.0;
    local.setForceLimit(forceLimit);

    // get position of the datapoints from the hypothesis.
    DataFieldIdentifierPtr dataFieldId = new DataFieldIdentifier("ObjectLearningByPushingObserver.objectHypothesisPosition.objectPosition");
    FramedPositionPtr objectPositionFramed = armarx::VariantPtr::dynamicCast(getObjectLearningByPushingObserver()->getDataField(dataFieldId))->getClass<FramedPosition>();

    // get the robot
    SharedRobotInterfacePrx robotSnapshot = getRobotStateComponent()->getRobotSnapshot("PushObjectStateRobotSnapshot");
    const std::string rootRobotNodeName = robotSnapshot->getRootNode()->getName();
    const std::string agentName = getRobotStateComponent()->getSynchronizedRobot()->getName();
    //get information about the hypothesis.
    Eigen::Matrix3f id = Eigen::Matrix3f::Identity();
    LinkedPosePtr objectPositionLinked = new LinkedPose(id, objectPositionFramed->toEigen(), objectPositionFramed->getFrame(), robotSnapshot);
    VirtualRobot::LinkedCoordinate objectPositionLinkedCoord = objectPositionLinked->createLinkedCoordinate();
    objectPositionLinkedCoord.changeFrame(rootRobotNodeName);

    // nicht sicher, was das Linked ist.
    // Eigen::Vector3f objectPosition = objectPositionLinkedCoord.getPose().block<3,1>(0,3);

    // visualize hypothesis
    if (getWorkingMemory()->getAgentInstancesSegment() && getDebugDrawerTopic())
    {
        if (getWorkingMemory()->getAgentInstancesSegment()->getAgentInstanceByName(agentName))
        {
            visionx::types::PointList hypothesisPointList = getObjectLearningByPushingProxy()->getObjectHypothesisPoints();
            //clean layer

            getDebugDrawerTopic()->clearLayer("boundingbox");
            getDebugDrawerTopic()->clearLayer("Hypothesispoints");

            // Get transformation to global coordinates
            PosePtr trafoCamToGlobal = PosePtr::dynamicCast(robotSnapshot->getRobotNode(objectPositionFramed->getFrame())->getGlobalPose());
            Eigen::Matrix4f transMatCameraToGlobal = trafoCamToGlobal->toEigen();


            //Painting the bounding box with the principal axis (at the moment off)
            {
                //Beispiel:
                //            DataFieldIdentifierPtr dataFieldId = new DataFieldIdentifier("ObjectLearningByPushingObserver.objectHypothesisPosition.objectPosition");
                //            FramedPositionPtr objectPositionFramed = armarx::VariantPtr::dynamicCast(getObjectLearningByPushingObserver()->getDataField(dataFieldId))->getClass<FramedPosition>();

                //            dataFieldId = new DataFieldIdentifier("ObjectLearningByPushingObserver.objectHypothesisPosition.principalAxis1");
                //            Vector3Ptr principalAxis1 = armarx::VariantPtr::dynamicCast(getObjectLearningByPushingObserver()->getDataField(dataFieldId))->getClass<Vector3>();
                //            dataFieldId = new DataFieldIdentifier("ObjectLearningByPushingObserver.objectHypothesisPosition.principalAxis2");
                //            Vector3Ptr principalAxis2 = armarx::VariantPtr::dynamicCast(getObjectLearningByPushingObserver()->getDataField(dataFieldId))->getClass<Vector3>();
                //            dataFieldId = new DataFieldIdentifier("ObjectLearningByPushingObserver.objectHypothesisPosition.principalAxis3");
                //            Vector3Ptr principalAxis3 = armarx::VariantPtr::dynamicCast(getObjectLearningByPushingObserver()->getDataField(dataFieldId))->getClass<Vector3>();
                //            dataFieldId = new DataFieldIdentifier("ObjectLearningByPushingObserver.objectHypothesisPosition.eigenValues");
                //            Vector3Ptr eigenValues = armarx::VariantPtr::dynamicCast(getObjectLearningByPushingObserver()->getDataField(dataFieldId))->getClass<Vector3>();

                //            //at the moment not used
                //            //dataFieldId = new DataFieldIdentifier("ObjectLearningByPushingObserver.objectHypothesisPosition.objectExtent");
                //            //float objectExtent = getObjectLearningByPushingObserver()->getDataField(dataFieldId)->getFloat();

                //            Eigen::Matrix3f m = Eigen::Matrix3f::Identity();
                //            FramedPosePtr objectPoseEyeLeftCamera = new FramedPose(m, objectPositionFramed->toEigen(), objectPositionFramed->frame, rootRobotNodeName);
                //            Eigen::Matrix4f objectPoseGlobalMat = transMatCameraToGlobal * objectPoseEyeLeftCamera->toEigen();
                //            Eigen::Vector3f objectPositionGlobalVec = objectPoseGlobalMat.block<3,1>(0,3);


                //            float eigenValueX = eigenValues->x;
                //            float eigenValueY = eigenValues->y;
                //            float eigenValueZ = eigenValues->z;

                //            //for printing the eigen achses
                //            //double sqrteigenValueX = sqrt(eigenValueX);
                //            Eigen::Vector3f principalAxis1GlobalVec = transMatCameraToGlobal.block<3,3>(0,0) * principalAxis1->toEigen();
                //            //Eigen::Vector3f startPointVec1 = objectPositionGlobalVec;
                //            //Eigen::Vector3f endPointVec1 = objectPositionGlobalVec + (principalAxis1GlobalVec.array() * sqrteigenValueX).matrix();
                //            //getDebugDrawerTopic()->setLineDebugLayerVisu("principalAxis1", new Vector3(startPointVec1), new Vector3(endPointVec1), 5, DrawColor{1,0,0,1});
                //            //double sqrteigenValueY = sqrt(eigenValueY);
                //            Eigen::Vector3f principalAxis2GlobalVec = transMatCameraToGlobal.block<3,3>(0,0) * principalAxis2->toEigen();
                //            //Eigen::Vector3f startPointVec2 = objectPositionGlobalVec;
                //            //Eigen::Vector3f endPointVec2 = objectPositionGlobalVec + (principalAxis2GlobalVec.array() * sqrteigenValueY).matrix();
                //            //getDebugDrawerTopic()->setLineDebugLayerVisu("principalAxis2", new Vector3(startPointVec2), new Vector3(endPointVec2), 5, DrawColor{0,1,0,1});
                //            //double sqrteigenValueZ = sqrt(eigenValueZ);
                //            Eigen::Vector3f principalAxis3GlobalVec = transMatCameraToGlobal.block<3,3>(0,0) * principalAxis3->toEigen();
                //            //Eigen::Vector3f startPointVec3 = objectPositionGlobalVec;
                //            //Eigen::Vector3f endPointVec3 = objectPositionGlobalVec + (principalAxis3GlobalVec.array() * sqrteigenValueZ).matrix();
                //            //getDebugDrawerTopic()->setLineDebugLayerVisu("principalAxis3", new Vector3(startPointVec3), new Vector3(endPointVec3), 5, DrawColor{0,0,1,1});



                //            //for printing the eigen bounding box (bb)
                //            double poweigenValueX = 0.15 * pow(eigenValueX, 1.0/2.0); // urspünglich *2.5
                //            double poweigenValueY = 0.15 * pow(eigenValueY, 1.0/2.0);
                //            double poweigenValueZ = 0.15 * pow(eigenValueZ, 1.0/2.0);

                //            Eigen::Vector3f axisX = poweigenValueX * principalAxis1GlobalVec;
                //            Eigen::Vector3f axisY = poweigenValueY * principalAxis2GlobalVec;
                //            Eigen::Vector3f axisZ = poweigenValueZ * principalAxis3GlobalVec;

                //            Eigen::Vector3f bbPoint1 = objectPositionGlobalVec + axisX + axisY + axisZ;
                //            Eigen::Vector3f bbPoint2 = objectPositionGlobalVec - axisX + axisY + axisZ;
                //            Eigen::Vector3f bbPoint3 = objectPositionGlobalVec - axisX - axisY + axisZ;
                //            Eigen::Vector3f bbPoint4 = objectPositionGlobalVec + axisX - axisY + axisZ;
                //            Eigen::Vector3f bbPoint5 = objectPositionGlobalVec + axisX + axisY - axisZ;
                //            Eigen::Vector3f bbPoint6 = objectPositionGlobalVec - axisX + axisY - axisZ;
                //            Eigen::Vector3f bbPoint7 = objectPositionGlobalVec - axisX - axisY - axisZ;
                //            Eigen::Vector3f bbPoint8 = objectPositionGlobalVec + axisX - axisY - axisZ;

                //            //top side
                //            getDebugDrawerTopic()->setLineVisu("boundingbox", "bbLine1", new Vector3(bbPoint1), new Vector3(bbPoint2), 5, DrawColor{1,0,0,1}); //red
                //            getDebugDrawerTopic()->setLineVisu("boundingbox", "bbLine2", new Vector3(bbPoint2), new Vector3(bbPoint3), 5, DrawColor{0,1,0,1}); //green
                //            getDebugDrawerTopic()->setLineVisu("boundingbox", "bbLine3", new Vector3(bbPoint3), new Vector3(bbPoint4), 5, DrawColor{0,0,1,1}); //blue
                //            getDebugDrawerTopic()->setLineVisu("boundingbox", "bbLine4", new Vector3(bbPoint4), new Vector3(bbPoint1), 5, DrawColor{1,1,1,1}); //white
                //            //undersite
                //            getDebugDrawerTopic()->setLineVisu("boundingbox", "bbLine5", new Vector3(bbPoint5), new Vector3(bbPoint6), 5, DrawColor{0,0,0,1}); //black
                //            getDebugDrawerTopic()->setLineVisu("boundingbox", "bbLine6", new Vector3(bbPoint6), new Vector3(bbPoint7), 5, DrawColor{0,1,0,1}); //green
                //            getDebugDrawerTopic()->setLineVisu("boundingbox", "bbLine7", new Vector3(bbPoint7), new Vector3(bbPoint8), 5, DrawColor{0,1,1,1}); //neo blue
                //            getDebugDrawerTopic()->setLineVisu("boundingbox", "bbLine8", new Vector3(bbPoint8), new Vector3(bbPoint5), 5, DrawColor{1,1,1,1}); //white
                //            //side
                //            getDebugDrawerTopic()->setLineVisu("boundingbox", "bbLine09", new Vector3(bbPoint1), new Vector3(bbPoint5), 5, DrawColor{1,1,0,1}); //yellow
                //            getDebugDrawerTopic()->setLineVisu("boundingbox", "bbLine10", new Vector3(bbPoint2), new Vector3(bbPoint6), 5, DrawColor{1,1,0,1}); //yellow
                //            getDebugDrawerTopic()->setLineVisu("boundingbox", "bbLine11", new Vector3(bbPoint3), new Vector3(bbPoint7), 5, DrawColor{1,1,0,1}); //yellow
                //            getDebugDrawerTopic()->setLineVisu("boundingbox", "bbLine12", new Vector3(bbPoint4), new Vector3(bbPoint8), 5, DrawColor{1,1,0,1}); //yellow
            }

            // show the point cloud data
            // transform the point cloud vectors from the EyeLeftCamera frame to the global frame
            for (size_t i = 0; i < hypothesisPointList.size(); i++)
            {
                Vector3Ptr pointVec = Vector3Ptr::dynamicCast(hypothesisPointList[i]);
                Eigen::Vector3f pointGlobalVec = transMatCameraToGlobal.block<3, 1>(0, 3) + transMatCameraToGlobal.block<3, 3>(0, 0) * pointVec->toEigen();
                getDebugDrawerTopic()->setSphereVisu("Hypothesispoints", to_string(i), new Vector3(pointGlobalVec), DrawColor {1, 1, 1, 1}, 5.0f);
            }
        }
        else
        {
            ARMARX_WARNING << "Agent with name " << agentName << " not found in the AgentInstancesSegment";
        }
    }
    else
    {
        ARMARX_WARNING << "getWorkingMemory()->getAgentInstancesSegment() = " << getWorkingMemory()->getAgentInstancesSegment() << ",getDebugDrawerTopic() = " << getDebugDrawerTopic();
    }
}

void PushObject::run()
{
    /*
    std::string kinematicChainName = in.getKinematicChainName();
    bool useLeftHand = kinematicChainName.find("left") != std::string::npos || kinematicChainName.find("Left") != std::string::npos;

    Eigen::Vector3f viewTargetEigen(0, 800, 1150);
    viewTargetEigen(0) = useLeftHand ? -250 : 250;
    FramedPositionPtr viewTarget = new FramedPosition(viewTargetEigen, getRobotStateComponent()->getSynchronizedRobot()->getRootNode()->getName(), getRobotStateComponent()->getSynchronizedRobot()->getName());

    getHeadIKUnit()->setHeadTarget("IKVirtualGaze", viewTarget);
    */
}

void PushObject::onBreak()
{
    getTcpControlUnit()->release();
    // getViewSelection()->deactivateAutomaticViewSelection();
}

void PushObject::onExit()
{
    // getTcpControlUnit()->release();
    // getViewSelection()->deactivateAutomaticViewSelection();
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr PushObject::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new PushObject(stateData));
}
