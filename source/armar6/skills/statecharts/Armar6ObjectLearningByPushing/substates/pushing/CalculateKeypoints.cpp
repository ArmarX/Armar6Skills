/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ObjectLearningByPushing
 * @author     Sarah Puch ( sarah dot puch at domain dot de )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CalculateKeypoints.h"

#include <VirtualRobot/LinkedCoordinate.h>

#include <RobotAPI/libraries/core/LinkedPose.h>
#include <RobotAPI/libraries/core/math/MathUtils.h>
#include <RobotAPI/libraries/core/math/MatrixHelpers.h>
#include <RobotAPI/libraries/core/math/SVD.h>
#include <RobotAPI/libraries/core/math/StatUtils.h>
#include <RobotAPI/libraries/core/math/Trigonometry.h>

#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

using namespace armarx;
using namespace Armar6ObjectLearningByPushing;

// DO NOT EDIT NEXT LINE
CalculateKeypoints::SubClassRegistry CalculateKeypoints::Registry(CalculateKeypoints::GetName(), &CalculateKeypoints::CreateInstance);



void CalculateKeypoints::onEnter()
{
    ARMARX_IMPORTANT << "Entering CalculateKeypoints!";

    /*
    if (in.getUseCoG())
    {
        ARMARX_VERBOSE << "CalculateWithCoG!";
        CalculateWithCoM();
    }
    else
    */
    {
        ARMARX_VERBOSE << "CalculateTraditionally!";
        CalculateTraditionally();
    }
}

//void CalculateKeypoints::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void CalculateKeypoints::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void CalculateKeypoints::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CalculateKeypoints::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CalculateKeypoints(stateData));
}


void CalculateKeypoints::CalculateTraditionally()
{
    getTcpControlUnit()->request();
    getDebugDrawerTopic()->clearAll();

    // get position of the datapoints from the hypothesis.
    DataFieldIdentifierPtr dataFieldId = new DataFieldIdentifier("ObjectLearningByPushingObserver.objectHypothesisPosition.objectPosition");
    FramedPositionPtr objectPositionFramed = armarx::VariantPtr::dynamicCast(getObjectLearningByPushingObserver()->getDataField(dataFieldId))->getClass<FramedPosition>();
    ARMARX_VERBOSE << "Object position: " << *objectPositionFramed;

    // get the robot
    SharedRobotInterfacePrx robotSnapshot = getRobotStateComponent()->getRobotSnapshot("PushObjectStateRobotSnapshot");
    const std::string rootRobotNodeName = robotSnapshot->getRootNode()->getName();
    const std::string agentName = getRobotStateComponent()->getSynchronizedRobot()->getName();

    Eigen::Matrix3f id = Eigen::Matrix3f::Identity();
    LinkedPosePtr objectPositionLinked = new LinkedPose(id, objectPositionFramed->toEigen(), objectPositionFramed->getFrame(), robotSnapshot);
    VirtualRobot::LinkedCoordinate objectPositionLinkedCoord = objectPositionLinked->createLinkedCoordinate();
    objectPositionLinkedCoord.changeFrame(rootRobotNodeName);
    // Matrix for coordinate transformation.
    Eigen::Vector3f objectPosition = objectPositionLinkedCoord.getPose().block<3, 1>(0, 3);

    // Matrix for coordinate transformation. Platform to global.
    FramedPositionPtr agentPosition = FramedPositionPtr::dynamicCast(getWorkingMemory()->getAgentInstancesSegment()->getAgentInstanceByName(agentName)->getPositionBase());
    FramedOrientationPtr agentOrientation = FramedOrientationPtr::dynamicCast(getWorkingMemory()->getAgentInstancesSegment()->getAgentInstanceByName(agentName)->getOrientationBase());
    PosePtr agentPose = new Pose(agentOrientation->toEigen(), agentPosition->toEigen());
    Eigen::Matrix4f agentPoseMatrix = agentPose->toEigen();

    ARMARX_IMPORTANT << "Object position in root frame: " << objectPosition;

    // params for workspace
    Eigen::Vector3f workspaceLimitsHigh = in.getWorkspaceLimitsHigh()->toEigen();
    Eigen::Vector3f workspaceLimitsLow = in.getWorkspaceLimitsLow()->toEigen();
    Eigen::Vector3f centralPoint = in.getCentralPoint()->toEigen();

    // manual offset to compensate calibration errors while using real Armar
    Eigen::Vector3f manualOffsetLeft = Eigen::Vector3f::Zero();
    Eigen::Vector3f manualOffsetRight = Eigen::Vector3f::Zero();

    if (in.isManualOffsetLeftSet())
    {
        manualOffsetLeft = in.getManualOffsetLeft()->toEigen();
    }

    if (in.isManualOffsetRightSet())
    {
        manualOffsetRight = in.getManualOffsetRight()->toEigen();
    }

    // determine pushing direction
    Eigen::Vector3f pushingDirection = centralPoint - objectPosition;
    pushingDirection(2) = 0;
    // create a pushing direction that is mainly in x direction
    pushingDirection(0) *= 2;
    if (pushingDirection.norm() == 0)
    {
        pushingDirection(0) = 1;
    }
    if (fabs(pushingDirection(0)) < fabs(pushingDirection(1)))
    {
        pushingDirection(0) *= fabs(pushingDirection(1)) / fabs(pushingDirection(0));
    }
    pushingDirection = pushingDirection / pushingDirection.norm();

    float pushLength = in.getPushLength() * (0.9 + 0.2 * 0.001 * (rand() % 1000));

    Eigen::Vector3f pushingOffset = in.getPushingOffset()->toEigen();
    ARMARX_INFO << "Pushing offset " << pushingOffset;
    if (pushingOffset.norm() > 0)
    {
        pushLength *= 1.2;
        manualOffsetLeft(2) -= 10;
    }

    Eigen::Vector3f manualOffset = pushingDirection(0) > 0 ? manualOffsetLeft : manualOffsetRight;
    ARMARX_VERBOSE << "Manual offset " << manualOffset;
    pushingOffset += manualOffset;

    // calculate pushing keypoints
    Eigen::Vector3f pushingKeypoint1 = objectPosition - in.getPrePushDistanceFromObject() * pushingDirection;
    pushingKeypoint1(2) += in.getPrePushHeightOffset();
    pushingKeypoint1 += pushingOffset;
    clipToWorkspaceLimits(pushingKeypoint1, workspaceLimitsLow, workspaceLimitsHigh);
    FramedPosition pushingKeypoint1Framed(pushingKeypoint1, rootRobotNodeName, agentName);
    out.setPushingKeypoint1(pushingKeypoint1Framed);
    ARMARX_VERBOSE << "Pushing keypoint 1: " << pushingKeypoint1;

    Eigen::Vector3f pushingKeypoint2 = objectPosition - in.getPrePushDistanceFromObject() * pushingDirection;
    pushingKeypoint2 += pushingOffset;
    clipToWorkspaceLimits(pushingKeypoint2, workspaceLimitsLow, workspaceLimitsHigh);
    FramedPosition pushingKeypoint2Framed(pushingKeypoint2, rootRobotNodeName, agentName);
    out.setPushingKeypoint2(pushingKeypoint2Framed);
    ARMARX_VERBOSE << "Pushing keypoint 2: " << pushingKeypoint2;
    Eigen::Vector3f pushingKeypoint2G = agentPoseMatrix.block<3, 1>(0, 3) + agentPoseMatrix.block<3, 3>(0, 0) * pushingKeypoint2;
    getDebugDrawerTopic()->setSphereVisu("Pushingkeypoint", "PK2", new Vector3(pushingKeypoint2G), DrawColor{5, 0, 0, 1}, 15.0f); // red

    Eigen::Vector3f pushingKeypoint3 = objectPosition - (in.getPrePushDistanceFromObject() - pushLength) * pushingDirection;
    pushingKeypoint3 += pushingOffset;
    clipToWorkspaceLimits(pushingKeypoint3, workspaceLimitsLow, workspaceLimitsHigh);
    FramedPosition pushingKeypoint3Framed(pushingKeypoint3, rootRobotNodeName, agentName);
    out.setPushingKeypoint3(pushingKeypoint3Framed);
    ARMARX_VERBOSE << "Pushing keypoint 3: " << pushingKeypoint3;
    Eigen::Vector3f pushingKeypoint3G = agentPoseMatrix.block<3, 1>(0, 3) + agentPoseMatrix.block<3, 3>(0, 0) * pushingKeypoint3;
    getDebugDrawerTopic()->setSphereVisu("Pushingkeypoint", "PK3", new Vector3(pushingKeypoint3G), DrawColor{5, 0, 0, 1}, 20.0f); // red

    Eigen::Vector3f pushingKeypoint4 = objectPosition - (in.getPrePushDistanceFromObject() - 0.7f * pushLength) * pushingDirection;
    pushingKeypoint4(2) += in.getPrePushHeightOffset();
    pushingKeypoint4 += pushingOffset;
    clipToWorkspaceLimits(pushingKeypoint4, workspaceLimitsLow, workspaceLimitsHigh);
    FramedPosition pushingKeypoint4Framed(pushingKeypoint4, rootRobotNodeName, agentName);
    out.setPushingKeypoint4(pushingKeypoint4Framed);
    ARMARX_VERBOSE << "Pushing keypoint 4: " << pushingKeypoint4;

    // choose which arm is used for pushing
    if (pushingDirection(0) > 0)
    {
        ARMARX_IMPORTANT << "Using left arm: " << in.getKinematicChainNameLeftArm();
        out.setKinematicChainName(in.getKinematicChainNameLeftArm());
        out.setHandModelName(in.getHandModelNameLeft());

        if (in.isLeftHandMemoryChannelSet() && in.getLeftHandMemoryChannel())
        {
            out.setHandMemoryChannel(in.getLeftHandMemoryChannel());
        }
    }
    else
    {
        ARMARX_IMPORTANT << "Using right arm: " << in.getKinematicChainNameRightArm();
        out.setKinematicChainName(in.getKinematicChainNameRightArm());
        out.setHandModelName(in.getHandModelNameRight());

        if (in.isRightHandMemoryChannelSet() && in.getRightHandMemoryChannel())
        {
            out.setHandMemoryChannel(in.getRightHandMemoryChannel());
        }
    }

    ARMARX_VERBOSE << "CalculateKeypoints pushing";

    out.setMinimalAllowedHeightForTCP(workspaceLimitsLow(2));
    out.setPushDirection(pushingDirection);
    out.setObjectPosition(objectPosition);
    // out.setWaitingTimeAfterPushing(8000);
    // out.setExistLastLine(false);

    emitStartPushing();
}

void CalculateKeypoints::clipToWorkspaceLimits(Eigen::Vector3f& point, const Eigen::Vector3f& workspaceLimitsLow, const Eigen::Vector3f& workspaceLimitsHigh)
{
    point(0) = (point(0) < workspaceLimitsLow(0)) ? workspaceLimitsLow(0) : ((point(0) > workspaceLimitsHigh(0)) ? workspaceLimitsHigh(0) : point(0));
    point(1) = (point(1) < workspaceLimitsLow(1)) ? workspaceLimitsLow(1) : ((point(1) > workspaceLimitsHigh(1)) ? workspaceLimitsHigh(1) : point(1));
    point(2) = (point(2) < workspaceLimitsLow(2)) ? workspaceLimitsLow(2) : ((point(2) > workspaceLimitsHigh(2)) ? workspaceLimitsHigh(2) : point(2));
}


/*
void CalculateKeypoints::CalculateWithCoM()
{
    ObjectLearningByPushingStatechartContext* context = getContext<ObjectLearningByPushingStatechartContext>();
    context->getTCPControlUnit()->request();

    //get position of the datapoints from the hypothesis.
    DataFieldIdentifierPtr dataFieldId = new DataFieldIdentifier("ObjectLearningByPushingObserver.objectHypothesisPosition.objectPosition");
    FramedPositionPtr objectPositionFramed = armarx::VariantPtr::dynamicCast(context->objectLearningByPushingListenerProxy->getDataField(dataFieldId))->getClass<FramedPosition>();

    //get the robot
    SharedRobotInterfacePrx robotSnapshot = context->getRobotStateComponent()->getRobotSnapshot("PushObjectStateRobotSnapshot");
    const std::string rootRobotNodeName = robotSnapshot->getRootNode()->getName();
    const std::string agentName = context->getRobotStateComponent()->getSynchronizedRobot()->getName();

    //get other information from the robot for painting (global pose)
    FramedPositionPtr agentPosition = FramedPositionPtr::dynamicCast(context->getWorkingMemoryProxy()->getAgentInstancesSegment()->getAgentInstanceByName(agentName)->getPositionBase());
    FramedOrientationPtr agentOrientation = FramedOrientationPtr::dynamicCast(context->getWorkingMemoryProxy()->getAgentInstancesSegment()->getAgentInstanceByName(agentName)->getOrientationBase());
    PosePtr agentPose = new Pose(agentOrientation->toEigen(), agentPosition->toEigen());
    Eigen::Matrix4f agentPoseMatrix = agentPose->toEigen();

    //manual offset to compensate calibration errors -> wird hofentlich nicht mehr gebraucht Achtung: Löschen?
    Eigen::Vector3f manualOffset(0, 0, 0);

    //Params for workspace
    Eigen::Vector3f workspaceLimitsHigh = in.getWorkspaceLimitsHigh()->toEigen();
    Eigen::Vector3f workspaceLimitsLow = in.getWorkspaceLimitsLow()->toEigen();
    Eigen::Vector3f centralPoint = in.getCentralPoint()->toEigen();

    //Transformation from camera to platform
    FramedPosePtr trafoCamToPlatformPtr = FramedPosePtr::dynamicCast(robotSnapshot->getRobotNode(objectPositionFramed->getFrame())->getPoseInRootFrame());
    Eigen::Matrix4f trafoCamToPlatform = trafoCamToPlatformPtr->toEigen();

    //counter for pushing
    ChannelRefPtr counterId = in.getCounterId();
    int counterValue = counterId->getDataField("value")->getInt();
    ARMARX_IMPORTANT << "In CalculateKPCoM Counter Value: " << counterValue;

    //cleaning the lists and layers
    visionx::types::PointList hypothesisPointList;
    hypothesisPointList.clear();
    visionx::types::PointList newHypothesisPointList;
    newHypothesisPointList.clear();
    context->getDebugDrawerTopicProxy()->clearAll();

    //Paint the central point on the floor.
    Eigen::Vector3f centralPointG = agentPoseMatrix.block<3, 3>(0, 0) * centralPoint + agentPoseMatrix.block<3, 1>(0, 3);
    centralPointG(2) = 0;
    context->getDebugDrawerTopicProxy()->setSphereVisu("CentralPoint", "Orkan", new Vector3(centralPointG), DrawColor{0, 0, 10, 1}, 30.0f);

    //get hypothesis from observer and bring the points in platform coord.
    visionx::types::PointList temp_hypothesisPointList = context->objectLearningByPushingProxy->getObjectHypothesisPoints();
    for (size_t i = 0; i < temp_hypothesisPointList.size(); i++)
    {
        Vector3Ptr pointVec = Vector3Ptr::dynamicCast(temp_hypothesisPointList[i]);
        Eigen::Vector3f temp_pointPlatformVec = trafoCamToPlatform.block<3, 3>(0, 0) * pointVec->toEigen() + trafoCamToPlatform.block<3, 1>(0, 3);
        armarx::Vector3Ptr point = new armarx::Vector3(temp_pointPlatformVec);
        hypothesisPointList.push_back(point);
    }

    //Get first and second singular vector
    Eigen::Vector3f principalAxis1;
    Eigen::Vector3f principalAxis2;
    calculateExtent(hypothesisPointList, principalAxis1, principalAxis2);

    // Calculate the four maximum points. These points form a boundary rectangel around the hypo. points. [(0)x_large, (1)y_large, (2)x_small, (3)y_small]
    // The value "extent" indicates the maximum extent of the point cloud from its center on to the edging.
    Eigen::MatrixXf boundaryPoints = getRectanglePoints(principalAxis1, principalAxis2, hypothesisPointList);
    Eigen::Vector3f tempCenterPoint = calculateAverageHypothesisPoint(hypothesisPointList);
    Eigen::Vector3f intersectionXMax = intersectionTwoLines(tempCenterPoint, principalAxis1, boundaryPoints.block<3, 1>(0, 0), principalAxis2);
    Eigen::Vector3f extentVec = tempCenterPoint - intersectionXMax;
    float extent = extentVec.norm();

    //Translation and Rotation of the hypothesis movement in camera frame and transfom. to platform frame
    PosePtr objectTransformation = PosePtr::dynamicCast(context->objectLearningByPushingProxy->getLastObjectTransformation());
    Eigen::Matrix4f objectTransformationPlatform = trafoCamToPlatform * objectTransformation->toEigen() * trafoCamToPlatform.inverse();

    //A: End of preparation

    //B: Beginning calculating center of mass
    Eigen::Vector3f objectPosition(0, 0, 0);

    // When a line has been added in the last run, it will be converted or deleted in one sector.
    if (in.getExistLastLine())
    {
        ARMARX_VERBOSE << "Last round we added a line!";

        context->lines.startPoints.push_back(context->lastStartPoint);
        context->lines.pushDirections.push_back(context->lastPushDirection);

        // Adjusts the line of rotation and displacement of the point cloud.
        adjustLinesOfMovement(context->lines.startPoints, context->lines.pushDirections, objectTransformationPlatform);

        //calculate the rotation from the pushing line. We use the angle to decide how big the sector should be.
        Eigen::AngleAxisf angleAxis = Eigen::AngleAxisf(objectTransformationPlatform.block<3, 3>(0, 0));
        float angle = angleAxis.angle();

        ARMARX_VERBOSE << "angel without transformation: " << angle;
        if (angle < 0)
        {
            angle = 2 * M_PI + angle;
        }
        if (angle > 6.3)
        {
            ARMARX_ERROR << "angel transformation is false!";
        }

        ARMARX_VERBOSE << "The angle with transformation: " << angle;

        // We make sure that the line is not too far away from the center. Not used at the moment.
        // Eigen::Vector3f tempCenterPoint = calculateAverageHypothesisPoint(hypothesisPointList);
        // float maxDistance = (boundaryPoints.block<3, 1>(0,0) - tempCenterPoint).norm();
        // shiftLine(context->lines.startPoints.back(), context->lines.pushDirections.back(), tempCenterPoint, maxDistance);

        // Perhaps the limits as input parameters.
        float limitLow = 80 * M_PI / 180;
        float limitHigh = 280 * M_PI / 180;

        // Decision in which way the object turned.
        // left(counterclock) || right
        if (angle <= limitLow || angle > limitHigh)
        {
            Eigen::Vector3f startPointTemp = context->lines.startPoints.back();
            Eigen::Vector3f pushDirectionTemp = context->lines.pushDirections.back();

            // Normal vector to move the line.
            Eigen::Vector3f normale(0, 0, 0);
            if (pushDirectionTemp(1) != 0 && pushDirectionTemp(0) != 0)
            {
                normale(0) = - pushDirectionTemp(1);
                normale(1) = pushDirectionTemp(0);
                normale(2) = 0.0;
                normale = normale / normale.norm();
            }
            else
            {
                ARMARX_ERROR << "Normale is wrong!!!";
            }

            // The normal vector is definitely right.
            if (chooseSideofPointLinieToLine(pushDirectionTemp, normale) == -1)
            {
                normale *= -1;
            }

            Eigen::Vector3f startPointRight;
            Eigen::Vector3f startPointLeft;
            int excludeSideRight = -1;
            int excludeSideLeft = 1;

            // Calculations for the width of the sector.
            float angleInPercentSide1 = (angle >= limitHigh) ? ((2 * M_PI - angle) / (2 * M_PI - limitHigh)) * 100.0 : 0;
            float angleInPercentSideMinus1 = (angle <= limitLow) ? (angle / limitLow) * 100.0 : 0;
            ARMARX_VERBOSE << "angleInPercentSide1: " << angleInPercentSide1 << ", angleInPercentSideMinus1: " << angleInPercentSideMinus1;

            // Width of shift.
            float extentSide1 = 20.0 + ((extent / 100.0) * angleInPercentSide1);
            float extentSideMinus1 = 20.0 + ((extent / 100.0) * angleInPercentSideMinus1);
            ARMARX_VERBOSE << "extentSide1: " << extentSide1 << ", extentSideMinus1: " << extentSideMinus1;

            startPointRight = startPointTemp + normale * extentSide1;
            startPointLeft = startPointTemp - normale * extentSideMinus1;

            context->lines.startPoints.pop_back();
            context->lines.pushDirections.pop_back();


            //We save the side were CoM is for visualisation.
            Eigen::Vector3i colorPlusOne(1, 0, 0);
            Eigen::Vector3i colorMinusOne(10, 0, 0);

            // Painting the the normal
            //            Eigen::Vector3f startPointRightG = agentPoseMatrix.block<3,3>(0,0) * startPointRight + agentPoseMatrix.block<3,1>(0,3);
            //            Eigen::Vector3f startPointLeftG = agentPoseMatrix.block<3,3>(0,0) * startPointLeft + agentPoseMatrix.block<3,1>(0,3);
            //            Eigen::Vector3f pushDirectionTempG = agentPoseMatrix.block<3,3>(0,0) * pushDirectionTemp;
            //            Eigen::Vector3f plusE =  startPointRightG + pushDirectionTempG*10.0;
            //            Eigen::Vector3f minusE =  startPointLeftG + pushDirectionTempG*10.0;
            //            context->getDebugDrawerTopicProxy()->setLineVisu("line", "hase", new Vector3(startPointRightG), new Vector3(plusE), 5, DrawColor{0,0,1,1}); //blau
            //            context->getDebugDrawerTopicProxy()->setLineVisu("line", "katze", new Vector3(startPointLeftG), new Vector3(minusE), 5, DrawColor{1,0,1,1}); //lila
            // old way to calculate the sector.
            // float Length2 = normaleG.norm();
            // Eigen::Vector3f plus = startPointTempG + ((normale.norm()/Length2) * normaleG);
            // Eigen::Vector3f minus = startPointTempG - ((normale.norm()/Length2) * normaleG);
            // context->getDebugDrawerTopicProxy()->setSphereVisu("Startpoint", "blubbb", new Vector3(startPointTempG), DrawColor{0,0,10,1}, 10.0f); //Blau
            // context->getDebugDrawerTopicProxy()->setLineVisu("line", "hase", new Vector3(startPointTempG), new Vector3(plus), 5, DrawColor{0,0,1,1}); //blau
            // context->getDebugDrawerTopicProxy()->setLineVisu("line", "katze", new Vector3(startPointTempG), new Vector3(minus), 5, DrawColor{1,0,1,1}); //lila

            ARMARX_VERBOSE << "StartPointRight: " << startPointRight << ", startPointLeft: " << startPointLeft;
            context->lines.startPoints.push_back(startPointRight);
            context->lines.pushDirections.push_back(pushDirectionTemp);
            context->lines.sides.push_back(excludeSideRight);
            context->lines.color.push_back(colorPlusOne);
            context->lines.startPoints.push_back(startPointLeft);
            context->lines.pushDirections.push_back(pushDirectionTemp);
            context->lines.sides.push_back(excludeSideLeft);
            context->lines.color.push_back(colorMinusOne);
        }
        else
        {
            ARMARX_VERBOSE << "Rotation was to big. Ignore the line!";
            context->lines.startPoints.pop_back();
            context->lines.pushDirections.pop_back();
        }

    }

    if (!context->lines.startPoints.empty() && in.getExistLastLine())
    {
        ARMARX_VERBOSE << "Last round we added a line and now we calculate the list!";

        // Restrict the area of center of mass
        newHypothesisPointList = excludePointsWithLines(hypothesisPointList, context->lines.startPoints, context->lines.pushDirections, context->lines.sides);

        if (newHypothesisPointList.empty() && in.getExistLastLine())
        {
            ARMARX_VERBOSE << "m_newHypothesisPointList is empty! Last added line is deleted!";
            objectPosition = calculateAverageHypothesisPoint(hypothesisPointList);
            for (size_t i = 1; i <= 2; i++)
            {
                context->lines.startPoints.pop_back();
                context->lines.pushDirections.pop_back();
                context->lines.sides.pop_back();
                context->lines.color.pop_back();
            }
        }
        //this "if" does the same as the "else". Only for future extensions.
        else if (newHypothesisPointList.size() < 100)
        {
            ARMARX_VERBOSE << "Attention, maybe m_newHypothesisPointList size is too small!";

            //Eigen::Vector3f objectPositionOld = calculateAverageHypothesisPoint(hypothesisPointList);
            //Eigen::Vector3f objectPositionNew = calculateAverageHypothesisPoint(newHypothesisPointList);
            //Eigen::Vector3f distanceObjectPositionOldXMax = boundaryPoints.block<3, 1>(0,0) - objectPositionOld;
            //float distancelength = distanceObjectPositionOldXMax.norm();
            //objectPosition = adjustObjectPoint(objectPositionOld, objectPositionNew, distancelength);
            objectPosition = calculateAverageHypothesisPoint(newHypothesisPointList);

            //show the new point cloud data
            for (size_t i = 0; i < newHypothesisPointList.size(); i++)
            {
                Vector3Ptr pointVec = Vector3Ptr::dynamicCast(newHypothesisPointList[i]);
                Eigen::Vector3f pointGlobalVec = agentPoseMatrix.block<3, 3>(0, 0) * pointVec->toEigen() + agentPoseMatrix.block<3, 1>(0, 3);
                context->getDebugDrawerTopicProxy()->setSphereVisu("PickedHypothesispoints", to_string(i), new Vector3(pointGlobalVec), DrawColor{1, 1, 0, 1}, 8.0f); //yellow
            }
        }
        else
        {
            //Eigen::Vector3f objectPositionOld = calculateAverageHypothesisPoint(hypothesisPointList);
            //Eigen::Vector3f objectPositionNew = calculateAverageHypothesisPoint(newHypothesisPointList);
            //Eigen::Vector3f distanceObjectPositionOldXMax = boundaryPoints.block<3, 1>(0,0) - objectPositionOld;
            //float distancelength = distanceObjectPositionOldXMax.norm();
            //objectPosition = adjustObjectPoint(objectPositionOld, objectPositionNew, distancelength);
            objectPosition = calculateAverageHypothesisPoint(newHypothesisPointList);

            //show the new point cloud data
            for (size_t i = 0; i < newHypothesisPointList.size(); i++)
            {
                Vector3Ptr pointVec = Vector3Ptr::dynamicCast(newHypothesisPointList[i]);
                Eigen::Vector3f pointGlobalVec = agentPoseMatrix.block<3, 3>(0, 0) * pointVec->toEigen() + agentPoseMatrix.block<3, 1>(0, 3);
                context->getDebugDrawerTopicProxy()->setSphereVisu("PickedHypothesispoints", to_string(i), new Vector3(pointGlobalVec), DrawColor{1, 1, 0, 1}, 8.0f); //yellow
            }
        }
        ARMARX_IMPORTANT << "Size of new List(added new line): " << newHypothesisPointList.size();
    }
    else if (!context->lines.startPoints.empty())
    {
        ARMARX_VERBOSE << "Last round we did not add a line and now we calculate the list!";
        //adjust the orientation and the movement
        adjustLinesOfMovement(context->lines.startPoints, context->lines.pushDirections, objectTransformationPlatform);

        newHypothesisPointList = excludePointsWithLines(hypothesisPointList, context->lines.startPoints, context->lines.pushDirections, context->lines.sides);

        if (!newHypothesisPointList.empty())
        {
            //Eigen::Vector3f objectPositionOld = calculateAverageHypothesisPoint(hypothesisPointList);
            //Eigen::Vector3f objectPositionNew = calculateAverageHypothesisPoint(newHypothesisPointList);
            //Eigen::Vector3f distanceObjectPositionOldXMax = boundaryPoints.block<3, 1>(0,0) - objectPositionOld;
            //float distancelength = distanceObjectPositionOldXMax.norm();
            //objectPosition = adjustObjectPoint(objectPositionOld, objectPositionNew, distancelength);
            objectPosition = calculateAverageHypothesisPoint(newHypothesisPointList);
            ARMARX_VERBOSE << "m_newHypothesisPointList was calculated";
            //show the new point cloud data
            for (size_t i = 0; i < newHypothesisPointList.size(); i++)
            {
                Vector3Ptr pointVec = Vector3Ptr::dynamicCast(newHypothesisPointList[i]);
                Eigen::Vector3f pointGlobalVec = agentPoseMatrix.block<3, 3>(0, 0) * pointVec->toEigen() + agentPoseMatrix.block<3, 1>(0, 3);
                context->getDebugDrawerTopicProxy()->setSphereVisu("PickedHypothesispoints", to_string(i), new Vector3(pointGlobalVec), DrawColor{0, 0, 0, 1}, 8.0f);
            }
        }
        else
        {
            ARMARX_VERBOSE << "List with hypoth. is empty, although we have lines.";
            objectPosition = calculateAverageHypothesisPoint(hypothesisPointList);
        }
    }
    else
    {
        ARMARX_VERBOSE << "List with lines is empty and size of m_hypothesisPointList: " << hypothesisPointList.size();
        objectPosition = calculateAverageHypothesisPoint(hypothesisPointList);
    }

    //B: End of the part, in which the center of gravity is calculated.

    //C: The beginning of the part where the keypoints are calculated.

    //Paint all existing (sector-)lines

    if (!context->lines.pushDirections.empty())
    {
        std::list<Eigen::Vector3f>::iterator startP = context->lines.startPoints.begin();
        std::list<Eigen::Vector3f>::iterator directionP = context->lines.pushDirections.begin();
        std::list<Eigen::Vector3i>::iterator colorP = context->lines.color.begin();
        int i = 1000;
        while (startP != context->lines.startPoints.end())
        {
            Eigen::Vector3f startPointG = agentPoseMatrix.block<3, 3>(0, 0) * *startP + agentPoseMatrix.block<3, 1>(0, 3);
            Eigen::Vector3f pushDirectionG = agentPoseMatrix.block<3, 3>(0, 0) * *directionP ;
            float Length1 = pushDirectionG.norm();
            Eigen::Vector3f oldLineG1 = startPointG + ((200 / Length1) * pushDirectionG);
            Eigen::Vector3f oldLineG2 = startPointG - ((200 / Length1) * pushDirectionG);
            context->getDebugDrawerTopicProxy()->setLineVisu("line", to_string(i), new Vector3(startPointG), new Vector3(oldLineG1), 5, DrawColor{0, 0, 1, 1}); //blue
            context->getDebugDrawerTopicProxy()->setLineVisu("line", to_string(i), new Vector3(startPointG), new Vector3(oldLineG2), 5, DrawColor{0, 0, 1, 1}); //blue
            //            Eigen::Vector3i coloring = *colorP;
            //            Ice::Float one = (float)coloring(0);
            //            Ice::Float two = (float)coloring(1);
            //            Ice::Float drei = (float)coloring(2);
            //            context->getDebugDrawerTopicProxy()->setSphereVisu("Startpoint", to_string(i), new Vector3(startPointG), DrawColor{one,two,drei,1}, 10.0f);
            startP++;
            directionP++;
            colorP++;
            i += 20;
        }
    }


    // Old way for calculate the pushDirection
    {
        //pushingDirection(2) = 0;
        //create a pushing direction that is mainly in x direction
        //pushingDirection(0) *= 2;
        //if (pushingDirection.norm() == 0) pushingDirection(0) = 1;
        //if (fabs(pushingDirection(0)) < fabs(pushingDirection(1))) pushingDirection(0) *= fabs(pushingDirection(1)) / fabs(pushingDirection(0));
        //pushingDirection = pushingDirection/pushingDirection.norm();
    }

    //Paint the boundaryPoints
    {
        Eigen::Vector3f boundaryPoints1 = boundaryPoints.block<3, 1>(0, 0);
        Eigen::Vector3f boundaryPoints2 = boundaryPoints.block<3, 1>(0, 1);
        Eigen::Vector3f boundaryPoints3 = boundaryPoints.block<3, 1>(0, 2);
        Eigen::Vector3f boundaryPoints4 = boundaryPoints.block<3, 1>(0, 3);

        Eigen::Vector3f boundaryPoints1G = agentPoseMatrix.block<3, 3>(0, 0) * boundaryPoints1 + agentPoseMatrix.block<3, 1>(0, 3);
        boundaryPoints1G(2) = 0;
        Eigen::Vector3f boundaryPoints2G = agentPoseMatrix.block<3, 3>(0, 0) * boundaryPoints2 + agentPoseMatrix.block<3, 1>(0, 3);
        boundaryPoints2G(2) = 0;
        Eigen::Vector3f boundaryPoints3G = agentPoseMatrix.block<3, 3>(0, 0) * boundaryPoints3 + agentPoseMatrix.block<3, 1>(0, 3);
        boundaryPoints3G(2) = 0;
        Eigen::Vector3f boundaryPoints4G = agentPoseMatrix.block<3, 3>(0, 0) * boundaryPoints4 + agentPoseMatrix.block<3, 1>(0, 3);
        boundaryPoints4G(2) = 0;
        //        context->getDebugDrawerTopicProxy()->setSphereVisu("boundaryPoints", "Regen", new Vector3(boundaryPoints1G), DrawColor{0,0,1,1}, 5.0f); //xL Blau
        //        context->getDebugDrawerTopicProxy()->setSphereVisu("boundaryPoints", "Sonne", new Vector3(boundaryPoints2G), DrawColor{0,1,0,1}, 5.0f); //yL Grün
        //        context->getDebugDrawerTopicProxy()->setSphereVisu("boundaryPoints", "Nebel", new Vector3(boundaryPoints3G), DrawColor{1,0,0,1}, 5.0f); //xS Rot
        //        context->getDebugDrawerTopicProxy()->setSphereVisu("boundaryPoints", "Sturm", new Vector3(boundaryPoints4G), DrawColor{0,0,0,1}, 5.0f); //yS Schwarz

        Eigen::Vector3f singular1G = agentPoseMatrix.block<3, 3>(0, 0) * principalAxis1;
        Eigen::Vector3f singular2G = agentPoseMatrix.block<3, 3>(0, 0) * principalAxis2;


        Eigen::Vector3f ende1 = boundaryPoints1G + 200 * singular2G;
        ende1(2) = 0;
        Eigen::Vector3f ende2 = boundaryPoints2G + 200 * singular1G;
        ende2(2) = 0;
        Eigen::Vector3f ende3 = boundaryPoints3G + 200 * singular2G;
        ende3(2) = 0;
        Eigen::Vector3f ende4 = boundaryPoints4G + 200 * singular1G;
        ende4(2) = 0;
        Eigen::Vector3f ende5 = boundaryPoints1G - 200 * singular2G;
        ende5(2) = 0;
        Eigen::Vector3f ende6 = boundaryPoints2G - 200 * singular1G;
        ende6(2) = 0;
        Eigen::Vector3f ende7 = boundaryPoints3G - 200 * singular2G;
        ende7(2) = 0;
        Eigen::Vector3f ende8 = boundaryPoints4G - 200 * singular1G;
        ende8(2) = 0;

        context->getDebugDrawerTopicProxy()->setLineVisu("inter", "Kath", new Vector3(boundaryPoints1G), new Vector3(ende1), 5, DrawColor{0, 0, 0, 1}); //xL
        context->getDebugDrawerTopicProxy()->setLineVisu("inter", "Evg", new Vector3(boundaryPoints2G), new Vector3(ende2), 5, DrawColor{0, 0, 1, 1}); //yL
        context->getDebugDrawerTopicProxy()->setLineVisu("inter", "Buddah", new Vector3(boundaryPoints3G), new Vector3(ende3), 5, DrawColor{0, 1, 0, 1}); //xS
        context->getDebugDrawerTopicProxy()->setLineVisu("inter", "Satan", new Vector3(boundaryPoints4G), new Vector3(ende4), 5, DrawColor{1, 0, 0, 1}); //yS
        context->getDebugDrawerTopicProxy()->setLineVisu("inter", "Koran", new Vector3(boundaryPoints1G), new Vector3(ende5), 5, DrawColor{0, 0, 0, 1}); //xL
        context->getDebugDrawerTopicProxy()->setLineVisu("inter", "Bibel", new Vector3(boundaryPoints2G), new Vector3(ende6), 5, DrawColor{0, 0, 1, 1}); //yL
        context->getDebugDrawerTopicProxy()->setLineVisu("inter", "Kirche", new Vector3(boundaryPoints3G), new Vector3(ende7), 5, DrawColor{0, 1, 0, 1}); //xS
        context->getDebugDrawerTopicProxy()->setLineVisu("inter", "Atheist", new Vector3(boundaryPoints4G), new Vector3(ende8), 5, DrawColor{1, 0, 0, 1}); //yS

    }

    // Für die Evaluierung. Gibt die Abstände der Winkel und Position wieder.
    {
        Eigen::Vector3f  objectPositionEvaluierung = calculateAverageHypothesisPoint(hypothesisPointList);
        objectPositionEvaluierung(2) = 0;
        Eigen::Vector3f  centralPointZ = centralPoint;
        centralPointZ(2) = 0;
        Eigen::Vector3f distanceEvaluierung = objectPositionEvaluierung - centralPointZ;
        float distanceEvaluierungLentgh = distanceEvaluierung.norm();
        ARMARX_IMPORTANT << "In Durchlauf: " << counterValue << "Evaluierung: Schieben, Abstand: " << distanceEvaluierungLentgh;
        // Für die Evaluierung Drehen:
        double angleEvaluierung;
        Eigen::Vector3f xMax = boundaryPoints.block<3, 1>(0, 0);
        Eigen::Vector3f xMin = boundaryPoints.block<3, 1>(0, 2);
        Eigen::Vector3f intersectionXMax = intersectionTwoLines(objectPosition, principalAxis1, xMax, principalAxis2);
        Eigen::Vector3f intersectionXMin = intersectionTwoLines(objectPosition, principalAxis1, xMin, principalAxis2);
        Eigen::Vector3f movingAxisLarge = intersectionXMax - objectPosition;
        movingAxisLarge(2) = 0;
        Eigen::Vector3f movingAxisSmall = intersectionXMin - objectPosition;
        movingAxisSmall(2) = 0;
        Eigen::Vector3f movingAxis;
        Eigen::Vector3f normaleX(1.0, 0, 0);
        // The targetVector is the desired target orientation.
        Eigen::Vector3f targetVector = rotateAxis(normaleX, 70.0, -1);
        targetVector(2) = 0;

        double angelLarge = calculateAngel(movingAxisLarge);
        double angelSmall = calculateAngel(movingAxisSmall);
        double angeltargetVector = calculateAngel(targetVector);

        if (fabs(angelLarge - angeltargetVector) < fabs(angelSmall - angeltargetVector))
        {
            movingAxis = movingAxisLarge;
            angleEvaluierung = fabs(angelLarge - angeltargetVector);
        }
        else
        {
            movingAxis = movingAxisSmall;
            angleEvaluierung = fabs(angelSmall - angeltargetVector);
        }

        ARMARX_IMPORTANT << "In Durchlauf: " << counterValue << "Evaluierung: Drehen, Winkel: " << angleEvaluierung;
    }

    // Old way. Rotates the object by a predetermined angle.
    if (false)
    {

        Eigen::Vector3f directionXLargest = boundaryPoints.block<3, 1>(0, 0);
        Eigen::Vector3f directionXSmalles = boundaryPoints.block<3, 1>(0, 2);
        Eigen::Vector3f movingAxis;
        // With this configuration, it is attempted to remove the object back as far from the centerPoint. If you don't want this use >.
        //Define movingAxis
        if (objectPosition(1) < centralPoint(1))
        {
            if (directionXLargest(1) > directionXSmalles(1))
            {
                movingAxis = directionXLargest - objectPosition;
            }
            else
            {
                movingAxis = directionXSmalles - objectPosition;
            }
        }
        else
        {
            if (directionXLargest(1) < centralPoint(1))
            {
                movingAxis = directionXLargest - objectPosition;
            }
            else
            {
                movingAxis = directionXSmalles - objectPosition;
            }
        }

        movingAxis(2) = 0;
        //Rotates the vector by 70 degrees clockwise. Attention: Don't use 90 or 180 degrees!
        Eigen::Vector3f targetVector = rotateAxis(movingAxis, 70.0, -1);
        targetVector(2) = 0;

        // The new objectPosition for the turn.
        Eigen::Vector3f referencePoint = objectPosition + (movingAxis * 0.7);

        Eigen::Vector3f movingAxisVertical;
        movingAxisVertical(0) = movingAxis(1) * (-1);
        movingAxisVertical(1) = movingAxis(0);
        movingAxisVertical(2) = 0;
        movingAxisVertical = movingAxisVertical / movingAxisVertical.norm();

        Eigen::Vector3f targetVectorNorm = targetVector / targetVector.norm();
        Eigen::Vector3f targetPosition = intersectionTwoLines(referencePoint, movingAxisVertical, objectPosition, targetVectorNorm);

        // Old approach: adjustKeypointforTurn(objectPosition, pushingDirection, 10.0);
        // check if objectPosition is inside the workspace
        // Eigen::Vector3f objectPositionClippedToWorkspaceLimits = objectPosition;
        // clipToWorkspaceLimits(objectPositionClippedToWorkspaceLimits, workspaceLimitsLow, workspaceLimitsHigh); //hier vielleicht noch den Code von oben einfügen

        objectPosition = referencePoint;
        centralPoint = targetPosition;

    }

    // Rotate the object in a predetermined orientation.
    //if (!in.getNoTurn())
    if (!(counterValue % 2 == 0))
    {
        Eigen::Vector3f xMax = boundaryPoints.block<3, 1>(0, 0);
        Eigen::Vector3f xMin = boundaryPoints.block<3, 1>(0, 2);

        Eigen::Vector3f intersectionXMax = intersectionTwoLines(objectPosition, principalAxis1, xMax, principalAxis2);
        Eigen::Vector3f intersectionXMin = intersectionTwoLines(objectPosition, principalAxis1, xMin, principalAxis2);

        // The pricipal axis. One in the direction of xMax and the other in the direction of xMin.
        // The alignment is previously specified by the targetVector. We choose the axis that is closer to the targetVector.
        Eigen::Vector3f movingAxisLarge = intersectionXMax - objectPosition;
        movingAxisLarge(2) = 0;
        Eigen::Vector3f movingAxisSmall = intersectionXMin - objectPosition;
        movingAxisSmall(2) = 0;
        Eigen::Vector3f movingAxis;

        // The axes, between the objectPosition and the largest (directionXLargest) or smallest (directionXSmalles) x value is to be aligned.
        // The alignment is previously specified by the targetVector. We choose the axis that is closer to the targetVector.
        // Eigen::Vector3f movingAxisLarge = xMax - objectPosition;
        // movingAxisLarge(2) = 0;
        // Eigen::Vector3f movingAxisSmall = xMin - objectPosition;
        // movingAxisSmall(2) = 0;
        // Eigen::Vector3f movingAxis;


        // Define targeting
        Eigen::Vector3f normaleX(1.0, 0, 0);
        // The targetVector is the desired target orientation.
        Eigen::Vector3f targetVector = rotateAxis(normaleX, 70.0, -1);
        targetVector(2) = 0;

        double angelLarge = calculateAngel(movingAxisLarge);
        double angelSmall = calculateAngel(movingAxisSmall);
        double angeltargetVector = calculateAngel(targetVector);

        if (fabs(angelLarge - angeltargetVector) < fabs(angelSmall - angeltargetVector))
        {
            movingAxis = movingAxisLarge;
        }
        else
        {
            movingAxis = movingAxisSmall;
        }

        float test = movingAxis.dot(targetVector);


        if (fabs(test) < 0.05)
        {
            ARMARX_ERROR << "Attention: The angel is 90 degree. We can't calculate the keypoints.";
            targetVector = rotateAxis(targetVector, 10.0, -1);
            targetVector(2) = 0;
        }

        // Painting targetVector and movingAxis
        Eigen::Vector3f movingAxisG = agentPoseMatrix.block<3, 3>(0, 0) * movingAxis;
        movingAxisG(2) = 0;
        Eigen::Vector3f targetVectorG = agentPoseMatrix.block<3, 3>(0, 0) * targetVector;
        targetVectorG(2) = 0;
        targetVectorG = targetVectorG / targetVectorG.norm();
        movingAxisG = movingAxisG / movingAxisG.norm();
        Eigen::Vector3f tempblubb = centralPointG + 100 * targetVectorG;
        Eigen::Vector3f tempblubb2 = centralPointG + 100 * movingAxisG;
        context->getDebugDrawerTopicProxy()->setLineVisu("flasche", "Tee", new Vector3(centralPointG), new Vector3(tempblubb), 15, DrawColor{0, 1, 0, 1});
        context->getDebugDrawerTopicProxy()->setLineVisu("flasche", "Rum", new Vector3(centralPointG), new Vector3(tempblubb2), 15, DrawColor{1, 0, 0, 1});

        // The new objectPosition for the turn.
        //Achtung: Aus Verschiebung Inputparam. machen!
        Eigen::Vector3f referencePoint = objectPosition + (movingAxis * 0.6);

        // You can decide on which way to shift the object.
        // 1. PushDirection is vertical to the movingAxis.
        // 2. PushDirection is vertical to the targetVector.
        // 3. We use the mean of the results. -> implemented

        // Eigen::Vector3f movingAxisVertical;
        // movingAxisVertical(0) = - movingAxis(1);
        // movingAxisVertical(1) = movingAxis(0);
        // movingAxisVertical(2) = 0;
        // Eigen::Vector3f targetPosition = intersectionTwoLines(referencePoint, movingAxisVertical, objectPosition, targetVector);

        // Eigen::Vector3f targetVectorVertical;
        // targetVectorVertical(0) = - targetVector(1);
        // targetVectorVertical(1) = targetVector(0);
        // targetVectorVertical(2) = 0;
        // Eigen::Vector3f targetPosition = intersectionTwoLines(referencePoint,targetVectorVertical , objectPosition, targetVector);

        // Eigen::Vector3f diffTargetPositions = targetPositionMovingAxis -targetPositiontargetVector;

        // Eigen::Vector3f targetPosition = objectPosition + (targetVector/targetVector.norm() *
        //                                                           (diffTargetPositions.norm()/2.0 + (targetPositiontargetVector-objectPosition).norm()));

        // Eigen::Vector3f targetPosition = intersectionTwoLines(referencePoint, movingAxisVertical, objectPosition, targetVector);

        // *********************************************************************
        // In this way the calculated point is pushed around the rotation angle.
        Eigen::Vector3f targetPosition = objectPosition + targetVector / targetVector.norm() * movingAxis.norm() * 0.7;

        objectPosition = referencePoint;
        centralPoint = targetPosition;
    }

    //Paint the objectPostion on the floor.
    Eigen::Vector3f objectPositionZG = agentPoseMatrix.block<3, 3>(0, 0) * objectPosition + agentPoseMatrix.block<3, 1>(0, 3);
    objectPositionZG(2) = 0;
    context->getDebugDrawerTopicProxy()->setSphereVisu("CentralPoint", "Wurm", new Vector3(objectPositionZG), DrawColor{10, 10, 0, 1}, 10.0f); //Gelb

    //Determine pushing direction. The direction is from the object center to centralPoint. (Dann wird die z Komponente auf 0 gesetzt. Die Länge wird erhalten)
    Eigen::Vector3f pushDirection = centralPoint - objectPosition;
    pushDirection(2) = 0;
    float pushLength = pushDirection.norm();
    pushDirection = pushDirection / pushLength;

    //Intersection of line objectPosition + pushDirection with all lines from the boundary rectangle.
    //We choose the intersection where the sign of the scalar factor before the direction vector is negative and has the minimal absolute value.
    //
    //                     CentralPoint @@@@@
    //                                @@@@@@@@
    //                         @@@@@@@@@ @@@
    //                                  @@ @
    //  boundary rectangle             @@ @
    // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    // @                              @@           @
    // @                             @@            @
    // @                            @@             @
    // @                           @@              @
    // @                         @@@               @
    // @                      @@@@@@               @
    // @                     @@@@@@@@              @
    // @                     @@@@@@@ ObjectPosition@
    // @                     @@@@@@                @
    // @                      @@@@                 @
    // @                     @@                    @
    // @      PushDirection @@                     @
    // @                   @@                      @
    // @                  @@                       @
    // @                 @@                        @
    // @                @@                         @
    // @             !!!!@   IntersectionPoint     @
    // @@@@@@@@@@@@@!!!!!!@@@@@@@@@@@@@@@@@@@@@@@@@@
    //               @!!!
    //              @@
    //         @@
    //

    Eigen::Vector3f intersectionPoint = intersection(objectPosition, pushDirection, boundaryPoints, principalAxis1, principalAxis2);
    //clipToWorkspaceLimits(intersectionPoint, workspaceLimitsLow, workspaceLimitsHigh); //-> zurück anpassen der Werte...
    intersectionPoint(2) = 0;

    // Paint the intersection on the floor
    Eigen::Vector3f intersectionPointG1 = agentPoseMatrix.block<3, 3>(0, 0) * intersectionPoint + agentPoseMatrix.block<3, 1>(0, 3);
    intersectionPointG1(2) = 0;
    context->getDebugDrawerTopicProxy()->setSphereVisu("boundaryPoints", "Blasen", new Vector3(intersectionPointG1), DrawColor{0, 0, 0, 1}, 20.0f);

    // check if object is reachable.
    bool transitionStartPushing = true;
    if (!isInWorkspaceLimits(objectPosition, workspaceLimitsLow, workspaceLimitsHigh))
    {
        ARMARX_ERROR << "Object unreachable!";
        ARMARX_VERBOSE << "objectPosition: " << objectPosition;
        transitionStartPushing = false;
    }
    ARMARX_VERBOSE << "objectPosition reachable: " << objectPosition;
    intersectionPoint(2) = objectPosition(2);

    // calculate pushing keypoints
    // keypoint 1 *************************************************
    Eigen::Vector3f pushingKeypoint1  = intersectionPoint - pushDirection * (in.getHandRadius() / 2.0);
    pushingKeypoint1(2) += in.getPrePushHeightOffset();
    pushingKeypoint1 += manualOffset;

    //keypoint 2 (and keypoint 1) *********************************
    Eigen::Vector3f pushingKeypoint2  = intersectionPoint - pushDirection * (in.getHandRadius() / 2.0);
    pushingKeypoint2 += manualOffset;
    if (!isInWorkspaceLimits(pushingKeypoint2, workspaceLimitsLow, workspaceLimitsHigh))
    {
        ARMARX_VERBOSE << "PushingKeypoint2 is not in the workspace.";
        pushingKeypoint2 = objectPosition;
        //pushingKeypoint2(2) += 3.0;//(in.getHandRadius()*0.8);
        pushingKeypoint1 = objectPosition;
        pushingKeypoint1(2) += in.getPrePushHeightOffset();
    }

    Eigen::Vector3f pushingKeypoint2G = agentPoseMatrix.block<3, 1>(0, 3) + agentPoseMatrix.block<3, 3>(0, 0) * pushingKeypoint2;
    //if (!in.getNoTurn())
    if (!(counterValue % 2 == 0))
    {
        context->getDebugDrawerTopicProxy()->setSphereVisu("Pushingkeypoint", "PK2", new Vector3(pushingKeypoint2G), DrawColor{0, 5, 0, 1}, 10.0f); //Grün
    }
    else
    {
        context->getDebugDrawerTopicProxy()->setSphereVisu("Pushingkeypoint", "PK2", new Vector3(pushingKeypoint2G), DrawColor{1, 3, 3, 1}, 10.0f); //Bläulich Weiß
    }

    // keypoint 3*************************************************
    Eigen::Vector3f pushingKeypoint3 = pushingKeypoint2 + pushDirection * (pushLength + in.getHandRadius());
    pushingKeypoint3 += manualOffset;
    clipPointToWorkspaceLimitConsideringDirection(pushingKeypoint3, pushDirection, workspaceLimitsLow, workspaceLimitsHigh);
    //ClipPointToWorkspaceLimitConsideringDirection(pushingKeypoint3, directionFiller, pushingDirectionWithSlidingLength, workspaceLimitsLow, workspaceLimitsHigh);
    Eigen::Vector3f pushingKeypoint3G = agentPoseMatrix.block<3, 1>(0, 3) + agentPoseMatrix.block<3, 3>(0, 0) * pushingKeypoint3;
    //if (!in.getNoTurn())
    if (!(counterValue % 2 == 0))
    {
        context->getDebugDrawerTopicProxy()->setSphereVisu("Pushingkeypoint", "PK3", new Vector3(pushingKeypoint3G), DrawColor{0, 5, 0, 1}, 15.0f); //Grün
    }
    else
    {
        context->getDebugDrawerTopicProxy()->setSphereVisu("Pushingkeypoint", "PK3", new Vector3(pushingKeypoint3G), DrawColor{1, 3, 3, 1}, 15.0f); //Bläulich Weiß
    }

    // keypoint 4*************************************************
    Eigen::Vector3f pushingKeypoint4 = pushingKeypoint2 + pushDirection * (pushLength + in.getHandRadius());
    pushingKeypoint4(2) += in.getPrePushHeightOffset();
    pushingKeypoint4 += manualOffset;
    clipPointToWorkspaceLimitConsideringDirection(pushingKeypoint4, pushDirection, workspaceLimitsLow, workspaceLimitsHigh);

    FramedPosition pushingKeypoint1Framed(pushingKeypoint1, rootRobotNodeName, agentName);
    out.setPushingKeypoint1(pushingKeypoint1Framed);
    ARMARX_VERBOSE << "Pushing keypoint 1: " << pushingKeypoint1;

    FramedPosition pushingKeypoint2Framed(pushingKeypoint2, rootRobotNodeName, agentName);
    out.setPushingKeypoint2(pushingKeypoint2Framed);
    ARMARX_VERBOSE << "Pushing keypoint 2: " << pushingKeypoint2;

    FramedPosition pushingKeypoint3Framed(pushingKeypoint3, rootRobotNodeName, agentName);
    out.setPushingKeypoint3(pushingKeypoint3Framed);
    ARMARX_VERBOSE << "Pushing keypoint 3: " << pushingKeypoint3;

    FramedPosition pushingKeypoint4Framed(pushingKeypoint4, rootRobotNodeName, agentName);
    out.setPushingKeypoint4(pushingKeypoint4Framed);
    ARMARX_VERBOSE << "Pushing keypoint 4: " << pushingKeypoint4;

    // choose which arm is used for pushing
    if (pushDirection(0) > 0)
    {
        ARMARX_IMPORTANT << "Using left arm: " << in.getKinematicChainNameLeftArm();
        out.setKinematicChainName(in.getKinematicChainNameLeftArm());
        out.setHandMemoryChannel(in.getLeftHandMemoryChannel());
    }
    else
    {
        ARMARX_IMPORTANT << "Using right arm: " << in.getKinematicChainNameRightArm();
        out.setKinematicChainName(in.getKinematicChainNameRightArm());
        out.setHandMemoryChannel(in.getRightHandMemoryChannel());
    }


    //if (in.getNoTurn() && counterValue < 6 && context->lines.pushDirections.size() < 5)
    if ((counterValue % 2 == 0) && counterValue < 6 && context->lines.pushDirections.size() < 5)
    {
        context->lastStartPoint = pushingKeypoint2;
        context->lastPushDirection = pushingKeypoint3 - pushingKeypoint2;
        out.setExistLastLine(true);
        ARMARX_VERBOSE << "new lines were added";
    }
    else
    {
        out.setExistLastLine(false);
    }

    out.setMinimalAllowedHeightForTCP(workspaceLimitsLow(2));
    out.setWaitingTimeAfterPushing(4000);
    out.setObjectPosition(objectPosition);
    out.setPushDirection(pushDirection);
    if (transitionStartPushing)
    {
        ARMARX_IMPORTANT << "Emit StartPushing!";
        emitStartPushing();
    }
    else
    {
        ARMARX_IMPORTANT << "Emit ObjectNotReachable!";
        emitObjectNotReachable();
    }


}
*/

/*
bool CalculateKeypoints::isInWorkspaceLimits(Eigen::Vector3f& point, const Eigen::Vector3f& workspaceLimitsLow, const Eigen::Vector3f& workspaceLimitsHigh)
{
    point(2) = (point(2) < workspaceLimitsLow(2)) ? workspaceLimitsLow(2) : ((point(2) > workspaceLimitsHigh(2)) ? workspaceLimitsHigh(2) : point(2));
    return point(0) >= workspaceLimitsLow(0) && point(1) >= workspaceLimitsLow(1) && point(2) >= workspaceLimitsLow(2) && point(0) <= workspaceLimitsHigh(0) && point(1) <= workspaceLimitsHigh(1) && point(2) <= workspaceLimitsHigh(2);
}
*/

/*
// Check whether a point is in the workspace located, and if not, he will be moved to them.
// But while maintaining the direction in wich he should move. The direction vector is also clipped.
void CalculateKeypoints::clipPointToWorkspaceLimitConsideringDirection(Eigen::Vector3f& point, Eigen::Vector3f pointDirection, const Eigen::Vector3f& workspaceLimitsLow,
        const Eigen::Vector3f& workspaceLimitsHigh)
{

    Eigen::Vector3f workspaceLimitsLowZzero = workspaceLimitsLow;
    workspaceLimitsLowZzero(2) = 0;
    Eigen::Vector3f workspaceLimitsHighZzero = workspaceLimitsHigh;
    workspaceLimitsHighZzero(2) = 0;

    Eigen::Vector3f directionX(1.0, 0.0, 0.0);
    Eigen::Vector3f directionY(0.0, 1.0, 0.0);


    if (point(0) < workspaceLimitsLow(0))
    {
        point = intersectionTwoLines(point, pointDirection, workspaceLimitsLowZzero, directionY);
        ARMARX_VERBOSE << "Keypoints are clip to workspacelimits. The direction vector is also clipped. X low " << point;
    }
    else if (point(0) > workspaceLimitsHigh(0))
    {
        point = intersectionTwoLines(point, pointDirection, workspaceLimitsHighZzero, directionY);
        ARMARX_VERBOSE << "Keypoints are clip to workspacelimits. The direction vector is also clipped. X hight " << point;
    }

    if (point(1) < workspaceLimitsLow(1))
    {
        point = intersectionTwoLines(point, pointDirection, workspaceLimitsLowZzero, directionX);
        ARMARX_VERBOSE << "Keypoints are clip to workspacelimits. The direction vector is also clipped. Y low " << point;
    }
    else if (point(1) > workspaceLimitsHigh(1))
    {
        point = intersectionTwoLines(point, pointDirection, workspaceLimitsHighZzero, directionX);
        ARMARX_VERBOSE << "Keypoints are clip to workspacelimits. The direction vector is also clipped. Y hight " << point;
    }

    if (point(2) < workspaceLimitsLow(2))
    {
        point(2) = workspaceLimitsLow(2);
        ARMARX_VERBOSE << "Keypoints are clip to workspacelimits. The direction vector is also clipped. Z low";
    }
    else if (point(2) > workspaceLimitsHigh(2))
    {
        point(2) = workspaceLimitsHigh(2);
        ARMARX_VERBOSE << "Keypoints are clip to workspacelimits. The direction vector is also clipped. z hight";
    }

}
*/

/*
//Calcualte intersection between only two lines
Eigen::Vector3f CalculateKeypoints::intersectionTwoLines(Eigen::Vector3f point, Eigen::Vector3f pointDirection, Eigen::Vector3f limit, Eigen::Vector3f limitDirection)
{
    float tempZ = point(2);
    point(2) = 0;
    pointDirection(2) = 0;
    pointDirection = pointDirection / pointDirection.norm();

    limit(2) = 0.0;
    limitDirection(2) = 0.0;
    limitDirection = limitDirection / limitDirection.norm();
    ARMARX_VERBOSE << "limitDirection: " << limitDirection;

    Eigen::Vector3f point2 = point + pointDirection;
    Eigen::Vector3f limit2 = limit + limitDirection;

    ARMARX_VERBOSE << "point: " << point;
    ARMARX_VERBOSE << "limit: " << limit;
    ARMARX_VERBOSE << "point2: " << point2;
    ARMARX_VERBOSE << "limit2: " << limit2;

    float intersectionX, intersectionY, check;

    //check = (limit2(1) - limit(1)) * (point2(0) - point(0)) - (point2(1) - point(1)) * (limit2(0) - limit(0));
    check = (point(0) - point2(0)) * (limit(1) - limit2(1)) - (point(1) - point2(1)) * (limit(0) - limit2(0));
    ARMARX_VERBOSE << " , check: " << check;
    if (check == 0)
    {
        ARMARX_ERROR << "No intersection!!";
    }

    intersectionX = (point(0) * point2(1) - point(1) * point2(0)) * (limit(0) - limit2(0)) - (point(0) - point2(0)) * (limit(0) * limit2(1) - limit(1) * limit2(0));
    intersectionX = intersectionX / check;
    intersectionY = (point(0) * point2(1) - point(1) * point2(0)) * (limit(1) - limit2(1)) - (point(1) - point2(1)) * (limit(0) * limit2(1) - limit(1) * limit2(0));
    intersectionY = intersectionY / check;

    ARMARX_VERBOSE << "intersectionX: " << intersectionX << ", intersectionY: " << intersectionY << " , check: " << check;
    Eigen::Vector3f intersection;
    intersection(0) = intersectionX;
    intersection(1) = intersectionY;
    intersection(2) = tempZ;

    //ARMARX_VERBOSE<<"Intersection Point: "<<intersection;
    return intersection;
}
*/

/*
//Calcualte intersection between only two lines
Eigen::Vector3f CalculateKeypoints::intersectionTwoLinesNaive(Eigen::Vector3f point, Eigen::Vector3f pointDirection, Eigen::Vector3f limit, Eigen::Vector3f limitDirection)
{
    float tempZ = point(2);
    point(2) = 0;
    pointDirection(2) = 0;
    pointDirection = pointDirection / pointDirection.norm();

    limit(2) = 0.0;
    limitDirection(2) = 0.0;
    limitDirection = limitDirection / limitDirection.norm();

    float lambda, mue;
    // Achtung: Zweiter 0 Überprüfer fehlt!
    if (pointDirection(0) == 0)
    {
        ARMARX_ERROR << "intersectionTwoLines: we have a zero divisor. We change the x direction a little bit!";
    }

    mue = (point(1) - limit(1) + pointDirection(1) / pointDirection(0) * limit(0) - pointDirection(1) / pointDirection(0) * point(0))
          / limitDirection(1) - pointDirection(1) / pointDirection(0) * limitDirection(0);

    lambda = (limit(0) - point(0) + mue * limitDirection(0)) / pointDirection(0);

    Eigen::Vector3f intersectionOne = point + lambda * pointDirection;
    Eigen::Vector3f intersectionTwo = limit + mue * limitDirection;

    Eigen::Vector3f error = intersectionOne - intersectionTwo;
    if (error.norm() <= 0.05)
    {
        intersectionOne(2) = tempZ;
    }
    else
    {
        ARMARX_ERROR << "No intersection";
    }
    return intersectionOne;
}
*/

/*
//calculate the average from all hypotheses
Eigen::Vector3f CalculateKeypoints::calculateAverageHypothesisPoint(visionx::types::PointList hypothesisPointList)
{
    Eigen::Vector3f summhypothesisPoint(0, 0, 0);
    Eigen::Vector3f pointGlobalVec(0, 0, 0);
    for (size_t i = 0; i < hypothesisPointList.size(); i++)
    {
        Vector3Ptr pointVec = Vector3Ptr::dynamicCast(hypothesisPointList[i]);
        pointGlobalVec = pointVec->toEigen();

        summhypothesisPoint(0) += pointGlobalVec(0);
        summhypothesisPoint(1) += pointGlobalVec(1);
        summhypothesisPoint(2) += pointGlobalVec(2);
    }
    summhypothesisPoint(0) = summhypothesisPoint(0) / hypothesisPointList.size();
    summhypothesisPoint(1) = summhypothesisPoint(1) / hypothesisPointList.size();
    summhypothesisPoint(2) = summhypothesisPoint(2) / hypothesisPointList.size();
    ARMARX_VERBOSE << "New center point: " << summhypothesisPoint;

    return summhypothesisPoint;
}
*/

/*
//calculate angle from one line
double CalculateKeypoints::calculateAngel(Eigen::Vector3f line)
{
    //    double angle1 = atan2((double) line(1), (double) line(0)) * 180.0 / 3.14159265;
    //    double angle = std::fmod((angle1+360), 360.0);
    double angle = atan2((double) line(1), (double) line(0));
    // Result is given in radian. (0 - 6.8)
    if (angle < 0)
    {
        return 2 * M_PI + angle;
    }
    else
    {
        return angle;
    }
}
*/

/*
// CoM = Center of Mass. Calculate the side from a point to a line.
int CalculateKeypoints::chooseSideofPointLinieToLine(Eigen::Vector3f line, Eigen::Vector3f pointLine)
{
    double angleLine = calculateAngel(line);
    double anglePoint = calculateAngel(pointLine);
    double angleLinePlus = angleLine + 3.14;
    double angleLineMinus = angleLine - 3.14;
    int side;
    if ((angleLine - 3.14) >= 0)
    {
        if (anglePoint <= angleLine && angleLineMinus < anglePoint)
        {
            side = 1;
        }
        else
        {
            side = -1;
        }
    }
    else
    {
        if (anglePoint > angleLine && angleLinePlus >= anglePoint)
        {
            side = -1;
        }
        else
        {
            side = 1;
        }
    }
    //ARMARX_VERBOSE<<"angleLine and anglePoint: "<<angleLine<<", "<<anglePoint<<" and side: "<<side;
    return side;
}
*/

/*
// Rules the sector, where the CoM can be. Not used at the moment
int CalculateKeypoints::chooseSectorWidth(Eigen::Vector3f line1, Eigen::Vector3f line2)
{
    double angle1 = calculateAngel(line1);
    double angle2 = calculateAngel(line2);
    if (fabs(angle1 - angle2) <= 3.4907 && fabs(angle1 - angle2) >= 0.524) //in Grad: 200 und 30
    {
        return 2; // normal large rotation
    }
    else if (fabs(angle1 - angle2) < 0.524) //in Grad 30
    {
        return 3; // small rotation
    }
    else
    {
        return 4; //rotation is to big
    }
}
*/

/*
//CoM= Center of Mass.  Not used at the moment. Old method.
visionx::types::PointList CalculateKeypoints::excludePointsWithOneLine(visionx::types::PointList hypothesisPointList,
        Eigen::Vector3f start, Eigen::Vector3f direction, int side)
{

    visionx::types::PointList newhypothesisPointList;
    newhypothesisPointList.clear();
    std::map<Eigen::Vector3f, int> temporaryStoragePoints;
    temporaryStoragePoints.clear();
    Eigen::Vector3f pointGlobalVec(0, 0, 0);
    Eigen::Vector3f pointLine(0, 0, 0);
    int sideOfPoint;
    for (size_t i = 0; i < hypothesisPointList.size(); i++)
    {
        Vector3Ptr pointVec = Vector3Ptr::dynamicCast(hypothesisPointList[i]);
        pointGlobalVec = pointVec->toEigen();

        pointLine = pointGlobalVec - start;
        sideOfPoint = chooseSideofPointLinieToLine(direction, pointLine);

        if (sideOfPoint == side)
        {
            armarx::Vector3Ptr point = new armarx::Vector3(pointGlobalVec);
            newhypothesisPointList.push_back(point);
        }
    }
    return newhypothesisPointList;
}
*/

/*
// For determine the hypotheses, witch can be the CoM. Exclude the points, wiche are not in a sector.
visionx::types::PointList CalculateKeypoints::excludePointsWithLines(visionx::types::PointList hypothesisPointList, std::list<Eigen::Vector3f> start,
        std::list<Eigen::Vector3f> direction, std::list<int> side)
{
    visionx::types::PointList newhypothesisPointList;
    newhypothesisPointList.clear();

    std::list<int> numberOfExcludes;
    numberOfExcludes.clear();
    for (size_t i = 0; i < hypothesisPointList.size(); i++)
    {
        numberOfExcludes.push_back(0);
    }

    std::list<Eigen::Vector3f>::iterator startP = start.begin();
    std::list<Eigen::Vector3f>::iterator directionP = direction.begin();
    std::list<int>::iterator sideP = side.begin();

    Eigen::Vector3f pointGlobalVec(0, 0, 0);
    Eigen::Vector3f pointLine(0, 0, 0);
    int sideOfPoint;

    while (startP != start.end())
    {
        std::list<int>::iterator numberOfExcludesP = numberOfExcludes.begin();
        for (size_t i = 0; i < hypothesisPointList.size(); i++, numberOfExcludesP++)
        {
            Vector3Ptr pointVec = Vector3Ptr::dynamicCast(hypothesisPointList[i]);
            pointGlobalVec = pointVec->toEigen();
            pointLine = pointGlobalVec - *startP;
            sideOfPoint = chooseSideofPointLinieToLine(*directionP, pointLine);
            if (sideOfPoint != *sideP)
            {
                *numberOfExcludesP += 1;
            }
        }
        startP++;
        directionP++;
        sideP++;
    }

    std::list<int>::iterator numberOfExcludesP2 = numberOfExcludes.begin();
    ARMARX_VERBOSE << "Size of lines for Exclude: " << start.size() << ", Hypot. List size: " << hypothesisPointList.size();
    if (hypothesisPointList.size() != numberOfExcludes.size())
    {
        ARMARX_ERROR << "In Exlude is a big error!";
    }
    for (size_t i = 0; i < hypothesisPointList.size(); i++, numberOfExcludesP2++)
    {
        //if (*numberOfExcludesP2 <= roundf(start.size()/ 3.0))
        if (*numberOfExcludesP2 == 0)
        {
            //ARMARX_VERBOSE<<"In Exlude: numberOfExcludesP2 <= start size/3 ?:  "<< *numberOfExcludesP2 <<  " ; " <<roundf(start.size()/ 3.0);
            Vector3Ptr pointVec = Vector3Ptr::dynamicCast(hypothesisPointList[i]);
            pointGlobalVec = pointVec->toEigen();
            armarx::Vector3Ptr point = new armarx::Vector3(pointGlobalVec);
            newhypothesisPointList.push_back(point);
        }
    }
    ARMARX_VERBOSE << "Size of the new list(exclude): " << newhypothesisPointList.size();
    return newhypothesisPointList;
}
*/

/*
// If we would like to rotate the objekt the objectPosition is adjust. But not used at the moment.
void CalculateKeypoints::adjustKeypointforTurn(Eigen::Vector3f& objectPosition, Eigen::Vector3f& pushDirection, double movement)
{
    Eigen::Vector3f normale(0, 0, 0);
    normale(0) = pushDirection(1) * (-1.0);
    normale(1) = pushDirection(0);
    normale(2) = 0.0;
    float LengthNormale = normale.norm();
    ARMARX_VERBOSE << "Movement: " << movement << "and movemend/6: " << movement / 6.0;
    normale *= ((0.9 * movement) / LengthNormale); //vorher 55 vorher 30
    int side1 = chooseSideofPointLinieToLine(pushDirection, normale);
    if (side1 == -1)
    {
        objectPosition = objectPosition - normale;
    }
    else
    {
        objectPosition = objectPosition + normale;
    }

    //    Eigen::Vector3f zNormale(0.0, 0.0, 1.0);
    //    Eigen::AngleAxis<float> aa(0.52, zNormale);
    //    ARMARX_VERBOSE<<"pushDirection befor: "<<pushDirection;
    //    pushDirection = aa * pushDirection;
    //    ARMARX_VERBOSE<<"pushDirection after: "<<pushDirection;

}
*/

/*
// This method ensures that the oldObjectPosition and the newObjectPosition not too far apart.
Eigen::Vector3f CalculateKeypoints::adjustObjectPoint(Eigen::Vector3f center, Eigen::Vector3f roughlyCenterOfMass, float objectRadius)
{

    Eigen::Vector3f distanceCenters =  roughlyCenterOfMass - center;
    float distancePoints = distanceCenters.norm();
    Eigen::Vector3f pointnew = roughlyCenterOfMass;
    if (distancePoints > objectRadius / 2.0)
    {
        distanceCenters *= ((objectRadius / 2.0) / distancePoints); //vorher 20
        pointnew = center + distanceCenters;
        ARMARX_VERBOSE << "adjustObjectPoint -> " << pointnew;
    }
    else
    {
        ARMARX_VERBOSE << "No adjustObjectPoint";
    }

    return pointnew;
}
*/

/*
// Adjust the lines to the roation of the hypotheses.
void CalculateKeypoints::adjustLinesOfMovement(std::list<Eigen::Vector3f>& startPoints, std::list<Eigen::Vector3f>& pushDirections, Eigen::Matrix4f objectTransformationPlatform)
{
    //adjust the orientation and the movement
    for (std::list<Eigen::Vector3f>::iterator startP = startPoints.begin(); startP != startPoints.end(); startP++)
    {
        *startP = objectTransformationPlatform.block<3, 3>(0, 0) * (*startP) + objectTransformationPlatform.block<3, 1>(0, 3);
    }
    for (std::list<Eigen::Vector3f>::iterator directionP = pushDirections.begin(); directionP != pushDirections.end(); directionP++)
    {
        *directionP = objectTransformationPlatform.block<3, 3>(0, 0) * (*directionP);
    }
    ARMARX_VERBOSE << "Lists with lines were adapted";
}
*/

/*
// This method ensures that the line and the ObjectPosition not too far apart.
void CalculateKeypoints::shiftLine(Eigen::Vector3f& start, Eigen::Vector3f dir, Eigen::Vector3f centerPoint, float maxDistance)
{
    Eigen::Vector3f direction = dir / dir.norm();
    //float distance = (direction.cross(centerPoint - start)).norm() / direction.norm(); // Formel aus Wikipedia übernehmen: https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
    Eigen::Vector3f normaleDirection = (start - centerPoint) - ((start - centerPoint).dot(direction) * direction);
    float distance = normaleDirection.norm();

    if (distance > (maxDistance / 2.0))
    {
        start = start - (normaleDirection / normaleDirection.norm() * (maxDistance / 2.0));

        //        Eigen::Vector3f centerPointLine = centerPoint - start;
        //        int sideOfCenterPoint = chooseSideofCoM(direction, centerPointLine);

        //        Eigen::Vector3f normale(0, 0, 0);
        //        normale(0) = - direction(1);
        //        normale(1) = direction(0);
        //        normale(2) = 0.0;
        //        float LengthNormale = normale.norm();
        //        normale *= (30.0/LengthNormale);//vorher 40
        //        if (chooseSideofCoM(direction, normale) == sideOfCenterPoint)
        //        {
        //            start = start + normale;
        //        }
        //        else
        //        {
        //            start = start - normale;
        //        }
        ARMARX_VERBOSE << "We shift the line to the centerPoint with the distance: " << distance;
    }
    else
    {
        ARMARX_VERBOSE << "We don't shift the line to the centerPoint";
    }
}
*/

/*
//Calculate the extent of the hypothese. Here the singular vectors for the points.
void CalculateKeypoints::calculateExtent(visionx::types::PointList hypothesisList, Eigen::Vector3f& principalAxis1, Eigen::Vector3f& principalAxis2)
{
    Eigen::MatrixXf pointsAsMatrix = getPointsAsMatrix(hypothesisList);
    Eigen::Vector3f center = MatrixHelpers::CalculateCog3D(pointsAsMatrix);
    Eigen::MatrixXf pointsNoMean = MatrixHelpers::SubtractVectorFromAllColumns3D(pointsAsMatrix, center);
    MatrixHelpers::SetRowToValue(pointsAsMatrix, 2, 0);
    SVD svd(pointsNoMean);
    principalAxis1 = svd.getLeftSingularVector3D(0);
    principalAxis2 = svd.getLeftSingularVector3D(1);
}
*/

/*
Eigen::MatrixXf CalculateKeypoints::calculateExtent(visionx::types::PointList hypothesisList)
{
    Eigen::MatrixXf singularValues(3, 2);

    Eigen::MatrixXf pointsAsMatrix = getPointsAsMatrix(hypothesisList);
    Eigen::Vector3f center = MatrixHelpers::CalculateCog3D(pointsAsMatrix);
    Eigen::MatrixXf pointsNoMean = MatrixHelpers::SubtractVectorFromAllColumns3D(pointsAsMatrix, center);
    MatrixHelpers::SetRowToValue(pointsAsMatrix, 2, 0);
    SVD svd(pointsNoMean);

    // approach vector is biggest left singular vector
    Eigen::Vector3f approachVectorOne = svd.getLeftSingularVector3D(0);
    Eigen::Vector3f approachVectorTwo = svd.getLeftSingularVector3D(1);
    Eigen::Vector3f extentOne = approachVectorOne.normalized() * pow(svd.singularValues(0), 1.0 / 2.0) * 2;
    Eigen::Vector3f extentTwo = approachVectorTwo.normalized() * pow(svd.singularValues(1), 1.0 / 2.0) * 2;
    singularValues << extentOne(0), extentTwo(0), extentOne(1), extentTwo(1), extentOne(2), extentTwo(2);
    ARMARX_VERBOSE << "Singular Vektor 1: " << extentOne;
    ARMARX_VERBOSE << "Singular Matrix: " << singularValues;
    return singularValues;
}
*/

/*
// Took all points into a matrix.
Eigen::MatrixXf CalculateKeypoints::getPointsAsMatrix(const visionx::types::PointList& points)
{
    Eigen::MatrixXf matrix(3, points.size());
    for (size_t i = 0; i < points.size(); i++)
    {
        matrix.block<3, 1>(0, i) = Vector3Ptr::dynamicCast(points.at(i))->toEigen();
    }
    return matrix;
}


// If  we need only the x and y direction we can change z to zero.(but achieve the Length).
Eigen::Vector3f CalculateKeypoints::zToZero(Eigen::Vector3f vector)
{
    float vectorLength = vector.norm(); //sqrt(pow(vector(0), 2) + pow(vector(1), 2) + pow(vector(2), 2));
    vector(2) = 0.0;
    if (vector.norm() > 0)
    {
        vector = vector / vector.norm();
    }
    vector = vector * vectorLength;
    return vector;
}
*/

/*
//Calculate the boundary points for the regtangel. The regtangle is used for estimating the extent of the object.
Eigen::MatrixXf CalculateKeypoints::getRectanglePoints(Eigen::Vector3f singularOne, Eigen::Vector3f singularTwo, visionx::types::PointList hypothesisList)
{
    Eigen::Vector3f singularThree = singularOne.cross(singularTwo);
    Eigen::Matrix3f transformMatrix;
    transformMatrix.block<3, 1>(0, 0) = singularOne / singularOne.norm();
    transformMatrix.block<3, 1>(0, 1) = singularTwo / singularTwo.norm();
    transformMatrix.block<3, 1>(0, 2) = singularThree / singularThree.norm();
    Eigen::Matrix3f transformMatrixInvert = transformMatrix.inverse();

    Eigen::Vector3f pointPlatformVec(0, 0, 0);
    Eigen::Vector3f pointPlatformVecTransformiert(0, 0, 0);

    Eigen::Vector3f pointXMax, pointYMax, pointXMin, pointYMin;
    float valueXMax, valueYMax, valueXMin, valueYMin;

    //Initializing the values
    Vector3Ptr start = Vector3Ptr::dynamicCast(hypothesisList[0]);
    Eigen::Vector3f startEigen = start->toEigen();
    Eigen::Vector3f startEigenT = transformMatrixInvert * startEigen;
    valueXMax = startEigenT(0);
    valueYMax = startEigenT(1);
    valueXMin = startEigenT(0);
    valueYMin = startEigenT(1);

    for (size_t i = 0; i < hypothesisList.size(); i++)
    {
        Vector3Ptr pointVec = Vector3Ptr::dynamicCast(hypothesisList[i]);
        pointPlatformVec = pointVec->toEigen();
        pointPlatformVecTransformiert = transformMatrixInvert * pointPlatformVec;

        //Vielleicht nur if ohne Else.. aber man will j a nicht, das ein Punkt der Größte und der Kleinste ist...
        if (pointPlatformVecTransformiert(0) > valueXMax)
        {
            pointXMax = pointPlatformVecTransformiert;
            valueXMax = pointPlatformVecTransformiert(0);
        }
        else if (pointPlatformVecTransformiert(0) <= valueXMin)
        {
            pointXMin = pointPlatformVecTransformiert;
            valueXMin = pointPlatformVecTransformiert(0);
        }

        if (pointPlatformVecTransformiert(1) > valueYMax)
        {
            pointYMax = pointPlatformVecTransformiert;
            valueYMax = pointPlatformVecTransformiert(1);
        }
        else if (pointPlatformVecTransformiert(1) <= valueYMin)
        {
            pointYMin = pointPlatformVecTransformiert;
            valueYMin = pointPlatformVecTransformiert(1);
        }
    }

    Eigen::MatrixXf boundaryPoints(3, 4);
    pointXMax = transformMatrix * pointXMax;
    pointYMax = transformMatrix * pointYMax;
    pointXMin = transformMatrix * pointXMin;
    pointYMin = transformMatrix * pointYMin;
    boundaryPoints.block<3, 1>(0, 0) = pointXMax;
    boundaryPoints.block<3, 1>(0, 1) = pointYMax;
    boundaryPoints.block<3, 1>(0, 2) = pointXMin;
    boundaryPoints.block<3, 1>(0, 3) = pointYMin;

    return boundaryPoints;
}
*/

/*
//Calculates the intersection where the robot is approximately touching the object .
Eigen::Vector3f CalculateKeypoints::intersection(Eigen::Vector3f startPush, Eigen::Vector3f directionPush,
        Eigen::MatrixXf boundaryPoints, Eigen::Vector3f prinzipalAxis1, Eigen::Vector3f prinzipalAxis2)
{
    startPush(2) = 0;
    directionPush(2) = 0;
    Eigen::Vector3f directionPushNorm = directionPush / directionPush.norm();

    // boundaryPoints: (0, 0): pointXMax (S2), (0, 1) pointYMax (S1), (0, 2) pointXMin (S2), (0, 3) pointYMin (S1)
    Eigen::Vector3f startRectangle;
    Eigen::Vector3f directionRectangle;

    Eigen::Matrix4f intersections;

    for (size_t i = 0; i < 4; i++)
    {
        int column = std::abs((static_cast<int>(i) % 2) - 1);
        // ARMARX_VERBOSE<<"Runde + column: "<<i<<" + "<<column;
        startRectangle = boundaryPoints.block<3, 1>(0, i);
        if (column == 1)
        {
            directionRectangle = prinzipalAxis2;
        }
        else
        {
            directionRectangle = prinzipalAxis1;
        }

        startRectangle(2) = 0;
        directionRectangle(2) = 0;
        Eigen::Vector3f directionRectangleNorm = directionRectangle / directionRectangle.norm();

        float lambda, mue;

        float skalar = directionPushNorm.dot(directionRectangleNorm);
        if (skalar == 0)
        {
            ARMARX_ERROR << "Attention, Pushdirection is parallel to the rectangel.";
        }
        if (directionPushNorm(0) == 0 || directionRectangleNorm(1) == 0)
        {
            ARMARX_ERROR << "zero divisor! Warum auch immer...";
        }

        float tempA = directionPushNorm(1) / directionPushNorm(0);
        float tempB = startPush(1) - startRectangle(1);
        float tempC = directionRectangleNorm(1) - tempA * directionRectangleNorm(0);

        mue = (tempB + tempA * startRectangle(0) - tempA * startPush(0)) / tempC;
        lambda = (startRectangle(0) - startPush(0) + mue * directionRectangleNorm(0)) / directionPushNorm(0);


        Eigen::Vector3f intersectionOne = startPush + lambda * directionPushNorm;
        Eigen::Vector3f intersectionTwo = startRectangle + mue * directionRectangleNorm;

        Eigen::Vector3f error = intersectionOne - intersectionTwo;
        if (error.norm() <= 0.5)
        {
            // ARMARX_VERBOSE<<"Intersection:intersectionOne "<<intersectionOne;
            Eigen::Vector4f intersectionTemp;
            intersectionTemp(0) = intersectionOne(0);
            intersectionTemp(1) = intersectionOne(1);
            intersectionTemp(2) = intersectionOne(2);
            intersectionTemp(3) = lambda;
            intersections.block<4, 1>(0, i) =  intersectionTemp;
        }
        else
        {
            ARMARX_ERROR << "No Intersection! Something wrong! (I now this is very helpful...)";
        }
    }

    float referenceValue = FLT_MAX;
    float lambda;
    Eigen::Vector3f intersectionPoint;

    for (size_t i = 0; i < 4; i++)
    {
        lambda = intersections(3, i);
        if (lambda < 0 && fabs(lambda) < fabs(referenceValue))
        {
            intersectionPoint = intersections.block<3, 1>(0, i);
            referenceValue = lambda;
        }
    }
    return intersectionPoint;
}
*/

/*
// This method rotates a vector by a predefined angel in the clockwise or counterclockwise.
// RotationalDirection has only the values 1 and -1 for clockwise and contraclockwise.
Eigen::Vector3f CalculateKeypoints::rotateAxis(Eigen::Vector3f movingAxis, float rotationAngel, int rotationalDirection)
{
    Eigen::Vector3f targetVector;
    float rotationAngelRad = rotationAngel * M_PI / 180.0;
    if (rotationalDirection == 1)
    {
        rotationAngelRad = 2 * M_PI - rotationAngelRad;
    }

    Eigen::AngleAxisf zRotation(rotationAngelRad, Eigen::Vector3f::UnitZ());
    targetVector = zRotation * movingAxis / movingAxis.norm();

    return targetVector;
}
*/
