/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ObjectLearningByPushing
 * @author     Sarah Puch ( sarah dot puch at domain dot de )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <armar6/skills/statecharts/Armar6ObjectLearningByPushing/ForceMeasurement.generated.h>

namespace armarx
{
    namespace Armar6ObjectLearningByPushing
    {
        class ForceMeasurement :
            public ForceMeasurementGeneratedBase < ForceMeasurement >
        {
        public:
            ForceMeasurement(const XMLStateConstructorParams& stateData):
                XMLStateTemplate < ForceMeasurement > (stateData), ForceMeasurementGeneratedBase < ForceMeasurement > (stateData)
            {
            }

            // inherited from StateBase
            void onEnter() override;
            // void run() override;
            // void onBreak() override;
            void onExit() override;

            // static functions for AbstractFactory Method
            static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
            static SubClassRegistry Registry;

            // DO NOT INSERT ANY CLASS MEMBERS,
            // use stateparameters instead,
            // if classmember are neccessary nonetheless, reset them in onEnter

        private:
            void setKeypoints(Eigen::Vector3f pushDirection, Eigen::Vector3f pushingKeypoint1, Eigen::Vector3f pushingKeypoint2, const std::string& rootRobotNodeName, const std::string& agentName);
            void incrementKeypointTwo(Eigen::Vector3f pushingKeypoint2, Eigen::Vector3f pushingKeypoint3, const std::string& rootRobotNodeName, const std::string& agentName);
        };
    }
}
