/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ObjectLearningByPushing
 * @author     Sarah Puch ( sarah dot puch at domain dot de )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveHandToKeypoint.h"

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

using namespace armarx;
using namespace Armar6ObjectLearningByPushing;

// DO NOT EDIT NEXT LINE
MoveHandToKeypoint::SubClassRegistry MoveHandToKeypoint::Registry(MoveHandToKeypoint::GetName(), &MoveHandToKeypoint::CreateInstance);



void MoveHandToKeypoint::onEnter()
{
    // ARMARX_VERBOSE << "entering MoveHandToKeypointState";

    // get other information from the robot for painting
    const std::string agentName = getRobotStateComponent()->getSynchronizedRobot()->getName();
    SharedRobotInterfacePrx agentRobot = getRobotStateComponent()->getSynchronizedRobot();
    ::memoryx::AgentInstanceBasePtr agent = getWorkingMemory()->getAgentInstancesSegment()->getAgentInstanceByName(agentName);
    FramedPositionPtr agentPosition = FramedPositionPtr::dynamicCast(agent->getPositionBase());
    FramedOrientationPtr agentOrientation = FramedOrientationPtr::dynamicCast(agent->getOrientationBase());
    PosePtr agentPose = new Pose(agentOrientation->toEigen(), agentPosition->toEigen());
    Eigen::Matrix4f agentPoseMatrix = agentPose->toEigen();

    FramedPositionPtr pushingKeypointPtr = in.getPushingKeypoint();
    Eigen::Vector3f pushingKeypoint = pushingKeypointPtr->toEigen();

    const std::string kinematicChainName = in.getKinematicChainName();
    const std::string tcpName = getRobotStateComponent()->getSynchronizedRobot()->getRobotNodeSet(kinematicChainName)->tcpName;
    FramedPosePtr handPoseFromKinematicModel = FramedPosePtr::dynamicCast(getRobotStateComponent()->getSynchronizedRobot()->getRobotNode(tcpName)->getPoseInRootFrame());
    FramedPosePtr handPoseFromMemoryX = handPoseFromKinematicModel;

    if (in.isHandMemoryChannelSet())
    {
        ChannelRefPtr handMemoryChannel = in.getHandMemoryChannel();

        memoryx::ChannelRefBaseSequence instances = getObjectMemoryObserver()->getObjectInstances(handMemoryChannel);
        if (instances.size() == 0)
        {
            ARMARX_WARNING << "No instances of the hand in the memory";
        }
        else
        {
            FramedPositionPtr position = ChannelRefPtr::dynamicCast(instances.front())->get<FramedPosition>("position");
            FramedOrientationPtr orientation = ChannelRefPtr::dynamicCast(instances.front())->get<FramedOrientation>("orientation");
            handPoseFromMemoryX = FramedPosePtr(new FramedPose(position, orientation, position->getFrame(), position->agent));

            handPoseFromMemoryX = handPoseFromMemoryX->toRootFrame(agentRobot);
        }
    }

    // ARMARX_VERBOSE << "kinematicChainName: " << kinematicChainName << "context->getRobot()->getRobotNodeSet(kinematicChainName)->getName()" << context->getRobot()->getRobotNodeSet(kinematicChainName)->getName();
    // ARMARX_VERBOSE << "context->getRobot()->getRobotNodeSet(kinematicChainName)->getTCP()->getGlobalPose(): " << context->getRobot()->getRobotNodeSet(kinematicChainName)->getTCP()->getGlobalPose();
    // ARMARX_VERBOSE << "context->getRobot()->getGlobalPose(): " << context->getRobot()->getGlobalPose();
    // ARMARX_VERBOSE << "TCP pose in root frame: " << handPoseFromMemoryX->toEigen();
    // ARMARX_VERBOSE << "Pushing keypoint: " << *pushingKeypointPtr;

    const float maxSpeed = in.getMaxSpeed(); // In Simulation choose: 150.0f. In real choose: 50.0f.
    const float baseSpeedFactor = in.getBaseSpeedFactor(); // In Simulation choose: 30.0f. In real choose: 0.8f.

    Eigen::Matrix4f tcpPoseNew = handPoseFromMemoryX->toEigen();

    /*
    Eigen::Vector3f offset(0.0, 0.0, 0.0);
    if (in.getLeftHandMemoryChannel()->channelName == handMemoryChannel->channelName)
    {
        offset(0) = -50.0;
        offset(1) = -20.0;
        offset(2) = -30.0;
    }
    else
    {
        offset(0) = -40.0; //-20
        offset(1) = 20.0; //25
        offset(2) = -30.0; //0
    }
    // To compensate for the bad calibration.
    Eigen::Vector3f offsetPlatform = tcpPoseNew.block<3,3>(0,0) * offset;
    tcpPoseNew.block<3,1>(0,3) += offsetPlatform;
    */

    Eigen::Vector3f diffPos = pushingKeypoint - tcpPoseNew.block<3, 1>(0, 3);
    const float distanceToTarget = diffPos.norm();

    handPoseFromMemoryX->toGlobal(agentRobot);
    // getDebugDrawerTopic()->setPoseDebugLayerVisu("hand", handPoseFromMemoryX);

    Eigen::Vector3f tcpPoseNewG1 = agentPoseMatrix.block<3, 3>(0, 0) * tcpPoseNew.block<3, 1>(0, 3) + agentPoseMatrix.block<3, 1>(0, 3);
    getDebugDrawerTopic()->setSphereVisu("tcpPose", "tcpNewOffset", new Vector3(tcpPoseNewG1), DrawColor{5, 0, 0, 1}, 10.0f);

    float speedFactor = baseSpeedFactor;
    if (speedFactor * distanceToTarget > maxSpeed)
    {
        speedFactor = maxSpeed / distanceToTarget;
    }

    diffPos = speedFactor * diffPos;

    ARMARX_IMPORTANT << "Distance: " << distanceToTarget;
    // ARMARX_VERBOSE << "Speed: " << diffPos.norm() << "   TCP translation: " << diffPos;
    // ARMARX_IMPORTANT << "Distance: " << distanceToTarget<< "  Speed: " << diffPos.norm() << "   TCP translation: " << diffPos;

    FramedDirectionPtr cartesianVelocity = new FramedDirection(diffPos, getRobotStateComponent()->getSynchronizedRobot()->getRootNode()->getName(), getRobotStateComponent()->getSynchronizedRobot()->getName());

    // make sure we don't move the hand too low
    if (in.isMinimalAllowedHeightForTCPSet())
    {
        const float minimalHeight = in.getMinimalAllowedHeightForTCP();
        if (tcpPoseNew(2, 3) < minimalHeight + 10 && cartesianVelocity->z < 0.1f * maxSpeed)
        {
            ARMARX_IMPORTANT << "Hand too low, moving it up a bit";
            if (tcpPoseNew(2, 3) < minimalHeight - 10)
            {
                cartesianVelocity->z = 0.1f * maxSpeed;
            }
            else if (cartesianVelocity->z < 0.05f * maxSpeed)
            {
                cartesianVelocity->z = 0.05f * maxSpeed;
            }
        }
    }


    // stop when target reached
    if (distanceToTarget < 20.0f)
    {
        cartesianVelocity->x = 0;
        cartesianVelocity->y = 0;
        cartesianVelocity->z = 0;

        getTcpControlUnit()->setTCPVelocity(kinematicChainName, "", cartesianVelocity, NULL);

        getDebugDrawerTopic()->clearLayer("tcpPose");
        getDebugDrawerTopic()->clearLayer("hand");
        ARMARX_IMPORTANT << "Target reached";

        emitKeypointReached();
    }
    else
    {
        getTcpControlUnit()->setTCPVelocity(kinematicChainName, "", cartesianVelocity, NULL);
        emitApproachingKeypoint();
    }
}

//void MoveHandToKeypoint::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void MoveHandToKeypoint::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MoveHandToKeypoint::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveHandToKeypoint::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveHandToKeypoint(stateData));
}
