/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ObjectLearningByPushing
 * @author     Sarah Puch ( sarah dot puch at domain dot de )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <armar6/skills/statecharts/Armar6ObjectLearningByPushing/CalculateKeypoints.generated.h>

namespace armarx
{
    namespace Armar6ObjectLearningByPushing
    {
        class CalculateKeypoints :
            public CalculateKeypointsGeneratedBase < CalculateKeypoints >
        {
        public:
            CalculateKeypoints(const XMLStateConstructorParams& stateData):
                XMLStateTemplate < CalculateKeypoints > (stateData), CalculateKeypointsGeneratedBase < CalculateKeypoints > (stateData)
            {
            }

            // inherited from StateBase
            void onEnter() override;
            // void run() override;
            // void onBreak() override;
            void onExit() override;

            // static functions for AbstractFactory Method
            static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
            static SubClassRegistry Registry;

            // DO NOT INSERT ANY CLASS MEMBERS,
            // use stateparameters instead,
            // if classmember are neccessary nonetheless, reset them in onEnter

        private:
            void CalculateTraditionally();
            void clipToWorkspaceLimits(Eigen::Vector3f& point, const Eigen::Vector3f& workspaceLimitsLow, const Eigen::Vector3f& workspaceLimitsHigh);

            /*
            TODO: port CoM calculation

            void CalculateWithCoM(); // CoM = Center of mass

            Eigen::MatrixXf getPointsAsMatrix(const visionx::types::PointList& points);
            Eigen::MatrixXf calculateExtent(visionx::types::PointList hypothesisList);
            void calculateExtent(visionx::types::PointList hypothesisList, Eigen::Vector3f& principalAxis1, Eigen::Vector3f& principalAxis2);
            Eigen::Vector3f zToZero(Eigen::Vector3f vector);
            Eigen::MatrixXf getRectanglePoints(Eigen::Vector3f singularOne, Eigen::Vector3f singularTwo, visionx::types::PointList hypothesisList);
            Eigen::Vector3f intersection(Eigen::Vector3f startPush, Eigen::Vector3f directionPush, Eigen::MatrixXf boundaryPoints, Eigen::Vector3f prinzipalAxis1, Eigen::Vector3f prinzipalAxis2);
            Eigen::Vector3f intersectionTwoLines(Eigen::Vector3f point, Eigen::Vector3f pointDirection, Eigen::Vector3f limit, Eigen::Vector3f limitDirection);
            Eigen::Vector3f rotateAxis(Eigen::Vector3f movingAxisNorm, float rotationAngel, int rotationalDirection);
            Eigen::Vector3f calculateAverageHypothesisPoint(visionx::types::PointList hypothesisPointList);
            double calculateAngel(Eigen::Vector3f line);
            int chooseSideofPointLinieToLine(Eigen::Vector3f line, Eigen::Vector3f pointLine);
            int chooseSectorWidth(Eigen::Vector3f line1, Eigen::Vector3f line2);
            visionx::types::PointList excludePointsWithOneLine(visionx::types::PointList hypothesisPointList, Eigen::Vector3f start, Eigen::Vector3f direction, int side);
            visionx::types::PointList excludePointsWithLines(visionx::types::PointList hypothesisPointList, std::list<Eigen::Vector3f> start,
                    std::list<Eigen::Vector3f> direction, std::list<int> side);
            void adjustKeypointforTurn(Eigen::Vector3f& objectPosition, Eigen::Vector3f& pushDirection, double movement);
            Eigen::Vector3f adjustObjectPoint(Eigen::Vector3f center, Eigen::Vector3f roughlyCenterOfMass, float objectRadius);
            void adjustLinesOfMovement(std::list<Eigen::Vector3f>& startPoints, std::list<Eigen::Vector3f>& pushDirections, Eigen::Matrix4f objectTransformationPlatform);
            void shiftLine(Eigen::Vector3f& start, Eigen::Vector3f direction, Eigen::Vector3f centerPoint, float maxDistance);

            Eigen::Vector3f intersectionTwoLinesNaive(Eigen::Vector3f point, Eigen::Vector3f pointDirection, Eigen::Vector3f limit, Eigen::Vector3f limitDirection);
            bool isInWorkspaceLimits(Eigen::Vector3f& point, const Eigen::Vector3f& workspaceLimitsLow, const Eigen::Vector3f& workspaceLimitsHigh);
            void clipPointToWorkspaceLimitConsideringDirection(Eigen::Vector3f& point, Eigen::Vector3f pointDirection, const Eigen::Vector3f& workspaceLimitsLow, const Eigen::Vector3f& workspaceLimitsHigh);
            */
        };
    }
}
