/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ObjectLearningByPushing
 * @author     Sarah Puch ( sarah dot puch at domain dot de )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ForceMeasurement.h"

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <RobotAPI/libraries/core/observerfilters/PoseMedianFilter.h>
#include <RobotAPI/libraries/core/FramedPose.h>

using namespace armarx;
using namespace Armar6ObjectLearningByPushing;

// DO NOT EDIT NEXT LINE
ForceMeasurement::SubClassRegistry ForceMeasurement::Registry(ForceMeasurement::GetName(), &ForceMeasurement::CreateInstance);


// David: Todo: Fix this state


void ForceMeasurement::onEnter()
{
    ARMARX_VERBOSE << "Entering ForceMeasurement state";

    // get the robot
    SharedRobotInterfacePrx robotSnapshot = getRobotStateComponent()->getRobotSnapshot("PushObjectStateRobotSnapshot");
    const std::string rootRobotNodeName = robotSnapshot->getRootNode()->getName();
    const std::string agentName = getRobotStateComponent()->getSynchronizedRobot()->getName();

    // get other information from the robot for painting (global pose)
    // FramedPositionPtr agentPosition = FramedPositionPtr::dynamicCast(getWorkingMemory()->getAgentInstancesSegment()->getAgentInstanceByName(agentName)->getPositionBase());
    // FramedOrientationPtr agentOrientation = FramedOrientationPtr::dynamicCast(getWorkingMemory()->getAgentInstancesSegment()->getAgentInstanceByName(agentName)->getOrientationBase());
    // PosePtr agentPose = new Pose(agentOrientation->toEigen(), agentPosition->toEigen());
    // Eigen::Matrix4f agentPoseMatrix = agentPose->toEigen();


    FramedPositionPtr pushingKeypointPtr1 = in.getPushingKeypoint1();
    Eigen::Vector3f pushingKeypoint1 = pushingKeypointPtr1->toEigen();
    FramedPositionPtr pushingKeypointPtr2 = in.getPushingKeypoint1();
    Eigen::Vector3f pushingKeypoint2 = pushingKeypointPtr2->toEigen();
    FramedPositionPtr pushingKeypointPtr3 = in.getPushingKeypoint3();
    Eigen::Vector3f pushingKeypoint3 = pushingKeypointPtr3->toEigen();

    Eigen::Vector3f pushDirection = in.getPushDirection()->toEigen();

    //Readout the forces and convert the datatype
    DatafieldRefPtr datafieldRight = in.getDatafieldFilteredRight();
    DatafieldRefPtr datafieldLeft = in.getDatafieldFilteredLeft();

    FramedDirectionPtr forcesFieldRight;
    FramedDirectionPtr forcesFieldLeft;
    Eigen::Vector3f forcesInPlatformFrameRight{0, 0, 0};
    Eigen::Vector3f forcesInPlatformFrameLeft{0, 0, 0};


    if (!datafieldRight || !datafieldLeft)
    {
        ARMARX_WARNING << "Datafield is NULL";
    }
    else
    {
        // show the forces in the log viewer
        // ARMARX_VERBOSE << "Datafield is NOT-NULL";

        forcesFieldRight = datafieldRight->get<FramedDirection>();
        forcesFieldLeft = datafieldLeft->get<FramedDirection>();

        //convert the hand-coordinate system into the global-coordinate system (for this method you need the include VirtualRobot/Robot.h)
        Eigen::Matrix4f nodePoseRight = FramedPosePtr::dynamicCast(getRobotStateComponent()->getSynchronizedRobot()->getRobotNode(forcesFieldRight->getFrame())->getGlobalPose())->toEigen();
        forcesInPlatformFrameRight = nodePoseRight.block<3, 3>(0, 0) * forcesFieldRight->toEigen();
        Eigen::Matrix4f nodePoseLeft = FramedPosePtr::dynamicCast(getRobotStateComponent()->getSynchronizedRobot()->getRobotNode(forcesFieldLeft->getFrame())->getGlobalPose())->toEigen();
        forcesInPlatformFrameLeft = nodePoseLeft.block<3, 3>(0, 0) * forcesFieldLeft->toEigen();
    }

    ARMARX_VERBOSE << "Forces left: " << forcesInPlatformFrameLeft(2);
    ARMARX_VERBOSE << "Forces right: " << forcesInPlatformFrameRight(2);

    Eigen::Vector3f distanceToObjectPosition = in.getObjectPosition()->toEigen() - pushingKeypoint2;
    float distanceToOP = distanceToObjectPosition.norm();
    ARMARX_VERBOSE << "Hand distance To Object: " << distanceToOP;

    // If the forces are to big, we break the movement and restart with new points.
    // if (in.getLeftHandMemoryChannel()->channelName == in.getHandMemoryChannel()->channelName)
    if (in.getHandModelNameLeft() == in.getHandModelName())
    {

        // if (forcesInPlatformFrameLeft(2) > in.getForceLimit() && distanceToOP <= in.getHandRadius())
        if (forcesInPlatformFrameLeft(2) <= -140.0 && distanceToOP >= in.getHandRadius())
        {
            setKeypoints(pushDirection, pushingKeypoint1, pushingKeypoint2, rootRobotNodeName, agentName);
            ARMARX_VERBOSE << "Left hand: Forces to big, adjust keypoint one and two.";
            emitRestart();
        }
        else if (forcesInPlatformFrameLeft(2) <= -140.0 && distanceToOP <= in.getHandRadius())
        {
            incrementKeypointTwo(pushingKeypoint2, pushingKeypoint3, rootRobotNodeName, agentName);
            emitSuccess();
        }
        else
        {
            emitSuccess();
        }
    }
    else
    {
        // if (forcesInPlatformFrameRight(2) > in.getForceLimit() && distanceToOP <= in.getHandRadius())
        if (forcesInPlatformFrameRight(2) <= -140.0 && distanceToOP >= in.getHandRadius())
        {
            setKeypoints(pushDirection, pushingKeypoint1, pushingKeypoint2, rootRobotNodeName, agentName);
            ARMARX_VERBOSE << "Right hand: Forces to big, adjust keypoint one and two.";
            emitRestart();
        }
        // else if (forcesInPlatformFrameRight(2) > in.getForceLimit() && distanceToOP <= in.getHandRadius())
        else if (forcesInPlatformFrameRight(2) <= -140.0 && distanceToOP <= in.getHandRadius())
        {
            incrementKeypointTwo(pushingKeypoint2, pushingKeypoint3, rootRobotNodeName, agentName);
            emitSuccess();
        }
        else
        {
            emitSuccess();
        }
    }
}

//void ForceMeasurement::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void ForceMeasurement::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ForceMeasurement::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ForceMeasurement::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ForceMeasurement(stateData));
}


// The keypoints are shifted along the pushDirection.
void ForceMeasurement::setKeypoints(Eigen::Vector3f pushDirection, Eigen::Vector3f pushingKeypoint1, Eigen::Vector3f pushingKeypoint2, const std::string& rootRobotNodeName, const std::string& agentName)
{
    Eigen::Vector3f newPushingKeypoint1 = pushingKeypoint1 + (pushDirection / pushDirection.norm() * in.getIncrement());
    Eigen::Vector3f newPushingKeypoint2 = pushingKeypoint2 + (pushDirection / pushDirection.norm() * in.getIncrement());
    FramedPosition pushingKeypoint1Framed(newPushingKeypoint1, rootRobotNodeName, agentName);
    FramedPosition pushingKeypoint2Framed(newPushingKeypoint2, rootRobotNodeName, agentName);
    out.setNewPushingKeypoint1(pushingKeypoint1Framed);
    out.setNewPushingKeypoint2(pushingKeypoint2Framed);
    // float increment = in.getIncrement() + 45.0;
    // out.setIncrement(increment);
}


void ForceMeasurement::incrementKeypointTwo(Eigen::Vector3f pushingKeypoint2, Eigen::Vector3f pushingKeypoint3, const std::string& rootRobotNodeName, const std::string& agentName)
{
    pushingKeypoint2(2) = pushingKeypoint2(2) + 10.0;
    pushingKeypoint3(2) = pushingKeypoint3(2) + 10.0;
    FramedPosition pushingKeypoint2Framed(pushingKeypoint2, rootRobotNodeName, agentName);
    FramedPosition pushingKeypoint3Framed(pushingKeypoint3, rootRobotNodeName, agentName);
    out.setNewPushingKeypoint2(pushingKeypoint2Framed);
    out.setNewPushingKeypoint3(pushingKeypoint3Framed);
}
