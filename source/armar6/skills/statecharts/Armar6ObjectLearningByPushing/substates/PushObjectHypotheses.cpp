/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ObjectLearningByPushing
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @author     Christoph Haas ( utdtq at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PushObjectHypotheses.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6ObjectLearningByPushing;

// DO NOT EDIT NEXT LINE
PushObjectHypotheses::SubClassRegistry PushObjectHypotheses::Registry(PushObjectHypotheses::GetName(), &PushObjectHypotheses::CreateInstance);


static Eigen::Vector3f clipToWorkspaceLimits(Eigen::Vector3f point, const Eigen::Vector3f& workspaceLimitsLow, const Eigen::Vector3f& workspaceLimitsHigh)
{
    Eigen::Vector3f result;
    // TODO: replace with min, max
    result(0) = (point(0) < workspaceLimitsLow(0)) ? workspaceLimitsLow(0) : ((point(0) > workspaceLimitsHigh(0)) ? workspaceLimitsHigh(0) : point(0));
    result(1) = (point(1) < workspaceLimitsLow(1)) ? workspaceLimitsLow(1) : ((point(1) > workspaceLimitsHigh(1)) ? workspaceLimitsHigh(1) : point(1));
    result(2) = (point(2) < workspaceLimitsLow(2)) ? workspaceLimitsLow(2) : ((point(2) > workspaceLimitsHigh(2)) ? workspaceLimitsHigh(2) : point(2));
    return result;
}

void PushObjectHypotheses::onEnter()
{
    // get position of the datapoints from the hypothesis.
    DataFieldIdentifierPtr dataFieldId = new DataFieldIdentifier("ObjectLearningByPushingObserver.objectHypothesisPosition.objectPosition");
    FramedPositionPtr objectPositionFramed = armarx::VariantPtr::dynamicCast(getObjectLearningByPushingObserver()->getDataField(dataFieldId))->getClass<FramedPosition>();
    ARMARX_VERBOSE << "Object position: " << *objectPositionFramed;

    ARMARX_IMPORTANT << "PushObjectHypothesis::onEnter()";
    DebugDrawerInterfacePrx const& debugDrawer = getDebugDrawerTopic();
    SharedRobotInterfacePrx sharedRobot = getRobotStateComponent()->getSynchronizedRobot();
    VirtualRobot::RobotPtr robot = getLocalRobot();
    RemoteRobot::synchronizeLocalClone(robot, sharedRobot);
    Eigen::Affine3f robotToGlobal;
    robotToGlobal.matrix() = robot->getGlobalPose();


    local.setObjectPosition(Vector3Ptr {new Vector3(objectPositionFramed->toGlobalEigen(robot))});
    // Eigen::Vector3f objectPosInGlobal = in.getObjectPosition()->toEigen();
    Eigen::Vector3f objectPosInGlobal = local.getObjectPosition()->toEigen();
    Eigen::Vector3f objectPosInRobot = robotToGlobal.inverse() * objectPosInGlobal;

    //Params for workspace
    Eigen::Vector3f workspaceLimitsHigh = in.getWorkspaceLimitsHigh()->toEigen();
    Eigen::Vector3f workspaceLimitsLow = in.getWorkspaceLimitsLow()->toEigen();
    Eigen::Vector3f centralPointInRobot = in.getCentralPoint()->toEigen();
    Eigen::Vector3f centralPointInGlobal = robotToGlobal * centralPointInRobot;

    //manual offset to compensate calibration errors while using real Armar
    Eigen::Vector3f manualOffsetLeft = Eigen::Vector3f::Zero();
    if (in.isManualOffsetLeftSet())
    {
        manualOffsetLeft = in.getManualOffsetLeft()->toEigen();
    }

    Eigen::Vector3f manualOffsetRight = Eigen::Vector3f::Zero();
    if (in.isManualOffsetRightSet())
    {
        manualOffsetRight = in.getManualOffsetRight()->toEigen();
    }

    // Determine pushing direction
    Eigen::Vector3f pushingDirection = centralPointInRobot - objectPosInRobot;
    pushingDirection(2) = 0;

    // Create a pushing direction that is mainly in x direction
    pushingDirection(0) *= 2;
    if (pushingDirection.norm() == 0)
    {
        pushingDirection(0) = 1;
    }
    if (fabs(pushingDirection(0)) < fabs(pushingDirection(1)))
    {
        pushingDirection(0) *= fabs(pushingDirection(1)) / fabs(pushingDirection(0));
    }

    pushingDirection = pushingDirection / pushingDirection.norm();

    float pushLength = in.getPushLength() * (0.9 + 0.2 * 0.001 * (rand() % 1000));

    Eigen::Vector3f pushingOffset = Eigen::Vector3f::Zero(); // TODO: What was this used for? Seems to be zero in the old code?
    //in.getPushingOffset()->toEigen();
    //    ARMARX_INFO << "Pushing offset " << pushingOffset;
    //    if (pushingOffset.norm() > 0)
    //    {
    //        pushLength *= 1.2;
    //        manualOffsetLeft(2) -= 10;
    //    }

    Eigen::Vector3f manualOffset = pushingDirection(0) > 0 ? manualOffsetLeft : manualOffsetRight;
    ARMARX_VERBOSE << "Manual offset " << manualOffset;
    pushingOffset += manualOffset;

    float drawKeypointRadius = 15.0f;
    float prePushDistanceFromObject = in.getPrePushDistance();
    float prePushHeightOffset = in.getPrePushHeightOffset();

    // Calculate pushing keypoints
    Eigen::Vector3f pushingKeypoint1 = objectPosInRobot - prePushDistanceFromObject * pushingDirection;
    pushingKeypoint1(2) += prePushHeightOffset;
    pushingKeypoint1 += pushingOffset;
    pushingKeypoint1 = clipToWorkspaceLimits(pushingKeypoint1, workspaceLimitsLow, workspaceLimitsHigh);

    Eigen::Vector3f pushingKeypoint2 = objectPosInRobot - prePushDistanceFromObject * pushingDirection;
    pushingKeypoint2 += pushingOffset;
    pushingKeypoint2 = clipToWorkspaceLimits(pushingKeypoint2, workspaceLimitsLow, workspaceLimitsHigh);

    Eigen::Vector3f pushingKeypoint3 = objectPosInRobot - (prePushDistanceFromObject - pushLength) * pushingDirection;
    pushingKeypoint3 += pushingOffset;
    pushingKeypoint3 = clipToWorkspaceLimits(pushingKeypoint3, workspaceLimitsLow, workspaceLimitsHigh);

    Eigen::Vector3f pushingKeypoint4 = objectPosInRobot - (prePushDistanceFromObject - 0.7f * pushLength) * pushingDirection;
    pushingKeypoint4(2) += prePushHeightOffset;
    pushingKeypoint4 += pushingOffset;
    pushingKeypoint4 = clipToWorkspaceLimits(pushingKeypoint4, workspaceLimitsLow, workspaceLimitsHigh);

    // Draw debug output
    {
        std::string layerName = getName();
        float drawRadius = 40.0f;
        //Eigen::Vector3f objectPosInGlobal = robotToGlobal * objectPosInRobot;
        debugDrawer->setSphereVisu(layerName, "ObjectPos", toIce(objectPosInGlobal), DrawColor {1, 0, 0, 1}, drawRadius);
        Eigen::Vector3f pushingDirectionGlobal = robotToGlobal.linear() * pushingDirection;
        debugDrawer->setSphereVisu(layerName, "CentralPoint", toIce((centralPointInGlobal + 50.0f * Eigen::Vector3f::UnitZ()).eval()), DrawColor {0, 1, 0, 1}, drawRadius);
        float arrowLength = 200.0f;
        float arrowWidth = 10.0f;
        debugDrawer->setArrowDebugLayerVisu(layerName, toIce((objectPosInGlobal + 50.0f * Eigen::Vector3f::UnitZ()).eval()), toIce(pushingDirectionGlobal), DrawColor {1, 1, 0, 1}, arrowLength, arrowWidth);

        Eigen::Vector3f pushingKeypoint1Global = robotToGlobal * pushingKeypoint1;
        debugDrawer->setSphereVisu(layerName, "Keypoint1", toIce(pushingKeypoint1Global), DrawColor {0, 0, 1, 1}, drawKeypointRadius);
        Eigen::Vector3f pushingKeypoint2Global = robotToGlobal * pushingKeypoint2;
        debugDrawer->setSphereVisu(layerName, "Keypoint2", toIce(pushingKeypoint2Global), DrawColor {0.3, 0.3f, 1, 1}, drawKeypointRadius);
        Eigen::Vector3f pushingKeypoint3Global = robotToGlobal * pushingKeypoint3;
        debugDrawer->setSphereVisu(layerName, "Keypoint3", toIce(pushingKeypoint3Global), DrawColor {0.5, 0.5f, 1, 1}, drawKeypointRadius);
        Eigen::Vector3f pushingKeypoint4Global = robotToGlobal * pushingKeypoint4;
        debugDrawer->setSphereVisu(layerName, "Keypoint4", toIce(pushingKeypoint4Global), DrawColor {0.7, 0.7f, 1, 1}, drawKeypointRadius);
    }

    // Save results to local parameters
    {
        std::string rootRobotNodeName = robot->getRootNode()->getName();
        std::string agentName = sharedRobot->getName();

        std::vector<FramedPositionPtr> keypoints;
        keypoints.reserve(4);
        keypoints.push_back(new FramedPosition(pushingKeypoint1, rootRobotNodeName, agentName));
        keypoints.push_back(new FramedPosition(pushingKeypoint2, rootRobotNodeName, agentName));
        keypoints.push_back(new FramedPosition(pushingKeypoint3, rootRobotNodeName, agentName));
        keypoints.push_back(new FramedPosition(pushingKeypoint4, rootRobotNodeName, agentName));

        local.setKeypoints(keypoints);
        local.setPushDirection(pushingDirection);
    }

    // Decision which hand to use
    if (pushingDirection(0) > 0)
    {
        ARMARX_IMPORTANT << "Using left arm";
        local.setKinematicChainName(in.getKinematicChainNameLeftArm());
        local.setHandModelName(in.getHandModelNameLeft());
        local.setForceTorqueDatafield(in.getForceTorqueDatafieldLeft());

        if (in.isHandMemoryChannelLeftSet())
        {
            local.setHandMemoryChannel(in.getHandMemoryChannelLeft());
        }
    }
    else
    {
        ARMARX_IMPORTANT << "Using right arm";
        local.setKinematicChainName(in.getKinematicChainNameRightArm());
        local.setHandModelName(in.getHandModelNameRight());
        local.setForceTorqueDatafield(in.getForceTorqueDatafieldRight());

        if (in.isHandMemoryChannelRightSet())
        {
            local.setHandMemoryChannel(in.getHandMemoryChannelRight());
        }
    }

    ARMARX_INFO << "Finished calculating keypoints";
}

//void PushObjectHypotheses::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void PushObjectHypotheses::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void PushObjectHypotheses::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr PushObjectHypotheses::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new PushObjectHypotheses(stateData));
}
