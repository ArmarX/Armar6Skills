/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ObjectLearningByPushing
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @author     Christoph Haas ( utdtq at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveHandAlongKeypoints.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/time/CycleUtil.h>
// #include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6ObjectLearningByPushing;

// DO NOT EDIT NEXT LINE
MoveHandAlongKeypoints::SubClassRegistry MoveHandAlongKeypoints::Registry(MoveHandAlongKeypoints::GetName(), &MoveHandAlongKeypoints::CreateInstance);



void MoveHandAlongKeypoints::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void MoveHandAlongKeypoints::run()
{
    TCPControlUnitInterfacePrx tcpControlUnit = getTcpControlUnit();
    DebugDrawerInterfacePrx debugDrawer = getDebugDrawerTopic();
    SharedRobotInterfacePrx sharedRobot = getRobotStateComponent()->getSynchronizedRobot();
    VirtualRobot::RobotPtr robot = getLocalRobot();

    std::vector<Eigen::Vector3f> keypoints;
    {
        auto framedKeypoints = in.getKeypoints();
        keypoints.reserve(framedKeypoints.size());
        for (auto& framedKeypoint : framedKeypoints)
        {
            keypoints.push_back(framedKeypoint->toRootEigen(robot));
        }
        if (keypoints.size() != 4)
        {
            ARMARX_WARNING << "Expected four keypoints but got " << keypoints.size();
            emitFailure();
            return;
        }
    }

    std::string kinematicChainName = in.getKinematicChainName();
    float maxSpeed = in.getMaxKeypointApproachSpeed(); // In Simulation choose: 150.0f. In real choose: 50.0f.
    float baseSpeedFactor = in.getBaseKeypointApproachSpeedFactor(); // In Simulation choose: 30.0f. In real choose: 0.8f.
    Eigen::Vector3f objectPosition = in.getObjectPosition()->toEigen();
    Eigen::Vector3f pushDirection = in.getPushDirection()->toEigen();
    float forceCorrectionIncrement = in.getForceCorrectionIncrement();
    float handRadius = in.getHandRadius();
    float minimalHeight = in.getWorkspaceLimitsLow()->z;
    DatafieldRefPtr forceTorqueDatafield = in.getForceTorqueDatafield();

    const float forceLimitZ = in.getForceLimitZ();
    if (forceLimitZ > 0)
    {
        ARMARX_WARNING << "Force Limit has to be negative!";
        emitFailure();
        return;
    }

    VirtualRobot::RobotNodePtr tcpNode = robot->getRobotNodeSet(kinematicChainName)->getTCP();
    std::string tcpNodeName = tcpNode->getName();

    ChannelRefPtr handInstance;
    if (in.isHandMemoryChannelSet())
    {
        ChannelRefPtr handMemoryChannel = in.getHandMemoryChannel();
        memoryx::ChannelRefBaseSequence instances = getObjectMemoryObserver()->getObjectInstances(handMemoryChannel);
        if (instances.size() == 0)
        {
            ARMARX_WARNING << "No instances of the hand in the memory";
        }
        else
        {
            handInstance = ChannelRefPtr::dynamicCast(instances.front());
        }
    }

    std::string layerName = "MoveToKeypoint";

    tcpControlUnit->request();

    std::size_t keypointIndex = 0;
    FramedDirectionPtr cartesianVelocity = new FramedDirection(Eigen::Vector3f::Zero(), robot->getRootNode()->getName(), sharedRobot->getName());
    CycleUtil cycle(5);
    bool lastKeypointReached = false;
    while (!isRunningTaskStopped() && !lastKeypointReached)
    {
        RemoteRobot::synchronizeLocalClone(robot, sharedRobot);

        // Get new pose from memory (or model if memory channel not available)
        Eigen::Vector3f tcpPositionNew;
        if (handInstance)
        {
            Eigen::Vector3f position = handInstance->get<FramedPosition>("position")->toRootEigen(robot);
            tcpPositionNew = position;
        }
        else
        {
            tcpPositionNew = tcpNode->getPositionInRootFrame();
        }

        // Check forces while going down
        if (keypointIndex == 1)
        {
            FramedDirectionPtr framedForceTorque = forceTorqueDatafield->get<FramedDirection>();
            Eigen::Vector3f forcesInRobot = framedForceTorque->toGlobalEigen(robot);
            float distanceToObject = (objectPosition - tcpPositionNew).norm();

            // if (forcesInRobot(2) <= -140.0 && distanceToObject >= handRadius)
            if (forcesInRobot(2) <= forceLimitZ && distanceToObject >= handRadius)
            {
                ARMARX_IMPORTANT << "Forces to big (" << forcesInRobot(2) << " <= " << forceLimitZ << "), adjust keypoint one and two and restarting";

                keypoints[0] += (pushDirection / pushDirection.norm() * forceCorrectionIncrement);
                keypoints[1] += (pushDirection / pushDirection.norm() * forceCorrectionIncrement);
                keypointIndex = 0;
            }
            // else if (forcesInRobot(2) <= -140.0 && distanceToObject <= handRadius)
            else if (forcesInRobot(2) <= forceLimitZ && distanceToObject <= handRadius)
            {
                ARMARX_IMPORTANT << "Raising keypoints 2 and 3 a bit...";

                keypoints[1] += Eigen::Vector3f(0, 0, 10.0f);
                keypoints[2] += Eigen::Vector3f(0, 0, 10.0f);
            }
        }

        // Use TCP control to move towards the current keypoint
        Eigen::Vector3f pushingKeypoint = keypoints[keypointIndex];
        Eigen::Vector3f fromTcpToKeypoint = pushingKeypoint - tcpPositionNew;
        float distanceToTarget = fromTcpToKeypoint.norm();

        Eigen::Vector3f velocity = baseSpeedFactor * fromTcpToKeypoint;
        if (velocity.norm() > maxSpeed)
        {
            velocity = maxSpeed * velocity.normalized();
        }

        // Make sure we don't move the hand too low
        if (tcpPositionNew.z() < minimalHeight + 10 && velocity.z() < 0.1f * maxSpeed)
        {
            ARMARX_IMPORTANT << "Hand too low, moving it up a bit";
            if (tcpPositionNew.z() < minimalHeight - 10)
            {
                velocity.z() = 0.1f * maxSpeed;
            }
            else if (cartesianVelocity->z < 0.05f * maxSpeed)
            {
                velocity.z() = 0.05f * maxSpeed;
            }
        }

        // Switch to next keypoint or stop at final keypoint
        if (distanceToTarget < 20.0f)
        {
            keypointIndex += 1;
            if (keypointIndex < keypoints.size())
            {
                pushingKeypoint = keypoints[keypointIndex];
                ARMARX_INFO << "Keypoint[" << keypointIndex << "]: (" << pushingKeypoint.x() << ", " << pushingKeypoint.y() << ", " << pushingKeypoint.z() << ")";
            }
            else
            {
                ARMARX_IMPORTANT << "Last keypoint reached";
                velocity = Eigen::Vector3f::Zero();
                lastKeypointReached = true;
            }
        }

        cartesianVelocity->x = velocity.x();
        cartesianVelocity->y = velocity.y();
        cartesianVelocity->z = velocity.z();
        tcpControlUnit->setTCPVelocity(kinematicChainName, tcpNodeName, cartesianVelocity, nullptr);

        // Draw stuff after sending control commands to reduce latency
        Eigen::Matrix4f agentPoseMatrix = robot->getGlobalPose();
        Eigen::Affine3f robotToGlobal;
        robotToGlobal.matrix() = agentPoseMatrix;

        Eigen::Vector3f tcpPositionNewG1 = robotToGlobal * tcpPositionNew;
        debugDrawer->setSphereVisu(layerName, "tcpNewOffset", new Vector3(tcpPositionNewG1), DrawColor {1, 0, 0, 1}, 10.0f);

        cycle.waitForCycleDuration();
    }

    debugDrawer->clearLayer(layerName);
    tcpControlUnit->release();

    if (lastKeypointReached)
    {
        emitSuccess();
    }
    else
    {
        ARMARX_WARNING << "Did not reach last keypoint";
        emitFailure();
    }
}

//void MoveHandAlongKeypoints::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MoveHandAlongKeypoints::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveHandAlongKeypoints::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveHandAlongKeypoints(stateData));
}
