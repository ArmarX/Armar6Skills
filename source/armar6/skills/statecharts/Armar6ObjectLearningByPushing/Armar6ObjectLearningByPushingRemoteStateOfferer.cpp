/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ObjectLearningByPushing::Armar6ObjectLearningByPushingRemoteStateOfferer
 * @author     Christoph Haas ( utdtq at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6ObjectLearningByPushingRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6ObjectLearningByPushing;

// DO NOT EDIT NEXT LINE
Armar6ObjectLearningByPushingRemoteStateOfferer::SubClassRegistry Armar6ObjectLearningByPushingRemoteStateOfferer::Registry(Armar6ObjectLearningByPushingRemoteStateOfferer::GetName(), &Armar6ObjectLearningByPushingRemoteStateOfferer::CreateInstance);



Armar6ObjectLearningByPushingRemoteStateOfferer::Armar6ObjectLearningByPushingRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6ObjectLearningByPushingStatechartContext > (reader)
{
}

void Armar6ObjectLearningByPushingRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6ObjectLearningByPushingRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6ObjectLearningByPushingRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6ObjectLearningByPushingRemoteStateOfferer::GetName()
{
    return "Armar6ObjectLearningByPushingRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6ObjectLearningByPushingRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6ObjectLearningByPushingRemoteStateOfferer(reader));
}
