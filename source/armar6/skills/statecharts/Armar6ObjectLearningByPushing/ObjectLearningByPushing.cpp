/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ObjectLearningByPushing
 * @author     Christoph Haas ( utdtq at student dot kit dot edu )
 * @author     Sarah Puch ( sarah dot puch at domain dot de )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ObjectLearningByPushing.h"

#include <RobotAPI/interface/components/ViewSelectionInterface.h>

using namespace armarx;
using namespace Armar6ObjectLearningByPushing;

// DO NOT EDIT NEXT LINE
ObjectLearningByPushing::SubClassRegistry ObjectLearningByPushing::Registry(ObjectLearningByPushing::GetName(), &ObjectLearningByPushing::CreateInstance);



void ObjectLearningByPushing::onEnter()
{
    /*
    // Counter
    ChannelRefPtr CounterId = ChannelRefPtr::dynamicCast(getSystemObserver()->startCounter(0, "counterId"));
    local.setCounterId(CounterId);
    */

    // define parameters for pushing
    const float handRadius = in.getHandRadius();
    const float tableHeight = in.getTableHeight(); // previously 920 (Gut Simulation) previously 950 for real

    // ensure positive
    if (handRadius < 0 || tableHeight < 0)
    {
        ARMARX_ERROR << "Hand Radius and table height must be > 0";
        emitFailure();
        return;
    }

    local.setPrePushDistanceFromObject(140 + handRadius); //previously 80 + hand
    local.setPushLength(400); // 250 110  90

    Eigen::Vector3f direction(0, 0, 0);
    local.setPushDirection(direction);

    Eigen::Vector3f pushingOffset(0, 0, 0);
    local.setPushingOffset(pushingOffset);

    // safety guards (space over the table)
    Vector3Ptr inputLimitsLow = Vector3Ptr::dynamicCast(in.getWorkspaceLimitsLowInput());
    Vector3Ptr inputLimitsHigh = Vector3Ptr::dynamicCast(in.getWorkspaceLimitsHighInput());

    // make sure that min height is >= tableHeight + handRadius
    float minHeight = tableHeight + handRadius;
    if (minHeight < inputLimitsLow->z)
    {
        minHeight = inputLimitsLow->z;
    }
    else
    {
        ARMARX_IMPORTANT << "Adjusted input workspace limit low Z (min. height) to handRadius + tableHeight. New Z limit: " << minHeight;
    }

    // Eigen::Vector3f workspaceLimitsLow(-450, 450, (tableHeight + handRadius)); // (-500, 470, (local.getTableHeight() + local.getHandRadius()))
    Eigen::Vector3f workspaceLimitsLow(inputLimitsLow->x, inputLimitsLow->y, minHeight);
    local.setWorkspaceLimitsLow(workspaceLimitsLow);

    // Eigen::Vector3f workspaceLimitsHigh(450, 800, 1200); // previously: (500, 750, 1200)
    Eigen::Vector3f workspaceLimitsHigh = inputLimitsHigh->toEigen();
    local.setWorkspaceLimitsHigh(workspaceLimitsHigh);

    /*
    local.setNoTurn(true);
    local.setExistLastLine(false);
    */

    // Get memory channel for the hand if set
    if (in.isHandMemoryNameLeftSet() && in.isHandMemoryNameRightSet())
    {
        const int priority = armarx::DEFAULT_VIEWTARGET_PRIORITY;
        ChannelRefBasePtr leftHandMemoryChannel = getObjectMemoryObserver()->requestObjectClassRepeated(in.getHandMemoryNameLeft(), 150, priority);
        ChannelRefBasePtr rightHandMemoryChannel = getObjectMemoryObserver()->requestObjectClassRepeated(in.getHandMemoryNameRight(), 150, priority);
        if (rightHandMemoryChannel && leftHandMemoryChannel)
        {
            rightHandMemoryChannel->validate();
            leftHandMemoryChannel->validate();
            local.setRightHandMemoryChannel(ChannelRefPtr::dynamicCast(rightHandMemoryChannel));
            local.setLeftHandMemoryChannel(ChannelRefPtr::dynamicCast(leftHandMemoryChannel));
        }
        else
        {
            ARMARX_ERROR << "Unknown Object Class: " << in.getHandMemoryNameRight() << ", " << in.getHandMemoryNameLeft();
            emitFailure();
            return;
        }
    }
    else
    {
        ARMARX_DEBUG << "Not using hand memory channels.";
    }

    // getViewSelection()->activateAutomaticViewSelection();
    // getViewSelection()->deactivateAutomaticViewSelection();

    ARMARX_VERBOSE << "Finished entering ObjectLearningByPushingMain";
}

//void ObjectLearningByPushing::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void ObjectLearningByPushing::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ObjectLearningByPushing::onExit()
{
    ARMARX_VERBOSE << "Exiting ObjectLearningByPushingMain";

    if (local.isLeftHandMemoryChannelSet())
    {
        getObjectMemoryObserver()->releaseObjectClass(local.getLeftHandMemoryChannel());
    }

    if (local.isRightHandMemoryChannelSet())
    {
        getObjectMemoryObserver()->releaseObjectClass(local.getRightHandMemoryChannel());
    }
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ObjectLearningByPushing::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ObjectLearningByPushing(stateData));
}
