/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6LowLevelHandControlGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copy  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <thread>

#include "HandPwmControl.h"
#include "HandController.h"

#include <algorithm>

using namespace armarx;
using namespace Armar6LowLevelHandControlGroup;

// DO NOT EDIT NEXT LINE
HandPwmControl::SubClassRegistry HandPwmControl::Registry(HandPwmControl::GetName(), &HandPwmControl::CreateInstance);

void HandPwmControl::run()
{
    const unsigned int freq = 100;
    ARMARX_INFO << "HandPwmControl: configuring controller";
    HandController ctrl;
    {
        const bool ctrlFingers = in.isFingersSet();
        const bool ctrlThumb = in.isThumbSet();
        ctrl.done = !(ctrlFingers || ctrlThumb);
        ARMARX_INFO << "HandPwmControl. Initial State: " << VAROUT(ctrl.done) << " " << VAROUT(in.isFingersSet()) << " " << VAROUT(in.isThumbSet());
        if (!ctrl.done)
        {
            ctrl.handController =
                devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getHandControllerName()));
            ARMARX_CHECK_NOT_NULL(ctrl.handController);
            ctrl.handController->activateController();

            const auto initial = ctrl.handController->getJointValues();
            ctrl.currentFingersTarget = initial.fingersJointValue;
            ctrl.currentThumbTarget   = initial.thumbJointValue;

            ctrl.targetFingers = std::clamp(ctrlFingers ? in.getFingers() : ctrl.currentFingersTarget, 0.0f, 2.0f);
            ctrl.targetThumb   = std::clamp(ctrlThumb   ? in.getThumb()   : ctrl.currentThumbTarget, 0.0f, 2.0f);

            ctrl.rangeFingers = std::minmax(ctrl.currentFingersTarget, ctrl.targetFingers);
            ctrl.rangeThumb   = std::minmax(ctrl.currentThumbTarget, ctrl.targetThumb);

            ctrl.directionFingers = ctrl.currentFingersTarget < ctrl.targetFingers ? 1 : -1;
            ctrl.directionThumb   = ctrl.currentThumbTarget   < ctrl.targetThumb ? 1 : -1;

            ctrl.iterationFingersDelta = in.getFingersVel() > 0 ? in.getFingersVel() / freq : 2;
            ctrl.iterationThumbDelta   = in.getThumbVel()   > 0 ? in.getThumbVel()   / freq : 2;

            ctrl.fingersRelativeMaxPwm = in.getFingersRelativeMaxPwm();
            ctrl.thumbRelativeMaxPwm = in.getThumbRelativeMaxPwm();
            ARMARX_INFO << "HandPwmControl: " << VAROUT(ctrl.targetFingers) << " " << VAROUT(ctrl.targetThumb);
        }
    }


    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    while (!isRunningTaskStopped() && !ctrl.done)
    {
        ctrl.run();
        ARMARX_INFO << deactivateSpam(1) << "HandPwmControl. State: " << VAROUT(ctrl.done);
        std::this_thread::sleep_for(std::chrono::milliseconds{1000 / freq});
    }
    ARMARX_IMPORTANT << "HandPwmControl DONE";
    emitSuccess();
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr HandPwmControl::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new HandPwmControl(stateData));
}

