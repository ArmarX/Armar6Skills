/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6LowLevelHandControlGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <boost/algorithm/clamp.hpp>

#include <ArmarXCore/core/logging/Logging.h>

#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
namespace armarx
{
    struct HandController
    {
        bool done = true;

        devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx handController;

        float currentFingersTarget;
        float currentThumbTarget;

        float targetFingers;
        float targetThumb;

        std::pair<float, float> rangeFingers;
        std::pair<float, float> rangeThumb;

        float directionFingers;
        float directionThumb;

        float iterationFingersDelta;
        float iterationThumbDelta;

        float fingersRelativeMaxPwm;
        float thumbRelativeMaxPwm;

        bool run()
        {
            using boost::algorithm::clamp;
            ARMARX_INFO << deactivateSpam(1) << "Running HandController";
            if (done)
            {
                return true;
            }
            currentFingersTarget = currentFingersTarget + directionFingers * iterationFingersDelta;
            currentThumbTarget   = currentThumbTarget   + directionThumb  * iterationThumbDelta;
            //clamp
            currentFingersTarget = std::clamp(currentFingersTarget, rangeFingers.first, rangeFingers.second);
            currentThumbTarget   = std::clamp(currentThumbTarget, rangeThumb.first, rangeThumb.second);

            ARMARX_INFO << deactivateSpam(1) << VAROUT(currentFingersTarget) << "\t" << VAROUT(currentThumbTarget) << "\t" << VAROUT(fingersRelativeMaxPwm) << "\t" << VAROUT(thumbRelativeMaxPwm);
            handController->setTargetsWithPwm(
                currentFingersTarget, currentThumbTarget,
                fingersRelativeMaxPwm, thumbRelativeMaxPwm);

            done = (currentFingersTarget == targetFingers && currentThumbTarget == targetThumb);
            if (done)
            {
                ARMARX_IMPORTANT << "Hand reached Target " << VAROUT(targetFingers) << "\t" << VAROUT(targetThumb);
            }
            return done;
        }
    };
}
