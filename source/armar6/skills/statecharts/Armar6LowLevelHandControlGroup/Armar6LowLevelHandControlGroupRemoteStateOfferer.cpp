/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6LowLevelHandControlGroup::Armar6LowLevelHandControlGroupRemoteStateOfferer
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6LowLevelHandControlGroupRemoteStateOfferer.h"
#include <RobotAPI/libraries/core/Pose.h>

using namespace armarx;
using namespace Armar6LowLevelHandControlGroup;

// DO NOT EDIT NEXT LINE
Armar6LowLevelHandControlGroupRemoteStateOfferer::SubClassRegistry Armar6LowLevelHandControlGroupRemoteStateOfferer::Registry(Armar6LowLevelHandControlGroupRemoteStateOfferer::GetName(), &Armar6LowLevelHandControlGroupRemoteStateOfferer::CreateInstance);



Armar6LowLevelHandControlGroupRemoteStateOfferer::Armar6LowLevelHandControlGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6LowLevelHandControlGroupStatechartContext > (reader)
{
    Vector3Ptr v = new Vector3(0, 0, 0);
    ARMARX_INFO << VAROUT(v);
}

void Armar6LowLevelHandControlGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6LowLevelHandControlGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6LowLevelHandControlGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6LowLevelHandControlGroupRemoteStateOfferer::GetName()
{
    return "Armar6LowLevelHandControlGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6LowLevelHandControlGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6LowLevelHandControlGroupRemoteStateOfferer(reader));
}
