/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6LowLevelHandControlGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <thread>

#include "BothHandsPwmControl.h"
#include "HandController.h"

#include <algorithm>

using namespace armarx;
using namespace Armar6LowLevelHandControlGroup;

// DO NOT EDIT NEXT LINE
BothHandsPwmControl::SubClassRegistry BothHandsPwmControl::Registry(BothHandsPwmControl::GetName(), &BothHandsPwmControl::CreateInstance);

void BothHandsPwmControl::run()
{
    const unsigned int freq = 100;
    HandController ctrlLeft;
    {
        const bool ctrlLeftFingers = in.isLeftHandFingersSet();
        const bool ctrlLeftThumb = in.isLeftHandThumbSet();
        ctrlLeft.done = !(ctrlLeftFingers || ctrlLeftThumb);
        ARMARX_INFO << "HandPwmControl. Initial State Left: " << VAROUT(ctrlLeft.done) << " " << VAROUT(in.isLeftHandFingersSet()) << " " << VAROUT(in.isLeftHandThumbSet());
        if (!ctrlLeft.done)
        {
            ctrlLeft.handController =
                devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getLeftHandControllerName()));
            ARMARX_CHECK_NOT_NULL(ctrlLeft.handController);
            ctrlLeft.handController->activateController();

            const auto initial = ctrlLeft.handController->getJointValues();
            ctrlLeft.currentFingersTarget = initial.fingersJointValue;
            ctrlLeft.currentThumbTarget   = initial.thumbJointValue;

            ctrlLeft.targetFingers = std::clamp(ctrlLeftFingers ? in.getLeftHandFingers() : ctrlLeft.currentFingersTarget, 0.0f, 2.0f);
            ctrlLeft.targetThumb   = std::clamp(ctrlLeftThumb   ? in.getLeftHandThumb()   : ctrlLeft.currentThumbTarget, 0.0f, 2.0f);

            ctrlLeft.rangeFingers = std::minmax(ctrlLeft.currentFingersTarget, ctrlLeft.targetFingers);
            ctrlLeft.rangeThumb   = std::minmax(ctrlLeft.currentThumbTarget, ctrlLeft.targetThumb);

            ctrlLeft.directionFingers = ctrlLeft.currentFingersTarget < ctrlLeft.targetFingers ? 1 : -1;
            ctrlLeft.directionThumb   = ctrlLeft.currentThumbTarget   < ctrlLeft.targetThumb ? 1 : -1;

            ctrlLeft.iterationFingersDelta = in.getLeftHandFingersVel() > 0 ? in.getLeftHandFingersVel() / freq : 2;
            ctrlLeft.iterationThumbDelta   = in.getLeftHandThumbVel()   > 0 ? in.getLeftHandThumbVel()   / freq : 2;

            ctrlLeft.fingersRelativeMaxPwm = in.getLeftHandFingersRelativeMaxPwm();
            ctrlLeft.thumbRelativeMaxPwm = in.getLeftHandThumbRelativeMaxPwm();
            ARMARX_INFO << "HandPwmControl: " << VAROUT(ctrlLeft.targetFingers) << " " << VAROUT(ctrlLeft.targetThumb);
        }
    }
    HandController ctrlRight;
    {
        const bool ctrlRightFingers = in.isRightHandFingersSet();
        const bool ctrlRightThumb = in.isRightHandThumbSet();
        ctrlRight.done = !(ctrlRightFingers || ctrlRightThumb);
        ARMARX_INFO << "HandPwmControl. Initial State Right: " << VAROUT(ctrlRight.done) << " " << VAROUT(in.isRightHandFingersSet()) << " " << VAROUT(in.isRightHandThumbSet());
        if (!ctrlRight.done)
        {
            ctrlRight.handController =
                devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getRightHandControllerName()));
            ARMARX_CHECK_NOT_NULL(ctrlRight.handController);
            ctrlRight.handController->activateController();

            const auto initial = ctrlRight.handController->getJointValues();
            ctrlRight.currentFingersTarget = initial.fingersJointValue;
            ctrlRight.currentThumbTarget   = initial.thumbJointValue;

            ctrlRight.targetFingers = std::clamp(ctrlRightFingers ? in.getRightHandFingers() : ctrlRight.currentFingersTarget, 0.0f, 2.0f);
            ctrlRight.targetThumb   = std::clamp(ctrlRightThumb   ? in.getRightHandThumb()   : ctrlRight.currentThumbTarget, 0.0f, 2.0f);

            ctrlRight.rangeFingers = std::minmax(ctrlRight.currentFingersTarget, ctrlRight.targetFingers);
            ctrlRight.rangeThumb   = std::minmax(ctrlRight.currentThumbTarget, ctrlRight.targetThumb);

            ctrlRight.directionFingers = ctrlRight.currentFingersTarget < ctrlRight.targetFingers ? 1 : -1;
            ctrlRight.directionThumb   = ctrlRight.currentThumbTarget   < ctrlRight.targetThumb ? 1 : -1;

            ctrlRight.iterationFingersDelta = in.getRightHandFingersVel() > 0 ? in.getRightHandFingersVel() / freq : 2;
            ctrlRight.iterationThumbDelta   = in.getRightHandThumbVel()   > 0 ? in.getRightHandThumbVel()   / freq : 2;

            ctrlRight.fingersRelativeMaxPwm = in.getRightHandFingersRelativeMaxPwm();
            ctrlRight.thumbRelativeMaxPwm = in.getRightHandThumbRelativeMaxPwm();
            ARMARX_INFO << "HandPwmControl: " << VAROUT(ctrlRight.targetFingers) << " " << VAROUT(ctrlRight.targetThumb);
        }
    }

    while (!isRunningTaskStopped() && (!ctrlLeft.done || !ctrlRight.done))
    {
        ctrlLeft.run();
        ctrlRight.run();
        ARMARX_INFO << deactivateSpam(1) << "HandPwmControl. State: " << VAROUT(ctrlLeft.done);
        ARMARX_INFO << deactivateSpam(1) << "HandPwmControl. State: " << VAROUT(ctrlRight.done);
        std::this_thread::sleep_for(std::chrono::milliseconds{1000 / freq});
    }
    ARMARX_IMPORTANT << "BothHandsPwmControl DONE";
    emitSuccess();
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr BothHandsPwmControl::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new BothHandsPwmControl(stateData));
}

