/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6CleanUpTaskGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SelectPlaceDownStrategy.h"

#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>

#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>

#include <VirtualRobot/Grasping/GraspSet.h>

using namespace armarx;
using namespace Armar6CleanUpTaskGroup;

// DO NOT EDIT NEXT LINE
SelectPlaceDownStrategy::SubClassRegistry SelectPlaceDownStrategy::Registry(SelectPlaceDownStrategy::GetName(), &SelectPlaceDownStrategy::CreateInstance);



void SelectPlaceDownStrategy::onEnter()
{

    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());


    //memory segments / structures
    ARMARX_CHECK_NOT_NULL(getWorkingMemory());
    memoryx::ObjectInstanceMemorySegmentBasePrx objInstanceSeg = getWorkingMemory()->getObjectInstancesSegment();
    memoryx::PersistentObjectClassSegmentBasePrx objClassesSeg = getPriorKnowledge()->getObjectClassesSegment();
    memoryx::CommonStorageInterfacePrx commonStorage = getWorkingMemory()->getCommonStorage();
    memoryx::GridFileManagerPtr fileManager(new memoryx::GridFileManager(commonStorage));

    ARMARX_CHECK_NOT_NULL(objInstanceSeg);
    ARMARX_CHECK_NOT_NULL(objClassesSeg);
    ARMARX_CHECK_NOT_NULL(commonStorage);
    ARMARX_CHECK_NOT_NULL(fileManager);

    //instance
    memoryx::ObjectInstancePtr objectInstance =  memoryx::ObjectInstancePtr::dynamicCast(objInstanceSeg->getObjectInstanceById(in.getObjectInstanceId()));
    ARMARX_IMPORTANT << "Object instance id '" << in.getObjectInstanceId() << "' = (@" << objectInstance.get() << ")\n" << objectInstance;
    ARMARX_CHECK_NOT_NULL(objectInstance);

    //class
    const std::string className = objectInstance->getMostProbableClass();

    static const std::set<std::string> objectsStorageBox {"cleaning_cloth", "sponge"};
    static const std::set<std::string> objectsWorkBenchLeft ; //{"cleaning_paste", "isoalcohol"};

    if (objectsStorageBox.count(className))
    {
        emitFrontInStorageBox();
    }
    else if (objectsWorkBenchLeft.count(className))
    {
        emitFrontOnWorkBench();
    }
    else
    {
        emitBack();
    }

}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SelectPlaceDownStrategy::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SelectPlaceDownStrategy(stateData));
}
