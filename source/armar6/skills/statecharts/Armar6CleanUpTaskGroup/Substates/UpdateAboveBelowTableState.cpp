/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6CleanUpTaskGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "UpdateAboveBelowTableState.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6CleanUpTaskGroup;

// DO NOT EDIT NEXT LINE
UpdateAboveBelowTableState::SubClassRegistry UpdateAboveBelowTableState::Registry(UpdateAboveBelowTableState::GetName(), &UpdateAboveBelowTableState::CreateInstance);

void UpdateAboveBelowTableState::onEnter()
{


    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());


    if (in.getSelectedArm() == "Right")
    {
        out.setLeftArmAboveTable(in.getLeftArmAboveTable());
        out.setRightArmAboveTable(in.getArmAboveTable());
    }
    else
    {
        out.setRightArmAboveTable(in.getRightArmAboveTable());
        out.setLeftArmAboveTable(in.getArmAboveTable());
    }

    out.setRightArmAboveTable(localRobot->getRobotNodeSet("RightArm")->getTCP()->getPoseInRootFrame()(2, 3) > 1050);
    out.setLeftArmAboveTable(localRobot->getRobotNodeSet("LeftArm")->getTCP()->getPoseInRootFrame()(2, 3) > 1050);

    emitSuccess();
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr UpdateAboveBelowTableState::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new UpdateAboveBelowTableState(stateData));
}
