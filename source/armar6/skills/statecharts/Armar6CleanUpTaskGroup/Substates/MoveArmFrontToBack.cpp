/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6CleanUpTaskGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveArmFrontToBack.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6CleanUpTaskGroup;

// DO NOT EDIT NEXT LINE
MoveArmFrontToBack::SubClassRegistry MoveArmFrontToBack::Registry(MoveArmFrontToBack::GetName(), &MoveArmFrontToBack::CreateInstance);



void MoveArmFrontToBack::onEnter()
{
    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

    if (in.getNodeSetName() == "RightArm")
    {
        local.setArmMotionFile({"/home/armar-user/repos/armarx_integration/robots/armar6/rt/data/armar6_motion/taskspace-trajectory-RightArmPreMoveFrontToBack.csv"});
        PosePtr p = new Pose;
        p->orientation->qw = 0.6842660903930664 ;
        p->orientation->qx = -0.6410484910011292;
        p->orientation->qy = 0.1337349861860275;
        p->orientation->qz = -0.3208611905574799;
        p->position->x = 974.68;
        p->position->y = 11.4399;
        p->position->z = 1095.75;

        local.setGoalPose(p);
        if (in.getDrawDebugInfo())
        {
            getDebugDrawerTopic()->setPoseVisu("MoveArFrontBack", "dmp_goal", new Pose {Eigen::Matrix4f{localRobot->getGlobalPose() * p->toEigen()}});
        }


        p->orientation->qw = 0.9098179340362549 ;
        p->orientation->qx = -0.3970954418182373;
        p->orientation->qy = 0.05350083857774734;
        p->orientation->qz = 0.1080940291285515;
        p->position->x = 760.0163574218750;
        p->position->y = 464.2390136718750;
        p->position->z = 1772.617675781250;

        local.setViaPose({p});
        local.setViaPoseCanVal({0.55});
        if (in.getDrawDebugInfo())
        {
            getDebugDrawerTopic()->setPoseVisu("MoveArFrontBack", "dmp_via", new Pose {Eigen::Matrix4f{localRobot->getGlobalPose() * p->toEigen()}});
        }
    }
    else
    {
        local.setArmMotionFile({"/home/armar-user/repos/armarx_integration/robots/armar6/rt/data/armar6_motion/taskspace-trajectory-LeftArmPreMoveFrontToBack.csv"});
        PosePtr p = new Pose;
        p->orientation->qw = 0.7611168622970581;
        p->orientation->qx = -0.6037436127662659;
        p->orientation->qy =  0.007313956506550312;
        p->orientation->qz = 0.2369417250156403;
        p->position->x = -913.57;
        p->position->y = 280.582;
        p->position->z = 1122.17;
        local.setGoalPose(p);

        if (in.getDrawDebugInfo())
        {
            getDebugDrawerTopic()->setPoseVisu("MoveArFrontBack", "dmp_goal", new Pose {Eigen::Matrix4f{localRobot->getGlobalPose() * p->toEigen()}});
        }

        p->orientation->qw = 0.7439652085304260 ;
        p->orientation->qx = -0.5494705438613892;
        p->orientation->qy = -0.1074578762054443;
        p->orientation->qz = -0.3647615015506744;
        p->position->x = -483.941894531250;
        p->position->y = 550;//850.5133056640625;
        p->position->z = 1678.547363281250;
        //        PosePtr p2 = new Pose;

        local.setViaPose({p});
        local.setViaPoseCanVal({0.6});

        if (in.getDrawDebugInfo())
        {
            getDebugDrawerTopic()->setPoseVisu("MoveArFrontBack", "dmp_via", new Pose {Eigen::Matrix4f{localRobot->getGlobalPose() * p->toEigen()}});
        }
    }
}

//void MoveArmFrontToBack::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void MoveArmFrontToBack::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MoveArmFrontToBack::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveArmFrontToBack::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveArmFrontToBack(stateData));
}
