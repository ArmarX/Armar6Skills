/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6CleanUpTaskGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LiftUpAndPlaceDownObject.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>

#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/libraries/motionmodels/MotionModelStaticObject.h>

using namespace armarx;
using namespace Armar6CleanUpTaskGroup;

// DO NOT EDIT NEXT LINE
LiftUpAndPlaceDownObject::SubClassRegistry LiftUpAndPlaceDownObject::Registry(LiftUpAndPlaceDownObject::GetName(), &LiftUpAndPlaceDownObject::CreateInstance);

void LiftUpAndPlaceDownObject::onExit()
{

    NJointCartesianVelocityControllerWithRampInterfacePrx tcpController =
        NJointCartesianVelocityControllerWithRampInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getCartesianVelocityControllerName()));
    ARMARX_CHECK_EXPRESSION(tcpController);
    tcpController->activateController();
    tcpController->setTargetVelocity(0, 0, 0, 0, 0, 0);

    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx handController =
        devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getHandControllerName()));
    ARMARX_CHECK_EXPRESSION(handController);
    handController->activateController();
    handController->setTargets(0, 0);


    auto instanceSeg = getWorkingMemory()->getObjectInstancesSegment();
    auto robotToLink = getRobotStateComponent()->getRobotSnapshot(getRobotStateComponent()->getRobotName());

    auto objInstance = memoryx::ObjectInstancePtr::dynamicCast(instanceSeg->getObjectInstanceById(in.getObjectInstanceId()));
    FramedPosePtr pose = objInstance->getPose();
    instanceSeg->setObjectPose(in.getObjectInstanceId(), new LinkedPose(*pose, robotToLink));

    memoryx::MotionModelStaticObjectPtr newMotionModel = new memoryx::MotionModelStaticObject(getRobotStateComponent());
    instanceSeg->setNewMotionModel(in.getObjectInstanceId(), newMotionModel);

    auto display = in.getDisplay_Text();
    if (display.size() == 2)
    {
        getMessageDisplay()->setMessage(display[0], display[1]);
    }
    else
    {
        ARMARX_WARNING << "Display text not set (wrong size): " << display.size();
    }
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr LiftUpAndPlaceDownObject::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new LiftUpAndPlaceDownObject(stateData));
}
