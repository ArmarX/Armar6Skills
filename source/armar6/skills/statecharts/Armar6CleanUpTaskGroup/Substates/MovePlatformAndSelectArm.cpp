/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6CleanUpTaskGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "MovePlatformAndSelectArm.h"

#include <RobotAPI/libraries/ArmarXObjects/ice_conversions.h>

#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>

#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>

#include <VirtualRobot/Grasping/GraspSet.h>


using namespace armarx;
using namespace Armar6CleanUpTaskGroup;

// DO NOT EDIT NEXT LINE
MovePlatformAndSelectArm::SubClassRegistry MovePlatformAndSelectArm::Registry(MovePlatformAndSelectArm::GetName(), &MovePlatformAndSelectArm::CreateInstance);



void MovePlatformAndSelectArm::onEnter()
{
    std::string objectName;
    Eigen::Matrix4f globalObj = Eigen::Matrix4f::Identity();
    Eigen::Matrix4f rootObj = Eigen::Matrix4f::Identity();

    VirtualRobot::RobotPtr localRobot = getLocalRobot();
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

    bool found = false;
    if (in.getObjectInstanceId().find("/") != std::string::npos)
    {
        // It's an ObjectID.
        ObjectID id(in.getObjectInstanceId());
        objpose::ObjectPoseSeq objectPoses = objpose::fromIce(getObjectPoseStorage()->getObjectPoses());
        for (const auto& p : objectPoses)
        {
            if (p.objectID == id)
            {
                found = true;
                objectName = p.objectID.str();
                globalObj = p.objectPoseGlobal;
                rootObj = p.objectPoseRobot;
                break;
            }
        }
        if (!found)
        {
            ARMARX_WARNING << "Object instance ID '" << in.getObjectInstanceId() << "' looks like an armarx::ObjectID, "
                           << "but no such object was found in ObjectPoseStorage (offering " << objectPoses.size() << " objects)."
                           << "Trying to interpret it as memory instance ID.";
        }
    }
    if (!found)
    {
        //memory segments / structures
        ARMARX_CHECK_NOT_NULL(getWorkingMemory());
        memoryx::ObjectInstanceMemorySegmentBasePrx objInstanceSeg = getWorkingMemory()->getObjectInstancesSegment();
        memoryx::PersistentObjectClassSegmentBasePrx objClassesSeg = getPriorKnowledge()->getObjectClassesSegment();
        memoryx::CommonStorageInterfacePrx commonStorage = getWorkingMemory()->getCommonStorage();
        memoryx::GridFileManagerPtr fileManager(new memoryx::GridFileManager(commonStorage));

        ARMARX_CHECK_NOT_NULL(objInstanceSeg);
        ARMARX_CHECK_NOT_NULL(objClassesSeg);
        ARMARX_CHECK_NOT_NULL(commonStorage);
        ARMARX_CHECK_NOT_NULL(fileManager);

        //instance
        memoryx::ObjectInstancePtr objectInstance =  memoryx::ObjectInstancePtr::dynamicCast(objInstanceSeg->getObjectInstanceById(in.getObjectInstanceId()));
        ARMARX_IMPORTANT << "Object instance id '" << in.getObjectInstanceId() << "' = (@" << objectInstance.get() << ")\n" << objectInstance;
        ARMARX_CHECK_NOT_NULL(objectInstance);

        objectName = objectInstance->getName();
        globalObj = objectInstance->getPose()->toGlobalEigen(localRobot);
        rootObj = objectInstance->getPose()->toRootEigen(localRobot);
    }

    //    const std::string className = objectInstance->getMostProbableClass();


    bool rightArm = true;
    if (globalObj(0, 3) < in.getWorkbenchMiddleX())
    {
        out.setSelectedArm("Left");
        rightArm = false;
    }
    else
    {
        out.setSelectedArm("Right");
    }

    ARMARX_IMPORTANT << "I am going to grasp " + objectName + "  with  my "  +  out.getSelectedArm() + " arm.";
    //    getTextToSpeech()->reportText("I am going to grasp " + objectInstance->getName() + "  with  my "  +  out.getSelectedArm() + " arm.");

    {
        const float selectedObjX = rootObj(0, 3);

        float relPlatfX  = selectedObjX - in.getPositionXOffset() * (rightArm ? 1 : -1);

        local.setRelativePose(new Vector3 {Eigen::Vector3f{relPlatfX, 0, 0}});


        ARMARX_INFO << /*VAROUT(selectedObjXTarget) << "\t" <<*/ VAROUT(selectedObjX) << "\t" << VAROUT(relPlatfX);

        //draw splitting plane

        if (in.getDrawDebugInfo())
        {
            const float c = 10000;
            auto draw = [&](DrawColor clr, std::string name, float x)
            {
                PolygonPointList points;
                points.emplace_back(new Vector3 {Eigen::Vector3f{x, c, c}});
                points.emplace_back(new Vector3 {Eigen::Vector3f{x, c, -c,}});
                points.emplace_back(new Vector3 {Eigen::Vector3f{x, -c, -c}});
                points.emplace_back(new Vector3 {Eigen::Vector3f{x, -c, c}});
                getDebugDrawerTopic()->setPolygonVisu("MovePlatformAndSelectArm", name, points, clr, clr, 0);
            };
            draw({0, 0, 1, 0.3f}, "splitting_plane", in.getWorkbenchMiddleX());

            const float robx = localRobot->getGlobalPose()(0, 3);

            draw({0, 1, 1, 0.3f}, "ra_max", robx + in.getRelativeObjectPositionMaxX());
            draw({0, 1, 0.5, 0.3f}, "ra_min", robx + in.getRelativeObjectPositionMinX());

            draw({1, 0, 1, 0.3f}, "la_max", robx - in.getRelativeObjectPositionMaxX());
            draw({1, 0, 0.5, 0.3f}, "la_min", robx - in.getRelativeObjectPositionMinX());
        }

    }
    auto objPoseInRoot = FramedPosition(Eigen::Vector3f {globalObj.block<3, 1>(0, 3)}, GlobalFrame, "").toRootFrame(localRobot);
    objPoseInRoot->y += 50;
    local.setGazeTarget(objPoseInRoot->toGlobal(localRobot));
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MovePlatformAndSelectArm::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MovePlatformAndSelectArm(stateData));
}

