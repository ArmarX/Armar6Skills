/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6CleanUpTaskGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SetArmParameters.h"

using namespace armarx;
using namespace Armar6CleanUpTaskGroup;

// DO NOT EDIT NEXT LINE
SetArmParameters::SubClassRegistry SetArmParameters::Registry(SetArmParameters::GetName(), &SetArmParameters::CreateInstance);


void SetArmParameters::onEnter()
{
    out.setGraspCountLeft(in.getGraspCountLeft());
    out.setGraspCountRight(in.getGraspCountRight());

    VirtualRobot::RobotPtr localRobot = getLocalRobot();
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

    if (in.getSelectedArm() == "Left")
    {
        out.setNodeSetName(in.getLeftNodeSetName());
        out.setGraspSetName(in.getLeftGraspSetName());
        out.setFTDatafieldName(in.getLeftFTDatafieldName());
        out.setHandControllerName(in.getLeftHandControllerName());
        out.setCartesianVelocityControllerName(in.getLeftCartesianVelocityControllerName());
        out.setHandName(in.getLeftHandName());
        out.setHandChannel(in.getLeftHandChannel());
        out.setDMPTrajectoryFileForFirstGrasp(in.getLeftDMPTrajectoryFileForFirstGrasp());
        out.setGraspCountLeft(in.getGraspCountLeft() + 1);
        out.setGraspCount(in.getGraspCountLeft());

        out.setArmAboveTabel(in.getLeftArmAboveTabel());
        out.setBackToFrontWaypoints(in.getLeftBackToFrontWaypoints());
        out.setFrontToBackWaypoints(in.getLeftFrontToBackWaypoints());

        out.setViaPoseCanValGrasp(in.getLeftViaPoseCanValGrasp());
        out.setViaPoseGrasp(in.getLeftViaPoseGrasp());
    }
    else if (in.getSelectedArm() == "Right")
    {
        out.setNodeSetName(in.getRightNodeSetName());
        out.setGraspSetName(in.getRightGraspSetName());
        out.setFTDatafieldName(in.getRightFTDatafieldName());
        out.setHandControllerName(in.getRightHandControllerName());
        out.setCartesianVelocityControllerName(in.getRightCartesianVelocityControllerName());
        out.setHandName(in.getRightHandName());
        out.setHandChannel(in.getRightHandChannel());
        out.setDMPTrajectoryFileForFirstGrasp(in.getRightDMPTrajectoryFileForFirstGrasp());
        out.setGraspCountRight(in.getGraspCountRight() + 1);
        out.setGraspCount(in.getGraspCountRight());

        out.setArmAboveTabel(in.getRightArmAboveTabel());
        out.setBackToFrontWaypoints(in.getRightBackToFrontWaypoints());
        out.setFrontToBackWaypoints(in.getRightFrontToBackWaypoints());

        out.setViaPoseCanValGrasp(in.getRightViaPoseCanValGrasp());
        out.setViaPoseGrasp(in.getRightViaPoseGrasp());
    }
    else
    {
        throw InvalidArgumentException {"Unknown selected arm: " + in.getSelectedArm()};
    }


    out.setArmAboveTabel(localRobot->getRobotNodeSet(out.getNodeSetName())->getTCP()->getPoseInRootFrame()(2, 3) > 1050);




    ARMARX_IMPORTANT << "Selected:\n"
                     << "---- FTDatafieldName                : " << out.getFTDatafieldName() << "\n"
                     << "---- CartesianVelocityControllerName: " << out.getCartesianVelocityControllerName() << "\n"
                     << "---- GraspSetName                   : " << out.getGraspSetName() << "\n"
                     << "---- HandControllerName             : " << out.getHandControllerName() << "\n"
                     << "---- NodeSetName                    : " << out.getNodeSetName() << "\n"
                     << "---- HandName                       : " << out.getHandName() << "\n"
                     << "---- HandChannel                    : " << out.getHandChannel() << "\n";

    emitArmSelected();
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SetArmParameters::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SetArmParameters(stateData));
}
