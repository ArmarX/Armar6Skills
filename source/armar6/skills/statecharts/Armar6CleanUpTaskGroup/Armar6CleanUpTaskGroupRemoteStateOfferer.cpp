/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6CleanUpTaskGroup::Armar6CleanUpTaskGroupRemoteStateOfferer
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6CleanUpTaskGroupRemoteStateOfferer.h"
#include <MemoryX/core/MemoryXCoreObjectFactories.h>

using namespace armarx;
using namespace Armar6CleanUpTaskGroup;

// DO NOT EDIT NEXT LINE
Armar6CleanUpTaskGroupRemoteStateOfferer::SubClassRegistry Armar6CleanUpTaskGroupRemoteStateOfferer::Registry(Armar6CleanUpTaskGroupRemoteStateOfferer::GetName(), &Armar6CleanUpTaskGroupRemoteStateOfferer::CreateInstance);



Armar6CleanUpTaskGroupRemoteStateOfferer::Armar6CleanUpTaskGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6CleanUpTaskGroupStatechartContext > (reader)
{
}

void Armar6CleanUpTaskGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6CleanUpTaskGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6CleanUpTaskGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6CleanUpTaskGroupRemoteStateOfferer::GetName()
{
    return "Armar6CleanUpTaskGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6CleanUpTaskGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6CleanUpTaskGroupRemoteStateOfferer(reader));
}
