/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6CleanUpTaskGroup
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <thread>

#include "CleanUpTask.h"

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>

#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>

using namespace armarx;
using namespace Armar6CleanUpTaskGroup;

// DO NOT EDIT NEXT LINE
CleanUpTask::SubClassRegistry CleanUpTask::Registry(CleanUpTask::GetName(), &CleanUpTask::CreateInstance);

void CleanUpTask::onEnter()
{
    local.setGraspCountLeft(0);
    local.setGraspCountRight(0);

    local.setLeftArmAboveTable(0);
    local.setRightArmAboveTable(0);

    auto display = in.getDisplay_Intro();
    if (display.size() == 2)
    {
        getMessageDisplay()->setMessage(display[0], display[1]);
    }
    else
    {
        ARMARX_WARNING << "Display text not set (wrong size): " << display.size();
    }
}

void CleanUpTask::onExit()
{
    getMessageDisplay()->setMessage("", "");
}

void CleanUpTask::run()
{
    NJointCartesianVelocityControllerWithRampConfigPtr configRight = new NJointCartesianVelocityControllerWithRampConfig(in.getRightNodeSetName(), "", CartesianSelectionMode::eAll, 500, 1, 2, 0, 2);
    NJointControllerInterfacePrx ctrlRight = getRobotUnit()->createNJointController("NJointCartesianVelocityControllerWithRamp", in.getRightCartesianVelocityControllerName(), configRight);

    NJointCartesianVelocityControllerWithRampConfigPtr configLeft = new NJointCartesianVelocityControllerWithRampConfig(in.getLeftNodeSetName(), "", CartesianSelectionMode::eAll, 500, 1, 2, 0, 2);
    NJointControllerInterfacePrx ctrlLeft = getRobotUnit()->createNJointController("NJointCartesianVelocityControllerWithRamp", in.getLeftCartesianVelocityControllerName(), configLeft);

    {
        devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx rightHandController =
            devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getRightHandControllerName()));
        ARMARX_CHECK_EXPRESSION(rightHandController);
        rightHandController->activateController();
        rightHandController->setTargetsWithPwm(0, 0, 1, 1);
    }
    {
        devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx leftHandController =
            devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController(in.getLeftHandControllerName()));
        ARMARX_CHECK_EXPRESSION(leftHandController);
        leftHandController->activateController();
        leftHandController->setTargetsWithPwm(0, 0, 1, 1);
    }

    while (!isRunningTaskStopped())
    {
        std::this_thread::sleep_for(std::chrono::milliseconds {1});
    }

    ctrlRight->deactivateController();
    ctrlLeft->deactivateController();

    while (ctrlRight->isControllerActive() || ctrlLeft->isControllerActive())
    {
        std::this_thread::sleep_for(std::chrono::milliseconds {1});
    }
    ctrlRight->deleteController();
    ctrlLeft->deleteController();
}

void CleanUpTask::transitionSuccessFromSelectObjectToRemove(MovePlatformAndSelectArmIn& next, const Armar6HighlevelTaskGroup::SelectObjectToRemoveOut& prev)
{
    ARMARX_IMPORTANT << VAROUT(prev.getSelectedObject());
    const auto id = prev.getSelectedObject()->getDataField("id")->getString();
    const auto name =  prev.getSelectedObject()->getDataField("instanceName")->getString();
    local.setObjectName(name);
    local.setObjectInstanceId(id);
    next.setObjectInstanceId(id);
    ARMARX_IMPORTANT << "using object id '" << id << "' with instance name '" << name << "'";

    std::map<std::string, std::string> humanObjectNameMap = in.getHumanObjectNameMap();
    boost::regex re("[^a-zA-Z0-9]+");

    auto resolveObjectName = [&](ChannelRefPtr channelRef)
    {
        std::string memoryName = channelRef->getDataField("className")->getString();
        if (humanObjectNameMap.count(memoryName))
        {
            return humanObjectNameMap.at(memoryName);
        }
        else
        {
            return boost::regex_replace(memoryName, re, std::string(" "));
        }
    };

    auto display = in.getDisplay_GraspObject();
    if (display.size() == 2)
    {
        std::string resolvedName = resolveObjectName(prev.getSelectedObject());
        ARMARX_INFO << "Resolved object name: " << resolvedName;
        boost::replace_all(display[1], "{0}", resolvedName);
        getMessageDisplay()->setMessage(display[0], display[1]);
    }
    else
    {
        ARMARX_WARNING << "Display text not set (wrong size): " << display.size();
    }
}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CleanUpTask::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CleanUpTask(stateData));
}
