/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::CartesianControlGroup::CartesianControlGroupRemoteStateOfferer
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CartesianControlGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace CartesianControlGroup;

// DO NOT EDIT NEXT LINE
CartesianControlGroupRemoteStateOfferer::SubClassRegistry CartesianControlGroupRemoteStateOfferer::Registry(CartesianControlGroupRemoteStateOfferer::GetName(), &CartesianControlGroupRemoteStateOfferer::CreateInstance);



CartesianControlGroupRemoteStateOfferer::CartesianControlGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < CartesianControlGroupStatechartContext > (reader)
{
}

void CartesianControlGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void CartesianControlGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void CartesianControlGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string CartesianControlGroupRemoteStateOfferer::GetName()
{
    return "CartesianControlGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr CartesianControlGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new CartesianControlGroupRemoteStateOfferer(reader));
}
