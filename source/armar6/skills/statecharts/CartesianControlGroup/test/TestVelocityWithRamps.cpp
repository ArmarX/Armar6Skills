/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::CartesianControlGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TestVelocityWithRamps.h"

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <Eigen/Dense>
#include <RobotAPI/libraries/core/EigenHelpers.h>
#include <VirtualRobot/Robot.h>

using namespace armarx;
using namespace CartesianControlGroup;

// DO NOT EDIT NEXT LINE
TestVelocityWithRamps::SubClassRegistry TestVelocityWithRamps::Registry(TestVelocityWithRamps::GetName(), &TestVelocityWithRamps::CreateInstance);



void TestVelocityWithRamps::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void TestVelocityWithRamps::run()
{
    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet(in.getNodeSetName());
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();

    NameControlModeMap positionControlModes;
    NameControlModeMap velocityControlModes;

    for (const std::pair<std::string, float> pair : in.getInitialPosition())
    {
        positionControlModes[pair.first] = ePositionControl;
    }
    for (const std::pair<std::string, float> pair : in.getInitialVelocity())
    {
        velocityControlModes[pair.first] = eVelocityControl;
    }

    getKinematicUnit()->switchControlMode(positionControlModes);
    getKinematicUnit()->setJointAngles(in.getInitialPosition());

    //TimeUtil::Sleep(IceUtil::Time::secondsDouble(in.getInitialPositionWaitTime()));

    usleep((int)(in.getInitialPositionWaitTime() * 1000000));

    getKinematicUnit()->switchControlMode(velocityControlModes);
    getKinematicUnit()->setJointVelocities(in.getInitialVelocity());

    //TimeUtil::Sleep(IceUtil::Time::secondsDouble(in.getInitialVelocityWaitTIme()));
    usleep((int)(in.getInitialVelocityWaitTIme() * 1000000));

    NJointCartesianVelocityControllerWithRampConfigPtr config = new NJointCartesianVelocityControllerWithRampConfig(in.getNodeSetName(), "", CartesianSelectionMode::eAll,
            in.getmaxPositionAcceleration(), in.getmaxOrientationAcceleration(), in.getmaxNullspaceAcceleration(),
            in.getKpJointLimitAvoidance(), in.getjointLimitAvoidanceScale());

    NJointCartesianVelocityControllerWithRampInterfacePrx controller = NJointCartesianVelocityControllerWithRampInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointCartesianVelocityControllerWithRamp", "testController", config));
    Eigen::VectorXf jv = MakeVectorXf(in.getTargetVelocity());
    controller->setTargetVelocity(jv(0), jv(1), jv(2), jv(3), jv(4), jv(5));
    controller->activateController();


    IceUtil::Time last = TimeUtil::GetTime();

    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
    Eigen::Affine3f lastPose;
    lastPose.matrix() = tcp->getPoseInRootFrame();

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        //TimeUtil::Sleep(IceUtil::Time::secondsDouble(0.01));
        usleep(100000);

        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        IceUtil::Time now = TimeUtil::GetTime();

        float dt = (now - last).toSecondsDouble();

        Eigen::Affine3f currentPose;
        currentPose.matrix() = tcp->getPoseInRootFrame();

        Eigen::Vector3f posDiff = currentPose.translation() - lastPose.translation();
        Eigen::Vector3f posVel = posDiff / dt;
        getDebugObserver()->setDebugDatafield("TestVelocityWithRamps", "vx", new Variant(posVel(0)));
        getDebugObserver()->setDebugDatafield("TestVelocityWithRamps", "vy", new Variant(posVel(1)));
        getDebugObserver()->setDebugDatafield("TestVelocityWithRamps", "vz", new Variant(posVel(2)));


        lastPose = currentPose;
        last = now;
    }



    controller->deactivateController();
    while (controller->isControllerActive())
    {
        //TimeUtil::Sleep(IceUtil::Time::secondsDouble(0.01));
        usleep(10000);
    }

    controller->deleteController();
}

//void TestVelocityWithRamps::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void TestVelocityWithRamps::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TestVelocityWithRamps::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TestVelocityWithRamps(stateData));
}
