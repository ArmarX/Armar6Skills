/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HighlevelTaskGroup
 * @author     Mirko Wächter ( mirko dot waechter at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PlaceObjectTask.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6HighlevelTaskGroup;

// DO NOT EDIT NEXT LINE
PlaceObjectTask::SubClassRegistry PlaceObjectTask::Registry(PlaceObjectTask::GetName(), &PlaceObjectTask::CreateInstance);



void PlaceObjectTask::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    //    ARMARX_CHECK_GREATER(in.getargs().size(), 0);
    std::string objectName;
    if (in.isargsSet() && in.getargs().size() > 0)
    {
        objectName = in.getargs().at(0)->entityName;
    }
    else if (in.isFixedObjectNameSet())
    {
        objectName = in.getFixedObjectName();
    }
    else
    {
        throw LocalException() << "An object name must be given either with the parameters args(0) or FixedObjectName";
    }
    ARMARX_CHECK_EXPRESSION(!objectName.empty());
    local.setObjectName(objectName);
    auto channel = getObjectMemoryObserver()->getFirstObjectInstanceByClass(objectName);
    ARMARX_CHECK_EXPRESSION(channel) << objectName;
    local.setObjectInstanceChannel(channel);
}

//void PlaceObjectTask::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//    // get a private kinematic instance for this state of the robot (tick "Robot State Component" proxy checkbox in statechart group)
//    VirtualRobot::RobotPtr robot = getLocalRobot();
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//        // synchronize robot clone to most recent state
//        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
//    }
//}

//void PlaceObjectTask::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void PlaceObjectTask::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr PlaceObjectTask::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new PlaceObjectTask(stateData));
}
