/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HighlevelTaskGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MovePlatformSkill.h"

#include <MemoryX/libraries/memorytypes/variants/GraphNode/GraphNode.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
//#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6HighlevelTaskGroup;

// DO NOT EDIT NEXT LINE
MovePlatformSkill::SubClassRegistry MovePlatformSkill::Registry(MovePlatformSkill::GetName(), &MovePlatformSkill::CreateInstance);



void MovePlatformSkill::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    memoryx::GraphNodePtr landmark = memoryx::GraphNodePtr::dynamicCast(in.getargs().at(2)->getEntity());
    ARMARX_CHECK_EXPRESSION(landmark);
    // if the landmark is not global then its a special landmark associated with an object
    // (e.g. place setting on a table)

    local.setLandmarkString(landmark->getName());

}

//void MovePlatformSkill::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void MovePlatformSkill::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MovePlatformSkill::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MovePlatformSkill::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MovePlatformSkill(stateData));
}
