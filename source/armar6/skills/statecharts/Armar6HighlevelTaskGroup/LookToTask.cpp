/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HighlevelTaskGroup
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LookToTask.h"

#include <SimoxUtility/algorithm/string/string_tools.h>

using namespace armarx;
using namespace Armar6HighlevelTaskGroup;

// DO NOT EDIT NEXT LINE
LookToTask::SubClassRegistry LookToTask::Registry(LookToTask::GetName(), &LookToTask::CreateInstance);


void LookToTask::onEnter()
{
    std::string direction;

    if (in.isHeadDirectionSet())
    {
        direction = in.getHeadDirection();
    }
    else if (in.isargsSet() && in.getargs().size() >= 1)
    {
        std::vector<memoryx::EntityRefPtr> args = in.getargs();
        direction = args.at(0)->entityName;
    }
    else
    {
        throw LocalException("Either args or HeadDirection have to be set");
    }


    ARMARX_WARNING << VAROUT(direction);
    ARMARX_IMPORTANT << "Looking in direction: " << direction;

    std::map<std::string, ::armarx::StringValueMapPtr> const& poses = in.getHeadPoses();
    StringValueMapPtr selectedDirectionJointValues;
    for (auto& pose : poses)
    {
        if (simox::alg::to_lower(pose.first) == simox::alg::to_lower(direction))
        {
            selectedDirectionJointValues = pose.second;
            break;
        }
    }
    if (!selectedDirectionJointValues)
    {
        ARMARX_WARNING << "Unknown direction: " << direction  << "\n Available directions: " << poses;
        emitFailure();
    }
    else
    {
        try
        {
            for (auto& pair : in.getTextReply())
            {
                if (simox::alg::to_lower(pair.first) == simox::alg::to_lower(direction))
                {
                    std::string const& textReply = pair.second;
                    getTextToSpeech()->reportText(textReply);
                    break;
                }
            }
        }
        catch (std::exception const& ex)
        {
            ARMARX_WARNING << "Text output failed: " << ex.what();
        }

        in.setalternativeJointMap(selectedDirectionJointValues->toStdMap<float>());
    }

    {
        objpose::SignalHeadMovementInput input;
        input.action = objpose::HeadMovementAction_Starting;
        input.discardUpdatesIntervalMilliSeconds = in.getObjectPoseStorageDiscardUpdatesIntervalMS();
        getObjectPoseStorage()->signalHeadMovement(input);
    }
}

//void LookToTask::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void LookToTask::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void LookToTask::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
    if (false) // We should not do this here, just wait until timeout runs out.
    {
        objpose::SignalHeadMovementInput input;
        input.action = objpose::HeadMovementAction_Stopping;
        input.discardUpdatesIntervalMilliSeconds = -1;
        getObjectPoseStorage()->signalHeadMovement(input);
    }
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr LookToTask::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new LookToTask(stateData));
}

