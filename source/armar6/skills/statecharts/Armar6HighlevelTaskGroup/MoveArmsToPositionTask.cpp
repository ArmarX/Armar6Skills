/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HighlevelTaskGroup
 * @author     armar6-user@kit.edu ( [Author Email] )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveArmsToPositionTask.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6HighlevelTaskGroup;

// DO NOT EDIT NEXT LINE
MoveArmsToPositionTask::SubClassRegistry MoveArmsToPositionTask::Registry(MoveArmsToPositionTask::GetName(), &MoveArmsToPositionTask::CreateInstance);



void MoveArmsToPositionTask::onEnter()
{
    std::vector<memoryx::EntityRefPtr> args = in.getargs();
    std::string name = args.at(0)->entityName;
    ARMARX_IMPORTANT << "Moving to position: '" << name << "'";

    int openHand = 0;
    auto openHandValues = in.getOpenHand();
    auto openHandIter = openHandValues.find(name);
    if (openHandIter != in.getOpenHand().end())
    {
        openHand = openHandIter->second;
    }
    ARMARX_IMPORTANT << "Opening hand: " << openHand;
    local.setOpenBothHands(openHand);

    std::map<std::string, ::armarx::StringValueMapPtr> const& poses = in.getArmJointValues();
    auto iter = poses.find(name);
    if (iter == poses.end())
    {
        ARMARX_WARNING << "Unknown position: '" << name << "'";
        emitFailure();
    }
    else
    {
        try
        {
            std::string const& textReply = in.getTextReply().at(name);
            getTextToSpeech()->reportText(textReply);
        }
        catch (std::exception const& ex)
        {
            ARMARX_WARNING << "Text output failed: " << ex.what();
        }

        ::armarx::StringValueMapPtr map = iter->second;
        in.setalternativeJointMap(map->toStdMap<float>());
    }
}

//void MoveArmsToPositionTask::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void MoveArmsToPositionTask::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MoveArmsToPositionTask::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveArmsToPositionTask::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveArmsToPositionTask(stateData));
}
