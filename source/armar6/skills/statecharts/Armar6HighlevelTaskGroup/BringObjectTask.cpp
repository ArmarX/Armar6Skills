/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HighlevelTaskGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BringObjectTask.h"

#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6HighlevelTaskGroup;

// DO NOT EDIT NEXT LINE
BringObjectTask::SubClassRegistry BringObjectTask::Registry(BringObjectTask::GetName(), &BringObjectTask::CreateInstance);



void BringObjectTask::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)

    KinematicUnitHelper kinUnitHelper(getKinematicUnit());
    kinUnitHelper.setJointAngles(in.getInitialJointAngles());

    if (in.isargsSet())
    {
        const std::vector<::memoryx::EntityRefPtr> args = in.getargs();

        const std::string objectName = args.at(0)->entityName;
        ARMARX_IMPORTANT << "Bringing object: " << objectName;

        local.setObjectName(objectName);
        // args.at(1) is the FROM location
        if (args.size() > 2)
        {
            local.setHandoverLandmark(args.at(2)->entityName);
            ARMARX_INFO << "Handing over at: " << args.at(0)->entityName;
        }
    }
    else
    {
        local.setObjectName(in.getOverwriteObjectName());
        std::string overwriteLandmark = in.getOverwriteHandoverLandmark();
        if (overwriteLandmark.size() > 0)
        {
            local.setHandoverLandmark(in.getOverwriteHandoverLandmark());
        }
    }
}

//void BringObjectTask::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void BringObjectTask::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void BringObjectTask::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr BringObjectTask::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new BringObjectTask(stateData));
}
