/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HighlevelTaskGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BringObjectAndPutAwayTask.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectID.h>


using namespace armarx;
using namespace Armar6HighlevelTaskGroup;

// DO NOT EDIT NEXT LINE
BringObjectAndPutAwayTask::SubClassRegistry BringObjectAndPutAwayTask::Registry(BringObjectAndPutAwayTask::GetName(), &BringObjectAndPutAwayTask::CreateInstance);



void BringObjectAndPutAwayTask::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)

    std::vector<::memoryx::EntityRefPtr> args;
    if (in.isargsSet())
    {
        args = in.getargs();
    }
    const std::string objectName = args.empty() ? in.getDefaultObjectName() : args.at(0)->entityName;
    ARMARX_IMPORTANT << "Bringing object '" << objectName << "'";
    local.setObjectName(objectName);
}

//void BringObjectAndPutAwayTask::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void BringObjectAndPutAwayTask::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void BringObjectAndPutAwayTask::onExit()
{
    getMessageDisplay()->setMessage("", "");
}

void BringObjectAndPutAwayTask::transitionSuccessFromBringObjectToLandmark(HandOverGroup::ReceiveFromRobotIn& next, const Armar6GraspingGroup::BringObjectToLandmarkOut& prev)
{
    (void) next, (void) prev;

    std::string objectName = local.getObjectName();
    if (objectName.find("/") != std::string::npos)
    {
        // ToDo @Rainer: Load spoken names from prior knowledge.
        ObjectID id(objectName);
        objectName = id.className();
    }
    getTextToSpeech()->reportText("Here is the " + objectName + " for you.");
}

void BringObjectAndPutAwayTask::transitionSuccessFromHandOverToRobot(Armar6HeadGroup::LookStraightIn& next, const HandOverGroup::HandOverToRobotOut& prev)
{
    (void) next;
    {
        const std::string objectID = prev.getNewObjectID();

        ARMARX_INFO << "Finding object instance channel for new object ID: " << objectID;
        auto channel = getObjectMemoryObserver()->getObjectInstanceById(objectID);
        if (channel)
        {
            local.setObjectInstanceChannel(channel);
        }
    }
    {
        std::string objectName = local.getObjectName();
        if (objectName.find("/") != std::string::npos)
        {
            // ToDo @Rainer: Load spoken names from prior knowledge.
            ObjectID id(objectName);
            objectName = id.className();
        }
        getTextToSpeech()->reportText("I will put the " + objectName + " away.");
    }
}




// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr BringObjectAndPutAwayTask::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new BringObjectAndPutAwayTask(stateData));
}
