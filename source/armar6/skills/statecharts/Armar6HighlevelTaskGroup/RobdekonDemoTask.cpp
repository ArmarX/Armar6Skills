/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HighlevelTaskGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RobdekonDemoTask.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

namespace armarx
{
    namespace Armar6HighlevelTaskGroup
    {
        // DO NOT EDIT NEXT LINE
        RobdekonDemoTask::SubClassRegistry RobdekonDemoTask::Registry(RobdekonDemoTask::GetName(), &RobdekonDemoTask::CreateInstance);

        void RobdekonDemoTask::onEnter()
        {
            //getArmar6DemoConnector()->startArmar6Skill("ROBDEKON", new aron::AronObject);
        }

        void RobdekonDemoTask::run()
        {
            while (!isRunningTaskStopped())
            {
                TimeUtil::SleepMS(10);
                Armar6DemoConnectorStatus s = getArmar6DemoConnector()->getStatus();
                if (s.status == "Completed")
                {
                    ARMARX_IMPORTANT << "Skill ROBDEKON completed";
                    emitSuccess();
                    return;
                }
                else if (s.status == "Started")
                {
                    ARMARX_INFO << deactivateSpam(1) << "Waiting for skill ROBDEKON to start";
                }
                else if (s.status == "Running")
                {
                    ARMARX_INFO << deactivateSpam(1) << "Skill ROBDEKON is running";
                }
                else
                {
                    ARMARX_WARNING << "unknown status: " << s.status;
                }

                getArmar6DemoConnector()->notifyKeepalive("ROBDEKON");

            }
            getArmar6DemoConnector()->stopArmar6Skill("ROBDEKON");
            emitSuccess();
        }

        //void RobdekonDemoTask::onBreak()
        //{
        //    // put your user code for the breaking point here
        //    // execution time should be short (<100ms)
        //}

        void RobdekonDemoTask::onExit()
        {
            // put your user code for the exit point here
            // execution time should be short (<100ms)
        }


        // DO NOT EDIT NEXT FUNCTION
        XMLStateFactoryBasePtr RobdekonDemoTask::CreateInstance(XMLStateConstructorParams stateData)
        {
            return XMLStateFactoryBasePtr(new RobdekonDemoTask(stateData));
        }
    }
}
