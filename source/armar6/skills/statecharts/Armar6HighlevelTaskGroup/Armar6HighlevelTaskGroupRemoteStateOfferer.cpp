/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HighlevelTaskGroup::Armar6HighlevelTaskGroupRemoteStateOfferer
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6HighlevelTaskGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6HighlevelTaskGroup;

// DO NOT EDIT NEXT LINE
Armar6HighlevelTaskGroupRemoteStateOfferer::SubClassRegistry Armar6HighlevelTaskGroupRemoteStateOfferer::Registry(Armar6HighlevelTaskGroupRemoteStateOfferer::GetName(), &Armar6HighlevelTaskGroupRemoteStateOfferer::CreateInstance);



Armar6HighlevelTaskGroupRemoteStateOfferer::Armar6HighlevelTaskGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6HighlevelTaskGroupStatechartContext > (reader)
{
}

void Armar6HighlevelTaskGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6HighlevelTaskGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6HighlevelTaskGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6HighlevelTaskGroupRemoteStateOfferer::GetName()
{
    return "Armar6HighlevelTaskGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6HighlevelTaskGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6HighlevelTaskGroupRemoteStateOfferer(reader));
}
