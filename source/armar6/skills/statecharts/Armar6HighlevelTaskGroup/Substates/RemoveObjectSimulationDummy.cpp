/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HighlevelTaskGroup
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RemoveObjectSimulationDummy.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6HighlevelTaskGroup;

// DO NOT EDIT NEXT LINE
RemoveObjectSimulationDummy::SubClassRegistry RemoveObjectSimulationDummy::Registry(RemoveObjectSimulationDummy::GetName(), &RemoveObjectSimulationDummy::CreateInstance);



void RemoveObjectSimulationDummy::onEnter()
{
    //    std::string instanceName = in.getObject()->getDataField("instanceName")->getString();
    //    std::string entityId = in.getObject()->getDataField("id")->getString();

    //    ARMARX_INFO << "Removing object '" << instanceName << "' (id " << entityId << ") from simulated scene";
    //    getSimulatorInterface()->removeObject(instanceName);

    //    ARMARX_INFO << "Removing object '" << instanceName << "' (id " << entityId << ") from working memory";
    //    getWorkingMemory()->getObjectInstancesSegment()->removeEntity(entityId);
}

void RemoveObjectSimulationDummy::onExit()
{
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr RemoveObjectSimulationDummy::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new RemoveObjectSimulationDummy(stateData));
}
