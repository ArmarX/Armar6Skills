/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HighlevelTaskGroup
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "WaitForGuardSupportCommand.h"

#include <ArmarXCore/core/time/CycleUtil.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6HighlevelTaskGroup;

// DO NOT EDIT NEXT LINE
WaitForGuardSupportCommand::SubClassRegistry WaitForGuardSupportCommand::Registry(WaitForGuardSupportCommand::GetName(), &WaitForGuardSupportCommand::CreateInstance);



void WaitForGuardSupportCommand::onEnter()
{
}

std::string const GUARD_SUPPORT = "guardsupport";

void WaitForGuardSupportCommand::run()
{
    const armarx::PoseBasedActionRecognitionInterfacePrx& recognition = getPoseBasedActionRecognition();

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    CycleUtil c(in.getCycleTimeMs());
    int consecutiveGuardSupport = 0;
    int minCount = in.getMinConsecutiveCount();
    while (!isRunningTaskStopped())
    {
        //ARMARX_INFO << VAROUT(consecutiveGuardSupport);

        std::string label = recognition->getLatestRecognition();
        //ARMARX_IMPORTANT << VAROUT(label);

        if (label == GUARD_SUPPORT)
        {
            ++consecutiveGuardSupport;
        }
        else
        {
            consecutiveGuardSupport = 0;
        }

        if (consecutiveGuardSupport >= minCount)
        {
            ARMARX_IMPORTANT << "Detected guard support after " << consecutiveGuardSupport << " classifications";
            emitGuardSupportDetected();
        }

        c.waitForCycleDuration();
    }
}

//void WaitForGuardSupportCommand::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void WaitForGuardSupportCommand::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr WaitForGuardSupportCommand::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new WaitForGuardSupportCommand(stateData));
}
