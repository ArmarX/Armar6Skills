/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HighlevelTaskGroup
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SelectObjectToRemove.h"

#include <ArmarXCore/observers/variant/ChannelRef.h>
#include <RobotAPI/libraries/core/FramedPose.h>

using namespace armarx;
using namespace Armar6HighlevelTaskGroup;

// DO NOT EDIT NEXT LINE
SelectObjectToRemove::SubClassRegistry SelectObjectToRemove::Registry(SelectObjectToRemove::GetName(), &SelectObjectToRemove::CreateInstance);



void SelectObjectToRemove::onEnter()
{
    std::vector<armarx::ChannelRefPtr> objects = in.getFoundObjects();
    float maxRemovalDistance = in.getMaxRemovalDistance();

    ARMARX_INFO << "Found " << objects.size() << " objects: \n" << objects;

    std::vector<Eigen::Vector3f> objectPositions;
    std::vector<Eigen::Vector3f> objectGlobalPositions;
    std::vector<std::string> objectInstanceNames;
    std::vector<armarx::ChannelRefPtr> consideredObjects;

    ARMARX_INFO <<  " limits: \n"
                << VAROUT(in.getObjectMinX()) << "\t/ " << VAROUT(in.getObjectMaxX())
                << VAROUT(in.getObjectMinY()) << "\t/ " << VAROUT(in.getObjectMaxY());

    for (auto& object : objects)
    {

        try
        {
            if (!getWorkingMemory()->getObjectInstancesSegment()->hasEntityById(object->getDataField("id")->getString()))
            {
                ARMARX_INFO << "Ignoring object '" << object->getDataField("instanceName")->getString() << "'. It was deleted from the memory";
                continue;
            }
        }
        catch (InvalidChannelException&)
        {
            ARMARX_INFO << "Ignoring object. Its channel was deleted";
            continue;
        }


        Eigen::Vector3f position = object->getDataField("position")->get<FramedPosition>()->toRootEigen(getRobot());
        Eigen::Vector3f gposition = object->getDataField("position")->get<FramedPosition>()->toGlobalEigen(getRobot());
        position(2) = 0;

        if (maxRemovalDistance >= 0 && position.norm() > maxRemovalDistance)
        {
            ARMARX_INFO << "Ignoring object '" << object->getDataField("instanceName")->getString() << "'. distance = " << position.norm();
            continue;
        }
        //filter for desk pos
        if (
            gposition(0) < in.getObjectMinX() || gposition(0) > in.getObjectMaxX() ||
            gposition(1) < in.getObjectMinY() || gposition(1) > in.getObjectMaxY()
        )
        {
            ARMARX_INFO << "Ignoring object '" << object->getDataField("instanceName")->getString() <<  "' position " << gposition.block<2, 1>(0, 0).transpose();

            continue;
        }

        objectPositions.push_back(position);
        objectGlobalPositions.push_back(gposition);
        objectInstanceNames.push_back(object->getDataField("instanceName")->getString());
        consideredObjects.push_back(object);
    }

    if (consideredObjects.size() == 0)
    {
        ARMARX_IMPORTANT << "No objects left on the table";
        emitNoObjectsFound();
        return;
    }

    ARMARX_INFO << "Objects found: " << objectInstanceNames;
    ARMARX_INFO << (objects.size() - objectPositions.size()) << " objects filtered due to exceeding distance";

    int selectedIndex = -1;
    if (in.getStrategy() == "nearest")
    {
        for (unsigned int i = 0; i < consideredObjects.size(); i++)
        {
            if (selectedIndex < 0)
            {
                ARMARX_INFO << "Considering object " << objectInstanceNames.at(i) << ", position = " << objectPositions.at(i) << ", norm = " << objectPositions.at(i).norm() << ", current best = none";
            }
            else
            {
                ARMARX_INFO << "Considering object " << objectInstanceNames.at(i) << ", position = " << objectPositions.at(i) << ", norm = " << objectPositions.at(i).norm() << ", current best = " << objectPositions.at(selectedIndex) << ", norm = " << objectPositions.at(selectedIndex).norm();
            }
            if (selectedIndex < 0 || objectPositions.at(i).norm() < objectPositions.at(selectedIndex).norm())
            {
                selectedIndex = i;
            }
        }
    }
    else if (in.getStrategy() == "r2l")
    {

        for (unsigned int i = 0; i < consideredObjects.size(); i++)
        {
            if (selectedIndex < 0)
            {
                ARMARX_INFO << "Considering object " << objectInstanceNames.at(i) << ", position = " << objectPositions.at(i) << ", pos = " << objectPositions.at(i).transpose() << ", current best = none";
            }
            else
            {
                ARMARX_INFO << "Considering object " << objectInstanceNames.at(i) << ", position = " << objectPositions.at(i) << ", pos = " << objectPositions.at(i).transpose() << ", current best = " << objectPositions.at(selectedIndex) << ", pos = " << objectPositions.at(selectedIndex).transpose();
            }
            if (selectedIndex < 0 || objectPositions.at(i)(0) > objectPositions.at(selectedIndex)(0))
            {
                selectedIndex = i;
            }
        }
    }
    else if (in.getStrategy() == "r2m_l2m")
    {
        std::size_t indexMinX = 0;
        std::size_t indexMaxX = 0;
        for (std::size_t i = 0; i < consideredObjects.size(); i++)
        {
            if (objectPositions.at(i)(0) > objectPositions.at(indexMaxX)(0))
            {
                indexMaxX = i;
            }
            if (objectPositions.at(i)(0) < objectPositions.at(indexMinX)(0))
            {
                indexMinX = i;
            }
        }
        if (objectGlobalPositions.at(indexMaxX)(0) >= in.getWorkbenchMiddleX())
        {
            selectedIndex = indexMaxX;
        }
        else
        {
            selectedIndex = indexMinX;
        }
    }
    else
    {
        ARMARX_ERROR << "Unknown selection strategy '" << in.getStrategy() << "'";
    }

    if (selectedIndex < 0)
    {
        ARMARX_ERROR << "No object selected";
        emitFailure();
        return;
    }

    ARMARX_IMPORTANT << "Selected object " << objectInstanceNames.at(selectedIndex) << " (index " << selectedIndex << ") for removal";
    out.setSelectedObject(consideredObjects.at(selectedIndex));

    emitSuccess();
}

void SelectObjectToRemove::onExit()
{
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SelectObjectToRemove::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SelectObjectToRemove(stateData));
}
