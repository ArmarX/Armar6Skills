/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HighlevelTaskGroup
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SelectArm.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <SimoxUtility/algorithm/string/string_tools.h>

using namespace armarx;
using namespace Armar6HighlevelTaskGroup;

// DO NOT EDIT NEXT LINE
SelectArm::SubClassRegistry SelectArm::Registry(SelectArm::GetName(), &SelectArm::CreateInstance);



void SelectArm::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    std::vector<memoryx::EntityRefPtr> args = in.getargs();

    const std::string arm = simox::alg::capitalize_words(args.at(0)->entityName);
    ARMARX_IMPORTANT << "Using arm: " << arm;

    try
    {
        std::string const textReply = in.getTextReply().at(arm);
        getTextToSpeech()->reportText(textReply);
    }
    catch (std::exception const& ex)
    {
        ARMARX_WARNING << "Text output failed: " << ex.what();
    }

    if (arm == "Left" || arm == "Left arm")
    {
        emitLeft();
    }
    else if (arm == "Right" || arm == "Right arm")
    {
        emitRight();
    }
    else
    {
        ARMARX_WARNING << "Unknown arm: " << arm;
        emitFailure();
    }
}

//void SelectArm::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void SelectArm::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void SelectArm::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SelectArm::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SelectArm(stateData));
}
