/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     JeffGao ( jianfenggaobit at gmail dot com )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveToPickupLocationWithMarker.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <VirtualRobot/math/Helpers.h>
#include <VirtualRobot/math/Line.h>

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
MoveToPickupLocationWithMarker::SubClassRegistry MoveToPickupLocationWithMarker::Registry(MoveToPickupLocationWithMarker::GetName(), &MoveToPickupLocationWithMarker::CreateInstance);



void MoveToPickupLocationWithMarker::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}



void MoveToPickupLocationWithMarker::run()
{
    VirtualRobot::RobotPtr robot = getLocalRobot();

    //PhaseType currentPhase = PhaseType::LocalizeMarkers;

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

        //        if (currentPhase == PhaseType::LocalizeMarkers)
        //        {

        //            Eigen::Vector3f marker1pos;
        //            Eigen::Vector3f marker2pos;

        //            int marker1id = 614;
        //            int marker2id = 617;
        //            bool localized1 = false;
        //            bool localized2 = false;


        //            visionx::ArMarkerLocalizationResultList markers = getArMarkerLocalizer()->GetLatestLocalizationResult();
        //            for (const visionx::ArMarkerLocalizationResult& r : markers)
        //            {
        //                FramedPosePtr posePtr = FramedPosePtr::dynamicCast(r.pose);
        //                Eigen::Matrix4f pose = posePtr->toRootEigen(robot);
        //                ARMARX_IMPORTANT << r.id << " " << math::Helpers::GetPosition(pose).transpose();

        //                if (r.id == marker1id)
        //                {
        //                    localized1 = true;
        //                    marker1pos = math::Helpers::GetPosition(pose);
        //                }
        //                if (r.id == marker2id)
        //                {
        //                    localized2 = true;
        //                    marker2pos = math::Helpers::GetPosition(pose);
        //                }
        //            }

        //            if (localized1 && localized2)
        //            {
        //                ARMARX_IMPORTANT << "Both markers localized. Positions: " << marker1pos.transpose() << "; " << marker2pos.transpose() << "; distance: " << (marker1pos - marker2pos).norm();

        //                marker1pos(2) = 0;
        //                marker2pos(2) = 0;
        //                math::Line line = math::Line::FromPoints(marker1pos, marker2pos);
        //                //line.Get(0.5f)

        //                float platformX = getPlatformUnitObserver()->getDatafieldByName("platformPose", "positionX")->getFloat();
        //                float platformY = getPlatformUnitObserver()->getDatafieldByName("platformPose", "positionY")->getFloat();
        //                float platformA = getPlatformUnitObserver()->getDatafieldByName("platformPose", "rotation")->getFloat();
        //            }

        //        }

        TimeUtil::SleepMS(10);
    }
}

//void MoveToPickupLocationWithMarker::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MoveToPickupLocationWithMarker::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveToPickupLocationWithMarker::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveToPickupLocationWithMarker(stateData));
}
