/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     JeffGao ( jianfenggaobit at gmail dot com )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "InsertGuardV2.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <armar6/skills/statecharts/Armar6BoardSupportDemo/Armar6BoardSupportDemoStatechartContext.generated.h>

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
InsertGuardV2::SubClassRegistry InsertGuardV2::Registry(InsertGuardV2::GetName(), &InsertGuardV2::CreateInstance);



void InsertGuardV2::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)

    getDebugDrawerTopic()->clearAll();
    auto display = in.getDisplay_InsertingTheGuard();
    if (display.size() == 2)
    {
        getMessageDisplay()->setMessage(display[0], display[1]);
    }
    else
    {
        ARMARX_WARNING << "Display text not set (wrong size): " << display.size();
    }



    if (in.getIsFingerProtect())
    {
        local.setValue(0);
    }
    else
    {
        local.setValue(1);
    }


    getTextToSpeech()->reportText(in.getTextForGuardInsertion());
    //    getLookAtHand()->enableLookAtHand();

}

void InsertGuardV2::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    // get a private kinematic instance for this state of the robot (tick "Robot State Component" proxy checkbox in statechart group)
    VirtualRobot::RobotPtr localRobot = getLocalRobot();


    DatafieldRefPtr thumbDFLeft =  DatafieldRefPtr::dynamicCast(getKinematicUnitObserver()->getDatafieldRefByName("jointangles", "LeftHandThumb"));
    DatafieldRefPtr thumbDFRight =  DatafieldRefPtr::dynamicCast(getKinematicUnitObserver()->getDatafieldRefByName("jointangles", "RightHandThumb"));
    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx leftHandcontroller;
    if (!getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"))
    {
        leftHandcontroller = StateBase::getContext<Armar6BoardSupportDemoStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_L_EEF_NJointKITHandV2ShapeController");
    }
    else
    {
        leftHandcontroller =  devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"));
    }
    leftHandcontroller->activateController();

    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx rightHandcontroller;
    if (!getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"))
    {
        rightHandcontroller = StateBase::getContext<Armar6BoardSupportDemoStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_R_EEF_NJointKITHandV2ShapeController");
    }
    else
    {
        rightHandcontroller = devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"));
    }
    rightHandcontroller->activateController();
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

    leftHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
    rightHandcontroller->setTargetsWithPwm(0, 2, 1, 1);

    usleep(in.getThumbCalibrationTime() * 1000);
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

    float thumbLeftClose = thumbDFLeft->getDataField()->getFloat();
    float thumbRightClose = thumbDFRight->getDataField()->getFloat();

    leftHandcontroller->setTargetsWithPwm(0, 0, 1, 1);
    rightHandcontroller->setTargetsWithPwm(0, 0, 1, 1);

    float thumbLeftThreshold = thumbLeftClose - 0.1;
    float thumbRightThreshold = thumbRightClose + 0.1;

    ARMARX_INFO << "Thumb Threshold: " << " left: " << thumbLeftThreshold << " right changed " << thumbRightThreshold;

    local.setLeftThumbThreshold(thumbLeftThreshold);
    local.setRightThumbThreshold(thumbRightThreshold);
}

void InsertGuardV2::onBreak()
{
    //    getLookAtHand()->disableLookAtHand();

    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void InsertGuardV2::onExit()
{
    //    getLookAtHand()->disableLookAtHand();

    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr InsertGuardV2::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new InsertGuardV2(stateData));
}

