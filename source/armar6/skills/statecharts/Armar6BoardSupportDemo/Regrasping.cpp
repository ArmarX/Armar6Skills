/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Regrasping.h"

#include <armarx/control/deprecated_njoint_mp_controller/task_space/TaskSpaceImpedanceDMPControllerInterface.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <armar6/skills/statecharts/Armar6BoardSupportDemo/Armar6BoardSupportDemoStatechartContext.generated.h>
#include "Helpers.h"
#include <ArmarXCore/core/time/TimeUtil.h>
#include <RobotAPI/libraries/core/FramedPose.h>


using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
Regrasping::SubClassRegistry Regrasping::Registry(Regrasping::GetName(), &Regrasping::CreateInstance);



void Regrasping::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void Regrasping::run()
{
    //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");


    getTextToSpeech()->reportText(in.getTextForRegrasping());


    NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController(in.getLeftControllerName());
    if (controller)
    {
        getRobotUnit()->deactivateAndDeleteNJointController(in.getLeftControllerName());
        TimeUtil::SleepMS(10);
    }

    controller = getRobotUnit()->getNJointController(in.getRightControllerName());
    if (controller)
    {
        getRobotUnit()->deactivateAndDeleteNJointController(in.getRightControllerName());
        TimeUtil::SleepMS(10);
    }

    controller = getRobotUnit()->getNJointController(in.getRightControllerName() + "_recover");
    if (controller)
    {
        getRobotUnit()->deactivateAndDeleteNJointController(in.getRightControllerName() + "_recover");
        TimeUtil::SleepMS(10);
    }

    controller = getRobotUnit()->getNJointController(in.getLeftControllerName() + "_recover");
    if (controller)
    {
        getRobotUnit()->deactivateAndDeleteNJointController(in.getLeftControllerName() + "_recover");
        TimeUtil::SleepMS(10);
    }

    double phaseL = 100;
    double phaseK = 1000;
    double phaseDist0 = 100;
    double phaseDist1 = 50;
    double posToOriRatio = 10;

    std::vector<float> defaultLeftJointValues;
    if (in.isDesiredLeftJointValuesSet())
    {
        defaultLeftJointValues = in.getDesiredLeftJointValues();
    }
    else
    {
        defaultLeftJointValues = getRobot()->getRobotNodeSet("LeftArm")->getJointValues();
    }

    std::vector<float> defaultRightJointValues;
    if (in.isDesiredRightJointValuesSet())
    {
        defaultRightJointValues = in.getDesiredRightJointValues();
    }
    else
    {
        defaultRightJointValues = getRobot()->getRobotNodeSet("RightArm")->getJointValues();
    }

    float torqueLimit = in.getTorqueLimit();
    std::string forceSensorName = "FT L";
    float waitTimeForCalibration = 0.0;
    float forceFilter = 0.5;
    float forceDeadZone = 2;
    Eigen::Vector3f forceThreshold{100, 100, 100};
    std::string forceFrameName = "ArmL8_Wri2";

    NJointTaskSpaceImpedanceDMPControllerConfigPtr tsConfig = new NJointTaskSpaceImpedanceDMPControllerConfig(
        in.getDMPKernelSize(),
        in.getBaseMode(),
        in.getDMPStyle(),
        in.getTimeDuration(),
        "LeftArm",
        phaseL,
        phaseK,
        phaseDist0,
        phaseDist1,
        posToOriRatio,
        in.getKposVec(),
        in.getDposVec(),
        in.getKoriVec(),
        in.getDoriVec(),
        in.getKnull(),
        in.getDnull(),
        false,
        defaultLeftJointValues,
        torqueLimit,

        forceSensorName,
        waitTimeForCalibration,
        forceFilter,
        forceDeadZone,
        forceThreshold,
        forceFrameName
    );


    NJointTaskSpaceImpedanceDMPControllerInterfacePrx leftController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", in.getLeftControllerName(), tsConfig));

    tsConfig->timeDuration = in.getRecoverTimeDuration();
    NJointTaskSpaceImpedanceDMPControllerInterfacePrx leftRecoverController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", in.getLeftControllerName() + "_recover", tsConfig));


    tsConfig->nodeSetName = "RightArm";
    tsConfig->defaultNullSpaceJointValues = defaultRightJointValues;
    tsConfig->timeDuration = in.getTimeDuration();
    tsConfig->forceFrameName = "ArmR8_Wri2";
    tsConfig->forceSensorName = "FT R";


    NJointTaskSpaceImpedanceDMPControllerInterfacePrx rightController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", in.getRightControllerName(), tsConfig));

    tsConfig->timeDuration = in.getRecoverTimeDuration();
    NJointTaskSpaceImpedanceDMPControllerInterfacePrx rightRecoverController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", in.getRightControllerName() + "_recover", tsConfig));


    std::vector<std::string> fileNames = in.getLeftArmMotionFile();
    leftController->learnDMPFromFiles(fileNames);
    leftRecoverController->learnDMPFromFiles(fileNames);

    fileNames = in.getRightArmMotionFile();
    rightController->learnDMPFromFiles(fileNames);
    rightRecoverController->learnDMPFromFiles(fileNames);

    std::vector<double> leftGoals = Helpers::pose2dvec(in.getLeftTcpGoalPose()->toEigen());
    std::vector<double> leftCanValSet;

    {
        std::map<std::string, armarx::PosePtr> viaPoseMap = in.getLeftViaPose();

        for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
             it != viaPoseMap.end(); ++it)
        {
            double canVal = std::stod(it->first);
            leftController->setViaPoints(canVal, Helpers::pose2dvec(it->second->toEigen()));
            leftCanValSet.push_back(canVal);
        }
    }

    std::sort(leftCanValSet.begin(), leftCanValSet.end(), std::greater<double>());

    std::vector<double> rightGoals = Helpers::pose2dvec(in.getRightTcpGoalPose()->toEigen());
    std::vector<double> rightCanValSet;

    {
        std::map<std::string, armarx::PosePtr> viaPoseMap = in.getRightViaPose();
        for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
             it != viaPoseMap.end(); ++it)
        {
            double canVal = std::stod(it->first);
            rightController->setViaPoints(canVal, Helpers::pose2dvec(it->second->toEigen()));
            rightCanValSet.push_back(canVal);
        }
    }
    std::sort(rightCanValSet.begin(), rightCanValSet.end(), std::greater<double>());


    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx leftHandcontroller = devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"));
    leftHandcontroller->activateController();

    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx rightHandcontroller = devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"));
    rightHandcontroller->activateController();

    leftController->activateController();
    rightController->activateController();


    DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT"));
    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT"));
    Eigen::Vector3f initialForceLeft;
    initialForceLeft.setZero();
    Eigen::Vector3f initialForceRight;
    initialForceRight.setZero();
    VirtualRobot::RobotPtr localRobot = getLocalRobot();


    DatafieldRefPtr thumbDFLeft =  DatafieldRefPtr::dynamicCast(getKinematicUnitObserver()->getDatafieldRefByName("jointangles", "LeftHandThumb"));
    DatafieldRefPtr thumbDFRight =  DatafieldRefPtr::dynamicCast(getKinematicUnitObserver()->getDatafieldRefByName("jointangles", "RightHandThumb"));

    Eigen::Vector3f forceLeft;
    Eigen::Vector3f forceRight;


    if (in.getIsGuardPlacement())
    {
        if (leftCanValSet.size() < 2)
        {
            ARMARX_ERROR << "Lowering: the number of left via-points cannot be smaller than 2";
            emitFailure();
        }

        leftHandcontroller->setTargets(0, 0);
        leftController->runDMP(leftGoals);
        rightController->runDMP(rightGoals);

        IceUtil::Time forceTimer = IceUtil::Time::now();
        float forceIntegral = 0;

        /* Regrasping Lowering: Move Left Hand For Regrasping */
        while (!isRunningTaskStopped())
        {
            RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
            {
                FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
                forceLeft = forcePtr->toRootEigen(localRobot);
                forceLeft -= initialForceLeft;
            }


            if (leftController->getVirtualTime() < leftCanValSet[0] * in.getTimeDuration())
            {

                float forceMag = forceLeft.dot(in.getCloseLeftHandOri()->toEigen());
                double deltaT = (IceUtil::Time::now() - forceTimer).toSecondsDouble();
                if (forceMag > 5)
                {
                    forceIntegral += forceMag * deltaT;
                }
                else
                {
                    forceIntegral = 0;
                }
                forceTimer = IceUtil::Time::now();
                getDebugObserver()->setDebugDatafield("Regrasping_Lowering", "forceIntegral_FirstLeft", new Variant(forceIntegral));
                getDebugObserver()->setDebugDatafield("Regrasping_Lowering", "forceMag_FirstLeft", new Variant(forceMag));

                if (forceIntegral > in.getCloseLeftHandThreshold())
                {
                    leftHandcontroller->setTargetsWithPwm(0.5, 2, 1, 1);
                    leftController->stopDMP();
                    break;
                }

            }
            else
            {
                forceTimer = IceUtil::Time::now();
                RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
                {
                    FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
                    initialForceLeft = forcePtr->toRootEigen(localRobot);
                }
            }

            if (leftController->getVirtualTime() < leftCanValSet[1] * in.getTimeDuration())
            {
                leftHandcontroller->setTargetsWithPwm(0.5, 2, 1, 1);
                leftController->stopDMP();
                break;
            }
        }

        rightController->stopDMP();
        usleep(1000000);
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        ARMARX_INFO << "thumb left: " << thumbDFLeft->getDataField()->getFloat();
        /* Recovery Failure of Left Regrasping */
        bool speechOutputAfterWait = true;
        float thumbThresholdLeft = in.getLeftThumbThreshold();
        while (thumbDFLeft->getDataField()->getFloat() > thumbThresholdLeft)
        {

            if (speechOutputAfterWait)
            {
                getTextToSpeech()->reportText(in.getTextForFailureRecovery());
                speechOutputAfterWait = false;
            }
            leftController->deactivateController();
            while (!isRunningTaskStopped() && leftController->isControllerActive())
            {
                usleep(1000);
            }

            Eigen::Matrix4f targetRecover = localRobot->getRobotNodeSet("LeftArm")->getTCP()->getPoseInRootFrame();
            targetRecover(1, 3) += 20;

            std::vector<double> targetRecoverVec = Helpers::pose2dvec(targetRecover);

            leftRecoverController->setGoals(targetRecoverVec);
            Eigen::Matrix4f viaRecover = targetRecover;
            viaRecover(1, 3) = viaRecover(1, 3) - 150;
            viaRecover(2, 3) = viaRecover(2, 3) - 200;


            leftRecoverController->activateController();
            leftHandcontroller->setTargets(0, 0);
            usleep(1000000);
            leftRecoverController->runDMP(Helpers::pose2dvec(viaRecover));

            while (!isRunningTaskStopped() && !leftRecoverController->isFinished())
            {
                usleep(1000);
            }

            leftRecoverController->stopDMP();

            // re-calibrate thumb
            {
                leftHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
                usleep(in.getThumbCalibrationTime() * 1000);
                RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
                float thumbLeftClose = thumbDFLeft->getDataField()->getFloat();
                thumbThresholdLeft = thumbLeftClose - 0.1;
                leftHandcontroller->setTargetsWithPwm(0, 0, 1, 1);
                usleep(in.getThumbCalibrationTime() * 1000);
            }

            leftRecoverController->runDMP(Helpers::pose2dvec(targetRecover));

            forceIntegral = 0;
            forceTimer = IceUtil::Time::now();

            while (!isRunningTaskStopped())
            {
                if (leftRecoverController->getVirtualTime() < 0.6 * in.getRecoverTimeDuration())
                {
                    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
                    {
                        FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
                        forceLeft = forcePtr->toRootEigen(localRobot);
                        forceLeft -= initialForceLeft;
                    }

                    float forceMag = forceLeft.dot(in.getCloseLeftHandOri()->toEigen());
                    double deltaT = (IceUtil::Time::now() - forceTimer).toSecondsDouble();

                    if (forceMag > 5)
                    {
                        forceIntegral += forceMag * deltaT;
                    }
                    else
                    {
                        forceIntegral = 0;
                    }
                    forceTimer = IceUtil::Time::now();
                    getDebugObserver()->setDebugDatafield("Regrasping_Lowering", "forceIntegral_RecoveryLeft", new Variant(forceIntegral));
                    getDebugObserver()->setDebugDatafield("Regrasping_Lowering", "forceMag_RecoveryLeft", new Variant(forceMag));

                    if (forceIntegral > in.getCloseLeftHandThreshold() || leftRecoverController->isFinished())
                    {
                        leftHandcontroller->setTargetsWithPwm(0.5, 2, 1, 1);
                        usleep(1000000);
                        leftRecoverController->stopDMP();
                        break;
                    }

                }
                else
                {
                    forceTimer = IceUtil::Time::now();
                    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
                    {
                        FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
                        initialForceLeft = forcePtr->toRootEigen(localRobot);
                    }
                }

            }

            leftRecoverController->deactivateController();
            while (leftRecoverController->isControllerActive())
            {
                usleep(1000);
            }
            RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

        }
        /************************************************************/
        leftController->activateController();
        //        leftController->runDMPWithTime(leftGoals, in.getTimeDuration());
        leftController->resumeDMP();

        ARMARX_INFO << "leftController: " << leftGoals;

        forceTimer = IceUtil::Time::now();
        forceIntegral = 0;
        rightHandcontroller->setTargets(0, 0);

        rightController->resumeDMP();
        /* move right arm to Regrasping targetb */
        while (!isRunningTaskStopped())
        {
            ARMARX_INFO << "current canVal: " << rightCanValSet[rightCanValSet.size() - 1];
            if (rightController->getVirtualTime() < rightCanValSet[rightCanValSet.size() - 1] * in.getTimeDuration())
            {
                RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
                {
                    FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
                    forceRight = forcePtr->toRootEigen(localRobot);
                    forceRight -= initialForceRight;
                }

                float forceMag = forceRight.dot(in.getCloseRightHandOri()->toEigen());

                double deltaT = (IceUtil::Time::now() - forceTimer).toSecondsDouble();

                if (forceMag > 5)
                {
                    forceIntegral += forceMag * deltaT;
                }
                else
                {
                    forceIntegral = 0;
                }
                forceTimer = IceUtil::Time::now();
                getDebugObserver()->setDebugDatafield("Regrasping_Lowering", "forceIntegral_LastRight", new Variant(forceIntegral));
                getDebugObserver()->setDebugDatafield("Regrasping_Lowering", "forceMag_LastRight", new Variant(forceMag));

                if (forceIntegral > in.getCloseRightHandThreshold() || rightController->isFinished())
                {
                    rightHandcontroller->setTargetsWithPwm(0.5, 2, 1, 1);
                    rightController->stopDMP();
                    break;
                }

            }
            else
            {
                forceTimer = IceUtil::Time::now();
                RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
                {
                    FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
                    initialForceRight = forcePtr->toRootEigen(localRobot);
                }
            }
        }
    }
    else //***************************************** Regrasping after pickup *****************************************************
    {
        if (rightCanValSet.size() < 2)
        {
            ARMARX_ERROR << "Pickup: the number of right via-points cannot be smaller than 2";
            emitFailure();
        }

        rightHandcontroller->setTargets(0, 0);

        float forceIntegral = 0;

        leftController->runDMP(leftGoals);
        rightController->runDMP(rightGoals);
        IceUtil::Time forceTimer = IceUtil::Time::now();

        /* move right arm for regrasping */
        while (!isRunningTaskStopped())
        {
            RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
            {
                FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
                forceRight = forcePtr->toRootEigen(localRobot);
                forceRight -= initialForceRight;
            }


            if (rightController->getVirtualTime() < rightCanValSet[0] * in.getTimeDuration())
            {

                float forceMag = forceRight.dot(in.getCloseRightHandOri()->toEigen());
                double deltaT = (IceUtil::Time::now() - forceTimer).toSecondsDouble();
                if (forceMag > 5)
                {
                    forceIntegral += forceMag * deltaT;
                }
                else
                {
                    forceIntegral = 0;
                }
                forceTimer = IceUtil::Time::now();
                getDebugObserver()->setDebugDatafield("Regrasping_Pickup", "forceIntegral_FirstRight", new Variant(forceIntegral));

                if (forceIntegral > in.getCloseRightHandThreshold())
                {
                    rightHandcontroller->setTargetsWithPwm(0.5, 2, 1, 1);
                    rightController->stopDMP();
                    break;
                }

            }
            else
            {
                forceTimer = IceUtil::Time::now();
                RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
                {
                    FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
                    initialForceRight = forcePtr->toRootEigen(localRobot);
                }
            }

            if (rightController->getVirtualTime() < rightCanValSet[1] * in.getTimeDuration())
            {
                rightHandcontroller->setTargetsWithPwm(0.5, 2, 1, 1);
                rightController->stopDMP();
                break;
            }
        }
        leftController->stopDMP();

        usleep(500000);
        /* Recovery Failure */
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        ARMARX_IMPORTANT << "thumb: " << localRobot->getRobotNode("RightHandThumb")->getJointValue();
        ARMARX_INFO << "thumb right: " << thumbDFRight->getDataField()->getFloat();
        bool speechOutputAfterWait = true;
        float thumbThresholdRight = in.getRightThumbThreshold();
        while (thumbDFRight->getDataField()->getFloat() > thumbThresholdRight)
        {
            if (speechOutputAfterWait)
            {
                getTextToSpeech()->reportText(in.getTextForFailureRecovery());
                speechOutputAfterWait = false;
            }
            rightController->deactivateController();
            while (!isRunningTaskStopped() && rightController->isControllerActive())
            {
                usleep(1000);
            }

            Eigen::Matrix4f targetRecover = localRobot->getRobotNodeSet("RightArm")->getTCP()->getPoseInRootFrame();
            targetRecover(1, 3) += 20;

            std::vector<double> targetRecoverVec = Helpers::pose2dvec(targetRecover);

            rightRecoverController->setGoals(targetRecoverVec);
            Eigen::Matrix4f viaRecover = targetRecover;
            viaRecover(1, 3) = viaRecover(1, 3) - 150;
            viaRecover(2, 3) = viaRecover(2, 3) - 200;

            rightHandcontroller->setTargets(0, 0);
            usleep(1000000);
            rightRecoverController->activateController();
            rightRecoverController->runDMPWithTime(Helpers::pose2dvec(viaRecover), in.getRecoverTimeDuration());

            forceIntegral = 0;
            forceTimer = IceUtil::Time::now();
            while (!isRunningTaskStopped() && !rightRecoverController->isFinished())
            {
                usleep(1000);
            }

            rightRecoverController->stopDMP();

            // re-calibrate thumb
            {
                rightHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
                usleep(in.getThumbCalibrationTime() * 1000);
                RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
                float thumbRightClose = thumbDFRight->getDataField()->getFloat();
                thumbThresholdRight = thumbRightClose - 0.1;
                rightHandcontroller->setTargetsWithPwm(0, 0, 1, 1);
                usleep(in.getThumbCalibrationTime() * 1000);
            }

            rightRecoverController->runDMPWithTime(Helpers::pose2dvec(targetRecover), in.getRecoverTimeDuration());

            while (!isRunningTaskStopped())
            {
                if (rightRecoverController->getVirtualTime() < 0.6 * in.getRecoverTimeDuration())
                {
                    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
                    {
                        FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
                        forceRight = forcePtr->toRootEigen(localRobot);
                        forceRight -= initialForceRight;
                    }

                    float forceMag = forceRight.dot(in.getCloseRightHandOri()->toEigen());
                    double deltaT = (IceUtil::Time::now() - forceTimer).toSecondsDouble();
                    if (forceMag > 5)
                    {
                        forceIntegral += forceMag * deltaT;
                    }
                    else
                    {
                        forceIntegral = 0;
                    }
                    forceTimer = IceUtil::Time::now();
                    getDebugObserver()->setDebugDatafield("Regrasping_Pickup", "forceIntegral_RecoveryRight", new Variant(forceIntegral));

                    if (forceIntegral > in.getCloseRightHandThreshold() || rightRecoverController->isFinished())
                    {
                        rightHandcontroller->setTargetsWithPwm(0.5, 2, 1, 1);
                        usleep(1000000);
                        rightRecoverController->stopDMP();
                        break;
                    }

                }
                else
                {
                    forceTimer = IceUtil::Time::now();
                    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
                    {
                        FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
                        initialForceRight = forcePtr->toRootEigen(localRobot);
                    }
                }

            }

            rightRecoverController->deactivateController();
            while (rightRecoverController->isControllerActive())
            {
                usleep(1000);
            }
            RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

        }
        /************************************************************/

        rightController->activateController();
        rightController->resumeDMP();

        forceIntegral = 0;
        leftHandcontroller->setTargets(0, 0);

        leftController->resumeDMP();
        forceTimer = IceUtil::Time::now();

        while (!isRunningTaskStopped())
        {
            if (leftController->getVirtualTime() < leftCanValSet[leftCanValSet.size() - 1] * in.getTimeDuration())
            {
                RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
                {
                    FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
                    forceLeft = forcePtr->toRootEigen(localRobot);
                    forceLeft -= initialForceLeft;
                }

                float forceMag = forceLeft.dot(in.getCloseLeftHandOri()->toEigen());
                double deltaT = (IceUtil::Time::now() - forceTimer).toSecondsDouble();

                if (forceMag > 5)
                {
                    forceIntegral += forceMag * deltaT;
                }
                else
                {
                    forceIntegral = 0;
                }
                forceTimer = IceUtil::Time::now();
                getDebugObserver()->setDebugDatafield("Regrasping_Pickup", "forceIntegral_LastLeft", new Variant(forceIntegral));

                if (forceIntegral > in.getCloseLeftHandThreshold() || leftController->isFinished())
                {
                    leftHandcontroller->setTargetsWithPwm(0.5, 2, 1, 1);
                    leftController->stopDMP();
                    break;
                }

            }
            else
            {
                forceTimer = IceUtil::Time::now();
                RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
                {
                    FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
                    initialForceLeft = forcePtr->toRootEigen(localRobot);
                }
            }
        }
    }

    std::vector<std::string> controllerNames;
    controllerNames.push_back(in.getLeftControllerName());
    controllerNames.push_back(in.getLeftControllerName() + "_recover");
    controllerNames.push_back(in.getRightControllerName());
    controllerNames.push_back(in.getRightControllerName() + "_recover");

    ARMARX_INFO << "controllerNames: " << controllerNames;
    getRobotUnit()->deactivateNJointControllers(controllerNames);
    usleep(200000); // 0.2s
    getRobotUnit()->deleteNJointControllers(controllerNames);
    usleep(100000);

    emitSuccess();
}

void Regrasping::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void Regrasping::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)


}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr Regrasping::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new Regrasping(stateData));
}

