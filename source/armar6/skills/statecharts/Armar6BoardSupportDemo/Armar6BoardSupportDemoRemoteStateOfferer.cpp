/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo::Armar6BoardSupportDemoRemoteStateOfferer
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6BoardSupportDemoRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
Armar6BoardSupportDemoRemoteStateOfferer::SubClassRegistry Armar6BoardSupportDemoRemoteStateOfferer::Registry(Armar6BoardSupportDemoRemoteStateOfferer::GetName(), &Armar6BoardSupportDemoRemoteStateOfferer::CreateInstance);



Armar6BoardSupportDemoRemoteStateOfferer::Armar6BoardSupportDemoRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6BoardSupportDemoStatechartContext > (reader)
{
}

void Armar6BoardSupportDemoRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6BoardSupportDemoRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6BoardSupportDemoRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6BoardSupportDemoRemoteStateOfferer::GetName()
{
    return "Armar6BoardSupportDemoRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6BoardSupportDemoRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6BoardSupportDemoRemoteStateOfferer(reader));
}
