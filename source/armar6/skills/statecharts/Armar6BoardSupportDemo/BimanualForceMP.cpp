/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BimanualForceMP.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <armarx/control/deprecated_njoint_mp_controller/bimanual/ForceMPControllerInterface.h>
#include "Helpers.h"

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
BimanualForceMP::SubClassRegistry BimanualForceMP::Registry(BimanualForceMP::GetName(), &BimanualForceMP::CreateInstance);



void BimanualForceMP::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void BimanualForceMP::run()
{
    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");
    NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController("BimanualForceMPController");
    if (controller)
    {
        if (controller->isControllerActive())
        {
            controller->deactivateController();
            while (controller->isControllerActive())
            {
                TimeUtil::SleepMS(10);
            }
        }
        controller->deleteController();
        TimeUtil::SleepMS(10);
    }

    DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT"));
    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT"));

    double timer = 0;
    while (timer < in.getOffsetRecordTimeOut())
    {
        timer += 1;
    }

    FramedDirectionPtr leftForcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
    FramedDirectionPtr rightForcePtr = forceDfRight->getDataField()->get<FramedDirection>();

    VirtualRobot::RobotPtr localRobot = getLocalRobot();
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
    Eigen::Vector3f forceLeftVec = leftForcePtr->toEigen();
    Eigen::Vector3f forceRightVec = rightForcePtr->toEigen();
    std::vector<float> leftForceOffset;
    std::vector<float> rightForceOffset;
    for (size_t i = 0; i < 3; ++i)
    {
        leftForceOffset.push_back(forceLeftVec(i));
        rightForceOffset.push_back(forceRightVec(i));
    }

    int kernelSize = 100;
    std::string dmpMode = "Linear";
    std::string dmpType = "Discrete";
    double phaseL = 1000;
    double phaseK = 1000;
    double phaseDist0 = 100;
    double phaseDist1 = 50;
    double phaseKpPos = 1;
    double phaseKpOri = 1;
    double timeDuration = 1;
    double posToOriRatio = 10;
    double dmpAmplitude = 1;

    std::string debugName = "BiForceMP_";

    double maxLinearVel = in.getMaxLinearVel();
    double maxAngularVel = in.getMaxAngularVel();
    float Kp_LinearVel = in.getKpLinearVel();
    float Kd_LinearVel = in.getKdLinearVel();
    float Kp_AngularVel = in.getKpAngularVel();
    float Kd_AngularVel = in.getKdAngularVel();
    float forceP = in.getForceP();
    float forceI = in.getForceI();
    float forceD = 0;
    float filterCoeff = in.getFilterCoeff();
    float KpJointLimitAvoidanceScale = in.getKpJointLimitAvoidance();
    float targetSupportForce = in.getTargetSupportForce();


    NJointBimanualForceMPControllerConfigPtr config = new NJointBimanualForceMPControllerConfig(
        kernelSize,
        dmpMode,
        dmpType,
        dmpAmplitude,
        phaseL,
        phaseK,
        phaseDist0,
        phaseDist1,
        phaseKpPos,
        phaseKpOri,
        timeDuration,
        posToOriRatio,
        maxLinearVel,
        maxAngularVel,
        Kp_LinearVel,
        Kd_LinearVel,
        Kp_AngularVel,
        Kd_AngularVel,
        forceP,
        forceI,
        forceD,
        filterCoeff,
        KpJointLimitAvoidanceScale,
        targetSupportForce,
        leftForceOffset,
        rightForceOffset,
        debugName
    );

    NJointBimanualForceMPControllerInterfacePrx dmpController
        = NJointBimanualForceMPControllerInterfacePrx::checkedCast(
              getRobotUnit()->createNJointController("NJointBimanualForceMPController", "NJointBimanualForceMPController", config));

    std::vector<std::string> fileNames = in.getLeftTrajFileNames();
    dmpController->learnDMPFromFiles("Left", fileNames);
    fileNames = in.getRightTrajFileNames();
    dmpController->learnDMPFromFiles("Right", fileNames);

    std::vector<double> leftGoals = Helpers::pose2dvec(in.getLeftGoals()->toEigen());
    std::vector<double> rightGoals = Helpers::pose2dvec(in.getRightGoals()->toEigen());

    if (in.isLeftViaPoseSet())
    {
        std::map<std::string, armarx::PosePtr> viaPoseMap = in.getLeftViaPose();

        for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
             it != viaPoseMap.end(); ++it)
        {
            double canVal = std::stod(it->first);
            dmpController->setViaPoints("Left", canVal, Helpers::pose2dvec(it->second->toEigen()));

        }
    }

    if (in.isRightViaPoseSet())
    {
        std::map<std::string, armarx::PosePtr> viaPoseMap = in.getRightViaPose();

        for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
             it != viaPoseMap.end(); ++it)
        {
            double canVal = std::stod(it->first);
            dmpController->setViaPoints("Right", canVal, Helpers::pose2dvec(it->second->toEigen()));
        }
    }




    dmpController->activateController();
    dmpController->runDMP(leftGoals, rightGoals);

    IceUtil::Time start = TimeUtil::GetTime();
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        ARMARX_INFO << "canVal: " << dmpController->getCanVal();
        IceUtil::Time now = TimeUtil::GetTime();
        double t = (now - start).toSecondsDouble();

        if (dmpController->isFinished())
        {
            break;
        }
    }

    dmpController->deactivateController();
    while (dmpController->isControllerActive())
    {
        usleep(10000);
    }
    dmpController->deleteController();

    emitSuccess();
}

//void BimanualForceMP::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void BimanualForceMP::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr BimanualForceMP::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new BimanualForceMP(stateData));
}
