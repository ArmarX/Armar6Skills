/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     JeffGao ( jianfenggaobit at gmail dot com )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GuardCarrying.h"

#include <SimoxUtility/math/convert/mat4f_to_pos.h>
#include <SimoxUtility/math/convert/mat4f_to_quat.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <RobotAPI/interface/units/RobotUnit/TaskSpaceActiveImpedanceControl.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <armar6/skills/statecharts/Armar6BoardSupportDemo/Armar6BoardSupportDemoStatechartContext.generated.h>

#include <RobotAPI/libraries/core/math/MathUtils.h>
#include <RobotAPI/libraries/core/PIDController.h>

#include <VirtualRobot/VirtualRobot.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include "Helpers.h"
#include <VirtualRobot/MathTools.h>
#include <RobotAPI/libraries/core/FramedPose.h>

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
GuardCarrying::SubClassRegistry GuardCarrying::Registry(GuardCarrying::GetName(), &GuardCarrying::CreateInstance);


float signedMin(float newValue, float minAbsValue)
{
    return std::copysign(std::min<float>(fabs(newValue), minAbsValue), newValue);
}


void GuardCarrying::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void GuardCarrying::run()
{
    std::map<std::string, ::armarx::MatrixFloatPtr> restrictPolygonMap = in.getRestrictPolygon();
    std::map<std::string, ::armarx::Vector3Ptr> platFormTargetMap = in.getPlatformTargetPose();

    Eigen::MatrixXf polygon;
    Eigen::Vector3f platformTargetPose;
    if (in.getIsGuardPlacement())
    {
        polygon = restrictPolygonMap["GuardPlacement"]->toEigen();
        platformTargetPose = platFormTargetMap["GuardPlacement"]->toEigen();
    }
    else
    {
        polygon = restrictPolygonMap["GuardLifting"]->toEigen();
        platformTargetPose = platFormTargetMap["GuardLifting"]->toEigen();
    }



    std::vector<Eigen::Vector2f > points;

    for (int i = 0; i < polygon.rows(); ++i)
    {
        Eigen::Vector2f point;
        point << polygon(i, 0), polygon(i, 1);
        points.push_back(point);
    }
    VirtualRobot::MathTools::ConvexHull2DPtr convPolygon = VirtualRobot::MathTools::createConvexHull2D(points);


    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx leftHandcontroller;
    if (!getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"))
    {
        leftHandcontroller = StateBase::getContext<Armar6BoardSupportDemoStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_L_EEF_NJointKITHandV2ShapeController");
    }
    else
    {
        leftHandcontroller =  devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"));
    }
    leftHandcontroller->activateController();

    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx rightHandcontroller;
    if (!getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"))
    {
        rightHandcontroller = StateBase::getContext<Armar6BoardSupportDemoStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_R_EEF_NJointKITHandV2ShapeController");
    }
    else
    {
        rightHandcontroller = devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"));
    }
    rightHandcontroller->activateController();

    leftHandcontroller->setTargetsWithPwm(0.5, 2, 1, 1);
    rightHandcontroller->setTargetsWithPwm(0.5, 2, 1, 1);


    VirtualRobot::RobotPtr localRobot = getLocalRobot();

    Eigen::Matrix4f desiredPoseLeft;
    if (in.isDesiredPoseLeftSet())
    {
        desiredPoseLeft = in.getDesiredPoseLeft()->toEigen();
    }
    else
    {
        desiredPoseLeft = localRobot->getRobotNodeSet("LeftArm")->getTCP()->getPoseInRootFrame();
    }

    Eigen::Matrix4f desiredPoseRight;
    if (in.isDesiredPoseRightSet())
    {
        desiredPoseRight = in.getDesiredPoseRight()->toEigen();
    }
    else
    {
        desiredPoseRight = localRobot->getRobotNodeSet("RightArm")->getTCP()->getPoseInRootFrame();
    }

    const Eigen::Vector3f targetRight = desiredPoseRight.topRightCorner<3, 1>();
    const Eigen::Vector3f targetLeft = desiredPoseLeft.topRightCorner<3, 1>();

    const std::vector<float> Kpos = in.getKpos();
    const std::vector<float> Kori = in.getKori();
    const std::vector<float> Dpos = in.getDpos();
    const std::vector<float> Dori = in.getDori();

    const std::string nodeSetNameLeft = "LeftArm";
    const std::string nodeSetNameRight = "RightArm";
    VirtualRobot::RobotNodePtr tcpRight = localRobot->getRobotNodeSet(nodeSetNameRight)->getTCP();
    VirtualRobot::RobotNodePtr tcpLeft = localRobot->getRobotNodeSet(nodeSetNameLeft)->getTCP();

    std::vector<float> desiredJointPositionLeft;
    if (in.isDesiredLeftJointValuesSet())
    {
        desiredJointPositionLeft = in.getDesiredLeftJointValues();
    }
    else
    {
        desiredJointPositionLeft = localRobot->getRobotNodeSet(nodeSetNameLeft)->getJointValues();

    }

    std::vector<float> desiredJointPositionRight;
    if (in.isDesiredRightJointValuesSet())
    {
        desiredJointPositionRight = in.getDesiredRightJointValues();
    }
    else
    {
        desiredJointPositionRight = localRobot->getRobotNodeSet(nodeSetNameRight)->getJointValues();

    }

    float knullval = in.getKnull();
    float dnullval = in.getDnull();

    std::vector<float> knull;
    std::vector<float> dnull;

    for (size_t i = 0; i < 8; ++i)
    {
        knull.push_back(knullval);
        dnull.push_back(dnullval);
    }

    NJointTaskSpaceImpedanceControlConfigPtr leftConfig = new NJointTaskSpaceImpedanceControlConfig;
    leftConfig->nodeSetName = nodeSetNameLeft;
    leftConfig->desiredPosition = simox::math::mat4f_to_pos(desiredPoseLeft);
    leftConfig->desiredOrientation = simox::math::mat4f_to_quat(desiredPoseLeft);
    leftConfig->Kpos = Eigen::Map<const Eigen::Vector3f>(Kpos.data(), Kpos.size());
    leftConfig->Kori = Eigen::Map<const Eigen::Vector3f>(Kori.data(), Kori.size());
    leftConfig->Dpos = Eigen::Map<const Eigen::Vector3f>(Dpos.data(), Dpos.size());
    leftConfig->Dori = Eigen::Map<const Eigen::Vector3f>(Dori.data(), Dori.size());
    leftConfig->desiredJointPositions = Eigen::Map<const Eigen::VectorXf>(desiredJointPositionLeft.data(), desiredJointPositionLeft.size());
    leftConfig->Knull = Eigen::Map<const Eigen::VectorXf>(knull.data(), knull.size());
    leftConfig->Dnull = Eigen::Map<const Eigen::VectorXf>(dnull.data(), dnull.size());
    leftConfig->torqueLimit = in.getTorqueLimit();

    NJointTaskSpaceImpedanceControlConfigPtr rightConfig = new NJointTaskSpaceImpedanceControlConfig;
    rightConfig->nodeSetName = nodeSetNameRight;
    rightConfig->desiredPosition = simox::math::mat4f_to_pos(desiredPoseRight);
    rightConfig->desiredOrientation = simox::math::mat4f_to_quat(desiredPoseRight);
    rightConfig->Kpos = Eigen::Map<const Eigen::Vector3f>(Kpos.data(), Kpos.size());
    rightConfig->Kori = Eigen::Map<const Eigen::Vector3f>(Kori.data(), Kori.size());
    rightConfig->Dpos = Eigen::Map<const Eigen::Vector3f>(Dpos.data(), Dpos.size());
    rightConfig->Dori = Eigen::Map<const Eigen::Vector3f>(Dori.data(), Dori.size());
    rightConfig->desiredJointPositions = Eigen::Map<const Eigen::VectorXf>(desiredJointPositionRight.data(), desiredJointPositionRight.size());
    rightConfig->Knull = Eigen::Map<const Eigen::VectorXf>(knull.data(), knull.size());
    rightConfig->Dnull = Eigen::Map<const Eigen::VectorXf>(dnull.data(), dnull.size());
    rightConfig->torqueLimit = in.getTorqueLimit();

    NJointTaskSpaceImpedanceControlInterfacePrx leftController
        = NJointTaskSpaceImpedanceControlInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceController", "LeftController", leftConfig));
    NJointTaskSpaceImpedanceControlInterfacePrx rightController
        = NJointTaskSpaceImpedanceControlInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceController", "RightController", rightConfig));


    leftController->activateController();
    rightController->activateController();

    float Kplatform = in.getKplatform();
    float Dplatform = in.getDplatform();

    float maxVelocity = in.getMaxVelocity();



    float KplatformAngle = in.getKplatformAngle();
    float DplatformAngle = in.getDplatformAngle();
    float maxAngularVelocity = in.getMaxAngularVelocity();


    Eigen::Vector3f filteredLinearVel;
    filteredLinearVel.setZero();
    float filteredAngularVel = 0;

    //        currentTCPLinearVelocity_filtered = (1 - filterFactor) * currentTCPLinearVelocity_filtered + filterFactor * currentTCPLinearVelocity;

    IceUtil::Time last = TimeUtil::GetTime();
    float filterTimeConstant = 0.01; //0.05;

    MultiDimPIDController pidTrans(in.getRestrictTranslationPID()->x, in.getRestrictTranslationPID()->y, in.getRestrictTranslationPID()->z);
    PIDController pidRot(in.getRestrictRotationPID()->x, in.getRestrictRotationPID()->y, in.getRestrictRotationPID()->z);

    DatafieldRefPtr platformVelX = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformVelocity", "velocityX"));
    DatafieldRefPtr platformVelY = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformVelocity", "velocityY"));
    DatafieldRefPtr platformRotVel = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformVelocity", "velocityRotation"));

    DatafieldRefPtr platformPosX = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformPose", "positionX"));
    DatafieldRefPtr platformPosY = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformPose", "positionY"));
    DatafieldRefPtr platformRotAng = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformPose", "rotation"));

    DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT"));
    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT"));
    Eigen::Vector3f initialForceLeft;
    initialForceLeft.setZero();
    Eigen::Vector3f initialForceRight;
    initialForceRight.setZero();

    DatafieldRefPtr thumbDFLeft =  DatafieldRefPtr::dynamicCast(getKinematicUnitObserver()->getDatafieldRefByName("jointangles", "LeftHandThumb"));
    DatafieldRefPtr thumbDFRight =  DatafieldRefPtr::dynamicCast(getKinematicUnitObserver()->getDatafieldRefByName("jointangles", "RightHandThumb"));

    IceUtil::Time start = TimeUtil::GetTime();


    Eigen::Vector3f initialPositionRight;
    Eigen::Vector3f initialPositionLeft;
    Eigen::Vector3f initialDirection;

    Eigen::Vector3f dimensions;
    dimensions << 805, 1205, 20;
    bool isFirstLoop = true;
    bool speechOutputAfterWait = true;
    float minPosiTolerance = in.getMinPosiTolerance();
    bool isImpedanceParameterReset = false;
    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

        if (in.getIsConsiderFailure())
        {
            if (thumbDFLeft->getDataField()->getFloat() > in.getLeftThumbThreshold() || thumbDFRight->getDataField()->getFloat() > in.getRightThumbThreshold())
            {
                getPlatformUnit()->move(0, 0, 0);
                emitLoseGuard();
            }
        }
        IceUtil::Time now = TimeUtil::GetTime();

        /* Debug Drawer */
        if (in.isShowGuardSet() && in.getShowGuard())
        {
            std::string layerName = "guard_layer";
            std::string boxName = "guard";
            //DrawColor color = {0.5, 0.5, 0.5, 1};

            //            if (in.getIsReGrasped())
            //            {
            //                // board debug drawer
            //                Eigen::Matrix4f leftPose = localRobot->getRobotNodeSet("LeftArm")->getTCP()->getPoseInRootFrame();
            //                Eigen::Matrix4f guardLocal = Eigen::Matrix4f::Identity();
            //                guardLocal(0, 3) = leftPose(0, 3);
            //                guardLocal(1, 3) = leftPose(1, 3) + 600;
            //                guardLocal(2, 3) = leftPose(2, 3);
            //                Eigen::Matrix4f globalPose = localRobot->getRootNode()->toGlobalCoordinateSystem(guardLocal);
            //                getDebugDrawerTopic()->setBoxVisu(layerName, boxName, new Pose(globalPose), new Vector3(dimensions), color);
            //            }
            //            else
            //            {
            //                Eigen::Matrix4f rightPose = localRobot->getRobotNodeSet("RightArm")->getTCP()->getPoseInRootFrame();
            //                Eigen::Matrix4f guardLocal = Eigen::Matrix4f::Identity();
            //                guardLocal(0, 3) = rightPose(0, 3) - 300;
            //                guardLocal(1, 3) = rightPose(1, 3) + 600;
            //                guardLocal(2, 3) = rightPose(2, 3);
            //                Eigen::Matrix4f globalPose = localRobot->getRootNode()->toGlobalCoordinateSystem(guardLocal);
            //                getDebugDrawerTopic()->setBoxVisu(layerName, boxName, new Pose(globalPose), new Vector3(dimensions), color);

            //            }
        }

        /* Control Platform */
        double timeDuration = (now - start).toSecondsDouble();
        if (timeDuration < in.getMinExecutionTime() || isFirstLoop)
        {
            std::vector<float> currPoseLeft = Helpers::pose2vec(localRobot->getRobotNodeSet("LeftArm")->getTCP()->getPoseInRootFrame());
            std::vector<float> currPoseRight = Helpers::pose2vec(localRobot->getRobotNodeSet("RightArm")->getTCP()->getPoseInRootFrame());

            initialPositionRight << currPoseRight.at(0), currPoseRight.at(1), currPoseRight.at(2);
            initialPositionLeft << currPoseLeft.at(0), currPoseLeft.at(1), currPoseLeft.at(2);
            initialDirection = initialPositionRight - initialPositionLeft;
            initialDirection = initialDirection / initialDirection.norm();

            {
                FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
                initialForceLeft = forcePtr->toRootEigen(localRobot);
            }
            {
                FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
                initialForceRight = forcePtr->toRootEigen(localRobot);
            }

            isFirstLoop = false;

            continue;
        }
        if (speechOutputAfterWait)
        {
            getTextToSpeech()->reportText(in.getTextAfterWaitForZeroTorque());
            speechOutputAfterWait = false;

            const Eigen::Vector3f desiredPositionRightVec = desiredPoseRight.topRightCorner<3, 1>();
            const Eigen::Vector3f desiredPositionLeftVec = desiredPoseLeft.topRightCorner<3, 1>();
            const Eigen::Vector3f positionRightError = initialPositionRight - desiredPositionRightVec;
            const Eigen::Vector3f positionLeftError = initialPositionLeft - desiredPositionLeftVec;
            ARMARX_INFO << "differece Pose: " << positionLeftError.norm() << ", " << positionRightError.norm();
        }


        Eigen::Vector3f currentPosition;
        currentPosition << platformPosX->getDataField()->getFloat(), platformPosY->getDataField()->getFloat(), platformRotAng->getDataField()->getFloat();

        bool platformInPolygon = VirtualRobot::MathTools::isInside(currentPosition.head(2), convPolygon);
        platformInPolygon = platformInPolygon && fabs(platformTargetPose(2) - currentPosition(2)) < in.getRestrictAngle() * M_PI / 180.0;

        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        Eigen::Vector3f currentPositionRight = tcpRight->getPositionInRootFrame();
        Eigen::Vector3f currentPositionLeft = tcpLeft->getPositionInRootFrame();

        bool automaticDriveCondition = (targetLeft - currentPositionLeft).norm() < in.getHandOutofRangeThreshold() || (targetRight - currentPositionRight).norm() < in.getHandOutofRangeThreshold();
        if (platformInPolygon && automaticDriveCondition)
        {
            if (!isImpedanceParameterReset)
            {
                getTextToSpeech()->reportText(in.getHandOutofRangeSpeechOutput());
                leftController->setImpedanceParameters("Kpos", in.getRestrictKpos());
                leftController->setImpedanceParameters("Dpos", in.getRestrictDpos());
                rightController->setImpedanceParameters("Kpos", in.getRestrictKpos());
                rightController->setImpedanceParameters("Dpos", in.getRestrictDpos());
                minPosiTolerance = in.getHandOutofRangeThreshold();
                isImpedanceParameterReset = true;
            }
            float posx = platformPosX->getDataField()->getFloat();
            float posy = platformPosY->getDataField()->getFloat();
            float ori = platformRotAng->getDataField()->getFloat();

            Eigen::Vector3f pos {posx, posy, 0.0f};

            if (ori > M_PI)
            {
                ori = - 2  * M_PI + ori;
            }

            float angleDelta = platformTargetPose(2) - ori;
            ARMARX_INFO << deactivateSpam(1) << VAROUT(angleDelta);

            // transform alpha to [-pi, pi)
            while (angleDelta < -M_PI)
            {
                angleDelta += 2 * M_PI;
            }

            while (angleDelta >= M_PI)
            {
                angleDelta -= 2 * M_PI;
            }

            Eigen::Vector3f target {platformTargetPose(0), platformTargetPose(1), 0.0f};
            pidTrans.update(pos, target);
            pidRot.update(angleDelta, 0);

            Eigen::Vector2f newVel(pidTrans.getControlValue()[0], pidTrans.getControlValue()[1]);


            float newVelocityRot = -signedMin(pidRot.getControlValue(), in.getRestrictMaxAngularVelocity());
            Eigen::Vector3f globalVel {newVel[0], newVel[1], 0};
            Eigen::Matrix3f m;
            m = Eigen::AngleAxisf(ori, Eigen::Vector3f::UnitZ());
            Eigen::Vector3f localVel = m.inverse() * globalVel;
            if (localVel.norm() > in.getRestrictMaxVelocity())
            {
                localVel *= in.getRestrictMaxVelocity() / localVel.norm();
            }

            if (std::isnan(localVel[0]) || std::isnan(localVel[1]) || std::isnan(newVelocityRot))
            {
                throw LocalException("A target velocity is NaN!");
            }



            getPlatformUnit()->move(localVel[0], localVel[1], newVelocityRot);


            if (fabs(pidTrans.previousError) < in.getPositionalAccuracy() && fabs(pidRot.previousError) < in.getOrientationalAccuracy())
            {
                break;
            }
            else
            {
                ARMARX_INFO << deactivateSpam(1) << "error: " << pidTrans.previousError << " x velocity: " << newVel[0] << " y velocity: " << newVel[1];
                ARMARX_INFO << deactivateSpam(1) << "rot error: " << pidRot.previousError << " rot velocity: " << newVelocityRot;
            }

            usleep(100000);
        }
        else
        {
            if (!platformInPolygon && isImpedanceParameterReset)
            {
                leftController->setImpedanceParameters("Kpos", in.getKpos());
                leftController->setImpedanceParameters("Dpos", in.getDpos());
                rightController->setImpedanceParameters("Kpos", in.getKpos());
                rightController->setImpedanceParameters("Dpos", in.getDpos());
                minPosiTolerance = in.getMinPosiTolerance();
                isImpedanceParameterReset = false;
            }

            Eigen::Vector3f positionDeltaRight = currentPositionRight - initialPositionRight;
            Eigen::Vector3f positionDeltaLeft = currentPositionLeft - initialPositionLeft;
            Eigen::Vector3f positionDelta = (positionDeltaLeft + positionDeltaRight) / 2;
            Eigen::Vector3f currentVel;
            currentVel << platformVelX->getDataField()->getFloat(), platformVelY->getDataField()->getFloat(), 0;
            Eigen::Vector3f velocity;

            if (positionDelta.norm() < minPosiTolerance)
            {
                velocity.setZero();
            }
            else
            {
                positionDelta = positionDelta - minPosiTolerance * positionDelta / positionDelta.norm();
                velocity = positionDelta * Kplatform - currentVel * Dplatform;

            }


            double deltaT = (now - last).toSecondsDouble();
            last = now;

            if (deltaT == 0)
            {
                continue;
            }
            ARMARX_INFO << "deltaT: " << deltaT;
            float Tstar = 1 / ((filterTimeConstant / deltaT) + 1);
            filteredLinearVel = Tstar * (velocity - filteredLinearVel) + filteredLinearVel;

            if (!std::isfinite(filteredLinearVel(0)))
            {
                filteredLinearVel(0) = 0;
            }
            if (!std::isfinite(filteredLinearVel(1)))
            {
                filteredLinearVel(1) = 0;
            }

            velocity = math::MathUtils::LimitTo(filteredLinearVel, maxVelocity);

            float angularVelocity ;

            Eigen::Vector3f currentDirection = currentPositionRight - currentPositionLeft;
            currentDirection = currentDirection / currentDirection.norm();

            float cosAlpha = currentDirection.dot(initialDirection);

            float alpha = 0;

            Eigen::Vector3f directionDiff = currentDirection - initialDirection;
            if (directionDiff(1) < 0)
            {
                alpha = -acos(cosAlpha);
            }
            else
            {
                alpha = acos(cosAlpha);
            }


            float minAngleTolerance = in.getMinAngleTolerance();
            if (fabs(alpha) < minAngleTolerance)
            {
                angularVelocity = 0;
            }
            else
            {
                if (alpha > 0)
                {
                    alpha -= minAngleTolerance;
                }
                else
                {
                    alpha += minAngleTolerance;
                }
                angularVelocity =  KplatformAngle * alpha - DplatformAngle * platformRotVel;
            }

            filteredAngularVel = Tstar * (angularVelocity - filteredAngularVel) + filteredAngularVel;
            if (!std::isfinite(filteredAngularVel))
            {
                filteredAngularVel = 0;
            }

            angularVelocity = math::MathUtils::LimitTo(filteredAngularVel, maxAngularVelocity);

            getDebugObserver()->setDebugDatafield("FollowMeTorque", "vx", new Variant(velocity(0)));
            getDebugObserver()->setDebugDatafield("FollowMeTorque", "vy", new Variant(velocity(1)));
            getDebugObserver()->setDebugDatafield("FollowMeTorque", "vangle", new Variant(angularVelocity));
            getPlatformUnit()->move(velocity(0), velocity(1), angularVelocity);
        }

        //        Eigen::Vector3f forceLeft;
        //        {
        //            FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
        //            forceLeft = forcePtr->toRootEigen(localRobot);
        //            forceLeft -= initialForceLeft;
        //        }

        //        Eigen::Vector3f forceRight;
        //        {
        //            FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
        //            forceRight = forcePtr->toRootEigen(localRobot);
        //            forceRight -= initialForceRight;
        //        }

        //        getDebugObserver()->setDebugDatafield("GuardCarrying", "forceleft_x", new Variant(forceLeft(0)));
        //        getDebugObserver()->setDebugDatafield("GuardCarrying", "forceleft_y", new Variant(forceLeft(1)));
        //        getDebugObserver()->setDebugDatafield("GuardCarrying", "forceleft_z", new Variant(forceLeft(2)));
        //        getDebugObserver()->setDebugDatafield("GuardCarrying", "forceleft_x", new Variant(forceRight(0)));
        //        getDebugObserver()->setDebugDatafield("GuardCarrying", "forceleft_y", new Variant(forceRight(1)));
        //        getDebugObserver()->setDebugDatafield("GuardCarrying", "forceleft_z", new Variant(forceRight(2)));


        usleep(1000);
    }



    //    ARMARX_IMPORTANT << "In Restrict Polygon";


    //    while (!isRunningTaskStopped())
    //    {
    //        // do your calculations
    //        float posx = platformPosX->getDataField()->getFloat();
    //        float posy = platformPosY->getDataField()->getFloat();
    //        float ori = platformRotAng->getDataField()->getFloat();

    //        Eigen::Vector3f pos {posx, posy, 0.0f};

    //        if (ori > M_PI)
    //        {
    //            ori = - 2  * M_PI + ori;
    //        }

    //        float angleDelta = platformTargetPose(2) - ori;
    //        ARMARX_INFO << deactivateSpam(1) << VAROUT(angleDelta);

    //        // transform alpha to [-pi, pi)
    //        while (angleDelta < -M_PI)
    //        {
    //            angleDelta += 2 * M_PI;
    //        }

    //        while (angleDelta >= M_PI)
    //        {
    //            angleDelta -= 2 * M_PI;
    //        }

    //        Eigen::Vector3f target {platformTargetPose(0), platformTargetPose(1), 0.0f};
    //        pidTrans.update(pos, target);
    //        pidRot.update(angleDelta, 0);

    //        Eigen::Vector2f newVel(pidTrans.getControlValue()[0], pidTrans.getControlValue()[1]);


    //        float newVelocityRot = -signedMin(pidRot.getControlValue(), in.getRestrictMaxAngularVelocity());
    //        Eigen::Vector3f globalVel {newVel[0], newVel[1], 0};
    //        Eigen::Matrix3f m;
    //        m = Eigen::AngleAxisf(ori, Eigen::Vector3f::UnitZ());
    //        Eigen::Vector3f localVel = m.inverse() * globalVel;
    //        if (localVel.norm() > in.getRestrictMaxVelocity())
    //        {
    //            localVel *= in.getRestrictMaxVelocity() / localVel.norm();
    //        }

    //        if (std::isnan(localVel[0]) || std::isnan(localVel[1]) || std::isnan(newVelocityRot))
    //        {
    //            throw LocalException("A target velocity is NaN!");
    //        }


    //        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

    //        Eigen::Vector3f currLeft = localRobot->getRobotNodeSet("LeftArm")->getTCP()->getPositionInRootFrame();
    //        Eigen::Vector3f currRight = localRobot->getRobotNodeSet("RightArm")->getTCP()->getPositionInRootFrame();

    //        if((targetLeft - currLeft).norm() < in.getHandOutofRangeThreshold() || (targetRight - currRight).norm() < in.getHandOutofRangeThreshold())
    //        {
    //            getPlatformUnit()->move(localVel[0], localVel[1], newVelocityRot);
    //        }
    //        else
    //        {
    //            getPlatformUnit()->move(0, 0, 0);
    //            getTextToSpeech()->reportText(in.getHandOutofRangeSpeechOutput());
    //        }

    //        if (fabs(pidTrans.previousError) < in.getPositionalAccuracy() && fabs(pidRot.previousError) < in.getOrientationalAccuracy())
    //        {
    //            break;
    //        }
    //        else
    //        {
    //            ARMARX_INFO << deactivateSpam(1) << "error: " << pidTrans.previousError << " x velocity: " << newVel[0] << " y velocity: " << newVel[1];
    //            ARMARX_INFO << deactivateSpam(1) << "rot error: " << pidRot.previousError << " rot velocity: " << newVelocityRot;
    //        }

    //        usleep(100000);
    //    }

    getPlatformUnit()->move(0, 0, 0);

    rightController->deactivateController();
    leftController->deactivateController();

    while (rightController->isControllerActive() || leftController->isControllerActive())
    {
        usleep(10000);
    }
    rightController->deleteController();
    leftController->deleteController();

    emitSuccess();
}

//void GuardCarrying::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GuardCarrying::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GuardCarrying::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GuardCarrying(stateData));
}
