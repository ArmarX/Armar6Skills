/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     JeffGao ( jianfenggaobit at gmail dot com )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <SimoxUtility/math/convert/mat4f_to_pos.h>
#include <SimoxUtility/math/convert/mat4f_to_quat.h>

#include "BimanualAdjustPlatform.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <RobotAPI/interface/units/RobotUnit/TaskSpaceActiveImpedanceControl.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <armar6/skills/statecharts/Armar6BoardSupportDemo/Armar6BoardSupportDemoStatechartContext.generated.h>

#include <RobotAPI/libraries/core/math/MathUtils.h>
#include <VirtualRobot/VirtualRobot.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include "Helpers.h"
using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
BimanualAdjustPlatform::SubClassRegistry BimanualAdjustPlatform::Registry(BimanualAdjustPlatform::GetName(), &BimanualAdjustPlatform::CreateInstance);



void BimanualAdjustPlatform::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void BimanualAdjustPlatform::run()
{
    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx leftHandcontroller;
    if (!getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"))
    {
        leftHandcontroller = StateBase::getContext<Armar6BoardSupportDemoStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_L_EEF_NJointKITHandV2ShapeController");
    }
    else
    {
        leftHandcontroller =  devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"));
    }
    leftHandcontroller->activateController();

    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx rightHandcontroller;
    if (!getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"))
    {
        rightHandcontroller = StateBase::getContext<Armar6BoardSupportDemoStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_R_EEF_NJointKITHandV2ShapeController");
    }
    else
    {
        rightHandcontroller = devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"));
    }
    rightHandcontroller->activateController();


    VirtualRobot::RobotPtr localRobot = getLocalRobot();

    Eigen::Matrix4f desiredPoseLeft;
    if (in.isDesiredPoseLeftSet())
    {
        desiredPoseLeft = in.getDesiredPoseLeft()->toEigen();
    }
    else
    {
        desiredPoseLeft = localRobot->getRobotNodeSet("LeftArm")->getTCP()->getPoseInRootFrame();
    }


    Eigen::Matrix4f desiredPoseRight;
    if (in.isDesiredPoseRightSet())
    {
        desiredPoseRight = in.getDesiredPoseRight()->toEigen();
    }
    else
    {
        desiredPoseRight = localRobot->getRobotNodeSet("RightArm")->getTCP()->getPoseInRootFrame();
    }

    const std::vector<float> Kpos = in.getKpos();
    const std::vector<float> Kori = in.getKori();
    const std::vector<float> Dpos = in.getDpos();
    const std::vector<float> Dori = in.getDori();

    const std::string nodeSetNameLeft = "LeftArm";
    const std::string nodeSetNameRight = "RightArm";
    VirtualRobot::RobotNodePtr tcpRight = localRobot->getRobotNodeSet(nodeSetNameRight)->getTCP();
    VirtualRobot::RobotNodePtr tcpLeft = localRobot->getRobotNodeSet(nodeSetNameLeft)->getTCP();

    std::vector<float> desiredJointPositionLeft;
    if (in.isDesiredLeftJointValuesSet())
    {
        desiredJointPositionLeft = in.getDesiredLeftJointValues();
    }
    else
    {
        desiredJointPositionLeft = localRobot->getRobotNodeSet(nodeSetNameLeft)->getJointValues();

    }

    std::vector<float> desiredJointPositionRight;
    if (in.isDesiredRightJointValuesSet())
    {
        desiredJointPositionRight = in.getDesiredRightJointValues();
    }
    else
    {
        desiredJointPositionRight = localRobot->getRobotNodeSet(nodeSetNameRight)->getJointValues();

    }

    float knullval = in.getKnull();
    float dnullval = in.getDnull();

    std::vector<float> knull;
    std::vector<float> dnull;

    for (size_t i = 0; i < 8; ++i)
    {
        knull.push_back(knullval);
        dnull.push_back(dnullval);
    }

    NJointTaskSpaceImpedanceControlConfigPtr leftConfig = new NJointTaskSpaceImpedanceControlConfig;
    leftConfig->nodeSetName = nodeSetNameLeft;
    leftConfig->desiredPosition = simox::math::mat4f_to_pos(desiredPoseLeft);
    leftConfig->desiredOrientation = simox::math::mat4f_to_quat(desiredPoseLeft);
    leftConfig->Kpos = Eigen::Map<const Eigen::Vector3f>(Kpos.data(), Kpos.size());
    leftConfig->Kori = Eigen::Map<const Eigen::Vector3f>(Kori.data(), Kori.size());
    leftConfig->Dpos = Eigen::Map<const Eigen::Vector3f>(Dpos.data(), Dpos.size());
    leftConfig->Dori = Eigen::Map<const Eigen::Vector3f>(Dori.data(), Dori.size());
    leftConfig->desiredJointPositions = Eigen::Map<const Eigen::VectorXf>(desiredJointPositionLeft.data(), desiredJointPositionLeft.size());
    leftConfig->Knull = Eigen::Map<const Eigen::VectorXf>(knull.data(), knull.size());
    leftConfig->Dnull = Eigen::Map<const Eigen::VectorXf>(dnull.data(), dnull.size());
    leftConfig->torqueLimit = in.getTorqueLimit();

    NJointTaskSpaceImpedanceControlConfigPtr rightConfig = new NJointTaskSpaceImpedanceControlConfig;
    rightConfig->nodeSetName = nodeSetNameRight;
    rightConfig->desiredPosition = simox::math::mat4f_to_pos(desiredPoseRight);
    rightConfig->desiredOrientation = simox::math::mat4f_to_quat(desiredPoseRight);
    rightConfig->Kpos = Eigen::Map<const Eigen::Vector3f>(Kpos.data(), Kpos.size());
    rightConfig->Kori = Eigen::Map<const Eigen::Vector3f>(Kori.data(), Kori.size());
    rightConfig->Dpos = Eigen::Map<const Eigen::Vector3f>(Dpos.data(), Dpos.size());
    rightConfig->Dori = Eigen::Map<const Eigen::Vector3f>(Dori.data(), Dori.size());
    rightConfig->desiredJointPositions = Eigen::Map<const Eigen::VectorXf>(desiredJointPositionRight.data(), desiredJointPositionRight.size());
    rightConfig->Knull = Eigen::Map<const Eigen::VectorXf>(knull.data(), knull.size());
    rightConfig->Dnull = Eigen::Map<const Eigen::VectorXf>(dnull.data(), dnull.size());
    rightConfig->torqueLimit = in.getTorqueLimit();

    NJointTaskSpaceImpedanceControlInterfacePrx leftController
        = NJointTaskSpaceImpedanceControlInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceController", "LeftController", leftConfig));
    NJointTaskSpaceImpedanceControlInterfacePrx rightController
        = NJointTaskSpaceImpedanceControlInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceController", "RightController", rightConfig));

    leftController->activateController();
    rightController->activateController();

    float Kplatform = in.getKplatform();
    float Dplatform = in.getDplatform();

    float maxVelocity = in.getMaxVelocity();



    float KplatformAngle = in.getKplatformAngle();
    float DplatformAngle = in.getDplatformAngle();
    float maxAngularVelocity = in.getMaxAngularVelocity();


    Eigen::Vector3f filteredLinearVel;
    filteredLinearVel.setZero();
    float filteredAngularVel = 0;

    //        currentTCPLinearVelocity_filtered = (1 - filterFactor) * currentTCPLinearVelocity_filtered + filterFactor * currentTCPLinearVelocity;

    IceUtil::Time last = TimeUtil::GetTime();
    float filterTimeConstant = in.getFilterTimeConstant();// 0.01; //0.05;


    DatafieldRefPtr platformVelX = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformVelocity", "velocityX"));
    DatafieldRefPtr platformVelY = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformVelocity", "velocityY"));
    DatafieldRefPtr platformRotVel = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformVelocity", "velocityRotation"));


    DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT"));
    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT"));
    Eigen::Vector3f initialForceLeft;
    initialForceLeft.setZero();
    Eigen::Vector3f initialForceRight;
    initialForceRight.setZero();

    IceUtil::Time start = TimeUtil::GetTime();


    Eigen::Vector3f initialPositionRight;
    Eigen::Vector3f initialPositionLeft;
    Eigen::Vector3f initialDirection;

    std::string layerName = "guard_layer";
    std::string boxName = "guard";
    Eigen::Vector3f dimensions;
    dimensions << 805, 1205, 20;
    DrawColor color = {0.5, 0.5, 0.5, 1};
    bool speechOutputAfterWait = true;

    float filteredForceZ = 0;

    float forceFilter = 0.9;
    double timeDuration = 0;
    while (timeDuration < in.getMinExecutionTime())
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

        IceUtil::Time now = TimeUtil::GetTime();
        timeDuration = (now - start).toSecondsDouble();

        std::vector<float> currPoseLeft = Helpers::pose2vec(localRobot->getRobotNodeSet("LeftArm")->getTCP()->getPoseInRootFrame());
        std::vector<float> currPoseRight = Helpers::pose2vec(localRobot->getRobotNodeSet("RightArm")->getTCP()->getPoseInRootFrame());

        initialPositionRight << currPoseRight.at(0), currPoseRight.at(1), currPoseRight.at(2);
        initialPositionLeft << currPoseLeft.at(0), currPoseLeft.at(1), currPoseLeft.at(2);
        initialDirection = initialPositionRight - initialPositionLeft;
        initialDirection = initialDirection / initialDirection.norm();

        {
            FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
            initialForceLeft = (1 - forceFilter) * initialForceLeft + forceFilter * forcePtr->toRootEigen(localRobot);
        }
        {
            FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
            initialForceRight = (1 - forceFilter) * initialForceRight + forceFilter * forcePtr->toRootEigen(localRobot);
        }

    }

    IceUtil::Time forceTriggerTimer = TimeUtil::GetTime();
    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        IceUtil::Time now = TimeUtil::GetTime();

        /* Debug Drawer */
        if (in.isShowGuardSet() && in.getShowGuard())
        {

            Eigen::Matrix4f rightPose = localRobot->getRobotNodeSet("RightArm")->getTCP()->getPoseInRootFrame();
            Eigen::Matrix4f guardLocal = Eigen::Matrix4f::Identity();
            guardLocal(0, 3) = rightPose(0, 3) - 300;
            guardLocal(1, 3) = rightPose(1, 3) + 600;
            guardLocal(2, 3) = rightPose(2, 3);
            Eigen::Matrix4f globalPose = localRobot->getRootNode()->toGlobalCoordinateSystem(guardLocal);
            getDebugDrawerTopic()->setBoxVisu(layerName, boxName, new Pose(globalPose), new Vector3(dimensions), color);

        }

        /* Control Platform */

        if (speechOutputAfterWait)
        {
            getTextToSpeech()->reportText(in.getTextAfterWaitForZeroTorque());
            speechOutputAfterWait = false;

            const Eigen::Vector3f desiredPositionRightVec = desiredPoseRight.topRightCorner<3, 1>();
            const Eigen::Vector3f desiredPositionLeftVec = desiredPoseLeft.topRightCorner<3, 1>();
            const Eigen::Vector3f positionRightError = initialPositionRight - desiredPositionRightVec;
            const Eigen::Vector3f positionLeftError = initialPositionLeft - desiredPositionLeftVec;
            ARMARX_INFO << "differece Pose: " << positionLeftError.norm() << ", " << positionRightError.norm();
        }

        Eigen::Vector3f currentPositionRight = tcpRight->getPositionInRootFrame();
        Eigen::Vector3f positionDeltaRight = currentPositionRight - initialPositionRight;

        Eigen::Vector3f currentPositionLeft = tcpLeft->getPositionInRootFrame();
        Eigen::Vector3f positionDeltaLeft = currentPositionLeft - initialPositionLeft;
        Eigen::Vector3f positionDelta = (positionDeltaLeft + positionDeltaRight) / 2;

        float minPosiTolerance = in.getMinPosiTolerance();
        Eigen::Vector3f currentVel;
        currentVel << platformVelX->getDataField()->getFloat(), platformVelY->getDataField()->getFloat(), 0;
        Eigen::Vector3f velocity;

        if (positionDelta.norm() < minPosiTolerance)
        {
            velocity.setZero();
        }
        else
        {
            positionDelta = positionDelta - minPosiTolerance * positionDelta / positionDelta.norm();
            velocity = positionDelta * Kplatform - currentVel * Dplatform;

        }


        double deltaT = (now - last).toSecondsDouble();
        last = now;

        if (deltaT == 0)
        {
            continue;
        }
        ARMARX_INFO << "deltaT: " << deltaT;
        float Tstar = 1 / ((filterTimeConstant / deltaT) + 1);
        filteredLinearVel = Tstar * (velocity - filteredLinearVel) + filteredLinearVel;

        if (!std::isfinite(filteredLinearVel(0)))
        {
            filteredLinearVel(0) = 0;
        }
        if (!std::isfinite(filteredLinearVel(1)))
        {
            filteredLinearVel(1) = 0;
        }

        velocity = math::MathUtils::LimitTo(filteredLinearVel, maxVelocity);

        float angularVelocity ;

        Eigen::Vector3f currentDirection = currentPositionRight - currentPositionLeft;
        currentDirection = currentDirection / currentDirection.norm();

        float cosAlpha = currentDirection.dot(initialDirection);

        float alpha = 0;

        Eigen::Vector3f directionDiff = currentDirection - initialDirection;
        if (directionDiff(1) < 0)
        {
            alpha = -acos(cosAlpha);
        }
        else
        {
            alpha = acos(cosAlpha);
        }


        float minAngleTolerance = in.getMinAngleTolerance();
        if (fabs(alpha) < minAngleTolerance)
        {
            angularVelocity = 0;
        }
        else
        {
            if (alpha > 0)
            {
                alpha -= minAngleTolerance;
            }
            else
            {
                alpha += minAngleTolerance;
            }
            angularVelocity =  KplatformAngle * alpha - DplatformAngle * platformRotVel;
        }

        filteredAngularVel = Tstar * (angularVelocity - filteredAngularVel) + filteredAngularVel;
        if (!std::isfinite(filteredAngularVel))
        {
            filteredAngularVel = 0;
        }

        angularVelocity = math::MathUtils::LimitTo(filteredAngularVel, maxAngularVelocity);

        getDebugObserver()->setDebugDatafield("FollowMeTorque", "vx", new Variant(velocity(0)));
        getDebugObserver()->setDebugDatafield("FollowMeTorque", "vy", new Variant(velocity(1)));
        getDebugObserver()->setDebugDatafield("FollowMeTorque", "vangle", new Variant(angularVelocity));
        getPlatformUnit()->move(velocity(0), velocity(1), angularVelocity);



        Eigen::Vector3f forceLeft;
        {
            FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
            forceLeft = forcePtr->toRootEigen(localRobot);
            forceLeft -= initialForceLeft;
        }

        Eigen::Vector3f forceRight;
        {
            FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
            forceRight = forcePtr->toRootEigen(localRobot);
            forceRight -= initialForceRight;
        }


        float fz = forceLeft(2) + forceRight(2);
        double dTtrigger = (TimeUtil::GetTime() - forceTriggerTimer).toSecondsDouble();
        if (fz < -5)
        {
            filteredForceZ = filteredForceZ + fz * dTtrigger;
        }
        else
        {
            filteredForceZ = 0;
        }

        if (fabs(filteredForceZ) > in.getPlacementThreshold())
        {
            break;
        }
        forceTriggerTimer =  TimeUtil::GetTime();


        usleep(1000);
        getDebugObserver()->setDebugDatafield("BimanualGuardPlacement", "forceleft_z", new Variant(forceLeft(2)));
        getDebugObserver()->setDebugDatafield("BimanualGuardPlacement", "forceright_z", new Variant(forceRight(2)));
        getDebugObserver()->setDebugDatafield("BimanualGuardPlacement", "filteredForceZ", new Variant(filteredForceZ));

    }

    getPlatformUnit()->move(0, 0, 0);

    rightController->deactivateController();
    leftController->deactivateController();

    while (rightController->isControllerActive() || leftController->isControllerActive())
    {
        usleep(10000);
    }
    rightController->deleteController();
    leftController->deleteController();
    emitSuccess();

}

//void BimanualAdjustPlatform::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void BimanualAdjustPlatform::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr BimanualAdjustPlatform::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new BimanualAdjustPlatform(stateData));
}
