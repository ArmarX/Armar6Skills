/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GoDownForceDetection.h"

#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/core/time/TimeUtil.h>

#include <ArmarXCore/observers/filters/MedianFilter.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <RobotAPI/libraries/core/FramedPose.h>

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
GoDownForceDetection::SubClassRegistry GoDownForceDetection::Registry(GoDownForceDetection::GetName(), &GoDownForceDetection::CreateInstance);



void GoDownForceDetection::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void GoDownForceDetection::run()
{

    getTextToSpeech()->reportText("I am ready.");

    DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT"));
    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT"));

    Eigen::Vector3f initialForceLeft;


    Eigen::Vector3f initialForceRight;

    IceUtil::Time start = TimeUtil::GetTime();

    VirtualRobot::RobotPtr localRobot = getLocalRobot();

    bool firstLoop = true;
    IceUtil::Time forceTimerStart;

    CycleUtil c(10);
    filters::MedianFilter mf(400);
    filters::MedianFilter fmf(100);
    bool isGuardOnHand = false;

    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

        IceUtil::Time now = TimeUtil::GetTime();
        float t = (now - start).toSecondsDouble();

        if (t < in.getMinExecutionTime() || firstLoop)
        {
            {
                FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
                initialForceRight = forcePtr->toRootEigen(localRobot);
            }


            {
                FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
                initialForceLeft = forcePtr->toRootEigen(localRobot);
            }

            firstLoop = false;
            forceTimerStart = TimeUtil::GetTime();
            continue;
        }


        Eigen::Vector3f forceLeft;
        {
            FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
            forceLeft = forcePtr->toRootEigen(localRobot);
            forceLeft -= initialForceLeft;
        }

        Eigen::Vector3f forceRight;
        {
            FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
            forceRight = forcePtr->toRootEigen(localRobot);
            forceRight -= initialForceRight;
        }


        if (!isGuardOnHand)
        {
            double forceYMag = forceRight(1) + forceLeft(1);
            fmf.update(0, new Variant(forceYMag));
            double forceY = fmf.getValue()->getDouble();
            getDebugObserver()->setDebugDatafield("GetForce", "forceDiff", new Variant(forceYMag - forceY));
            getDebugObserver()->setDebugDatafield("GetForce", "forceY", new Variant(forceY));
            getDebugObserver()->setDebugDatafield("GetForce", "forceYMag", new Variant(forceYMag));

            IceUtil::Time forceNow = TimeUtil::GetTime();
            double forceTimer = (forceNow - forceTimerStart).toSecondsDouble();
            ARMARX_INFO << deactivateSpam(0.3) << VAROUT(forceTimer);

            if (forceYMag - forceY > in.getForceXThreshold() || forceTimer > in.getFrictionWaitTimeOut())
            {
                isGuardOnHand = true;
            }
            ARMARX_INFO << deactivateSpam(0.3) << VAROUT(forceY) << VAROUT(forceYMag);

            continue;
        }

        double  forceMag = forceRight(2) + forceLeft(2);
        mf.update(0, new Variant(forceMag));
        double force = mf.getValue()->getDouble();
        getDebugObserver()->setDebugDatafield("GoDownForceDetection", "force", new Variant(force));
        ARMARX_INFO << deactivateSpam(0.3) << VAROUT(force) << VAROUT(forceMag) << " threshold: " <<  in.getForceGoDownThreshold();
        if (force < in.getForceGoDownThreshold())
        {
            ARMARX_INFO << "Detected force!";
            ARMARX_INFO << deactivateSpam(0.3) << VAROUT(force) << VAROUT(forceMag) << " threshold: " <<  in.getForceGoDownThreshold();
            break;
        }



        //        if (forceMag < in.getForceGoDownThreshold())
        //        {
        //            if (!forceTimerStarted)
        //            {
        //                forceTimerStart =  TimeUtil::GetTime();
        //                forceTimerStarted = true;
        //            }
        //            IceUtil::Time forceTimerNow = TimeUtil::GetTime();
        //            double waitTime = (forceTimerStart - forceTimerNow).toSecondsDouble();
        //            if (waitTime > in.getWaitTime())
        //            {
        //                break;
        //            }
        //        }
        //        else
        //        {
        //            forceTimerStarted = false;
        //        }
        c.waitForCycleDuration();
    }

    getTextToSpeech()->reportText("I got it. We can lower the guard now.");
    usleep(2000000);


    ARMARX_INFO << "Going Down ";
    emitGoDownForceDetected();

}

//void GoDownForceDetection::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GoDownForceDetection::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GoDownForceDetection::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GoDownForceDetection(stateData));
}
