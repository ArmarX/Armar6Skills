/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     JeffGao ( jianfenggaobit at gmail dot com )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RemoveGuardV2.h"

#include <chrono>
#include <optional>
#include <thread>


//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <armar6/skills/statecharts/Armar6BoardSupportDemo/Armar6BoardSupportDemoStatechartContext.generated.h>
#include <ArmarXCore/core/time/CycleUtil.h>
#include <armar6/skills/interface/LookAtHandInterface.h>

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
RemoveGuardV2::SubClassRegistry RemoveGuardV2::Registry(RemoveGuardV2::GetName(), &RemoveGuardV2::CreateInstance);



void RemoveGuardV2::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    getDebugDrawerTopic()->clearAll();
    auto display = in.getDisplay_RemovingTheGuard();
    if (display.size() == 2)
    {
        getMessageDisplay()->setMessage(display[0], display[1]);
    }
    else
    {
        ARMARX_WARNING << "Display text not set (wrong size): " << display.size();
    }

    bool isGuardLifting = true;
    if (in.getIsGuardPlacement())
    {
        isGuardLifting = false;
    }
    local.setIsGuardLifting(isGuardLifting);
    //    getLookAtHand()->enableLookAtHand();
}


namespace
{
    class WaitUntilHandIsClosed
    {
    public:

        WaitUntilHandIsClosed(DatafieldRefPtr jointAngleDf, std::optional<IceUtil::Time> start = std::nullopt) :
            jointAngleDf(jointAngleDf), start(start)
        {
        }


        bool waitUntilHandIsClosed(float differenceThreshold, IceUtil::Time timeout)
        {
            using namespace std::chrono_literals;

            if (not start.has_value())
            {
                start = IceUtil::Time::now();
            }
            float lastValue = jointAngleDf->getFloat();

            CycleUtil cycle(10);
            while (true)
            {
                float currentValue = jointAngleDf->getFloat();
                float diff = std::abs(lastValue - currentValue);
                if (diff < differenceThreshold)
                {
                    break;
                }

                if (timeout.toSecondsDouble() > 0 && (IceUtil::Time::now() - *start) > timeout)
                {
                    return false;
                }

                cycle.waitForCycleDuration();
            }

            std::this_thread::sleep_for(200ms);

            return true;
        }


    public:

        DatafieldRefPtr jointAngleDf;
        std::optional<IceUtil::Time> start;

    };
}


void RemoveGuardV2::run()
{
    using namespace std::chrono_literals;

    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    // get a private kinematic instance for this state of the robot (tick "Robot State Component" proxy checkbox in statechart group)
    //    VirtualRobot::RobotPtr localRobot = getLocalRobot();

    DatafieldRefPtr thumbDFLeft =  DatafieldRefPtr::dynamicCast(getKinematicUnitObserver()->getDatafieldRefByName("jointangles", "LeftHandThumb"));
    DatafieldRefPtr thumbDFRight =  DatafieldRefPtr::dynamicCast(getKinematicUnitObserver()->getDatafieldRefByName("jointangles", "RightHandThumb"));
    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx leftHandcontroller;
    if (!getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"))
    {
        leftHandcontroller = StateBase::getContext<Armar6BoardSupportDemoStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_L_EEF_NJointKITHandV2ShapeController");
    }
    else
    {
        leftHandcontroller =  devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"));
    }
    leftHandcontroller->activateController();

    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx rightHandcontroller;
    if (!getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"))
    {
        rightHandcontroller = StateBase::getContext<Armar6BoardSupportDemoStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_R_EEF_NJointKITHandV2ShapeController");
    }
    else
    {
        rightHandcontroller = devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"));
    }
    rightHandcontroller->activateController();

    float minThumbCloseValue = 0.5;
    float thumbLeftClose = 0;
    float thumbRightClose = 0;


    auto waitForBothHands = [ & ](const std::string& ctrl = "close")
    {
        if (ctrl == "close")
        {
            leftHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
            rightHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
        }
        else
        {
            leftHandcontroller->setTargetsWithPwm(0, 0, 1, 1);
            rightHandcontroller->setTargetsWithPwm(0, 0, 1, 1);
        }

        std::this_thread::sleep_for(500ms);

        const IceUtil::Time start = IceUtil::Time::now();

        const std::vector<std::string> sides { "left", "right" };
        std::vector<WaitUntilHandIsClosed> waits;
        waits.emplace_back(thumbDFLeft, start);
        waits.emplace_back(thumbDFRight, start);

        const float diffThreshold = 0.02;
        const IceUtil::Time timeout = IceUtil::Time::milliSeconds(3000);

        for (size_t i = 0; i < waits.size(); ++i)
        {
            if (not waits[i].waitUntilHandIsClosed(diffThreshold, timeout))
            {
                ARMARX_WARNING << "Hand joint angles of " << sides.at(i) << " hand did not converge after "
                               << timeout.toMilliSecondsDouble() << " ms.";

                // Open hands again.
                leftHandcontroller->setTargetsWithPwm(0, 0, 1, 1);
                rightHandcontroller->setTargetsWithPwm(0, 0, 1, 1);

                emitFailure();
            }
        }
    };


    while (thumbLeftClose < minThumbCloseValue || thumbRightClose < minThumbCloseValue)
    {
        ARMARX_INFO << "Closing hand ... ";

        // Wait until joint angles converge.
        waitForBothHands("close");

        thumbLeftClose = thumbDFLeft->getDataField()->getFloat();
        thumbRightClose = thumbDFRight->getDataField()->getFloat();


        ARMARX_INFO << "Opening hand ... ";
        waitForBothHands("open");
    }


    float thumbLeftThreshold = thumbLeftClose - 0.1;
    float thumbRightThreshold = thumbRightClose - 0.1;

    ARMARX_INFO << "Closed thumb joint values and thresholds: "
                << "\n- left:  v = " << thumbLeftClose << " => threshold = " << thumbLeftThreshold
                << "\n- right: v = " << thumbRightClose << " => threshold = " << thumbRightThreshold;

    local.setLeftThumbThreshold(thumbLeftThreshold);
    local.setRightThumbThreshold(thumbRightThreshold);
}

void RemoveGuardV2::onBreak()
{
    //    getLookAtHand()->disableLookAtHand();
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void RemoveGuardV2::onExit()
{
    //    getLookAtHand()->disableLookAtHand();
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr RemoveGuardV2::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new RemoveGuardV2(stateData));
}
