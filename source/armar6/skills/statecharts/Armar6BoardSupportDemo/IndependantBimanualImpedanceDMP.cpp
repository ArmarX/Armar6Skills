/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "IndependantBimanualImpedanceDMP.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>
#include <armarx/control/deprecated_njoint_mp_controller/task_space/TaskSpaceImpedanceDMPControllerInterface.h>

#include "Helpers.h"

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
IndependantBimanualImpedanceDMP::SubClassRegistry IndependantBimanualImpedanceDMP::Registry(IndependantBimanualImpedanceDMP::GetName(), &IndependantBimanualImpedanceDMP::CreateInstance);



void IndependantBimanualImpedanceDMP::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void IndependantBimanualImpedanceDMP::run()
{
    //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");

    NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController("leftController");
    if (controller)
    {
        if (controller->isControllerActive())
        {
            controller->deactivateController();
            while (controller->isControllerActive())
            {
                TimeUtil::SleepMS(10);
            }
        }
        controller->deleteController();
        TimeUtil::SleepMS(10);
    }

    controller = getRobotUnit()->getNJointController("rightController");
    if (controller)
    {
        if (controller->isControllerActive())
        {
            controller->deactivateController();
            while (controller->isControllerActive())
            {
                TimeUtil::SleepMS(10);
            }
        }
        controller->deleteController();
        TimeUtil::SleepMS(10);
    }

    double phaseL = 100;
    double phaseK = 1000;
    double phaseDist0 = 100;
    double phaseDist1 = 50;
    double posToOriRatio = 10;

    std::vector<float> defaultLeftJointValues;
    if (in.isDesiredLeftJointValuesSet())
    {
        defaultLeftJointValues = in.getDesiredLeftJointValues();
    }
    else
    {
        defaultLeftJointValues = getRobot()->getRobotNodeSet("LeftArm")->getJointValues();
    }

    std::vector<float> defaultRightJointValues;
    if (in.isDesiredRightJointValuesSet())
    {
        defaultRightJointValues = in.getDesiredRightJointValues();
    }
    else
    {
        defaultRightJointValues = getRobot()->getRobotNodeSet("RightArm")->getJointValues();
    }

    float torqueLimit = in.getTorqueLimit();

    std::string forceSensorName = "FT L";
    float waitTimeForCalibration = 0.0;
    float forceFilter = 0.5;
    float forceDeadZone = 2;
    Eigen::Vector3f forceThreshold{100, 100, 100};
    std::string forceFrameName = "ArmL8_Wri2";

    NJointTaskSpaceImpedanceDMPControllerConfigPtr tsConfig = new NJointTaskSpaceImpedanceDMPControllerConfig(
        in.getDMPKernelSize(),
        in.getBaseMode(),
        in.getDMPStyle(),
        in.getTimeDuration(),
        "LeftArm",
        phaseL,
        phaseK,
        phaseDist0,
        phaseDist1,
        posToOriRatio,
        in.getKposVec(),
        in.getDposVec(),
        in.getKoriVec(),
        in.getDoriVec(),
        in.getKnull(),
        in.getDnull(),
        false,
        defaultLeftJointValues,
        torqueLimit,
        forceSensorName,
        waitTimeForCalibration,
        forceFilter,
        forceDeadZone,
        forceThreshold,
        forceFrameName
    );




    NJointTaskSpaceImpedanceDMPControllerInterfacePrx leftController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", in.getLeftControllerName(), tsConfig));


    tsConfig->nodeSetName = "RightArm";
    tsConfig->defaultNullSpaceJointValues = defaultRightJointValues;
    tsConfig->forceFrameName = "ArmR8_Wri2";
    tsConfig->forceSensorName = "FT R";

    NJointTaskSpaceImpedanceDMPControllerInterfacePrx rightController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", in.getRightControllerName(), tsConfig));


    std::vector<std::string> fileNames = in.getLeftArmMotionFile();
    leftController->learnDMPFromFiles(fileNames);
    fileNames = in.getRightArmMotionFile();
    rightController->learnDMPFromFiles(fileNames);

    std::vector<double> leftGoals = Helpers::pose2dvec(in.getLeftTcpGoalPose()->toEigen());

    if (in.isLeftViaPoseSet())
    {
        std::map<std::string, armarx::PosePtr> viaPoseMap = in.getLeftViaPose();

        for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
             it != viaPoseMap.end(); ++it)
        {
            double canVal = std::stod(it->first);
            leftController->setViaPoints(canVal, Helpers::pose2dvec(it->second->toEigen()));

        }
    }

    std::vector<double> rightGoals = Helpers::pose2dvec(in.getRightTcpGoalPose()->toEigen());

    if (in.isRightViaPoseSet())
    {
        std::map<std::string, armarx::PosePtr> viaPoseMap = in.getRightViaPose();

        for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
             it != viaPoseMap.end(); ++it)
        {
            double canVal = std::stod(it->first);
            rightController->setViaPoints(canVal, Helpers::pose2dvec(it->second->toEigen()));

        }
    }


    leftController->activateController();
    rightController->activateController();
    leftController->runDMP(leftGoals);
    rightController->runDMP(rightGoals);


    IceUtil::Time dmpSoftStop_starter;
    bool isDMPFinished = false;
    if (in.isShowGuardSet() && in.getShowGuard())
    {
        VirtualRobot::RobotPtr localRobot = getLocalRobot();
        std::string layerName = "guard_layer";
        std::string boxName = "guard";
        Eigen::Vector3f dimensions;
        dimensions << 805, 1205, 20;
        DrawColor color = {0.5, 0.5, 0.5, 1};

        while (!isRunningTaskStopped() && !(leftController->isFinished() && leftController->isFinished()))
        {
            RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

            // board debug drawer
            Eigen::Matrix4f leftPose = localRobot->getRobotNodeSet("LeftArm")->getTCP()->getPoseInRootFrame();
            Eigen::Matrix4f rightPose = localRobot->getRobotNodeSet("RightArm")->getTCP()->getPoseInRootFrame();
            Eigen::Matrix4f guardLocal = Eigen::Matrix4f::Identity();
            guardLocal.block<3, 1>(0, 3) = 0.5 * leftPose.block<3, 1>(0, 3) + 0.5 * rightPose.block<3, 1>(0, 3);
            guardLocal(1, 3) += 600;
            Eigen::Matrix4f globalPose = localRobot->getRootNode()->toGlobalCoordinateSystem(guardLocal);

            getDebugDrawerTopic()->setBoxVisu(layerName, boxName, new Pose(globalPose), new Vector3(dimensions), color);


            if (leftController->isFinished() && leftController->isFinished() && !isDMPFinished)
            {
                // soft stop:
                dmpSoftStop_starter = TimeUtil::GetTime();
                isDMPFinished = true;

            }

            if (isDMPFinished)
            {
                IceUtil::Time dmpSoftStop_stopper = TimeUtil::GetTime();
                float softStopTimer = (dmpSoftStop_stopper - dmpSoftStop_starter).toSecondsDouble();
                if (softStopTimer > in.getSoftStopTimer())
                {
                    break;
                }
            }
        }
    }
    else
    {
        while (!isRunningTaskStopped() && !(leftController->isFinished() && rightController->isFinished()))
        {
        }
    }

    leftController->deactivateController();
    rightController->deactivateController();

    while (leftController->isControllerActive() || rightController->isControllerActive())
    {
        usleep(10000);
    }

    leftController->deleteController();
    rightController->deleteController();

    usleep(100000);

    emitSuccess();
}

//void IndependantBimanualImpedanceDMP::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void IndependantBimanualImpedanceDMP::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr IndependantBimanualImpedanceDMP::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new IndependantBimanualImpedanceDMP(stateData));
}
