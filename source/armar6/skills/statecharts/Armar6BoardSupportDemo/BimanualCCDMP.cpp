/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BimanualCCDMP.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <armarx/control/deprecated_njoint_mp_controller/bimanual/ControllerInterface.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <armar6/skills/statecharts/Armar6BoardSupportDemo/Armar6BoardSupportDemoStatechartContext.generated.h>

#include "Helpers.h"

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
BimanualCCDMP::SubClassRegistry BimanualCCDMP::Registry(BimanualCCDMP::GetName(), &BimanualCCDMP::CreateInstance);



void BimanualCCDMP::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void BimanualCCDMP::run()
{
    //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");


    NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController("dmpController");
    if (controller)
    {
        if (controller->isControllerActive())
        {
            controller->deactivateController();
            while (controller->isControllerActive())
            {
                TimeUtil::SleepMS(10);
            }
        }
        controller->deleteController();
        TimeUtil::SleepMS(10);
    }

    int kernelSize = 100;
    std::string dmpMode = "Linear";//MinimumJerk
    std::string dmpType = "Discrete";
    double phaseL = 1000;
    double phaseK = 1000;
    double phaseDist0 = 100;
    double phaseDist1 = 50;
    double phaseKpPos = 1;
    double phaseKpOri = 1;
    double timeDuration = in.getTimeDuration();
    std::string defaultLeader = "Left";
    double posToOriRatio = 10;

    double maxLinearVel = in.getMaxLinearVel();
    double maxAngularVel = in.getMaxAngularVel();

    std::vector<float> leftKpos = in.getleftKposVec();
    std::vector<float> leftDpos = in.getleftDposVec();
    std::vector<float> leftKori = in.getleftKoriVec();
    std::vector<float> leftDori = in.getleftDoriVec();

    std::vector<float> rightKpos = in.getrightKposVec();
    std::vector<float> rightDpos = in.getrightDposVec();
    std::vector<float> rightKori = in.getrightKoriVec();
    std::vector<float> rightDori = in.getrightDoriVec();

    float knull = in.getKnull();
    float dnull = in.getDnull();

    float torqueLimit = in.getTorqueLimit();

    std::vector<float> leftDesiredJointValues;

    if (in.isDesiredLeftJointValuesSet())
    {
        leftDesiredJointValues = in.getDesiredLeftJointValues();
    }
    else
    {
        leftDesiredJointValues = getRobot()->getRobotNodeSet("LeftArm")->getJointValues();
    }

    std::vector<float> rightDesiredJointValues;
    if (in.isDesiredRightJointValuesSet())
    {
        rightDesiredJointValues = in.getDesiredRightJointValues();
    }
    else
    {
        rightDesiredJointValues = getRobot()->getRobotNodeSet("RightArm")->getJointValues();

    }

    float startReduceTorqueTime = in.getStartReduceTorqueTime();

    NJointBimanualCCDMPControllerConfigPtr config = new NJointBimanualCCDMPControllerConfig(
        kernelSize,
        dmpMode,
        dmpType,
        phaseL,
        phaseK,
        phaseDist0,
        phaseDist1,
        phaseKpPos,
        phaseKpOri,
        posToOriRatio,
        timeDuration,
        defaultLeader,
        leftDesiredJointValues,
        rightDesiredJointValues,
        maxLinearVel,
        maxAngularVel,
        leftKpos,
        leftDpos,
        leftKori,
        leftDori,
        rightKpos,
        rightDpos,
        rightKori,
        rightDori,
        knull,
        dnull,
        torqueLimit,
        startReduceTorqueTime);


    NJointBimanualCCDMPControllerInterfacePrx dmpController
        = NJointBimanualCCDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointBimanualCCDMPController", "dmpController", config));

    std::vector<std::string> fileNames = in.getLeftLeaderFileNames();
    dmpController->learnDMPFromFiles("LeftLeader", fileNames);

    fileNames = in.getLeftFollowerFileNames();
    dmpController->learnDMPFromFiles("LeftFollower", fileNames);

    fileNames = in.getRightLeaderFileNames();
    dmpController->learnDMPFromFiles("RightLeader", fileNames);

    fileNames = in.getRightFollowerFileNames();
    dmpController->learnDMPFromFiles("RightFollower", fileNames);


    std::vector<double> leftGoals = Helpers::pose2dvec(in.getLeftGoals()->toEigen());
    std::vector<double> rightGoals = Helpers::pose2dvec(in.getRightGoals()->toEigen());

    dmpController->activateController();

    if (in.isViaPoseSet())
    {
        dmpController->setViaPoints(0.5, Helpers::pose2dvec(in.getViaPose()->toEigen()));
    }



    auto leftHandcontroller = StateBase::getContext<Armar6BoardSupportDemoStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_L_EEF_NJointKITHandV2ShapeController");
    if (!leftHandcontroller->isControllerActive())
    {
        leftHandcontroller->activateController();
    }


    auto rightHandcontroller = StateBase::getContext<Armar6BoardSupportDemoStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_R_EEF_NJointKITHandV2ShapeController");
    if (!rightHandcontroller->isControllerActive())
    {
        rightHandcontroller->activateController();
    }

    leftHandcontroller->setTargets(0, 0);
    rightHandcontroller->setTargets(0, 0);

    bool handclosed = false;


    DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT"));
    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT"));
    Eigen::Vector3f initialForceLeft;
    Eigen::Vector3f initialForceRight;


    IceUtil::Time start = TimeUtil::GetTime();


    std::string layerName = "guard_layer";
    std::string boxName = "guard";
    Eigen::Vector3f dimensions;
    dimensions << 805, 1205, 20;
    DrawColor color = {0.5, 0.5, 0.5, 1};

    dmpController->runDMP(leftGoals, rightGoals);

    VirtualRobot::RobotPtr localRobot = getLocalRobot();

    {
        FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
        initialForceRight = forcePtr->toRootEigen(localRobot);
    }
    {
        FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
        initialForceLeft = forcePtr->toRootEigen(localRobot);
    }

    IceUtil::Time dmpSoftStop_starter;
    bool isDMPFinished = false;
    while (!isRunningTaskStopped() /*&& !dmpController->isFinished()*/) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

        if (in.isShowGuardSet() && in.getShowGuard())
        {
            // board debug drawer
            Eigen::Matrix4f leftTipPose = Eigen::Matrix4f::Identity();
            leftTipPose(2, 3) += 100;
            leftTipPose = localRobot->getRobotNodeSet("LeftArm")->getTCP()->toGlobalCoordinateSystem(leftTipPose);
            Eigen::Matrix4f rigtTipPose = Eigen::Matrix4f::Identity();
            rigtTipPose(2, 3) += 100;
            rigtTipPose = localRobot->getRobotNodeSet("RightArm")->getTCP()->toGlobalCoordinateSystem(rigtTipPose);

            Eigen::Matrix4f leftPose = localRobot->getRobotNodeSet("LeftArm")->getTCP()->getPoseInRootFrame();
            Eigen::Matrix4f rightPose = localRobot->getRobotNodeSet("RightArm")->getTCP()->getPoseInRootFrame();
            Eigen::Matrix4f guardLocal = Eigen::Matrix4f::Identity();
            guardLocal.block<3, 1>(0, 3) = 0.5 * leftPose.block<3, 1>(0, 3) + 0.5 * rightPose.block<3, 1>(0, 3);
            guardLocal(1, 3) += 600;
            guardLocal(2, 3) = std::max(leftTipPose(2, 3), rigtTipPose(2, 3));
            Eigen::Matrix4f globalPose = localRobot->getRootNode()->toGlobalCoordinateSystem(guardLocal);

            getDebugDrawerTopic()->setBoxVisu(layerName, boxName, new Pose(globalPose), new Vector3(dimensions), color);
        }


        // force detection
        IceUtil::Time now = TimeUtil::GetTime();
        float t = (now - start).toSecondsDouble();

        if (t < in.getMinExecutionTime())
        {
            {
                FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
                initialForceRight = forcePtr->toRootEigen(localRobot);
            }
            {
                FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
                initialForceLeft = forcePtr->toRootEigen(localRobot);
            }
            continue;
        }
        Eigen::Vector3f forceLeft;
        Eigen::Vector3f origForceLeft;
        {
            FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
            origForceLeft = forcePtr->toRootEigen(localRobot);
            forceLeft = origForceLeft - initialForceLeft;
        }

        Eigen::Vector3f forceRight;
        Eigen::Vector3f origForceRight;
        {
            FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
            origForceRight = forcePtr->toRootEigen(localRobot);
            forceRight = origForceRight - initialForceRight;
        }

        float forcelefty = forceLeft(1);
        float forcerighty = forceRight(1);
        getDebugObserver()->setDebugDatafield("BimanualCCDMP", "forcelefty", new Variant(forcelefty));
        getDebugObserver()->setDebugDatafield("BimanualCCDMP", "forcerighty", new Variant(forcerighty));
        bool forceTest = (forcelefty < in.getCloseHandForceThreshold()) && (forcerighty < in.getCloseHandForceThreshold());
        if (in.getIsCloseHand() && forceTest && !handclosed)
        {
            leftHandcontroller->setTargetsWithPwm(1, 2, 1, 1);
            rightHandcontroller->setTargetsWithPwm(1, 2, 1, 1);
            handclosed = true;
        }

        if (dmpController->isFinished() && !isDMPFinished)
        {
            // soft stop:
            dmpSoftStop_starter = TimeUtil::GetTime();
            isDMPFinished = true;

        }

        if (isDMPFinished)
        {
            IceUtil::Time dmpSoftStop_stopper = TimeUtil::GetTime();
            float softStopTimer = (dmpSoftStop_stopper - dmpSoftStop_starter).toSecondsDouble();
            if (softStopTimer > in.getSoftStopTimer())
            {
                if (in.getIsCloseHand())
                {
                    if (handclosed)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }

    }

    dmpController->deactivateController();
    while (dmpController->isControllerActive())
    {
        usleep(10000);
    }
    dmpController->deleteController();

    emitSuccess();
}

//void BimanualCCDMP::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void BimanualCCDMP::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr BimanualCCDMP::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new BimanualCCDMP(stateData));
}
