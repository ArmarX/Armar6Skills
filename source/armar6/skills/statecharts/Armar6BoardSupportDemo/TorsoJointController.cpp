/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     JeffGao ( jianfenggaobit at gmail dot com )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TorsoJointController.h"

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityController.h>

#include <RobotAPI/libraries/core/CartesianPositionController.h>

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <RobotAPI/libraries/core/FramedPose.h>


using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
TorsoJointController::SubClassRegistry TorsoJointController::Registry(TorsoJointController::GetName(), &TorsoJointController::CreateInstance);



void TorsoJointController::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void TorsoJointController::run()
{
    VirtualRobot::RobotPtr robot = getLocalRobot();


    DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT"));
    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT"));
    Eigen::Vector3f initialForceLeft;
    initialForceLeft.setZero();
    Eigen::Vector3f initialForceRight;
    initialForceRight.setZero();

    double timeDuration = 0;
    IceUtil::Time start = TimeUtil::GetTime();
    float forceFilter = 0.9;

    while (timeDuration < in.getForceCalibrationTime())
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

        IceUtil::Time now = TimeUtil::GetTime();
        timeDuration = (now - start).toSecondsDouble();

        {
            FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
            initialForceLeft = (1 - forceFilter) * initialForceLeft + forceFilter * forcePtr->toRootEigen(robot);
        }
        {
            FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
            initialForceRight = (1 - forceFilter) * initialForceRight + forceFilter * forcePtr->toRootEigen(robot);
        }

    }

    NameValueMap js;
    js["TorsoJoint"] = -364;
    NameControlModeMap jcm;
    jcm["TorsoJoint"] = ePositionControl;
    getKinematicUnit()->switchControlMode(jcm);
    getKinematicUnit()->setJointAngles(js);


    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

        Eigen::Vector3f forceLeft;
        {
            FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
            forceLeft = forcePtr->toRootEigen(robot);
            forceLeft -= initialForceLeft;
        }

        Eigen::Vector3f forceRight;
        {
            FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
            forceRight = forcePtr->toRootEigen(robot);
            forceRight -= initialForceRight;
        }

        float currentDownForce = fabs(forceRight(2)) + fabs(forceLeft(2));

        if (currentDownForce > in.getTouchThreshold())
        {
            break;
        }

        getDebugObserver()->setDebugDatafield("TorsoJointController", "currentDownforce", new Variant(currentDownForce));
    }

    jcm["TorsoJoint"] = eVelocityControl;
    getKinematicUnit()->switchControlMode(jcm);
    js["TorsoJoint"] = 0;
    getKinematicUnit()->setJointVelocities(js);
    emitSuccess();

}

//void TorsoJointController::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void TorsoJointController::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TorsoJointController::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TorsoJointController(stateData));
}
