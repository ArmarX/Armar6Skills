/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "IndependantBimanualDMP.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <VirtualRobot/VirtualRobot.h>
#include <armarx/control/deprecated_njoint_mp_controller/task_space/ControllerInterface.h>
#include "Helpers.h"

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
IndependantBimanualDMP::SubClassRegistry IndependantBimanualDMP::Registry(IndependantBimanualDMP::GetName(), &IndependantBimanualDMP::CreateInstance);



void IndependantBimanualDMP::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}



void IndependantBimanualDMP::run()
{
    //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");

    NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController("leftController");
    if (controller)
    {
        if (controller->isControllerActive())
        {
            controller->deactivateController();
            while (controller->isControllerActive())
            {
                TimeUtil::SleepMS(10);
            }
        }
        controller->deleteController();
        TimeUtil::SleepMS(10);
    }

    controller = getRobotUnit()->getNJointController("rightController");
    if (controller)
    {
        if (controller->isControllerActive())
        {
            controller->deactivateController();
            while (controller->isControllerActive())
            {
                TimeUtil::SleepMS(10);
            }
        }
        controller->deleteController();
        TimeUtil::SleepMS(10);
    }

    NJointTaskSpaceDMPControllerConfigPtr config = new NJointTaskSpaceDMPControllerConfig(
        20,
        200,
        1,
        "MinimumJerk",
        "Discrete",
        1,
        100,
        1000,
        1000,
        500,
        1,
        1,
        in.getTimeDuration(),
        100,
        "LeftArm",
        "",
        "",
        NJointTaskSpaceDMPControllerMode::eAll,
        1000,
        100,
        50.0,
        "LeftArm",
        in.getKpLinearVel(),
        in.getKdLinearVel(),
        in.getKpAngularVel(),
        in.getKdAngularVel(),
        1.0,
        1.0
    );

    NJointTaskSpaceDMPControllerInterfacePrx leftController
        = NJointTaskSpaceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTSDMPController", "leftController", config));


    config->nodeSetName = "RightArm";

    config->debugName = "RightArm";
    NJointTaskSpaceDMPControllerInterfacePrx rightController
        = NJointTaskSpaceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTSDMPController", "rightController", config));


    std::vector<std::string> fileNames = in.getArmMotionFileLeft();
    leftController->learnDMPFromFiles(fileNames);


    fileNames = in.getArmMotionFileRight();
    rightController->learnDMPFromFiles(fileNames);

    std::vector<double> leftGoals = Helpers::pose2dvec(in.getLeftTcpGoalPose()->toEigen());

    if (in.isLeftViaPoseSet())
    {
        std::map<std::string, armarx::PosePtr> viaPoseMap = in.getLeftViaPose();

        for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
             it != viaPoseMap.end(); ++it)
        {
            double canVal = std::stod(it->first);
            leftController->setViaPoints(canVal, Helpers::pose2dvec(it->second->toEigen()));

        }
    }

    std::vector<double> rightGoals = Helpers::pose2dvec(in.getRightTcpGoalPose()->toEigen());

    if (in.isRightViaPoseSet())
    {
        std::map<std::string, armarx::PosePtr> viaPoseMap = in.getRightViaPose();

        for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
             it != viaPoseMap.end(); ++it)
        {
            double canVal = std::stod(it->first);
            rightController->setViaPoints(canVal, Helpers::pose2dvec(it->second->toEigen()));

        }
    }



    leftController->activateController();
    rightController->activateController();

    leftController->runDMP(leftGoals, 1.0);
    rightController->runDMP(rightGoals, 1.0);


    VirtualRobot::RobotPtr localRobot = getLocalRobot();
    std::string layerName = "guard_layer";
    std::string boxName = "guard";
    Eigen::Vector3f dimensions;
    dimensions << 805, 1205, 20;
    DrawColor color = {0.5, 0.5, 0.5, 1};

    Eigen::Matrix3f rotation_x = Eigen::Matrix3f::Zero();
    Eigen::Matrix3f rotation_y = Eigen::Matrix3f::Zero();
    rotation_y(0, 2) = -1;
    rotation_y(2, 0) = 1;
    rotation_y(1, 1) = 1;

    rotation_x(0, 0) = 1;
    rotation_x(1, 1) = 0.866;
    rotation_x(1, 2) = 0.5;
    rotation_x(2, 1) = -0.5;
    rotation_x(2, 2) = 0.866;

    Eigen::Matrix4f guardLocal = Eigen::Matrix4f::Identity();
    guardLocal.block<3, 3>(0, 0) =  rotation_x * rotation_y;
    guardLocal(0, 3) += 0;
    guardLocal(2, 3) += 465;

    while (!isRunningTaskStopped() && (!rightController->isFinished() || !leftController->isFinished())) // stop run function if returning true
    {

        if (in.isShowGuardSet() && in.getShowGuard())
        {
            RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

            // board debug drawer
            Eigen::Matrix4f globalPose = localRobot->getRobotNodeSet("RightArm")->getTCP()->toGlobalCoordinateSystem(guardLocal);
            getDebugDrawerTopic()->setBoxVisu(layerName, boxName, new Pose(globalPose), new Vector3(dimensions), color);
        }
    }

    leftController->deactivateController();
    rightController->deactivateController();

    while (leftController->isControllerActive() || rightController->isControllerActive())
    {
        usleep(10000);
    }
    leftController->deleteController();
    rightController->deleteController();

    emitSuccess();

}

//void IndependantBimanualDMP::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void IndependantBimanualDMP::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr IndependantBimanualDMP::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new IndependantBimanualDMP(stateData));
}
