/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     JeffGao ( jianfenggaobit at gmail dot com )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GuardInHandManipulate.h"
#include <ArmarXCore/core/time/TimeUtil.h>

#include <armarx/control/deprecated_njoint_mp_controller/task_space/TaskSpaceImpedanceDMPControllerInterface.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <armar6/skills/statecharts/Armar6BoardSupportDemo/Armar6BoardSupportDemoStatechartContext.generated.h>
#include "Helpers.h"

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
GuardInHandManipulate::SubClassRegistry GuardInHandManipulate::Registry(GuardInHandManipulate::GetName(), &GuardInHandManipulate::CreateInstance);



void GuardInHandManipulate::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void GuardInHandManipulate::run()
{
    //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");
    VirtualRobot::RobotPtr robot = getLocalRobot();

    /* hand controller initialization */
    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx leftHandcontroller;
    if (!getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"))
    {
        leftHandcontroller = StateBase::getContext<Armar6BoardSupportDemoStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_L_EEF_NJointKITHandV2ShapeController");
    }
    else
    {
        leftHandcontroller =  devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"));
    }
    leftHandcontroller->activateController();

    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx rightHandcontroller;
    if (!getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"))
    {
        rightHandcontroller = StateBase::getContext<Armar6BoardSupportDemoStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_R_EEF_NJointKITHandV2ShapeController");
    }
    else
    {
        rightHandcontroller = devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"));
    }
    rightHandcontroller->activateController();


    //    /* Force Calibration */
    DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT"));
    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT"));
    Eigen::Vector3f initialForceLeft;
    initialForceLeft.setZero();
    Eigen::Vector3f initialForceRight;
    initialForceRight.setZero();

    /* DMP controller setting */
    double phaseL = 100;
    double phaseK = 1000;
    double phaseDist0 = 100;
    double phaseDist1 = 50;
    double posToOriRatio = 10;

    std::vector<float> defaultLeftJointValues;
    if (in.isDesiredLeftJointValuesSet())
    {
        defaultLeftJointValues = in.getDesiredLeftJointValues();
    }
    else
    {
        defaultLeftJointValues = getRobot()->getRobotNodeSet("LeftArm")->getJointValues();
    }

    std::vector<float> defaultRightJointValues;
    if (in.isDesiredRightJointValuesSet())
    {
        defaultRightJointValues = in.getDesiredRightJointValues();
    }
    else
    {
        defaultRightJointValues = getRobot()->getRobotNodeSet("RightArm")->getJointValues();
    }

    float torqueLimit = in.getTorqueLimit();

    std::string forceSensorName = "FT L";
    float waitTimeForCalibration = 1;
    float forceFilter = 0.5;
    float forceDeadZone = 2;
    Eigen::Vector3f forceThreshold{100, 100, 100};
    std::string forceFrameName = "ArmL8_Wri2";

    NJointTaskSpaceImpedanceDMPControllerConfigPtr tsConfig = new NJointTaskSpaceImpedanceDMPControllerConfig(
        in.getDMPKernelSize(),
        in.getBaseMode(),
        in.getDMPStyle(),
        in.getTimeDuration(),
        "LeftArm",
        phaseL,
        phaseK,
        phaseDist0,
        phaseDist1,
        posToOriRatio,
        in.getKposVec(),
        in.getDposVec(),
        in.getKoriVec(),
        in.getDoriVec(),
        in.getKnull(),
        in.getDnull(),
        false,
        defaultLeftJointValues,
        torqueLimit,
        forceSensorName,
        waitTimeForCalibration,
        forceFilter,
        forceDeadZone,
        forceThreshold,
        forceFrameName

    );
    NJointTaskSpaceImpedanceDMPControllerInterfacePrx leftController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", "leftController", tsConfig));
    tsConfig->nodeSetName = "RightArm";
    tsConfig->defaultNullSpaceJointValues = defaultRightJointValues;
    NJointTaskSpaceImpedanceDMPControllerInterfacePrx rightController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", "rightController", tsConfig));

    //DatafieldRefPtr thumbDFLeft =  DatafieldRefPtr::dynamicCast(getKinematicUnitObserver()->getDatafieldRefByName("jointangles", "LeftHandThumb"));
    //DatafieldRefPtr thumbDFRight =  DatafieldRefPtr::dynamicCast(getKinematicUnitObserver()->getDatafieldRefByName("jointangles", "RightHandThumb"));

    std::vector<std::string> fileNames = in.getLeftArmMotionFile();
    leftController->learnDMPFromFiles(fileNames);
    fileNames = in.getRightArmMotionFile();
    rightController->learnDMPFromFiles(fileNames);

    RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

    //float leftThumbValue = robot->getRobotNode("LeftHandThumb")->getJointValue();
    //float rightThumbValue = robot->getRobotNode("RightHandThumb")->getJointValue();

    const Eigen::Matrix4f origLeft = robot->getRobotNodeSet("LeftArm")->getTCP()->getPoseInRootFrame();
    const Eigen::Matrix4f origRight = robot->getRobotNodeSet("RightArm")->getTCP()->getPoseInRootFrame();

    // control left arm
    {
        leftHandcontroller->setTargetsWithPwm(0, 0, 1, 1);
        usleep(1000000);

        Eigen::Matrix4f currentLeftTarget = origLeft;
        currentLeftTarget(1, 3) -= 100;
        currentLeftTarget(2, 3) -= 150;
        leftController->activateController();
        leftController->runDMP(Helpers::pose2dvec(currentLeftTarget));
        while (!leftController->isFinished())
        {
            usleep(1000);
        }

        leftController->stopDMP();
        leftHandcontroller->setTargetsWithPwm(2, 2, 1, 1);
        usleep(1000000);

        currentLeftTarget = origLeft;
        currentLeftTarget.block<3, 3>(0, 0) = in.getAdjustLeftHandOri()->toEigen();
        currentLeftTarget(1, 3) += 100;
        currentLeftTarget(2, 3) -= 150;
        leftController->runDMP(Helpers::pose2dvec(currentLeftTarget));

        while (!leftController->isFinished())
        {
            usleep(1000);
        }

        currentLeftTarget = origLeft;
        currentLeftTarget.block<3, 3>(0, 0) = in.getAdjustLeftHandOri()->toEigen();
        currentLeftTarget(1, 3) += 100;
        currentLeftTarget(2, 3) -= 50;
        leftController->runDMP(Helpers::pose2dvec(currentLeftTarget));

        while (!leftController->isFinished())
        {
            usleep(1000);
        }

    }


    // control Right arm
    {
        rightHandcontroller->setTargetsWithPwm(0, 0, 1, 1);
        usleep(1000000);


        Eigen::Matrix4f currentRightTarget = origRight;
        currentRightTarget(1, 3) -= 100;
        currentRightTarget(2, 3) -= 150;
        rightController->activateController();
        rightController->runDMP(Helpers::pose2dvec(currentRightTarget));
        while (!rightController->isFinished())
        {
            usleep(1000);
        }

        rightController->stopDMP();
        rightHandcontroller->setTargetsWithPwm(2, 2, 1, 1);
        usleep(1000000);

        currentRightTarget = origRight;
        currentRightTarget.block<3, 3>(0, 0) = in.getAdjustRightHandOri()->toEigen();
        currentRightTarget(1, 3) += 100;
        currentRightTarget(2, 3) -= 150;
        rightController->runDMP(Helpers::pose2dvec(currentRightTarget));

        while (!rightController->isFinished())
        {
            usleep(1000);
        }

        currentRightTarget = origRight;
        currentRightTarget.block<3, 3>(0, 0) = in.getAdjustRightHandOri()->toEigen();
        currentRightTarget(1, 3) += 100;
        currentRightTarget(2, 3) -= 50;

        rightController->runDMP(Helpers::pose2dvec(currentRightTarget));

        while (!rightController->isFinished())
        {
            usleep(1000);
        }

    }




    leftController->deactivateController();
    rightController->deactivateController();

    while (leftController->isControllerActive() || rightController->isControllerActive())
    {
        usleep(10000);
    }

    leftController->deleteController();
    rightController->deleteController();

    emitSuccess();
}

//void GuardInHandManipulate::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GuardInHandManipulate::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GuardInHandManipulate::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GuardInHandManipulate(stateData));
}
