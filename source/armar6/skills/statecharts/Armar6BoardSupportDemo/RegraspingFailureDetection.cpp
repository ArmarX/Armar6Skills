/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     JeffGao ( jianfenggaobit at gmail dot com )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RegraspingFailureDetection.h"

#include <armarx/control/deprecated_njoint_mp_controller/task_space/TaskSpaceImpedanceDMPControllerInterface.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <armar6/skills/statecharts/Armar6BoardSupportDemo/Armar6BoardSupportDemoStatechartContext.generated.h>
#include "Helpers.h"
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <ArmarXCore/observers/variant/ChannelRef.h>

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
RegraspingFailureDetection::SubClassRegistry RegraspingFailureDetection::Registry(RegraspingFailureDetection::GetName(), &RegraspingFailureDetection::CreateInstance);



void RegraspingFailureDetection::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void RegraspingFailureDetection::run()
{
    //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");
    VirtualRobot::RobotPtr robot = getLocalRobot();

    /* hand controller initialization */
    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx leftHandcontroller;
    if (!getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"))
    {
        leftHandcontroller = StateBase::getContext<Armar6BoardSupportDemoStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_L_EEF_NJointKITHandV2ShapeController");
    }
    else
    {
        leftHandcontroller =  devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"));
    }
    leftHandcontroller->activateController();

    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx rightHandcontroller;
    if (!getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"))
    {
        rightHandcontroller = StateBase::getContext<Armar6BoardSupportDemoStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_R_EEF_NJointKITHandV2ShapeController");
    }
    else
    {
        rightHandcontroller = devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"));
    }
    rightHandcontroller->activateController();

    // for vision

    memoryx::ObjectInstancePtr objectInstance;
    Eigen::Matrix4f objectDefaultPose;
    Eigen::Matrix3f leftHandDefaultOri;
    Eigen::Vector3f leftHandObjOffsetVecLocal;
    Eigen::Matrix3f rightHandDefaultOri;
    Eigen::Vector3f rightHandObjOffsetVecLocal;

    if (in.getIsVisionUsed())
    {
        std::vector<ChannelRefBasePtr> objectInstanceChannelList = getObjectMemoryObserver()->getObjectInstancesByClass("guard");
        memoryx::ObjectInstanceMemorySegmentBasePrx objInstanceSeg = getWorkingMemory()->getObjectInstancesSegment();

        while (objectInstanceChannelList.size() < 1)
        {
            objectInstanceChannelList = getObjectMemoryObserver()->getObjectInstancesByClass("guard");
            usleep(1000000);
        }

        ChannelRefPtr channelRef = ChannelRefPtr::dynamicCast(objectInstanceChannelList.at(0));
        std::string instanceId = channelRef->getDataField("id")->getString();
        objectInstance =  memoryx::ObjectInstancePtr::dynamicCast(objInstanceSeg->getObjectInstanceById(instanceId));
        objectDefaultPose <<  0, -1,  0,   -25.415,
                          -1,  0,  0,    1252.67,
                          0,  0, -1,    1051.17,
                          0,  0, 0,   1;


        std::string objHandPoseName = in.getObjHandRelativePoseName();

        QuaternionPtr leftQ = in.getObjHandDefaultOriLeft()[objHandPoseName];
        Eigen::Matrix4f leftHandDefaultPose =  VirtualRobot::MathTools::quat2eigen4f(leftQ->qx, leftQ->qy, leftQ->qz, leftQ->qw);
        leftHandDefaultOri = leftHandDefaultPose.block<3, 3>(0, 0);
        leftHandObjOffsetVecLocal = in.getObjHandOffsetMapLeft()[objHandPoseName]->toEigen();

        QuaternionPtr rightQ = in.getObjHandDefaultOriRight()[objHandPoseName];
        Eigen::Matrix4f rightHandDefaultPose =  VirtualRobot::MathTools::quat2eigen4f(rightQ->qx, rightQ->qy, rightQ->qz, rightQ->qw);
        rightHandDefaultOri = rightHandDefaultPose.block<3, 3>(0, 0);
        rightHandObjOffsetVecLocal = in.getObjHandOffsetMapRight()[objHandPoseName]->toEigen();
    }

    //    /* Force Calibration */
    DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT"));
    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT"));
    Eigen::Vector3f initialForceLeft;
    initialForceLeft.setZero();
    Eigen::Vector3f initialForceRight;
    initialForceRight.setZero();
    //    IceUtil::Time start = TimeUtil::GetTime();

    //    while (timeDuration < in.getMinExecutionTime())
    //    {
    //        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

    //        IceUtil::Time now = TimeUtil::GetTime();
    //        timeDuration = (now - start).toSecondsDouble();

    //        {
    //            FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
    //            initialForceLeft = (1 - forceFilter) * initialForceLeft + forceFilter * forcePtr->toRootEigen(robot);
    //        }
    //        {
    //            FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
    //            initialForceRight = (1 - forceFilter) * initialForceRight + forceFilter * forcePtr->toRootEigen(robot);
    //        }

    //    }

    /* DMP controller setting */
    double phaseL = 100;
    double phaseK = 1000;
    double phaseDist0 = 100;
    double phaseDist1 = 50;
    double posToOriRatio = 10;

    std::vector<float> defaultLeftJointValues;
    if (in.isDesiredLeftJointValuesSet())
    {
        defaultLeftJointValues = in.getDesiredLeftJointValues();
    }
    else
    {
        defaultLeftJointValues = getRobot()->getRobotNodeSet("LeftArm")->getJointValues();
    }

    std::vector<float> defaultRightJointValues;
    if (in.isDesiredRightJointValuesSet())
    {
        defaultRightJointValues = in.getDesiredRightJointValues();
    }
    else
    {
        defaultRightJointValues = getRobot()->getRobotNodeSet("RightArm")->getJointValues();
    }

    float torqueLimit = in.getTorqueLimit();

    std::string forceSensorName = "FT L";
    float waitTimeForCalibration = 0.0;
    float forceFilter = 0.5;
    float forceDeadZone = 2;
    Eigen::Vector3f forceThreshold{100, 100, 100};
    std::string forceFrameName = "ArmL8_Wri2";

    NJointTaskSpaceImpedanceDMPControllerConfigPtr tsConfig = new NJointTaskSpaceImpedanceDMPControllerConfig(
        in.getDMPKernelSize(),
        in.getBaseMode(),
        in.getDMPStyle(),
        in.getTimeDuration(),
        "LeftArm",
        phaseL,
        phaseK,
        phaseDist0,
        phaseDist1,
        posToOriRatio,
        in.getKposVec(),
        in.getDposVec(),
        in.getKoriVec(),
        in.getDoriVec(),
        in.getKnull(),
        in.getDnull(),
        false,
        defaultLeftJointValues,
        torqueLimit,
        forceSensorName,
        waitTimeForCalibration,
        forceFilter,
        forceDeadZone,
        forceThreshold,
        forceFrameName
    );
    {
        NJointControllerInterfacePrx ctrl_L = getRobotUnit()->getNJointController("leftController");
        NJointControllerInterfacePrx ctrl_R = getRobotUnit()->getNJointController("rightController");
        if (ctrl_L)
        {
            ctrl_L->deactivateAndDeleteController();
            TimeUtil::SleepMS(10);
        }
        if (ctrl_R)
        {
            ctrl_R->deactivateAndDeleteController();
            TimeUtil::SleepMS(10);
        }
    }

    NJointTaskSpaceImpedanceDMPControllerInterfacePrx leftController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", "leftController", tsConfig));
    tsConfig->nodeSetName = "RightArm";
    tsConfig->defaultNullSpaceJointValues = defaultRightJointValues;
    tsConfig->forceFrameName = "ArmR8_Wri2";
    tsConfig->forceSensorName = "FT R";


    NJointTaskSpaceImpedanceDMPControllerInterfacePrx rightController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", "rightController", tsConfig));

    DatafieldRefPtr thumbDFLeft =  DatafieldRefPtr::dynamicCast(getKinematicUnitObserver()->getDatafieldRefByName("jointangles", "LeftHandThumb"));
    DatafieldRefPtr thumbDFRight =  DatafieldRefPtr::dynamicCast(getKinematicUnitObserver()->getDatafieldRefByName("jointangles", "RightHandThumb"));


    std::vector<std::string> fileNames = in.getLeftArmMotionFile();
    leftController->learnDMPFromFiles(fileNames);
    fileNames = in.getRightArmMotionFile();
    rightController->learnDMPFromFiles(fileNames);

    RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

    float leftThumbValue = robot->getRobotNode("LeftHandThumb")->getJointValue();
    float rightThumbValue = robot->getRobotNode("RightHandThumb")->getJointValue();

    ARMARX_IMPORTANT << "rightThumbValue: " << rightThumbValue;
    ARMARX_IMPORTANT << "leftThumbValue: " << leftThumbValue;

    Eigen::Vector3f leftHandAdjVec = in.getLeftHandAdjVec()->toEigen();
    Eigen::Vector3f rightHandAdjVec = in.getRightHandAdjVec()->toEigen();
    Eigen::Vector3f leftHandOffsetVec = in.getHandReturnOffsetVecLeft()->toEigen();
    Eigen::Vector3f rightHandOffsetVec = in.getHandReturnOffsetVecRight()->toEigen();

    float thumbThresholdLeft = in.getLeftThumbThreshold();
    float thumbThresholdRight = in.getRightThumbThreshold();

    ARMARX_INFO << "thumb left in failure detection: " << thumbDFLeft->getDataField()->getFloat() << "| the threshold is: " << thumbThresholdLeft;
    leftHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
    rightHandcontroller->setTargetsWithPwm(0, 2, 1, 1);

    usleep(1000000);
    bool speechOutputAfterWait = true;
    while (thumbDFLeft->getDataField()->getFloat() > thumbThresholdLeft)
    {
        ARMARX_IMPORTANT << thumbDFLeft->getDataField()->getFloat() << " > " << thumbThresholdLeft
                         << " -> retrying to grasp board with left hand";
        if (speechOutputAfterWait)
        {
            getTextToSpeech()->reportText(in.getTextForFailureRecovery());
            speechOutputAfterWait = false;
        }
        leftController->activateController();
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        Eigen::Matrix4f targetPose = robot->getRobotNodeSet("LeftArm")->getTCP()->getPoseInRootFrame();
        Eigen::Matrix4f origPose = targetPose;
        ARMARX_INFO << "move before: " << targetPose;
        targetPose(0, 3) += leftHandAdjVec(0);
        targetPose(1, 3) += leftHandAdjVec(1);
        targetPose(2, 3) += leftHandAdjVec(2);
        leftHandcontroller->setTargets(0, 0);
        usleep(1000000);

        leftController->runDMP(Helpers::pose2dvec(targetPose));

        while (!isRunningTaskStopped() && !leftController->isFinished())
        {
            usleep(1000);
        }

        leftController->stopDMP();

        // re-calibrate thumb
        {
            leftHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
            usleep(in.getThumbCalibrationTime() * 1000);
            RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
            float thumbLeftClose = thumbDFLeft->getDataField()->getFloat();
            thumbThresholdLeft = thumbLeftClose - 0.1;
            leftHandcontroller->setTargetsWithPwm(0, 0, 1, 1);
            usleep(in.getThumbCalibrationTime() * 1000);
        }

        targetPose(0, 3) = origPose(0, 3) - leftHandOffsetVec(0);
        targetPose(1, 3) = origPose(1, 3) - leftHandOffsetVec(1);
        targetPose(2, 3) = origPose(2, 3) - leftHandOffsetVec(2);

        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        if (in.getIsVisionUsed())
        {
            Eigen::Matrix4f objectCurrentPose = objectInstance->getPose()->toRootEigen(robot);
            //            targetPose.block<3, 3>(0, 0) = leftHandDefaultOri * objectDefaultPose.block<3, 3>(0, 0).transpose() * objectCurrentPose.block<3, 3>(0, 0);
            targetPose.block<3, 3>(0, 0) = objectCurrentPose.block<3, 3>(0, 0) * objectDefaultPose.block<3, 3>(0, 0).transpose() * leftHandDefaultOri;
            targetPose.block<3, 1>(0, 3) = objectCurrentPose.block<3, 3>(0, 0) * leftHandObjOffsetVecLocal + objectCurrentPose.block<3, 1>(0, 3);
        }

        leftController->runDMP(Helpers::pose2dvec(targetPose));

        float forceIntegral = 0;
        IceUtil::Time forceTimer = IceUtil::Time::now();
        while (!isRunningTaskStopped())
        {
            Eigen::Vector3f currentForceVec;
            RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
            {
                FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
                currentForceVec = forcePtr->toRootEigen(robot) - initialForceLeft;
            }

            float currentForce = currentForceVec.dot(in.getCloseLeftHandOri()->toEigen());
            double deltaT = (IceUtil::Time::now() - forceTimer).toSecondsDouble();
            if (currentForce > 5)
            {
                forceIntegral += deltaT * currentForce;
            }
            forceTimer = IceUtil::Time::now();

            if (forceIntegral > in.getCloseLeftHandThreshold() || leftController->isFinished())
            {
                break;
            }
            usleep(1000);
        }
        ARMARX_INFO << "move after: " <<  robot->getRobotNodeSet("LeftArm")->getTCP()->getPoseInRootFrame();

        leftHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
        usleep(1000000);
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

        leftController->runDMPWithTime(Helpers::pose2dvec(origPose), 2);
        while (!isRunningTaskStopped() && !leftController->isFinished())
        {
            usleep(1000);
        }
        leftController->stopDMP();
    }
    ARMARX_INFO << "thumb right in failure detection: " << thumbDFRight->getDataField()->getFloat() << " the threshold is: " << thumbThresholdRight;
    while (thumbDFRight->getDataField()->getFloat() > thumbThresholdRight)
    {
        ARMARX_IMPORTANT << thumbDFRight->getDataField()->getFloat() << " > " << thumbThresholdRight
                         << " -> retrying to grasp board with right hand";
        if (speechOutputAfterWait)
        {
            getTextToSpeech()->reportText(in.getTextForFailureRecovery());
            speechOutputAfterWait = false;
        }
        rightController->activateController();
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        Eigen::Matrix4f targetPose = robot->getRobotNodeSet("RightArm")->getTCP()->getPoseInRootFrame();
        Eigen::Matrix4f origPose = targetPose;
        targetPose(0, 3) += rightHandAdjVec(0);
        targetPose(1, 3) += rightHandAdjVec(1);
        targetPose(2, 3) += rightHandAdjVec(2);
        rightHandcontroller->setTargets(0, 0);
        usleep(1000000);

        rightController->runDMP(Helpers::pose2dvec(targetPose));

        while (!isRunningTaskStopped() && !rightController->isFinished())
        {
            usleep(1000);
        }


        rightController->stopDMP();

        // re-calibrate thumb
        {
            rightHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
            usleep(in.getThumbCalibrationTime() * 1000);
            RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
            float thumbRightClose = thumbDFRight->getDataField()->getFloat();
            thumbThresholdRight = thumbRightClose - 0.1;
            rightHandcontroller->setTargetsWithPwm(0, 0, 1, 1);
            usleep(in.getThumbCalibrationTime() * 1000);
        }


        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        {
            FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
            initialForceRight = forcePtr->toRootEigen(robot);
        }

        targetPose(0, 3) = origPose(0, 3) - rightHandOffsetVec(0);
        targetPose(1, 3) = origPose(1, 3) - rightHandOffsetVec(1);
        targetPose(2, 3) = origPose(2, 3) - rightHandOffsetVec(2);
        ARMARX_INFO << "targetPose for failure recovery: \n" << targetPose;

        if (in.getIsVisionUsed())
        {
            Eigen::Matrix4f objectCurrentPose = objectInstance->getPose()->toRootEigen(robot);
            targetPose.block<3, 3>(0, 0) = objectCurrentPose.block<3, 3>(0, 0) * objectDefaultPose.block<3, 3>(0, 0).transpose() * rightHandDefaultOri;
            targetPose.block<3, 1>(0, 3) = objectCurrentPose.block<3, 3>(0, 0) * rightHandObjOffsetVecLocal + objectCurrentPose.block<3, 1>(0, 3);
            ARMARX_INFO << "rightHandObjOffsetVecLocal: " << rightHandObjOffsetVecLocal << " objectCurrentPose: " << objectCurrentPose.block<3, 1>(0, 3);
            ARMARX_INFO << "targetPose: " << targetPose;
        }

        // (Guess:) Move hand forward until contact with board.
        rightController->runDMP(Helpers::pose2dvec(targetPose));

        float forceIntegral = 0;
        IceUtil::Time forceTimer = IceUtil::Time::now();
        while (!isRunningTaskStopped())
        {
            Eigen::Vector3f currentForceVec;
            RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
            {
                FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
                currentForceVec = forcePtr->toRootEigen(robot) - initialForceRight;
            }

            float currentForce = currentForceVec.dot(in.getCloseRightHandOri()->toEigen());
            double deltaT = (IceUtil::Time::now() - forceTimer).toSecondsDouble();
            if (currentForce > 5)
            {
                forceIntegral += deltaT * currentForce;
            }
            forceTimer = IceUtil::Time::now();

            if (forceIntegral > in.getCloseRightHandThreshold() || rightController->isFinished())
            {
                //                ARMARX_INFO << VAROUT(forceIntegral);
                break;
            }
            usleep(1000);
        }
        rightHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
        usleep(1000000);
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        rightController->runDMPWithTime(Helpers::pose2dvec(origPose), 2);
        while (!isRunningTaskStopped() && !rightController->isFinished())
        {
            usleep(1000);
        }
        rightController->stopDMP();
    }

    ARMARX_INFO << "Calibrated thumb thresholds after regrasping: "
                << "\nLeft  = \t" << thumbThresholdLeft
                << "\nRight = \t" << thumbThresholdRight
               ;

    leftHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
    rightHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
    leftController->deactivateController();
    rightController->deactivateController();

    while (leftController->isControllerActive() || rightController->isControllerActive())
    {
        usleep(10000);
    }

    leftController->deleteController();
    rightController->deleteController();
    usleep(100000);

    out.setLeftThumbThreshold(thumbThresholdLeft);
    out.setRightThumbThreshold(thumbThresholdRight);

    emitSuccess();
}

//void RegraspingFailureDetection::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void RegraspingFailureDetection::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr RegraspingFailureDetection::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new RegraspingFailureDetection(stateData));
}

