/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     JeffGao ( jianfenggaobit at gmail dot com )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DummyStateWithContinueButton.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXGui/libraries/RemoteGui/RemoteGui.h>

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
DummyStateWithContinueButton::SubClassRegistry DummyStateWithContinueButton::Registry(DummyStateWithContinueButton::GetName(), &DummyStateWithContinueButton::CreateInstance);



void DummyStateWithContinueButton::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void DummyStateWithContinueButton::run()
{

    RemoteGui::detail::VBoxLayoutBuilder rootLayoutBuilder = RemoteGui::makeVBoxLayout();
    rootLayoutBuilder
    .addChild(RemoteGui::makeHBoxLayout()
              .addChild(RemoteGui::makeButton("Continue").label("Continue")));

    getRemoteGui()->createTab("RecordingAndTesting", rootLayoutBuilder);
    RemoteGui::TabProxy guiTab = RemoteGui::TabProxy(getRemoteGui(), "RecordingAndTesting");
    int lastContinueClickCount = guiTab.getValue<int>("Continue").get();

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        guiTab.receiveUpdates();
        int continueClickCount = guiTab.getValue<int>("Continue").get();
        bool continueClicked = continueClickCount > lastContinueClickCount;
        lastContinueClickCount = continueClickCount;
        if (continueClicked)
        {
            ARMARX_INFO << "Start Demo ";
            break;
        }
        ARMARX_INFO << "Waiting ... ";

        usleep(1000);
    }

    emitSuccess();
}

//void DummyStateWithContinueButton::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void DummyStateWithContinueButton::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr DummyStateWithContinueButton::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new DummyStateWithContinueButton(stateData));
}
