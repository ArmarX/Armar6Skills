/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ForceTouch.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityController.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <RobotAPI/libraries/core/CartesianPositionController.h>
#include <RobotAPI/libraries/core/math/MathUtils.h>
#include <RobotAPI/libraries/core/FramedPose.h>

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
ForceTouch::SubClassRegistry ForceTouch::Registry(ForceTouch::GetName(), &ForceTouch::CreateInstance);



void ForceTouch::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void ForceTouch::run()
{
    VirtualRobot::RobotPtr localRobot = getLocalRobot();

    NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController("leftTCPController");
    if (controller)
    {
        if (controller->isControllerActive())
        {
            controller->deactivateController();
            while (controller->isControllerActive())
            {
                TimeUtil::SleepMS(10);
            }
        }
        controller->deleteController();
        TimeUtil::SleepMS(10);
    }

    controller = getRobotUnit()->getNJointController("rightTCPController");
    if (controller)
    {
        if (controller->isControllerActive())
        {
            controller->deactivateController();
            while (controller->isControllerActive())
            {
                TimeUtil::SleepMS(10);
            }
        }
        controller->deleteController();
        TimeUtil::SleepMS(10);
    }

    DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT"));
    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT"));

    Eigen::Vector3f initialForceLeft;
    Eigen::Matrix4f targetLeftPose;

    VirtualRobot::RobotNodePtr tcpLeft;
    {
        VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet("LeftArm");
        tcpLeft = rns->getTCP();
        targetLeftPose = tcpLeft->getPoseInRootFrame();
        targetLeftPose.block<3, 1>(0, 3) += in.getRelativeTargetLeft()->toEigen();
    }
    CartesianPositionController leftPosController(tcpLeft);
    leftPosController.KpPos = in.getControllerKp();
    leftPosController.KpOri = 0;

    leftPosController.maxPosVel = in.getMaxVelocity();




    Eigen::Vector3f initialForceRight;

    Eigen::Matrix4f targetRightPose;
    VirtualRobot::RobotNodePtr tcpRight;
    {
        VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet("RightArm");
        tcpRight = rns->getTCP();
        targetRightPose = tcpRight->getPoseInRootFrame();
        targetRightPose.block<3, 1>(0, 3) += in.getRelativeTargetRight()->toEigen();
    }
    CartesianPositionController rightPosController(tcpRight);
    rightPosController.KpPos = in.getControllerKp();
    rightPosController.KpOri = 0;
    rightPosController.maxPosVel = in.getMaxVelocity();



    NJointCartesianVelocityControllerConfigPtr config = new NJointCartesianVelocityControllerConfig("LeftArm", "", NJointCartesianVelocityControllerMode::eAll);
    NJointCartesianVelocityControllerInterfacePrx leftController = NJointCartesianVelocityControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointCartesianVelocityController", "leftTCPController", config));
    leftController->activateController();

    config->nodeSetName = "RightArm";
    NJointCartesianVelocityControllerInterfacePrx rightController = NJointCartesianVelocityControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointCartesianVelocityController", "rightTCPController", config));
    rightController->activateController();
    IceUtil::Time start = TimeUtil::GetTime();



    float forceThreshold = in.getForceThreshold();
    float KpAvoidJointLimits = 0;

    float minExecutionTime = in.getMinExecutionTime();
    bool rightHandFinished = false;
    bool leftHandFinished = false;



    std::string layerName = "guard_layer";
    std::string boxName = "guard";
    Eigen::Vector3f dimensions;
    dimensions << 805, 1205, 20;
    DrawColor color = {0.5, 0.5, 0.5, 1};

    Eigen::Matrix3f rotation_x = Eigen::Matrix3f::Zero();
    Eigen::Matrix3f rotation_y = Eigen::Matrix3f::Zero();
    rotation_y(0, 2) = -1;
    rotation_y(2, 0) = 1;
    rotation_y(1, 1) = 1;

    rotation_x(0, 0) = 1;
    rotation_x(1, 1) = 0.866;
    rotation_x(1, 2) = 0.5;
    rotation_x(2, 1) = -0.5;
    rotation_x(2, 2) = 0.866;

    Eigen::Matrix4f guardLocal = Eigen::Matrix4f::Identity();
    guardLocal.block<3, 3>(0, 0) =  rotation_x * rotation_y;
    guardLocal(0, 3) += 0;
    guardLocal(2, 3) += 465;

    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

        if (in.isShowGuardSet() && in.getShowGuard())
        {

            // board debug drawer
            Eigen::Matrix4f globalPose = localRobot->getRobotNodeSet("RightArm")->getTCP()->toGlobalCoordinateSystem(guardLocal);
            getDebugDrawerTopic()->setBoxVisu(layerName, boxName, new Pose(globalPose), new Vector3(dimensions), color);
        }

        IceUtil::Time now = TimeUtil::GetTime();
        float t = (now - start).toSecondsDouble();

        if (t < minExecutionTime)
        {
            {
                FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
                initialForceRight = forcePtr->toRootEigen(localRobot);
            }


            {
                FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
                initialForceLeft = forcePtr->toRootEigen(localRobot);
            }

            continue;
        }
        Eigen::Vector3f leftLinearVel;
        Eigen::Vector3f rightLinearVel;

        Eigen::Vector3f forceLeft;
        {
            FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
            forceLeft = forcePtr->toRootEigen(localRobot);
            forceLeft -= initialForceLeft;
        }

        Eigen::Vector3f forceRight;
        {
            FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
            forceRight = forcePtr->toRootEigen(localRobot);
            forceRight -= initialForceRight;
        }


        Eigen::Vector3f forceLeftDir = in.getLeftForceDir()->toEigen();
        float forceLeftMagnitude = forceLeft.dot(forceLeftDir);
        if (in.getIsLeftIgnored() || (forceLeftMagnitude > forceThreshold))
        {

            leftHandFinished = true;
            leftLinearVel.setZero();
        }
        else
        {
            Eigen::VectorXf cartesianVelocity = leftPosController.calculate(targetLeftPose, VirtualRobot::IKSolver::CartesianSelection::All);
            leftLinearVel << cartesianVelocity(0), cartesianVelocity(1), cartesianVelocity(2);
        }

        if (leftController->isControllerActive())
        {
            leftController->setControllerTarget(
                leftLinearVel(0), leftLinearVel(1), leftLinearVel(2),
                0, 0, 0,
                KpAvoidJointLimits, NJointCartesianVelocityControllerMode::eAll);
        }

        Eigen::Vector3f forceRightDir = in.getRightForceDir()->toEigen();
        float forceRightMagnitude = forceRight.dot(forceRightDir);

        if (in.getIsRightIgnored() || (forceRightMagnitude > forceThreshold))
        {

            rightHandFinished = true;
            rightLinearVel.setZero();
        }
        else
        {
            Eigen::VectorXf cartesianVelocity = rightPosController.calculate(targetRightPose, VirtualRobot::IKSolver::CartesianSelection::All);
            rightLinearVel << cartesianVelocity(0), cartesianVelocity(1), cartesianVelocity(2);
        }
        ARMARX_INFO << deactivateSpam(0.5) << VAROUT(forceLeftMagnitude) << VAROUT(forceRightMagnitude) << VAROUT(leftHandFinished) << VAROUT(rightHandFinished);

        if (rightController->isControllerActive())
        {
            rightController->setControllerTarget(
                rightLinearVel(0), rightLinearVel(1), rightLinearVel(2),
                0, 0, 0,
                KpAvoidJointLimits, NJointCartesianVelocityControllerMode::eAll);
        }

        getDebugObserver()->setDebugDatafield("ForceTouch", "velleft_x", new Variant(leftLinearVel(0)));
        getDebugObserver()->setDebugDatafield("ForceTouch", "velleft_y", new Variant(leftLinearVel(1)));
        getDebugObserver()->setDebugDatafield("ForceTouch", "velleft_z", new Variant(leftLinearVel(2)));
        getDebugObserver()->setDebugDatafield("ForceTouch", "velright_x", new Variant(rightLinearVel(0)));
        getDebugObserver()->setDebugDatafield("ForceTouch", "velright_y", new Variant(rightLinearVel(1)));
        getDebugObserver()->setDebugDatafield("ForceTouch", "velright_z", new Variant(rightLinearVel(2)));

        getDebugObserver()->setDebugDatafield("ForceTouch", "rightForceMag", new Variant(forceRightMagnitude));
        getDebugObserver()->setDebugDatafield("ForceTouch", "leftForceMag", new Variant(forceLeftMagnitude));

        if (leftHandFinished && rightHandFinished)
        {
            break;
        }

        usleep(10000);
    }


    if (in.getGoDownAfterTouch())
    {
        CartesianPositionController posControllerLeft(tcpLeft);
        CartesianPositionController posControllerRight(tcpRight);
        Eigen::Matrix4f targetPoseLeft = tcpLeft->getPoseInRootFrame();
        targetPoseLeft(2, 3) -= 20;
        Eigen::Matrix4f targetPoseRight = tcpRight->getPoseInRootFrame();
        targetPoseRight(2, 3) -= 20;
        while (!isRunningTaskStopped())
        {

            RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
            if (in.isShowGuardSet() && in.getShowGuard())
            {

                // board debug drawer
                Eigen::Matrix4f globalPose = localRobot->getRobotNodeSet("RightArm")->getTCP()->toGlobalCoordinateSystem(guardLocal);
                getDebugDrawerTopic()->setBoxVisu(layerName, boxName, new Pose(globalPose), new Vector3(dimensions), color);
            }
            {
                Eigen::VectorXf targetVelLeft = posControllerLeft.calculate(targetPoseLeft,
                                                VirtualRobot::IKSolver::All);
                leftController->setControllerTarget(targetVelLeft(0),
                                                    targetVelLeft(1),
                                                    targetVelLeft(2),
                                                    0,
                                                    0,
                                                    0, KpAvoidJointLimits, NJointCartesianVelocityControllerMode::eAll);
            }
            {
                Eigen::VectorXf targetVelRight = posControllerRight.calculate(targetPoseRight,
                                                 VirtualRobot::IKSolver::All);
                rightController->setControllerTarget(targetVelRight(0),
                                                     targetVelRight(1),
                                                     targetVelRight(2),
                                                     0,
                                                     0,
                                                     0, KpAvoidJointLimits, NJointCartesianVelocityControllerMode::eAll);
            }
            ARMARX_INFO << deactivateSpam(1) << " dist left: " << posControllerLeft.getPositionError(targetPoseLeft) << " dist right: " << posControllerRight.getPositionError(targetPoseRight);
            if (posControllerLeft.getPositionError(targetPoseLeft) < 1 && posControllerRight.getPositionError(targetPoseRight) < 1)
            {
                rightController->setControllerTarget(0,
                                                     0,
                                                     0,
                                                     0,
                                                     0,
                                                     0, KpAvoidJointLimits, NJointCartesianVelocityControllerMode::eAll);
                leftController->setControllerTarget(0,
                                                    0,
                                                    0,
                                                    0,
                                                    0,
                                                    0, KpAvoidJointLimits, NJointCartesianVelocityControllerMode::eAll);
                ARMARX_INFO << "target reached";
                break;
            }

        }
    }

    if (leftController->isControllerActive())
    {
        leftController->deactivateController();
    }

    if (rightController->isControllerActive())
    {
        rightController->deactivateController();
    }

    while (leftController->isControllerActive() || rightController->isControllerActive())
    {
        usleep(10000);
    }

    leftController->deleteController();
    rightController->deleteController();




    emitSuccess();
}

//void ForceTouch::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ForceTouch::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ForceTouch::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ForceTouch(stateData));
}
