/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     SecondHands Demo ( shdemo at armar6 )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GuardLocalizer.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
//#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
//#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
//#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <ArmarXCore/observers/variant/ChannelRef.h>
#include <RobotAPI/interface/components/ViewSelectionInterface.h>

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
GuardLocalizer::SubClassRegistry GuardLocalizer::Registry(GuardLocalizer::GetName(), &GuardLocalizer::CreateInstance);



void GuardLocalizer::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void GuardLocalizer::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    // get a private kinematic instance for this state of the robot (tick "Robot State Component" proxy checkbox in statechart group)
    //    VirtualRobot::RobotPtr robot = getLocalRobot();
    //    int cycleTime = static_cast<int>(30);
    //    getObjectMemoryObserver()->requestObjectClassRepeated("guard", cycleTime, armarx::DEFAULT_VIEWTARGET_PRIORITY);
    //    std::vector<ChannelRefBasePtr> objectInstanceChannelList = getObjectMemoryObserver()->getObjectInstancesByClass("guard");
    //    memoryx::ObjectInstanceMemorySegmentBasePrx objInstanceSeg = getWorkingMemory()->getObjectInstancesSegment();


    //    while (objectInstanceChannelList.size() < 1)
    //    {
    //        objectInstanceChannelList = getObjectMemoryObserver()->getObjectInstancesByClass("guard");
    //        usleep(1000000);
    //    }

    //    ChannelRefPtr channelRef = ChannelRefPtr::dynamicCast(objectInstanceChannelList.at(0));
    //    std::string instanceId = channelRef->getDataField("id")->getString();
    //    memoryx::ObjectInstancePtr objectInstance =  memoryx::ObjectInstancePtr::dynamicCast(objInstanceSeg->getObjectInstanceById(instanceId));

    //    RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    //    Eigen::Matrix4f objInRoot = objectInstance->getPose()->toRootEigen(robot);
    //    ARMARX_INFO << "objInRoot: " << objInRoot;

    //    Eigen::Matrix4f leftHandPose = robot->getRobotNodeSet("LeftArm")->getTCP()->getPoseInRootFrame();
    //    ARMARX_INFO << "leftHandPose: " << leftHandPose;

    emitFailure();
}

//void GuardLocalizer::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GuardLocalizer::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GuardLocalizer::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GuardLocalizer(stateData));
}
