/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BoardPlacement.h"

#include <SimoxUtility/math/convert.h>
//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <RobotAPI/interface/units/RobotUnit/TaskSpaceActiveImpedanceControl.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <armar6/skills/statecharts/Armar6BoardSupportDemo/Armar6BoardSupportDemoStatechartContext.generated.h>
#include "Helpers.h"
#include <ArmarXCore/core/time/TimeUtil.h>
#include <RobotAPI/libraries/core/FramedPose.h>

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
BoardPlacement::SubClassRegistry BoardPlacement::Registry(BoardPlacement::GetName(), &BoardPlacement::CreateInstance);



void BoardPlacement::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void BoardPlacement::run()
{
    VirtualRobot::RobotPtr localRobot = getLocalRobot();

    std::string forceSensorName;
    std::string handControllerName;
    std::string handName = in.getKinematicChainName();
    if (in.getKinematicChainName() == "RightArm")
    {
        forceSensorName = "FT R_ArmR_FT";
        handControllerName = "Hand_R_EEF_NJointKITHandV2ShapeController";
    }
    else if (in.getKinematicChainName() == "LeftArm")
    {
        forceSensorName = "FT L_ArmL_FT";
        handControllerName = "Hand_L_EEF_NJointKITHandV2ShapeController";
    }
    else
    {
        ARMARX_ERROR << "Kinematic Chain should be RightArm or LeftArm ... ";
        emitFailure();
    }




    Eigen::Matrix4f desiredPose;
    if (in.isDesiredPoseSet())
    {
        desiredPose = in.getDesiredPose()->toEigen();
    }
    else
    {
        desiredPose = getRobot()->getRobotNodeSet(in.getKinematicChainName())->getTCP()->getPoseInRootFrame();
    }



    const std::vector<float> Kpos = in.getKpos();
    const std::vector<float> Kori = in.getKori();
    const std::vector<float> Dpos = in.getDpos();
    const std::vector<float> Dori = in.getDori();

    std::vector<float> desiredJointPosition;
    if (in.isDesiredJointValuesSet())
    {
        desiredJointPosition = in.getDesiredJointValues();
    }
    else
    {
        desiredJointPosition = localRobot->getRobotNodeSet(in.getKinematicChainName())->getJointValues();

    }

    float knullval = in.getKnull();
    float dnullval = in.getDnull();

    std::vector<float> knull;
    std::vector<float> dnull;

    for (size_t i = 0; i < 8; ++i)
    {
        knull.push_back(knullval);
        dnull.push_back(dnullval);
    }

    NJointTaskSpaceImpedanceControlConfigPtr impConfig = new NJointTaskSpaceImpedanceControlConfig;
    impConfig->nodeSetName = in.getKinematicChainName();
    impConfig->desiredPosition = simox::math::mat4f_to_pos(desiredPose);
    impConfig->desiredOrientation = simox::math::mat4f_to_quat(desiredPose);
    impConfig->Kpos = Eigen::Map<const Eigen::Vector3f>(Kpos.data(), Kpos.size());
    impConfig->Kori = Eigen::Map<const Eigen::Vector3f>(Kori.data(), Kori.size());
    impConfig->Dpos = Eigen::Map<const Eigen::Vector3f>(Dpos.data(), Dpos.size());
    impConfig->Dori = Eigen::Map<const Eigen::Vector3f>(Dori.data(), Dori.size());
    impConfig->desiredJointPositions = Eigen::Map<const Eigen::VectorXf>(desiredJointPosition.data(), desiredJointPosition.size());
    impConfig->Knull = Eigen::Map<const Eigen::VectorXf>(knull.data(), knull.size());
    impConfig->Dnull = Eigen::Map<const Eigen::VectorXf>(dnull.data(), dnull.size());
    impConfig->torqueLimit = in.getTorqueLimit();

    NJointTaskSpaceImpedanceControlInterfacePrx impController
        = NJointTaskSpaceImpedanceControlInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceController", "impController", impConfig));


    impController->activateController();


    DatafieldRefPtr forceDf = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield(forceSensorName));


    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx handController = StateBase::getContext<Armar6BoardSupportDemoStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>(handControllerName);

    handController->setTargetsWithPwm(2, 2, 1, 1);
    IceUtil::Time start = TimeUtil::GetTime();

    float minExecutionTime = 1;
    Eigen::Vector3f initialForce;
    initialForce.setZero();

    //    VirtualRobot::RobotPtr localRobot = getLocalRobot();
    std::string layerName = "guard_layer";
    std::string boxName = "guard";
    Eigen::Vector3f dimensions;
    dimensions << 805, 1205, 20;
    DrawColor color = {0.5, 0.5, 0.5, 1};

    Eigen::Matrix3f rotation_x = Eigen::Matrix3f::Zero();
    Eigen::Matrix3f rotation_y = Eigen::Matrix3f::Zero();
    rotation_y(0, 2) = -1;
    rotation_y(2, 0) = 1;
    rotation_y(1, 1) = 1;

    rotation_x(0, 0) = 1;
    rotation_x(1, 1) = 0.866;
    rotation_x(1, 2) = 0.5;
    rotation_x(2, 1) = -0.5;
    rotation_x(2, 2) = 0.866;

    Eigen::Matrix4f guardLocal = Eigen::Matrix4f::Identity();
    guardLocal.block<3, 3>(0, 0) =  rotation_x * rotation_y;
    guardLocal(0, 3) += 0;
    guardLocal(2, 3) += 465;

    IceUtil::Time timer_begin = TimeUtil::GetTime();

    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

        if (in.isShowGuardSet() && in.getShowGuard())
        {

            // board debug drawer
            Eigen::Matrix4f globalPose = localRobot->getRobotNodeSet("RightArm")->getTCP()->toGlobalCoordinateSystem(guardLocal);
            getDebugDrawerTopic()->setBoxVisu(layerName, boxName, new Pose(globalPose), new Vector3(dimensions), color);
        }

        IceUtil::Time now = TimeUtil::GetTime();
        float t = (now - start).toSecondsDouble();

        if (t < minExecutionTime)
        {
            {
                FramedDirectionPtr forcePtr = forceDf->getDataField()->get<FramedDirection>();
                initialForce = forcePtr->toRootEigen(localRobot);
            }
            continue;
        }
        Eigen::Vector3f currentForce;
        {
            FramedDirectionPtr forcePtr = forceDf->getDataField()->get<FramedDirection>();
            currentForce = forcePtr->toRootEigen(localRobot);
            currentForce -= initialForce;
        }


        getDebugObserver()->setDebugDatafield("BoardPlacement", "force_x", new Variant(currentForce(0)));
        getDebugObserver()->setDebugDatafield("BoardPlacement", "force_y", new Variant(currentForce(1)));
        getDebugObserver()->setDebugDatafield("BoardPlacement", "force_z", new Variant(currentForce(2)));

        if (fabs(currentForce(2)) > in.getForceThreshold())
        {
            IceUtil::Time timer_end = TimeUtil::GetTime();
            float reactionTime = (timer_end - timer_begin).toSecondsDouble();

            if (reactionTime > in.getReactionTime())
            {
                break;
            }
        }
        else
        {
            timer_begin = TimeUtil::GetTime();

        }
    }

    //    handController->setTargets(0, 0);
    impController->deactivateController();
    while (impController->isControllerActive())
    {
        usleep(10000);
    }
    impController->deleteController();


    getTextToSpeech()->reportText(in.getTextPlacementDone());
    emitSuccess();

}

//void BoardPlacement::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void BoardPlacement::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr BoardPlacement::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new BoardPlacement(stateData));
}
