/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <SimoxUtility/math/convert/mat4f_to_pos.h>
#include <SimoxUtility/math/convert/mat4f_to_quat.h>

#include "GuardTransportationWithObstacleAvoidance.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <RobotAPI/interface/units/RobotUnit/TaskSpaceActiveImpedanceControl.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <armar6/skills/statecharts/Armar6BoardSupportDemo/Armar6BoardSupportDemoStatechartContext.generated.h>

#include <RobotAPI/libraries/core/math/MathUtils.h>
#include <VirtualRobot/VirtualRobot.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include "Helpers.h"
#include <RobotAPI/interface/components/ObstacleAvoidance/ObstacleDetectionInterface.h>
//#include <armar6/skills/components/ObstacleAvoidance/ObstacleAvoidanceDrawFunctions.h>
//#include <RobotAPI/interface/components/ObstacleAvoidance/DSObstacleAvoidanceInterface.h>
#include <RobotAPI/libraries/core/FramedPose.h>

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
GuardTransportationWithObstacleAvoidance::SubClassRegistry GuardTransportationWithObstacleAvoidance::Registry(GuardTransportationWithObstacleAvoidance::GetName(), &GuardTransportationWithObstacleAvoidance::CreateInstance);



void GuardTransportationWithObstacleAvoidance::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void GuardTransportationWithObstacleAvoidance::run()
{
    VirtualRobot::RobotPtr localRobot = getLocalRobot();

    Eigen::Matrix4f desiredPoseLeft;
    if (in.isDesiredPoseLeftSet())
    {
        desiredPoseLeft = in.getDesiredPoseLeft()->toEigen();
    }
    else
    {
        desiredPoseLeft = localRobot->getRobotNodeSet("LeftArm")->getTCP()->getPoseInRootFrame();
    }

    Eigen::Matrix4f desiredPoseRight;
    if (in.isDesiredPoseRightSet())
    {
        desiredPoseRight = in.getDesiredPoseRight()->toEigen();
    }
    else
    {
        desiredPoseRight = localRobot->getRobotNodeSet("RightArm")->getTCP()->getPoseInRootFrame();
    }


    const std::vector<float> Kpos = in.getKpos();
    const std::vector<float> Kori = in.getKori();
    const std::vector<float> Dpos = in.getDpos();
    const std::vector<float> Dori = in.getDori();
    VirtualRobot::RobotNodePtr tcpRight = localRobot->getRobotNodeSet("RightArm")->getTCP();
    VirtualRobot::RobotNodePtr tcpLeft = localRobot->getRobotNodeSet("LeftArm")->getTCP();

    std::vector<float> desiredJointPositionLeft;
    if (in.isDesiredLeftJointValuesSet())
    {
        desiredJointPositionLeft = in.getDesiredLeftJointValues();
    }
    else
    {
        desiredJointPositionLeft = localRobot->getRobotNodeSet("LeftArm")->getJointValues();

    }

    std::vector<float> desiredJointPositionRight;
    if (in.isDesiredRightJointValuesSet())
    {
        desiredJointPositionRight = in.getDesiredRightJointValues();
    }
    else
    {
        desiredJointPositionRight = localRobot->getRobotNodeSet("RightArm")->getJointValues();

    }

    float knullval = in.getKnull();
    float dnullval = in.getDnull();

    std::vector<float> knull;
    std::vector<float> dnull;

    for (size_t i = 0; i < 8; ++i)
    {
        knull.push_back(knullval);
        dnull.push_back(dnullval);
    }

    NJointTaskSpaceImpedanceControlConfigPtr leftConfig = new NJointTaskSpaceImpedanceControlConfig;
    leftConfig->nodeSetName = "LeftArm";
    leftConfig->desiredPosition = simox::math::mat4f_to_pos(desiredPoseLeft);
    leftConfig->desiredOrientation = simox::math::mat4f_to_quat(desiredPoseLeft);
    leftConfig->Kpos = Eigen::Map<const Eigen::Vector3f>(Kpos.data(), Kpos.size());
    leftConfig->Kori = Eigen::Map<const Eigen::Vector3f>(Kori.data(), Kori.size());
    leftConfig->Dpos = Eigen::Map<const Eigen::Vector3f>(Dpos.data(), Dpos.size());
    leftConfig->Dori = Eigen::Map<const Eigen::Vector3f>(Dori.data(), Dori.size());
    leftConfig->desiredJointPositions = Eigen::Map<const Eigen::VectorXf>(desiredJointPositionLeft.data(), desiredJointPositionLeft.size());
    leftConfig->Knull = Eigen::Map<const Eigen::VectorXf>(knull.data(), knull.size());
    leftConfig->Dnull = Eigen::Map<const Eigen::VectorXf>(dnull.data(), dnull.size());
    leftConfig->torqueLimit = in.getTorqueLimit();

    NJointTaskSpaceImpedanceControlConfigPtr rightConfig = new NJointTaskSpaceImpedanceControlConfig;
    rightConfig->nodeSetName = "RightArm";
    rightConfig->desiredPosition = simox::math::mat4f_to_pos(desiredPoseRight);
    rightConfig->desiredOrientation = simox::math::mat4f_to_quat(desiredPoseRight);
    rightConfig->Kpos = Eigen::Map<const Eigen::Vector3f>(Kpos.data(), Kpos.size());
    rightConfig->Kori = Eigen::Map<const Eigen::Vector3f>(Kori.data(), Kori.size());
    rightConfig->Dpos = Eigen::Map<const Eigen::Vector3f>(Dpos.data(), Dpos.size());
    rightConfig->Dori = Eigen::Map<const Eigen::Vector3f>(Dori.data(), Dori.size());
    rightConfig->desiredJointPositions = Eigen::Map<const Eigen::VectorXf>(desiredJointPositionRight.data(), desiredJointPositionRight.size());
    rightConfig->Knull = Eigen::Map<const Eigen::VectorXf>(knull.data(), knull.size());
    rightConfig->Dnull = Eigen::Map<const Eigen::VectorXf>(dnull.data(), dnull.size());
    rightConfig->torqueLimit = in.getTorqueLimit();

    NJointTaskSpaceImpedanceControlInterfacePrx leftController
        = NJointTaskSpaceImpedanceControlInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceController", "LeftController", leftConfig));
    NJointTaskSpaceImpedanceControlInterfacePrx rightController
        = NJointTaskSpaceImpedanceControlInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceController", "RightController", rightConfig));
    leftController->activateController();
    rightController->activateController();

    float Kplatform = in.getKplatform();
    float Dplatform = in.getDplatform();
    float maxVelocity = in.getMaxVelocity();
    float KplatformAngle = in.getKplatformAngle();
    float DplatformAngle = in.getDplatformAngle();
    float maxAngularVelocity = in.getMaxAngularVelocity();
    Eigen::Vector3f filteredLinearVel;
    filteredLinearVel.setZero();
    float filteredAngularVel = 0;

    obstacledetection::Obstacle obstacle;
    obstacle.name = "box";
    obstacle.posX = 3000;
    obstacle.posY = 4000;
    obstacle.posZ = 0;
    obstacle.yaw = 0;
    obstacle.axisLengthX = 520 / 2 + 100;
    obstacle.axisLengthY = 850 / 2 + 100;
    obstacle.axisLengthZ = 1120;
    obstacle.refPosX = obstacle.posX;
    obstacle.refPosY = obstacle.posY;
    obstacle.refPosZ = obstacle.posZ;
    obstacle.safetyMarginX = 200;
    obstacle.safetyMarginY = 200;
    obstacle.safetyMarginZ = 200;
    obstacle.curvatureX = 2;
    obstacle.curvatureY = 2;
    obstacle.curvatureZ = 2;

    getObstacleDetection()->updateObstacle(obstacle);
    getObstacleDetection()->updateEnvironment();
    //getObstacleAvoidance3D()->updateObstacle(obstacle);
    //getObstacleAvoidance3D()->updateEnvironment();
    DebugDrawerInterfacePrx debug = getDebugDrawerTopic();
    //clear_drawings(debug);
    usleep(100000);


    float filterTimeConstant = 0.01; //0.05;

    DatafieldRefPtr platformVelX = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformVelocity", "velocityX"));
    DatafieldRefPtr platformVelY = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformVelocity", "velocityY"));
    DatafieldRefPtr platformRotVel = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformVelocity", "velocityRotation"));
    DatafieldRefPtr platformPosX = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformPose", "positionX"));
    DatafieldRefPtr platformPosY = DatafieldRefPtr::dynamicCast(getPlatformUnitObserver()->getDatafieldRefByName("platformPose", "positionY"));
    DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT L_ArmL_FT"));
    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield("FT R_ArmR_FT"));

    Eigen::Vector3f initialPositionRight;
    Eigen::Vector3f initialPositionLeft;
    Eigen::Vector3f initialDirection;
    Eigen::Vector3f initialForceLeft;
    initialForceLeft.setZero();
    Eigen::Vector3f initialForceRight;
    initialForceRight.setZero();


    IceUtil::Time start = TimeUtil::GetTime();
    double timeDuration = 0;
    while (timeDuration < in.getMinExecutionTime())
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        IceUtil::Time now = TimeUtil::GetTime();
        timeDuration = (now - start).toSecondsDouble();
        initialPositionRight = tcpRight->getPositionInRootFrame();
        initialPositionLeft = tcpLeft->getPositionInRootFrame();
        initialDirection = initialPositionRight - initialPositionLeft;
        initialDirection.normalize();
        {
            FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
            initialForceRight = forcePtr->toRootEigen(localRobot);
        }
        {
            FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
            initialForceLeft = forcePtr->toRootEigen(localRobot);
        }
    }


    IceUtil::Time last = TimeUtil::GetTime();


    //Eigen::Vector3f forceVelFactor = in.getForceVelFactor()->toEigen();
    Eigen::Vector3f filteredForceLeft;
    filteredForceLeft.setZero();
    Eigen::Vector3f filteredForceRight;
    filteredForceRight.setZero();


    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        Eigen::Vector3f currentPositionRight = tcpRight->getPositionInRootFrame();
        Eigen::Vector3f positionDeltaRight = currentPositionRight - initialPositionRight;
        Eigen::Vector3f currentPositionLeft = tcpLeft->getPositionInRootFrame();
        Eigen::Vector3f positionDeltaLeft = currentPositionLeft - initialPositionLeft;
        Eigen::Vector3f positionDelta = (positionDeltaLeft + positionDeltaRight) / 2;

        float minPosiTolerance = in.getMinPosiTolerance();
        Eigen::Vector3f currentVel;
        currentVel << platformVelX->getDataField()->getFloat(), platformVelY->getDataField()->getFloat(), 0;
        Eigen::Vector3f velocity;
        if (positionDelta.norm() < minPosiTolerance)
        {
            velocity.setZero();
        }
        else
        {
            positionDelta = positionDelta - minPosiTolerance * positionDelta.normalized();
            velocity = positionDelta * Kplatform - currentVel * Dplatform;

        }

        IceUtil::Time now = TimeUtil::GetTime();
        double deltaT = (now - last).toSecondsDouble();
        last = now;
        float Tstar = 1 / ((filterTimeConstant / deltaT) + 1);

        if (deltaT == 0)
        {
            continue;
        }
        filteredLinearVel = Tstar * (velocity - filteredLinearVel) + filteredLinearVel;

        if (!std::isfinite(filteredLinearVel(0)))
        {
            filteredLinearVel(0) = 0;
        }
        if (!std::isfinite(filteredLinearVel(1)))
        {
            filteredLinearVel(1) = 0;
        }

        velocity = math::MathUtils::LimitTo(filteredLinearVel, maxVelocity);

        float angularVelocity ;
        Eigen::Vector3f currentDirection = currentPositionRight - currentPositionLeft;
        currentDirection.normalize();
        float cosAlpha = currentDirection.dot(initialDirection);
        float alpha = 0;
        Eigen::Vector3f directionDiff = currentDirection - initialDirection;
        if (directionDiff(1) < 0)
        {
            alpha = -acos(cosAlpha);
        }
        else
        {
            alpha = acos(cosAlpha);
        }
        float minAngleTolerance = in.getMinAngleTolerance();
        if (fabs(alpha) < minAngleTolerance)
        {
            angularVelocity = 0;
        }
        else
        {
            if (alpha > 0)
            {
                alpha -= minAngleTolerance;
            }
            else
            {
                alpha += minAngleTolerance;
            }
            angularVelocity =  KplatformAngle * alpha - DplatformAngle * platformRotVel;
        }

        filteredAngularVel = Tstar * (angularVelocity - filteredAngularVel) + filteredAngularVel;
        if (!std::isfinite(filteredAngularVel))
        {
            filteredAngularVel = 0;
        }

        angularVelocity = math::MathUtils::LimitTo(filteredAngularVel, maxAngularVelocity);


        Eigen::Vector3f currentGuardLocalPosi = (currentPositionLeft + currentPositionRight) / 2;
        currentGuardLocalPosi(1) += 0.6;
        Eigen::Vector3f currentGuardGlobalPosi = localRobot->toGlobalCoordinateSystemVec(currentGuardLocalPosi);
        currentGuardGlobalPosi(2) = 0;
        Eigen::Vector3f currentObsPosi;
        currentObsPosi << 3000, 4000, 0;

        float distToObs = 0.001 * (currentGuardGlobalPosi - currentObsPosi).norm();
        float heightOffset = 0;

        float distToReactInMeter = 0.001 * in.getDistToReact();
        if (distToObs < distToReactInMeter)
        {
            float changeFactor = cos(distToObs / distToReactInMeter * M_PI / 2);
            heightOffset = in.getHeightAmplitude() * changeFactor;

            //            Eigen::Vector3f forceLeft, forceRight;

            //            {
            //                FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
            //                forceRight = forcePtr->toRootEigen(localRobot) - initialForceRight;
            //                filteredForceRight = (1 - in.getForceFilterFactor()) * forceRight + in.getForceFilterFactor() * filteredForceRight;
            //            }
            //            {
            //                FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
            //                forceLeft = forcePtr->toRootEigen(localRobot) - initialForceLeft;
            //                filteredForceLeft = (1 - in.getForceFilterFactor()) * forceLeft + in.getForceFilterFactor() * filteredForceLeft;
            //            }

            //            Eigen::Vector3f filteredForce = filteredForceRight + filteredForceLeft;
            //            filteredForce(2) = 0;

            //            if (filteredForce.norm() < in.getForceDeadZone())
            //            {
            //                filteredForce.setZero();
            //            }
            //            else
            //            {
            //                filteredForce = filteredForce - in.getForceDeadZone() * filteredForce.normalized();
            //            }

            //            getDebugObserver()->setDebugDatafield("filteredForce_x", "fx", new Variant(filteredForce(0)));
            //            getDebugObserver()->setDebugDatafield("filteredForce_y", "fy", new Variant(filteredForce(1)));
            //            velocity = forceVelFactor.cwiseProduct(filteredForce);
            angularVelocity = 0;
            std::vector<float> currentKpos = Kpos;
            currentKpos[0] = Kpos[0] - in.getMaxiReducedStiffness() * changeFactor;

            leftController->setPosition(
            {
                desiredPoseLeft(0, 3),
                desiredPoseLeft(1, 3),
                desiredPoseLeft(2, 3) + heightOffset
            });
            rightController->setPosition(
            {
                desiredPoseRight(0, 3),
                desiredPoseRight(1, 3),
                desiredPoseRight(2, 3) + heightOffset
            });
            leftController->setImpedanceParameters("Kpos", currentKpos);
            rightController->setImpedanceParameters("Kpos", currentKpos);

        }

        getPlatformUnit()->move(velocity(0), velocity(1), angularVelocity);
        getDebugObserver()->setDebugDatafield("GuardTransportation", "vx", new Variant(velocity(0)));
        getDebugObserver()->setDebugDatafield("GuardTransportation", "vy", new Variant(velocity(1)));
        getDebugObserver()->setDebugDatafield("GuardTransportation", "vangle", new Variant(angularVelocity));
        getDebugObserver()->setDebugDatafield("GuardTransportation", "distToObs", new Variant(distToObs));
        getDebugObserver()->setDebugDatafield("GuardTransportation", "heightOffset", new Variant(heightOffset / in.getHeightAmplitude()));

    }

    getPlatformUnit()->move(0, 0, 0);

    rightController->deactivateController();
    leftController->deactivateController();
    while (rightController->isControllerActive() || leftController->isControllerActive())
    {
        usleep(10000);
    }
    rightController->deleteController();
    leftController->deleteController();

}

//void GuardTransportationWithObstacleAvoidance::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GuardTransportationWithObstacleAvoidance::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GuardTransportationWithObstacleAvoidance::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GuardTransportationWithObstacleAvoidance(stateData));
}
