/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     armar6-user ( armar6-user at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "WholeBodyDMPController.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <armarx/control/deprecated_njoint_mp_controller/task_space/TaskSpaceImpedanceDMPControllerInterface.h>
#include "Helpers.h"
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>

#include <armar6/skills/statecharts/Armar6BoardSupportDemo/Armar6BoardSupportDemoStatechartContext.generated.h>
#include <RobotAPI/libraries/core/FramedPose.h>

using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
WholeBodyDMPController::SubClassRegistry WholeBodyDMPController::Registry(WholeBodyDMPController::GetName(), &WholeBodyDMPController::CreateInstance);



void WholeBodyDMPController::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void WholeBodyDMPController::run()
{
    ARMARX_INFO << "Running WholeBodyDMPController ... ";

    //    getRobotUnit()->loadLibFromPackage("RobotAPI", "RobotAPINJointControllers");

    NJointControllerInterfacePrx controller = getRobotUnit()->getNJointController(in.getLeftControllerName());
    if (controller)
    {
        getRobotUnit()->deactivateAndDeleteNJointController(in.getLeftControllerName());

        //        if (controller->isControllerActive())
        //        {
        //            controller->deactivateController();
        //            while (controller->isControllerActive())
        //            {
        //                TimeUtil::SleepMS(10);
        //            }
        //        }
        //        controller->deleteController();
        TimeUtil::SleepMS(10);
    }

    controller = getRobotUnit()->getNJointController(in.getRightControllerName());
    if (controller)
    {
        getRobotUnit()->deactivateAndDeleteNJointController(in.getRightControllerName());

        //        if (controller->isControllerActive())
        //        {
        //            controller->deactivateController();
        //            while (controller->isControllerActive())
        //            {
        //                TimeUtil::SleepMS(10);
        //            }
        //        }
        //        controller->deleteController();
        TimeUtil::SleepMS(10);
    }

    controller = getRobotUnit()->getNJointController(in.getRightControllerName() + "_touch");
    if (controller)
    {
        getRobotUnit()->deactivateAndDeleteNJointController(in.getRightControllerName() + "_touch");
        TimeUtil::SleepMS(10);
    }

    controller = getRobotUnit()->getNJointController(in.getLeftControllerName() + "_touch");
    if (controller)
    {
        getRobotUnit()->deactivateAndDeleteNJointController(in.getLeftControllerName() + "_touch");
        TimeUtil::SleepMS(10);
    }


    double phaseL = 100;
    double phaseK = 1000;
    double phaseDist0 = 100;
    double phaseDist1 = 50;
    double posToOriRatio = 10;

    std::vector<float> defaultLeftJointValues;
    if (in.isDesiredLeftJointValuesSet())
    {
        defaultLeftJointValues = in.getDesiredLeftJointValues();
    }
    else
    {
        defaultLeftJointValues = getRobot()->getRobotNodeSet("LeftArm")->getJointValues();
    }

    std::vector<float> defaultRightJointValues;
    if (in.isDesiredRightJointValuesSet())
    {
        defaultRightJointValues = in.getDesiredRightJointValues();
    }
    else
    {
        defaultRightJointValues = getRobot()->getRobotNodeSet("RightArm")->getJointValues();
    }

    float torqueLimit = in.getTorqueLimit();

    std::string forceSensorName = "FT L";
    float waitTimeForCalibration = 0.0;
    float forceFilter = 0.5;
    float forceDeadZone = 2;
    Eigen::Vector3f forceThreshold{100, 100, 100};
    std::string forceFrameName = "ArmL8_Wri2";
    NJointTaskSpaceImpedanceDMPControllerConfigPtr tsConfig = new NJointTaskSpaceImpedanceDMPControllerConfig(
        in.getDMPKernelSize(),
        in.getBaseMode(),
        in.getDMPStyle(),
        in.getTimeDuration(),
        "LeftArm",
        phaseL,
        phaseK,
        phaseDist0,
        phaseDist1,
        posToOriRatio,
        in.getKposVec(),
        in.getDposVec(),
        in.getKoriVec(),
        in.getDoriVec(),
        in.getKnull(),
        in.getDnull(),
        false,
        defaultLeftJointValues,
        torqueLimit,
        forceSensorName,
        waitTimeForCalibration,
        forceFilter,
        forceDeadZone,
        forceThreshold,
        forceFrameName
    );

    NJointTaskSpaceImpedanceDMPControllerInterfacePrx leftController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", in.getLeftControllerName(), tsConfig));

    tsConfig->nodeSetName = "RightArm";
    tsConfig->defaultNullSpaceJointValues = defaultRightJointValues;
    tsConfig->forceFrameName = "ArmR8_Wri2";
    tsConfig->forceSensorName = "FT R";

    NJointTaskSpaceImpedanceDMPControllerInterfacePrx rightController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", in.getRightControllerName(), tsConfig));

    tsConfig->nodeSetName = "LeftArm";
    tsConfig->defaultNullSpaceJointValues = defaultLeftJointValues;
    tsConfig->timeDuration = in.getTouchMotionDuration();
    tsConfig->forceFrameName = "ArmL8_Wri2";
    tsConfig->forceSensorName = "FT L";

    NJointTaskSpaceImpedanceDMPControllerInterfacePrx leftTouchController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", in.getLeftControllerName() + "_touch", tsConfig));

    tsConfig->nodeSetName = "RightArm";
    tsConfig->defaultNullSpaceJointValues = defaultRightJointValues;
    tsConfig->forceFrameName = "ArmR8_Wri2";
    tsConfig->forceSensorName = "FT R";

    NJointTaskSpaceImpedanceDMPControllerInterfacePrx rightTouchController
        = NJointTaskSpaceImpedanceDMPControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTaskSpaceImpedanceDMPController", in.getRightControllerName() + "_touch", tsConfig));


    std::vector<std::string> fileNames = in.getLeftArmMotionFile();
    leftController->learnDMPFromFiles(fileNames);
    fileNames = in.getRightArmMotionFile();
    rightController->learnDMPFromFiles(fileNames);
    leftTouchController->learnDMPFromFiles(fileNames);
    rightTouchController->learnDMPFromFiles(fileNames);

    std::vector<double> leftGoals = Helpers::pose2dvec(in.getLeftTcpGoalPose()->toEigen());

    if (in.isLeftViaPoseSet())
    {
        std::map<std::string, armarx::PosePtr> viaPoseMap = in.getLeftViaPose();

        for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
             it != viaPoseMap.end(); ++it)
        {
            double canVal = std::stod(it->first);
            leftController->setViaPoints(canVal, Helpers::pose2dvec(it->second->toEigen()));

        }
    }

    std::vector<double> rightGoals = Helpers::pose2dvec(in.getRightTcpGoalPose()->toEigen());

    if (in.isRightViaPoseSet())
    {
        std::map<std::string, armarx::PosePtr> viaPoseMap = in.getRightViaPose();

        for (std::map<std::string, armarx::PosePtr>::iterator it = viaPoseMap.begin();
             it != viaPoseMap.end(); ++it)
        {
            double canVal = std::stod(it->first);
            rightController->setViaPoints(canVal, Helpers::pose2dvec(it->second->toEigen()));

        }
    }

    leftController->activateController();
    rightController->activateController();
    leftController->runDMP(leftGoals);
    rightController->runDMP(rightGoals);


    NameControlModeMap controlModes;
    controlModes["TorsoJoint"] = ePositionControl;
    getKinematicUnit()->switchControlMode(controlModes);
    NameValueMap targetTorso;

    VirtualRobot::RobotPtr localRobot = getLocalRobot();
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
    //    float initTorsoJoint = localRobot->getRobotNode("TorsoJoint")->getJointValue();


    ARMARX_INFO << "Started DMP ... ";

    RobotNameHelper::Arm armLeft = getRobotNameHelper()->getArm("Left");
    RobotNameHelper::Arm armRight = getRobotNameHelper()->getArm("Right");


    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx leftHandcontroller = devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController(armLeft.getHandControllerName()));

    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx rightHandcontroller = devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController(armRight.getHandControllerName()));


    DatafieldRefPtr forceDfLeft = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield(armLeft.getForceTorqueSensor()));
    Eigen::Vector3f initialForceLeft;
    initialForceLeft.setZero();
    Eigen::Vector3f forceLeft;
    IceUtil::Time forceTimerLeft = IceUtil::Time::now();
    float forceIntegralLeft = 0;


    DatafieldRefPtr forceDfRight = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield(armRight.getForceTorqueSensor()));
    Eigen::Vector3f initialForceRight;
    initialForceRight.setZero();
    Eigen::Vector3f forceRight;
    IceUtil::Time forceTimerRight = IceUtil::Time::now();
    float forceIntegralRight = 0;


    bool isLeftStopped = false;
    bool isRightStopped = false;

    targetTorso["TorsoJoint"] = in.getTorsoJointTarget();
    getKinematicUnit()->setJointAngles(targetTorso);

    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        double canVal = leftController->getVirtualTime();

        if (in.getIsPickUp())
        {
            //            if (canVal < 0.2 * in.getTimeDuration())
            //            {
            //                {
            //                    FramedDirectionPtr forcePtr = forceDfLeft->getDataField()->get<FramedDirection>();
            //                    forceLeft = forcePtr->toRootEigen(localRobot);
            //                    forceLeft -= initialForceLeft;
            //                }

            //                Eigen::Vector3f fdir;
            //                fdir << 0, -1, 0;
            //                float forceMag = forceLeft.dot(fdir);
            //                double deltaT = (IceUtil::Time::now() - forceTimerLeft).toSecondsDouble();
            //                if (forceMag > 5)
            //                {
            //                    forceIntegralLeft += forceMag * deltaT;
            //                }
            //                else
            //                {
            //                    forceIntegralLeft = 0;
            //                }
            //                getDebugObserver()->setDebugDatafield("GoToPickUp", "forceIntegralLeft", new Variant(forceIntegralLeft));
            //                getDebugObserver()->setDebugDatafield("GoToPickUp", "forceMag", new Variant(forceMag));

            //                if (forceIntegralLeft >  5)
            //                {
            //                    leftHandcontroller->activateController();
            //                    leftHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
            //                    leftController->stopDMP();
            //                    isLeftStopped = true;
            //                }
            //            }
            if (canVal > 0.05 * in.getTimeDuration())
            {
                forceTimerLeft = IceUtil::Time::now();
                RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
                initialForceLeft =  forceDfLeft->getDataField()->get<FramedDirection>()->toRootEigen(localRobot);
            }

            //            if (canVal < 0.2 * in.getTimeDuration())
            //            {
            //                {
            //                    FramedDirectionPtr forcePtr = forceDfRight->getDataField()->get<FramedDirection>();
            //                    forceRight = forcePtr->toRootEigen(localRobot);
            //                    forceRight -= initialForceRight;
            //                }

            //                Eigen::Vector3f fdir;
            //                fdir << 1, 0, 0;
            //                float forceMag = forceRight.dot(fdir);
            //                double deltaT = (IceUtil::Time::now() - forceTimerRight).toSecondsDouble();
            //                if (forceMag > 5)
            //                {
            //                    forceIntegralRight += forceMag * deltaT;
            //                }
            //                else
            //                {
            //                    forceIntegralRight = 0;
            //                }
            //                getDebugObserver()->setDebugDatafield("GoToPickUp", "forceIntegralRight", new Variant(forceIntegralRight));
            //                getDebugObserver()->setDebugDatafield("GoToPickUp", "forceMag", new Variant(forceMag));

            //                if (forceIntegralRight >  5)
            //                {
            //                    rightHandcontroller->activateController();
            //                    rightHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
            //                    rightController->stopDMP();
            //                    isRightStopped = true;
            //                }
            //            }
            if (canVal > 0.05 * in.getTimeDuration())
            {
                forceTimerRight = IceUtil::Time::now();
                RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
                initialForceRight =  forceDfRight->getDataField()->get<FramedDirection>()->toRootEigen(localRobot);
            }
        }


        if (leftController->isFinished())
        {
            isLeftStopped = true;
        }


        if (rightController->isFinished())
        {
            isRightStopped = true;
        }

        if (isLeftStopped && isRightStopped)
        {
            break;
        }

        //        float torsoVal = (in.getTimeDuration() - canVal) / (in.getTimeDuration()) * (torsoJointTarget - initTorsoJoint) + initTorsoJoint;
        //        targetTorso["TorsoJoint"] = torsoVal;
        //        getKinematicUnit()->setJointAngles(targetTorso);
    }
    initialForceLeft = forceDfLeft->getDataField()->get<FramedDirection>()->toRootEigen(localRobot);
    initialForceRight = forceDfRight->getDataField()->get<FramedDirection>()->toRootEigen(localRobot);

    if (in.getIsPickUp())
    {
        leftGoals.at(1) += 150;
        leftGoals.at(0) -= 50;
        rightGoals.at(0) -= 200;
        leftTouchController->activateController();
        rightTouchController->activateController();

        leftTouchController->runDMP(leftGoals);
        rightTouchController->runDMP(rightGoals);

        isLeftStopped = false;
        isRightStopped = false;
        while (!isRunningTaskStopped())
        {
            RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

            FramedDirectionPtr forceRightPtr = forceDfRight->getDataField()->get<FramedDirection>();
            forceRight = forceRightPtr->toRootEigen(localRobot);
            forceRight -= initialForceRight;

            FramedDirectionPtr forceLeftPtr = forceDfLeft->getDataField()->get<FramedDirection>();
            forceLeft = forceLeftPtr->toRootEigen(localRobot);
            forceLeft -= initialForceLeft;

            {
                Eigen::Vector3f fdir;
                fdir << 0, -1, 0;
                float forceMag = forceLeft.dot(fdir);
                double deltaT = (IceUtil::Time::now() - forceTimerLeft).toSecondsDouble();
                if (forceMag > 4)
                {
                    forceIntegralLeft += forceMag * deltaT;
                }
                else
                {
                    forceIntegralLeft = 0;
                }
                getDebugObserver()->setDebugDatafield("GoToPickUp", "forceIntegralLeft", new Variant(forceIntegralLeft));
                getDebugObserver()->setDebugDatafield("GoToPickUp", "forceMag", new Variant(forceMag));

                if (forceIntegralLeft >  4)
                {
                    leftHandcontroller->activateController();
                    leftHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
                    leftTouchController->stopDMP();
                    isLeftStopped = true;
                }
            }
            {
                Eigen::Vector3f fdir;
                fdir << 1, 0, 0;
                float forceMag = forceRight.dot(fdir);
                double deltaT = (IceUtil::Time::now() - forceTimerRight).toSecondsDouble();
                if (forceMag > 4)
                {
                    forceIntegralRight += forceMag * deltaT;
                }
                else
                {
                    forceIntegralRight = 0;
                }
                getDebugObserver()->setDebugDatafield("GoToPickUp", "forceIntegralRight", new Variant(forceIntegralRight));
                getDebugObserver()->setDebugDatafield("GoToPickUp", "forceMag", new Variant(forceMag));

                if (forceIntegralRight >  4)
                {
                    rightHandcontroller->activateController();
                    rightHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
                    rightTouchController->stopDMP();
                    isRightStopped = true;
                }
            }
            if (leftTouchController->isFinished())
            {
                isLeftStopped = true;
            }


            if (rightTouchController->isFinished())
            {
                isRightStopped = true;
            }
            if (isLeftStopped && isRightStopped)
            {
                ARMARX_INFO << "reach force threshold";
                break;
            }
            usleep(1000);
        }
    }


    leftController->deactivateController();
    leftTouchController->deactivateController();
    rightController->deactivateController();
    rightTouchController->deactivateController();

    if (in.getIsPickUp())
    {
        leftHandcontroller->activateController();
        rightHandcontroller->activateController();
        leftHandcontroller->setTargetsWithPwm(0, 2, 1, 1);
        rightHandcontroller->setTargetsWithPwm(2, 2, 1, 1);
    }

    while (leftController->isControllerActive() || rightController->isControllerActive() || leftTouchController->isControllerActive() || rightTouchController->isControllerActive())
    {
        usleep(10000);
    }

    leftController->deleteController();
    leftTouchController->deleteController();
    rightController->deleteController();
    rightTouchController->deleteController();
    usleep(100000);

    emitSuccess();
}

//void WholeBodyDMPController::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void WholeBodyDMPController::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr WholeBodyDMPController::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new WholeBodyDMPController(stateData));
}
