/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6BoardSupportDemo
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "OpenHand.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>
#include <armar6/skills/statecharts/Armar6BoardSupportDemo/Armar6BoardSupportDemoStatechartContext.generated.h>
#include <VirtualRobot/MathTools.h>
using namespace armarx;
using namespace Armar6BoardSupportDemo;

// DO NOT EDIT NEXT LINE
OpenHand::SubClassRegistry OpenHand::Registry(OpenHand::GetName(), &OpenHand::CreateInstance);



void OpenHand::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void OpenHand::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true
    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx leftHandcontroller;
    if (!getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"))
    {
        leftHandcontroller = StateBase::getContext<Armar6BoardSupportDemoStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_L_EEF_NJointKITHandV2ShapeController");
    }
    else
    {
        leftHandcontroller =  devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_L_EEF_NJointKITHandV2ShapeController"));
    }
    leftHandcontroller->activateController();

    devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx rightHandcontroller;
    if (!getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"))
    {
        rightHandcontroller = StateBase::getContext<Armar6BoardSupportDemoStatechartContext>()->getProxy<devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx>("Hand_R_EEF_NJointKITHandV2ShapeController");
    }
    else
    {
        rightHandcontroller = devices::ethercat::hand::armar6_v2::ShapeControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("Hand_R_EEF_NJointKITHandV2ShapeController"));
    }
    rightHandcontroller->activateController();

    leftHandcontroller->setTargets(0, 0);
    rightHandcontroller->setTargets(0, 0);


    VirtualRobot::RobotPtr localRobot = getLocalRobot();
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

    if (in.getIsRightHandPlacement())
    {
        std::map<std::string, armarx::PosePtr> viaPoseMap;
        Eigen::Matrix4f currPose = localRobot->getRobotNodeSet("RightArm")->getTCP()->getPoseInRootFrame();

        float qw = 0.69;
        float qx = -0.72;
        float qy = -0.064;
        float qz = 0.044;
        Eigen::Matrix4f viaPose = VirtualRobot::MathTools::quat2eigen4f(qx, qy, qz, qw);
        viaPose(0, 3) = currPose(0, 3);
        viaPose(1, 3) = currPose(1, 3);
        viaPose(2, 3) = currPose(2, 3) + 200;
        armarx::PosePtr viaPosePtr(new armarx::Pose(viaPose));
        viaPoseMap["0.7"] = viaPosePtr;
        out.setRightHandViaPose(viaPoseMap);
    }
    else
    {
        std::map<std::string, armarx::PosePtr> viaPoseMap;
        Eigen::Matrix4f viaPose = localRobot->getRobotNodeSet("LeftArm")->getTCP()->getPoseInRootFrame();
        viaPose(2, 3) += 200;
        armarx::PosePtr viaPosePtr(new armarx::Pose(viaPose));
        viaPoseMap["0.7"] = viaPosePtr;
        out.setRightHandViaPose(viaPoseMap);
    }

    usleep(10000);


    //    leftHandcontroller->deactivateController();
    //    rightHandcontroller->deactivateController();

    //    while (leftHandcontroller->isControllerActive() || rightHandcontroller->isControllerActive())
    //    {
    //        usleep(1000);
    //    }

    //    leftHandcontroller->deleteController();
    //    rightHandcontroller->deleteController();

    emitSuccess();
}

//void OpenHand::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void OpenHand::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr OpenHand::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new OpenHand(stateData));
}
