/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6HandDemoGroup::Armar6HandDemoGroupRemoteStateOfferer
 * @author     armar6-user ( armar6-user at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6HandDemoGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6HandDemoGroup;

// DO NOT EDIT NEXT LINE
Armar6HandDemoGroupRemoteStateOfferer::SubClassRegistry Armar6HandDemoGroupRemoteStateOfferer::Registry(Armar6HandDemoGroupRemoteStateOfferer::GetName(), &Armar6HandDemoGroupRemoteStateOfferer::CreateInstance);



Armar6HandDemoGroupRemoteStateOfferer::Armar6HandDemoGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6HandDemoGroupStatechartContext > (reader)
{
}

void Armar6HandDemoGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6HandDemoGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6HandDemoGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6HandDemoGroupRemoteStateOfferer::GetName()
{
    return "Armar6HandDemoGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6HandDemoGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6HandDemoGroupRemoteStateOfferer(reader));
}
