/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::PlannedGraspingGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspingPipelineWithVision.h"

#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>

#include <RobotAPI/libraries/core/LinkedPose.h>

#include <MemoryX/libraries/motionmodels/MotionModelStaticObject.h>

#include <ArmarXCore/statechart/StatechartContext.h>

#include <VisionX/interface/components/VoxelGridProviderInterface.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace PlannedGraspingGroup;

// DO NOT EDIT NEXT LINE
GraspingPipelineWithVision::SubClassRegistry GraspingPipelineWithVision::Registry(GraspingPipelineWithVision::GetName(), &GraspingPipelineWithVision::CreateInstance);



void GraspingPipelineWithVision::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)

    ARMARX_CHECK_EXPRESSION(getWorkingMemory());

    auto objSeg = getWorkingMemory()->getObjectInstancesSegment();
    ARMARX_CHECK_EXPRESSION(objSeg);

    getViewSelection()->deactivateAutomaticViewSelection();



    auto object = memoryx::ObjectInstancePtr::dynamicCast(objSeg->getObjectInstanceByName(in.getObjectName()));
    ARMARX_CHECK_EXPRESSION(object);
    Eigen::Quaternionf q(object->getPose()->toEigen().block<3, 3>(0, 0));
    ARMARX_INFO << "object pose: " << q.w() << " and " << q.vec();


    std::string objectName = object->getName();
    if (in.isObjectPoseSet())
    {
        //        getWorkingMemory()->getObjectInstancesSegment()->removeEntity(object->getId());
        getWorkingMemory()->getObjectInstancesSegment()->setObjectPose(object->getId(),
                new LinkedPose(*in.getObjectPose(), getRobotStateComponent()->getSynchronizedRobot()));

        //                                                               new memoryx::MotionModelStaticObject(getRobotStateComponent())
        object = memoryx::ObjectInstancePtr::dynamicCast(objSeg->getObjectInstanceById(object->getId()));
    }

    ARMARX_CHECK_EXPRESSION(object) << in.getObjectName();

    auto ref = objSeg->getEntityRefById(object->getId());

    ARMARX_CHECK_EXPRESSION(ref) << in.getObjectName();

    local.setObjectRef(ref);

    FramedPosePtr objectPose = object->getPose();
    if (object->hasLocalizationTimestamp())
    {
        RemoteRobot::synchronizeLocalCloneToTimestamp(getLocalRobot(), getRobotStateComponent(), object->getLocalizationTimestamp().toMicroSeconds());
        objectPose->changeToGlobal(getLocalRobot());
    }


    local.setInitialObjectPose(objectPose);


}

//void GraspingPipelineWithVision::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void GraspingPipelineWithVision::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GraspingPipelineWithVision::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
    getObjectMemoryObserver()->releaseObjectClass(local.getObjectClassChannel());
    //    getViewSelection()->deactivateAutomaticViewSelection();
    TimeUtil::MSSleep(100);
    getHeadIKUnit()->setHeadTarget("IKVirtualGaze", new FramedPosition(Eigen::Vector3f(0, 800, 1800), getLocalRobot()->getRootNode()->getName(), getLocalRobot()->getName()));
    out.setUsedEndeffector(local.getHandName());
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GraspingPipelineWithVision::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GraspingPipelineWithVision(stateData));
}



void GraspingPipelineWithVision::transitionObjectInstanceChannelFoundFromGetObjectInstance(VerifyObjectPoseIn& next, const VisualServoGroup::GetObjectInstanceOut& prev)
{
    getObjectMemoryObserver()->releaseObjectClass(local.getObjectClassChannel());

}


//void armarx::PlannedGraspingGroup::GraspingPipelineWithVision::transitionSuccessFromPlayTrajectory(HandGroup::CloseHandIn& next, const PlayTrajectoryOut& prev)
//{
//    auto tcpName = getLocalRobot()->getRobotNodeSet(local.getSelectedKinematicChain())->getTCP()->getName();
//    next.setHandName(in.getHandUnitMapping().at(tcpName));
//}


void armarx::PlannedGraspingGroup::GraspingPipelineWithVision::transitionSuccessFromIf1(ScanLocationGroup::ScanWorkspaceIn& next, const CoreUtility::If1Out& prev)
{
    auto gridProvider = getContext<StatechartContext>()->getProxy<visionx::VoxelGridProviderInterfacePrx>(in.getVoxelGridProviderName());
    gridProvider->reset();
    gridProvider->startCollectingPointClouds();
}

void armarx::PlannedGraspingGroup::GraspingPipelineWithVision::transitionSuccessFromScanWorkspace(GraspingPipelineGroup::CalculateTrajectoryIn& next, const ScanLocationGroup::ScanWorkspaceOut& prev)
{
    auto gridProvider = getContext<StatechartContext>()->getProxy<visionx::VoxelGridProviderInterfacePrx>(in.getVoxelGridProviderName());
    gridProvider->stopCollectingPointClouds();
}


void armarx::PlannedGraspingGroup::GraspingPipelineWithVision::transitionSuccessFromIfVerifyPose(VisualServoGroup::GetObjectInstanceIn& next, const CoreUtility::If1Out& prev)
{
    ChannelRefBasePtr objectMemoryChannel;
    getHeadIKUnit()->setHeadTarget("IKVirtualGaze", new FramedPosition(Eigen::Vector3f(0, 800, 800), getLocalRobot()->getRootNode()->getName(), getLocalRobot()->getName()));
    TimeUtil::MSSleep(1000);
    objectMemoryChannel = getObjectMemoryObserver()->requestObjectClassRepeated(in.getObjectName(), 1000 / in.getObjectLocalizationFrequency(), armarx::DEFAULT_VIEWTARGET_PRIORITY);
    if (objectMemoryChannel)
    {
        next.setObjectClassChannel(ChannelRefPtr::dynamicCast(objectMemoryChannel));
        local.setObjectClassChannel(ChannelRefPtr::dynamicCast(objectMemoryChannel));
    }
    else
    {
        ARMARX_WARNING << "Could not get Object Class Memory Channel for object " << in.getObjectName();
    }
}

void GraspingPipelineWithVision::transitionSuccessFromPlayTrajectory(ExecuteGraspIn& next, const PlayTrajectoryOut& prev)
{
    next.setSelectedArm(armarx::Contains(local.getSelectedKinematicChain(), "right", true) ? "Right" : "Left");
}
