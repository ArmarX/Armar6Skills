/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::PlannedGraspingGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspingWithVision.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <RobotAPI/interface/components/ViewSelectionInterface.h>
#include <VisionX/interface/components/VoxelGridProviderInterface.h>
#include <ArmarXCore/statechart/StatechartContext.h>
using namespace armarx;
using namespace PlannedGraspingGroup;

// DO NOT EDIT NEXT LINE
GraspingWithVision::SubClassRegistry GraspingWithVision::Registry(GraspingWithVision::GetName(), &GraspingWithVision::CreateInstance);



void GraspingWithVision::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)

    auto objectName = in.getObjectName();
    ChannelRefBasePtr objectMemoryChannel;

    objectMemoryChannel = getObjectMemoryObserver()->requestObjectClassRepeated(objectName, 1000 / in.getObjectLocalizationFrequency(), armarx::DEFAULT_VIEWTARGET_PRIORITY);
    if (objectMemoryChannel)
    {
        local.setObjectClassMemoryChannel(ChannelRefPtr::dynamicCast(objectMemoryChannel));
    }
    else
    {
        emitFailure();
        cancelSubstates();
        ARMARX_WARNING << "Unknown Object Class: " << objectName;
    }
}

//void GraspingWithVision::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void GraspingWithVision::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GraspingWithVision::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
    getObjectMemoryObserver()->releaseObjectClass(local.getObjectClassMemoryChannel());
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GraspingWithVision::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GraspingWithVision(stateData));
}


void armarx::PlannedGraspingGroup::GraspingWithVision::transitionObjectInstanceChannelFoundFromGetObjectInstance(CoreUtility::If1In& next, const VisualServoGroup::GetObjectInstanceOut& prev)
{
    local.setObjectInstanceId(prev.getObjectInstanceChannel()->getDataField("id")->getString());
}

void armarx::PlannedGraspingGroup::GraspingWithVision::transitionSuccessFromIf1(ScanLocationGroup::ScanWorkspaceIn& next, const CoreUtility::If1Out& prev)
{
    auto gridProvider = getContext<StatechartContext>()->getProxy<visionx::VoxelGridProviderInterfacePrx>(in.getVoxelGridProviderName());
    gridProvider->reset();
    gridProvider->startCollectingPointClouds();
}


void armarx::PlannedGraspingGroup::GraspingWithVision::transitionSuccessFromScanWorkspace(GraspingPipelineWithoutRobotPlacementIn& next, const ScanLocationGroup::ScanWorkspaceOut& prev)
{
    auto gridProvider = getContext<StatechartContext>()->getProxy<visionx::VoxelGridProviderInterfacePrx>(in.getVoxelGridProviderName());
    gridProvider->stopCollectingPointClouds();;
}
