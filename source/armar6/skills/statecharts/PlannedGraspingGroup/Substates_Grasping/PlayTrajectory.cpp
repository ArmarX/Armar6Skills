/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::PlannedGraspingGroup
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PlayTrajectory.h"
#include <RobotAPI/interface/components/TrajectoryPlayerInterface.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace PlannedGraspingGroup;

// DO NOT EDIT NEXT LINE
PlayTrajectory::SubClassRegistry PlayTrajectory::Registry(PlayTrajectory::GetName(), &PlayTrajectory::CreateInstance);



void PlayTrajectory::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void PlayTrajectory::run()
{
    TrajectoryPlayerInterfacePrx trajectoryPlayer = getTrajectoryPlayer();


    TrajectoryPtr trajectory = TrajectoryPtr::dynamicCast(in.getTrajectory());
    if (trajectory->size() == 0)
    {
        emitFailure();
        return;
    }

    ARMARX_INFO << VAROUT(trajectory->output());
    ARMARX_INFO << trajectory->getTimestamps();
    trajectoryPlayer->resetTrajectoryPlayer(false);
    trajectoryPlayer->loadJointTraj(trajectory);
    trajectoryPlayer->loadBasePoseTraj(trajectory);
    trajectoryPlayer->setIsVelocityControl(true);


    trajectoryPlayer->startTrajectoryPlayer();

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        if (this->getTrajectoryPlayer()->getCurrentTime() >= this->getTrajectoryPlayer()->getEndTime())
        {
            ARMARX_LOG << "emitSuccess()";
            emitSuccess();
            return;
        }
        //Wait 50ms
        usleep(50000);
    }

    emitFailure();
}

void PlayTrajectory::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
    getTrajectoryPlayer()->stopTrajectoryPlayer();
}

void PlayTrajectory::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr PlayTrajectory::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new PlayTrajectory(stateData));
}
