/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::PlannedGraspingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ExecuteGrasp.h"

#include <RobotAPI/libraries/RobotStatechartHelpers/ForceTorqueHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/PositionControllerHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>

#include <VirtualRobot/math/LineStrip.h>
#include <VirtualRobot/math/Helpers.h>

#include <ArmarXCore/observers/filters/rtfilters/AverageFilter.h>

#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <functional>

using namespace math;
using namespace armarx;
using namespace PlannedGraspingGroup;

// DO NOT EDIT NEXT LINE
ExecuteGrasp::SubClassRegistry ExecuteGrasp::Registry(ExecuteGrasp::GetName(), &ExecuteGrasp::CreateInstance);


std::string ExecuteGrasp::phaseToStr(PhaseType phase)
{
    switch (phase)
    {
        case PhaseType::Approach:
            return "Approach";
        case PhaseType::Grasp:
            return "Grasp";
        case PhaseType::Lift:
            return "Lift";
        default:
            return "-";
    }
}


void ExecuteGrasp::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void ExecuteGrasp::shapeHand(float fingers, float thumb, const std::string& handJointsPrefix)
{
    std::string fingersName = handJointsPrefix + "Fingers";
    std::string thumbName = handJointsPrefix + "Thumb";

    getKinematicUnit()->switchControlMode({{fingersName, ePositionControl}, {thumbName, ePositionControl}});
    getKinematicUnit()->setJointAngles({{fingersName, fingers}, {thumbName, thumb}});
}

class MessageDisplayHelper
{
public:
    MessageDisplayHelper(const MessageDisplayInterfacePrx& messageDisplay)
        : messageDisplay(messageDisplay) {}
    void setMessage(const std::string& caption, const std::string& subCaption)
    {
        if (enable)
        {
            messageDisplay->setMessage(caption, subCaption);
        }
    }
    void setCaption(const std::string& caption)
    {
        if (enable)
        {
            messageDisplay->setCaption(caption);
        }
    }
    void setSubCaption(const std::string& subCaption)
    {
        if (enable)
        {
            messageDisplay->setSubCaption(subCaption);
        }
    }
    bool enable = true;
    MessageDisplayInterfacePrx messageDisplay;

};

template<typename T>
class StatePhaseHelper
{
public:

    class Phase
    {
    public:
        Phase(const std::function<T()>& run) : run(run) {}
        std::function<void()> onenter = []() {};
        std::function<T()> run;
        std::function<void()> onexit = []() {};
    };
    std::map<T, Phase> phases;
    void addPhase(T type, const Phase& phase)
    {
        phases.insert(std::make_pair(type, phase));
    }

    T currentPhase;
    void run()
    {
        T newPhase = phases[currentPhase].run();
        if (newPhase != currentPhase)
        {
            phases[currentPhase].onexit();
            phases[newPhase].onenter();
            currentPhase = newPhase;
        }
    }
    virtual std::string phaseToString(T phase) = 0;

    virtual ~StatePhaseHelper() = default;
};
class ExecuteGraspPhases
    : public StatePhaseHelper<ExecuteGrasp::PhaseType>
{

public:
    std::string phaseToString(ExecuteGrasp::PhaseType phase) override
    {
        switch (phase)
        {
            case ExecuteGrasp::PhaseType::Approach:
                return "Approach";
            case ExecuteGrasp::PhaseType::Grasp:
                return "Grasp";
            case ExecuteGrasp::PhaseType::Lift:
                return "Lift";
            default:
                return "-";
        }
    }
    bool targetReached = false;
    bool targetNear = false;
    bool forceExceeded = false;
    float forceNormFiltered = 0;
    float torqueNormFiltered = 0;
    IceUtil::Time graspStartTime = TimeUtil::GetTime();
};

void ExecuteGrasp::run()
{

    std::string selectedArm = in.getSelectedArm();
    if (selectedArm != "Right" && selectedArm != "Left")
    {
        throw LocalException("SelectedArm has to be either 'Right' or 'Left'.");
    }

    MessageDisplayHelper msgHelper(getMessageDisplay());
    msgHelper.enable = in.getUseMessageDisplay();


    std::string nodeSetName = in.getNodeSetName().at(selectedArm);
    std::string FTDatefieldName = in.getFTDatefieldName().at(selectedArm);
    std::string handJointPrefix = in.getHandJointsPrefix().at(selectedArm);

    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent(), VirtualRobot::RobotIO::eStructure);
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(nodeSetName);
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();
    Eigen::Vector3f handForwardVectorInHandCoords = in.getHandForwardVectorInTcpFrame().at(selectedArm)->toEigen().normalized();

    VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(getRobotUnit(), nodeSetName, in.getCartesianVelocityControllerName()));

    ForceTorqueHelper ftHelper(getForceTorqueObserver(), FTDatefieldName);
    PositionControllerHelper posHelper(tcp, velHelper, Eigen::Matrix4f::Zero());
    posHelper.thresholdOrientationNear = in.getThresholdOrientationNear();
    posHelper.thresholdOrientationReached = in.getThresholdOrientationReached();
    posHelper.thresholdPositionNear = in.getThresholdPositionNear();
    posHelper.thresholdPositionReached = in.getThresholdPositionReached();

    float temporalFactor = in.getTemporalFactor();
    auto updateTemporalFactor = [&](float factor)
    {
        posHelper.posController.maxOriVel = in.getMaxOriVel() * temporalFactor;
        posHelper.posController.maxPosVel = in.getMaxPosVel() * temporalFactor;
        posHelper.posController.KpOri = in.getKpOrientation() * temporalFactor;
        posHelper.posController.KpPos = in.getKpPosition() * temporalFactor;
        ARMARX_IMPORTANT << "update temporal factor to " << factor;
    };
    updateTemporalFactor(temporalFactor);

    Eigen::Vector3f handPreshape = in.getHandPreshape()->toEigen();

    shapeHand(handPreshape(0), handPreshape(1), handJointPrefix);

    std::vector<Eigen::Vector3f> handClosing;
    handClosing.push_back(Eigen::Vector3f(0.16f, 0.18f, 0));
    handClosing.push_back(Eigen::Vector3f(0.30f, 0.26f, 0));
    handClosing.push_back(Eigen::Vector3f(0.39f, 0.34f, 0));
    handClosing.push_back(Eigen::Vector3f(0.47f, 0.47f, 0));
    handClosing.push_back(Eigen::Vector3f(1.00f, 1.00f, 0));
    LineStrip handClosingSequence(handClosing, 0, in.getGraspTime());

    msgHelper.setCaption("Grasp Object");


    Eigen::Matrix4f graspPose = in.getGraspPose()->toRootEigen(robot);
    graspPose = ::math::Helpers::TranslatePose(graspPose, in.getGraspOffset()->toEigen());
    Eigen::Vector3f handForwardVectorInRoot = Helpers::TransformDirection(graspPose, handForwardVectorInHandCoords);
    Eigen::Vector3f preGraspOffset = -handForwardVectorInRoot * in.getPreGraspDistance();
    Eigen::Matrix4f preGraspPose = Helpers::TranslatePose(graspPose, preGraspOffset);

    ARMARX_IMPORTANT << VAROUT(handForwardVectorInHandCoords.transpose());
    ARMARX_IMPORTANT << VAROUT(handForwardVectorInRoot.transpose());

    float handForwardZAxesDeviationDeg = Helpers::rad2deg(Helpers::SmallestAngle(handForwardVectorInRoot, -Eigen::Vector3f::UnitZ()));
    bool isTopGrasp = handForwardZAxesDeviationDeg < in.getTopGraspAngleBoundDeg();
    ARMARX_IMPORTANT << (isTopGrasp ? "Top Grasp" : "Side Grasp");
    if (isTopGrasp)
    {
        graspPose = Helpers::TranslatePose(graspPose, in.getTopGraspOffset()->toEigen());
    }

    getDebugDrawerTopic()->setPoseVisu("ExecuteGrasp", "preGraspPose", new Pose(robot->getGlobalPose() * preGraspPose));
    getDebugDrawerTopic()->setPoseVisu("ExecuteGrasp", "graspPose", new Pose(robot->getGlobalPose() * graspPose));

    posHelper.setTarget(preGraspPose);
    posHelper.addWaypoint(graspPose);

    /*ExecuteGraspPhases phases;
    phases.currentPhase = PhaseType::Approach;
    ExecuteGraspPhases::Phase approach([&]()
    {
        if (!posHelper.isLastWaypoint())
        {
            ftHelper.setZero();
        }
        if (phases.targetReached)
        {
            ARMARX_IMPORTANT << "targetReached";
            velHelper->controller->immediateHardStop();
            posHelper.setTarget(tcp->getPoseInRootFrame());
            phases.graspStartTime = TimeUtil::GetTime();
            return PhaseType::Grasp;
        }
        if (posHelper.isLastWaypoint() && phases.forceExceeded)
        {
            ARMARX_IMPORTANT << "Force exceeded: " << phases.forceNormFiltered;
            velHelper->controller->immediateHardStop();
            posHelper.setTarget(tcp->getPoseInRootFrame());
            phases.graspStartTime = TimeUtil::GetTime();
            return PhaseType::Grasp;
        }
        return PhaseType::Approach;
    });
    phases.addPhase(PhaseType::Approach, approach);*/

    PhaseType currentPhase = PhaseType::Approach;
    bool waypointIndexChanged = true;

    //posHelper.setNullSpaceControl(false);

    rtfilters::AverageFilter forceFiltered(10);
    rtfilters::AverageFilter torqueFiltered(10);

    IceUtil::Time graspStartTime = TimeUtil::GetTime();
    float forceNormFiltered = 0;
    float torqueNormFiltered = 0;
    CycleUtil c(10);
    while (!isRunningTaskStopped())
    {
        IceUtil::Time now = TimeUtil::GetTime();
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        PhaseType nextPhase = currentPhase;
        size_t currentWaypointIndex = posHelper.currentWaypointIndex;

        posHelper.update();

        Eigen::Vector3f currentTargetPos = posHelper.getCurrentTarget().block<3, 1>(0, 3);
        Eigen::Vector3f tcpPos = tcp->getPositionInRootFrame();
        getDebugDrawerTopic()->setLineVisu("ExecuteGrasp", "targetLine",
                                           new Vector3(Helpers::TransformPosition(robot->getGlobalPose(), currentTargetPos)),
                                           new Vector3(Helpers::TransformPosition(robot->getGlobalPose(), tcpPos)),
                                           2, DrawColor {0, 0, 1, 1});

        bool targetReached = posHelper.isFinalTargetReached();
        bool targetNear = posHelper.isFinalTargetNear();

        const Eigen::Vector3f currentForce = ftHelper.getForce();
        const Eigen::Vector3f currentTorque = ftHelper.getTorque();

        forceNormFiltered = forceFiltered.update(now, currentForce.norm());
        torqueNormFiltered = torqueFiltered.update(now, currentTorque.norm());

        //getDebugObserver()->setDebugDatafield("ExecuteGrasp", "force_x", new Variant(currentForce(0)));
        //getDebugObserver()->setDebugDatafield("ExecuteGrasp", "force_y", new Variant(currentForce(1)));
        //getDebugObserver()->setDebugDatafield("ExecuteGrasp", "force_z", new Variant(currentForce(2)));
        //getDebugObserver()->setDebugDatafield("ExecuteGrasp", "torque_x", new Variant(currentTorque(0)));
        //getDebugObserver()->setDebugDatafield("ExecuteGrasp", "torque_y", new Variant(currentTorque(1)));
        //getDebugObserver()->setDebugDatafield("ExecuteGrasp", "torque_z", new Variant(currentTorque(2)));

        getDebugObserver()->setDebugDatafield("ExecuteGrasp", "force", new Variant(currentForce.norm()));
        getDebugObserver()->setDebugDatafield("ExecuteGrasp", "torque", new Variant(currentTorque.norm()));
        getDebugObserver()->setDebugDatafield("ExecuteGrasp", "force_filtered", new Variant(forceNormFiltered));
        getDebugObserver()->setDebugDatafield("ExecuteGrasp", "torque_filtered", new Variant(torqueNormFiltered));

        getDebugObserver()->setDebugDatafield("ExecuteGrasp", "positionError", new Variant(posHelper.getPositionError()));
        getDebugObserver()->setDebugDatafield("ExecuteGrasp", "orientationError", new Variant(posHelper.getOrientationError()));

        bool forceExceeded = forceNormFiltered > in.getForceThreshold();
        //ARMARX_IMPORTANT << deactivateSpam(0.5) << "currentForce: " << currentForce.norm();
        getDebugObserver()->setDebugDatafield("ExecuteGrasp", "waypointIndex", new Variant(posHelper.currentWaypointIndex * 1.f));

        if (currentPhase != PhaseType::Lift)
        {
            std::stringstream ss;
            ss.precision(2);
            ss << phaseToStr(currentPhase) + " " + posHelper.getStatusText();
            ss << ", force: " << forceNormFiltered;
            msgHelper.setSubCaption(ss.str());
        }

        if (currentPhase == PhaseType::Approach)
        {
            if (!posHelper.isLastWaypoint())
            {
                ftHelper.setZero();
            }
            if (targetReached)
            {
                ARMARX_IMPORTANT << "targetReached";
                velHelper->controller->immediateHardStop();
                posHelper.setTarget(tcp->getPoseInRootFrame());
                nextPhase = PhaseType::Grasp;
                graspStartTime = TimeUtil::GetTime();
            }
            if (posHelper.isLastWaypoint() && waypointIndexChanged)
            {
                updateTemporalFactor(in.getTemporalFactorGrasp());
            }
            if (posHelper.isLastWaypoint() && forceExceeded)
            {
                ARMARX_IMPORTANT << "Force exceeded: " << forceNormFiltered;
                velHelper->controller->immediateHardStop();
                posHelper.setTarget(tcp->getPoseInRootFrame());
                nextPhase = PhaseType::Grasp;
                graspStartTime = TimeUtil::GetTime();
            }
        }
        if (currentPhase == PhaseType::Grasp)
        {
            IceUtil::Time now = TimeUtil::GetTime();
            float elapsedGraspTime = (now - graspStartTime).toSecondsDouble();
            Eigen::Vector3f handShape = handClosingSequence.Get(Helpers::Clamp(0, in.getGraspTime(), elapsedGraspTime));
            handShape = Helpers::CwiseMax(handPreshape, handShape);
            shapeHand(handShape(0), handShape(1), handJointPrefix);
            if (isTopGrasp)
            {
                posHelper.setTarget(tcp->getPoseInRootFrame());
                posHelper.setFeedForwardVelocity(in.getTopGraspLiftVelocity()->toEigen(), Eigen::Vector3f::Zero());
            }

            if (elapsedGraspTime > in.getGraspTime())
            {
                //posHelper.clearFeedForwardVelocity();
                nextPhase = PhaseType::Lift;
                posHelper.setTarget(::math::Helpers::TranslatePose(tcp->getPoseInRootFrame(), in.getLiftVector()->toEigen()));
                updateTemporalFactor(temporalFactor);
            }
        }
        if (currentPhase == PhaseType::Lift)
        {
            if (targetNear)
            {
                velHelper->controller->immediateHardStop();
                emitSuccess();
                break;
            }

            {
                std::stringstream ss;
                ss.precision(3);
                ss << "Estimated object mass: " << std::fixed << (forceNormFiltered / 10) << " kg";
                msgHelper.setMessage("Lift object", ss.str());
            }
        }

        waypointIndexChanged = currentWaypointIndex != posHelper.currentWaypointIndex;
        currentPhase = nextPhase;

        c.waitForCycleDuration();
    }

    {
        std::stringstream ss;
        ss.precision(3);
        ss << "Estimated object mass: " << std::fixed << (forceNormFiltered / 10) << " kg";
        msgHelper.setMessage("Object grasped", ss.str());
        ARMARX_IMPORTANT << ss.str();
    }


    velHelper->cleanup();
}


//void ExecuteGrasp::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ExecuteGrasp::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ExecuteGrasp::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ExecuteGrasp(stateData));
}
