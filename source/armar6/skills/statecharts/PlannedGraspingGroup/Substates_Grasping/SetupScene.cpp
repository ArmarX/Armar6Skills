/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::PlannedGraspingGroup
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SetupScene.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>

using namespace armarx;
using namespace PlannedGraspingGroup;

// DO NOT EDIT NEXT LINE
SetupScene::SubClassRegistry SetupScene::Registry(SetupScene::GetName(), &SetupScene::CreateInstance);

#define TABLEC_X_OFFSET 4413.91
#define TABLEC_Y_OFFSET 7128.92
#define TABLEC_Z_OFFSET 902.11

void SetupScene::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void SetupScene::run()
{
    if (in.getSkipSetup())
    {
        emitSuccess();
        return;
    }
    memoryx::PersistentObjectClassSegmentBasePrx classesSegmentPrx = getPriorKnowledge()->getObjectClassesSegment();
    memoryx::GridFileManagerPtr fileManager;
    memoryx::WorkingMemoryInterfacePrx workingMemory = getWorkingMemory();
    fileManager.reset(new memoryx::GridFileManager(workingMemory->getCommonStorage()));



    // Setting up table
    std::string tableClassName = "lighttable";
    memoryx::EntityBasePtr classEntity = classesSegmentPrx->getEntityByName(tableClassName);
    if (!classEntity)
    {
        ARMARX_ERROR << "No memory entity found with name " << tableClassName;
    }
    memoryx::ObjectClassPtr tableClass = memoryx::ObjectClassPtr::dynamicCast(classEntity);
    if (!tableClass)
    {
        ARMARX_ERROR << "Could not cast entitiy to object class, name: " << tableClassName;
    }

    float tableHeight = in.getTableHeight();
    float tablePosX = in.getRelativeTablePosX();
    float tablePosY = in.getRelativeTablePosY();

    auto robot = getRobotStateComponent()->getSynchronizedRobot();
    PosePtr robotGlobalPose = PosePtr::dynamicCast(robot->getGlobalPose());
    FramedPosePtr tablePose = new FramedPose(robotGlobalPose->position, robotGlobalPose->orientation, "Global", robot->getName());
    tablePose->changeFrame(robot, robot->getRootNode()->getName());
    tablePose->position->x += tablePosX;
    //    tablePose->position->x -= TABLEC_X_OFFSET;
    tablePose->position->y += tablePosY;
    tablePose->position->z = tableHeight - 900;
    tablePose->changeToGlobal(robot);
    ARMARX_INFO << "table pose: " << tablePose->output();
    //    tablePose->position->y -= TABLEC_Y_OFFSET;
    //    tablePose->position->z = tableHeight;

    memoryx::EntityWrappers::SimoxObjectWrapperPtr sw = tableClass->addWrapper(new memoryx::EntityWrappers::SimoxObjectWrapper(fileManager));
    VirtualRobot::ManipulationObjectPtr maniObjTable = sw->getManipulationObject();

    memoryx::ObjectInstancePtr newTableObj = new memoryx::ObjectInstance(tableClassName);
    newTableObj->addClass(tableClassName, 1);
    newTableObj->setExistenceCertainty(1.0);
    newTableObj->setPose(tablePose);

    NameList attributeNames = tableClass->getAttributeNames();
    for (NameList::const_iterator it = attributeNames.begin(); it != attributeNames.end(); ++it)
    {
        newTableObj->putAttribute(tableClass->getAttribute(*it));
    }

    memoryx::ObjectInstancePtr tableInstance;
    std::string objId;
    if (workingMemory->getObjectInstancesSegment()->hasEntityByName(newTableObj->getName()))
    {
        tableInstance = memoryx::ObjectInstancePtr::dynamicCast(workingMemory->getObjectInstancesSegment()->getEntityByName(newTableObj->getName()));
        objId = tableInstance->getId();
        workingMemory->getObjectInstancesSegment()->setObjectPoseWithoutMotionModel(objId, newTableObj->getPose());
    }
    else
    {
        objId = workingMemory->getObjectInstancesSegment()->addEntity(newTableObj);
        tableInstance = memoryx::ObjectInstancePtr::dynamicCast(workingMemory->getObjectInstancesSegment()->getEntityById(objId));
    }


    // Setting up "wide" table
    if (in.getUseWideTable())
    {
        // Calculate width of table
        float tableWidth = maniObjTable->getCollisionModel()->getBoundingBox(true).getMax()[0] - maniObjTable->getCollisionModel()->getBoundingBox(true).getMin()[0];
        for (int i = 0; i < 2; i++)
        {
            tableWidth = (i % 2) == 0 ? tableWidth : -tableWidth;
            FramedPosePtr wideTablePose = new FramedPose(*tablePose);
            wideTablePose->changeFrame(robot, robot->getRootNode()->getName());
            wideTablePose->position->x += tableWidth;
            wideTablePose->changeToGlobal(robot);

            std::string tableInstanceName = tableClassName + ((i % 2) == 0 ? "1" : "2");
            newTableObj = new memoryx::ObjectInstance(tableInstanceName);
            newTableObj->addClass(tableClassName, 1);
            newTableObj->setExistenceCertainty(1.0);
            newTableObj->setPose(wideTablePose);

            NameList attributeNames = tableClass->getAttributeNames();
            for (NameList::const_iterator it = attributeNames.begin(); it != attributeNames.end(); ++it)
            {
                newTableObj->putAttribute(tableClass->getAttribute(*it));
            }

            std::string objId;
            if (workingMemory->getObjectInstancesSegment()->hasEntityByName(newTableObj->getName()))
            {
                objId = memoryx::ObjectInstancePtr::dynamicCast(workingMemory->getObjectInstancesSegment()->getEntityByName(newTableObj->getName()))->getId();
                workingMemory->getObjectInstancesSegment()->setObjectPoseWithoutMotionModel(objId, newTableObj->getPose());
            }
            else
            {
                objId = workingMemory->getObjectInstancesSegment()->addEntity(newTableObj);
            }
        }
    }
    else
    {
        for (int i = 0; i < 2; i++)
        {
            std::string s = tableClassName + ((i % 2) == 0 ? "1" : "2");
            if (workingMemory->getObjectInstancesSegment()->hasEntityByName(s))
            {
                std::string id = memoryx::ObjectInstancePtr::dynamicCast(workingMemory->getObjectInstancesSegment()->getEntityByName(s))->getId();
                workingMemory->getObjectInstancesSegment()->removeEntity(id);
            }
        }
    }


    // Setting up Object
    std::string objectName = in.getObjectName();

    classEntity = classesSegmentPrx->getEntityByName(objectName);
    if (!classEntity)
    {
        ARMARX_ERROR << "No memory entity found with name " << objectName;
    }
    memoryx::ObjectClassPtr objectClass = memoryx::ObjectClassPtr::dynamicCast(classEntity);
    if (!objectClass)
    {
        ARMARX_ERROR << "Could not cast entitiy to object class, name: " << objectName;
    }

    // determine placing pose
    sw = objectClass->addWrapper(new memoryx::EntityWrappers::SimoxObjectWrapper(fileManager));
    VirtualRobot::ManipulationObjectPtr maniObj = sw->getManipulationObject();
    Eigen::Matrix4f globalPose = maniObj->getGlobalPose();

    float tableZmax = maniObjTable->getCollisionModel()->getBoundingBox(true).getMax()[2];

    ARMARX_INFO << "robot pose: " << robot->getGlobalPose()->output();
    FramedPosePtr objPose = new FramedPose(QuaternionPtr::dynamicCast(robotGlobalPose->orientation)->toEigen() * in.getRelativeObjectOrientation()->toEigen(), globalPose.block<3, 1>(0, 3), GlobalFrame, "");
    //    objPose->orientation = ;
    ARMARX_INFO << "ori before: " << objPose->output() ;
    objPose->changeFrame(robot, robot->getRootNode()->getName());
    ARMARX_INFO << "ori after change to platfrom-frame: " << objPose->output() ;
    //    maniObj->setGlobalPose(objPose->toEigen());
    float poseCenter_zOffset = in.getObjectModelHeight();
    float objectPosZ = tableZmax + poseCenter_zOffset + tableHeight - 900;

    objPose->position->x = in.getRelativeObjectPosX();
    objPose->position->y = in.getRelativeObjectPosY();
    objPose->position->z = objectPosZ;
    objPose->changeToGlobal(robot);

    ARMARX_INFO << "ori after change to global-frame: " << objPose->output() ;
    memoryx::ObjectInstancePtr newObj = new memoryx::ObjectInstance(objectName);
    newObj->addClass(objectName, 1);
    newObj->setExistenceCertainty(1.0);
    newObj->setPose(objPose);

    attributeNames = objectClass->getAttributeNames();
    for (NameList::const_iterator it = attributeNames.begin(); it != attributeNames.end(); ++it)
    {
        newObj->putAttribute(objectClass->getAttribute(*it));
    }

    memoryx::ObjectInstancePtr objectInstance;
    if (workingMemory->getObjectInstancesSegment()->hasEntityByName(newObj->getName()))
    {
        objectInstance = memoryx::ObjectInstancePtr::dynamicCast(workingMemory->getObjectInstancesSegment()->getEntityByName(newObj->getName()));
        objId = objectInstance->getId();
        if (in.getUpdateObjectPoseInWorkingMemory())
        {
            workingMemory->getObjectInstancesSegment()->setObjectPoseWithoutMotionModel(objId, newObj->getPose());
        }
    }
    else
    {
        objId = workingMemory->getObjectInstancesSegment()->addEntity(newObj);
        objectInstance = memoryx::ObjectInstancePtr::dynamicCast(workingMemory->getObjectInstancesSegment()->getEntityById(objId));
    }

    out.setObjectInstanceId(objId);

    ARMARX_INFO << "Setting up Scene was successful";

    emitSuccess();



    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //    }
}

//void SetupScene::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void SetupScene::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SetupScene::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SetupScene(stateData));
}
