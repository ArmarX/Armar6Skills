/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::PlannedGraspingGroup
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SolveIKAndSetMotionPlanningConfigs.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <VirtualRobot/Grasping/GraspSet.h>
#include <MemoryX/libraries/observers/ObjectMemoryObserver.h>
#include <RobotAPI/interface/components/ViewSelectionInterface.h>

using namespace armarx;
using namespace PlannedGraspingGroup;

// DO NOT EDIT NEXT LINE
SolveIKAndSetMotionPlanningConfigs::SubClassRegistry SolveIKAndSetMotionPlanningConfigs::Registry(SolveIKAndSetMotionPlanningConfigs::GetName(), &SolveIKAndSetMotionPlanningConfigs::CreateInstance);



void SolveIKAndSetMotionPlanningConfigs::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void SolveIKAndSetMotionPlanningConfigs::run()
{
    memoryx::PersistentObjectClassSegmentBasePrx classesSegmentPrx = getPriorKnowledge()->getObjectClassesSegment();
    memoryx::GridFileManagerPtr fileManager;
    memoryx::WorkingMemoryInterfacePrx workingMemory = getWorkingMemory();
    fileManager.reset(new memoryx::GridFileManager(workingMemory->getCommonStorage()));


    // Calculate Pose of GraspPrePose

    std::string graspPreposeName = in.getGraspPreposeName();
    std::string graspSetName = in.getGraspSetName();

    memoryx::ObjectInstancePtr objectInstance = memoryx::ObjectInstancePtr::dynamicCast(workingMemory->getObjectInstancesSegment()->getObjectInstanceById(in.getObjectInstanceId()));

    std::string objectClassName = objectInstance->getMostProbableClass();
    memoryx::EntityBasePtr classEntity = classesSegmentPrx->getEntityByName(objectClassName);
    if (!classEntity)
    {
        ARMARX_ERROR << "No memory entity found with name " << objectClassName;
    }
    memoryx::ObjectClassPtr objectClass = memoryx::ObjectClassPtr::dynamicCast(classEntity);
    if (!objectClass)
    {
        ARMARX_ERROR << "Could not cast entitiy to object class, name: " << objectClassName;
    }
    memoryx::EntityWrappers::SimoxObjectWrapperPtr simoxWrapper = objectClass->addWrapper(new memoryx::EntityWrappers::SimoxObjectWrapper(fileManager));
    VirtualRobot::ManipulationObjectPtr mo = simoxWrapper->getManipulationObject();

    //    auto tcpObjectClass = classesSegmentPrx->getObjectClassByName(in.getHandNameInMemory());
    //    if (tcpObjectClass)
    //    {
    //        context->getEntityDrawerTopic()->setObjectVisu("VisualServoHandVisu", "tcpTarget_" + chainName, tcpObjectClass, new Pose());
    //        context->getEntityDrawerTopic()->updateObjectTransparency("VisualServoHandVisu", "tcpTarget_" + chainName, 0.3);
    //    }
    //    else
    //    {
    //        ARMARX_IMPORTANT << "Could not get objectclass for tcp " << in.getHandNameInMemory() << " for visu purposes";
    //    }

    FramedPosePtr framedTcpPose;

    if (mo->getGraspSet(graspSetName))
    {
        VirtualRobot::GraspPtr grasp = mo->getGraspSet(graspSetName)->getGrasp(graspPreposeName);

        if (!grasp)
        {
            ARMARX_ERROR << "No grasp with name " << graspPreposeName << "in grasp set " << graspSetName << " found!";
            emitFailure();
        }
        else
        {
            Eigen::Matrix4f objectPose = objectInstance->getPose()->toEigen();
            auto tcpPose = grasp->getTcpPoseGlobal(objectPose); // tcpPose is global
            framedTcpPose = new FramedPose(tcpPose, objectInstance->getPose()->frame, objectInstance->getPose()->agent);
        }
    }
    else
    {
        std::stringstream s;
        for (auto set : mo->getAllGraspSets())
        {
            s << set->getName() << ", ";
        }
        ARMARX_ERROR << "No grasp set with name " << graspSetName << " found for object class " << objectClass->getName() << "! sets: " << s.str();
        emitFailure();
    }

    // Check if valid IK-Solution exists

    RobotIKInterfacePrx robotIK = getRobotIK();
    std::string rns = in.getRobotNodeSet();
    SharedRobotInterfacePrx localRobot = getRobotStateComponent()->getSynchronizedRobot();

    auto robotPoseEigen = getRobot()->getGlobalPose();
    // objectPose is actually the tcp pose...
    auto objectPose = framedTcpPose->toGlobalEigen(localRobot);
    auto objectPoseRelativeToRobotEigen = robotPoseEigen.inverse() * objectPose;
    FramedPosePtr objectPoseRelativeToRobot {new FramedPose(objectPoseRelativeToRobotEigen, localRobot->getRootNode()->getName(), localRobot->getName())};

    NameValueMap desiredRobotConfig;
    if (localRobot->hasRobotNodeSet(rns))
    {
        desiredRobotConfig = robotIK->computeIKFramedPose(rns, objectPoseRelativeToRobot, eAll);
    }
    else
    {
        ARMARX_ERROR << "Could not find RNS '" << rns << "' in RNS list of robot " << localRobot->getName();
        emitFailure();
    }

    if (desiredRobotConfig.empty())
    {
        ARMARX_WARNING << "Could not find IK-Solution for pose: " << VAROUT(*objectPoseRelativeToRobot);
        emitFailure();
    }

    // If yes, then set configs for motion planning

    ARMARX_INFO << "DesiredRobotConfig: " << VAROUT(desiredRobotConfig);
    ARMARX_INFO << "StartRobotConfig: " << VAROUT(localRobot->getConfig());
    out.setTargetConfig(desiredRobotConfig);
    out.setStartConfig(localRobot->getConfig());
    out.setInitialAgentPose(new Pose(robotPoseEigen));
    out.setKinematicChainNames({rns});


    // Setting actual grasp pose and handMemoryChannel
    std::string actualGraspName = in.getGraspName();
    if (mo->getGraspSet(graspSetName))
    {
        VirtualRobot::GraspPtr grasp = mo->getGraspSet(graspSetName)->getGrasp(actualGraspName);

        if (!grasp)
        {
            ARMARX_ERROR << "No grasp with name " << actualGraspName << "in grasp set " << graspSetName << " found!";
            emitFailure();
        }
        else
        {
            Eigen::Matrix4f objectPose = objectInstance->getPose()->toEigen();
            auto tcpPose = grasp->getTcpPoseGlobal(objectPose); // tcpPose is global
            framedTcpPose = new FramedPose(tcpPose, objectInstance->getPose()->frame, objectInstance->getPose()->agent);
        }
    }
    else
    {
        ARMARX_ERROR << "No grasp set with name " << graspSetName << " found! ";
        emitFailure();
    }

    out.setGraspTCPPose(framedTcpPose);

    //    // get memory channel for the hand
    //    std::string handName = in.getHandNameInMemory();
    //    ChannelRefBasePtr handMemoryChannel = getObjectMemoryObserver()->requestObjectClassRepeated(handName, 150, armarx::DEFAULT_VIEWTARGET_PRIORITY);

    //    if (handMemoryChannel)
    //    {
    //        handMemoryChannel->validate();
    //        out.setHandMemoryChannel(ChannelRefPtr::dynamicCast(handMemoryChannel));
    //    }
    //    else
    //    {
    //        emitFailure();
    //        ARMARX_WARNING << "Unknown Object Class: " << handName;
    //    }

    emitSuccess();
}

//void SolveIKAndSetMotionPlanningConfigs::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void SolveIKAndSetMotionPlanningConfigs::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SolveIKAndSetMotionPlanningConfigs::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SolveIKAndSetMotionPlanningConfigs(stateData));
}
