/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::PlannedGraspingGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "VerifyObjectPose.h"

#include <ArmarXCore/observers/variant/TimestampVariant.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace PlannedGraspingGroup;

// DO NOT EDIT NEXT LINE
VerifyObjectPose::SubClassRegistry VerifyObjectPose::Registry(VerifyObjectPose::GetName(), &VerifyObjectPose::CreateInstance);



void VerifyObjectPose::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    auto variant = in.getObjectInstanceChannel()->getDataField("pose");
    ARMARX_CHECK_EXPRESSION(variant) << in.getObjectInstanceChannel()->getChannelName();
    FramedPosePtr currentPose = variant->get<FramedPose>();
    ARMARX_CHECK_EXPRESSION(currentPose) << in.getObjectInstanceChannel()->getChannelName() << " - " << variant->ice_id();
    auto timestamp = in.getObjectInstanceChannel()->getDataField("timestamp")->get<TimestampVariant>()->getTimestamp();
    RemoteRobot::synchronizeLocalCloneToTimestamp(getLocalRobot(), getRobotStateComponent(), timestamp);
    currentPose->changeToGlobal(getLocalRobot());
    out.setObjectPose(currentPose);
    out.setObjectInstanceChannel(in.getObjectInstanceChannel());
    ARMARX_CHECK_EXPRESSION(currentPose->frame == GlobalFrame) << "actual frame: " << currentPose->frame;
    ARMARX_CHECK_EXPRESSION(in.getInitialObjectPose()->frame == GlobalFrame);
    Eigen::Matrix4f deltaPose = in.getInitialObjectPose()->toEigen().inverse() * currentPose->toEigen();
    Eigen::AngleAxisf aa(deltaPose.block<3, 3>(0, 0));
    if (aa.angle() > in.getMaxAllowedOrientationDifference())
    {
        ARMARX_INFO << "Orientation error is too big: " << aa.angle() << " allowed: " << in.getMaxAllowedOrientationDifference();
        emitFailure();
        return;
    }
    float posDifference = deltaPose.block<3, 1>(0, 3).norm();
    if (posDifference > in.getMaxAllowedPositionDifference())
    {
        ARMARX_INFO << "Position error is too big: " << posDifference << " allowed: " << in.getMaxAllowedPositionDifference();
        emitFailure();
        return;
    }

    emitSuccess();
}

//void VerifyObjectPose::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void VerifyObjectPose::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void VerifyObjectPose::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr VerifyObjectPose::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new VerifyObjectPose(stateData));
}
