/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::PlannedGraspingGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <armar6/skills/statecharts/PlannedGraspingGroup/GraspingPipelineWithVision.generated.h>

namespace armarx::PlannedGraspingGroup
{
    class GraspingPipelineWithVision :
        public GraspingPipelineWithVisionGeneratedBase < GraspingPipelineWithVision >
    {
    public:
        GraspingPipelineWithVision(const XMLStateConstructorParams& stateData):
            XMLStateTemplate < GraspingPipelineWithVision > (stateData), GraspingPipelineWithVisionGeneratedBase < GraspingPipelineWithVision > (stateData)
        {
        }

        // inherited from StateBase
        void onEnter() override;
        // void run() override;
        // void onBreak() override;
        void onExit() override;

        // static functions for AbstractFactory Method
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static SubClassRegistry Registry;

        // DO NOT INSERT ANY CLASS MEMBERS,
        // use stateparameters instead,
        // if classmember are neccessary nonetheless, reset them in onEnter

        // GraspingPipelineWithVisionGeneratedBase interface
    public:
        void transitionObjectInstanceChannelFoundFromGetObjectInstance(PlannedGraspingGroup::VerifyObjectPoseIn& next, const VisualServoGroup::GetObjectInstanceOut& prev) override;

        //            void transitionSuccessFromPlayTrajectory(HandGroup::CloseHandIn& next, const PlayTrajectoryOut& prev) override;

        // GraspingPipelineWithVisionGeneratedBase interface
    public:
        void transitionSuccessFromIf1(ScanLocationGroup::ScanWorkspaceIn& next, const CoreUtility::If1Out& prev) override;
        void transitionSuccessFromScanWorkspace(GraspingPipelineGroup::CalculateTrajectoryIn& next, const ScanLocationGroup::ScanWorkspaceOut& prev) override;

        // GraspingPipelineWithVisionGeneratedBase interface
    public:
        void transitionSuccessFromIfVerifyPose(VisualServoGroup::GetObjectInstanceIn& next, const CoreUtility::If1Out& prev) override;

        // GraspingPipelineWithVisionGeneratedBase interface
    public:
        void transitionSuccessFromPlayTrajectory(ExecuteGraspIn& next, const PlayTrajectoryOut& prev) override;
    };
}
