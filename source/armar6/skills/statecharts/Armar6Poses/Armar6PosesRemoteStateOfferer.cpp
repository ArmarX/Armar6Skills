/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6Poses::Armar6PosesRemoteStateOfferer
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6PosesRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6Poses;

// DO NOT EDIT NEXT LINE
Armar6PosesRemoteStateOfferer::SubClassRegistry Armar6PosesRemoteStateOfferer::Registry(Armar6PosesRemoteStateOfferer::GetName(), &Armar6PosesRemoteStateOfferer::CreateInstance);



Armar6PosesRemoteStateOfferer::Armar6PosesRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6PosesStatechartContext > (reader)
{
}

void Armar6PosesRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6PosesRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6PosesRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6PosesRemoteStateOfferer::GetName()
{
    return "Armar6PosesRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6PosesRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6PosesRemoteStateOfferer(reader));
}
