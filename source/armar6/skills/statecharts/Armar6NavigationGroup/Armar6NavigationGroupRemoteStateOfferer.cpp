/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6NavigationGroup::Armar6NavigationGroupRemoteStateOfferer
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6NavigationGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6NavigationGroup;

// DO NOT EDIT NEXT LINE
Armar6NavigationGroupRemoteStateOfferer::SubClassRegistry Armar6NavigationGroupRemoteStateOfferer::Registry(Armar6NavigationGroupRemoteStateOfferer::GetName(), &Armar6NavigationGroupRemoteStateOfferer::CreateInstance);



Armar6NavigationGroupRemoteStateOfferer::Armar6NavigationGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6NavigationGroupStatechartContext > (reader)
{
}

void Armar6NavigationGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6NavigationGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6NavigationGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6NavigationGroupRemoteStateOfferer::GetName()
{
    return "Armar6NavigationGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6NavigationGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6NavigationGroupRemoteStateOfferer(reader));
}
