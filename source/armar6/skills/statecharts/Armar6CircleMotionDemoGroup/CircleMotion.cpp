/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6CircleMotionDemoGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CircleMotion.h"

#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityController.h>

#include <ArmarXCore/core/time/TimeUtil.h>

#include <RobotAPI/libraries/core/CartesianPositionController.h>

#include <RobotAPI/libraries/core/math/MathUtils.h>
#include <RobotAPI/libraries/core/FramedPose.h>


using namespace armarx;
using namespace Armar6CircleMotionDemoGroup;

// DO NOT EDIT NEXT LINE
CircleMotion::SubClassRegistry CircleMotion::Registry(CircleMotion::GetName(), &CircleMotion::CreateInstance);



void CircleMotion::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void CircleMotion::run()
{
    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());

    NJointCartesianVelocityControllerConfigPtr config = new NJointCartesianVelocityControllerConfig(in.getNodeSetName(), "", NJointCartesianVelocityControllerMode::eAll);

    NJointCartesianVelocityControllerInterfacePrx extendedTCPController = NJointCartesianVelocityControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController(RobotUnitControllerNames::NJointCartesianVelocityController, "extendedTCPController", config));
    extendedTCPController->activateController();

    IceUtil::Time start = TimeUtil::GetTime();

    Eigen::Vector3f center = in.getCenter()->toEigen();
    float radius = in.getRadius();
    float period = in.getPeriod();
    Eigen::Vector3f u, v;
    u << 1, 0, 0;
    v << 0, 0, 1;
    u = u.normalized() * radius;
    v = v.normalized() * radius;

    VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet(in.getNodeSetName());
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();

    CartesianPositionController posController(tcp);

    float KpAvoidJointLimits = in.getKpAvoidJointLimits();
    float startT = in.getStartT();

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

        IceUtil::Time now = TimeUtil::GetTime();

        float t = (now - start).toSecondsDouble() * 2 * M_PI / period + startT;

        ARMARX_IMPORTANT << math::MathUtils::angleMod2PI(t);

        getDebugObserver()->setDebugDatafield("CircleMotion", "t", new Variant(math::MathUtils::angleMod2PI(t)));
        getDebugObserver()->setDebugDatafield("CircleMotion", "cos t", new Variant(cos(t)));
        getDebugObserver()->setDebugDatafield("CircleMotion", "sin t", new Variant(sin(t)));

        Eigen::Vector3f targetPos = center + cos(t) * u + sin(t) * v;

        Eigen::Vector3f dir = targetPos - tcp->getPositionInRootFrame();
        dir *= in.getKpPosition();

        Eigen::Vector3f oriX, oriY, oriZ;
        oriX = cos(t) * u + sin(t) * v;
        oriZ << 0, 1, 0;
        oriY = -oriX.cross(oriZ);

        Eigen::Matrix3f targetOri;
        targetOri.block<3, 1>(0, 0) = oriX.normalized();
        targetOri.block<3, 1>(0, 1) = oriY.normalized();
        targetOri.block<3, 1>(0, 2) = oriZ.normalized();

        Eigen::Matrix4f targetPose = Eigen::Matrix4f::Identity();
        targetPose.block<3, 3>(0, 0) = targetOri;
        targetPose.block<3, 1>(0, 3) = targetPos;

        Eigen::VectorXf cartesianVelocity = posController.calculate(targetPose, VirtualRobot::IKSolver::CartesianSelection::All);

        Eigen::Matrix3f tcpOri = tcp->getPoseInRootFrame().block<3, 3>(0, 0);
        Eigen::Matrix3f oriDir =  targetOri * tcpOri.inverse();
        Eigen::AngleAxisf aa(oriDir);

        FramedPosePtr targetPosePtr = new FramedPose(targetPose, localRobot->getRootNode()->getName(), localRobot->getName());
        FramedPosePtr tcpPosePtr = new FramedPose(tcp->getPoseInRootFrame(), localRobot->getRootNode()->getName(), localRobot->getName());

        getDebugDrawerTopic()->setPoseVisu("CircleMotion", "target", targetPosePtr->toGlobal(localRobot));
        getDebugDrawerTopic()->setPoseVisu("CircleMotion", "tcp", tcpPosePtr->toGlobal(localRobot));

        Eigen::Vector3f oriVel = aa.axis() * aa.angle();
        ARMARX_IMPORTANT << "oriVel: " << oriVel.transpose();
        //oriVel = Eigen::Vector3f::Zero();
        oriVel *= in.getKpOrientation();

        //extendedTCPController->setControllerTarget(dir(0), dir(1), dir(2), oriVel(0), oriVel(1), oriVel(2), 1, NJointCartesianVelocityControllerMode::eAll);
        extendedTCPController->setControllerTarget(
            cartesianVelocity(0), cartesianVelocity(1), cartesianVelocity(2),
            cartesianVelocity(3), cartesianVelocity(4), cartesianVelocity(5),
            KpAvoidJointLimits, NJointCartesianVelocityControllerMode::eAll);

        getDebugObserver()->setDebugDatafield("CircleMotion", "vpx", new Variant(cartesianVelocity(0) / 1000));
        getDebugObserver()->setDebugDatafield("CircleMotion", "vpy", new Variant(cartesianVelocity(1) / 1000));
        getDebugObserver()->setDebugDatafield("CircleMotion", "vpz", new Variant(cartesianVelocity(2) / 1000));
        getDebugObserver()->setDebugDatafield("CircleMotion", "vrx", new Variant(cartesianVelocity(3)));
        getDebugObserver()->setDebugDatafield("CircleMotion", "vry", new Variant(cartesianVelocity(4)));
        getDebugObserver()->setDebugDatafield("CircleMotion", "vrz", new Variant(cartesianVelocity(5)));

        //extendedTCPController->setControllerTarget(dir(0), dir(1), dir(2), 0, 1, 0, 1, NJointCartesianVelocityControllerMode::ePosition);
        //extendedTCPController->setControllerTarget(dir(0), dir(1), dir(2), 0, 0, 0, 1, NJointCartesianVelocityControllerMode::eAll);
        usleep(10000);
    }

    extendedTCPController->deactivateController();
    while (extendedTCPController->isControllerActive())
    {
        usleep(10000);
    }

    extendedTCPController->deleteController();
}

//void CircleMotion::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void CircleMotion::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr CircleMotion::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new CircleMotion(stateData));
}
