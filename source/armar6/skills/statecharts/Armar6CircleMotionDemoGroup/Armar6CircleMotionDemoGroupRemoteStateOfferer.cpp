/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6CircleMotionDemoGroup::Armar6CircleMotionDemoGroupRemoteStateOfferer
 * @author     [Author Name] ( [Author Email] )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6CircleMotionDemoGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6CircleMotionDemoGroup;

// DO NOT EDIT NEXT LINE
Armar6CircleMotionDemoGroupRemoteStateOfferer::SubClassRegistry Armar6CircleMotionDemoGroupRemoteStateOfferer::Registry(Armar6CircleMotionDemoGroupRemoteStateOfferer::GetName(), &Armar6CircleMotionDemoGroupRemoteStateOfferer::CreateInstance);



Armar6CircleMotionDemoGroupRemoteStateOfferer::Armar6CircleMotionDemoGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6CircleMotionDemoGroupStatechartContext > (reader)
{
}

void Armar6CircleMotionDemoGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6CircleMotionDemoGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6CircleMotionDemoGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6CircleMotionDemoGroupRemoteStateOfferer::GetName()
{
    return "Armar6CircleMotionDemoGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6CircleMotionDemoGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6CircleMotionDemoGroupRemoteStateOfferer(reader));
}
