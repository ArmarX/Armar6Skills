/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::BimanualBoxManipulation
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveBothHandsAlongWaypoints.h"


#include <RobotAPI/libraries/RobotStatechartHelpers/ForceTorqueHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/PositionControllerHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>

#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>

#include <VirtualRobot/math/Helpers.h>

#include <ArmarXCore/core/time/CycleUtil.h>

using namespace armarx;
using namespace BimanualBoxManipulation;

// DO NOT EDIT NEXT LINE
MoveBothHandsAlongWaypoints::SubClassRegistry MoveBothHandsAlongWaypoints::Registry(MoveBothHandsAlongWaypoints::GetName(), &MoveBothHandsAlongWaypoints::CreateInstance);



void MoveBothHandsAlongWaypoints::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

//void MoveBothHandsAlongWaypoints::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//    // get a private kinematic instance for this state of the robot (tick "Robot State Component" proxy checkbox in statechart group)
//    VirtualRobot::RobotPtr robot = getLocalRobot();
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//        // synchronize robot clone to most recent state
//        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
//    }
//}

//void MoveBothHandsAlongWaypoints::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MoveBothHandsAlongWaypoints::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveBothHandsAlongWaypoints::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveBothHandsAlongWaypoints(stateData));
}
