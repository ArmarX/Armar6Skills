/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::BimanualBoxManipulation
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveHandAlongWaypoints.h"

#include <RobotAPI/libraries/RobotStatechartHelpers/ForceTorqueHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/PositionControllerHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>

#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>

#include <VirtualRobot/math/Helpers.h>

#include <ArmarXCore/core/time/CycleUtil.h>

using namespace armarx;
using namespace BimanualBoxManipulation;

// DO NOT EDIT NEXT LINE
MoveHandAlongWaypoints::SubClassRegistry MoveHandAlongWaypoints::Registry(MoveHandAlongWaypoints::GetName(), &MoveHandAlongWaypoints::CreateInstance);



void MoveHandAlongWaypoints::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void MoveHandAlongWaypoints::run()
{
    std::string side = in.getSide();
    RobotNameHelperPtr nameHelper = getRobotNameHelper();
    RobotNameHelper::Arm arm = nameHelper->getArm(side);
    VirtualRobot::RobotPtr robot = getLocalRobot();
    ::armarx::RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
    VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
    VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

    KinematicUnitHelper kinUnitHelper(getKinematicUnit());
    DebugDrawerHelper debugDrawerHelper(getDebugDrawerTopic(), "BimanualBoxManipulation", robot);
    std::string controllerName = in.getCartesianVelocityControllerName() + "_" + side;
    VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(getRobotUnit(), arm.getKinematicChain(), controllerName));
    ForceTorqueHelper ftHelper(getForceTorqueObserver(), arm.getForceTorqueSensor());

    Eigen::Matrix4f initial_tcp_pose = tcp->getPoseInRootFrame();

    PositionControllerHelper posHelper(tcp, velHelper, initial_tcp_pose);
    posHelper.readConfig(in.getControllerConfig());

    std::vector<armarx::PosePtr> waypoints = in.getTcpWaypoints()[side]->toStdVector<armarx::PosePtr>();
    for (auto& waypoint : waypoints)
    {
        posHelper.addWaypoint(waypoint->toEigen());
    }

    Eigen::Vector3f initial_tcp_position = math::Helpers::Position(initial_tcp_pose);
    if (posHelper.waypoints.size() > 1)
    {
        Eigen::Vector3f start_position = math::Helpers::Position(posHelper.waypoints[1]);
        Eigen::Vector3f tcp_to_start = start_position - initial_tcp_position;
        float distance_tcp_to_start_in_mm = tcp_to_start.norm();
        ARMARX_INFO << "Distance TCP <-> start: " << distance_tcp_to_start_in_mm << " mm";
        if (distance_tcp_to_start_in_mm > in.getMaximalDistanceToStartInMM())
        {
            ARMARX_WARNING << "Maximal distance between start and TCP exceeded: " << distance_tcp_to_start_in_mm
                           << " mm > " << in.getMaximalDistanceToStartInMM() << " mm";
            ARMARX_WARNING << "Motion is not started. Are you in the right configuration to start this motion?";
            emitFailure();
            return;
        }
    }


    CycleUtil c(IceUtil::Time::milliSeconds(10));
    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        posHelper.updateRead();

        bool targetReached = posHelper.isFinalTargetReached();

        if (targetReached)
        {
            posHelper.immediateHardStop();
            emitSuccess();
        }

        posHelper.updateWrite();

        c.waitForCycleDuration();
    }

    posHelper.immediateHardStop();
    posHelper.updateWrite();
}

//void MoveHandAlongWaypoints::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MoveHandAlongWaypoints::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveHandAlongWaypoints::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveHandAlongWaypoints(stateData));
}
