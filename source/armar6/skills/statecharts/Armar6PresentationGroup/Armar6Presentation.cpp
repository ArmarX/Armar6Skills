/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6PresentationGroup
 * @author     Sonja Marahrens ( sonja dot marahrens )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6Presentation.h"
#include "InterruptedException.h"

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/math/Helpers.h>

#include <Eigen/Geometry>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/core/time/TimeUtil.h>

//#include <ArmarXCore/observers/variant/DatafieldRef.>

#include <RobotAPI/libraries/SimpleTrajectory/Trajectory.h>


using namespace armarx;
using namespace Armar6PresentationGroup;

using Time = IceUtil::Time;


// DO NOT EDIT NEXT LINE
Armar6Presentation::SubClassRegistry Armar6Presentation::Registry(Armar6Presentation::GetName(), &Armar6Presentation::CreateInstance);

void Armar6Presentation::onEnter()
{
    auto display = in.getDisplay_Text();
    if (display.size() == 2)
    {
        getMessageDisplay()->setMessage(display[0], display[1]);
    }
    else
    {
        ARMARX_WARNING << "Display text not set (wrong size): " << display.size();
    }
}


void Armar6Presentation::switchControlMode(ControlMode mode)
{
    const std::set<std::string> ignored {"RightHandFingers", "RightHandThumb", "LeftHandFingers", "LeftHandThumb"};

    NameControlModeMap modeMap;
    for (const std::string& name : in.getControlledJoints())
    {
        if (ignored.count(name) == 0)
        {
            modeMap[name] = mode;
        }
    }
    getKinematicUnit()->switchControlMode(modeMap);
}


void Armar6Presentation::setJointAngles(const NameValueMap& values, VirtualRobot::RobotPtr localRobot,
                                        const std::string& name)
{
    if (!name.empty())
    {
        std::stringstream ss;
        for (const auto& [name, value] : values)
        {
            ss << name << " -> " << value << "\n";
        }
        ARMARX_INFO << "Set joint angles: " << name << "\n.Values: \n" << ss.str();
    }

    if (in.getInSimulation())
    {
        setJointAnglesSimulation(values, localRobot);
    }
    else
    {
        getKinematicUnit()->setJointAngles(values);
    }
}


void Armar6Presentation::setJointAnglesSimulation(const NameValueMap& values, VirtualRobot::RobotPtr _localRobot)
{
    ARMARX_CHECK(_localRobot) << "Robot must not be null.";

    // Clone the robot to use it asynchronously later.
    VirtualRobot::RobotPtr localRobot = _localRobot->clone();
    RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

    const float speedDefault = in.getSimulationJointAngleSpeed();
    const NameValueMap speedOverride = in.getSimulationJointAngleSpeedOverride();

    // Build trajectory.
    float maxDuration = 0;

    trajectory::Trajectory traj;
    for (const auto& [name, targetValue] : values)
    {
        const float speed = speedOverride.count(name) > 0 ? speedOverride.at(name) : speedDefault;

        const float currentValue = localRobot->getRobotNode(name)->getJointValue();
        const float duration = std::abs(targetValue - currentValue) / speed;

        trajectory::VariantTrack& track = traj.addTrack(name);
        track[0] = currentValue;
        track[duration] = targetValue;

        if (duration > maxDuration)
        {
            maxDuration = duration;
        }
    }
    ARMARX_VERBOSE << "Trajectory (duration: " << maxDuration << "): " << traj;


    // Run trajectory asynchronously.

    if (setJointAnglesSimulationTask && setJointAnglesSimulationTask->isRunning())
    {
        setJointAnglesSimulationTask->stop();
    }

    setJointAnglesSimulationTask  = new armarx::SimpleRunningTask<>(
        [this, values, localRobot, maxDuration, traj]()
    {
        CycleUtil cycle(Time::milliSeconds(10));
        const Time start = TimeUtil::GetTime();
        while (!this->isRunningTaskStopped())
        {
            const float t = static_cast<float>((TimeUtil::GetTime() - start).toSecondsDouble());
            if (t > maxDuration)
            {
                break;
            }

            NameValueMap nextValues;
            StringVariantBaseMap reportedValues;
            for (const auto& [name, _] : values)
            {
                const float value = std::get<float>(traj[name].at(t));
                nextValues[name] = value;
                reportedValues[name] = new Variant(value);
            }

            getDebugObserver()->setDebugChannel("Trajectory Values", reportedValues);
            getKinematicUnit()->setJointAngles(nextValues);

            cycle.waitForCycleDuration();
        }

        if (this->isRunningTaskStopped())
        {
            throw InterruptedException();
        }
    }, "setJointAnglesSimulation");
    setJointAnglesSimulationTask->start();
}

void Armar6Presentation::speak(const std::string& text)
{
    ARMARX_INFO << "Saying '" << text.substr(0, 50) << "...'";
    getTextToSpeech()->reportText(text);
}


void Armar6Presentation::waitForSpeechFinished()
{
    TimeUtil::SleepMS(20);
    while (!isRunningTaskStopped())
    {
        ARMARX_INFO << deactivateSpam(1) << "Speech observer status: "
                    << getSpeechObserver()->getDatafieldByName("TextToSpeech", "State")->getString();
        if (getSpeechObserver()->getDatafieldByName("TextToSpeech", "State")->getString() == "FinishedSpeaking")
        {
            ARMARX_IMPORTANT << "Speech observer status: " << getSpeechObserver()->getDatafieldByName("TextToSpeech", "State")->getString();
            break;
        }
        TimeUtil::SleepMS(10);
    }
    if (isRunningTaskStopped())
    {
        throw InterruptedException();
    }
}

void Armar6Presentation::waitForMoveFinish(const NameValueMap& values, VirtualRobot::RobotPtr localRobot, const std::string& name)
{
    if (!name.empty())
    {
        ARMARX_INFO << "Wait for move finish: " << name;
    }

    ARMARX_CHECK(localRobot) << "Robot must not be null.";
    static const std::set<std::string> ignored { "RightHandFingers", "RightHandThumb", "LeftHandFingers", "LeftHandThumb" };

    while (!isRunningTaskStopped())
    {
        bool reached = true;
        TimeUtil::SleepMS(10);
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

        for (const auto& [name, value] : values)
        {
            float accuracy = in.getPosAccuracy();

            if (ignored.count(name) > 0)
            {
                continue;
            }
            if (name == "TorsoJoint")
            {
                accuracy *= 10;
                if (in.getInSimulation())
                {
                    accuracy *= 10;
                }
            }
            if (std::abs(value - localRobot->getRobotNode(name)->getJointValue()) > accuracy)
            {
                reached = false;
                ARMARX_INFO << deactivateSpam(1) << "Delta for joint '" << name << "' is: "
                            << value - localRobot->getRobotNode(name)->getJointValue()
                            << "\nLimit is: " << accuracy;
                break;
            }
        }
        if (reached)
        {
            ARMARX_INFO << "Move '" << name << "' finished.";
            return;
        }
    }

    if (isRunningTaskStopped())
    {
        ARMARX_WARNING << "Interrupted!";
        throw InterruptedException();
    }
}


void Armar6Presentation::run()
{
    try
    {
        performPresentation();
    }
    catch (const InterruptedException&)
    {
        ARMARX_WARNING << "Interrupted!";
    }

    ARMARX_IMPORTANT << "Switching to position control and stopping platform.";
    switchControlMode(ePositionControl);
    getPlatformUnit()->move(0, 0, 0);
    getRobotUnit()->switchNJointControllerSetup({});

    emitSucces();
}


void Armar6Presentation::performPresentation()
{
    ARMARX_IMPORTANT << "Perform presentation ...";

    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent(), VirtualRobot::RobotIO::eStructure);
    std::map<std::string, ::armarx::StringValueMapPtr> configurations = in.getConfigurations();
    const NameValueMap configInitPose = configurations.at("configInitPose")->toStdMap<float>();

    {
        NameControlModeMap modeMap;
        for (const auto& name :
             {"RightHandFingers", "RightHandThumb", "LeftHandFingers", "LeftHandThumb"
             })
        {
            modeMap[name] = ePositionControl;
        }
        getKinematicUnit()->switchControlMode(modeMap);
    }

    switchControlMode(ePositionControl);

    const float initialTurnDuration = in.getInitialTurnDuration();
    const float initialTurnSpeed = in.getInitialTurnSpeed();

    const float initialMoveDuration = in.getInitialMoveDuration();
    const Eigen::Vector3f initialMoveVelocity = in.getInitialMoveVelocity()->toEigen();

    setJointAngles(configInitPose, localRobot, "configInitPose");

    auto movePlatformFor = [&](Eigen::Vector3f dir, float dur)
    {
        CycleUtil cycle(Time::milliSeconds(10));
        Time start = TimeUtil::GetTime();
        while (!isRunningTaskStopped() && (TimeUtil::GetTime() - start).toSecondsDouble() < dur)
        {
            getPlatformUnit()->move(dir.x(), dir.y(), dir.z());
            cycle.waitForCycleDuration();
        }
        getPlatformUnit()->move(0, 0, 0);
    };

    ARMARX_INFO << "Initial turn ...";
    movePlatformFor({0, 0, initialTurnSpeed}, initialTurnDuration);

    ARMARX_INFO << "Initial move ...";
    movePlatformFor(initialMoveVelocity, initialMoveDuration);

    ARMARX_INFO << "Stopping platform";
    getPlatformUnit()->move(0, 0, 0);

    // Armar 6 winkt, ist komplett oben und spricht: "Hello! My Name is Armar6. I was build 2017 at the H2T group of KIT."
    // Fährt in Mittel-Höhen-Position + nimmt Arme nach außen "I have 27 degrees of freedom covering a large range of motion."



    // setJointAngles(configInitPose, localRobot, "configInitPose");  // Is set before moving platform.
    waitForMoveFinish(configInitPose, localRobot, "configInitPose");


    const NameValueMap configWave1 = configurations.at("configWave1")->toStdMap<float>();
    const NameValueMap configWave2 = configurations.at("configWave2")->toStdMap<float>();
    const NameValueMap configWave3 = configurations.at("configWave3")->toStdMap<float>();
    const NameValueMap configWave4 = configurations.at("configWave4")->toStdMap<float>();
    const NameValueMap configWave5 = configurations.at("configWave5")->toStdMap<float>();
    const NameValueMap configWaveEnd = configurations.at("configWaveEnd")->toStdMap<float>();

    const NameValueMap configRomStart = configurations.at("configRomStart")->toStdMap<float>();
    const NameValueMap configRomMitte = configurations.at("configRomMitte")->toStdMap<float>();
    const NameValueMap configRomEnd = configurations.at("configRomEnd")->toStdMap<float>();

    //    getTextToSpeech()->reportText("Hello! My Name is Armar6. I have been developed at the Karlsruhe Institute of Technology."
    //                                  "My home is the High Performance Humanoid Technologies Lab at the Institute of Anthropomatics and Robotics."
    //                                  "I am a collaborative robot who can work safely with humans and provide help when needed."
    //                                  "I have been developed within the horizon 20 20  european project second hands.");

    const NameValueMap configTall = configurations.at("configTall")->toStdMap<float>();
    const NameValueMap configTallToSmall = configurations.at("configTallToSmall")->toStdMap<float>();
    const NameValueMap configSmall = configurations.at("configSmall")->toStdMap<float>();
    const NameValueMap configShowArms = configurations.at("configShowArms")->toStdMap<float>();
    const NameValueMap configShowHands = configurations.at("configShowHands")->toStdMap<float>();
    const NameValueMap configZero = configurations.at("configZero")->toStdMap<float>();
    const NameValueMap configIdle = configurations.at("configIdle")->toStdMap<float>();


    bool debugMode = in.getDebugMode();
    if (!debugMode)
    {
        speak(
            in.getGreetingText() + " "
            "My Name is Armar6. I am a member of the Armar humanoid robot family "
            "developed in several projects funded by the german research foundation and the european union. "
            "My home is the Institute of Anthropomatics and Robotics at KIT. "
        );


        setJointAngles(configWave1, localRobot, "configWave1");
        waitForMoveFinish(configWave1, localRobot, "configWave1");

        setJointAngles(configWave2, localRobot, "configWave2");
        waitForMoveFinish(configWave2, localRobot, "configWave2");

        setJointAngles(configWave3, localRobot, "configWave3");
        waitForMoveFinish(configWave3, localRobot, "configWave3");

        setJointAngles(configWave4, localRobot, "configWave4");
        waitForMoveFinish(configWave4, localRobot, "configWave4");

        setJointAngles(configWave5, localRobot, "configWave5");
        waitForMoveFinish(configWave5, localRobot, "configWave5");

        setJointAngles(configWaveEnd, localRobot, "configWaveEnd");
        waitForMoveFinish(configWaveEnd, localRobot, "configWaveEnd");

        waitForSpeechFinished();


        speak(
            "I am a collaborative robot who can work safely with humans and provide help when needed."
        );

        setJointAngles(configRomStart, localRobot, "configRomStart");
        waitForMoveFinish(configRomStart, localRobot, "configRomStart");

        waitForSpeechFinished();

        speak(
            "I have 27 degrees of freedom with a large range of motion and workspace. "
            "Each of my arms has 8 torque controlled joints, which allow safe human-robot collaboration. "
        );


        setJointAngles(configRomMitte, localRobot, "configRomMitte");
        waitForMoveFinish(configRomMitte, localRobot, "configRomMitte");


        setJointAngles(configRomEnd, localRobot, "configRomEnd");
        waitForMoveFinish(configRomEnd, localRobot, "configRomEnd");

        waitForSpeechFinished();


        speak(
            "Three limitless joints in each of my arms enlarge my workspace and my working skills significantly."
        );

        switchControlMode(eVelocityControl);

        const NameValueMap configVelLimitles = configurations.at("configVelLimitles")->toStdMap<float>();
        const NameValueMap configVelZero = configurations.at("configVelZero")->toStdMap<float>();
        getKinematicUnit()->setJointVelocities(configVelLimitles);

        waitForSpeechFinished();


        TimeUtil::Sleep(3);

        getKinematicUnit()->setJointVelocities(configVelZero);

        switchControlMode(ePositionControl);

        // Dann nimmt er die Arme vor sich: "To reach high xxx objects, I can grow up to 192 centimeters plus my arm length."
        // Dann fährt Armar6 runter, Arme bleiben vorm Körper: "But if there is not much space, I can be tiny as well."

        speak(
            "Thanks to my torso joint I can reach high objects at a height of 240 centimeters."
        );

        setJointAngles(configTall, localRobot, "configTall");
        waitForMoveFinish(configTall, localRobot, "configTall");

        speak(
            "So I reach objects which you could not easily reach yourself."
        );
        setJointAngles(configTallToSmall, localRobot, "configTallToSmall");
        waitForMoveFinish(configTallToSmall, localRobot, "configTallToSmall");

        setJointAngles(configSmall, localRobot, "configSmall");
        speak(
            "Using my torso joint I also can reach objects on the ground."
        );

        waitForMoveFinish(configSmall, localRobot, "configSmall");

        setJointAngles({{"RightHandFingers", 1.5}, {"RightHandThumb", 1.5}}, localRobot, "CloseRightFingers");
        TimeUtil::Sleep(1);
        setJointAngles({{"RightHandFingers", 0}, {"RightHandThumb", 0}}, localRobot, "CloseRightFingers");

        setJointAngles(configShowArms, localRobot, "configShowArms");
        speak(
            "Each of my arms can carry up to 10 kilograms. "
        );
        waitForMoveFinish(configShowArms, localRobot, "configShowArms");
        waitForSpeechFinished();


        setJointAngles(configShowHands, localRobot, "configShowHands");
        speak(
            "I have two underactuated five-finger hands which allow me to grasp various objects and tools."
        );
        waitForMoveFinish(configShowHands, localRobot, "configShowHands");
        waitForSpeechFinished();


        setJointAngles(configZero, localRobot, "configZero");

        waitForMoveFinish(configZero, localRobot, "configZero");
        waitForSpeechFinished();


        if (isRunningTaskStopped())
        {
            return;
        }

        speak(
            "My holonomic platform enables me to move fast and in arbitrary direction."
        );

        const std::vector<Vector3Ptr> platformMoveVelocities = in.getPlatformMoveVelocities();
        const std::vector<float> platformMoveDurations = in.getPlatformMoveDurations();
        ARMARX_CHECK_EQUAL(platformMoveVelocities.size(), platformMoveDurations.size());

        for (size_t i = 0; i < platformMoveVelocities.size(); ++i)
        {
            Vector3Ptr vel = platformMoveVelocities.at(i);
            double dur = static_cast<double>(platformMoveDurations.at(i));

            ARMARX_INFO << "Moving platform: " << vel->toEigen().transpose()
                        << " for " << dur << " seconds";
            movePlatformFor(vel->toEigen(), dur);
        }
        getPlatformUnit()->move(0, 0, 0);

        waitForSpeechFinished();

    }
    else
    {
        speak(
            "Skipping most part for faster testing."
        );
        waitForSpeechFinished();
    }


    setJointAngles(configIdle, localRobot, "configIdle");
    speak(
        "Next I will show you how I recognize the need of help "
        "and how I can support a technician performing maintenance tasks."
    );

    waitForMoveFinish(configIdle, localRobot, "configIdle");


    TimeUtil::Sleep(2);

    setJointAngles(configZero, localRobot, "configZero");

    waitForMoveFinish(configZero, localRobot, "configZero");
    waitForSpeechFinished();
}

//void Armar6Presentation::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void Armar6Presentation::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
    getPlatformUnit()->move(0, 0, 0);
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr Armar6Presentation::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new Armar6Presentation(stateData));
}

