/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6PresentationGroup
 * @author     Sonja Marahrens ( sonja dot marahrens )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <armar6/skills/statecharts/Armar6PresentationGroup/Armar6Presentation.generated.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

namespace armarx::Armar6PresentationGroup
{
    class Armar6Presentation :
        public Armar6PresentationGeneratedBase < Armar6Presentation >
    {
    public:
        Armar6Presentation(const XMLStateConstructorParams& stateData):
            XMLStateTemplate < Armar6Presentation > (stateData), Armar6PresentationGeneratedBase < Armar6Presentation > (stateData)
        {
        }

        // inherited from StateBase
        void onEnter() override;
        void run() override;
        // void onBreak() override;
        void onExit() override;

        // static functions for AbstractFactory Method
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static SubClassRegistry Registry;

        // DO NOT INSERT ANY CLASS MEMBERS,
        // use stateparameters instead,
        // if classmember are neccessary nonetheless, reset them in onEnter
    private:

        void switchControlMode(ControlMode mode);
        void setJointAngles(const NameValueMap& values, VirtualRobot::RobotPtr localRobot, const std::string& name = "");
        void setJointAnglesSimulation(const NameValueMap& values, VirtualRobot::RobotPtr localRobot);

        void speak(const std::string& text);

        void waitForSpeechFinished();
        void waitForMoveFinish(const NameValueMap& values, VirtualRobot::RobotPtr localRobot, const std::string& name = "");

        void performPresentation();


        SimpleRunningTask<>::pointer_type setJointAnglesSimulationTask;

    };
}
