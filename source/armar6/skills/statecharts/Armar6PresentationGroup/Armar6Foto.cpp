/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6PresentationGroup
 * @author     armar6-user ( armar6-user at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6Foto.h"
#include "InterruptedException.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <VirtualRobot/math/Helpers.h>

#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>

using namespace armarx;
using namespace Armar6PresentationGroup;

// DO NOT EDIT NEXT LINE
Armar6Foto::SubClassRegistry Armar6Foto::Registry(Armar6Foto::GetName(), &Armar6Foto::CreateInstance);



void Armar6Foto::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void Armar6Foto::waitForMoveFinish(const NameValueMap& values)
{

    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());

    while (!isRunningTaskStopped())

    {
        bool reached = true;
        TimeUtil::SleepMS(10);
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

        for (const auto& pair : values)
        {
            float accuracy = in.getPosAccuracy();
            if (pair.first == "TorsoJoint")
            {
                accuracy *= 10;
            }
            if (std::abs(pair.second - localRobot->getRobotNode(pair.first)->getJointValue()) > accuracy)
            {
                reached = false;
                ARMARX_INFO << deactivateSpam(1) << "Delta for joint " << pair.first << " is: " << pair.second - localRobot->getRobotNode(pair.first)->getJointValue()
                            << " Limit is: " << accuracy;
                break;
            }
        }
        if (reached)
        {
            return;
        }


    }
    if (isRunningTaskStopped())
    {
        ARMARX_WARNING << "Interrupted!";
        throw InterruptedException();
    }
}

void Armar6Foto::run()
{
    ARMARX_IMPORTANT << "ARMAR-6 says SMILE!";
    getTextToSpeech()->reportText("Who wants to take a foto with me?");

    float initialTurnAngle = math::Helpers::deg2rad(in.getInitialTurnAngleInDeg());
    float initialTurnSpeed = in.getInitialTurnSpeed();

    float initialTurnTime = fabs(initialTurnAngle / initialTurnSpeed);
    if (initialTurnAngle < 0)
    {
        initialTurnSpeed *= -1;
    }

    Eigen::Vector3f initialMoveOffset = in.getInitialMoveOffset()->toEigen();
    float initialMoveSpeed = in.getInitialMoveSpeed();
    float initialMoveTime = initialMoveOffset.norm() / initialMoveSpeed;
    Eigen::Vector3f initialMoveVelocity = initialMoveOffset.normalized() * initialMoveSpeed;


    getPlatformUnit()->move(0, 0, initialTurnSpeed);
    TimeUtil::Sleep(initialTurnTime);
    getPlatformUnit()->move(initialMoveVelocity(0), initialMoveVelocity(1), 0);
    TimeUtil::Sleep(initialMoveTime);
    getPlatformUnit()->move(0, 0, 0);

    KinematicUnitHelper kinUnitHelper(getKinematicUnit());
    kinUnitHelper.setJointAngles(in.getJointConfiguration());

    try
    {
        waitForMoveFinish(in.getJointConfiguration());
    }
    catch (const InterruptedException&)
    {
        ARMARX_WARNING << "Interrupted!";
    }

    getTextToSpeech()->reportText("I am ready. Say Cheeeeeese!");
    TimeUtil::Sleep(2);
    emitSuccess();
}

//void Armar6Foto::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void Armar6Foto::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr Armar6Foto::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new Armar6Foto(stateData));
}
