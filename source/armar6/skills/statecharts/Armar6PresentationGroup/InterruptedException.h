#pragma once

#include <stdexcept>

namespace armarx
{
    class InterruptedException : public std::exception
    {
    public:
        virtual ~InterruptedException() = default;
    };
}


