/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6PresentationGroup
 * @author     armar-user ( armar6 at kit )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6PresentationTask.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

namespace armarx::Armar6PresentationGroup
{
    // DO NOT EDIT NEXT LINE
    Armar6PresentationTask::SubClassRegistry Armar6PresentationTask::Registry(Armar6PresentationTask::GetName(), &Armar6PresentationTask::CreateInstance);

    void Armar6PresentationTask::onEnter()
    {
        // put your user code for the enter-point here
        // execution time should be short (<100ms)
    }

    //void Armar6PresentationTask::run()
    //{
    //    // put your user code for the execution-phase here
    //    // runs in seperate thread, thus can do complex operations
    //    // should check constantly whether isRunningTaskStopped() returns true
    //
    //    // get a private kinematic instance for this state of the robot (tick "Robot State Component" proxy checkbox in statechart group)
    //    VirtualRobot::RobotPtr robot = getLocalRobot();
    //
    //// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //        // synchronize robot clone to most recent state
    //        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    //    }
    //}

    //void Armar6PresentationTask::onBreak()
    //{
    //    // put your user code for the breaking point here
    //    // execution time should be short (<100ms)
    //}

    void Armar6PresentationTask::onExit()
    {
        getMessageDisplay()->setMessage("", "");
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr Armar6PresentationTask::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new Armar6PresentationTask(stateData));
    }
}
