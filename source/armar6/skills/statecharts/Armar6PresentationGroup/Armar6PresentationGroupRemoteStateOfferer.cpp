/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6PresentationGroup::Armar6PresentationGroupRemoteStateOfferer
 * @author     Sonja Marahrens ( sonja dot marahrens )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6PresentationGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6PresentationGroup;

// DO NOT EDIT NEXT LINE
Armar6PresentationGroupRemoteStateOfferer::SubClassRegistry Armar6PresentationGroupRemoteStateOfferer::Registry(Armar6PresentationGroupRemoteStateOfferer::GetName(), &Armar6PresentationGroupRemoteStateOfferer::CreateInstance);



Armar6PresentationGroupRemoteStateOfferer::Armar6PresentationGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6PresentationGroupStatechartContext > (reader)
{
}

void Armar6PresentationGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6PresentationGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6PresentationGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6PresentationGroupRemoteStateOfferer::GetName()
{
    return "Armar6PresentationGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6PresentationGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6PresentationGroupRemoteStateOfferer(reader));
}
