/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6Arms
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "EnableNJointZeroTorque.h"

#include <RobotAPI/interface/units/RobotUnit/NjointZeroTorqueController.h>

#include <IceUtil/UUID.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

using namespace armarx;
using namespace Armar6Arms;

// DO NOT EDIT NEXT LINE
EnableNJointZeroTorque::SubClassRegistry EnableNJointZeroTorque::Registry(EnableNJointZeroTorque::GetName(), &EnableNJointZeroTorque::CreateInstance);



void EnableNJointZeroTorque::onEnter()
{
    ARMARX_WARNING << "onEnter .....!!";
    std::cout << "on Enter!!!!";

    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    NJointZeroTorqueControllerConfigPtr config(new NJointZeroTorqueControllerConfig());
    if (in.isJointNamesSet())
    {
        config->jointNames = in.getJointNames();
    }
    config->maxTorque = 0;
    std::string controllerName;
    if (in.isKinematicChainNameSet())
    {
        controllerName = in.getKinematicChainName();
        ARMARX_CHECK_EXPRESSION(getLocalRobot()->hasRobotNodeSet(in.getKinematicChainName())) << in.getKinematicChainName();
        auto nodeSet = getLocalRobot()->getRobotNodeSet(in.getKinematicChainName());
        for (auto& name : nodeSet->getNodeNames())
        {
            config->jointNames.push_back(name);
        }
    }
    ARMARX_CHECK_GREATER(config->jointNames.size(), 0) << "Either JointNames or KinematicChainName must bet set!";
    if (controllerName.empty())
    {
        controllerName = "ZeroTorqueCtrl" + simox::alg::join(config->jointNames, "-");
    }
    auto ctrlPrx = getRobotUnit()->getNJointController(controllerName);
    if (!ctrlPrx)
    {
        ARMARX_INFO << "Creating controller `" << controllerName << "`";
        ctrlPrx = getRobotUnit()->createNJointController(RobotUnitControllerNames::NJointZeroTorqueController, controllerName, config);
    }

    ARMARX_INFO << "Activating controller `" << controllerName << "`";
    getRobotUnit()->activateNJointController(controllerName);
    out.setControllerName(controllerName);
    emitSuccess();
}

//void EnableNJointZeroTorque::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void EnableNJointZeroTorque::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void EnableNJointZeroTorque::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr EnableNJointZeroTorque::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new EnableNJointZeroTorque(stateData));
}
