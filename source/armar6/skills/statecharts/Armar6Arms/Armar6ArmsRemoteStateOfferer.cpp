/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6Arms::Armar6ArmsRemoteStateOfferer
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6ArmsRemoteStateOfferer.h"



using namespace armarx;
using namespace Armar6Arms;

// DO NOT EDIT NEXT LINE
Armar6ArmsRemoteStateOfferer::SubClassRegistry Armar6ArmsRemoteStateOfferer::Registry(Armar6ArmsRemoteStateOfferer::GetName(), &Armar6ArmsRemoteStateOfferer::CreateInstance);



Armar6ArmsRemoteStateOfferer::Armar6ArmsRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6ArmsStatechartContext > (reader)
{
}

void Armar6ArmsRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6ArmsRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6ArmsRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6ArmsRemoteStateOfferer::GetName()
{
    return "Armar6ArmsRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6ArmsRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6ArmsRemoteStateOfferer(reader));
}
