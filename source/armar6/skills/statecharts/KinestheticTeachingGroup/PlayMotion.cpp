/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::KinestheticTeachingGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PlayMotion.h"

#include <RobotAPI/interface/units/RobotUnit/NJointTrajectoryController.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <IceUtil/UUID.h>

#include <RobotAPI/libraries/core/math/MathUtils.h>

using namespace armarx;
using namespace KinestheticTeachingGroup;

// DO NOT EDIT NEXT LINE
PlayMotion::SubClassRegistry PlayMotion::Registry(PlayMotion::GetName(), &PlayMotion::CreateInstance);



void PlayMotion::onEnter()
{


}

void PlayMotion::run()
{
    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());

    TrajectoryPtr traj = in.getTrajectory();

    if (in.getGaussianFilterRadius() > 0)
    {
        traj->gaussianFilter(in.getGaussianFilterRadius());
    }

    StringFloatSeqDict plots;
    for (const Trajectory::TrajData& data : *traj)
    {
        std::vector<double> position = data.getPositions();
        for (size_t i = 0; i < traj->dim(); i++)
        {
            plots[traj->getDimensionName(i)].push_back(position.at(i));
        }
    }
    getStaticPlotter()->addPlotWithTimestampVector("trajectory", traj->getTimestampsFloat(), plots);

    std::string name = "PlayMotion_" + IceUtil::generateUUID();

    //NJointTrajectoryControllerInterfacePrx trajectoryController;
    //trajectoryController = NJointTrajectoryControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController(name));
    //if (trajectoryController)
    //{
    //    if (trajectoryController->isControllerActive())
    //    {
    //        trajectoryController->deactivateController();
    //    }
    //    trajectoryController->deleteController();
    //}


    NJointTrajectoryControllerConfigPtr config = new NJointTrajectoryControllerConfig();
    config->PID_p = in.getKp();
    config->PID_i = in.getKi();
    config->PID_d = in.getKd();
    config->jointNames = traj->getDimensionNames();
    config->considerConstraints = false;
    NJointTrajectoryControllerInterfacePrx trajectoryController;
    trajectoryController = NJointTrajectoryControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController(RobotUnitControllerNames::NJointTrajectoryController, name, config));
    trajectoryController->setTrajectory(traj);
    trajectoryController->activateController();

    VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet(in.getKinematicChainName());
    StringFloatSeqDict plot;
    StringFloatSeqDict errorPlot;
    std::vector<std::string> names = rns->getNodeNames();

    std::vector<float> timestamps;
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());
        float progress = trajectoryController->getCurrentProgressFraction();
        float t = trajectoryController->getCurrentTimestamp();
        timestamps.push_back(t);
        std::vector<float> jvs = rns->getJointValues();
        for (size_t i = 0; i < jvs.size(); i++)
        {
            auto trajPos = (*traj)[t];
            float targetPos = trajPos.at(i)->at(0);
            float err = trajPos.at(i)->at(0) - jvs.at(i);
            if (rns->getNode(i)->isLimitless())
            {
                err = math::MathUtils::angleModPI(err);
                targetPos = math::MathUtils::angleModX(targetPos, 0.5f * (rns->getNode(i)->getJointLimitLo() + rns->getNode(i)->getJointLimitHi()));
            }
            plot[names.at(i)].push_back(jvs.at(i));
            plot[names.at(i) + "_target"].push_back(targetPos);
            errorPlot[names.at(i)].push_back(err);
        }

        if (progress >= 1)
        {
            break;
        }
        //ARMARX_IMPORTANT << "progress: " << trajectoryController->getCurrentProgressFraction() << " timestamp: " << trajectoryController->getCurrentTimestamp();
        usleep(10000);
    }
    getStaticPlotter()->addPlotWithTimestampVector("trajectory replay", timestamps, plot);
    getStaticPlotter()->addPlotWithTimestampVector("trajectory replay error", timestamps, errorPlot);
    //trajectoryController->deactivateController();
    getRobotUnit()->deactivateNJointController(name);
    usleep(10000);
    //getRobotUnit()->deleteNJointController(name);
    //trajectoryController->deleteController();

    emitDone();
}

//void PlayMotion::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void PlayMotion::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr PlayMotion::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new PlayMotion(stateData));
}
