/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::KinestheticTeachingGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LoadTrajectory.h"

#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <ArmarXCore/core/csv/CsvReader.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace KinestheticTeachingGroup;

// DO NOT EDIT NEXT LINE
LoadTrajectory::SubClassRegistry LoadTrajectory::Registry(LoadTrajectory::GetName(), &LoadTrajectory::CreateInstance);



void LoadTrajectory::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void LoadTrajectory::run()
{
    armarx::CMakePackageFinder finder("armar6_skills");
    armarx::ArmarXDataPath::addDataPaths(finder.getDataDir());

    std::string filename = in.getFilename();
    filename = "armar6_skills/motions/" + filename;
    ArmarXDataPath::getAbsolutePath(filename, filename);
    ARMARX_IMPORTANT << "loading trajectory " << filename;

    CsvReader reader(filename, true);
    std::map<std::string, std::vector<float>> columns = reader.readAllColumnsFloatMap();
    ARMARX_IMPORTANT << "loading successful " << filename;

    std::vector<float> timestamps = columns["timestamp"];
    float speedupFactor = in.getSpeedupFactor();
    for (float& timestamp : timestamps)
    {
        timestamp = timestamp / speedupFactor;
    }

    std::vector<std::string> names = reader.getHeader();
    names.erase(names.begin());
    TrajectoryPtr traj = new Trajectory();
    traj->setDimensionNames(names);

    StringFloatSeqDict plot;
    std::vector<std::vector<float>> data;
    std::map<std::string, float> initialJointAngles;

    for (size_t i = 0; i < names.size(); i++)
    {
        data.push_back(columns[names.at(i)]);
        plot[names.at(i)] = data.at(i);
        initialJointAngles[names.at(i)] = data.at(i).at(0);
    }
    getStaticPlotter()->addPlotWithTimestampVector("motion " + in.getFilename(), timestamps, plot);

    for (size_t n = 0; n < timestamps.size(); n++)
    {
        for (size_t i = 0; i < data.size(); i++)
        {
            traj->setPositionEntry(timestamps.at(n), i, data.at(i).at(n));
        }
    }
    out.setTrajectory(traj);
    out.setInitialJointAngles(initialJointAngles);

    emitSuccess();
}

//void LoadTrajectory::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void LoadTrajectory::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr LoadTrajectory::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new LoadTrajectory(stateData));
}
