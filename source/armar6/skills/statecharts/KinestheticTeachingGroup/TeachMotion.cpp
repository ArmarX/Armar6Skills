/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::KinestheticTeachingGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TeachMotion.h"

#include <ArmarXCore/core/csv/CsvWriter.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/interface/core/BasicTypes.h>

#include <RobotAPI/libraries/core/math/LinearizeAngularTrajectory.h>
#include <RobotAPI/libraries/core/math/TimeSeriesUtils.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace KinestheticTeachingGroup;

// DO NOT EDIT NEXT LINE
TeachMotion::SubClassRegistry TeachMotion::Registry(TeachMotion::GetName(), &TeachMotion::CreateInstance);



void TeachMotion::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void TeachMotion::run()
{


    NameControlModeMap controlModes;
    for (const std::string& name : in.getTorqueControlledJoints())
    {
        controlModes[name] = in.getIsSimulation() ? ePositionControl : eTorqueControl;
    }
    getKinematicUnit()->switchControlMode(controlModes);

    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    float motionThresholdRadian = in.getMotionThresholdRadian();
    int noMotionTimeoutMs = in.getNoMotionTimeoutMs();

    VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet(in.getKinematicChainName());

    IceUtil::Time start = TimeUtil::GetTime();
    IceUtil::Time lastMotionTime = start;
    bool initialMotionDetected = false;

    std::vector<std::string> names = rns->getNodeNames();
    TrajectoryPtr traj = new Trajectory();
    traj->setDimensionNames(names);

    ARMARX_IMPORTANT << "Waiting for initial motion";

    Eigen::VectorXf jve = rns->getJointValuesEigen();
    std::map<std::string, float> initialJointAngles = rns->getJointValueMap();
    out.setInitialJointAngles(initialJointAngles);

    std::vector<std::vector<float>> data;
    for (size_t i = 0; i < names.size(); i++)
    {
        data.push_back(std::vector<float>());
    }
    std::vector<float> timestamps;

    int motionStartIndex = 0;
    int motionEndIndex = 0;

    int sigmaMs = in.getFilterSigmaMs();
    int sampleMs = 10;

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

        IceUtil::Time now = TimeUtil::GetTime();


        Eigen::VectorXf jve2 = rns->getJointValuesEigen();
        if ((jve2 - jve).norm() > motionThresholdRadian)
        {
            if (!initialMotionDetected)
            {
                ARMARX_IMPORTANT << "Start recording";
                motionStartIndex = timestamps.size();
                //start = now;
            }
            jve = jve2;
            initialMotionDetected = true;
            lastMotionTime = now;

        }

        double t = (now - start).toSecondsDouble();


        std::vector<float> jvs = rns->getJointValues();
        for (size_t i = 0; i < jvs.size(); i++)
        {
            //traj->setPositionEntry(t, i, jvs.at(i));
            data.at(i).push_back(jvs.at(i));
        }
        timestamps.push_back(t);


        if (initialMotionDetected && (now - lastMotionTime).toMilliSeconds() > noMotionTimeoutMs)
        {
            ARMARX_IMPORTANT << "Stop recording";
            motionEndIndex = timestamps.size();// - noMotionTimeoutMs / sampleMs;
            break;
        }

        TimeUtil::USleep(sampleMs * 1000);
    }

    motionStartIndex -= sigmaMs / sampleMs * 2;
    if (motionStartIndex < 0)
    {
        motionStartIndex = 0;
    }
    //motionEndIndex += sigmaMs / sampleMs * 2;
    //if (motionEndIndex >= data.size())
    //{
    //    motionEndIndex = data.size() - 1;
    //}

    ARMARX_IMPORTANT << VAROUT(motionStartIndex) << VAROUT(motionEndIndex);

    ARMARX_IMPORTANT << "copy timestamps";
    timestamps = std::vector<float>(timestamps.begin() + motionStartIndex, timestamps.begin() + motionEndIndex);
    for (size_t n = 1; n < timestamps.size(); n++)
    {
        timestamps.at(n) = timestamps.at(n) - timestamps.at(0);
    }
    timestamps.at(0) = 0;

    ARMARX_IMPORTANT << VAROUT(timestamps);
    std::vector<float> newT = math::TimeSeriesUtils::MakeTimestamps(timestamps.at(0), timestamps.at(timestamps.size() - 1), timestamps.size());
    ARMARX_IMPORTANT << VAROUT(newT);

    StringFloatSeqDict plotFiltered;
    StringFloatSeqDict plotOrig;
    StringFloatSeqDict plotResampled;
    for (size_t i = 0; i < data.size(); i++)
    {
        data.at(i) = std::vector<float>(data.at(i).begin() + motionStartIndex, data.at(i).begin() + motionEndIndex);
        if (rns->getNode(i)->isLimitless())
        {
            math::LinearizeAngularTrajectory::LinearizeRef(data.at(i));
        }
        plotOrig[names.at(i)] = data.at(i);
        data.at(i) = math::TimeSeriesUtils::Resample(timestamps, data.at(i), newT);
        plotResampled[names.at(i)] = data.at(i);
        data.at(i) = math::TimeSeriesUtils::ApplyGaussianFilter(data.at(i), sigmaMs * 0.001f, sampleMs * 0.001f, math::TimeSeriesUtils::BorderMode::Nearest);

        // Trajectory cannot handle limitless trajectory properly...
        //if (rns->getNode(i)->isLimitless())
        //{
        //    data.at(i) = math::LinearizeAngularTrajectory::Angularize(data.at(i), 0.5f * (rns->getNode(i)->getJointLimitLo() + rns->getNode(i)->getJointLimitHi()));
        //}
        plotFiltered[names.at(i)] = data.at(i);
    }

    getStaticPlotter()->addPlotWithTimestampVector("original motion", timestamps, plotOrig);
    timestamps = newT;
    for (size_t n = 0; n < timestamps.size(); n++)
    {
        for (size_t i = 0; i < data.size(); i++)
        {
            traj->setPositionEntry(timestamps.at(n), i, data.at(i).at(n));
        }
    }

    if (in.getWriteTrajectory())
    {
        std::vector<std::string> header;
        header.push_back("timestamp");
        for (const VirtualRobot::RobotNodePtr& rn : rns->getAllRobotNodes())
        {
            header.push_back(rn->getName());
        }

        IceUtil::Time now = IceUtil::Time::now();
        time_t timer = now.toSeconds();
        struct tm* ts;
        char buffer[80];
        ts = localtime(&timer);
        strftime(buffer, 80, "%Y-%m-%d-%H-%M-%S", ts);

        std::string packageName = "armar6_skills";
        armarx::CMakePackageFinder finder(packageName);
        std::string dataDir = finder.getDataDir() + "/" + packageName + "/motions/";
        std::string filepath = dataDir + "trajectory-" + buffer + ".csv";
        ARMARX_IMPORTANT << "writing trajectory to " << filepath;
        CsvWriter writer(filepath, header, false);

        for (size_t n = 0; n < timestamps.size(); n++)
        {
            std::vector<float> row;
            row.push_back(timestamps.at(n));
            for (size_t i = 0; i < data.size(); i++)
            {
                row.push_back(data.at(i).at(n));
            }
            writer.write(row);
        }
        writer.close();
    }

    getStaticPlotter()->addPlotWithTimestampVector("resampled motion", timestamps, plotResampled);
    getStaticPlotter()->addPlotWithTimestampVector("filtered motion", timestamps, plotFiltered);

    out.setTrajectory(traj);
    emitDone();
}

//void TeachMotion::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void TeachMotion::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TeachMotion::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TeachMotion(stateData));
}
