/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::KinestheticTeachingGroup
 * @author     armar6-user ( armar6-user at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TeachAndStore.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace KinestheticTeachingGroup;

// DO NOT EDIT NEXT LINE
TeachAndStore::SubClassRegistry TeachAndStore::Registry(TeachAndStore::GetName(), &TeachAndStore::CreateInstance);



void TeachAndStore::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void TeachAndStore::run()
{
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        usleep(10000);
    }
    NameControlModeMap controlModes;
    for (const std::string& name : in.getTorqueControlledJoints())
    {
        controlModes[name] = ePositionControl;
    }
    getKinematicUnit()->switchControlMode(controlModes);
}

//void TeachAndStore::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void TeachAndStore::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TeachAndStore::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TeachAndStore(stateData));
}
