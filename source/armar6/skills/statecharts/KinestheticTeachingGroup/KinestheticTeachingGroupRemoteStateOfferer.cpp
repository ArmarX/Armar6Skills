/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::KinestheticTeachingGroup::KinestheticTeachingGroupRemoteStateOfferer
 * @author     [Author Name] ( [Author Email] )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "KinestheticTeachingGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace KinestheticTeachingGroup;

// DO NOT EDIT NEXT LINE
KinestheticTeachingGroupRemoteStateOfferer::SubClassRegistry KinestheticTeachingGroupRemoteStateOfferer::Registry(KinestheticTeachingGroupRemoteStateOfferer::GetName(), &KinestheticTeachingGroupRemoteStateOfferer::CreateInstance);



KinestheticTeachingGroupRemoteStateOfferer::KinestheticTeachingGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < KinestheticTeachingGroupStatechartContext > (reader)
{
}

void KinestheticTeachingGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void KinestheticTeachingGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void KinestheticTeachingGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string KinestheticTeachingGroupRemoteStateOfferer::GetName()
{
    return "KinestheticTeachingGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr KinestheticTeachingGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new KinestheticTeachingGroupRemoteStateOfferer(reader));
}
