/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::SecondHandsDemoGroup
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SecondHandsDemo.h"

#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <ArmarXCore/core/util/StringHelpers.h>

using namespace armarx;
using namespace SecondHandsDemoGroup;

// DO NOT EDIT NEXT LINE
SecondHandsDemo::SubClassRegistry SecondHandsDemo::Registry(SecondHandsDemo::GetName(), &SecondHandsDemo::CreateInstance);



void SecondHandsDemo::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)

    ARMARX_CHECK_EXPRESSION(in.getargs().size() >= 1);
    auto objectName = in.getargs().at(0)->entityName;
    local.setObjectName(objectName);
}

//void SecondHandsDemo::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void SecondHandsDemo::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void SecondHandsDemo::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr SecondHandsDemo::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new SecondHandsDemo(stateData));
}



void armarx::SecondHandsDemoGroup::SecondHandsDemo::transitionSuccessFromMoveToHandover(ForceControlGroup::HandOverObjectIn& next, const MoveToHandoverOut& prev)
{
    if (!local.isUsedEndeffectorSet())
    {
        ARMARX_WARNING << "local parameter Endeffector name is not set!";
        //        next.setUseRightArm(false);
        return;
    }
    getHeadIKUnit()->setHeadTarget("IKVirtualGaze", new FramedPosition(Eigen::Vector3f(0, 800, 1700), getLocalRobot()->getRootNode()->getName(), getLocalRobot()->getName()));

    auto tcpName = getLocalRobot()->getEndEffector(local.getUsedEndeffector())->getTcpName();
    if (Contains(tcpName, "R", true))
    {
        next.setUseRightArm(true);
    }
    else
    {
        next.setUseRightArm(false);
    }
}
