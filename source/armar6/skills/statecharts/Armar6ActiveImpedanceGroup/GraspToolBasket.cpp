/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ActiveImpedanceGroup
 * @author     armar6-user ( armar6-user at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspToolBasket.h"

#include <RobotAPI/interface/units/RobotUnit/NJointActiveImpedanceController.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6ActiveImpedanceGroup;

// DO NOT EDIT NEXT LINE
GraspToolBasket::SubClassRegistry GraspToolBasket::Registry(GraspToolBasket::GetName(), &GraspToolBasket::CreateInstance);



void GraspToolBasket::onEnter()
{
    ARMARX_IMPORTANT << "onEnter";
    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(in.getKinematicChainName());
    std::vector<std::string> jointNames = rns->getNodeNames();
    float stiffness = in.getInitialStiffness();

    std::vector<std::string> controllerNames;
    for (const std::string& jointName : jointNames)
    {
        float jointAngle = robot->getRobotNode(jointName)->getJointValue();
        NJointActiveImpedanceControllerConfigPtr config = new NJointActiveImpedanceControllerConfig(jointAngle, stiffness, 0, jointName);
        std::string controllerName = "ActiveImpedance_" + jointName;
        NJointActiveImpedanceControllerInterfacePrx controller = NJointActiveImpedanceControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController(RobotUnitControllerNames::NJointActiveImpedanceController, controllerName, config));
        controllerNames.push_back(controllerName);
        //controllers[jointName] = controller;
        //targetJointAngles[jointName] = jointAngle;
    }
    getRobotUnit()->activateNJointControllers(controllerNames);
}

void GraspToolBasket::run()
{
    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(in.getKinematicChainName());
    std::vector<std::string> jointNames = rns->getNodeNames();

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        usleep(10000);
    }
    ARMARX_IMPORTANT << "after run";


    std::vector<std::string> controllerNames;
    for (const std::string& jointName : jointNames)
    {
        std::string controllerName = "ActiveImpedance_" + jointName;
        controllerNames.push_back(controllerName);
    }

    usleep(500000); // 0.5s
    getRobotUnit()->deactivateNJointControllers(controllerNames);
    usleep(200000); // 0.2s
    getRobotUnit()->deleteNJointControllers(controllerNames);

    ARMARX_IMPORTANT << "deletd controllers";
}

//void GraspToolBasket::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GraspToolBasket::onExit()
{
    ARMARX_IMPORTANT << "onExit";
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GraspToolBasket::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GraspToolBasket(stateData));
}
