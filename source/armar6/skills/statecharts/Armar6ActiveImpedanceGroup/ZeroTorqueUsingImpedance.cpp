/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ActiveImpedanceGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ZeroTorqueUsingImpedance.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <RobotAPI/interface/units/RobotUnit/NJointActiveImpedanceController.h>


using namespace armarx;
using namespace Armar6ActiveImpedanceGroup;

// DO NOT EDIT NEXT LINE
ZeroTorqueUsingImpedance::SubClassRegistry ZeroTorqueUsingImpedance::Registry(ZeroTorqueUsingImpedance::GetName(), &ZeroTorqueUsingImpedance::CreateInstance);



void ZeroTorqueUsingImpedance::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void ZeroTorqueUsingImpedance::run()
{
    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());

    float stiffness = in.getStiffness();

    //std::vector<NJointControllerInterfacePrx> controllers;
    std::vector<std::string> controllerNames;
    std::map<std::string, NJointActiveImpedanceControllerInterfacePrx> controllers;

    for (const std::string& jointName : in.getActiveImpedanceJoints())
    {
        float jointAngle = robot->getRobotNode(jointName)->getJointValue();
        NJointActiveImpedanceControllerConfigPtr config = new NJointActiveImpedanceControllerConfig(jointAngle, stiffness, 0, jointName);
        std::string controllerName = "ActiveImpedance_" + jointName;
        NJointActiveImpedanceControllerInterfacePrx controller = NJointActiveImpedanceControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController(RobotUnitControllerNames::NJointActiveImpedanceController, controllerName, config));
        controllerNames.push_back(controllerName);
        controllers[jointName] = controller;
    }
    getRobotUnit()->activateNJointControllers(controllerNames);


    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

        for (const std::string& jointName : in.getActiveImpedanceJoints())
        {
            NJointActiveImpedanceControllerInterfacePrx controller = controllers[jointName];
            controller->setControllerTarget(robot->getRobotNode(jointName)->getJointValue(), in.getStiffness(), 0);
        }

        TimeUtil::SleepMS(10);
    }

    getRobotUnit()->deactivateNJointControllers(controllerNames);
    usleep(200000); // 0.2s
    getRobotUnit()->deleteNJointControllers(controllerNames);


}

//void ZeroTorqueUsingImpedance::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ZeroTorqueUsingImpedance::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ZeroTorqueUsingImpedance::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ZeroTorqueUsingImpedance(stateData));
}
