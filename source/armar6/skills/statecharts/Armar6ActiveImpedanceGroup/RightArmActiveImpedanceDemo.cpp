/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ActiveImpedanceGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RightArmActiveImpedanceDemo.h"

//#include <armar6/rt/units/Armar6Unit/NJointController/NJointActiveImpedanceController.h>
#include <RobotAPI/interface/units/RobotUnit/NJointActiveImpedanceController.h>

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6ActiveImpedanceGroup;

// DO NOT EDIT NEXT LINE
RightArmActiveImpedanceDemo::SubClassRegistry RightArmActiveImpedanceDemo::Registry(RightArmActiveImpedanceDemo::GetName(), &RightArmActiveImpedanceDemo::CreateInstance);



void RightArmActiveImpedanceDemo::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void RightArmActiveImpedanceDemo::run()
{
    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    VirtualRobot::RobotPtr robotTarget = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());

    VirtualRobot::RobotNodePtr tcp = robot->getRobotNodeSet(in.getKinematicChainName())->getTCP();
    VirtualRobot::RobotNodePtr tcpTarget = robotTarget->getRobotNodeSet(in.getKinematicChainName())->getTCP();

    float stiffness = in.getStiffness();

    //std::vector<NJointControllerInterfacePrx> controllers;
    std::vector<std::string> controllerNames;
    std::map<std::string, NJointActiveImpedanceControllerInterfacePrx> controllers;
    std::map<std::string, float> targetJointAngles;

    for (const std::string& jointName : in.getActiveImpedanceJoints())
    {
        float jointAngle = robot->getRobotNode(jointName)->getJointValue();
        NJointActiveImpedanceControllerConfigPtr config = new NJointActiveImpedanceControllerConfig(jointAngle, stiffness, 0, jointName);
        std::string controllerName = "ActiveImpedance_" + jointName;
        NJointActiveImpedanceControllerInterfacePrx controller = NJointActiveImpedanceControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController(RobotUnitControllerNames::NJointActiveImpedanceController, controllerName, config));
        controllerNames.push_back(controllerName);
        controllers[jointName] = controller;
        targetJointAngles[jointName] = jointAngle;
    }
    getRobotUnit()->activateNJointControllers(controllerNames);

    std::string animatedJointName = in.getAnimatedJointName();
    float center = in.getAnimationCenter();
    float amplitude = in.getAnimationAmplitude();
    float period = in.getAnimationPeriod();
    NJointActiveImpedanceControllerInterfacePrx animatedJointController = controllers[animatedJointName];

    IceUtil::Time start = TimeUtil::GetTime();
    while (!isRunningTaskStopped())
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        IceUtil::Time now = TimeUtil::GetTime();
        float t = (now - start).toSecondsDouble();

        float jointAngle = center + sin(t / period * 2 * M_PI) * amplitude;
        animatedJointController->setControllerTarget(jointAngle, stiffness, 0);
        targetJointAngles[animatedJointName] = jointAngle;

        for (const std::string& jointName : in.getActiveImpedanceJoints())
        {
            float jointAngle = robot->getRobotNode(jointName)->getJointValue();
            getDebugObserver()->setDebugDatafield("RightArmActiveImpedanceDemo", jointName + "_Error", new Variant(jointAngle - targetJointAngles[jointName]));
            robotTarget->getRobotNode(jointName)->setJointValue(targetJointAngles[jointName]);
        }

        Eigen::Vector3f tcpError = tcp->getPositionInRootFrame() - tcpTarget->getPositionInRootFrame();
        getDebugObserver()->setDebugDatafield("RightArmActiveImpedanceDemo", "tcpError_x", new Variant(tcpError(0)));
        getDebugObserver()->setDebugDatafield("RightArmActiveImpedanceDemo", "tcpError_y", new Variant(tcpError(1)));
        getDebugObserver()->setDebugDatafield("RightArmActiveImpedanceDemo", "tcpError_z", new Variant(tcpError(2)));


        usleep(10000);
    }

    usleep(500000); // 0.5s

    getRobotUnit()->deactivateNJointControllers(controllerNames);
    usleep(200000); // 0.2s
    getRobotUnit()->deleteNJointControllers(controllerNames);

}

//void RightArmActiveImpedanceDemo::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void RightArmActiveImpedanceDemo::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr RightArmActiveImpedanceDemo::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new RightArmActiveImpedanceDemo(stateData));
}
