/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ActiveImpedanceGroup
 * @author     armar6-user ( armar6-user at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MoveOnLineUntilContact.h"

#include <VirtualRobot/math/Helpers.h>
#include <VirtualRobot/math/Line.h>

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <Eigen/Eigen>

#include <RobotAPI/libraries/core/CartesianPositionController.h>
#include <RobotAPI/libraries/core/CartesianVelocityController.h>

#include <RobotAPI/interface/units/RobotUnit/NJointActiveImpedanceController.h>

#include <armar6/skills/statecharts/Armar6ActiveImpedanceGroup/Armar6ActiveImpedanceGroupStatechartContext.generated.h>

using namespace armarx;
using namespace Armar6ActiveImpedanceGroup;

// DO NOT EDIT NEXT LINE
MoveOnLineUntilContact::SubClassRegistry MoveOnLineUntilContact::Registry(MoveOnLineUntilContact::GetName(), &MoveOnLineUntilContact::CreateInstance);

using namespace math;

void MoveOnLineUntilContact::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void MoveOnLineUntilContact::run()
{
    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    VirtualRobot::RobotPtr robotTarget = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());

    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(in.getKinematicChainName());
    VirtualRobot::RobotNodeSetPtr rnsTarget = robotTarget->getRobotNodeSet(in.getKinematicChainName());
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();
    VirtualRobot::RobotNodePtr tcpTarget = rnsTarget->getTCP();
    std::vector<std::string> jointNames = rns->getNodeNames();


    Eigen::Matrix4f startPose = in.isStartPoseSet() ? in.getStartPose()->toEigen() : tcp->getPoseInRootFrame();
    Eigen::Matrix4f targetPose;
    if (in.isRelativeTargetPositionSet())
    {
        targetPose = startPose;
        targetPose.block<3, 1>(0, 3) += in.getRelativeTargetPosition()->toEigen();
    }
    else if (in.isTargetPoseSet())
    {
        targetPose = in.getTargetPose()->toEigen();
    }
    else
    {
        ARMARX_ERROR << "Either TargetPose or RelativeTargetPosition have to be set";
        emitFailure();
    }



    Eigen::Quaternionf startQuat = Eigen::Quaternionf(startPose.block<3, 3>(0, 0));
    Eigen::Quaternionf targetQuat = Eigen::Quaternionf(targetPose.block<3, 3>(0, 0));

    Line line = Line::FromPoses(startPose, targetPose);


    float stiffness = in.getStiffness();
    float jointLimitAvoidanceKp = in.getJointLimitAvoidanceKp();
    float minExecutionTime = in.getMinExecutionTime();
    float errorThreshold = in.getErrorThreshold();
    float stiffnessRampDuration = in.getStiffnessRampDuration();

    std::map<std::string, NJointActiveImpedanceControllerInterfacePrx> controllers;
    for (const std::string& jointName : jointNames)
    {
        std::string controllerName = "ActiveImpedance_" + jointName;
        NJointActiveImpedanceControllerInterfacePrx controller = StateBase::getContext<Armar6ActiveImpedanceGroupStatechartContext>()->getProxy<NJointActiveImpedanceControllerInterfacePrx>(controllerName);
        controllers[jointName] = controller;
    }


    float duration = in.getDuration();
    IceUtil::Time start = TimeUtil::GetTime();
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        IceUtil::Time now = TimeUtil::GetTime();
        for (size_t n = 0; n < rns->getSize(); n++)
        {
            rnsTarget->getNode(n)->setJointValue(rns->getNode(n)->getJointValue());
        }

        float t = (now - start).toSecondsDouble();
        float f = t / duration;

        if (f > 1)
        {
            emitTargetReached();
            break;
        }

        float kp = t < stiffnessRampDuration ? Helpers::Lerp(0, stiffness, t / stiffnessRampDuration) : stiffness;


        Eigen::Quaternionf quat = Helpers::Lerp(startQuat, targetQuat, f);
        Eigen::Vector3f pos = line.Get(f);

        Eigen::Matrix4f target = Helpers::CreatePose(pos, quat);
        bool ikResult = SimpleDiffIK(rnsTarget, target, jointLimitAvoidanceKp);
        if (!ikResult)
        {
            emitFailure();
            break;
        }

        Eigen::Vector3f posError = tcpTarget->getPositionInRootFrame() - tcp->getPositionInRootFrame();
        if (t > minExecutionTime && posError.norm() > errorThreshold)
        {
            emitContact();
            break;
        }

        ARMARX_IMPORTANT << VAROUT(posError);

        getDebugObserver()->setDebugDatafield("MoveOnLineUntilContact", "posError_x", new Variant(posError(0)));
        getDebugObserver()->setDebugDatafield("MoveOnLineUntilContact", "posError_y", new Variant(posError(1)));
        getDebugObserver()->setDebugDatafield("MoveOnLineUntilContact", "posError_z", new Variant(posError(2)));

        for (size_t n = 0; n < rns->getSize(); n++)
        {
            std::string jointName = jointNames.at(n);
            NJointActiveImpedanceControllerInterfacePrx controller = controllers[jointName];
            controller->setControllerTarget(rnsTarget->getNode(n)->getJointValue(), kp, 0);
        }

        usleep(10000); // 10ms

    }

    /*
    usleep(500000); // 0.5s
    getRobotUnit()->deactivateNJointControllers(controllerNames);
    usleep(200000); // 0.2s
    getRobotUnit()->deleteNJointControllers(controllerNames);*/

}

bool MoveOnLineUntilContact::SimpleDiffIK(const VirtualRobot::RobotNodeSetPtr& rns, const Eigen::Matrix4f& target, float jointLimitAvoidanceKp)
{
    CartesianVelocityController velocityController(rns);
    CartesianPositionController positionController(rns->getTCP());

    float stepLength = 0.2f;

    for (int i = 0; i <= 25; i++)
    {
        Eigen::Vector3f posDiff = positionController.getPositionDiff(target);
        Eigen::Vector3f oriDiff = positionController.getOrientationDiff(target);

        //ARMARX_IMPORTANT << VAROUT(posDiff) << VAROUT(oriDiff);

        Eigen::VectorXf cartesialVel(6);
        cartesialVel << posDiff(0), posDiff(1), posDiff(2), oriDiff(0), oriDiff(1), oriDiff(2);
        Eigen::VectorXf jnv = jointLimitAvoidanceKp * velocityController.calculateJointLimitAvoidance();
        Eigen::VectorXf jv = velocityController.calculate(cartesialVel, jnv, VirtualRobot::IKSolver::All);

        if (i >= 20)
        {
            stepLength = 0.5;
        }

        jv = jv * stepLength;

        for (size_t n = 0; n < rns->getSize(); n++)
        {
            rns->getNode(n)->setJointValue(rns->getNode(n)->getJointValue() + jv(n));
        }
    }

    {
        Eigen::Vector3f posDiff = positionController.getPositionDiff(target);
        if (posDiff.norm() > 10)
        {
            ARMARX_ERROR_S << "cannot solve IK! error=" << posDiff.norm() << "mm";
            return false;
        }

    }

    return true;

}


//void MoveOnLineUntilContact::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MoveOnLineUntilContact::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MoveOnLineUntilContact::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MoveOnLineUntilContact(stateData));
}
