/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ActiveImpedanceGroup
 * @author     armar6-user ( armar6-user at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Lift.h"

#include <RobotAPI/interface/units/RobotUnit/NJointActiveImpedanceController.h>

#include <VirtualRobot/math/Helpers.h>
#include <VirtualRobot/math/Line.h>

#include <armar6/skills/statecharts/Armar6ActiveImpedanceGroup/Armar6ActiveImpedanceGroupStatechartContext.generated.h>

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include "MoveOnLineUntilContact.h"

using namespace armarx;
using namespace Armar6ActiveImpedanceGroup;
using namespace math;

// DO NOT EDIT NEXT LINE
Lift::SubClassRegistry Lift::Registry(Lift::GetName(), &Lift::CreateInstance);



void Lift::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void Lift::run()
{
    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());
    VirtualRobot::RobotPtr robotTarget = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());

    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(in.getKinematicChainName());
    VirtualRobot::RobotNodeSetPtr rnsTarget = robotTarget->getRobotNodeSet(in.getKinematicChainName());
    VirtualRobot::RobotNodePtr tcp = rns->getTCP();
    VirtualRobot::RobotNodePtr tcpTarget = rnsTarget->getTCP();
    std::vector<std::string> jointNames = rns->getNodeNames();

    Eigen::Matrix4f startPose = tcp->getPoseInRootFrame();
    Eigen::Matrix4f targetPose = startPose;
    targetPose.block<3, 1>(0, 3) += in.getRelativeTargetPosition()->toEigen();



    Eigen::Quaternionf startQuat = Eigen::Quaternionf(startPose.block<3, 3>(0, 0));
    Eigen::Quaternionf targetQuat = Eigen::Quaternionf(targetPose.block<3, 3>(0, 0));

    Line line = Line::FromPoses(startPose, targetPose);


    std::map<std::string, NJointActiveImpedanceControllerInterfacePrx> controllers;
    for (const std::string& jointName : jointNames)
    {
        std::string controllerName = "ActiveImpedance_" + jointName;
        NJointActiveImpedanceControllerInterfacePrx controller = StateBase::getContext<Armar6ActiveImpedanceGroupStatechartContext>()->getProxy<NJointActiveImpedanceControllerInterfacePrx>(controllerName);
        controllers[jointName] = controller;
    }

    float stiffness = in.getStiffness();
    float jointLimitAvoidanceKp = in.getJointLimitAvoidanceKp();

    float duration = in.getDuration();
    IceUtil::Time start = TimeUtil::GetTime();
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        IceUtil::Time now = TimeUtil::GetTime();
        for (size_t n = 0; n < rns->getSize(); n++)
        {
            rnsTarget->getNode(n)->setJointValue(rns->getNode(n)->getJointValue());
        }

        float t = (now - start).toSecondsDouble();
        float f = t / duration;

        if (f > 1)
        {
            emitSuccess();
            break;
        }

        Eigen::Quaternionf quat = Helpers::Lerp(startQuat, targetQuat, f);
        Eigen::Vector3f pos = line.Get(f);

        Eigen::Matrix4f target = Helpers::CreatePose(pos, quat);
        bool ikResult = MoveOnLineUntilContact::SimpleDiffIK(rnsTarget, target, jointLimitAvoidanceKp);
        if (!ikResult)
        {
            emitFailure();
            break;
        }

        Eigen::Vector3f posError = tcpTarget->getPositionInRootFrame() - tcp->getPositionInRootFrame();
        ARMARX_IMPORTANT << VAROUT(posError);

        getDebugObserver()->setDebugDatafield("MoveOnLineUntilContact", "lift_posError_x", new Variant(posError(0)));
        getDebugObserver()->setDebugDatafield("MoveOnLineUntilContact", "lift_posError_y", new Variant(posError(1)));
        getDebugObserver()->setDebugDatafield("MoveOnLineUntilContact", "lift_posError_z", new Variant(posError(2)));

        for (size_t n = 0; n < rns->getSize(); n++)
        {
            std::string jointName = jointNames.at(n);
            NJointActiveImpedanceControllerInterfacePrx controller = controllers[jointName];
            controller->setControllerTarget(rnsTarget->getNode(n)->getJointValue(), stiffness, 0);
        }

        usleep(10000); // 10ms
    }

}

//void Lift::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void Lift::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr Lift::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new Lift(stateData));
}
