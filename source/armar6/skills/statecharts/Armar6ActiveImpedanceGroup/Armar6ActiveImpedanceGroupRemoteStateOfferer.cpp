/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6ActiveImpedanceGroup::Armar6ActiveImpedanceGroupRemoteStateOfferer
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6ActiveImpedanceGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6ActiveImpedanceGroup;

// DO NOT EDIT NEXT LINE
Armar6ActiveImpedanceGroupRemoteStateOfferer::SubClassRegistry Armar6ActiveImpedanceGroupRemoteStateOfferer::Registry(Armar6ActiveImpedanceGroupRemoteStateOfferer::GetName(), &Armar6ActiveImpedanceGroupRemoteStateOfferer::CreateInstance);



Armar6ActiveImpedanceGroupRemoteStateOfferer::Armar6ActiveImpedanceGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6ActiveImpedanceGroupStatechartContext > (reader)
{
}

void Armar6ActiveImpedanceGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6ActiveImpedanceGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6ActiveImpedanceGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6ActiveImpedanceGroupRemoteStateOfferer::GetName()
{
    return "Armar6ActiveImpedanceGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6ActiveImpedanceGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6ActiveImpedanceGroupRemoteStateOfferer(reader));
}
