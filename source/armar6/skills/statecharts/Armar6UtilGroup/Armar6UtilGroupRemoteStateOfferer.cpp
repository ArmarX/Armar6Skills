/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6UtilGroup::Armar6UtilGroupRemoteStateOfferer
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6UtilGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6UtilGroup;

// DO NOT EDIT NEXT LINE
Armar6UtilGroupRemoteStateOfferer::SubClassRegistry Armar6UtilGroupRemoteStateOfferer::Registry(Armar6UtilGroupRemoteStateOfferer::GetName(), &Armar6UtilGroupRemoteStateOfferer::CreateInstance);



Armar6UtilGroupRemoteStateOfferer::Armar6UtilGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6UtilGroupStatechartContext > (reader)
{
}

void Armar6UtilGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6UtilGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6UtilGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6UtilGroupRemoteStateOfferer::GetName()
{
    return "Armar6UtilGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6UtilGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6UtilGroupRemoteStateOfferer(reader));
}
