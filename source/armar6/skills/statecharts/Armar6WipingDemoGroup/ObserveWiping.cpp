/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6WipingDemoGroup
 * @author     armar6-user ( armar6-user at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6WipingDemoGroupStatechartContext.h"
#include "ObserveWiping.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <RobotAPI/libraries/core/observerfilters/PoseMedianFilter.h>
#include <RobotAPI/libraries/core/observerfilters/PoseAverageFilter.h>
#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/components/ArViz/Client/ScopedClient.h>

#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/observers/filters/AverageFilter.h>

#include <SimoxUtility/shapes/AxisAlignedBoundingBox.h>
#include <SimoxUtility/math/pose/transform.h>
#include <SimoxUtility/math/pose/pose.h>

#include <ArmarXCore/core/time/TimeUtil.h>

#include <fftw3.h>

using namespace armarx;
using namespace visionx;
using namespace Armar6WipingDemoGroup;

// DO NOT EDIT NEXT LINE
ObserveWiping::SubClassRegistry ObserveWiping::Registry(ObserveWiping::GetName(), &ObserveWiping::CreateInstance);

void ObserveWiping::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    //getMessageDisplay()->setMessage("Observing human being", "");
    getDebugDrawerTopic()->clearAll();
}

void ObserveWiping::run()
{
    viz::Client arviz = viz::Client::createFromTopic("ObserverWiping", getArvizTopic());

    getTextToSpeech()->reportText("Can you please show me how to wipe?");

    VirtualRobot::RobotPtr robot = getLocalRobot();
    VirtualRobot::RobotPtr cameraRobot = getLocalRobot(); // receives new copy
    ARMARX_CHECK_EXPRESSION(robot.get() != cameraRobot.get());

    const Eigen::Vector3f wipingAreaMin = in.getWipingDemoAreaMinInRobotFrame()->toEigen();
    const Eigen::Vector3f wipingAreaMax = in.getWipingDemoAreaMaxInRobotFrame()->toEigen();

    {
        viz::Layer layer = arviz.layer("Wiping Demo Area");

        viz::Polygon area("Wiping Demo Area");
        float z = 0.5 * (wipingAreaMin.z() + wipingAreaMax.z());

        area.addPoint({wipingAreaMin.x(), wipingAreaMin.y(), z})
        .addPoint({wipingAreaMin.x(), wipingAreaMax.y(), z})
        .addPoint({wipingAreaMax.x(), wipingAreaMax.y(), z})
        .addPoint({wipingAreaMax.x(), wipingAreaMin.y(), z});
        area.color(simox::Color::orange(255, 0)).lineColor(simox::Color::orange()).lineWidth(5);
        area.pose(robot->getGlobalPose());

        layer.add(area);
        arviz.commit(layer);
    }

    // wait until demonstrator is ready
    float waitUntilObservation = in.getWaitingTimeUntilObservation();
    TimeUtil::Sleep(waitUntilObservation);

    Armar6WipingDemoGroupStatechartContextExtension* c = getContext<Armar6WipingDemoGroupStatechartContextExtension>();
    c->getOpenPoseEstimation()->start3DPoseEstimation();
    ARMARX_CHECK_EXPRESSION(c);
    CycleUtil cycle(100);

    filters::PoseMedianFilter rwristFilter(3); // make variable
    int bSizeAvg = 10;
    int bSizeRec = 140;
    int bDrawing = 300;

    std::vector<FramedPositionPtr> convAverage(bSizeAvg);
    std::vector<FramedPositionPtr> data(bSizeRec);


    std::string kinematicChainName = "RightArm"; // make variable
    ARMARX_CHECK_EXPRESSION(robot);
    auto nodeset = robot->getRobotNodeSet(kinematicChainName);
    auto tcp = nodeset->getTCP();
    auto kinematicRootName = nodeset->getKinematicRoot()->getName();

    ARMARX_INFO << VAROUT(kinematicRootName) << " position in root: " << nodeset->getKinematicRoot()->getPositionInRootFrame();
    RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    ARMARX_IMPORTANT << "GlobalPose: " << robot->getGlobalPose();
    FramedPositionPtr idlePosition = new FramedPosition(tcp->getPositionInRootFrame(), robot->getRootNode()->getName(), robot->getName());

    int trajPointCycle = 0;
    int dataCounter = 0;
    std::size_t trajPointCounter = 0;
    int drawingPointCycle = 0;
    bool startedRecording = false;
    IceUtil::Time timeRecorded = TimeUtil::GetTime();

    viz::Layer layerWristSamples = arviz.layer("Wrist Samples");

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        if (drawingPointCycle > bDrawing - 1)
        {
            drawingPointCycle = 0;
        }
        // reset the buffers
        if (trajPointCycle > bSizeAvg - 1)
        {
            trajPointCycle = 0;
        }
        if (dataCounter > bSizeRec - 1)
        {
            double timeDuration = (TimeUtil::GetTime() - timeRecorded).toSecondsDouble();
            getTextToSpeech()->reportText("Ok thank you, please let me try to wipe now.");

            // finalize
            ARMARX_IMPORTANT << "Leave observing state ...";
            out.setTrajectoryRecorded(data);
            out.setWipingTimeDuration(timeDuration);
            if (in.getStopOpenPoseAfterRun())
            {
                ARMARX_INFO << "Stopping OpenPose";
                c->getOpenPoseEstimation()->stop();
            }

            TimeUtil::MSSleep(1000);

            dataCounter = 0;
            break;
        }

        // lets draw the OpenPose polygon first
        // original: 100,2100;6050,2100;6050,4760;100,4760;100,2100
        // extended: 100,2100;6050,2100;6050,5760;100,5760;100,2100
        //        PolygonPointList points;
        //        points.push_back(new Vector3(Eigen::Vector3f(100, 2100, 100)));
        //        points.push_back(new Vector3(Eigen::Vector3f(6050, 2100, 100)));
        //        points.push_back(new Vector3(Eigen::Vector3f(6050, 5760, 100)));
        //        points.push_back(new Vector3(Eigen::Vector3f(100, 5760, 100)));
        //        points.push_back(new Vector3(Eigen::Vector3f(100, 2100, 100)));
        //        DrawColor c1 {0, 0, 0, 0};
        //        DrawColor c2 {0, 0, 1, 1};
        //        getDebugDrawerTopic()->setPolygonVisu("OpenPosePolygon", "OpenPosePol", points, c1, c2 , 5);



        auto now = armarx::TimeUtil::GetTime();

        // do your calculations
        std::vector<Keypoint3DMap> keypointList;
        IceUtil::Time timestamp;
        std::tie(timestamp, keypointList) = c->getPoseData()->getLatestData();

        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
        RemoteRobot::synchronizeLocalCloneToTimestamp(cameraRobot, getRobotStateComponent(), timestamp.toMicroSeconds());

        Keypoint3DMap poseData = getClosestPerson(keypointList);
        int validKeypoints = 0;
        for (auto& pair : poseData)
        {
            if (isKeypointValid(pair.second))
            {
                validKeypoints++;
            }
        }

        Keypoint3D rwrist = poseData["RWrist"];
        Keypoint3D rElbow = poseData["RElbow"];
        Vector3Ptr rwristPosition = new Vector3(rwrist.x, rwrist.y, rwrist.z);
        Vector3Ptr rElbowPosition = new Vector3(rElbow.x, rElbow.y, rElbow.z);

        if (isKeypointValid(rwrist))
        {
            rwristFilter.update(now.toMicroSeconds(), new Variant(rwristPosition));
        }


        auto rwristFilteredValue = rwristFilter.getValue();
        if (rwristFilteredValue)
        {
            Vector3Ptr filteredRWristPosition = VariantPtr::dynamicCast(rwristFilteredValue)->get<Vector3>();
            ARMARX_CHECK_EXPRESSION(filteredRWristPosition);

            FramedPositionPtr globalrWristPosition = FramedPosition(filteredRWristPosition->toEigen(), "DepthCamera", robot->getName()).toRootFrame(robot);
            globalrWristPosition->y -= 200.0;
            globalrWristPosition = globalrWristPosition->toGlobal(robot);
            convAverage[trajPointCycle++] = globalrWristPosition;

            trajPointCounter++;
            drawingPointCycle++;

            this->getDebugDrawerTopic()->setSphereVisu("ObserveWiping", "rwristCandidate", globalrWristPosition, DrawColor {1.0f, 1.0f,  0.0f, 1.0}, 10);

            if (trajPointCounter <= convAverage.size())
            {
                ARMARX_INFO << "Waiting for data! trajPointCounter: " << trajPointCounter << ", convAverage: " << convAverage.size();
                continue;
            }

            // create average
            FramedPositionPtr average = new FramedPosition(Eigen::Vector3f(0, 0, 0), GlobalFrame, "");
            for (auto position : convAverage)
            {
                average->x += position->x;
                average->y += position->y;
                average->z += position->z;
            }
            average->x /= convAverage.size();
            average->y /= convAverage.size();
            average->z /= convAverage.size();

            // let's restrict the z axis to workbench height
            average->z = 1020;
            {
                viz::Layer layer = arviz.layer("Wrist Position");
                layer.add(viz::Sphere("Wrist Position").position(average->toEigen()).radius(50).color(viz::Color::orange(255, 96)));
                if (false)
                {
                    Eigen::Matrix4f centerPose = simox::math::pose(0.5 * (wipingAreaMin + wipingAreaMax));
                    layer.add(viz::Box("Wiping Demo Area").pose(robot->getGlobalPose() * centerPose)
                              .size(wipingAreaMax - wipingAreaMin)
                              .color(viz::Color::cyan(255, 64)));
                }
                arviz.commit(layer);
            }

            if (!isInWipingDemoSpace(average->toRootFrame(robot)->toEigen(), wipingAreaMin, wipingAreaMax))
            {
                ARMARX_INFO << "Not in Wiping Demo Space!";

                // if we're out of the demo space
                // we'll restart recording
                //data.clear();
                //dataCounter = 0;
                startedRecording = false;

                this->getDebugDrawerTopic()->clearLayer("ObserveWiping");
                continue;
            }
            else
            {
                timeRecorded = TimeUtil::GetTime();
                startedRecording = true;
            }

            // ARMARX_INFO << deactivateSpam(1) << VAROUT(*globalrWristPosition);
            ARMARX_IMPORTANT << "Drawing average: " << average->toEigen().transpose();

            // calculate traj in root frame
            FramedPositionPtr filteredrWristPositionInRootFrame = new FramedPosition(average->toEigen(), GlobalFrame, robot->getName());
            filteredrWristPositionInRootFrame->changeFrame(robot, robot->getRootNode()->getName());
            data[dataCounter++] = filteredrWristPositionInRootFrame;

            // show the user that we are recording
            {
                layerWristSamples.add(viz::Sphere("rwrist " + std::to_string(drawingPointCycle)).position(average->toEigen()).radius(5)
                                      .color(simox::Color::orange()));
                arviz.commit(layerWristSamples);
            }

            getDebugObserver()->setDebugDatafield("ObserveWiping", "rwristpoint_x", new Variant(rwristPosition->x));
            getDebugObserver()->setDebugDatafield("ObserveWiping", "rwristpoint_y", new Variant(rwristPosition->y));
            getDebugObserver()->setDebugDatafield("ObserveWiping", "rwristpoint_z", new Variant(rwristPosition->z));
            getDebugObserver()->setDebugDatafield("ObserveWiping", "rwristpoint_x_filtered", new Variant(filteredRWristPosition->x));
            getDebugObserver()->setDebugDatafield("ObserveWiping", "rwristpoint_y_filtered", new Variant(filteredRWristPosition->y));
            getDebugObserver()->setDebugDatafield("ObserveWiping", "rwristpoint_z_filtered", new Variant(filteredRWristPosition->z));
        }

        cycle.waitForCycleDuration();
    }

    emitOnTrajectoryRecorded();


}

//void ObserveWiping::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ObserveWiping::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
    //getMessageDisplay()->setMessage("", "");
}

//workspace for demo
bool ObserveWiping::isInWipingDemoSpace(
    Eigen::Vector3f currentPos, Eigen::Vector3f min, Eigen::Vector3f max)
{
    simox::AxisAlignedBoundingBox aabb(min, max);
    bool result = aabb.is_inside(currentPos);
    ARMARX_IMPORTANT << "is \t" << currentPos.transpose() << "\n between \t" << min.transpose() << "\n and \t" << max.transpose() << "?"
                     << "\n\t " << result;
    return result;
}


Eigen::VectorXf ObserveWiping::calculateMedian(const Keypoint3DMap& keypointMap)
{
    if (keypointMap.empty())
    {
        return Eigen::Vector3f::Zero();
    }
    Eigen::MatrixXf keypointPositions(keypointMap.size(), 3);
    std::vector<std::vector<float>> values(3, Ice::FloatSeq(keypointMap.size(), 0.0));
    int i = 0;
    for (auto& pair : keypointMap)
    {
        values.at(0).at(i) = pair.second.x;
        values.at(1).at(i) = pair.second.y;
        values.at(2).at(i) = pair.second.z;
        i++;
    }

    std::sort(values.at(0).begin(), values.at(0).end());
    std::sort(values.at(1).begin(), values.at(1).end());
    std::sort(values.at(2).begin(), values.at(2).end());

    size_t center = keypointMap.size() / 2;
    return Eigen::Vector3f(values.at(0).at(center),
                           values.at(1).at(center),
                           values.at(2).at(center));
}

Eigen::VectorXf ObserveWiping::calculateCentroid(const Keypoint3DMap& keypointMap)
{
    Eigen::MatrixXf keypointPositions(keypointMap.size(), 3);
    int i = 0;
    for (auto& pair : keypointMap)
    {
        keypointPositions(i, 0) = pair.second.x;
        keypointPositions(i, 1) = pair.second.y;
        keypointPositions(i, 2) = pair.second.z;
        i++;
    }
    return keypointPositions.colwise().mean();
}

Keypoint3DMap ObserveWiping::getClosestPerson(const std::vector<Keypoint3DMap>& trackingData)
{
    float minDistance = std::numeric_limits<float>::max();
    Keypoint3DMap result;
    for (auto& keypointMap : trackingData)
    {
        if (keypointMap.empty())
        {
            continue;
        }
        float distance = calculateMedian(keypointMap).norm();
        if (distance < minDistance)
        {
            minDistance = distance;
            result = keypointMap;
        }
    }
    return result;
}

bool ObserveWiping::isKeypointValid(const Keypoint3D& point) const
{
    //ARMARX_IMPORTANT << "Confidence: " << point.confidence;
    //ARMARX_IMPORTANT << "X: " << point.x << ", Y: " << point.y << ", Z: " << point.z;
    if (point.confidence < 0.4) // change to variable
    {
        return false;
    }
    //if values directly at the camera -> not valid
    return Eigen::Vector3f(point.x, point.y, point.z).norm() > 100;
    //    return !(point.x == 0.0f && point.y == 0.0f && point.z == 0.0f);
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ObserveWiping::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ObserveWiping(stateData));
}
