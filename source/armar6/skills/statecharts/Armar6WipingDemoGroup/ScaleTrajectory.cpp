/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6WipingDemoGroup
 * @author     armar6-user ( armar6-user at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ScaleTrajectory.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6WipingDemoGroup;

// DO NOT EDIT NEXT LINE
ScaleTrajectory::SubClassRegistry ScaleTrajectory::Registry(ScaleTrajectory::GetName(), &ScaleTrajectory::CreateInstance);



void ScaleTrajectory::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void ScaleTrajectory::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    TrajectoryPtr traj = in.getTrajectory();
    StringFloatDictionary workspace = in.getWorkSpace();

    std::vector<double> xdim = traj->getDimensionData(0);
    double traj_x_max = *std::max_element(xdim.begin(), xdim.end());
    double traj_x_min = *std::min_element(xdim.begin(), xdim.end());
    float ws_x_max = workspace["x_max"];
    float ws_x_min = workspace["x_min"];


    std::vector<double> ydim = traj->getDimensionData(1);
    double traj_y_max = *std::max_element(ydim.begin(), ydim.end());
    double traj_y_min = *std::min_element(ydim.begin(), ydim.end());
    float ws_y_max = workspace["y_max"];
    float ws_y_min = workspace["y_min"];


    ARMARX_IMPORTANT << "ws_x_min: " << ws_x_min <<
                     " ws_x_max: " << ws_x_max <<
                     " ws_y_min: " << ws_y_min <<
                     " ws_y_max: " << ws_y_max;

    ARMARX_IMPORTANT << "traj_x_max: " << traj_x_max <<
                     " traj_x_min: " << traj_x_min <<
                     " traj_y_max: " << traj_y_max <<
                     " traj_y_min: " << traj_y_min;



    double ratio = 1;
    if (traj_x_max != traj_x_min && traj_y_max != traj_y_min)
    {
        double xratio = (ws_x_max - ws_x_min) / (traj_x_max - traj_x_min);
        double yratio = (ws_y_max - ws_y_min) / (traj_y_max - traj_y_min);

        if (xratio > yratio)
        {
            ratio = yratio;
        }
        else
        {
            ratio = xratio;
        }
    }
    else if (traj_x_max != traj_x_min)
    {
        ratio = (ws_x_max - ws_x_min) / (traj_x_max - traj_x_min);
    }
    else
    {
        ratio = (ws_y_max - ws_y_min) / (traj_y_max - traj_y_min);
    }

    if (ratio > 1)
    {
        ratio = 1;
    }

    double length = 0;
    double ws_cx = (ws_x_max + ws_x_min) * 0.5;
    double ws_cy = (ws_y_max + ws_y_min) * 0.5;
    double traj_cx = (traj_x_max + traj_x_min) * 0.5;
    double traj_cy = (traj_y_max + traj_y_min) * 0.5;
    for (size_t i = 0; i < xdim.size(); ++i)
    {
        xdim[i] = ws_cx + ratio * (xdim[i] - traj_cx);
        ydim[i] = ws_cy + ratio * (ydim[i] - traj_cy);
        if (i < xdim.size() - 1)
        {
            length += sqrt((xdim[i + 1] - xdim[i]) * (xdim[i + 1] - xdim[i]) + (ydim[i + 1] - ydim[i]) * (ydim[i + 1] - ydim[i]));
        }
    }

    if (length == 0)
    {
        ARMARX_ERROR << "Invalid trajectory with zero length";
        emitFailure();
    }

    double PeriodTimeDuration = in.getPeriodTimeDuration();
    double unitTimeDuration = PeriodTimeDuration / length;

    std::vector<double> timestamps;
    timestamps.push_back(0);
    for (size_t i = 0;  i < xdim.size() - 1; i++)
    {
        double arclength = sqrt((xdim[i + 1] - xdim[i]) * (xdim[i + 1] - xdim[i]) + (ydim[i + 1] - ydim[i]) * (ydim[i + 1] - ydim[i]));
        double dt = unitTimeDuration * arclength;
        timestamps.push_back(timestamps[i] + dt);
    }

    int sampleStep = std::max(1, (int)xdim.size() / 1000);

    ARMARX_IMPORTANT << "sample the trajectory with step: " << sampleStep;
    TrajectoryPtr newTraj = new Trajectory();

    std::vector<double> sampled_t;
    std::vector<double> sampled_x;
    std::vector<double> sampled_y;
    std::vector<double> zdim, qw, qx, qy, qz;
    for (size_t i = 0; i < xdim.size() - 1; i = i + sampleStep)
    {
        sampled_t.push_back(timestamps[i]);
        sampled_x.push_back(xdim[i]);
        sampled_y.push_back(ydim[i]);
        zdim.push_back(1020);
        qw.push_back(1.0);
        qx.push_back(0.0);
        qy.push_back(0.0);
        qz.push_back(0.0);
    }

    newTraj->addDimension(sampled_x, sampled_t);
    newTraj->addDimension(sampled_y, sampled_t);
    newTraj->addDimension(zdim, sampled_t);
    newTraj->addDimension(qw, sampled_t);
    newTraj->addDimension(qx, sampled_t);
    newTraj->addDimension(qy, sampled_t);
    newTraj->addDimension(qz, sampled_t);
    //    newTraj->gaussianFilter(0.05);


    VirtualRobot::RobotPtr robot = getLocalRobot();
    RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());


    Eigen::Matrix4f wipingPrePose = robot->getRobotNodeSet(in.getArmName())->getTCP()->getPoseInRootFrame();
    wipingPrePose(0, 3) = sampled_x[0];
    wipingPrePose(1, 3) = sampled_y[0];

    // debug draw ==> comment it out for real demo

    {
        Eigen::Vector3f poly_left_bottom;
        poly_left_bottom << workspace["x_min"], workspace["y_min"], 1020;
        poly_left_bottom = robot->toGlobalCoordinateSystemVec(poly_left_bottom);
        Eigen::Vector3f poly_right_bottom;
        poly_right_bottom << workspace["x_max"], workspace["y_min"], 1020;
        poly_right_bottom = robot->toGlobalCoordinateSystemVec(poly_right_bottom);
        Eigen::Vector3f poly_left_up;
        poly_left_up << workspace["x_min"], workspace["y_max"], 1020;
        poly_left_up = robot->toGlobalCoordinateSystemVec(poly_left_up);
        Eigen::Vector3f poly_right_up;
        poly_right_up << workspace["x_max"], workspace["y_max"], 1020;
        poly_right_up = robot->toGlobalCoordinateSystemVec(poly_right_up);

        getDebugDrawerTopic()->setLineVisu("workspace", "polygon_left", new Vector3(poly_left_bottom), new Vector3(poly_left_up), 10, DrawColor {1, 0, 0, 1});
        getDebugDrawerTopic()->setLineVisu("workspace", "polygon_up", new Vector3(poly_left_up), new Vector3(poly_right_up), 10, DrawColor {1, 0, 0, 1});
        getDebugDrawerTopic()->setLineVisu("workspace", "polygon_right", new Vector3(poly_right_up), new Vector3(poly_right_bottom), 10, DrawColor {1, 0, 0, 1});
        getDebugDrawerTopic()->setLineVisu("workspace", "polygon_bottom", new Vector3(poly_right_bottom), new Vector3(poly_left_bottom), 10, DrawColor {1, 0, 0, 1});
    }

    double dt = 0.01;
    for (double t = 0; t < timestamps[timestamps.size() - 1]; t = t + dt)
    {
        ARMARX_IMPORTANT << "localposition: " << newTraj->getState(t, 0, 0) << " " << newTraj->getState(t, 1, 0);
        Eigen::Vector3f globalPosition = robot->toGlobalCoordinateSystemVec(Eigen::Vector3f(newTraj->getState(t, 0, 0), newTraj->getState(t, 1, 0), 1020));
        DrawColor c {1, 0, 0, 1};
        getDebugDrawerTopic()->setSphereVisu("scaledTraj", "ss" + std::to_string(t), new Vector3(globalPosition), c, 5);
    }




    out.setWipingPrePose(wipingPrePose);
    out.setScaledTrajectory(newTraj);
    emitSuccess();

}

//void ScaleTrajectory::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void ScaleTrajectory::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ScaleTrajectory::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ScaleTrajectory(stateData));
}
