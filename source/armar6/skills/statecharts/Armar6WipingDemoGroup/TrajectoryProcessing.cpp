/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6WipingDemoGroup
 * @author     armar6-user ( armar6-user )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TrajectoryProcessing.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/core/time/CycleUtil.h>
#include <math.h>
#include <algorithm>
#include <fftw3.h>
#include <RobotAPI/libraries/core/Trajectory.h>

// just for debugging
#include <iostream>
#include <fstream>
#include <cstdio>

using namespace armarx;
using namespace Armar6WipingDemoGroup;

// DO NOT EDIT NEXT LINE
TrajectoryProcessing::SubClassRegistry TrajectoryProcessing::Registry(TrajectoryProcessing::GetName(), &TrajectoryProcessing::CreateInstance);



void TrajectoryProcessing::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void TrajectoryProcessing::run()
{
    //CycleUtil cycle(10);
    ARMARX_IMPORTANT << "Entering TrajectoryProcessing";
    std::vector<FramedPositionPtr> trajectoryRecorded = in.getTrajectoryRecorded();
    double wipingTimeDuration = in.getWipingTimeDuration();
    ARMARX_IMPORTANT << VAROUT(wipingTimeDuration);

    // just for debugging
    //    std::ofstream myfile;
    //    myfile.open("/tmp/trajectory.txt");
    //    for (auto pos : trajectoryRecorded)
    //    {
    //        myfile << pos->x << "," << pos->y << "\n";
    //    }
    //    myfile.close();

    // truncade the first 200 points
    int truncOffset = 20;
    auto truncBegin = trajectoryRecorded.begin() + truncOffset;
    auto truncEnd = trajectoryRecorded.end();
    std::vector<FramedPositionPtr> truncatedTraj(truncBegin, truncEnd);

    int nOutput = (truncatedTraj.size() / 2 + 1);
    int nInput = truncatedTraj.size();
    std::vector<FFTResult> fftOutput(nOutput);

    // do fourier transformations to extract period
    double* fftwInX;
    double* fftwInY;
    fftw_complex* fftwOutX;
    fftw_complex* fftwOutY;

    // do FFT for X values
    fftwInX = (double*) fftw_malloc(sizeof(double) * nInput);
    fftwOutX = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nOutput);
    fftwInY = (double*) fftw_malloc(sizeof(double) * nInput);
    fftwOutY = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nOutput);

    // create input
    ARMARX_IMPORTANT << "Create FFT input";
    for (std::size_t j = 0; j < truncatedTraj.size(); j++)
    {
        fftwInX[j] = truncatedTraj[j]->x;
        fftwInY[j] = truncatedTraj[j]->y;
    }

    ARMARX_IMPORTANT << "FFTW starting ...";
    fftw_plan pX = fftw_plan_dft_r2c_1d(truncatedTraj.size(), fftwInX, fftwOutX, FFTW_ESTIMATE);
    fftw_plan pY = fftw_plan_dft_r2c_1d(truncatedTraj.size(), fftwInY, fftwOutY, FFTW_ESTIMATE);
    fftw_execute(pX);
    fftw_execute(pY);
    ARMARX_IMPORTANT << "FFTW execution finished";

    // Create output for FFT values
    for (int j = 0; j < nOutput; j++)
    {
        ARMARX_IMPORTANT << "fftwOutX: " << fftwOutX[j][0];
        fftOutput[j].x = fftwOutX[j][0];
        fftOutput[j].complex_x = fftwOutX[j][1];

        ARMARX_IMPORTANT << "fftwOutY: " << fftwOutY[j][0];
        fftOutput[j].y = fftwOutY[j][0];
        fftOutput[j].complex_y = fftwOutY[j][1];
    }

    // clear memory
    fftw_destroy_plan(pX);
    fftw_destroy_plan(pY);
    fftw_free(fftwInX);
    fftw_free(fftwInY);
    fftw_free(fftwOutX);
    fftw_free(fftwOutY);

    ARMARX_IMPORTANT << "Finished cleaning memory";

    double max_x = 0;
    double max_y = 0;
    int id_x = 0;
    int id_y = 0;
    int id = 0;
    for (auto res : fftOutput)
    {
        double x = sqrt(res.x * res.x + res.complex_x * res.complex_x);
        double y = sqrt(res.y * res.y + res.complex_y * res.complex_y);
        if (max_x < x && id != 0)
        {
            max_x = std::max(max_x, sqrt(res.x * res.x + res.complex_x * res.complex_x));
            id_x = id;
        }
        if (max_y < y && id != 0)
        {
            max_y = std::max(max_y, sqrt(res.y * res.y + res.complex_y * res.complex_y));
            id_y = id;
        }
        id++;
    }

    // calculate the freq. index
    int gcd = std::__gcd(id_x, id_y);
    ARMARX_IMPORTANT << "x " << id_x << ": " << fftOutput[id_x].x << ", y " << id_y << ": " << fftOutput[id_y].y;
    ARMARX_IMPORTANT << "gcd " << gcd;

    std::vector<FramedPositionPtr> fTraj;
    double tDuration = in.getMaxTimeDuration();

    // if theres is no period in X and Y
    if (id_x == 1 && id_y == 1)
    {
        // use the entire trajectory
        for (std::size_t i = 0; i < truncatedTraj.size(); i++)
        {
            fTraj.push_back(truncatedTraj[i]);
            // just for clearance
            wipingTimeDuration = wipingTimeDuration;
        }
    }
    // if there is only a period in X
    else if (id_x > 1 && id_y == 1)
    {
        // cut the trajectory, given the freq. index (in X)
        // we'll use the 2nd period for now
        //        int cIdx = std::round(truncatedTraj.size() / id_x);
        //        for (int i = cIdx; i < cIdx * 2; i++)
        //        {
        //            fTraj.push_back(truncatedTraj[i]);
        //            wipingTimeDuration /= id_x;
        //        }

        // calculate avg
        fTraj = getAvgPeriodTrajectory(truncatedTraj, id_x);

        tDuration /= (double)id_x;


    }
    // if there is only a period in Y
    else if (id_y > 1 && id_x == 1)
    {
        // cut the trajectory, given the freq. index (in Y)
        // we'll use the 2nd period for now
        //        int cIdy = std::round(truncatedTraj.size() / id_y);
        //        for (int i = cIdy; i < cIdy * 2; i++)
        //        {
        //            fTraj.push_back(truncatedTraj[i]);
        //            wipingTimeDuration /= id_y;
        //        }

        // calculate avg
        fTraj = getAvgPeriodTrajectory(truncatedTraj, id_y);

        tDuration /= (double)id_y;
    }
    // otherwise, we should have a period in X and Y
    else
    {
        // cut the trajectory using the GCD of X and Y
        // we'll use the 2nd period for now
        //        int cIdxy = std::round(truncatedTraj.size() / gcd);
        //        for (int i = 0; i < cIdxy; i++)
        //        {
        //            fTraj.push_back(truncatedTraj[i]);
        //            wipingTimeDuration /= gcd;
        //        }

        // calculate avg
        fTraj = getAvgPeriodTrajectory(truncatedTraj, gcd);
        tDuration /= (double)gcd;
    }

    // get transformation matrix to rotate
    Eigen::Matrix3f m;
    m = Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitZ());
    Eigen::Matrix4f transf = Eigen::Matrix4f::Identity();
    transf.block<3, 3> (0, 0) = m;
    transf.block<3, 1>(0, 3) << 0.0, 1750.0, 0.0; //make this a parameter

    // get robot to visualize trajectory
    auto robot = getLocalRobot();
    RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());

    // iterate and transform trajectory
    std::vector<FramedPositionPtr> trajTransformed;

    for (std::size_t i = 0; i < fTraj.size(); i++)
    {
        Eigen::Matrix4f tmpMat = Eigen::Matrix4f::Identity();
        tmpMat(0, 3) = fTraj[i]->x;
        tmpMat(1, 3) = fTraj[i]->y;
        tmpMat(2, 3) = fTraj[i]->z;
        Eigen::Matrix4f localPosition = transf.inverse() * tmpMat;

        // save transformed trajectory
        FramedPositionPtr transformedPositionPtr = new FramedPosition(localPosition, robot->getRootNode()->getName(), robot->getName());
        trajTransformed.push_back(transformedPositionPtr);
    }

    // interpolate the trajectory
    std::vector<FramedPositionPtr> trajInterpolated = trajTransformed;
    FramedPositionPtr interpolatedPosition;
    int nPoints = 10;
    double x0, y0, x1, y1, x, y, z, dX, dY;
    //    for (int i = 0; i < trajTransformed.size(); i++)
    //    {
    //        if ((i + 1) < trajTransformed.size())
    //        {
    //            trajInterpolated.push_back(trajTransformed[i]);
    //            x0 = trajTransformed[i]->x;
    //            y0 = trajTransformed[i]->y;
    //            x1 = trajTransformed[i + 1]->x;
    //            y1 = trajTransformed[i + 1]->y;
    //            z = trajTransformed[i]->z;

    //            // interpolate nPoints x times between 2 points
    //            dX = (x1 - x0) / nPoints;
    //            dY = (y1 - y0) / nPoints;
    //            x = x0;
    //            y = y0;

    //            for (int j = 0; j < nPoints; j++)
    //            {
    //                x += dX;
    //                y += dY;

    //                interpolatedPosition = new FramedPosition(Eigen::Vector3f(x, y, z), robot->getRootNode()->getName(), robot->getName());
    //                trajInterpolated.push_back(interpolatedPosition);
    //            }
    //        }
    //    }

    //interpolate the starting/end point
    x0 = trajInterpolated[trajInterpolated.size() - 1]->x;
    y0 = trajInterpolated[trajInterpolated.size() - 1]->y;
    x1 = trajInterpolated[0]->x;
    y1 = trajInterpolated[0]->y;
    z = trajInterpolated[0]->z;

    // interpolate nPoints x times between 2 points
    dX = (x1 - x0) / nPoints;
    dY = (y1 - y0) / nPoints;
    x = x0;
    y = y0;
    for (int j = 0; j < nPoints; j++)
    {
        x += dX;
        y += dY;

        interpolatedPosition = new FramedPosition(Eigen::Vector3f(x, y, z), robot->getRootNode()->getName(), robot->getName());
        trajInterpolated.push_back(interpolatedPosition);
    }

    // get largest X value
    //    double xMax = 0.0;
    //    int xMaxIdx = 0;
    //    std::vector<FramedPositionPtr> trajStartPoint;
    //    for (int i = 0; i < trajInterpolated.size(); i++)
    //    {
    //        if (trajInterpolated[i]->x > xMax)
    //        {
    //            xMax = trajInterpolated[i]->x;
    //            xMaxIdx = i;
    //        }
    //    }

    //change starting point to biggest X value
    //    for (int i = xMaxIdx; i < trajInterpolated.size(); i++)
    //    {
    //        trajStartPoint.push_back(trajInterpolated[i]);
    //    }
    //    for (int i = 0; i < xMaxIdx; i++)
    //    {
    //        trajStartPoint.push_back(trajInterpolated[i]);
    //    }

    //    double xMin = 2000.0;
    //    int xMinIdx = 0;
    std::vector<FramedPositionPtr> trajStartPoint = trajInterpolated;
    //    for (int i = 0; i < trajInterpolated.size(); i++)
    //    {
    //        if (trajInterpolated[i]->x < xMin)
    //        {
    //            xMin = trajInterpolated[i]->x;
    //            xMinIdx = i;
    //        }
    //    }


    //    for (int i = xMinIdx; i < trajInterpolated.size(); i++)
    //    {
    //        trajStartPoint.push_back(trajInterpolated[i]);
    //    }
    //    for (int i = 0; i < xMinIdx; i++)
    //    {
    //        trajStartPoint.push_back(trajInterpolated[i]);
    //    }

    // draw/output the trajectory
    //    std::string filename = "/tmp/truncatedTraj.csv";
    //    std::ofstream myfile1;
    //    myfile1.open(filename);
    double deltaTime = wipingTimeDuration / trajStartPoint.size();
    double timeCnt = 0.0;

    TrajectoryPtr traj = new Trajectory();
    getDebugDrawerTopic()->clearAll();

    for (std::size_t i = 0; i < trajStartPoint.size(); i++)
    {
        // draw desired trajectory
        FramedPositionPtr desiredPositionPtr = FramedPosition(trajStartPoint[i]->toEigen(), robot->getRootNode()->getName(), robot->getName()).toGlobal(robot);
        desiredPositionPtr->y += 500;
        this->getDebugDrawerTopic()->setSphereVisu("WipingTraj", "desTrajectory" + i, desiredPositionPtr, DrawColor {0.0f, 0.0f,  0.0f, 1.0}, 5);

        // output the trajectory into a .csv file
        //        myfile1 << timeCnt << "," << trajStartPoint[i]->x << "," << trajStartPoint[i]->y << "," << 0 << "," << 1 << "," << 0 << "," << 0 << "," << 0 <<  "\n";

        traj->setPositionEntry(timeCnt, 0, trajStartPoint[i]->x);
        traj->setPositionEntry(timeCnt, 1, trajStartPoint[i]->y);
        traj->setPositionEntry(timeCnt, 2, 0);
        traj->setPositionEntry(timeCnt, 3, 0);
        traj->setPositionEntry(timeCnt, 4, 0);
        traj->setPositionEntry(timeCnt, 5, 0);
        traj->setPositionEntry(timeCnt, 6, 0);

        timeCnt += deltaTime;
    }
    //    myfile1.close();

    //    // copy the csv file
    //    std::string command = "scp " + filename + " armar-user@armar6a-0:/home/armar-user/repos/armarx_integration/robots/armar6/rt/data/armar6_motion/WipingMotions/";
    //    ARMARX_INFO << command;
    //    [[maybe_unused]] auto returned = system(command.c_str());

    TimeUtil::MSSleep(2000);

    ARMARX_IMPORTANT << VAROUT(wipingTimeDuration);
    out.setDeltaTimeDuration(tDuration);


    out.setWipingTimeDuration(wipingTimeDuration);
    out.setProcessedTrajectory(traj);
    emitSuccess();



    // do your calculations
    //cycle.waitForCycleDuration();
}

//void TrajectoryProcessing::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

std::vector<FramedPositionPtr> TrajectoryProcessing::getAvgPeriodTrajectory(std::vector<FramedPositionPtr> trajectory, int idxPeriod)
{
    std::vector<FramedPositionPtr> avgPeriodTraj;
    int idx, pSize = floor(1.0 * trajectory.size() / idxPeriod);
    ARMARX_IMPORTANT << VAROUT(trajectory.size()) << VAROUT(idxPeriod) << VAROUT(pSize);

    // sum up
    for (int i = 0; i < idxPeriod; i++)
    {
        for (int j = 0; j < pSize; j++)
        {
            idx = i * pSize + j;
            if (i == 0)
            {
                avgPeriodTraj.push_back(trajectory[idx]);
            }
            else
            {
                avgPeriodTraj[j]->x += trajectory[idx]->x;
                avgPeriodTraj[j]->y += trajectory[idx]->y;
            }
        }
    }

    // calc. avg for each point
    for (std::size_t i = 0; i < avgPeriodTraj.size(); i++)
    {
        avgPeriodTraj[i]->x /= idxPeriod;
        avgPeriodTraj[i]->y /= idxPeriod;
    }
    return avgPeriodTraj;
}

double TrajectoryProcessing::interpolate(double x0, double y0, double x1, double y1, double x)
{
    return (y0 * (x1 - x) + y1 * (x - x0)) / (x1 - x0);
}

void TrajectoryProcessing::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TrajectoryProcessing::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TrajectoryProcessing(stateData));
}
