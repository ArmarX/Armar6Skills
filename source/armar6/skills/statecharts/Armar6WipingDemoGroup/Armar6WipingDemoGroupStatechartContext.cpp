/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "Armar6WipingDemoGroupStatechartContext.h"
#include <IceUtil/UUID.h>
#include <ArmarXCore/core/util/algorithm.h>
namespace armarx
{
    using namespace visionx;


    Armar6WipingDemoGroupStatechartContextExtension::Armar6WipingDemoGroupStatechartContextExtension()
    {
        listener = new OpenPose3DListenerI();
    }

    const IceInternal::Handle<OpenPose3DListenerI>& Armar6WipingDemoGroupStatechartContextExtension::getPoseData() const
    {
        return listener;
    }
    void Armar6WipingDemoGroupStatechartContextExtension::onInitStatechartContext()
    {
        Armar6WipingDemoGroup::Armar6WipingDemoGroupStatechartContext::onInitStatechartContext();
        auto object = getIceManager()->registerObject(listener, "OpenPose3dListenerI" + IceUtil::generateUUID()).first;
        getIceManager()->subscribeTopic(object, "OpenPoseEstimation3D");
    }

    void Armar6WipingDemoGroupStatechartContextExtension::onConnectStatechartContext()
    {
        Armar6WipingDemoGroup::Armar6WipingDemoGroupStatechartContext::onConnectStatechartContext();
    }

    void armarx::OpenPose3DListenerI::report3DKeypoints(const armarx::HumanPose3DMap& data, Ice::Long timestamp, const Ice::Current&)
    {
        ARMARX_VERBOSE << deactivateSpam(1) << "Got new data of size: " << data.size() << " keys: " << ARMARX_STREAM_PRINTER
        {
            if (!data.empty())
            {
                //for (auto& pair : data.at(0))
                //{
                //    out << pair.first << ": " << pair.second.x << ", " << pair.second.y << ", " << pair.second.z << "\n";
                //}
                //                out << armarx::getMapKeys(data.at(0));
            }
        };
        std::unique_lock lock(mutex);
        std::vector<armarx::Keypoint3DMap> vec;
        for (const auto& [name, entity] : data)
        {
            vec.push_back(entity.keypointMap);
        }
        latestData = vec;
        this->timestamp = IceUtil::Time::microSeconds(timestamp);
    }

    std::pair<IceUtil::Time, std::vector<Keypoint3DMap>> OpenPose3DListenerI::getLatestData() const
    {
        std::unique_lock lock(mutex);
        return std::make_pair(timestamp, latestData);
    }
} // namespace armarx






