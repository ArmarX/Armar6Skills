/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6WipingDemoGroup::Armar6WipingDemoGroupRemoteStateOfferer
 * @author     armar6-user ( armar6-user at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6WipingDemoGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6WipingDemoGroup;

// DO NOT EDIT NEXT LINE
Armar6WipingDemoGroupRemoteStateOfferer::SubClassRegistry Armar6WipingDemoGroupRemoteStateOfferer::Registry(Armar6WipingDemoGroupRemoteStateOfferer::GetName(), &Armar6WipingDemoGroupRemoteStateOfferer::CreateInstance);



Armar6WipingDemoGroupRemoteStateOfferer::Armar6WipingDemoGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < armarx::Armar6WipingDemoGroupStatechartContextExtension> (reader)
{
}

void Armar6WipingDemoGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6WipingDemoGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6WipingDemoGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6WipingDemoGroupRemoteStateOfferer::GetName()
{
    return "Armar6WipingDemoGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6WipingDemoGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6WipingDemoGroupRemoteStateOfferer(reader));
}
