/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <armar6/skills/statecharts/Armar6WipingDemoGroup/Armar6WipingDemoGroupStatechartContext.generated.h>

#include <mutex>

namespace armarx
{

    class OpenPose3DListenerI : public armarx::OpenPose3DListener
    {


        // OpenPose3DListener interface
    public:
        void report3DKeypoints(const armarx::HumanPose3DMap&, Ice::Long, const Ice::Current&) override;
        std::pair<IceUtil::Time, std::vector<armarx::Keypoint3DMap>> getLatestData() const;
    private:
        mutable std::mutex mutex;
        std::vector<armarx::Keypoint3DMap> latestData;
        IceUtil::Time timestamp;
    };

    class Armar6WipingDemoGroupStatechartContextExtension : virtual public Armar6WipingDemoGroup::Armar6WipingDemoGroupStatechartContext
    {
    public:
        Armar6WipingDemoGroupStatechartContextExtension();
        ~Armar6WipingDemoGroupStatechartContextExtension() override {}
        const IceInternal::Handle<OpenPose3DListenerI>& getPoseData() const;
        // StatechartContext interface
    protected:
        void onInitStatechartContext() override;
        void onConnectStatechartContext() override;
    private:
        IceInternal::Handle<OpenPose3DListenerI> listener;

    };

} // namespace armarx
