/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6WipingDemoGroup
 * @author     armar6-user ( armar6-user )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <armar6/skills/statecharts/Armar6WipingDemoGroup/TrajectoryProcessing.generated.h>

namespace armarx::Armar6WipingDemoGroup
{
    class TrajectoryProcessing :
        public TrajectoryProcessingGeneratedBase < TrajectoryProcessing >
    {
    public:
        TrajectoryProcessing(const XMLStateConstructorParams& stateData):
            XMLStateTemplate < TrajectoryProcessing > (stateData), TrajectoryProcessingGeneratedBase < TrajectoryProcessing > (stateData)
        {
        }

        // inherited from StateBase
        void onEnter() override;
        void run() override;
        // void onBreak() override;
        void onExit() override;


        // static functions for AbstractFactory Method
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static SubClassRegistry Registry;

        // DO NOT INSERT ANY CLASS MEMBERS,
        // use stateparameters instead,
        // if classmember are neccessary nonetheless, reset them in onEnter

    private:
        struct FFTResult
        {
            double x, y;
            double complex_x, complex_y;
        };

        double interpolate(double x0, double y0, double x1, double y1, double x);
        std::vector<FramedPositionPtr> getAvgPeriodTrajectory(std::vector<FramedPositionPtr> trajectory, int idxPeriod);
    };
}
