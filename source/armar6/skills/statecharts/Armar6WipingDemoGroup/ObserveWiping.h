/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6WipingDemoGroup
 * @author     armar6-user ( armar6-user at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <armar6/skills/statecharts/Armar6WipingDemoGroup/ObserveWiping.generated.h>
#include "Armar6WipingDemoGroupStatechartContext.h"

namespace armarx::Armar6WipingDemoGroup
{
    class ObserveWiping :
        public ObserveWipingGeneratedBase < ObserveWiping >
    {
    public:
        ObserveWiping(const XMLStateConstructorParams& stateData):
            XMLStateTemplate < ObserveWiping > (stateData), ObserveWipingGeneratedBase < ObserveWiping > (stateData)
        {
        }

        // inherited from StateBase
        void onEnter() override;
        void run() override;
        // void onBreak() override;
        void onExit() override;

        Eigen::VectorXf calculateCentroid(const armarx::Keypoint3DMap& keypointMap);
        armarx::Keypoint3DMap getClosestPerson(const std::vector<armarx::Keypoint3DMap>& trackingData);
        bool isKeypointValid(const armarx::Keypoint3D& point) const;
        Eigen::VectorXf calculateMedian(const armarx::Keypoint3DMap& keypointMap);
        bool isInWipingDemoSpace(Eigen::Vector3f currentPos, Eigen::Vector3f min, Eigen::Vector3f max);

        // static functions for AbstractFactory Method
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static SubClassRegistry Registry;

        // DO NOT INSERT ANY CLASS MEMBERS,
        // use stateparameters instead,
        // if classmember are neccessary nonetheless, reset them in onEnter
    };
}
