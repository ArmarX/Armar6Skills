/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6SearchAction::Armar6SearchActionRemoteStateOfferer
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6SearchActionRemoteStateOfferer.h"

namespace armarx::Armar6SearchAction
{
    // DO NOT EDIT NEXT LINE
    Armar6SearchActionRemoteStateOfferer::SubClassRegistry Armar6SearchActionRemoteStateOfferer::Registry(Armar6SearchActionRemoteStateOfferer::GetName(), &Armar6SearchActionRemoteStateOfferer::CreateInstance);

    Armar6SearchActionRemoteStateOfferer::Armar6SearchActionRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
        XMLRemoteStateOfferer < Armar6SearchActionStatechartContext > (reader)
    {}

    void Armar6SearchActionRemoteStateOfferer::onInitXMLRemoteStateOfferer() {}

    void Armar6SearchActionRemoteStateOfferer::onConnectXMLRemoteStateOfferer() {}

    void Armar6SearchActionRemoteStateOfferer::onExitXMLRemoteStateOfferer() {}

    // DO NOT EDIT NEXT FUNCTION
    std::string Armar6SearchActionRemoteStateOfferer::GetName()
    {
        return "Armar6SearchActionRemoteStateOfferer";
    }

    // DO NOT EDIT NEXT FUNCTION
    XMLStateOffererFactoryBasePtr Armar6SearchActionRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
    {
        return XMLStateOffererFactoryBasePtr(new Armar6SearchActionRemoteStateOfferer(reader));
    }
}
