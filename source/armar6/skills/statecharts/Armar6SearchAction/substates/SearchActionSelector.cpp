/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6SearchAction
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SearchActionSelector.h"

#include <ArmarXCore/core/time/TimeUtil.h>

#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

namespace armarx
{
    namespace Armar6SearchAction
    {
        // DO NOT EDIT NEXT LINE
        SearchActionSelector::SubClassRegistry SearchActionSelector::Registry(SearchActionSelector::GetName(), &SearchActionSelector::CreateInstance);

        void SearchActionSelector::onEnter()
        {
            // put your user code for the enter-point here
            // execution time should be short (<100ms)
        }

        struct ViewTarget
        {
            float yaw = 0;
            float pitch = 0;

            std::map<std::string, float> getJointAngleMap()
            {
                return {{"Neck_1_Yaw", yaw}, {"Neck_2_Pitch", pitch}};
            }
        };

        struct SearchActionInfo
        {
            SearchActionInfo(std::string name)
                : name(name)
            {}
            std::string name;
            std::string nextAction;
            std::string landmark = "";
            std::vector<ViewTarget> viewTargets;
            bool useTravelGaze = true;

            SearchActionInfo& setLandmark(std::string landmark)
            {
                this->landmark = landmark;
                return *this;
            }
            SearchActionInfo& addViewTarget(float yaw, float pitch)
            {
                this->viewTargets.push_back({yaw, pitch});
                return *this;
            }
            SearchActionInfo& setUseTravelGaze(bool value)
            {
                useTravelGaze = value;
                return *this;
            }


        };

        SearchActionInfo getAction(std::string name)
        {
            // instead of named actions an index could be used. However, named actions help with debugging
            // --> parent state has named state, instead of index-state
            std::vector<SearchActionInfo> actions;
            actions.push_back(SearchActionInfo("Init"));
            actions.push_back(SearchActionInfo("LookAtWorkbench")
                              .setLandmark("wb_front").addViewTarget(-0.3, 0.67).addViewTarget(0, 0.67).addViewTarget(0.3, 0.67));
            actions.push_back(SearchActionInfo("LookAtGuardStand")
                              .setLandmark("gl_middle").addViewTarget(-0.37, 0.46));
            actions.push_back(SearchActionInfo("SearchTechnician")
                              .setLandmark("gl_middle").setUseTravelGaze(false)
                              .addViewTarget(-1.17, 0.31).addViewTarget(-0.4, 0.31).addViewTarget(0.32, 0.31).addViewTarget(1.00, 0.31));
            actions.push_back(SearchActionInfo("LookAtDiverterClose")
                              .setLandmark("gl_lowering").addViewTarget(0.29, -0.44));
            actions.push_back(SearchActionInfo("MoveBack")
                              .setLandmark("gl_middle"));
            actions.push_back(SearchActionInfo("FIN"));

            for (size_t i = 0; i < actions.size() - 1; i++)
            {
                actions.at(i).nextAction = actions.at(i + 1).name;
            }


            std::stringstream ss;
            for (const SearchActionInfo& a : actions)
            {
                if (a.name == name)
                {
                    return a;
                }
                ss << a.name << ", ";
            }

            throw LocalException("Action ") << name << " not found. Available: " << ss.str();
        }


        void SearchActionSelector::run()
        {
            VirtualRobot::RobotPtr robot = getLocalRobot();
            KinematicUnitHelper kinUnitHelper(getKinematicUnit());

            std::string state = in.getCurrentState();
            std::string subState = in.getCurrentSubState();
            ViewTarget travelGaze = ViewTarget{0, 0.3};

            while (!isRunningTaskStopped()) // stop run function if returning true
            {
                RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
                SearchActionInfo a = getAction(state);

                if (a.name == "FIN")
                {
                    out.setGoalName("");
                    out.settargetLandmark("");
                    out.setCurrentState(a.name);
                    out.setCurrentSubState("FIN");
                    emitDone();
                    return;
                }

                if (subState == "Init")
                {
                    subState = "Drive";
                    if (a.landmark != "")
                    {
                        if (a.useTravelGaze)
                        {
                            kinUnitHelper.setJointAngles(travelGaze.getJointAngleMap());
                        }
                        out.setGoalName(a.landmark);
                        out.settargetLandmark(a.landmark);
                        out.setCurrentSubState(subState);
                        out.setCurrentState(a.name);
                        emitMovePlatform();
                        return;
                    }
                }
                if (subState == "Drive")
                {
                    subState = "Look";
                    for (ViewTarget v : a.viewTargets)
                    {
                        kinUnitHelper.setJointAngles(v.getJointAngleMap());
                        TimeUtil::SleepMS(in.getViewDurationMS());
                    }
                    state = a.nextAction;
                    subState = "Init";
                }






                TimeUtil::SleepMS(10);
            }
        }

        //void SearchActionSelector::onBreak()
        //{
        //    // put your user code for the breaking point here
        //    // execution time should be short (<100ms)
        //}

        void SearchActionSelector::onExit()
        {
            // put your user code for the exit point here
            // execution time should be short (<100ms)
        }


        // DO NOT EDIT NEXT FUNCTION
        XMLStateFactoryBasePtr SearchActionSelector::CreateInstance(XMLStateConstructorParams stateData)
        {
            return XMLStateFactoryBasePtr(new SearchActionSelector(stateData));
        }
    }
}
