/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::GuardPoseEstimationGroup
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GetLatestGuardPose.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>


using namespace armarx;
using namespace GuardPoseEstimationGroup;

// DO NOT EDIT NEXT LINE
GetLatestGuardPose::SubClassRegistry GetLatestGuardPose::Registry(GetLatestGuardPose::GetName(), &GetLatestGuardPose::CreateInstance);



void GetLatestGuardPose::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void GetLatestGuardPose::run()
{
    data::GuardPose guard = getGuardPoseEstimation()->getLatestGuardPose();

    out.setGuardPosition(guard.position);
    out.setGuardOrientation(guard.orientation);
    out.setGuardPlaneNormal(guard.planeNormal);
    out.setGuardPlaneOffset(guard.planeOffset);

    if (false)
    {
        ARMARX_INFO << "Position: " << Vector3Ptr::dynamicCast(guard.position)->output();
        ARMARX_INFO << "Orientation: " << QuaternionPtr::dynamicCast(guard.orientation)->output();

        ARMARX_INFO << "Plane normal: " << Vector3Ptr::dynamicCast(guard.planeNormal)->output();
        ARMARX_INFO << "Plane offset: " << guard.planeOffset;
    }

    emitSuccess();
}

//void GetLatestGuardPose::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void GetLatestGuardPose::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr GetLatestGuardPose::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new GetLatestGuardPose(stateData));
}
