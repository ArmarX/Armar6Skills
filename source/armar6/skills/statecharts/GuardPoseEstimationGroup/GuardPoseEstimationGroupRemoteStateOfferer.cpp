/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::GuardPoseEstimationGroup::GuardPoseEstimationGroupRemoteStateOfferer
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GuardPoseEstimationGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace GuardPoseEstimationGroup;

// DO NOT EDIT NEXT LINE
GuardPoseEstimationGroupRemoteStateOfferer::SubClassRegistry GuardPoseEstimationGroupRemoteStateOfferer::Registry(GuardPoseEstimationGroupRemoteStateOfferer::GetName(), &GuardPoseEstimationGroupRemoteStateOfferer::CreateInstance);



GuardPoseEstimationGroupRemoteStateOfferer::GuardPoseEstimationGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < GuardPoseEstimationGroupStatechartContext > (reader)
{
}

void GuardPoseEstimationGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void GuardPoseEstimationGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void GuardPoseEstimationGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string GuardPoseEstimationGroupRemoteStateOfferer::GetName()
{
    return "GuardPoseEstimationGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr GuardPoseEstimationGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new GuardPoseEstimationGroupRemoteStateOfferer(reader));
}
