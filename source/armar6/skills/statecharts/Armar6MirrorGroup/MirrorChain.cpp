/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6MirrorGroup
 * @author     [Author Name] ( [Author Email] )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MirrorChain.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace Armar6MirrorGroup;

// DO NOT EDIT NEXT LINE
MirrorChain::SubClassRegistry MirrorChain::Registry(MirrorChain::GetName(), &MirrorChain::CreateInstance);



void MirrorChain::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void MirrorChain::run()
{
    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(getRobotStateComponent());

    VirtualRobot::RobotNodeSetPtr sourceRns = localRobot->getRobotNodeSet(in.getSourceKinematicChain());
    VirtualRobot::RobotNodeSetPtr targetRns = localRobot->getRobotNodeSet(in.getTargetKinematicChain());

    NameValueMap ja;
    std::vector<int> signs = in.getRotationSigns();

    ARMARX_CHECK_EQUAL(sourceRns->getAllRobotNodes().size(), targetRns->getAllRobotNodes().size());
    ARMARX_CHECK_EQUAL(sourceRns->getAllRobotNodes().size(), signs.size());

    std::vector<std::string> targetNames = targetRns->getNodeNames();

    NameControlModeMap controlModes;
    for (const std::string& name : in.getTorqueControlledJoints())
    {
        controlModes[name] = in.getIsSimulation() ? ePositionControl : eTorqueControl;
    }
    for (const std::string& name : targetNames)
    {
        controlModes[name] = ePositionControl;
    }
    getKinematicUnit()->switchControlMode(controlModes);

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        RemoteRobot::synchronizeLocalClone(localRobot, getRobotStateComponent());

        for (size_t i = 0; i < signs.size(); i++)
        {
            ja[targetNames.at(i)] = signs.at(i) * sourceRns->getNode(i)->getJointValue();
        }

        getKinematicUnit()->setJointAngles(ja);

        usleep(10000);
    }

    for (const std::string& name : in.getTorqueControlledJoints())
    {
        controlModes[name] = ePositionControl;
    }
    getKinematicUnit()->switchControlMode(controlModes);
}

//void MirrorChain::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void MirrorChain::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr MirrorChain::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new MirrorChain(stateData));
}
