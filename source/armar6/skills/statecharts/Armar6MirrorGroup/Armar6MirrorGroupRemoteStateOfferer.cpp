/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::Armar6MirrorGroup::Armar6MirrorGroupRemoteStateOfferer
 * @author     [Author Name] ( [Author Email] )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6MirrorGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace Armar6MirrorGroup;

// DO NOT EDIT NEXT LINE
Armar6MirrorGroupRemoteStateOfferer::SubClassRegistry Armar6MirrorGroupRemoteStateOfferer::Registry(Armar6MirrorGroupRemoteStateOfferer::GetName(), &Armar6MirrorGroupRemoteStateOfferer::CreateInstance);



Armar6MirrorGroupRemoteStateOfferer::Armar6MirrorGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < Armar6MirrorGroupStatechartContext > (reader)
{
}

void Armar6MirrorGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void Armar6MirrorGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void Armar6MirrorGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string Armar6MirrorGroupRemoteStateOfferer::GetName()
{
    return "Armar6MirrorGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr Armar6MirrorGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new Armar6MirrorGroupRemoteStateOfferer(reader));
}
