/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::LookAtHand
 * @author     armar-user ( armar6 at kit )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


// #include <mutex>

#include <ArmarXCore/core/Component.h>

// #include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>

// #include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

// #include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <SimoxUtility/shapes/AxisAlignedBoundingBox.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <bits/shared_ptr.h>
#include <VirtualRobot/VirtualRobot.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>
#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <armar6/skills/interface/LookAtHandInterface.h>

namespace armarx
{

    /**
     * @defgroup Component-LookAtHand LookAtHand
     * @ingroup armar6_skills-Components
     * A description of the component LookAtHand.
     *
     * @class LookAtHand
     * @ingroup Component-LookAtHand
     * @brief Brief description of class LookAtHand.
     *
     * Detailed description of class LookAtHand.
     */
    class LookAtHand :
        virtual public armarx::Component,
        virtual public armarx::LookAtHandInterface
    // , virtual public armarx::DebugObserverComponentPluginUser
    // , virtual public armarx::LightweightRemoteGuiComponentPluginUser
    // , virtual public armarx::ArVizComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        void enableLookAtHand(const Ice::Current& = Ice::emptyCurrent) override;
        void disableLookAtHand(const Ice::Current& = Ice::emptyCurrent) override;
        void setHandOffset(const Ice::DoubleSeq& offset, const Ice::Current& = Ice::emptyCurrent) override;
        void setRobotNodeSetName(const std::string& nodeSetName, const Ice::Current& = Ice::emptyCurrent) override;
        void setWorkspace(const Ice::DoubleSeq& workspaceMin, const Ice::DoubleSeq& workspaceMax, const Ice::Current& = Ice::emptyCurrent) override;

        /* (Requires armarx::LightweightRemoteGuiComponentPluginUser.)
        /// This function should be called once in onConnect() or when you
        /// need to re-create the Remote GUI tab.
        void createRemoteGuiTab();

        /// After calling `RemoteGui_startRunningTask`, this function is
        /// called periodically in a separate thread. If you update variables,
        /// make sure to synchronize access to them.
        void RemoteGui_update() override;
        */


    private:

        // Private methods go here.

        // Forward declare `Properties` if you used it before its defined.
        // struct Properties;

        /* (Requires the armarx::ArVizComponentPluginUser.)
        /// Draw some boxes in ArViz.
        void drawBoxes(const Properties& p, viz::Client& arviz);
        */


    private:

        // Private member variables go here.
        VirtualRobot::RobotPtr localRobot;


        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            //            float yaw_p = 0.0f;
            //            float yaw_i = 0.0f;
            //            float pitch_p = 0.0f;
            //            float pitch_i = 0.0f;
            std::string robotStateComponentName = "RobotStateComponent";
            std::string robotNodeSetName = "RightArm";
            bool enableLookAtHand = false;
        };
        Properties properties;
        /* Use a mutex if you access variables from different threads
         * (e.g. ice functions and RemoteGui_update()).
        std::mutex propertiesMutex;
        */


        /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
        /// Tab shown in the Remote GUI.
        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::LineEdit boxLayerName;
            armarx::RemoteGui::Client::IntSpinBox numBoxes;

            armarx::RemoteGui::Client::Button drawBoxes;
        };
        RemoteGuiTab tab;
        */


        /* (Requires the armarx::ArVizComponentPluginUser.)
         * When used from different threads, an ArViz client needs to be synchronized.
        /// Protects the arviz client inherited from the ArViz plugin.
        std::mutex arvizMutex;
        */

        simox::AxisAlignedBoundingBox workspace;
        Eigen::Vector2f pitchAngleLimit;
        Eigen::Vector2f yawAngleLimit;
        Eigen::Vector3f handPositionOffset;

        PeriodicTask<LookAtHand>::pointer_type lookAtHandTask;

        RobotStateComponentInterfacePrx robotStateComponent;
        KinematicUnitInterfacePrx kinematicUnit;
        KinematicUnitHelperPtr kinUnitHelper;
        VirtualRobot::RobotNodeSetPtr armNodeSet;
        void run();

        bool enabled = false;
    };
}
