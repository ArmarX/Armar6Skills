/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::LookAtHand
 * @author     armar-user ( armar6 at kit )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LookAtHand.h"

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

// Include headers you only need in function definitions in the .cpp.

// #include <Eigen/Core>

// #include <SimoxUtility/color/Color.h>

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>



namespace armarx
{

    armarx::PropertyDefinitionsPtr LookAtHand::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def = new ComponentPropertyDefinitions(getConfigIdentifier());

        // Publish to a topic (passing the TopicListenerPrx).
        // def->topic(myTopicListener);

        // Subscribe to a topic (passing the topic name).
        // def->topic<PlatformUnitListener>("MyTopic");

        // Use (and depend on) another component (passing the ComponentInterfacePrx).
        // def->component(myComponentProxy)


        // Add a required property. (The component won't start without a value being set.)
        // def->required(properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");

        // Add an optionalproperty.
        //        def->optional(properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");
        //        def->optional(properties.numBoxes, "p.box.Number", "Number of boxes to draw in ArViz.");

        def->required(properties.robotStateComponentName, "RobotStateComponent", "Name of Armar6 RobotStateComponent");
        def->required(properties.robotNodeSetName, "RobotNodeSetName", "Name of robot node set, either RightArm or LeftArm");
        def->required(properties.enableLookAtHand, "EnableLookAtHand", "Name of Enable");
        def->defineOptionalPropertyVector<Eigen::Vector3f>("Workspace_Min", Eigen::Vector3f{-300, 100, 700}, "min of workspace bounding box");
        def->defineOptionalPropertyVector<Eigen::Vector3f>("Workspace_Max", Eigen::Vector3f{300, 1000, 1500}, "max of workspace bounding box");
        def->defineOptionalPropertyVector<Eigen::Vector2f>("JointLimit_Pitch", Eigen::Vector2f{0.0f, 0.85f}, "[min, max] of pitch joint angle");
        def->defineOptionalPropertyVector<Eigen::Vector2f>("JointLimit_Yaw", Eigen::Vector2f{-1.0f, 1.0f}, "[min, max] of yaw joint angle");
        def->defineOptionalPropertyVector<Eigen::Vector3f>("HandPositionOffset", Eigen::Vector3f{0.0f, 0.0f, 0.0f}, "Offsets added up to the selected hand position vector");
        return def;
    }


    void LookAtHand::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.

        // Keep debug observer data until calling `sendDebugObserverBatch()`.
        // (Requies the armarx::DebugObserverComponentPluginUser.)
        // setDebugObserverBatchModeEnabled(true);

        usingProxy(getProperty<std::string>("RobotStateComponent").getValue());

    }


    void LookAtHand::onConnectComponent()
    {
        // Do things after connecting to topics and components.


        /* (Requies the armarx::DebugObserverComponentPluginUser.)
        // Use the debug observer to log data over time.
        // The data can be viewed in the ObserverView and the LivePlotter.
        // (Before starting any threads, we don't need to lock mutexes.)
        {
            setDebugObserverDatafield("numBoxes", properties.numBoxes);
            setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
            sendDebugObserverBatch();
        }
        */

        /* (Requires the armarx::ArVizComponentPluginUser.)
        // Draw boxes in ArViz.
        // (Before starting any threads, we don't need to lock mutexes.)
        drawBoxes(properties, arviz);
        */

        /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
        // Setup the remote GUI.
        {
            createRemoteGuiTab();
            RemoteGui_startRunningTask();
        }
        */

        robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponent").getValue());
        localRobot = RemoteRobot::createLocalCloneFromFile(robotStateComponent, VirtualRobot::RobotIO::eStructure);
        kinematicUnit = getProxy<KinematicUnitInterfacePrx>("Armar6KinematicUnit");
        handPositionOffset = getProperty<Eigen::Vector3f>("HandPositionOffset");

        std::string robotNodeSetName = getProperty<std::string>("RobotNodeSetName").getValue();
        if (robotNodeSetName != "RightArm" && robotNodeSetName != "LeftArm")
        {
            ARMARX_ERROR << "The robotNodeSetName " << robotNodeSetName << " is not supported";
        }
        armNodeSet = localRobot->getRobotNodeSet(robotNodeSetName);
        enabled = getProperty<bool>("EnableLookAtHand").getValue();

        workspace.set_min(getProperty<Eigen::Vector3f>("Workspace_Min"));
        workspace.set_max(getProperty<Eigen::Vector3f>("Workspace_Max"));

        pitchAngleLimit = getProperty<Eigen::Vector2f>("JointLimit_Pitch");
        yawAngleLimit = getProperty<Eigen::Vector2f>("JointLimit_Yaw");

        lookAtHandTask = new PeriodicTask<LookAtHand>(this, &LookAtHand::run, 10);
        lookAtHandTask->start();

    }


    void LookAtHand::onDisconnectComponent()
    {

    }


    void LookAtHand::onExitComponent()
    {

    }

    void LookAtHand::enableLookAtHand(const Ice::Current&)
    {
        enabled = true;
    }

    void LookAtHand::disableLookAtHand(const Ice::Current&)
    {
        enabled = false;
    }

    void LookAtHand::setHandOffset(const Ice::DoubleSeq& offset, const Ice::Current&)
    {
        handPositionOffset[0] = offset[0];
        handPositionOffset[1] = offset[1];
        handPositionOffset[2] = offset[2];
    }

    void LookAtHand::setRobotNodeSetName(const std::string& nodeSetName, const Ice::Current&)
    {
        armNodeSet = localRobot->getRobotNodeSet(nodeSetName);
    }

    void LookAtHand::setWorkspace(const Ice::DoubleSeq& workspaceMin, const Ice::DoubleSeq& workspaceMax, const Ice::Current&)
    {
        Eigen::Vector3f min;
        Eigen::Vector3f max;
        min << workspaceMin[0], workspaceMin[1], workspaceMin[2];
        max << workspaceMax[0], workspaceMax[1], workspaceMax[2];
        workspace.set_min(min);
        workspace.set_max(max);
    }


    std::string LookAtHand::getDefaultName() const
    {
        return "LookAtHand";
    }

    void LookAtHand::run()
    {
        RemoteRobot::synchronizeLocalClone(localRobot, robotStateComponent);
        //        ARMARX_INFO << "running task lookat hand";
        // get hand position in camera frame
        Eigen::Vector3f armPositionInRoot = armNodeSet->getTCP()->getPositionInRootFrame();
        //        ARMARX_INFO << VAROUT(armPositionInRoot);
        if (workspace.is_inside(armPositionInRoot) and enabled)
        {
            Eigen::Vector3f armPositionInRoot = armNodeSet->getTCP()->getPositionInRootFrame();
            armPositionInRoot += handPositionOffset;
            //             = armNodeSet->getTCP()->getPositionInFrame(localRobot->getRobotNode("DepthCamera"));
            Eigen::Vector3f armPositionInCamera = localRobot->getRobotNode("DepthCamera")->toLocalCoordinateSystemVec(armNodeSet->getRobot()->toGlobalCoordinateSystemVec(armPositionInRoot));
            float x = armPositionInCamera[0];
            float y = armPositionInCamera[1];
            float z = armPositionInCamera[2];
            float yaw = std::atan2(x, z);
            float pitch = std::atan2(y, std::sqrt(x * x + z * z));

            float currentYaw = localRobot->getRobotNode("Neck_1_Yaw")->getJointValue();
            float currentPitch = localRobot->getRobotNode("Neck_2_Pitch")->getJointValue();

            // Note that:
            //   higher yaw value --> look more to the left. x-axis of depth camera frame points to the right.
            //   higher pitch value --> look more down. y-axis of the depth camera frame points down.
            // this might be jiggle
            yaw = currentYaw - yaw;
            pitch = currentPitch + pitch;

            //            Eigen::Vector2f yawLimit {-1.0f/3.0f, 1.0f/3.0f};
            //            Eigen::Vector2f pitchLimit {0.0f, 0.7f};

            //            ARMARX_INFO << "set head joints to " << VAROUT(yaw) << VAROUT(pitch) << VAROUT(currentYaw) << VAROUT(currentPitch);

            yaw = std::clamp(yaw, yawAngleLimit[0], yawAngleLimit[1]);
            pitch = std::clamp(pitch, pitchAngleLimit[0], pitchAngleLimit[1]);

            // set joint value
            armarx::NameValueMap headJointTarget;
            headJointTarget["Neck_1_Yaw"] = yaw;
            headJointTarget["Neck_2_Pitch"] = pitch;

            armarx::NameControlModeMap headJointControlModes;
            headJointControlModes["Neck_1_Yaw"] = ePositionControl;
            headJointControlModes["Neck_2_Pitch"] = ePositionControl;
            kinematicUnit->switchControlMode(headJointControlModes);
            kinematicUnit->setJointAngles(headJointTarget);
        }
        //        else
        //        {
        //            ARMARX_IMPORTANT << "hand is not in the workspace.";
        //        }

    }

    //    void LookAtHand::runVelControl()
    //    {
    //        RemoteRobot::synchronizeLocalClone(localRobot, robotStateComponent);
    //        ARMARX_INFO << "running task lookat hand";
    //        // get hand position in camera frame
    //        Eigen::Vector3f armPositionInRoot = armNodeSet->getTCP()->getPositionInRootFrame();
    //        ARMARX_INFO << VAROUT(armPositionInRoot);
    //        if (workspace.is_inside(armPositionInRoot))
    //        {
    //            ARMARX_IMPORTANT << "arm position inside workspace ...";
    //            Eigen::Vector3f armPositionInCamera = armNodeSet->getTCP()->getPositionInFrame(localRobot->getRobotNode("DepthCamera"));
    //            float x = armPositionInCamera[0];
    //            float y = armPositionInCamera[1];
    //            float z = armPositionInCamera[2];
    //            float yaw = std::atan2(x, z);
    //            float pitch = std::atan2(y, std::sqrt(x * x + z * z));

    //            float currentYaw = localRobot->getRobotNode("Neck_1_Yaw")->getJointValue();
    //            float currentPitch = localRobot->getRobotNode("Neck_2_Pitch")->getJointValue();

    //            // Note that:
    //            //   higher yaw value --> look more to the left. x-axis of depth camera frame points to the right.
    //            //   higher pitch value --> look more down. y-axis of the depth camera frame points down.
    //            // this might be jiggle
    //            yaw = currentYaw - yaw;
    //            pitch = currentPitch + pitch;

    //            //            Eigen::Vector2f yawLimit {-1.0f/3.0f, 1.0f/3.0f};
    //            //            Eigen::Vector2f pitchLimit {0.0f, 0.7f};

    //            ARMARX_INFO << "set head joints to " << VAROUT(yaw) << VAROUT(pitch) << VAROUT(currentYaw) << VAROUT(currentPitch);

    //            yaw = std::clamp(yaw, -1.0f / 3.0f, 1.0f / 3.0f);
    //            pitch = std::clamp(pitch, 0.0f, 0.7f);
    //            ARMARX_INFO << "clipped target " << VAROUT(yaw) << VAROUT(pitch);

    //            // set joint value
    //            armarx::NameValueMap headJointTarget;
    //            headJointTarget["Neck_1_Yaw"] = yaw;
    //            headJointTarget["Neck_2_Pitch"] = pitch;

    //            armarx::NameControlModeMap headJointControlModes;
    //            headJointControlModes["Neck_1_Yaw"] = ePositionControl;
    //            headJointControlModes["Neck_2_Pitch"] = ePositionControl;
    //            kinematicUnit->switchControlMode(headJointControlModes);
    //            kinematicUnit->setJointAngles(headJointTarget);
    //        }
    //        else
    //        {
    //            ARMARX_IMPORTANT << "hand is not in the workspace.";
    //        }

    //    }


    /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
    void LookAtHand::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        // Setup the widgets.

        tab.boxLayerName.setValue(properties.boxLayerName);

        tab.numBoxes.setValue(properties.numBoxes);
        tab.numBoxes.setRange(0, 100);

        tab.drawBoxes.setLabel("Draw Boxes");

        // Setup the layout.

        GridLayout grid;
        int row = 0;
        {
            grid.add(Label("Box Layer"), {row, 0}).add(tab.boxLayerName, {row, 1});
            ++row;

            grid.add(Label("Num Boxes"), {row, 0}).add(tab.numBoxes, {row, 1});
            ++row;

            grid.add(tab.drawBoxes, {row, 0}, {2, 1});
            ++row;
        }

        VBoxLayout root = {grid, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void LookAtHand::RemoteGui_update()
    {
        if (tab.boxLayerName.hasValueChanged() || tab.numBoxes.hasValueChanged())
        {
            std::scoped_lock lock(propertiesMutex);
            properties.boxLayerName = tab.boxLayerName.getValue();
            properties.numBoxes = tab.numBoxes.getValue();

            {
                setDebugObserverDatafield("numBoxes", properties.numBoxes);
                setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
                sendDebugObserverBatch();
            }
        }
        if (tab.drawBoxes.wasClicked())
        {
            // Lock shared variables in methods running in seperate threads
            // and pass them to functions. This way, the called functions do
            // not need to think about locking.
            std::scoped_lock lock(propertiesMutex, arvizMutex);
            drawBoxes(properties, arviz);
        }
    }
    */

    /* (Requires the armarx::ArVizComponentPluginUser.)
    void LookAtHand::drawBoxes(const LookAtHand::Properties& p, viz::Client& arviz)
    {
        // Draw something in ArViz (requires the armarx::ArVizComponentPluginUser.
        // See the ArVizExample in RobotAPI for more examples.

        viz::Layer layer = arviz.layer(p.boxLayerName);
        for (int i = 0; i < p.numBoxes; ++i)
        {
            layer.add(viz::Box("box_" + std::to_string(i))
                      .position(Eigen::Vector3f(i * 100, 0, 0))
                      .size(20).color(simox::Color::blue()));
        }
        arviz.commit(layer);
    }
    */

    ARMARX_DECOUPLED_REGISTER_COMPONENT(armarx::LookAtHand);

}
