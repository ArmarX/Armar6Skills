/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::Armar6NaturalIKComponent
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6NaturalIKComponent.h"

#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

//#include <RobotAPI/libraries/aron/AronNavigator.h>


namespace armarx
{
    Armar6NaturalIKComponentPropertyDefinitions::Armar6NaturalIKComponentPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        //defineRequiredProperty<std::string>("PropertyName", "Description");
        //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");

        defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver",
                                            "Name of the topic the DebugObserver listens to.");

        //defineOptionalProperty<std::string>("MyProxyName", "MyProxy", "Name of the proxy to be used");
    }


    std::string Armar6NaturalIKComponent::getDefaultName() const
    {
        return "NaturalIK";
    }


    void Armar6NaturalIKComponent::onInitComponent()
    {
        // Register offered topices and used proxies here.
        offeringTopicFromProperty("DebugObserverName");
        // debugDrawer.offeringTopic(*this);  // Calls this->offeringTopic().

        // Using a proxy will cause the component to wait until the proxy is available.
        // usingProxyFromProperty("MyProxyName");
    }


    void Armar6NaturalIKComponent::onConnectComponent()
    {
        // Get topics and proxies here. Pass the *InterfacePrx type as template argument.
        getTopicFromProperty(debugObserver, "DebugObserverName");
        // debugDrawer.getTopic(*this);  // Calls this->getTopic().

        // getProxyFromProperty<MyProxyInterfacePrx>("MyProxyName");
        iks.clear();
        iks.insert(std::make_pair("Left", SideIK("Left", getRobotStateComponent())));
        iks.insert(std::make_pair("Right", SideIK("Right", getRobotStateComponent())));
        _rnh = RobotNameHelper::Create(getRobotStateComponent()->getRobotInfo(), StatechartProfilePtr());

    }


    void Armar6NaturalIKComponent::onDisconnectComponent()
    {

    }


    void Armar6NaturalIKComponent::onExitComponent()
    {

    }




    armarx::PropertyDefinitionsPtr Armar6NaturalIKComponent::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new Armar6NaturalIKComponentPropertyDefinitions(
                getConfigIdentifier()));
    }

    NaturalIKResult Armar6NaturalIKComponent::solveIK(const std::string& side, const Eigen::Matrix4f& target, bool setOri,
                                                      const armarx::aron::data::dto::DictPtr& args, const Ice::Current&)
    {
        std::string resolvedSide = "";
        for (const auto& pair : iks)
        {
            RobotNameHelper::Arm arm = _rnh->getArm(pair.first);
            if (side == pair.first)
            {
                resolvedSide = pair.first;
                break;
            }
            if (arm.getTCP() == side)
            {
                resolvedSide = pair.first;
                break;
            }
            if (arm.getEndEffector() == side)
            {
                resolvedSide = pair.first;
                break;
            }
        }
        if (resolvedSide == "")
        {
            throw  LocalException("Could not resolve side: ") << side;
        }

        ARMARX_IMPORTANT_S << VAROUT(side) << VAROUT(resolvedSide);
        NaturalDiffIK::Result result = iks.at(resolvedSide).calculateIK(target, setOri, args);
        NaturalIKResult r;
        r.reached = result.reached;
        for (int i = 0; i < result.jointValues.size(); i++)
        {
            r.jointValues.push_back(result.jointValues(i));
        }
        return r;
    }

    Armar6NaturalIKComponent::SideIK::SideIK(const std::string& side, RobotStateComponentInterfacePrx robotStateComponent)
        : _side(side), _robotStateComponent(robotStateComponent)
    {
        init();
    }

    NaturalDiffIK::Result Armar6NaturalIKComponent::SideIK::calculateIK(const Eigen::Matrix4f& target, bool setOri, const armarx::aron::data::dto::DictPtr& args)
    {
        NaturalIK::Parameters ikparams = _ikparams;
        if (args)
        {
            //aron::data::AronObjectNavigator nav = armarx::aron::data::data::AronNavigator(args).asDict();
            //if(nav.has("minimumElbowHeight"))
            //{
            //    ikparams.minimumElbowHeight = nav.at("minimumElbowHeight").asFloat();
            //}
        }
        return _ik->calculateIK(target, _arm, setOri ? NaturalDiffIK::Mode::All : NaturalDiffIK::Mode::Position, ikparams);
    }

    void Armar6NaturalIKComponent::SideIK::init()
    {
        RobotNameHelperPtr rnh = RobotNameHelper::Create(_robotStateComponent->getRobotInfo(), StatechartProfilePtr());
        _robot = RemoteRobot::createLocalClone(_robotStateComponent);
        RobotNameHelper::Arm arm = rnh->getArm(_side);
        RemoteRobot::synchronizeLocalClone(_robot, _robotStateComponent);
        RobotNameHelper::RobotArm robotArm = arm.addRobot(_robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr cla = rns->getNode(0);
        VirtualRobot::RobotNodePtr sho1 = rns->getNode(1);
        ARMARX_IMPORTANT_S << VAROUT(cla->getJointValue());
        cla->setJointValue(0);
        Eigen::Vector3f shoulder = sho1->getPositionInRootFrame();
        VirtualRobot::RobotNodePtr elb = rns->getNode(4);
        VirtualRobot::RobotNodePtr wri1 = rns->getNode(6);
        VirtualRobot::RobotNodePtr tcp = rns->getTCP();
        _arm.rns = rns;
        _arm.shoulder = sho1;
        _arm.elbow = elb;
        _arm.tcp = tcp;



        std::vector<float> jointCosts = std::vector<float>(rns->getSize(), 1);
        jointCosts.at(0) = 4;

        RemoteRobot::synchronizeLocalClone(_robot, _robotStateComponent);
        _ik.reset(new  NaturalIK(_side, shoulder, 1.3f));
        float upper_arm_length = (sho1->getPositionInRootFrame() - elb->getPositionInRootFrame()).norm();
        float lower_arm_length = (elb->getPositionInRootFrame() - wri1->getPositionInRootFrame()).norm()
                                 + (wri1->getPositionInRootFrame() - tcp->getPositionInRootFrame()).norm();
        _ik->setUpperArmLength(upper_arm_length);
        _ik->setLowerArmLength(lower_arm_length);
    }

}
