/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::Armar6NaturalIKComponent
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
//#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>
#include <RobotAPI/interface/components/NaturalIKInterface.h>

#include <RobotAPI/libraries/natik/NaturalIK.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>

#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>

namespace armarx
{
    /**
     * @class Armar6NaturalIKComponentPropertyDefinitions
     * @brief Property definitions of `Armar6NaturalIKComponent`.
     */
    class Armar6NaturalIKComponentPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        Armar6NaturalIKComponentPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-Armar6NaturalIKComponent Armar6NaturalIKComponent
     * @ingroup armar6_skills-Components
     * A description of the component Armar6NaturalIKComponent.
     *
     * @class Armar6NaturalIKComponent
     * @ingroup Component-Armar6NaturalIKComponent
     * @brief Brief description of class Armar6NaturalIKComponent.
     *
     * Detailed description of class Armar6NaturalIKComponent.
     */
    class Armar6NaturalIKComponent :
        virtual public armarx::Component,
        virtual public NaturalIKInterface,
        virtual public RobotStateComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        class SideIK
        {
        public:
            SideIK(const std::string& side, RobotStateComponentInterfacePrx robotStateComponent);
            NaturalDiffIK::Result calculateIK(const Eigen::Matrix4f& target, bool setOri,
                                              const armarx::aron::data::dto::DictPtr& args);
        private:
            void init();
            NaturalIKPtr _ik;
            NaturalIK::ArmJoints _arm;
            NaturalIK::Parameters _ikparams;
            std::string _side;
            RobotStateComponentInterfacePrx _robotStateComponent;
            VirtualRobot::RobotPtr _robot;

        };

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        // Private methods and member variables go here.

        /// Debug observer. Used to visualize e.g. time series.
        armarx::DebugObserverInterfacePrx debugObserver;
        /// Debug drawer. Used for 3D visualization.
        //armarx::DebugDrawerTopic debugDrawer;


        std::map<std::string, SideIK> iks;
        RobotNameHelperPtr _rnh;


        // NaturalIKInterface interface
    public:
        NaturalIKResult solveIK(const std::string& side, const Eigen::Matrix4f& target, bool setOri,
                                const armarx::aron::data::dto::DictPtr& args, const Ice::Current&) override;

    };
}
