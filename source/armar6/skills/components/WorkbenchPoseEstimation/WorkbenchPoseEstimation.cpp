/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::WorkbenchPoseEstimation
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "WorkbenchPoseEstimation.h"

#include <ArmarXCore/observers/variant/Variant.h>

#include <VisionX/libraries/PointCloudTools/FramedPointCloud.h>


namespace armarx
{

    const simox::meta::EnumNames<WorkbenchPoseEstimation::ExecutionMode>
    WorkbenchPoseEstimation::executionModeNames =
    {
        { WorkbenchPoseEstimation::ExecutionMode::ONLINE, "online" },
        { WorkbenchPoseEstimation::ExecutionMode::RECORD, "record" },
        { WorkbenchPoseEstimation::ExecutionMode::REPLAY, "replay" },
    };

    WorkbenchPoseEstimationPropertyDefinitions::WorkbenchPoseEstimationPropertyDefinitions(std::string prefix) :
        visionx::PointCloudProcessorPropertyDefinitions(prefix)
    {
        Recording recording;
        recording.defineProperties(*this, "record.");

        Replay replay;
        replay.defineProperties(*this, "replay.");
    }


    armarx::PropertyDefinitionsPtr WorkbenchPoseEstimation::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs(new WorkbenchPoseEstimationPropertyDefinitions(getConfigIdentifier()));

        defs->defineOptionalProperty<std::string>("tpc.pub.DebugObserverTopicName", "DebugObserver", "DebugObserver topic.");
        defs->defineOptionalProperty<std::string>("tpc.pub.WorkbenchPoseTopicName", "WorkbenchPoseTopic", "WorkbenchPose Topic.");

        defs->defineOptionalProperty<ExecutionMode>("ExecutionMode", executionMode,
                "How to run the component:"
                "\n- online: Use live point cloud input and robot state."
                "\n- record: Just record point cloud input and robot state."
                "\n- replay: Use previously recorded point cloud input and robot state.")
        .map("online", ExecutionMode::ONLINE)
        .map("record", ExecutionMode::RECORD)
        .map("replay", ExecutionMode::REPLAY);

        defs->defineOptionalProperty<bool>("AlwaysOn", alwaysOn,
                                           "If false, runs only when requested via the RequestableServiceListenerInterface.");

        estimator.defineProperties(*defs);

        return defs;
    }


    std::string WorkbenchPoseEstimation::getDefaultName() const
    {
        return "WorkbenchPoseEstimation";
    }


    void WorkbenchPoseEstimation::onInitPointCloudProcessor()
    {
        offeringTopicFromProperty("tpc.pub.DebugObserverTopicName");
        offeringTopicFromProperty("tpc.pub.WorkbenchPoseTopicName");

        usingTopic("ServiceRequests");

        getProperty(executionMode, "ExecutionMode");
        getProperty(alwaysOn, "AlwaysOn");
        ARMARX_INFO << "Execution mode: " << executionModeNames.to_name(executionMode);
        if (executionMode == ExecutionMode::ONLINE && !alwaysOn)
        {
            ARMARX_INFO << "Running only when requested.";
        }

        estimator.readProperties(*this);

        if (executionMode == ExecutionMode::RECORD)
        {
            record.readProperties(*this, "record.");

            ARMARX_INFO << "Recodings directory: " << record.directory;
            if (record.dryRun)
            {
                ARMARX_IMPORTANT << "Dry run - not actually writing files!";
            }
        }
        else if (executionMode == ExecutionMode::REPLAY)
        {
            replay.readProperties(*this, "replay.");
        }
    }


    void WorkbenchPoseEstimation::onConnectPointCloudProcessor()
    {
        getTopicFromProperty(debugObserver, "tpc.pub.DebugObserverTopicName");
        getTopicFromProperty(workbenchPoseTopic, "tpc.pub.WorkbenchPoseTopicName");


        std::vector<std::string> providerNames = getPointCloudProviderNames();
        if (providerNames.size() == 1)
        {
            this->pointCloudProviderFrame = getPointCloudFrame(providerNames.front());
        }
        else
        {
            ARMARX_WARNING << "Point cloud transformation only supported for one point cloud provider.";
        }

        enableResultPointClouds<PointT>();

        ARMARX_INFO << "Load robot ...";
        ARMARX_INFO << VAROUT(RobotState::hasRobot(robotName));
        this->robot = RobotState::addRobot(robotName, VirtualRobot::RobotIO::RobotDescription::eStructure);
        ARMARX_VERBOSE << "Loaded robot.";

        estimator.setArviz(arviz);
        estimator.setRobot(robot);
        estimator.debugObserver = debugObserver;


        if (executionMode == ExecutionMode::RECORD)
        {
            record.robot = robot;
        }
        else if (executionMode == ExecutionMode::REPLAY)
        {
            replay.robot = robot;
            replay.runTask([this](pcl::PointCloud<PointT>::Ptr cloud)
            {
                std::scoped_lock lock(estimatorMutex);
                WorkbenchPose WorkbenchPose = estimator.estimate(cloud);
                publishWorkbenchPose(WorkbenchPose);
            });
        }
    }


    void WorkbenchPoseEstimation::onDisconnectPointCloudProcessor()
    {
    }

    void WorkbenchPoseEstimation::onExitPointCloudProcessor()
    {
    }


    void WorkbenchPoseEstimation::process()
    {
        // Fetch input point cloud.
        pcl::PointCloud<PointT>::Ptr inputCloud(new pcl::PointCloud<PointT>());
        if (waitForPointClouds())
        {
            getPointClouds(inputCloud);
        }
        else
        {
            ARMARX_VERBOSE << "Timeout or error while waiting for point cloud data.";
            return;
        }

        if (executionMode == ExecutionMode::REPLAY)
        {
            return;
        }
        if (executionMode == ExecutionMode::ONLINE && !alwaysOn && TimeUtil::GetTime() > requestedUntil)
        {
            return;
        }

        // Get robot state.
        RobotState::synchronizeLocalClone(robotName, Ice::Long(inputCloud->header.stamp));

        if (!pointCloudProviderFrame.empty())
        {
            const std::string rootName = robot->getRootNode()->getName();
            ARMARX_VERBOSE << "Transforming point cloud from '" << pointCloudProviderFrame
                           << "' to '" << rootName << "'";

            visionx::FramedPointCloud<PointT> framed(inputCloud, pointCloudProviderFrame);
            framed.changeFrame(rootName, robot);
        }

        pcl::PointCloud<PointT>::Ptr resultCloud;

        // Do processing.
        if (executionMode == ExecutionMode::ONLINE)
        {
            std::scoped_lock lock(estimatorMutex);
            WorkbenchPose workbenchPose = estimator.estimate(inputCloud);
            publishWorkbenchPose(workbenchPose);
            resultCloud = estimator.workbenchPointCloud;
        }
        else if (executionMode == ExecutionMode::RECORD)
        {
            record.process(*inputCloud);
            resultCloud.reset(new pcl::PointCloud<PointT>());
            estimator.visu.updateInputPointCloud(*inputCloud);
        }

        // Publish result point cloud.
        provideResultPointClouds(resultCloud);
        estimator.visu.commit();
    }


    void WorkbenchPoseEstimation::publishWorkbenchPose(const WorkbenchPose& workbenchPose)
    {
        ARMARX_VERBOSE << "Publishing workbench pose.";
        if (workbenchPoseTopic)
        {
            workbenchPoseTopic->reportWorkbenchPose(toIce(workbenchPose));
        }
    }


    data::WorkbenchPose WorkbenchPoseEstimation::getLatestWorkbenchPose(const Ice::Current&)
    {
        std::scoped_lock lock(estimatorMutex);
        return toIce(estimator.getLatestWorkbenchPose());
    }

    void WorkbenchPoseEstimation::requestService(const std::string& providerName, int relativeTimeoutMS, const Ice::Current&)
    {
        if (providerName == getName())
        {
            requestedUntil = TimeUtil::GetTime() + IceUtil::Time::milliSeconds(relativeTimeoutMS);
        }
    }

    void WorkbenchPoseEstimation::setServiceConfig(const std::string& providerName, const StringVariantBaseMap& config, const Ice::Current&)
    {
        if (providerName == getName())
        {
            // Not supported.
            (void) config;
        }
    }

}
