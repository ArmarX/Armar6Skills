#include "WorkbenchPoseEstimatorVisu.h"

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotConfig.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>


namespace armarx
{

    void WorkbenchPoseEstimatorVisu::defineProperties(armarx::PropertyDefinitionContainer& props, std::string prefix)
    {
        props.defineOptionalProperty<int>(
            prefix + "level", this->level,
            "Visualization verbosity level."
            "\n- 0: No visualization."
            "\n- 1: Workbench pose and plane"
            "\n- 2: 1 + Workbench point cloud"
            "\n- 3: 2 + Input point cloud"
        );

        props.defineOptionalProperty<bool>(
            prefix + "showRobot", this->showRobot,
            "Whether to visualize robot. Only active if visu level > 0.");

        props.defineOptionalPropertyVector<Eigen::Vector2f>(
            prefix + "planeSize", this->planeSize,
            "Size of visualized plane.");

        props.defineOptionalProperty<std::string>(
            prefix + "workbenchObjectFile", workbenchObjectFile,
            "Manipulation object file of the workbench.");

        props.defineOptionalProperty<float>(
            prefix + "workbenchObjectHeightOffset", workbenchObjectHeightOffset,
            "Height offset of the workbench object.");
    }


    void WorkbenchPoseEstimatorVisu::readProperties(PropertyUser& props, std::string prefix)
    {
        level = props.getProperty<int>(prefix + "level");
        planeSize = props.getProperty<Eigen::Vector2f>(prefix + "planeSize");
        showRobot = props.getProperty<bool>(prefix + "showRobot");

        workbenchObjectFile = props.getProperty<std::string>(prefix + "workbenchObjectFile");
        workbenchObjectFile = ArmarXDataPath::resolvePath(workbenchObjectFile);
        workbenchObjectHeightOffset = props.getProperty<float>(prefix + "workbenchObjectHeightOffset");
    }


    void WorkbenchPoseEstimatorVisu::init(viz::Client arviz)
    {
        this->arviz = arviz;
    }

    viz::Layer& WorkbenchPoseEstimatorVisu::addLayer(const std::string& name)
    {
        return layers.emplace_back(arviz.layer(name));
    }

    void WorkbenchPoseEstimatorVisu::commit()
    {
        if (level > 0)
        {
            arviz.commit(layers);
            layers.clear();
        }
    }

    void WorkbenchPoseEstimatorVisu::updateWorkbench(const WorkbenchPose& workbench, const pcl::PointCloud<PointT>& workbenchCloud)
    {
        if (level >= 1)
        {
            {
                viz::Layer& layerWorkbenchPlane = addLayer("Workbench Plane");
                layerWorkbenchPlane.add(viz::Polygon("Plane")
                                        .plane(workbench.position, workbench.orientation, planeSize)
                                        .color(workbench.valid ? simox::Color(224, 189, 128) : simox::Color::red())
                                        .lineColor(simox::Color(199, 168, 113)).lineWidth(2));
            }
            {
                viz::Layer& layerWorkbenchObject = addLayer("Workbench Object");
                layerWorkbenchObject.add(viz::Object("workbench")
                                         .pose(workbench.position + Eigen::Vector3f(0, 0, workbenchObjectHeightOffset),
                                               workbench.orientation)
                                         .file("", workbenchObjectFile));
            }
            {
                viz::Layer& layerWorkbenchPose = addLayer("Workbench Pose");
                layerWorkbenchPose.add(viz::Pose("Workbench").pose(workbench.position, workbench.orientation));
            }
        }
        if (level >= 2)
        {
            {
                viz::Layer& layerWorkbenchCloud = addLayer("Point Cloud Workbench");
                this->workbenchPointCloud.pointCloud(workbenchCloud);
                layerWorkbenchCloud.add(this->workbenchPointCloud);
            }
        }
    }

    void WorkbenchPoseEstimatorVisu::initRobot(const std::string& name, VirtualRobot::Robot& robot)
    {
        if (level > 0 && showRobot)
        {
            viz::Layer& layer = addLayer("Robot");

            this->robot = viz::Robot(name).file("", robot.getFilename());
            this->robot.joints(robot.getConfig()->getRobotNodeJointValueMap());
            layer.add(this->robot);
            layer.add(origin);
        }
    }

    void WorkbenchPoseEstimatorVisu::updateRobot(VirtualRobot::Robot& robot)
    {
        if (level > 0 && showRobot)
        {
            viz::Layer& layer = addLayer("Robot");

            this->robot.joints(robot.getConfig()->getRobotNodeJointValueMap());
            layer.add(this->robot);
            layer.add(origin);
        }
    }

    void WorkbenchPoseEstimatorVisu::updateInputPointCloud(
        const pcl::PointCloud<PointT>& cloud)
    {
        if (level >= 3)
        {
            viz::Layer& layer = addLayer("Input Point Cloud");

            this->inputPointCloud.pointCloud(cloud);
            this->inputPointCloud.transparency(0.25);

            layer.add(this->inputPointCloud);
        }
    }
}

