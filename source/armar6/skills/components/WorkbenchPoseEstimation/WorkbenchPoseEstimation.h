/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::WorkbenchPoseEstimation
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <pcl/point_types.h>

#include <SimoxUtility/meta/EnumNames.hpp>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/Component.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>

#include <armar6/skills/components/GuardPoseEstimation/Recording.h>
#include <armar6/skills/interface/WorkbenchPose.h>


#include "WorkbenchPoseEstimator.h"


namespace armarx
{
    /**
     * @class WorkbenchPoseEstimationPropertyDefinitions
     * @brief Property definitions of `WorkbenchPoseEstimation`.
     */
    class WorkbenchPoseEstimationPropertyDefinitions :
        public visionx::PointCloudProcessorPropertyDefinitions
    {
    public:
        WorkbenchPoseEstimationPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-WorkbenchPoseEstimation WorkbenchPoseEstimation
     * @ingroup armar6_skills-Components
     * A description of the component WorkbenchPoseEstimation.
     *
     * @class WorkbenchPoseEstimation
     * @ingroup Component-WorkbenchPoseEstimation
     * @brief Brief description of class WorkbenchPoseEstimation.
     *
     * Detailed description of class WorkbenchPoseEstimation.
     */
    class WorkbenchPoseEstimation :
        virtual public visionx::PointCloudProcessor,
        virtual public WorkbenchPoseEstimationInterface,
        virtual public armarx::RobotStateComponentPluginUser,
        virtual public armarx::ArVizComponentPluginUser
    {
    private:
        using RobotState = RobotStateComponentPluginUser;

    public:

        /// The used point type.
        using PointT = pcl::PointXYZRGBA;

        /// The execution mode.
        enum class ExecutionMode
        {
            ONLINE,  ///< Use live point cloud input and robot state.
            RECORD,  ///< Just record point cloud input and robot state.
            REPLAY,  ///< Use previously recorded point cloud input and robot state.
        };
        static const simox::meta::EnumNames<ExecutionMode> executionModeNames;

    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        // WorkbenchPoseInterface interface
        data::WorkbenchPose getLatestWorkbenchPose(const Ice::Current& = Ice::emptyCurrent) override;


        // RequestableServiceListenerInterface
        void requestService(const std::string& providerName, int relativeTimeoutMS,
                            const Ice::Current& = Ice::emptyCurrent) override;
        void setServiceConfig(const std::string& providerName, const StringVariantBaseMap& config,
                              const Ice::Current& = Ice::emptyCurrent) override;


    protected:

        /// @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
        void onInitPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
        void onConnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
        void onDisconnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
        void onExitPointCloudProcessor() override;


        /// @see visionx::PointCloudProcessor::process()
        void process() override;


        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        void publishWorkbenchPose(const WorkbenchPose& workbenchPose);


    private:

        // Ice

        armarx::DebugObserverInterfacePrx debugObserver;
        armarx::WorkbenchPoseTopicPrx workbenchPoseTopic;


        // Estimation

        ExecutionMode executionMode = ExecutionMode::ONLINE;

        bool alwaysOn = false;
        IceUtil::Time requestedUntil = IceUtil::Time::now();


        const std::string robotName = "robot";
        VirtualRobot::RobotPtr robot;

        std::string pointCloudProviderFrame = "";


        std::mutex estimatorMutex;
        WorkbenchPoseEstimator estimator;


        // Record / Replay

        Recording record;
        Replay replay;

    };
}
