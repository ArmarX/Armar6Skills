#pragma once

#include <Eigen/Geometry>

#include <armar6/skills/interface/WorkbenchPose.h>


namespace Eigen
{
    using Hyperplane3f = Hyperplane<float, 3>;
}

namespace armarx
{

    struct WorkbenchPose
    {
        bool valid = false;

        Eigen::Vector3f position = Eigen::Vector3f::Zero();
        Eigen::Quaternionf orientation = Eigen::Quaternionf::Identity();

        Eigen::Matrix4f robotPose = Eigen::Matrix4f::Identity();

        float rotationAroundZ() const;
        float rotationAroundZGlobal() const;
    };

    data::WorkbenchPose toIce(const WorkbenchPose& workbenchPose);
    WorkbenchPose fromIce(const data::WorkbenchPose& workbenchPose);

}
