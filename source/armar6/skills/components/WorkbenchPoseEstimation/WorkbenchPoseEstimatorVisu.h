#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include <Eigen/Core>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/application/properties/PropertyUser.h>

#include <RobotAPI/components/ArViz/Client/Client.h>

#include "WorkbenchPose.h"


namespace armarx
{

    class WorkbenchPoseEstimatorVisu
    {
    public:
        using PointT = pcl::PointXYZRGBA;

    public:

        WorkbenchPoseEstimatorVisu() = default;

        void defineProperties(armarx::PropertyDefinitionContainer& props, std::string prefix = "visu.");
        void readProperties(armarx::PropertyUser& props, std::string prefix = "visu.");


        void init(viz::Client arviz);
        viz::Layer& addLayer(const std::string& name);
        void commit();


        void initRobot(const std::string& name, VirtualRobot::Robot& robot);
        void updateRobot(VirtualRobot::Robot& robot);

        void updateInputPointCloud(const pcl::PointCloud<PointT>& cloud);

        void updateWorkbench(const WorkbenchPose& workbench, const pcl::PointCloud<PointT>& workbenchCloud);


    public:

        // Params / settings
        int level = 0;

        Eigen::Vector2f planeSize { 750, 1200 };

        bool showRobot = true;

        std::vector<viz::Layer> layers;

        std::string workbenchObjectFile = "armar6_skills/objects/CeBIT/cebit_workbench.xml";
        float workbenchObjectHeightOffset = -1000;


    private:

        viz::Client arviz;

        viz::Robot robot {""};

        viz::PointCloud inputPointCloud { "Input Point Cloud" };
        viz::PointCloud workbenchPointCloud { "Workbench Point Cloud" };

        viz::Pose origin = viz::Pose("Origin").pose(Eigen::Matrix4f::Identity());

    };
}
