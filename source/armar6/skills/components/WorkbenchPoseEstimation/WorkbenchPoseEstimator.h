#pragma once

#include <mutex>

#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/Robot.h>

#include <Eigen/Core>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PointIndices.h>

#include <SimoxUtility/meta/EnumNames.hpp>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/application/properties/PropertyUser.h>

#include <RobotAPI/components/ArViz/Client/Client.h>

#include <armar6/skills/components/GuardPoseEstimation/Preprocessing.h>

#include "WorkbenchPoseEstimatorVisu.h"


namespace armarx
{

    class WorkbenchPoseEstimator
    {
    public:

        using PointT = pcl::PointXYZRGBA;

        enum class YawEstimationMode { NONE, OOBB, RECTANGLE };
        static const simox::meta::EnumNames<YawEstimationMode> YawEstimationModeNames;


    public:

        WorkbenchPoseEstimator();

        void defineProperties(armarx::PropertyDefinitionContainer& properties, std::string prefix = "");
        void readProperties(armarx::PropertyUser& properties, std::string prefix = "");

        void setArviz(viz::Client arviz);
        void setRobot(VirtualRobot::RobotPtr robot);


        /**
         * @brief Estimate the workbench's position and orientation.
         * @param inputCloud The input point cloud.
         */
        WorkbenchPose estimate(pcl::PointCloud<PointT>::Ptr inputCloud);


        // Results
        WorkbenchPose getLatestWorkbenchPose() const;
        void setLatestWorkbenchPose(WorkbenchPose workbenchPose);

        pcl::PointCloud<PointT>::Ptr workbenchPointCloud { new pcl::PointCloud<PointT> };


    public:

        VirtualRobot::RobotPtr robot;

        struct Params
        {
            void defineProperties(armarx::PropertyDefinitionContainer& props, std::string prefix = "");
            void readProperties(armarx::PropertyUser& properties, std::string prefix = "");

            // Plane fitting.
            float planeFittingDistanceThreshold = 1.0;
            bool refinePlaneSVD = true;

            // Color check.
            Eigen::Vector3f colorWorkbenchHsv = { 0, 0.15f, 0.2f };
            Eigen::Vector3f colorHsvWeights = { 0, 0.5, 0.5 };
            float colorHsvMaxDistance = 1.0;

            // Clustering / segmentation.
            float clusterEuclideanTolerance = 2.0;
            Eigen::Vector3f clusterHsvWeights = { 0, 0.5, 0.5 };
            float clusterHsvTolerance = 1.0;

            // Yaw estimation.
            YawEstimationMode yawEstimationMode;

            Eigen::Vector2f yawRectRectSize = { 750, 1200 };
            size_t yawRectNumSteps = 180;

            // Validity checks.
            float validPlaneMaxAngle = 60;
            size_t validMinInliers = 2000;
        };
        Params params;

        guard_pose_estimator::Preprocessing preprocessing;

        WorkbenchPoseEstimatorVisu visu;


        armarx::DebugObserverInterfacePrx debugObserver;


    private:

        pcl::PointCloud<PointT>::Ptr clusterWorkbenchCloud(pcl::PointCloud<PointT>::Ptr workbenchCloud);
        std::vector<pcl::PointIndices> clusterWorkbenchCloudColor(pcl::PointCloud<PointT>::Ptr workbenchCloud);
        pcl::PointCloud<PointT>::Ptr applyCollisionModelFilter(pcl::PointCloud<PointT>::Ptr input);


        bool checkNumInliers(size_t numInliers) const;
        bool checkColor(const pcl::PointCloud<PointT>& cloud, const std::vector<int>& indices);
        bool checkAngle(Eigen::Hyperplane3f plane) const;


        static pcl::PointCloud<PointT>::Ptr getLargestClusterPointCloud(
            pcl::PointCloud<PointT>::Ptr cloud,
            const std::vector<pcl::PointIndices>& clusters);


    private:

        mutable std::mutex latestWorkbenchPoseMutex;
        WorkbenchPose latestWorkbenchPose;

    };
}
