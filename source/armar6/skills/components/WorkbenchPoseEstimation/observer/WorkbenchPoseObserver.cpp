/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::WorkbenchPoseObserver
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "WorkbenchPoseObserver.h"

#include <SimoxUtility/math/pose/pose.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>


namespace armarx
{
    WorkbenchPoseObserverPropertyDefinitions::WorkbenchPoseObserverPropertyDefinitions(std::string prefix) :
        armarx::ObserverPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("WorkbenchPoseTopicName", "WorkbenchPoseTopic",
                                            "Name of the workbench pose topic to subscribe.");

        defineOptionalProperty<bool>("IgnoreInvalid", true,
                                     "If true, invalid estimations are ignored.");

        defineOptionalPropertyVector<Eigen::Vector3f>("workbench.InitialPosition", Eigen::Vector3f::Zero(),
                "Initial workbench position.");
        Eigen::Quaternionf ori = Eigen::Quaternionf::Identity();
        defineOptionalPropertyVector<Eigen::Vector4f>("workbench.InitialOrientation", {ori.w(), ori.x(), ori.y(), ori.z()},
                "Initial workbench orientation as quaternion (w, x, y, z).");

        defineOptionalProperty<bool>(
            "visu.WorkbenchObject", false, "Visualize the workbench object.");
        defineOptionalProperty<bool>(
            "visu.WorkbenchPose", false, "Visualize the workbench object.");
        defineOptionalProperty<std::string>(
            "visu.Frame", armarx::GlobalFrame, "Visualize the workbench object.")
        .map(armarx::GlobalFrame, armarx::GlobalFrame)
        .map("robot", "robot");
        defineOptionalProperty<std::string>(
            "visu.WorkbenchObjectFile", "armar6_skills/objects/CeBIT/cebit_workbench.xml", "Manipulation object file of the workbench.");
        defineOptionalProperty<float>(
            "visu.WorkbenchObjectHeightOffset", -1000, "Height offset of the workbench object.");
    }

    armarx::PropertyDefinitionsPtr WorkbenchPoseObserver::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new WorkbenchPoseObserverPropertyDefinitions(getConfigIdentifier()));
    }

    std::string WorkbenchPoseObserver::getDefaultName() const
    {
        return "WorkbenchPoseObserver";
    }


    void WorkbenchPoseObserver::onInitObserver()
    {
        ignoreInvalid = getProperty<bool>("IgnoreInvalid");

        {
            latestWorkbenchPose.position = getProperty<Eigen::Vector3f>("workbench.InitialPosition");

            Eigen::Vector4f ori = getProperty<Eigen::Vector4f>("workbench.InitialOrientation");
            latestWorkbenchPose.orientation.w() = ori(0);
            latestWorkbenchPose.orientation.x() = ori(1);
            latestWorkbenchPose.orientation.y() = ori(2);
            latestWorkbenchPose.orientation.z() = ori(3);
            latestWorkbenchPose.orientation.normalize();
        }
        latestWorkbenchPoseIce = toIce(latestWorkbenchPose);

        visuWorkbenchObject = getProperty<bool>("visu.WorkbenchObject");
        visuWorkbenchPose = getProperty<bool>("visu.WorkbenchPose");
        visuFrame = getProperty<std::string>("visu.Frame");
        visuWorkbenchObjectFile = getProperty<std::string>("visu.WorkbenchObjectFile");
        visuWorkbenchObjectFile = ArmarXDataPath::resolvePath(visuWorkbenchObjectFile);
        visuWorkbenchObjectHeightOffset = getProperty<float>("visu.WorkbenchObjectHeightOffset");

        usingTopicFromProperty("WorkbenchPoseTopicName");
    }


    void WorkbenchPoseObserver::onConnectObserver()
    {
        offerChannel(channelName, "Workbench Pose");

        onWorkbenchPoseUpdate();
    }


    void WorkbenchPoseObserver::onDisconnectComponent()
    {
    }


    void WorkbenchPoseObserver::onExitComponent()
    {
    }


    void WorkbenchPoseObserver::reportWorkbenchPose(const data::WorkbenchPose& workbenchPose, const Ice::Current&)
    {
        if (ignoreInvalid && !workbenchPose.valid)
        {
            return;
        }
        std::scoped_lock lock(latestWorkbenchPoseMutex);
        this->latestWorkbenchPoseIce = workbenchPose;
        this->latestWorkbenchPose = fromIce(workbenchPose);

        onWorkbenchPoseUpdate();
    }


    data::WorkbenchPose WorkbenchPoseObserver::getLatestWorkbenchPose(const Ice::Current&)
    {
        std::scoped_lock lock(latestWorkbenchPoseMutex);
        return latestWorkbenchPoseIce;
    }


    void WorkbenchPoseObserver::onWorkbenchPoseUpdate()
    {
        offerOrUpdateDataField(channelName, "valid", Variant(latestWorkbenchPose.valid), "Whether workbench was detected in this frame.");
        offerOrUpdateDataField(channelName, "position", *latestWorkbenchPoseIce.position, "Workbench Position");
        offerOrUpdateDataField(channelName, "orientation", *latestWorkbenchPoseIce.orientation, "Workbench Orientation");
        offerOrUpdateDataField(channelName, "rotationAroundZ", Variant(latestWorkbenchPoseIce.rotationAroundZ), "Rotation around z axis.");
        offerOrUpdateDataField(channelName, "rotationAroundZGlobal", Variant(latestWorkbenchPoseIce.rotationAroundZGlobal), "Rotation around global z axis.");
        offerOrUpdateDataField(channelName, "robotPose", *latestWorkbenchPoseIce.robotPose, "Global Robot Pose.");

        if (visuWorkbenchObject || visuWorkbenchPose)
        {
            viz::Layer layer = arviz.layer("WorkbenchPose");

            Eigen::Matrix4f pose = simox::math::pose(latestWorkbenchPose.position, latestWorkbenchPose.orientation);
            if (visuFrame == armarx::GlobalFrame)
            {
                pose = latestWorkbenchPose.robotPose * pose;
            }

            if (visuWorkbenchObject)
            {
                layer.add(viz::Object("Workbench Object")
                          .position(simox::math::position(pose) + Eigen::Vector3f(0, 0, visuWorkbenchObjectHeightOffset))
                          .orientation(simox::math::orientation(pose))
                          .file("", visuWorkbenchObjectFile));
            }
            if (visuWorkbenchPose)
            {
                layer.add(viz::Pose("Workbench Pose").pose(pose));
            }

            arviz.commit(layer);
        }
    }
}
