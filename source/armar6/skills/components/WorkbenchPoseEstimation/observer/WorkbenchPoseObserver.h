/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::WorkbenchPoseObserver
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/observers/Observer.h>

#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <armar6/skills/interface/WorkbenchPose.h>
#include <armar6/skills/components/WorkbenchPoseEstimation/WorkbenchPose.h>


namespace armarx
{
    /**
     * @class WorkbenchPoseObserverPropertyDefinitions
     * @brief Property definitions of `WorkbenchPoseObserver`.
     */
    class WorkbenchPoseObserverPropertyDefinitions :
        public ObserverPropertyDefinitions
    {
    public:
        WorkbenchPoseObserverPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-WorkbenchPoseObserver WorkbenchPoseObserver
     * @ingroup armar6_skills-Components
     * A description of the component WorkbenchPoseObserver.
     *
     * @class WorkbenchPoseObserver
     * @ingroup Component-WorkbenchPoseObserver
     * @brief Brief description of class WorkbenchPoseObserver.
     *
     * Detailed description of class WorkbenchPoseObserver.
     */
    class WorkbenchPoseObserver :
        virtual public armarx::Observer,
        virtual public armarx::ArVizComponentPluginUser,
        virtual public armarx::WorkbenchPoseObserverInterface
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // WorkbenchPoseTopic interface
        void reportWorkbenchPose(const data::WorkbenchPose& workbenchPose, const Ice::Current&) override;

        // WorkbenchPoseInterface interface
        data::WorkbenchPose getLatestWorkbenchPose(const Ice::Current&) override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitObserver() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectObserver() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        /// To be called with locked `latestWorkbenchPoseMutex`.
        void onWorkbenchPoseUpdate();


    private:

        bool ignoreInvalid = true;

        std::mutex latestWorkbenchPoseMutex;
        data::WorkbenchPose latestWorkbenchPoseIce;
        armarx::WorkbenchPose latestWorkbenchPose;

        std::string channelName = "WorkbenchPose";

        bool visuWorkbenchObject = false;
        bool visuWorkbenchPose = false;
        std::string visuFrame = armarx::GlobalFrame;
        std::string visuWorkbenchObjectFile = "armar6_skills/objects/CeBIT/cebit_workbench.xml";
        float visuWorkbenchObjectHeightOffset = -1000;

    };
}
