#include "WorkbenchPoseEstimator.h"

#include <pcl/io/pcd_io.h>
#include <pcl/segmentation/conditional_euclidean_clustering.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/point_types_conversion.h>

#include <SimoxUtility/color/hsv.h>
#include <SimoxUtility/math/convert/rad_to_deg.h>
#include <SimoxUtility/math/pose/align_box_orientation.h>
#include <SimoxUtility/math/pose/pose.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <ArmarXCore/observers/variant/Variant.h>

#include <VisionX/libraries/PointCloudTools/OOBB/OOBB.h>
#include <VisionX/libraries/PointCloudTools/plane_fitting.h>
#include <VisionX/libraries/PointCloudTools/segments.h>
#include <VisionX/libraries/PointCloudTools/FitKnownRectangleRotationMaxPoints.h>
#include <VisionX/libraries/PointCloudTools/tools.h>
#include <VisionX/libraries/PointCloudTools/PerpendicularPlaneFitting.h>


namespace armarx
{

    const simox::meta::EnumNames<WorkbenchPoseEstimator::YawEstimationMode>
    WorkbenchPoseEstimator::YawEstimationModeNames =
    {
        { YawEstimationMode::NONE, "none" },
        { YawEstimationMode::OOBB, "oobb" },
        { YawEstimationMode::RECTANGLE, "rect" }
    };


    void WorkbenchPoseEstimator::Params::defineProperties(
        PropertyDefinitionContainer& props, std::string prefix)
    {
        props.defineOptionalProperty(prefix + "plane.DistanceThreshold", planeFittingDistanceThreshold,
                                     "Distance threshold for plane fitting.")
        .setMin(0);

        props.defineOptionalProperty(prefix + "plane.refineSVD", refinePlaneSVD,
                                     "Whether to refine the plane fitting via SVD after clustering.");


        props.defineOptionalPropertyVector(prefix + "color.WorkbenchHsv", colorWorkbenchHsv, "Color of the workbench in HSV ([0, 360] x [0, 1] x [0, 1]).");
        props.defineOptionalPropertyVector(prefix + "color.HsvWeights", colorHsvWeights, "Weights of the HSV color comparison (is normalized before use).");
        props.defineOptionalProperty(prefix + "color.HsvMaxDistance", colorHsvMaxDistance, "Maximal distance of the HSV color comparison.");


        props.defineOptionalProperty(prefix + "cluster.EuclideanTolerance", clusterEuclideanTolerance,
                                     "Cluster tolerance for euclidean clustering after plane fitting.")
        .setMin(0);

        props.defineOptionalPropertyVector(prefix + "cluster.HsvWeights", clusterHsvWeights,
                                           "Weights for H, S, V for HSV clustering after plane fitting.");

        props.defineOptionalProperty(prefix + "cluster.HsvTolerance", clusterHsvTolerance,
                                     "Cluster tolerance for HSV clustering after plane fitting.")
        .setMin(0);



        props.defineOptionalProperty(prefix + "yaw.estimationMode", yawEstimationMode,
                                     "Whether and how to estimate the yaw angle.")
        .map(YawEstimationModeNames.to_name(YawEstimationMode::NONE), YawEstimationMode::NONE)
        .map(YawEstimationModeNames.to_name(YawEstimationMode::OOBB), YawEstimationMode::OOBB)
        .map(YawEstimationModeNames.to_name(YawEstimationMode::RECTANGLE), YawEstimationMode::RECTANGLE);


        props.defineOptionalPropertyVector(prefix + "yaw.rect.RectSize", yawRectRectSize, "Size of the fitted rectangle.");
        props.defineOptionalProperty(prefix + "yaw.rect.NumSteps", yawRectNumSteps, "Number of steps in the rectangle fitting.");


        props.defineOptionalProperty(prefix + "valid.minInliers", validMinInliers,
                                     "Minimum number inliers necessary to accept the solution.")
        .setMin(0);

        props.defineOptionalProperty(prefix + "valid.planeMaxAngle", validPlaneMaxAngle,
                                     "Maximum angle (in degrees) of plane to horizontal plane to accept the solution.")
        .setMin(0);

    }

    void WorkbenchPoseEstimator::Params::readProperties(PropertyUser& props, std::string prefix)
    {
        props.getProperty(planeFittingDistanceThreshold, prefix + "plane.DistanceThreshold");

        props.getProperty(refinePlaneSVD, prefix + "plane.refineSVD");


        props.getProperty(colorWorkbenchHsv, prefix + "color.WorkbenchHsv");
        props.getProperty(colorHsvWeights, prefix + "color.HsvWeights");
        props.getProperty(colorHsvMaxDistance, prefix + "color.HsvMaxDistance");


        props.getProperty(clusterEuclideanTolerance, prefix + "cluster.EuclideanTolerance");

        props.getProperty(clusterHsvWeights, prefix + "cluster.HsvWeights");
        props.getProperty(clusterHsvTolerance, prefix + "cluster.HsvTolerance");


        props.getProperty(yawEstimationMode, prefix + "yaw.estimationMode");


        props.getProperty(yawRectRectSize, prefix + "yaw.rect.RectSize");
        props.getProperty(yawRectNumSteps, prefix + "yaw.rect.NumSteps");


        props.getProperty(validMinInliers, prefix + "valid.minInliers");

        props.getProperty(validPlaneMaxAngle, prefix + "valid.planeMaxAngle");
    }


    WorkbenchPoseEstimator::WorkbenchPoseEstimator()
    {
    }


    void WorkbenchPoseEstimator::defineProperties(armarx::PropertyDefinitionContainer& properties, std::string prefix)
    {
        params.defineProperties(properties, prefix + "estimate.");
        preprocessing.defineProperties(properties, prefix + "estimate.pre.");
        visu.defineProperties(properties, prefix + "visu.");
    }

    void WorkbenchPoseEstimator::readProperties(PropertyUser& properties, std::string prefix)
    {
        params.readProperties(properties, prefix + "estimate.");
        preprocessing.readProperties(properties, prefix + "estimate.pre.");
        visu.readProperties(properties, prefix + "visu.");
    }

    void WorkbenchPoseEstimator::setArviz(viz::Client arviz)
    {
        visu.init(arviz);
    }

    void WorkbenchPoseEstimator::setRobot(VirtualRobot::RobotPtr robot)
    {
        this->robot = robot;
        visu.initRobot(robot->getName(), *robot);
        visu.commit();
    }

    WorkbenchPose WorkbenchPoseEstimator::estimate(pcl::PointCloud<PointT>::Ptr inputCloud)
    {
        auto onFailure = [this]()
        {
            visu.commit();
            WorkbenchPose result = latestWorkbenchPose;
            result.valid = false;
            return result;
        };

        visu.updateInputPointCloud(*inputCloud);
        visu.updateRobot(*robot);

        // Preprocessing.
        inputCloud = preprocessing.preprocess(inputCloud, robot);
        {
            visu.addLayer("PointCloud Preprocessed").add(
                viz::PointCloud("pointcloud").pointCloud(*inputCloud).transparency(0.75));
            visu.commit();
        }

        viz::Layer& layerPlanes = visu.addLayer("Planes");

        if (debugObserver)
        {
            debugObserver->setDebugDatafield("WorkbenchPoseEstimator", "MinInliers", new Variant(int(params.validMinInliers)));
        }


        ARMARX_DEBUG << "Finding suitable plane ...";
        Eigen::Hyperplane3f plane;
        pcl::PointIndicesPtr planeInliers;
        pcl::PointCloud<PointT>::Ptr workbenchCloud;

        bool success = false;
        while (!success)
        {
            if (inputCloud->size() < params.validMinInliers)
            {
                ARMARX_VERBOSE << "Point cloud too small." << "\n(" << inputCloud->size() << " < " << params.validMinInliers << ")";
                return onFailure();
            }

            visionx::tools::PerpendicularPlaneFitting planeFitting;
            planeFitting.distanceThreshold = double(params.planeFittingDistanceThreshold);

            std::optional<visionx::tools::PlaneFittingResult> result = planeFitting.fit(inputCloud);
            if (!result)
            {
                ARMARX_VERBOSE << "Plane fitting did not yield any inliers.";
                return onFailure();
            }

            plane = visionx::alignPlaneNormal(result->plane, Eigen::Vector3f::UnitZ());
            planeInliers = result->inliers;
            // planeInliers = visionx::getPlaneInliers(*inputCloud, plane, 2 * params.planeFittingDistanceThreshold);
            ARMARX_VERBOSE << "Found plane with " << planeInliers->indices.size() << " inliers.";

            if (debugObserver)
            {
                debugObserver->setDebugDatafield("WorkbenchPoseEstimator", "NumInliers InitialPlane", new Variant(int(planeInliers->indices.size())));
            }

            if (!checkNumInliers(planeInliers->indices.size()))
            {
                ARMARX_VERBOSE << "Discarding plane with too few inliers. \n(" << planeInliers->indices.size() << " < " << params.validMinInliers << ")";
                // Remove points and retry.
                inputCloud = visionx::getPointCloudWithoutIndices(*inputCloud, planeInliers->indices);
                continue;
            }

            pcl::PointCloud<PointT>::Ptr planeCloud = visionx::getPointCloudWithIndices(*inputCloud, *planeInliers);
            {
                Eigen::Vector3f center = visionx::tools::getAABB(*inputCloud, planeInliers).center();

                if (false)
                {
                    layerPlanes.add(viz::Polygon("ransac").plane(plane, center, {1000, 1000}).color(simox::Color::yellow(255, 128)));
                    visu.addLayer("Point Cloud Plane Inliers").add(
                        viz::PointCloud("Initial Plane Inliers").pointCloud(*planeCloud).transparency(0.5));
                }
            }

            // Cluster.
            if (params.clusterEuclideanTolerance > 0)
            {
                ARMARX_DEBUG << "Clustering plane cloud ...";
                std::vector<pcl::PointIndices> clusters = clusterWorkbenchCloudColor(planeCloud);
                // Clusters are sorted from largest to smallest.

                for (const auto& cluster : clusters)
                {
                    if (!checkNumInliers(cluster.indices.size()))
                    {
                        // All remaining clusters are too small.
                        ARMARX_VERBOSE << "Clusters are too small. \n(" << cluster.indices.size() << " < " << params.validMinInliers << ")";
                        break;
                    }
                    if (checkColor(*planeCloud, cluster.indices))
                    {
                        // Found large enough cluster with correct color.
                        success = true;
                        workbenchCloud = visionx::getPointCloudWithIndices(*planeCloud, cluster);

                        break;
                    }
                }

                if (debugObserver)
                {
                    debugObserver->setDebugDatafield("WorkbenchPoseEstimator", "NumPoints AfterClustering", new Variant(int(planeCloud->size())));
                }
            }
            else
            {
                if (checkColor(*inputCloud, planeInliers->indices))
                {
                    // Found large enough cluster with correct color.
                    success = true;
                    workbenchCloud = planeCloud;

                    break;
                }
            }

            if (!success)
            {
                // Remove points and retry.
                inputCloud = visionx::getPointCloudWithoutIndices(*inputCloud, planeInliers->indices);
            }
        }
        ARMARX_CHECK(success);
        ARMARX_CHECK(workbenchCloud);


        Eigen::Vector3f centerAABB = visionx::tools::getAABB(*workbenchCloud).center();
        Eigen::Vector3f centerMedian = visionx::pointCloudMedian(*workbenchCloud);

        viz::Layer& layerCenters = visu.addLayer("Centers");
        {
            layerCenters.add(viz::Sphere("aabb").position(centerAABB).radius(25)
                             .color(simox::Color::red()));
            layerCenters.add(viz::Sphere("median").position(centerMedian).radius(25)
                             .color(simox::Color::orange()));
        }

        // Refine plane fitting.
        if (params.refinePlaneSVD)
        {
            ARMARX_VERBOSE << "Refining plane via SVD (" << workbenchCloud->size() << " points)";

            const Eigen::Vector3f& center = centerMedian;
            Eigen::Hyperplane3f refined = visionx::fitPlaneSVD(*workbenchCloud, center);

            plane = visionx::alignPlaneNormal(refined, Eigen::Vector3f::UnitZ());
            planeInliers = visionx::getPlaneInliers(*workbenchCloud, refined, params.planeFittingDistanceThreshold);

            centerAABB = visionx::tools::getAABB(*workbenchCloud, *planeInliers).center();
            centerMedian = visionx::pointCloudMedian(*workbenchCloud, *planeInliers);

            {
                layerPlanes.add(viz::Polygon("svd").plane(plane, center, {1000, 1000})
                                .color(simox::Color::magenta(255, 128)));
            }
            if (debugObserver)
            {
                debugObserver->setDebugDatafield("WorkbenchPoseEstimator", "NumInliers AfterPlaneRefinement", new Variant(int(planeInliers->indices.size())));
            }
        }


        // Estimate center and orientation.
        bool valid = true;
        Eigen::Vector3f workbenchPos = Eigen::Vector3f::Zero();
        Eigen::Quaternionf workbenchOri = Eigen::Quaternionf::Identity();

        switch (params.yawEstimationMode)
        {
            case YawEstimationMode::NONE:
            {
                workbenchPos = plane.projection(visionx::tools::getAABB(*workbenchCloud).center());
                workbenchOri = Eigen::Quaternionf::FromTwoVectors(Eigen::Vector3f::UnitZ(), plane.normal());
            }
            break;

            case YawEstimationMode::OOBB:
            {
                // Estimate workbench yaw by OOBB.
                ARMARX_DEBUG << "Calculate 2D OOBB ...";
                const simox::OrientedBox<float> ob = calculate2dOOBB(*workbenchCloud, plane.normal().eval());

                int iy = 0, iz = 0;
                ob.dimensions().minCoeff(&iz);
                ob.dimensions().maxCoeff(&iy);
                Eigen::Vector3f y = ob.axis(iy);
                Eigen::Vector3f z = ob.axis(iz);
                // Assure z points upwards.
                if (z.dot(Eigen::Vector3f::UnitZ()) < 0)
                {
                    z = -z;
                }

                Eigen::Matrix3f orientation = Eigen::Matrix3f::Identity();
                orientation.col(0) = y.cross(z);
                orientation.col(1) = y;
                orientation.col(2) = z;

                workbenchPos = ob.center();
                workbenchOri = Eigen::Quaternionf(orientation);

                if (false)
                {
                    viz::Layer& layer = visu.addLayer("Workbench OOBB");
                    layer.add(viz::Box("box").pose(ob.center(), ob.rotation()).size(ob.dimensions()).color(viz::Color::azure(255, 64)));
                    layer.add(viz::Pose("pose").pose(ob.center(), ob.rotation()));
                    layer.add(viz::Arrow("x").fromTo(ob.center(), ob.center() + 100 * y.cross(z)).width(2).color(simox::Color::red()));
                    layer.add(viz::Arrow("y").fromTo(ob.center(), ob.center() + 100 * y).width(2).color(simox::Color::green()));
                    layer.add(viz::Arrow("z").fromTo(ob.center(), ob.center() + 100 * z).width(2).color(simox::Color::blue()));
                }
            }
            break;

            case YawEstimationMode::RECTANGLE:
            {
                visionx::FitKnownRectangleRotationMaxPoints fitter;
                fitter.setRectangleSize(params.yawRectRectSize);
                fitter.setNumSteps(params.yawRectNumSteps);

                ARMARX_VERBOSE << "Fitting fixed size rectangle of size " << params.yawRectRectSize.transpose();
                Eigen::Matrix4f pose = fitter.fit(*workbenchCloud, plane.normal(), centerMedian);
                planeInliers = fitter.getInliers();

                workbenchPos = centerMedian;
                workbenchOri = Eigen::Quaternionf(simox::math::orientation(pose));
                workbenchCloud = visionx::getPointCloudWithIndices(*workbenchCloud, *planeInliers);

                if (debugObserver)
                {
                    debugObserver->setDebugDatafield("WorkbenchPoseEstimator", "NumInliers in Rectangle",
                                                     new Variant(int(planeInliers->indices.size())));
                }
            }
            break;
        }

        {
            viz::Layer& layerRect = visu.addLayer("Workbench Rectangle");
            {
                layerRect.add(viz::Polygon("Rectangle").plane(workbenchPos, workbenchOri, params.yawRectRectSize)
                              .lineColor(simox::Color::green()).lineWidth(2).color(simox::Color::gray(64, 128)));
            }
            layerCenters.add(viz::Sphere("workbench").position(workbenchPos).radius(25).color(simox::Color::azure()));
        }

        // Check validity.
        valid = valid && checkNumInliers(workbenchCloud->size());
        valid = valid && checkAngle(plane);
        ARMARX_DEBUG << "Solution valid? " << valid;


        // Build result.
        WorkbenchPose workbenchPose;
        if (valid)
        {
            workbenchPose.position = workbenchPos;
            workbenchPose.orientation = workbenchOri;
            workbenchPose.robotPose = robot->getGlobalPose();
        }
        else
        {
            workbenchPose = getLatestWorkbenchPose();
        }
        workbenchPose.valid = valid;


        // Store results.
        setLatestWorkbenchPose(workbenchPose);
        this->workbenchPointCloud = workbenchCloud;

        visu.updateWorkbench(workbenchPose, *workbenchCloud);
        visu.commit();

        ARMARX_DEBUG << "Done.";
        return workbenchPose;
    }

    WorkbenchPose WorkbenchPoseEstimator::getLatestWorkbenchPose() const
    {
        std::unique_lock lock(latestWorkbenchPoseMutex);
        return latestWorkbenchPose;
    }

    void WorkbenchPoseEstimator::setLatestWorkbenchPose(WorkbenchPose workbenchPose)
    {
        std::unique_lock lock(latestWorkbenchPoseMutex);
        this->latestWorkbenchPose = workbenchPose;
    }


    auto WorkbenchPoseEstimator::getLargestClusterPointCloud(
        pcl::PointCloud<PointT>::Ptr cloud,
        const std::vector<pcl::PointIndices>& clusters) -> pcl::PointCloud<PointT>::Ptr
    {
        if (!clusters.empty())
        {
            const auto& indices = *std::max_element(clusters.begin(), clusters.end(), pcl::comparePointClusters);

            pcl::PointCloud<PointT>::Ptr cluster(new pcl::PointCloud<PointT>);
            pcl::copyPointCloud(*cloud, indices.indices, *cluster);

            return cluster;
        }
        return cloud;
    }


    auto WorkbenchPoseEstimator::clusterWorkbenchCloud(pcl::PointCloud<PointT>::Ptr workbenchCloud) -> pcl::PointCloud<PointT>::Ptr
    {
        pcl::search::KdTree<PointT>::Ptr tree(new pcl::search::KdTree<PointT>);
        tree->setInputCloud(workbenchCloud);

        pcl::EuclideanClusterExtraction<PointT> ec;
        ec.setClusterTolerance(double(params.clusterEuclideanTolerance));
        ec.setSearchMethod(tree);

        ec.setInputCloud(workbenchCloud);

        ARMARX_DEBUG << "Clustering ...";
        std::vector<pcl::PointIndices> clusterIndices;
        ec.extract(clusterIndices);
        ARMARX_DEBUG << "Found " << clusterIndices.size() << " clusters";

        return getLargestClusterPointCloud(workbenchCloud, clusterIndices);
    }


    std::vector<pcl::PointIndices> WorkbenchPoseEstimator::clusterWorkbenchCloudColor(pcl::PointCloud<PointT>::Ptr workbenchCloud)
    {
        using PointHSV = pcl::PointXYZHSV;

        // Hue is in degrees, i.e. in [0.0, 360.0]
        pcl::PointCloud<PointHSV>::Ptr hsvCloud(new pcl::PointCloud<PointHSV>);
        pcl::PointCloudXYZRGBAtoXYZHSV(*workbenchCloud, *hsvCloud);

        pcl::ConditionalEuclideanClustering<PointHSV> cec(false);
        cec.setClusterTolerance(params.clusterEuclideanTolerance);

        // Explicit type necessary to avoid ambiguous call.
        std::function<bool(const PointHSV&, const PointHSV&, float)> condition =
            [this](const PointHSV & p1, const PointHSV & p2, float squaredDistance)
        {
            (void) squaredDistance;
            return visionx::hsvDistance(p1, p2, params.clusterHsvWeights) < params.clusterHsvTolerance;
        };
        cec.setConditionFunction(condition);

        cec.setInputCloud(hsvCloud);

        ARMARX_DEBUG << "Clustering by color ...";
        std::vector<pcl::PointIndices> clusterIndices;
        cec.segment(clusterIndices);
        ARMARX_DEBUG << "Found " << clusterIndices.size() << " clusters.";
        visionx::sortPointClustersDescending(clusterIndices);

        {
            std::stringstream ss;
            ss << "Clusters with at least " << params.validMinInliers / 10 << " points:";
            viz::Layer& layer = visu.addLayer("Point Cloud Plane Segmented");
            {
                for (size_t i = 0; i < clusterIndices.size(); ++i)
                {
                    const pcl::PointIndices& indices = clusterIndices[i];

                    if (indices.indices.size() >= (params.validMinInliers / 10))
                    {
                        Eigen::Vector3f hsv = visionx::hsvMean(*hsvCloud, indices.indices);
                        simox::Color color(simox::color::hsv_to_rgb(hsv));

                        ss << "\n\t" << i << " (" << indices.indices.size() << "): HSV: " << hsv.transpose() << " | RGB: " << color;

#if 0
                        layer.add(viz::PointCloud(std::to_string(i))
                                  .pointCloud(*workbenchCloud, indices.indices, color)
                                  .pointSizeInPixels(2));
#else
                        pcl::PointCloud<pcl::PointXYZ> unicolored;
                        pcl::copyPointCloud(*workbenchCloud, indices.indices, unicolored);
                        viz::PointCloud pc(std::to_string(i));
                        pc.pointSizeInPixels(3);
                        for (const auto& p : unicolored)
                        {
                            pc.addPoint(p.x, p.y, p.z, color.r, color.g, color.b, color.a);
                        }
                        layer.add(pc);
#endif

                        layer.add(viz::Sphere(std::to_string(i) + " center")
                                  .position(visionx::tools::getAABB(*workbenchCloud, indices).center())
                                  .radius(30).color(color));
                    }
                }
            }
            ARMARX_DEBUG << ss.str();
        }

        //return getLargestClusterPointCloud(workbenchCloud, clusterIndices);
        return clusterIndices;
    }


    bool WorkbenchPoseEstimator::checkNumInliers(size_t numInliers) const
    {
        ARMARX_DEBUG << "Checking inliers: " << numInliers << " >= " << params.validMinInliers <<  " ?";
        return numInliers >= params.validMinInliers;
    }

    bool WorkbenchPoseEstimator::checkColor(const pcl::PointCloud<PointT>& cloud, const std::vector<int>& indices)
    {
        Eigen::Vector3f hsvMean = visionx::hsvMean(cloud, indices);
        float hsvDist = visionx::hsvDistance(hsvMean, params.colorWorkbenchHsv, params.colorHsvWeights);

        if (debugObserver)
        {
            debugObserver->setDebugDatafield("WorkbenchPoseEstimator", "HSV Distance", new Variant(hsvDist));
            debugObserver->setDebugDatafield("WorkbenchPoseEstimator", "HSV Max Distance", new Variant(params.colorHsvMaxDistance));
        }

        ARMARX_DEBUG << "Checking color: |" << hsvMean.transpose() << " - " << params.colorWorkbenchHsv.transpose() << "| == "
                     << hsvDist << " <= " << params.colorHsvMaxDistance << " ?";
        return hsvDist <= params.colorHsvMaxDistance;
    }


    bool WorkbenchPoseEstimator::checkAngle(Eigen::Hyperplane3f plane) const
    {
        float angleNormalToZ = simox::math::rad_to_deg(std::acos(plane.normal().dot(Eigen::Vector3f::UnitZ())));
        // e.g. 100 -> 80
        if (angleNormalToZ > 90)
        {
            angleNormalToZ = 90 - (angleNormalToZ - 90);
        }

        ARMARX_DEBUG << "Checking angle: " << std::abs(angleNormalToZ) << " <= " << params.validPlaneMaxAngle << "?";
        return std::abs(angleNormalToZ) <= params.validPlaneMaxAngle;
    }

}
