#include "WorkbenchPose.h"

#include <SimoxUtility/math/pose/transform.h>

#include <RobotAPI/libraries/core/Pose.h>


namespace
{
    float rotationAroundZ(const Eigen::Vector3f normal)
    {
        Eigen::Vector2f horizontalNormal = normal.head<2>().normalized();
        return std::atan2(horizontalNormal.y(), horizontalNormal.x());
    }
}

float armarx::WorkbenchPose::rotationAroundZ() const
{
    return ::rotationAroundZ(orientation * Eigen::Vector3f::UnitX());
}


float armarx::WorkbenchPose::rotationAroundZGlobal() const
{
    return ::rotationAroundZ(simox::math::transform_direction(robotPose, orientation * Eigen::Vector3f::UnitX()));
}



armarx::data::WorkbenchPose armarx::toIce(const armarx::WorkbenchPose& WorkbenchPose)
{
    data::WorkbenchPose ice;
    ice.valid = WorkbenchPose.valid;
    ice.position = new armarx::Vector3(WorkbenchPose.position);
    ice.orientation = new armarx::Quaternion(WorkbenchPose.orientation);
    ice.rotationAroundZ = WorkbenchPose.rotationAroundZ();
    ice.rotationAroundZGlobal = WorkbenchPose.rotationAroundZGlobal();
    ice.robotPose = new armarx::Pose(WorkbenchPose.robotPose);
    return ice;
}


armarx::WorkbenchPose armarx::fromIce(const data::WorkbenchPose& ice)
{
    armarx::WorkbenchPose workbenchPose;
    workbenchPose.valid = ice.valid;
    workbenchPose.position = armarx::Vector3Ptr::dynamicCast(ice.position)->toEigen();
    workbenchPose.orientation = armarx::QuaternionPtr::dynamicCast(ice.orientation)->toEigen();
    workbenchPose.robotPose = armarx::PosePtr::dynamicCast(ice.robotPose)->toEigen();
    return workbenchPose;
}


