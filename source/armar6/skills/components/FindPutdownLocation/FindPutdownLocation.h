/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::FindPutdownLocation
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_armar6_skills_FindPutdownLocation_H
#define _ARMARX_COMPONENT_armar6_skills_FindPutdownLocation_H


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <RobotAPI/interface/core/RobotState.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>



#include <VirtualRobot/Robot.h>
#include <VirtualRobot/VirtualRobot.h>

#include <pcl/point_types.h>

#include <armar6/skills/interface/FindPutdownLocation.h>
#include <mutex>


namespace armarx
{


    typedef pcl::PointXYZRGBL PointL;

    /**
     * @class FindPutdownLocationPropertyDefinitions
     * @brief
     */
    class FindPutdownLocationPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        FindPutdownLocationPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component.");

            defineOptionalProperty<std::string>("ProviderName", "TabletopSegmentationResult", "Name of the point cloud provider.");
            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the DebugDrawerTopic");
            defineOptionalProperty<bool>("ShowVisualization", true, "Visualize possible putdown locations");
            defineOptionalPropertyVector<Eigen::Vector3f>("SurfaceCenter", Eigen::Vector3f(4200.0f, 4500.0f, 1050.0f), "Center of the surface to be considered", ' ', PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<float>("StepSizeX", 180.f, "Size of the grid in X coordinates (left, right axis of robot)", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<float>("GridSize", 900.f, "Size of the grid on X axis (left, right axis of robot) in both directions", PropertyDefinitionBase::eModifiable);
        }
    };

    /**
     * @defgroup Component-FindPutdownLocation FindPutdownLocation
     * @ingroup ActiveVision-Components
     * A description of the component FindPutdownLocation.
     *
     * @class FindPutdownLocation
     * @ingroup Component-FindPutdownLocation
     * @brief Brief description of class FindPutdownLocation.
     *
     * Detailed description of class FindPutdownLocation.
     */

    class FindPutdownLocation :
        virtual public visionx::PointCloudProcessor,
        virtual public FindPutdownLocationInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const override
        {
            return "FindPutdownLocation";
        }

        std::vector<Vector3BasePtr> getPutdownLocations(const Ice::Current& c = ::Ice::Current());

    protected:
        /**
         * @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
         */
        virtual void onInitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
         */
        virtual void onConnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
         */
        virtual void onDisconnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
         */
        virtual void onExitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::process()
         */
        void process() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        DebugDrawerInterfacePrx debugDrawer;
        DebugObserverInterfacePrx debugObserver;

        armarx::RobotStateComponentInterfacePrx robotStateComponent;
        VirtualRobot::RobotPtr robot;

        std::mutex mutex;
        std::vector<Vector3BasePtr> putdownLocations;
        Ice::Long timestamp;
        Eigen::Vector3f tableCenter;

    };
}

#endif
