/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::FindPutdownLocation
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "FindPutdownLocation.h"


#include <pcl/filters/filter.h>
#include <Eigen/Core>
#include <RobotAPI/libraries/core/Pose.h>

#include <pcl/filters/crop_box.h>

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <IceUtil/Time.h>

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

using namespace armarx;



armarx::PropertyDefinitionsPtr FindPutdownLocation::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new FindPutdownLocationPropertyDefinitions(
            getConfigIdentifier()));
}



void armarx::FindPutdownLocation::onInitPointCloudProcessor()
{
    offeringTopic(getProperty<std::string>("DebugObserverName").getValue());
    offeringTopic(getProperty<std::string>("DebugDrawerTopicName").getValue());

    usingPointCloudProvider(getProperty<std::string>("ProviderName").getValue());

    usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
}

void armarx::FindPutdownLocation::onConnectPointCloudProcessor()
{
    debugObserver = getTopic<DebugObserverInterfacePrx>(getProperty<std::string>("DebugObserverName").getValue());
    debugDrawer = getTopic<DebugDrawerInterfacePrx>(getProperty<std::string>("DebugDrawerTopicName").getValue());

    robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
    robot = RemoteRobot::createLocalCloneFromFile(robotStateComponent, VirtualRobot::RobotIO::RobotDescription::eFull);

    enableResultPointClouds<PointL>();
    if (getProperty<bool>("ShowVisualization").getValue())
    {
        debugDrawer->clearLayer(getName());
    }
}


void armarx::FindPutdownLocation::onDisconnectPointCloudProcessor()
{
    if (getProperty<bool>("ShowVisualization").getValue())
    {
        debugDrawer->clearLayer(getName());
    }
}

void armarx::FindPutdownLocation::onExitPointCloudProcessor()
{
}

std::vector<Vector3BasePtr> armarx::FindPutdownLocation::getPutdownLocations(const Ice::Current&)
{
    std::lock_guard<std::mutex> lock(mutex);

    if ((TimeUtil::GetTime() - IceUtil::Time::microSeconds(timestamp)) > IceUtil::Time::seconds(2.0))
    {
        ARMARX_WARNING << "Computed putdown locations are too old. Discarding results.";
        putdownLocations.clear();
    }

    if (getProperty<bool>("ShowVisualization").getValue() && !putdownLocations.empty())




    {
        float step = 125.0;
        Vector3Ptr dim = new Vector3(2.0f * step, 2.0f * step, 2.0f * step);
        Eigen::Vector3f p = Vector3Ptr::dynamicCast(putdownLocations.front())->toEigen();
        PosePtr pose = new Pose(Eigen::Matrix3f::Identity(), p);
        debugDrawer->setBoxVisu(getName(), "box", pose, dim, {0.0, 0.0, 1.0, 0.5});
    }



    return putdownLocations;
}

void armarx::FindPutdownLocation::process()
{
    pcl::PointCloud<PointL>::Ptr inputCloud(new pcl::PointCloud<PointL>());
    pcl::PointCloud<PointL>::Ptr result(new pcl::PointCloud<PointL>());

    if (!waitForPointClouds())
    {
        ARMARX_INFO << "Timeout or error while waiting for point cloud data";
        return;
    }
    else
    {
        getPointClouds(inputCloud);
    }

    //    RemoteRobot::synchronizeLocalCloneToTimestamp(robot, robotStateComponent, static_cast<Ice::Long>(inputCloud->header.stamp));

    float step = 100.0;
    float stepX = getProperty<float>("StepSizeX").getValue();



    Eigen::Vector3f tableCenter = getProperty<Eigen::Vector3f>("SurfaceCenter");
    pcl::CropBox<PointL> cropBox;
    cropBox.setMin(Eigen::Vector4f(-stepX, -step, -step, 1.0));
    cropBox.setMax(Eigen::Vector4f(stepX, step, step, 1.0));
    cropBox.setInputCloud(inputCloud);
    float gridSize = getProperty<float>("GridSize").getValue();
    std::vector<Vector3BasePtr> putdownLocations;
    for (float i = 0; i < gridSize; i += stepX * 2.0f)
    {
        for (int j :
             {
                 -1, 1
                 })
        {
            Eigen::Vector3f p = tableCenter + j * Eigen::Vector3f(i, 0, 0);

            cropBox.setTranslation(p);
            cropBox.filter(*result);

            Vector3Ptr dim = new Vector3(2.0f * stepX, 2.0f * step, 2.0f * step);
            PosePtr pose = new Pose(Eigen::Matrix3f::Identity(), p);

            if (!result->points.size())
            {
                if (getProperty<bool>("ShowVisualization").getValue())
                {
                    debugDrawer->setBoxVisu(getName(), "box" + std::to_string(static_cast<int>(i)) +  std::to_string(j), pose, dim, {0.0, 0, 0.0, 0.5});
                }
            }
            else
            {

                int planePoints = 0;
                int objectPoints = 0;
                for (const PointL& p : result->points)
                {
                    if (p.label == 1)
                    {
                        planePoints++;
                    }
                    else
                    {
                        objectPoints++;
                        break;
                    }
                }

                if (planePoints > 1500 && objectPoints == 0)
                {
                    putdownLocations.push_back(new Vector3(p));
                    if (getProperty<bool>("ShowVisualization").getValue())
                    {
                        debugDrawer->setBoxVisu(getName(), "box" + std::to_string(static_cast<int>(i)) +  std::to_string(j), pose, dim, {0, 1.0, 0.0, 0.5});
                    }

                }
                else if (objectPoints == 0)
                {
                    if (getProperty<bool>("ShowVisualization").getValue())
                    {
                        debugDrawer->setBoxVisu(getName(), "box" + std::to_string(static_cast<int>(i)) +  std::to_string(j), pose, dim, {0.0, 0, 0.0, 0.5});
                    }
                }
                else
                {
                    if (getProperty<bool>("ShowVisualization").getValue())
                    {
                        debugDrawer->setBoxVisu(getName(), "box" + std::to_string(static_cast<int>(i)) +  std::to_string(j), pose, dim, {1.0, 0, 0.0, 0.5});
                    }
                }
            }
        }
    }

    std::lock_guard<std::mutex> lock(mutex);
    {
        this->timestamp = static_cast<Ice::Long>(inputCloud->header.stamp);
        this->putdownLocations.swap(putdownLocations);
    }

    provideResultPointClouds(inputCloud);


}

ARMARX_DECOUPLED_REGISTER_COMPONENT(armarx::FindPutdownLocation);
