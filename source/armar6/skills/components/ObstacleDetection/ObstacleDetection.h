/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::ObstacleDetection
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <pcl/point_types.h>

#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>

#include <pcl/features/moment_of_inertia_estimation.h>

namespace armarx
{

    /**
     * @class ObstacleDetectionPropertyDefinitions
     * @brief
     */
    class ObstacleDetectionPropertyDefinitions :
        public visionx::PointCloudProcessorPropertyDefinitions
    {
    public:
        ObstacleDetectionPropertyDefinitions(std::string prefix);
    };


    /**
     * @defgroup Component-ObstacleDetection ObstacleDetection
     * @ingroup armar6_skills-Components
     * A description of the component ObstacleDetection.
     *
     * @class ObstacleDetection
     * @ingroup Component-ObstacleDetection
     * @brief Brief description of class ObstacleDetection.
     *
     * Detailed description of class ObstacleDetection.
     */
    class ObstacleDetection :
        virtual public visionx::PointCloudProcessor
    {
        using PointT = pcl::PointXYZRGBL;

    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        virtual std::string getDefaultName() const override;


    protected:

        /// @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
        virtual void onInitPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
        virtual void onConnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
        virtual void onDisconnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
        virtual void onExitPointCloudProcessor() override;


        /// @see visionx::PointCloudProcessor::process()
        virtual void process() override;


        /// @see PropertyUser::createPropertyDefinitions()
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        DebugDrawerInterfacePrx debugDrawer;
        DebugObserverInterfacePrx debugObserver;


        pcl::MomentOfInertiaEstimation<PointT> bb;

    };
}
