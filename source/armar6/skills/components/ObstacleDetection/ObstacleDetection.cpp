/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::ObstacleDetection
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ObstacleDetection.h"

#include <Eigen/Core>

// #include <pcl/filters/filter.h>
//
#include <pcl/common/colors.h>

#include <VisionX/components/pointcloud_core/PCLUtilities.h>

#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/core/FramedPose.h>


namespace armarx
{

    ObstacleDetectionPropertyDefinitions::ObstacleDetectionPropertyDefinitions(std::string prefix) :
        visionx::PointCloudProcessorPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");
        defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the DebugDrawerTopic");
    }


    std::string ObstacleDetection::getDefaultName() const
    {
        return "ObstacleDetection";
    }


    void ObstacleDetection::onInitPointCloudProcessor()
    {
        offeringTopic(getProperty<std::string>("DebugObserverName").getValue());
        offeringTopic(getProperty<std::string>("DebugDrawerTopicName").getValue());
    }


    void ObstacleDetection::onConnectPointCloudProcessor()
    {
        debugObserver = getTopic<DebugObserverInterfacePrx>(getProperty<std::string>("DebugObserverName").getValue());
        debugDrawer = getTopic<DebugDrawerInterfacePrx>(getProperty<std::string>("DebugDrawerTopicName").getValue());

        enableResultPointClouds<PointT>();

        debugDrawer->clearLayer(getName());
    }


    void ObstacleDetection::onDisconnectPointCloudProcessor()
    {
    }

    void ObstacleDetection::onExitPointCloudProcessor()
    {
    }


    void ObstacleDetection::process()
    {
        // Fetch input point cloud.
        pcl::PointCloud<PointT>::Ptr inputCloud(new pcl::PointCloud<PointT>());

        if (!waitForPointClouds())
        {
            ARMARX_VERBOSE << "Timeout or error while waiting for point cloud data";
            return;
        }
        else
        {
            getPointClouds(inputCloud);
        }

        // Do processing.


        std::map<uint32_t, pcl::PointIndices> labelMap;
        visionx::tools::fillLabelMap(inputCloud, labelMap);


        bb.setInputCloud(inputCloud);


        for (const auto& [_label, _indices] : labelMap)
        {
            pcl::IndicesPtr indices(new std::vector<int>(_indices.indices));

            PointT min_point_OBB, max_point_OBB, position_OBB;
            Eigen::Matrix3f rotational_matrix_OBB;
            bb.setIndices(indices);
            bb.compute();
            bb.getOBB(min_point_OBB, max_point_OBB, position_OBB, rotational_matrix_OBB);

            Eigen::Vector3f position(position_OBB.x, position_OBB.y, position_OBB.z);
            Vector3Ptr dimension = new Vector3(max_point_OBB.x * 2, max_point_OBB.y * 2, max_point_OBB.z * 2);
            armarx::FramedPosePtr pose = new FramedPose(rotational_matrix_OBB, position, armarx::GlobalFrame, "");

            pcl::RGB c = pcl::GlasbeyLUT::at(_label % pcl::GlasbeyLUT::size());
            DrawColor color = {c.r / 255.0f, c.g / 255.0f, c.b / 255.0f, 0.9};

            debugDrawer->setBoxVisu(getName(), "bb_" + std::to_string(_label), pose, dimension, color);
        }

        StringVariantBaseMap debugValues;
        debugValues["debug_value"] = new Variant(static_cast<int>(inputCloud->header.stamp));
        debugObserver->setDebugChannel(getName(), debugValues);


        // Publish result point cloud.

        provideResultPointClouds(inputCloud);
        // provideResultPointClouds<PointT>(inputCloud, "ResultProviderName");
    }


    armarx::PropertyDefinitionsPtr ObstacleDetection::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new ObstacleDetectionPropertyDefinitions(
                getConfigIdentifier()));
    }

}
