/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::Armar6GraspProvider
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include "RobotAPI/components/ArViz/Client/elements/Color.h"
#include <ArmarXCore/core/Component.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>
#include <armar6/skills/interface/Armar6GraspProviderInterface.h>
#include <RobotAPI/interface/units/GraspCandidateProviderInterface.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h>

#include <VirtualRobot/Robot.h>

#include <RobotAPI/interface/core/RobotState.h>

#include <pcl/point_types.h>

#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/passthrough.h>

#include <armar6/skills/libraries/Armar6Tools/Armar6GraspTrajectory.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/interface/StaticPlotterInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/ObjectInstanceSegmentWrapper.h>

#include <VirtualRobot/math/Line.h>

#include <mutex>

namespace armarx
{


    /**
     * @class Armar6GraspProviderPropertyDefinitions
     * @brief
     */
    class Armar6GraspProviderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        Armar6GraspProviderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the debug observer that should be used");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the debug drawer topic that should be used");
            defineOptionalProperty<std::string>("StaticPlotterName", "StaticPlotter", "Name of the static plotter that should be used");

            defineOptionalProperty<std::string>("GraspCandidatesTopicName", "GraspCandidatesTopic", "Name of the Grasp Candidate Topic");
            defineOptionalProperty<std::string>("ConfigTopicName", "GraspCandidateProviderConfigTopic", "Name of the Grasp Candidate Provider Config Topic");

            defineOptionalProperty<std::string>("PointCloudFormat", "XYZRGBL", "Format of the input and output point cloud (XYZRGBA, XYZL, XYZRGBL)");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
            defineOptionalProperty<std::string>("providerName", "TabletopSegmentationResult", "name of the point cloud provider");
            defineOptionalProperty<std::string>("serviceProviderName", "TabletopSegmentation", "name of the service provider");
            defineOptionalProperty<std::string>("sourceFrameName", "root", "The source frame name");

            defineOptionalProperty<uint32_t>("BackgroundLabelId", 1, "Label in the pointcloud for the plane or background.", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<bool>("UseObjectIdMap", false, "Use ID map from config for named objects");

            defineOptionalProperty<float>("DownsamplingLeafSize", 5.f, "If >0, the pointcloud is downsampled with a leaf size of this value. In mm.", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<size_t>("MininumClusterPointCount", 100, "Minimum number of points per cluster/segmented object to be further processed.", PropertyDefinitionBase::eModifiable);

            defineOptionalProperty<float>("BoundingBoxZ1", 975.f, "");
            defineOptionalProperty<float>("BoundingBoxY1", 0.f, "");
            defineOptionalProperty<float>("BoundingBoxY2", 1300.f, "");

            defineOptionalProperty<float>("BoundingBoxX1", -200, "");
            defineOptionalProperty<float>("BoundingBoxX2", 200, "");

            defineOptionalProperty<bool>("enablePointCloudsVisu", true, "");
            defineOptionalProperty<bool>("enableVisu", true, "");
            defineOptionalProperty<bool>("enableCandidatesVisu", true, "");
            defineOptionalProperty<bool>("enableBoundigBoxesVisu", false, "");
            defineOptionalProperty<bool>("FlattenInZ", true, "");
            defineOptionalProperty<bool>("UseObjectPoseStorage", false, "");
            defineOptionalProperty<bool>("UseWorkingMemory", false, "");
            defineOptionalProperty<int>("WaitForPointCloudsMS", 200, "");

            defineOptionalProperty<std::string>("RemoteGuiName", "RemoteGuiProvider", "Name of the remote gui");
            defineOptionalProperty<std::string>("PriorKnowledgeName", "PriorKnowledge", "Name of the PriorKnowledge");
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Name of the WorkingMemory");


            defineOptionalProperty<int>("colorNonReachableGraspsR", 128, "The red color channel for non reachable grasps");
            defineOptionalProperty<int>("colorNonReachableGraspsG", 128, "The green color channel for non reachable grasps");
            defineOptionalProperty<int>("colorNonReachableGraspsB", 128, "The blue color channel color channel for non reachable grasps");
        }
    };



    /**
     * @defgroup Component-Armar6GraspProvider Armar6GraspProvider
     * @ingroup armar6_skills-Components
     * A description of the component Armar6GraspProvider.
     *
     * @class Armar6GraspProvider
     * @ingroup Component-Armar6GraspProvider
     * @brief Brief description of class Armar6GraspProvider.
     *
     * Detailed description of class Armar6GraspProvider.
     */
    class Armar6GraspProvider
        : virtual public armarx::Armar6GraspProviderInterface
        , virtual public visionx::PointCloudProcessor
        , virtual public ObjectPoseClientPluginUser
        , virtual public ArVizComponentPluginUser
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "Armar6GraspProvider";
        }

        struct BoundingBox
        {
            float x1 = -10000;
            float x2 = 10000;
            float y1 = -10000;
            float y2 = 10000;
            float z1 = -10000;
            float z2 = 10000;

            bool isInside(float x, float y, float z);
        };



        struct Config
        {
            std::map<std::string, Eigen::Matrix3f> ReferenceOrientationMap;
            float ObjectDepthPreshapeThreshold;
            float ObjectHeightPreshapeThreshold;
            float HandGraspingRadius;
            std::map<std::string, Eigen::Vector3f> GraspOffsetTCP;
            std::map<std::string, float> PreshapeVisu;
            std::map<std::string, float> OpenVisu;
            std::map<int, std::string> ObjectIdMap;
            std::map<std::string, std::string> ObjectGraspTypeMap;
            bool UseObjectIdMap;

            StringVariantBaseMap getAsVariantMap();
            IceUtil::Time disablePointCloudVisuUntil = IceUtil::Time::seconds(0);
            IceUtil::Time disableCandidatesVisuUntil = IceUtil::Time::seconds(0);

            bool enableVisu;
            bool enablePointCloudsVisu;
            bool enableCandidatesVisu;
            bool enableBoundigBoxesVisu;

            viz::Color colorNonReachableGrasps;

            bool getEffectivePointCloudVisu()
            {
                return enablePointCloudsVisu && IceUtil::Time::now() > disablePointCloudVisuUntil;
            }
            bool getEffectiveCandidatesVisu()
            {
                return enableCandidatesVisu && IceUtil::Time::now() > disableCandidatesVisuUntil;
            }

        };



        struct GraspDescription
        {
            Eigen::Matrix4f pose;
            Eigen::Vector3f approach;
            Armar6GraspProviderModule::PreshapeType preshape;
        };

        static std::string GraspTypeToString(Armar6GraspProviderModule::GraspType graspType);
        static std::string PreshapeTypeToString(Armar6GraspProviderModule::PreshapeType preshapeType);

    protected:
        /**
         * @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
         */
        void onInitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
         */
        void onConnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
         */
        void onDisconnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
         */
        void onExitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::process()
         */
        void process() override;

        void processPointCloud();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


        // GraspCandidateProviderInterface interface
    public:
        void requestService(const std::string& providerName, Ice::Int relativeTimeoutMS, const Ice::Current&) override;
        void setServiceConfig(const std::string& providerName, const StringVariantBaseMap& config, const Ice::Current&) override;

        // Armar6GraspProviderInterface interface
    public:
        Armar6GraspProviderModule::ResultSeq getResult(const Ice::Current&) override;
        Ice::Int getProcessCount(const Ice::Current&) override;


    private:
        void readConfig();
        std::map<std::string, Eigen::Matrix3f> readQuaternionMap(const RapidXmlReaderNode& node);
        std::map<int, std::string> readObjectIdMap(const RapidXmlReaderNode& node);
        std::map<std::string, std::string> readObjectGraspTypeMap(const RapidXmlReaderNode& node);
        std::map<std::string, Eigen::Vector3f> readVectorMap(const RapidXmlReaderNode& node);
        std::map<std::string, float> readPreshapeVisu(const RapidXmlReaderNode& node);
        void calculateReferenceOrientations();
        grasping::ProviderInfoPtr getProviderInfo();

        void calculateGraspCandidatesForBB(const objpose::ObjectPose& objectPose, // simox::OrientedBoxf const& bb,
                                           std::string const& className,
                                           std::string const& instanceName,
                                           VirtualRobot::RobotNodeSetPtr const& rns,
                                           VirtualRobot::RobotNodePtr const& tcp,
                                           viz::Layer& layer,
                                           grasping::GraspCandidateSeq& candidatesOut,
                                           Armar6GraspProviderModule::ResultSeq& resultListOut);

    private:



        std::mutex resultMutex;
        std::mutex capturedPointsMutex;
        std::string pointCloudFormat;

        VirtualRobot::RobotPtr localRobot;

        armarx::RobotStateComponentInterfacePrx robotStateComponent;
        DebugObserverInterfacePrx debugObserver;
        StaticPlotterInterfacePrx staticPlotter;


        std::string sourceFrameName;
        std::string providerName;

        BoundingBox boundingBox;
        bool flattenInZ = false;
        bool useObjectPoseStorage = false;
        bool useWorkingMemory = false;
        int waitForPointCloudsMS = 200;

        memoryx::PriorKnowledgeInterfacePrx priorKnowledge;
        memoryx::WorkingMemoryInterfacePrx workingMemory;
        memoryx::ObjectInstanceSegmentWrapper objectInstanceSegment;

        Armar6GraspProviderModule::ResultSeq resultList;
        int processCount = 0;

        RemoteGuiInterfacePrx remoteGuiPrx;

        SimplePeriodicTask<>::pointer_type guiTask;
        SimplePeriodicTask<>::pointer_type reportConfigTask;

        RemoteGui::TabProxy guiTab;

        RobotNameHelperPtr rnh;

        Config config;

        grasping::GraspCandidatesTopicInterfacePrx graspCandidatesTopic;
        RequestableServiceListenerInterfacePrx serviceRequestTopic;
        std::map<std::string, Armar6GraspTrajectoryPtr> graspTrajectories;
        IceUtil::Time disableVisuUntil;


    };
}
