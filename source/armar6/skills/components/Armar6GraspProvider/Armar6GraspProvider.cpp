/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::Armar6GraspProvider
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6GraspProvider.h"

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>
#include <ArmarXGui/libraries/StructuralJson/JPathNavigator.h>
#include <ArmarXGui/libraries/StructuralJson/StructuralJsonParser.h>
#include <RobotAPI/libraries/SimpleJsonLogger/SimpleJsonLoggerEntry.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <VirtualRobot/math/Helpers.h>
#include <VirtualRobot/ManipulationObject.h>
#include <VirtualRobot/Visualization/TriMeshModel.h>
#include <cmath>
#include "FitRectangle.h"
#include "RobotAPI/components/ArViz/Client/elements/Color.h"

#include <RobotAPI/libraries/diffik/SimpleDiffIK.h>

#include <armar6/skills/libraries/Armar6Tools/Armar6HandV2Helper.h>

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

using namespace math;
using namespace armarx;



std::string Armar6GraspProvider::GraspTypeToString(Armar6GraspProviderModule::GraspType graspType)
{
    switch (graspType)
    {
        case Armar6GraspProviderModule::GraspType::Top:
            return "Top";
        case Armar6GraspProviderModule::GraspType::Side:
            return "Side";
        default:
            throw LocalException("Unknown GraspType");
    }
}

std::string Armar6GraspProvider::PreshapeTypeToString(Armar6GraspProviderModule::PreshapeType preshapeType)
{
    switch (preshapeType)
    {
        case Armar6GraspProviderModule::PreshapeType::Open:
            return "Open";
        case Armar6GraspProviderModule::PreshapeType::Preshaped:
            return "Preshaped";
        default:
            throw LocalException("Unknown GraspType");
    }
}

void Armar6GraspProvider::onInitPointCloudProcessor()
{
    offeringTopic("ServiceRequests");
    usingTopic("ServiceRequests");
    providerName = getProperty<std::string>("providerName").getValue();
    sourceFrameName = getProperty<std::string>("sourceFrameName").getValue();
    pointCloudFormat = getProperty<std::string>("PointCloudFormat").getValue();

    boundingBox.z1 = getProperty<float>("BoundingBoxZ1").getValue();
    boundingBox.y1 = getProperty<float>("BoundingBoxY1").getValue();
    boundingBox.y2 = getProperty<float>("BoundingBoxY2").getValue();
    boundingBox.x1 = getProperty<float>("BoundingBoxX1").getValue();
    boundingBox.x2 = getProperty<float>("BoundingBoxX2").getValue();

    config.enablePointCloudsVisu = getProperty<bool>("enablePointCloudsVisu").getValue();
    config.enableVisu = getProperty<bool>("enableVisu").getValue();
    flattenInZ = getProperty<bool>("FlattenInZ").getValue();
    useObjectPoseStorage = getProperty<bool>("UseObjectPoseStorage").getValue();
    useWorkingMemory = getProperty<bool>("UseWorkingMemory").getValue();
    if (useWorkingMemory)
    {
        usingProxyFromProperty("PriorKnowledgeName");
        usingProxyFromProperty("WorkingMemoryName");
    }
    waitForPointCloudsMS = getProperty<int>("WaitForPointCloudsMS").getValue();

    usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
    usingPointCloudProvider(providerName);

    offeringTopic(getProperty<std::string>("DebugObserverName").getValue());
    offeringTopic(getProperty<std::string>("DebugDrawerTopicName").getValue());
    offeringTopic(getProperty<std::string>("StaticPlotterName").getValue());
    usingProxy(getProperty<std::string>("RemoteGuiName").getValue());

    offeringTopic(getProperty<std::string>("GraspCandidatesTopicName").getValue());
    usingTopic(getProperty<std::string>("ConfigTopicName").getValue());

    processCount = 0;
    disableVisuUntil = TimeUtil::GetTime();

    readConfig();


}

void Armar6GraspProvider::readConfig()
{
    std::string packageName = "armar6_skills";
    armarx::CMakePackageFinder finder(packageName);
    std::string dataDir = finder.getDataDir() + "/" + packageName;
    std::string filepath = dataDir + "/Armar6GraspProviderConfig.xml";

    RapidXmlReaderPtr reader = RapidXmlReader::FromFile(filepath);
    RapidXmlReaderNode rootNode = reader->getRoot("Armar6GraspProviderConfig");

    config.ReferenceOrientationMap = readQuaternionMap(rootNode.first_node("ReferenceOrientationMap"));
    config.ObjectDepthPreshapeThreshold = rootNode.first_node("ObjectDepthPreshapeThreshold").value_as_float();
    config.ObjectHeightPreshapeThreshold = rootNode.first_node("ObjectHeightPreshapeThreshold").value_as_float();
    config.HandGraspingRadius = rootNode.first_node("HandGraspingRadius").value_as_float();
    config.GraspOffsetTCP = readVectorMap(rootNode.first_node("GraspOffsetTCP"));
    config.PreshapeVisu = readPreshapeVisu(rootNode.first_node("PreshapeVisu"));
    config.OpenVisu = readPreshapeVisu(rootNode.first_node("OpenVisu"));
    config.ObjectIdMap = readObjectIdMap(rootNode.first_node("ObjectNameIdMap"));
    config.ObjectGraspTypeMap = readObjectGraspTypeMap(rootNode.first_node("ObjectGraspTypeMap"));
    config.UseObjectIdMap = getProperty<bool>("UseObjectIdMap").getValue();

    config.colorNonReachableGrasps = viz::Color(
                                         std::clamp(getProperty<int>("colorNonReachableGraspsR").getValue(), 0, 255),
                                         std::clamp(getProperty<int>("colorNonReachableGraspsG").getValue(), 0, 255),
                                         std::clamp(getProperty<int>("colorNonReachableGraspsB").getValue(), 0, 255));

    ARMARX_INFO << "Non reachable grasp color is " << config.colorNonReachableGrasps;

    graspTrajectories["RightTopOpen"] = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/RightTopOpen.xml");
    graspTrajectories["RightSideOpen"] = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/RightSideOpen.xml");

    calculateReferenceOrientations();
}

std::map<int, std::string> Armar6GraspProvider::readObjectIdMap(const RapidXmlReaderNode& node)
{
    StructuralJsonParser parser(node.value());
    parser.parse();
    JPathNavigator rootNav(parser.parsedJson);
    std::map<int, std::string> map;
    for (const JPathNavigator& nav : rootNav.select("*"))
    {
        map[nav.asInt()] = nav.getKey();
    }
    return map;
}

std::map<std::string, std::string> Armar6GraspProvider::readObjectGraspTypeMap(const RapidXmlReaderNode& node)
{
    StructuralJsonParser parser(node.value());
    parser.parse();
    JPathNavigator rootNav(parser.parsedJson);
    std::map<std::string, std::string> map;
    for (const JPathNavigator& nav : rootNav.select("*"))
    {
        map[nav.getKey()] = nav.asString();
    }
    return map;
}


std::map<std::string, Eigen::Matrix3f> Armar6GraspProvider::readQuaternionMap(const RapidXmlReaderNode& node)
{
    StructuralJsonParser parser(node.value());
    parser.parse();
    JPathNavigator rootNav(parser.parsedJson);
    std::map<std::string, Eigen::Matrix3f> map;
    for (const JPathNavigator& nav : rootNav.select("*"))
    {
        float qw = nav.selectSingleNode("qw").asFloat();
        float qx = nav.selectSingleNode("qx").asFloat();
        float qy = nav.selectSingleNode("qy").asFloat();
        float qz = nav.selectSingleNode("qz").asFloat();
        map[nav.getKey()] = Eigen::Quaternionf(qw, qx, qy, qz).toRotationMatrix();
    }
    return map;
}

std::map<std::string, Eigen::Vector3f> Armar6GraspProvider::readVectorMap(const RapidXmlReaderNode& node)
{
    StructuralJsonParser parser(node.value());
    parser.parse();
    JPathNavigator rootNav(parser.parsedJson);
    std::map<std::string, Eigen::Vector3f> map;
    for (const JPathNavigator& nav : rootNav.select("*"))
    {
        float x = nav.selectSingleNode("x").asFloat();
        float y = nav.selectSingleNode("y").asFloat();
        float z = nav.selectSingleNode("z").asFloat();
        map[nav.getKey()] = Eigen::Vector3f(x, y, z);
    }
    return map;
}

std::map<std::string, float> Armar6GraspProvider::readPreshapeVisu(const RapidXmlReaderNode& node)
{
    StructuralJsonParser parser(node.value());
    parser.parse();
    JPathNavigator rootNav(parser.parsedJson);
    std::map<std::string, float> map;
    for (const JPathNavigator& nav : rootNav.select("*"))
    {
        map[nav.getKey()] = nav.asFloat();
    }
    return map;
}

void Armar6GraspProvider::calculateReferenceOrientations()
{
    Eigen::Matrix3f RightTopOpen = config.ReferenceOrientationMap["RightTopOpen"];
    config.ReferenceOrientationMap["RightSideOpen"] = Eigen::AngleAxisf(M_PI / 2, Eigen::Vector3f::UnitY()) * RightTopOpen;
    config.ReferenceOrientationMap["RightTopBlob"] = Eigen::AngleAxisf(::math::Helpers::deg2rad(45), Eigen::Vector3f::UnitZ()) * RightTopOpen;
}

grasping::ProviderInfoPtr Armar6GraspProvider::getProviderInfo()
{
    grasping::ProviderInfoPtr info = new grasping::ProviderInfo();
    if (config.UseObjectIdMap)
    {
        info->objectType = objpose::KnownObject;
    }
    else
    {
        info->objectType = objpose::UnknownObject;
    }
    info->currentConfig = config.getAsVariantMap();
    return info;
}

void Armar6GraspProvider::onConnectPointCloudProcessor()
{
    serviceRequestTopic = getTopic<RequestableServiceListenerInterfacePrx>("ServiceRequests");

    robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
    localRobot = RemoteRobot::createLocalCloneFromFile(robotStateComponent, VirtualRobot::RobotIO::eStructure);
    debugObserver = getTopic<DebugObserverInterfacePrx>(getProperty<std::string>("DebugObserverName").getValue());
    staticPlotter = getTopic<StaticPlotterInterfacePrx>(getProperty<std::string>("StaticPlotterName").getValue());
    remoteGuiPrx = getProxy<RemoteGuiInterfacePrx>(getProperty<std::string>("RemoteGuiName").getValue());
    graspCandidatesTopic = getTopic<grasping::GraspCandidatesTopicInterfacePrx>(getProperty<std::string>("GraspCandidatesTopicName").getValue());
    if (useWorkingMemory)
    {
        getProxyFromProperty(priorKnowledge, "PriorKnowledgeName");
        getProxyFromProperty(workingMemory, "WorkingMemoryName");

        objectInstanceSegment.initFromProxies(priorKnowledge, workingMemory, robotStateComponent);
    }

    rnh = RobotNameHelper::Create(robotStateComponent->getRobotInfo(), nullptr);

    enableResultPointClouds<pcl::PointXYZRGBL>(getName() + "Result");


    RemoteGui::detail::VBoxLayoutBuilder rootLayoutBuilder = RemoteGui::makeVBoxLayout();

    auto makeSlider = [&](std::string name, int min, int max, float val)
    {
        rootLayoutBuilder.addChild(
            RemoteGui::makeHBoxLayout()
            .addChild(RemoteGui::makeTextLabel(name))
            .addChild(RemoteGui::makeFloatSlider(name).min(min).max(max).value(val))
            .addChild(RemoteGui::makeFloatSpinBox(name + "_spin").min(min).max(max).value(val))
        );
    };

    RemoteGui::detail::HBoxLayoutBuilder checkBoxLayoutBuilder = RemoteGui::makeHBoxLayout();

    auto makeCheckBox = [&](std::string name, bool val)
    {
        checkBoxLayoutBuilder.addChild(
            RemoteGui::makeHBoxLayout()
            .addChild(RemoteGui::makeCheckBox(name).value(val))
            .addChild(RemoteGui::makeTextLabel(name))
        );
    };


    // ValueProxy<float> x1Slider = makeSlider ...
    makeSlider("x1", -600, 600, boundingBox.x1);
    makeSlider("x2", -600, 600, boundingBox.x2);
    makeSlider("y1", 0, 1500, boundingBox.y1);
    makeSlider("y2", 0, 1500, boundingBox.y2);
    makeSlider("z1", 300, 1500, boundingBox.z1);
    rootLayoutBuilder.addChild(checkBoxLayoutBuilder);
    makeCheckBox("FlattenInZ", true);
    makeCheckBox("enableVisu", true);
    makeCheckBox("enableCandidatesVisu", true);
    makeCheckBox("enableBoundigBoxesVisu", false);
    makeCheckBox("enablePointCloudsVisu", true);
    checkBoxLayoutBuilder.addChild(new RemoteGui::HSpacer());

    rootLayoutBuilder.addChild(new RemoteGui::VSpacer());

    guiTask = new SimplePeriodicTask<>([&]()
    {
        guiTab.receiveUpdates();
        boundingBox.x1 = guiTab.getValue<float>("x1").get();
        boundingBox.x2 = guiTab.getValue<float>("x2").get();
        boundingBox.y1 = guiTab.getValue<float>("y1").get();
        boundingBox.y2 = guiTab.getValue<float>("y2").get();
        boundingBox.z1 = guiTab.getValue<float>("z1").get();
        flattenInZ = guiTab.getValue<bool>("FlattenInZ").get();
        config.enableVisu = guiTab.getValue<bool>("enableVisu").get();
        config.enableCandidatesVisu = guiTab.getValue<bool>("enableCandidatesVisu").get();
        config.enableBoundigBoxesVisu = guiTab.getValue<bool>("enableBoundigBoxesVisu").get();
        config.enablePointCloudsVisu = guiTab.getValue<bool>("enablePointCloudsVisu").get();

        guiTab.getValue<float>("x1_spin").set(boundingBox.x1);
        guiTab.getValue<float>("x2_spin").set(boundingBox.x2);
        guiTab.getValue<float>("y1_spin").set(boundingBox.y1);
        guiTab.getValue<float>("y2_spin").set(boundingBox.y2);
        guiTab.getValue<float>("z1_spin").set(boundingBox.z1);

        guiTab.sendUpdates();

        //ARMARX_IMPORTANT << VAROUT(boundingBox.z1);
    }, 10, false, "guiTask");

    reportConfigTask = new SimplePeriodicTask<>([&]()
    {
        graspCandidatesTopic->reportProviderInfo(getName(), getProviderInfo());
    }, 1000, false, "reportConfigTask");

    RemoteGui::WidgetPtr rootLayout = rootLayoutBuilder;

    remoteGuiPrx->createTab(getName(), rootLayout);
    guiTab = RemoteGui::TabProxy(remoteGuiPrx, getName());

    guiTask->start();
    reportConfigTask->start();

}

void Armar6GraspProvider::onDisconnectPointCloudProcessor()
{
    guiTask->stop();
    guiTask = nullptr;
    reportConfigTask->stop();
}

void Armar6GraspProvider::onExitPointCloudProcessor()
{
}

void Armar6GraspProvider::process()
{
    if (pointCloudFormat == "XYZRGBL")
    {
        processPointCloud();
    }
    else
    {
        ARMARX_ERROR << "Could not process point cloud, because format '" << pointCloudFormat << "' is not suppoted / unknown";
    }
}

namespace maptools
{
    template<typename TKey, typename TValue>
    bool TryGetValue(const std::map<TKey, TValue>& map, const TKey& key, TValue& outVal)
    {
        typename std::map<TKey, TValue>::const_iterator it = map.find(key);
        if (it == map.end())
        {
            return false;
        }
        outVal = it->second;
        return true;
    }

    template<typename TKey, typename TValue, typename TDefaultValue>
    TValue GetValueOrDefault(const std::map<TKey, TValue>& map, const TKey& key, const TDefaultValue& defaultVal)
    {
        typename std::map<TKey, TValue>::const_iterator it = map.find(key);
        if (it == map.end())
        {
            return defaultVal;
        }
        return it->second;
    }

    template<typename TKey, typename TValue>
    bool HasKey(const std::map<TKey, TValue>& map, const TKey& key)
    {
        return map.count(key) > 0;
    }
}

static void sortBoxExtendsForGraspCandidateGeneration(Eigen::Vector3f& v1,
        Eigen::Vector3f& v2,
        Eigen::Vector3f& v3,
        Eigen::Vector3f& robotExtend1,
        Eigen::Vector3f& robotExtend2,
        Eigen::Vector3f& robotExtend3)
{
    // These have to be sorted in a specific way
    //  v3 should point in the direction of Z
    //  v1 should point along the longer remaining axis (after aligning v3)
    Eigen::Vector3f z = Eigen::Vector3f::UnitZ();
    if (std::abs(v1.dot(z)) > 0.8f)
    {
        std::swap(v1, v3);
        std::swap(robotExtend1, robotExtend3);
    }
    if (std::abs(v2.dot(z)) > 0.8f)
    {
        std::swap(v2, v3);
        std::swap(robotExtend2, robotExtend3);
    }

    if (v3.z() < 0.0f)
    {
        v3 = -v3;
        robotExtend3 = -robotExtend3;
    }

    if (robotExtend1.norm() < robotExtend2.norm())
    {
        Eigen::Vector3f temp = v1;
        v1 = v2;
        v2 = -temp;

        temp = robotExtend1;
        robotExtend1 = robotExtend2;
        robotExtend2 = -temp;
    }
}

void Armar6GraspProvider::processPointCloud()
{
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr inputCloudPtr(new pcl::PointCloud<pcl::PointXYZRGBL>());
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr tmpCloudPtr(new pcl::PointCloud<pcl::PointXYZRGBL>());
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr outputCloudPtr(new pcl::PointCloud<pcl::PointXYZRGBL>());


    Armar6GraspProviderModule::ResultSeq resultList;
    int detectedObjects = 0;


    std::stringstream ssDebug;

    grasping::GraspCandidateSeq candidates;

    RobotNameHelper::RobotArm arm = rnh->getRobotArm("Right", localRobot);
    VirtualRobot::RobotNodeSetPtr rns = arm.getKinematicChain();
    VirtualRobot::RobotNodePtr tcp = arm.getTCP();

    StringVector2fSeqDict plots;

    viz::Layer layerForPC = arviz.layer("FromPointCloud");
    viz::Layer layerForOPO = arviz.layer("FromObjectPoseStorage");
    viz::Layer layerForWM = arviz.layer("FromWorkingMemory");

    Eigen::Matrix4f robotToGlobal = Eigen::Matrix4f::Identity();

    auto drawLine = [&](std::string const & name, Eigen::Vector3f fromRobot, Eigen::Vector3f toRobot, viz::Color color,
                        viz::Layer & layer)
    {
        Eigen::Vector4f fromGlobal = robotToGlobal * fromRobot.homogeneous();
        Eigen::Vector4f toGlobal = robotToGlobal * toRobot.homogeneous();
        viz::Cylinder cyl = viz::Cylinder(name)
                            .fromTo(fromGlobal.head<3>(), toGlobal.head<3>())
                            .radius(3.0f)
                            .color(color);
        layer.add(cyl);
    };

    auto drawBox = [&](std::string const & name, Eigen::Matrix4f const & poseRobot, Eigen::Vector3f size, viz::Color color,
                       viz::Layer & layer)
    {
        Eigen::Matrix4f poseGlobal = robotToGlobal * poseRobot;
        viz::Box box = viz::Box(name)
                       .pose(poseGlobal)
                       .size(size)
                       .color(color);
        layer.add(box);
    };

    if (waitForPointClouds(providerName, waitForPointCloudsMS))
    {
        getPointClouds(providerName, inputCloudPtr);

        typename pcl::PointCloud<pcl::PointXYZRGBL>::Ptr downsampledCloudPtr(new pcl::PointCloud<pcl::PointXYZRGBL>());

        float gridLeafSize = getProperty<float>("DownsamplingLeafSize");
        if (gridLeafSize > 0)
        {
            pcl::ApproximateVoxelGrid<pcl::PointXYZRGBL> grid;
            //    grid.setDownsampleAllData(false);
            grid.setLeafSize(gridLeafSize, gridLeafSize, gridLeafSize);
            grid.setInputCloud(inputCloudPtr);
            grid.filter(*downsampledCloudPtr);
            inputCloudPtr.swap(downsampledCloudPtr);
        }


        visionx::MetaPointCloudFormatPtr format = getPointCloudFormat(providerName);


        int inputSize = inputCloudPtr->points.size();

        if (inputSize < 100)
        {
            ARMARX_INFO << deactivateSpam(60) << "Input size too small! Skipping grasp candidate generation";
            return;
        }

        ARMARX_VERBOSE << "processPointCloud: " << inputCloudPtr->width << "x" << inputCloudPtr->height << " age: " << (TimeUtil::GetTime() - IceUtil::Time::microSeconds(inputCloudPtr->header.stamp)).toMilliSeconds();

        RemoteRobot::synchronizeLocalCloneToTimestamp(localRobot, robotStateComponent, inputCloudPtr->header.stamp);
        robotToGlobal = localRobot->getGlobalPose();

        Eigen::Matrix4f sourceFrameInRootFrame = Eigen::Matrix4f::Identity();
        if (sourceFrameName == "Global")
        {
            sourceFrameInRootFrame = localRobot->getRootNode()->getGlobalPose().inverse();
        }
        else
        {
            sourceFrameInRootFrame = localRobot->getRobotNode(sourceFrameName)->getPoseInRootFrame();
        }


        // Transform to global frame
        pcl::transformPointCloud(*inputCloudPtr, *tmpCloudPtr, sourceFrameInRootFrame);
        tmpCloudPtr.swap(inputCloudPtr);

        tmpCloudPtr->clear();
        //std::vector<Eigen::Vector3f> segmentedPoints;



        uint32_t backgroundLabel = getProperty<uint32_t>("BackgroundLabelId").getValue();
        std::map<uint32_t, std::vector<Eigen::Vector3f>> labeledPoints;
        std::map<uint32_t, DrawColor> colorMap;
        std::map<uint32_t, pcl::PointCloud<pcl::PointXYZRGBL>::Ptr> labeledClouds;

        for (const pcl::PointXYZRGBL& point : inputCloudPtr->points)
        {
            bool anyNan = ::isnan(point.x) || ::isnan(point.y) || ::isnan(point.z) || ::isnan(point.r) || ::isnan(point.g) || ::isnan(point.b);
            if (anyNan)
            {
                continue;
            }
            if (point.label == backgroundLabel)
            {
                //            ARMARX_VERBOSE << "Skipping background label with " << allPoints.size() << " points";
                continue;
            }
            pcl::PointXYZRGBL p;
            p.x = point.x;
            p.y = point.y;
            p.z = point.z;
            p.r = point.r;
            p.g = point.g;
            p.b = point.b;
            p.label = point.label;

            if (!labeledClouds[point.label])
            {
                labeledClouds[point.label].reset(new pcl::PointCloud<pcl::PointXYZRGBL>());
            }

            colorMap[point.label] = DrawColor {point.r / 255.f, point.g / 255.f, point.b / 255.f, 1.f};

            labeledClouds[point.label]->push_back(p);

            outputCloudPtr->push_back(p);
        }
        for (const auto& pair : labeledClouds)
        {
            pcl::PointCloud<pcl::PointXYZRGBL>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZRGBL>());
            pcl::StatisticalOutlierRemoval<pcl::PointXYZRGBL> sor;
            sor.setInputCloud(pair.second);
            sor.setMeanK(50);
            sor.setStddevMulThresh(1.0);
            sor.filter(*cloud_filtered);
            for (const pcl::PointXYZRGBL& point : cloud_filtered->points)
            {
                labeledPoints[point.label].push_back(Eigen::Vector3f(point.x, point.y, point.z));
            }
        }



        for (const auto& pair : labeledPoints)
        {
            std::vector<Eigen::Vector3f> allPoints = pair.second;
            uint32_t label = pair.first;

            if (label == backgroundLabel)
            {
                ARMARX_VERBOSE << "Skipping background label with " << allPoints.size() << " points";
                continue;
            }
            std::string objectName = "";
            bool isKnownObject = false;
            if (config.UseObjectIdMap)
            {
                objectName = maptools::GetValueOrDefault(config.ObjectIdMap, (int)label, "UNKNOWN");
                isKnownObject = maptools::HasKey(config.ObjectIdMap, (int)label);
            }
            else
            {
                objectName = "OBJ_" + std::to_string(label);
            }

            ARMARX_VERBOSE << VAROUT(objectName) << VAROUT(label) << " " << VAROUT(allPoints.size());

            if (allPoints.size() < getProperty<size_t>("MininumClusterPointCount").getValue())
            {
                ARMARX_VERBOSE << /*deactivateSpam(1, objectName) <<*/ "too few points for PCA for " << objectName << " (" << label << ")";
            }
            else
            {
                auto determineGraspType = [&](const Box3D & box)
                {
                    float height = box.Size3();
                    float width = box.Size1();
                    return width > height ? Armar6GraspProviderModule::GraspType::Top : Armar6GraspProviderModule::GraspType::Side;
                };

                FitRectangle fitBox(90);
                Box3D box = fitBox.Fit(allPoints);
                Eigen::Vector3f center = box.GetPoint(0, 0, 0);
                Armar6GraspProviderModule::GraspType graspType = determineGraspType(box);

                std::string graspTypeStr = maptools::GetValueOrDefault(config.ObjectGraspTypeMap, objectName, "calculate");
                if (graspTypeStr == "TopBlob")
                {
                    graspType = Armar6GraspProviderModule::GraspType::Top;
                }
                if (graspTypeStr == "Top")
                {
                    graspType = Armar6GraspProviderModule::GraspType::Top;
                }
                if (graspTypeStr == "SideTop")
                {
                    graspType = Armar6GraspProviderModule::GraspType::Side;
                }

                Vector2fSeq boxSizePlot;
                for (size_t i = 0; i < fitBox.results.size(); i++)
                {
                    boxSizePlot.push_back(Vector2f {(float)i, fitBox.results.at(i).Volume()});
                }
                plots[objectName] = boxSizePlot;



                std::vector<Eigen::Vector3f> filteredPoints;
                if (graspType == Armar6GraspProviderModule::GraspType::Top)
                {
                    for (const Eigen::Vector3f& p : allPoints)
                    {
                        if ((p - center).norm() < config.HandGraspingRadius)
                        {
                            filteredPoints.push_back(p);
                        }
                    }

                    if (filteredPoints.size() < 10)
                    {
                        filteredPoints = allPoints;
                    }
                }
                else
                {
                    filteredPoints = allPoints;
                }




                box = fitBox.Fit(filteredPoints);
                center = box.GetPoint(0, 0, 0);

                Eigen::Vector3f v1 = box.GetDir1().normalized();
                Eigen::Vector3f v2 = box.GetDir2().normalized();
                Eigen::Vector3f v3 = box.GetDir3().normalized();
                Line l1 = Line(center, v1);
                Line l2 = Line(center, v2);
                Line l3 = Line(center, v3);

                Percentile p1(FitRectangle::ProjectPoints(l1, filteredPoints));
                Percentile p2(FitRectangle::ProjectPoints(l2, filteredPoints));
                Percentile p3(FitRectangle::ProjectPoints(l3, filteredPoints));


                auto extractExtent = [&](const Percentile & percentile)
                {
                    Armar6GraspProviderModule::Extent extent;
                    extent.min = percentile.get(0.00f);
                    extent.max = percentile.get(1.00f);
                    extent.p05 = percentile.get(0.05f);
                    extent.p10 = percentile.get(0.10f);
                    extent.p90 = percentile.get(0.90f);
                    extent.p95 = percentile.get(0.95f);
                    return extent;
                };




                auto calculateGraspPoseTop = [&](const Armar6GraspProviderModule::Result & result, const Eigen::Vector3f & preferredForward, const std::string & graspTypeStr)
                {
                    Eigen::Vector3f graspForwardVector = v2;

                    Armar6GraspProviderModule::PreshapeType preshape = Armar6GraspProviderModule::PreshapeType::Open;


                    //@@@ disabled for now
                    //float height = result.extent3.p95 - result.extent3.p05;
                    //float depth = result.extent2.p95 - result.extent2.p05;
                    //if (depth < config.ObjectDepthPreshapeThreshold && height < config.ObjectHeightPreshapeThreshold)
                    //{
                    //    preshape = Armar6GraspProviderModule::PreshapeType::Preshaped;
                    //}

                    if (graspForwardVector.dot(preferredForward) < 0)
                    {
                        graspForwardVector = -graspForwardVector;
                    }
                    float angleV2 = Helpers::Angle(Eigen::Vector2f(graspForwardVector(0), graspForwardVector(1)));
                    float angleGraspOri = Helpers::Angle(Eigen::Vector2f(0, 1));
                    float rotationAngle = Helpers::AngleModPI(angleV2 - angleGraspOri);
                    Eigen::Matrix3f referenceOrientation = config.ReferenceOrientationMap.at("RightTopOpen");
                    Eigen::AngleAxisf aa = Eigen::AngleAxisf(rotationAngle, Eigen::Vector3f::UnitZ());
                    Eigen::Matrix3f graspOri = aa.toRotationMatrix() * referenceOrientation;

                    if (graspTypeStr == "TopBlob")
                    {
                        graspOri = config.ReferenceOrientationMap.at("RightTopBlob");
                    }

                    Eigen::Vector3f graspPos = center + v3 * result.extent3.p95 + graspOri * config.GraspOffsetTCP.at("Right");



                    GraspDescription grasp;
                    grasp.pose = Helpers::CreatePose(graspPos, graspOri);
                    grasp.approach = Eigen::Vector3f::UnitZ();
                    grasp.preshape = preshape;

                    return grasp;
                };

                auto calculateGraspPoseSide = [&](const Armar6GraspProviderModule::Result & result, const Eigen::Vector3f & preferredForward)
                {
                    Eigen::Vector3f graspForwardVector = v1;
                    Eigen::Vector3f offset_v1 = v1 * result.extent1.p95;

                    if (graspForwardVector.dot(preferredForward) < 0)
                    {
                        graspForwardVector = -graspForwardVector;
                        offset_v1 = v1 * result.extent1.p05;
                    }

                    float angleV1 = Helpers::Angle(Eigen::Vector2f(graspForwardVector(0), graspForwardVector(1)));
                    float angleGraspOri = Helpers::Angle(Eigen::Vector2f(1, 0));
                    float rotationAngle = Helpers::AngleModPI(angleV1 - angleGraspOri);
                    Eigen::Matrix3f referenceOrientation = config.ReferenceOrientationMap.at("RightSideOpen");
                    Eigen::AngleAxisf aa = Eigen::AngleAxisf(rotationAngle, Eigen::Vector3f::UnitZ());
                    Eigen::Matrix3f graspOri = aa.toRotationMatrix() * referenceOrientation;

                    Eigen::Vector3f graspPos = center + offset_v1;

                    GraspDescription grasp;
                    grasp.pose = Helpers::CreatePose(graspPos, graspOri);
                    grasp.approach = graspForwardVector.normalized();
                    grasp.preshape = Armar6GraspProviderModule::PreshapeType::Open;

                    return grasp;
                };

                Armar6GraspProviderModule::Result result;
                result.inputPointCount = allPoints.size();
                result.center = new Vector3(center);
                result.v1 = new Vector3(v1);
                result.v2 = new Vector3(v2);
                result.v3 = new Vector3(v3);

                result.extent1 = extractExtent(p1);
                result.extent2 = extractExtent(p2);
                result.extent3 = extractExtent(p3);






                //                for (const Eigen::Vector3f& p : allPoints)
                //                {
                //                    result.points.push_back(Armar6GraspProviderModule::Point {p(0), p(1), p(2)});
                //                }


                std::string labelstr = std::to_string(label);

                drawLine("ex1_" + labelstr, l1.Get(result.extent1.p05), l1.Get(result.extent1.p95), viz::Color::red(), layerForPC);
                drawLine("ex2_" + labelstr, l2.Get(result.extent2.p05), l2.Get(result.extent2.p95), viz::Color::green(), layerForPC);
                drawLine("ex3_" + labelstr, l3.Get(result.extent3.p05), l3.Get(result.extent3.p95), viz::Color::blue(), layerForPC);

                if (config.enableBoundigBoxesVisu)
                {
                    drawBox(labelstr + "_bbox", box.GetPose(), box.SizeVec(), viz::Color::blue(128), layerForPC);
                }

                Eigen::Vector3f preferredForwardTop(-1, 1, 0);
                Eigen::Vector3f preferredForwardSide(1, 0, 0);

                std::vector<float> signs = {1, -1};
                if (graspTypeStr == "TopBlob")
                {
                    signs = {1};
                }

                for (float fwdSign : signs)
                {
                    labelstr = std::to_string(label) + (fwdSign > 0 ? "_fwd" : "_rev");

                    GraspDescription grasp;
                    grasping::GraspCandidatePtr candidate = new grasping::GraspCandidate();
                    candidate->executionHints = new grasping::GraspCandidateExecutionHints();
                    candidate->sourceInfo = new grasping::GraspCandidateSourceInfo();
                    candidate->reachabilityInfo = new grasping::GraspCandidateReachabilityInfo();

                    Armar6GraspTrajectoryPtr graspTrajectory;
                    grasp.pose = Helpers::CreatePose(center, Eigen::Matrix3f::Identity());
                    if (graspType == Armar6GraspProviderModule::GraspType::Top)
                    {
                        grasp = calculateGraspPoseTop(result, fwdSign * preferredForwardTop, graspTypeStr);
                        candidate->executionHints->approach = grasping::TopApproach;
                        candidate->executionHints->graspTrajectoryName = "RightTopOpen";
                        //candidate->executionHints->graspTrajectoryPackage = "armar6_skills";
                        graspTrajectory = graspTrajectories.at("RightTopOpen");
                    }
                    else if (graspType == Armar6GraspProviderModule::GraspType::Side)
                    {
                        grasp = calculateGraspPoseSide(result, fwdSign * preferredForwardSide);
                        candidate->executionHints->approach = grasping::SideApproach;
                        candidate->executionHints->graspTrajectoryName = "RightSideOpen";
                        //candidate->executionHints->graspTrajectoryPackage = "armar6_skills";
                        graspTrajectory = graspTrajectories.at("RightSideOpen");
                    }
                    else
                    {
                        ARMARX_ERROR << "Unsupported grasp type";
                    }


                    graspTrajectory = graspTrajectory->getTransformedToGraspPose(grasp.pose);


                    std::map<std::string, float> fingerJointsVisu;
                    if (grasp.preshape == Armar6GraspProviderModule::PreshapeType::Open)
                    {
                        fingerJointsVisu = config.OpenVisu;
                        candidate->executionHints->preshape = grasping::OpenAperture;
                    }
                    else if (grasp.preshape == Armar6GraspProviderModule::PreshapeType::Preshaped)
                    {
                        fingerJointsVisu = config.PreshapeVisu;
                        candidate->executionHints->preshape = grasping::PreshapedAperture;
                    }
                    else
                    {
                        ARMARX_ERROR << "Unsupported preshape type";
                    }

                    result.preshape = grasp.preshape;
                    result.graspPose = new Pose(grasp.pose);
                    result.approach = new Vector3(grasp.approach);
                    result.side = "Right";
                    result.graspDirection = graspType;

                    candidate->robotPose = new Pose(localRobot->getGlobalPose());
                    candidate->graspPose = new Pose(grasp.pose);
                    candidate->groupNr = label;
                    candidate->providerName = getName();
                    candidate->side = "Right";
                    candidate->approachVector = new Vector3(grasp.approach);
                    candidate->graspSuccessProbability = 0.5f;
                    candidate->objectType = isKnownObject ? objpose::KnownObject : objpose::UnknownObject;
                    candidate->sourceFrame = "root";
                    candidate->targetFrame = "root";
                    candidate->sourceInfo->referenceObjectName = objectName;
                    candidate->sourceInfo->segmentationLabelID = label;
                    candidate->sourceInfo->bbox = new grasping::BoundingBox();
                    candidate->sourceInfo->bbox->center = new Vector3(box.GetCenter());
                    candidate->sourceInfo->bbox->ha1 = new Vector3(box.GetDir1());
                    candidate->sourceInfo->bbox->ha2 = new Vector3(box.GetDir2());
                    candidate->sourceInfo->bbox->ha3 = new Vector3(box.GetDir3());

                    SimpleDiffIK::Reachability reachability = graspTrajectory->calculateReachability(rns, tcp);

                    //ARMARX_IMPORTANT << labelstr << ": " << reachability.ikResults.at(0).posDiff.transpose();

                    candidate->reachabilityInfo->reachable = reachability.reachable;
                    candidate->reachabilityInfo->minimumJointLimitMargin = reachability.minimumJointLimitMargin;
                    candidate->reachabilityInfo->jointLimitMargins = ::math::Helpers::VectorToStd(reachability.jointLimitMargins);
                    candidate->reachabilityInfo->maxPosError = reachability.maxPosError;
                    candidate->reachabilityInfo->maxOriError = reachability.maxOriError;
                    candidates.push_back(candidate);


                    if (config.getEffectiveCandidatesVisu())
                    {
                        bool isReachable = candidate->reachabilityInfo->reachable;
                        viz::Color color = isReachable ? viz::Color::green() : config.colorNonReachableGrasps;

                        Eigen::Matrix4f tcp2handRoot = rnh->getRobotArm(RobotNameHelper::LocationRight, localRobot).getTCP()->getPoseInRootFrame().inverse()
                                                       * localRobot->getRobotNode("Hand R Root")->getPoseInRootFrame();

                        Eigen::Matrix4f graspPose = PosePtr::dynamicCast(candidate->graspPose)->toEigen();
                        std::string graspSide = "Right";
                        std::string modelFile = "armar6_rt/robotmodel/Armar6-SH/Armar6-" + graspSide + "Hand-v3.xml";
                        viz::Robot hand = viz::Robot(labelstr + "_grasp")
                                          .file("armar6_rt", modelFile)
                                          .pose(robotToGlobal * graspPose * tcp2handRoot)
                                          .overrideColor(color)
                                          .joints(fingerJointsVisu);
                        layerForPC.add(hand);
                    }


                    ssDebug << labelstr << " " << objectName << " (points=" << allPoints.size();
                    if (reachability.reachable)
                    {
                        ssDebug << ", jointMargin=" << reachability.minimumJointLimitMargin;
                    }
                    else
                    {
                        ssDebug << ", not reachable";
                    }

                    ssDebug << ") " << candidate->side << GraspTypeToString(graspType) << PreshapeTypeToString(grasp.preshape) << " " << ((int)center(0)) << "," << ((int)center(1)) << "," << ((int)center(2)) << std::endl;



                    resultList.push_back(result);
                }

                detectedObjects++;
            }
        }
    }
    else
    {
        // We did not get a point cloud but we should still generate grasps from ObjectPoseStorage and WorkingMemory
        RemoteRobot::synchronizeLocalCloneToTimestamp(localRobot, robotStateComponent, inputCloudPtr->header.stamp);
        robotToGlobal = localRobot->getGlobalPose();
    }


    if (useObjectPoseStorage)
    {
        // TODO: Reuse code
        objpose::ObjectPoseSeq objectPoses = getObjectPoses();
        for (const objpose::ObjectPose& objectPose : objectPoses)
        {
            std::optional<simox::OrientedBoxf> oobbRobot = objectPose.oobbRobot();
            if (!oobbRobot)
            {
                ARMARX_INFO << "Not generating grasp candidates for " << objectPose.objectID
                            << " because no bounding box was found";
                continue;
            }

            auto& id = objectPose.objectID;
            calculateGraspCandidatesForBB(objectPose,
                                          id.dataset() + "/" + id.className(),
                                          id.str(),
                                          rns, tcp,
                                          layerForOPO,
                                          candidates, resultList);

            detectedObjects++;
        }
    }

    if (useWorkingMemory)
    {
        // Get object instance channels from working memory
        std::vector<memoryx::ObjectInstanceWrapper> objects = objectInstanceSegment.queryObjects();
        for (memoryx::ObjectInstanceWrapper& object : objects)
        {
            // TODO: Get bounding box for objects
            VirtualRobot::ManipulationObjectPtr manipObj = object.manipulationObject;
            VirtualRobot::BoundingBox localBB = manipObj->getCollisionModel()->getTriMeshModel()->boundingBox;

            simox::OrientedBoxf oobb = simox::OrientedBoxf(0.5 * (localBB.getMin() + localBB.getMax()),
                                       Eigen::Quaternionf::Identity(),
                                       localBB.getMax() - localBB.getMin())
                                       .transformed(manipObj->getGlobalPose());

            /*
            calculateGraspCandidatesForBB(oobb,
                                          object.instanceInMemory->getMostProbableClass(),
                                          object.instanceInMemory->getId(),
                                          rns, tcp,
                                          layerForWM,
                                          candidates, resultList);
            */
            detectedObjects++;
        }

    }

    debugObserver->setDebugDatafield(getName(), "objects", new Variant(ssDebug.str()));
    staticPlotter->addPlot(getName() + "::ObjectSizes", plots);


    arviz.commit({layerForPC, layerForOPO, layerForWM});

    /*debugObserver->setDebugDatafield(getName(), "center_x", new Variant(center(0)));
    debugObserver->setDebugDatafield(getName(), "center_y", new Variant(center(1)));
    debugObserver->setDebugDatafield(getName(), "center_z", new Variant(center(2)));

    debugObserver->setDebugDatafield(getName(), "v1_x", new Variant(v1(0)));
    debugObserver->setDebugDatafield(getName(), "v1_y", new Variant(v1(1)));
    debugObserver->setDebugDatafield(getName(), "v1_z", new Variant(v1(2)));*/

    {
        std::unique_lock lock(resultMutex);
        this->resultList = resultList;
        processCount++;
        debugObserver->setDebugDatafield(getName(), "processCount", new Variant(processCount));
        debugObserver->setDebugDatafield(getName(), "detectedObjects", new Variant(detectedObjects));
    }
    ARMARX_VERBOSE << "Reporting grasp candidates";
    graspCandidatesTopic->reportGraspCandidates(getName(), candidates);


    //debugDrawerTopic->setArrowVisu(getName(), "v1", result.center, new Vector3(v1), DrawColor {1, 0, 0, 1}, 50, 3);
    //debugDrawerTopic->setArrowVisu(getName(), "v1_neg", result.center, new Vector3(Eigen::Vector3f(-v1)), DrawColor {1, 0, 0, 1}, 50, 3);
    //debugDrawerTopic->setArrowVisu(getName(), "v2", result.center, new Vector3(v2), DrawColor {0, 1, 0, 1}, 50, 3);
    //debugDrawerTopic->setArrowVisu(getName(), "v2_neg", result.center, new Vector3(Eigen::Vector3f(-v2)), DrawColor {0, 1, 0, 1}, 50, 3);


    if (outputCloudPtr->size() > 0)
    {
        provideResultPointClouds(outputCloudPtr, getName() + "Result");
    }
}

armarx::PropertyDefinitionsPtr Armar6GraspProvider::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new Armar6GraspProviderPropertyDefinitions(
            getConfigIdentifier()));
}


bool Armar6GraspProvider::BoundingBox::isInside(float x, float y, float z)
{
    return x >= x1 && x <= x2 &&
           y >= y1 && y <= y2 &&
           z >= z1 && z <= z2;
}




/*
CapturedPointSeq armarx::Armar6GraspProvider::getSegmentedPoints(const Ice::Current&)
{
    std::unique_lock lock(capturedPointsMutex);
    return capturedPoints;
}*/

StringVariantBaseMap Armar6GraspProvider::Config::getAsVariantMap()
{
    StringVariantBaseMap map;
    map["ObjectDepthPreshapeThreshold"] = new Variant(ObjectDepthPreshapeThreshold);
    map["ObjectHeightPreshapeThreshold"] = new Variant(ObjectHeightPreshapeThreshold);
    map["HandGraspingRadius"] = new Variant(HandGraspingRadius);
    return map;
}


void armarx::Armar6GraspProvider::requestService(const std::string& providerName, Ice::Int relativeTimeoutMS, const Ice::Current&)
{
    if (providerName == getName() || providerName == "AllGraspProviders")
    {
        serviceRequestTopic->requestService(getProperty<std::string>("serviceProviderName").getValue(), relativeTimeoutMS);
    }

}

void armarx::Armar6GraspProvider::setServiceConfig(const std::string& providerName, const armarx::StringVariantBaseMap& config, const Ice::Current&)
{
    if (providerName == "__any__GraspProvider__" || providerName == getName())
    {
        if (config.count("DisableVisuOverrideTimeoutSeconds") > 0)
        {
            float timeout = config.at("DisableVisuOverrideTimeoutSeconds")->getFloat();
            disableVisuUntil = TimeUtil::GetTime() + IceUtil::Time::secondsDouble(timeout);
            ARMARX_IMPORTANT << deactivateSpam(1) << "Disabling visu for " << timeout << " seconds.";
        }
    }
}

Armar6GraspProviderModule::ResultSeq armarx::Armar6GraspProvider::getResult(const Ice::Current&)
{
    std::unique_lock lock(resultMutex);
    return resultList;
}

Ice::Int Armar6GraspProvider::getProcessCount(const Ice::Current&)
{
    std::unique_lock lock(resultMutex);
    return processCount;
}


void Armar6GraspProvider::calculateGraspCandidatesForBB(
    const objpose::ObjectPose& objectPose,
    std::string const& className,
    std::string const& instanceName,
    VirtualRobot::RobotNodeSetPtr const& rns,
    VirtualRobot::RobotNodePtr const& tcp,
    viz::Layer& layer,
    grasping::GraspCandidateSeq& candidatesOut,
    Armar6GraspProviderModule::ResultSeq& resultListOut)
/*
void Armar6GraspProvider::calculateGraspCandidatesForBB(
        const simox::OrientedBoxf& oobbRobot,
        std::string const& className,
        std::string const& instanceName,
        VirtualRobot::RobotNodeSetPtr const& rns,
        VirtualRobot::RobotNodePtr const& tcp,
        viz::Layer& layer,
        grasping::GraspCandidateSeq& candidatesOut,
        Armar6GraspProviderModule::ResultSeq& resultListOut)
*/
{
    ARMARX_TRACE;
    std::optional<simox::OrientedBoxf> oobbRobotOpt = objectPose.oobbRobot();
    if (!oobbRobotOpt)
    {
        return;
    }
    const simox::OrientedBoxf oobbRobot = *oobbRobotOpt;

    auto determineGraspType = [&](const Box3D & box)
    {
        float height = box.Size3();
        float width = box.Size1();
        return width > height ? Armar6GraspProviderModule::GraspType::Top : Armar6GraspProviderModule::GraspType::Side;
    };

    Eigen::Matrix4f robotToGlobal = localRobot->getGlobalPose();

    auto drawLine = [&](std::string const & name, Eigen::Vector3f fromRobot, Eigen::Vector3f toRobot, viz::Color color,
                        viz::Layer & layer)
    {
        Eigen::Vector4f fromGlobal = robotToGlobal * fromRobot.homogeneous();
        Eigen::Vector4f toGlobal = robotToGlobal * toRobot.homogeneous();
        viz::Cylinder cyl = viz::Cylinder(name)
                            .fromTo(fromGlobal.head<3>(), toGlobal.head<3>())
                            .radius(3.0f)
                            .color(color);
        layer.add(cyl);
    };

    auto drawBox = [&](std::string const & name, Eigen::Matrix4f const & poseRobot, Eigen::Vector3f size, viz::Color color,
                       viz::Layer & layer)
    {
        Eigen::Matrix4f poseGlobal = robotToGlobal * poseRobot;
        viz::Box box = viz::Box(name)
                       .pose(poseGlobal)
                       .size(size)
                       .color(color);
        layer.add(box);
    };

    ARMARX_TRACE;
    Eigen::Vector3f bbCenter = oobbRobot.center();
    // We need to take half the extends
    Eigen::Vector3f bbExtend1 = 0.5f * oobbRobot.extend(0);
    Eigen::Vector3f bbExtend2 = 0.5f * oobbRobot.extend(1);
    Eigen::Vector3f bbExtend3 = 0.5f * oobbRobot.extend(2);

    Eigen::Vector3f v1 = bbExtend1.normalized();
    Eigen::Vector3f v2 = bbExtend2.normalized();
    Eigen::Vector3f v3 = bbExtend3.normalized();

    // We need to order the extends by size!
    sortBoxExtendsForGraspCandidateGeneration(v1, v2, v3,
            bbExtend1, bbExtend2, bbExtend3);

    Box3D box(bbCenter, bbExtend1, bbExtend2, bbExtend3);
    Eigen::Vector3f center = box.GetPoint(0, 0, 0);
    Armar6GraspProviderModule::GraspType graspType = determineGraspType(box);

    // TODO: Here we can add fixed grasp types in the config for specific object classes!
    std::string graspTypeStr = maptools::GetValueOrDefault(config.ObjectGraspTypeMap, className, "calculate");
    if (graspTypeStr == "TopBlob")
    {
        graspType = Armar6GraspProviderModule::GraspType::Top;
    }
    if (graspTypeStr == "Top")
    {
        graspType = Armar6GraspProviderModule::GraspType::Top;
    }
    if (graspTypeStr == "SideTop")
    {
        graspType = Armar6GraspProviderModule::GraspType::Side;
    }

    float length1 = box.GetDir1().norm();
    float length2 = box.GetDir2().norm();
    float length3 = box.GetDir3().norm();


    auto extractExtent = [&](float length)
    {
        Armar6GraspProviderModule::Extent extent;
        extent.min = -length;
        extent.max = length;
        extent.p05 = -0.90f * length;
        extent.p10 = -0.80f * length;
        extent.p90 = +0.80f * length;
        extent.p95 = +0.90f * length;
        return extent;
    };

    auto calculateGraspPoseTop = [&](const Armar6GraspProviderModule::Result & result, const Eigen::Vector3f & preferredForward, const std::string & graspTypeStr)
    {
        Eigen::Vector3f graspForwardVector = v2;

        Armar6GraspProviderModule::PreshapeType preshape = Armar6GraspProviderModule::PreshapeType::Open;


        //@@@ disabled for now
        //float height = result.extent3.p95 - result.extent3.p05;
        //float depth = result.extent2.p95 - result.extent2.p05;
        //if (depth < config.ObjectDepthPreshapeThreshold && height < config.ObjectHeightPreshapeThreshold)
        //{
        //    preshape = Armar6GraspProviderModule::PreshapeType::Preshaped;
        //}

        if (graspForwardVector.dot(preferredForward) < 0)
        {
            graspForwardVector = -graspForwardVector;
        }
        float angleV2 = Helpers::Angle(Eigen::Vector2f(graspForwardVector(0), graspForwardVector(1)));
        float angleGraspOri = Helpers::Angle(Eigen::Vector2f(0, 1));
        float rotationAngle = Helpers::AngleModPI(angleV2 - angleGraspOri);
        Eigen::Matrix3f referenceOrientation = config.ReferenceOrientationMap.at("RightTopOpen");
        Eigen::AngleAxisf aa = Eigen::AngleAxisf(rotationAngle, Eigen::Vector3f::UnitZ());
        Eigen::Matrix3f graspOri = aa.toRotationMatrix() * referenceOrientation;

        if (graspTypeStr == "TopBlob")
        {
            graspOri = config.ReferenceOrientationMap.at("RightTopBlob");
        }

        Eigen::Vector3f graspPos = center + v3 * result.extent3.p95 + graspOri * config.GraspOffsetTCP.at("Right");
        // FIXME: This is dependent on the camera!
        graspPos.y() -= 15.0f;



        GraspDescription grasp;
        grasp.pose = Helpers::CreatePose(graspPos, graspOri);
        grasp.approach = Eigen::Vector3f::UnitZ();
        grasp.preshape = preshape;

        return grasp;
    };

    auto calculateGraspPoseSide = [&](const Armar6GraspProviderModule::Result & result, const Eigen::Vector3f & preferredForward)
    {
        Eigen::Vector3f graspForwardVector = v1;
        Eigen::Vector3f offset_v1 = v1 * result.extent1.p95;

        if (graspForwardVector.dot(preferredForward) < 0)
        {
            graspForwardVector = -graspForwardVector;
            offset_v1 = v1 * result.extent1.p05;
        }

        float angleV1 = Helpers::Angle(Eigen::Vector2f(graspForwardVector(0), graspForwardVector(1)));
        float angleGraspOri = Helpers::Angle(Eigen::Vector2f(1, 0));
        float rotationAngle = Helpers::AngleModPI(angleV1 - angleGraspOri);
        Eigen::Matrix3f referenceOrientation = config.ReferenceOrientationMap.at("RightSideOpen");
        Eigen::AngleAxisf aa = Eigen::AngleAxisf(rotationAngle, Eigen::Vector3f::UnitZ());
        Eigen::Matrix3f graspOri = aa.toRotationMatrix() * referenceOrientation;

        Eigen::Vector3f graspPos = center + offset_v1;

        GraspDescription grasp;
        grasp.pose = Helpers::CreatePose(graspPos, graspOri);
        grasp.approach = graspForwardVector.normalized();
        grasp.preshape = Armar6GraspProviderModule::PreshapeType::Open;

        return grasp;
    };

    ARMARX_TRACE;

    Armar6GraspProviderModule::Result result;
    //result.inputPointCount = allPoints.size();
    result.center = new Vector3(center);
    result.v1 = new Vector3(v1);
    result.v2 = new Vector3(v2);
    result.v3 = new Vector3(v3);

    result.extent1 = extractExtent(length1);
    result.extent2 = extractExtent(length2);
    result.extent3 = extractExtent(length3);


    std::string label = instanceName;
    std::string labelstr = label;

    drawLine("ex1_" + labelstr, center - v1, center + v1, viz::Color::red(), layer);
    drawLine("ex2_" + labelstr, center - v2, center + v2, viz::Color::green(), layer);
    drawLine("ex3_" + labelstr, center - v3, center + v3, viz::Color::blue(), layer);

    if (config.enableBoundigBoxesVisu)
    {
        drawBox(labelstr + "_bbox", box.GetPose(), box.SizeVec(), viz::Color::blue(128), layer);
    }

    Eigen::Vector3f preferredForwardTop(-1, 1, 0);
    Eigen::Vector3f preferredForwardSide(1, 0, 0);

    std::vector<float> signs = {1, -1};
    if (graspTypeStr == "TopBlob")
    {
        signs = {1};
    }

    ARMARX_TRACE;
    for (float fwdSign : signs)
    {
        labelstr = label + (fwdSign > 0 ? "_fwd" : "_rev");

        GraspDescription grasp;
        grasping::GraspCandidatePtr candidate = new grasping::GraspCandidate();
        candidate->executionHints = new grasping::GraspCandidateExecutionHints();
        candidate->sourceInfo = new grasping::GraspCandidateSourceInfo();
        candidate->reachabilityInfo = new grasping::GraspCandidateReachabilityInfo();

        Armar6GraspTrajectoryPtr graspTrajectory;
        grasp.pose = Helpers::CreatePose(center, Eigen::Matrix3f::Identity());
        if (graspType == Armar6GraspProviderModule::GraspType::Top)
        {
            grasp = calculateGraspPoseTop(result, fwdSign * preferredForwardTop, graspTypeStr);
            candidate->executionHints->approach = grasping::TopApproach;
            candidate->executionHints->graspTrajectoryName = "RightTopOpen";
            //candidate->executionHints->graspTrajectoryPackage = "armar6_skills";
            graspTrajectory = graspTrajectories.at("RightTopOpen");
        }
        else if (graspType == Armar6GraspProviderModule::GraspType::Side)
        {
            grasp = calculateGraspPoseSide(result, fwdSign * preferredForwardSide);
            candidate->executionHints->approach = grasping::SideApproach;
            candidate->executionHints->graspTrajectoryName = "RightSideOpen";
            //candidate->executionHints->graspTrajectoryPackage = "armar6_skills";
            graspTrajectory = graspTrajectories.at("RightSideOpen");
        }
        else
        {
            ARMARX_ERROR << "Unsupported grasp type";
        }


        graspTrajectory = graspTrajectory->getTransformedToGraspPose(grasp.pose);


        std::map<std::string, float> fingerJointsVisu;
        if (grasp.preshape == Armar6GraspProviderModule::PreshapeType::Open)
        {
            fingerJointsVisu = config.OpenVisu;
            candidate->executionHints->preshape = grasping::OpenAperture;
        }
        else if (grasp.preshape == Armar6GraspProviderModule::PreshapeType::Preshaped)
        {
            fingerJointsVisu = config.PreshapeVisu;
            candidate->executionHints->preshape = grasping::PreshapedAperture;
        }
        else
        {
            ARMARX_ERROR << "Unsupported preshape type";
        }

        ARMARX_TRACE;
        result.preshape = grasp.preshape;
        result.graspPose = new Pose(grasp.pose);
        result.approach = new Vector3(grasp.approach);
        result.side = "Right";
        result.graspDirection = graspType;

        ARMARX_TRACE;
        candidate->robotPose = new Pose(objectPose.robotPose); //localRobot->getGlobalPose());
        candidate->graspPose = new Pose(grasp.pose);
        // TODO: What should we put into the group number?
        // TODO: Make a member variable for keeping track of groupNrs for object instances
        candidate->groupNr = -1;
        candidate->providerName = getName();
        candidate->side = "Right";
        candidate->approachVector = new Vector3(grasp.approach);
        candidate->graspSuccessProbability = 0.5f;
        candidate->objectType = objpose::KnownObject;
        candidate->sourceFrame = "root";
        candidate->targetFrame = "root";
        // We put the complete object ID into referenceObjectName (e.g. KIT/Amicelli/0)
        candidate->sourceInfo->referenceObjectName = instanceName;
        candidate->sourceInfo->segmentationLabelID = -1;
        candidate->sourceInfo->bbox = new grasping::BoundingBox();
        candidate->sourceInfo->bbox->center = new Vector3(box.GetCenter());
        candidate->sourceInfo->bbox->ha1 = new Vector3(box.GetDir1());
        candidate->sourceInfo->bbox->ha2 = new Vector3(box.GetDir2());
        candidate->sourceInfo->bbox->ha3 = new Vector3(box.GetDir3());

        SimpleDiffIK::Reachability reachability = graspTrajectory->calculateReachability(rns, tcp);

        //ARMARX_IMPORTANT << labelstr << ": " << reachability.ikResults.at(0).posDiff.transpose();

        candidate->reachabilityInfo->reachable = reachability.reachable;
        candidate->reachabilityInfo->minimumJointLimitMargin = reachability.minimumJointLimitMargin;
        candidate->reachabilityInfo->jointLimitMargins = ::math::Helpers::VectorToStd(reachability.jointLimitMargins);
        candidate->reachabilityInfo->maxPosError = reachability.maxPosError;
        candidate->reachabilityInfo->maxOriError = reachability.maxOriError;
        candidatesOut.push_back(candidate);

        ARMARX_TRACE;
        if (config.getEffectiveCandidatesVisu())
        {
            bool isReachable = candidate->reachabilityInfo->reachable;
            viz::Color color = isReachable ? viz::Color::green() : config.colorNonReachableGrasps;

            Eigen::Matrix4f tcp2handRoot = rnh->getRobotArm(RobotNameHelper::LocationRight, localRobot).getTCP()->getPoseInRootFrame().inverse()
                                           * localRobot->getRobotNode("Hand R Root")->getPoseInRootFrame();

            Eigen::Matrix4f graspPose = PosePtr::dynamicCast(candidate->graspPose)->toEigen();
            std::string graspSide = "Right";
            std::string modelFile = "armar6_rt/robotmodel/Armar6-SH/Armar6-" + graspSide + "Hand-v3.xml";
            viz::Robot hand = viz::Robot(labelstr + "_grasp")
                              .file("armar6_rt", modelFile)
                              .pose(objectPose.robotPose * graspPose * tcp2handRoot)
                              .overrideColor(color)
                              .joints(fingerJointsVisu);
            layer.add(hand);
        }

        ARMARX_TRACE;
        resultListOut.push_back(result);
    }
}

ARMARX_DECOUPLED_REGISTER_COMPONENT(armarx::Armar6GraspProvider);
