/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::DoorHandleLocalizer
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VisionX/components/pointcloud_core/PointCloudAndImageProcessor.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
#include <ArmarXGui/libraries/RemoteGui/Client/Tab.h>
#include <ArmarXGui/libraries/RemoteGui/Client/Widgets.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/Component.h>

#include <filesystem>
#include <mutex>

namespace armarx
{
    struct DoorHandleLocalizerParams
    {
        std::filesystem::path recordingPath;
        std::string inputProviderName;
        std::string resultProviderName;
    };

    using namespace armarx::RemoteGui::Client;
    struct DoorHandleLocalizerTab : Tab
    {
        Button buttonSaveState;
    };

    /**
     * @class DoorHandleLocalizerPropertyDefinitions
     * @brief Property definitions of `DoorHandleLocalizer`.
     */
    class DoorHandleLocalizerPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        DoorHandleLocalizerPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-DoorHandleLocalizer DoorHandleLocalizer
     * @ingroup armar6_skills-Components
     * A description of the component DoorHandleLocalizer.
     *
     * @class DoorHandleLocalizer
     * @ingroup Component-DoorHandleLocalizer
     * @brief Brief description of class DoorHandleLocalizer.
     *
     * Detailed description of class DoorHandleLocalizer.
     */
    class DoorHandleLocalizer
        : virtual public visionx::PointCloudAndImageProcessor
        , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        , virtual public armarx::ArVizComponentPluginUser
        , virtual public armarx::RobotStateComponentPluginUser
    {
    public:
        std::string getDefaultName() const override;

        void RemoteGui_update() override;

        void onInitPointCloudAndImageProcessor() override;
        void onConnectPointCloudAndImageProcessor() override;
        void onDisconnectPointCloudAndImageProcessor() override;
        void onExitPointCloudAndImageProcessor() override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void process() override;

    private:
        void createRemoteGuiTab();

        void saveState();

    private:
        /// Debug observer. Used to visualize e.g. time series.
        armarx::DebugObserverInterfacePrx debugObserver;

        DoorHandleLocalizerParams params;

        DoorHandleLocalizerTab tab;
        VirtualRobot::RobotPtr robot;
        std::string referenceFrameName;

        using PointCloud = pcl::PointCloud<pcl::PointXYZRGBA>;
        PointCloud::Ptr inputPointCloud;
        PointCloud::Ptr resultPointCloud;

        CByteImage* inputImages[2];
        CByteImage* resultImages[2];

        std::mutex saveStateMutex;
        std::filesystem::path saveStatePath;
        std::string saveStateTime;
        bool saveStateRequested = false;
    };
}
