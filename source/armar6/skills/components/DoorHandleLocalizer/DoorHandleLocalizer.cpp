/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::DoorHandleLocalizer
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DoorHandleLocalizer.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include <VisionX/tools/ImageUtil.h>

#include <SimoxUtility/json/json.hpp>
#include <SimoxUtility/json/eigen_conversion.h>
#include <SimoxUtility/json/io.h>

#include <pcl/io/pcd_io.h>

namespace armarx
{
    ARMARX_DECOUPLED_REGISTER_COMPONENT(DoorHandleLocalizer);

    DoorHandleLocalizerPropertyDefinitions::DoorHandleLocalizerPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        //defineRequiredProperty<std::string>("PropertyName", "Description");
        //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");

        defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver",
                                            "Name of the topic the DebugObserver listens to.");

        defineOptionalProperty<std::string>("RecordingsPath", "armar6_skills/door-handle-recordings", "Path to save the recordings to");

        defineOptionalProperty<std::string>("providerName", "OpenNIPointCloudProvider", "Name of the image provider that should be used");
        defineOptionalProperty<std::string>("ResultProviderName", "DoorHandleLocalizerResult", "Name of the image and point cloud result provider");
    }


    std::string DoorHandleLocalizer::getDefaultName() const
    {
        return "DoorHandleLocalizer";
    }


    void DoorHandleLocalizer::onInitPointCloudAndImageProcessor()
    {
        offeringTopicFromProperty("DebugObserverName");

        std::string relativeRecordingPath = getProperty<std::string>("RecordingsPath").getValue();
        std::string absoluteRecordingPath = relativeRecordingPath;
        if (!ArmarXDataPath::getAbsolutePath(relativeRecordingPath, absoluteRecordingPath))
        {
            ARMARX_WARNING << "Could not get absolute path from relative path: " << relativeRecordingPath;
        }
        params.recordingPath = absoluteRecordingPath;


        params.inputProviderName = getProperty<std::string>("providerName").getValue();
        params.resultProviderName = getProperty<std::string>("ResultProviderName").getValue();

        usingImageProvider(params.inputProviderName);
        usingPointCloudProvider(params.inputProviderName);
    }


    void DoorHandleLocalizer::onConnectPointCloudAndImageProcessor()
    {
        // Get topics and proxies here. Pass the *InterfacePrx type as template argument.
        getTopicFromProperty(debugObserver, "DebugObserverName");

        {
            visionx::ImageProviderInfo imageProviderInfo = getImageProvider(params.inputProviderName);

            referenceFrameName = "root";
            if (auto refFrameProvider = visionx::ReferenceFrameInterfacePrx::checkedCast(imageProviderInfo.proxy))
            {
                referenceFrameName = refFrameProvider->getReferenceFrame();
            }
            ARMARX_INFO << "Using reference frame: " << referenceFrameName;

            if (imageProviderInfo.numberImages != 2)
            {
                ARMARX_WARNING << "Expected 2 input images (RGB and depth)";
                throw armarx::LocalException() << "Expected 2 input images (RGB and depth), "
                                               << "but got: " << imageProviderInfo.numberImages;
            }

            for (int i = 0 ; i < 2; i++)
            {
                inputImages[i] = visionx::tools::createByteImage(imageProviderInfo);
                resultImages[i] = inputImages[i];
            }

            visionx::ImageDimension dimension(resultImages[0]->width, resultImages[0]->height);

            int pointCount = dimension.width * dimension.height;
            inputPointCloud.reset(new PointCloud);
            inputPointCloud->points.reserve(pointCount);
            resultPointCloud.reset(new PointCloud);
            resultPointCloud->points.reserve(pointCount);

            enableResultImagesAndPointClouds<pcl::PointXYZRGBA>(
                params.resultProviderName, 2, dimension, visionx::eRgb);
        }

        robot = addRobot("Armar6", VirtualRobot::RobotIO::eStructure);

        createRemoteGuiTab();
        RemoteGui_startRunningTask();
    }


    void DoorHandleLocalizer::onDisconnectPointCloudAndImageProcessor()
    {

    }


    void DoorHandleLocalizer::onExitPointCloudAndImageProcessor()
    {

    }




    armarx::PropertyDefinitionsPtr DoorHandleLocalizer::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new DoorHandleLocalizerPropertyDefinitions(
                getConfigIdentifier()));
    }

    void DoorHandleLocalizer::process()
    {
        if (!waitForImages(params.inputProviderName))
        {
            ARMARX_WARNING << "Timeout while waiting for images";
            return;
        }

        if (!waitForPointClouds(params.inputProviderName))
        {
            ARMARX_WARNING << "Timeout while waiting for pointcloud";
            return;
        }

        if (getImages(inputImages) != 2)
        {
            ARMARX_WARNING << "Unable to transfer or read images";
            return;
        }

        if (!getPointClouds(inputPointCloud))
        {
            ARMARX_WARNING << "Unable to transfer or read point cloud";
            return;
        }

        ARMARX_IMPORTANT << "Got images and point cloud";

        std::filesystem::path savePath;
        {
            std::unique_lock lock(saveStateMutex);
            if (saveStateRequested)
            {
                savePath = saveStatePath;
                saveStateRequested = false;
            }
        }
        if (!savePath.empty())
        {
            // Save the input image and point cloud to the directory
            ARMARX_IMPORTANT << "Trying to save to directory: " << savePath;

            std::filesystem::path pcdPath = savePath / "pointcloud.pcd";
            pcl::io::savePCDFile<pcl::PointXYZRGBA>(pcdPath.string(), *inputPointCloud);

            std::filesystem::path rgbPath = savePath / "rgb.bmp";
            inputImages[0]->SaveToFile(rgbPath.string().c_str());
            std::filesystem::path depthPath = savePath / "depth.bmp";
            inputImages[1]->SaveToFile(depthPath.string().c_str());
        }

        for (int i = 0 ; i < 2; i++)
        {
            resultImages[i] = inputImages[i];
        }

        *resultPointCloud = *inputPointCloud;

        provideResultImages(resultImages);
        provideResultPointClouds(params.resultProviderName, resultPointCloud);
    }

    void DoorHandleLocalizer::createRemoteGuiTab()
    {
        tab.buttonSaveState.setLabel("Save State");

        VBoxLayout root = {tab.buttonSaveState, VSpacer()};

        RemoteGui_createTab("DoorHandleLocalizer", root, &tab);
    }

    void DoorHandleLocalizer::RemoteGui_update()
    {
        if (tab.buttonSaveState.wasClicked())
        {
            saveState();
        }
    }

    void DoorHandleLocalizer::saveState()
    {
        ARMARX_IMPORTANT << "Saving state...";

        {
            std::unique_lock lock(saveStateMutex);
            saveStateRequested = true;
            saveStateTime = IceUtil::Time::now().toString("%Y-%m-%d %H-%M-%S");
            saveStatePath = params.recordingPath / saveStateTime;
            std::filesystem::create_directory(saveStatePath);
        }

        synchronizeLocalClone(robot);

        std::filesystem::path robotPath = saveStatePath / "robot.json";

        nlohmann::json data;

        auto& robotData = data["robot"];
        robotData["global_pose"] = robot->getGlobalPose();
        robotData["joint_configuration"] = robot->getConfig()->getRobotNodeJointValueMap();

        data["reference_frame"] = referenceFrameName;

        nlohmann::write_json(robotPath.string(), data, 2);
    }
}
