#pragma once

namespace armarx
{
    // Additional offsets for configuration via GUI
    const float defaultHandYOffset = 0;
    const float defaultHandDistanceXOffset = 0;

    // Hard-coded good parameters
    const float minimumHandDistance = 250.0f;
    const float standardHandYOffset = 22.0f;
    const float standardHandZOffset = 82.0f;
}
