/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/math/Line.h>
#include <VirtualRobot/math/SimpleAbstractFunctionR2R3.h>
#include <VirtualRobot/math/Helpers.h>
#include <VirtualRobot/math/Plane.h>

#include <memory>
#include <cfloat>

#ifndef M_PI_F
#define M_PI_F (static_cast<float>(M_PI))
#endif

namespace math
{
    typedef std::shared_ptr<class FitRectangle> FitRectanglePtr;

    // 3D Box. u: -1 .. 1, v: -1 .. 1, w: -1 .. 1 center at u=v=w=0
    class Box3D
    {
    public:
        Box3D(Eigen::Vector3f p, Eigen::Vector3f v1, Eigen::Vector3f v2, Eigen::Vector3f v3)
            : p(p), v1(v1), v2(v2), v3(v3)
        { }

        Eigen::Vector3f GetPoint(float u, float v, float w) const
        {
            return p + u * v1 + v * v2 + w * v3;
        }
        Eigen::Vector3f GetCenter()
        {
            return p;
        }
        Eigen::Matrix3f GetOrientation()
        {
            return math::Helpers::CreateOrientation(v1.normalized(), v2.normalized(), v3.normalized());
        }
        Eigen::Matrix4f GetPose()
        {
            return math::Helpers::CreatePose(p, GetOrientation());
        }

        float Volume() const
        {
            return Size1() * Size2() * Size3();
        }
        float Size1() const
        {
            return v1.norm() * 2;
        }
        float Size2() const
        {
            return v2.norm() * 2;
        }
        float Size3() const
        {
            return v3.norm() * 2;
        }
        Eigen::Vector3f SizeVec()
        {
            return Eigen::Vector3f(Size1(), Size2(), Size3());
        }

        Eigen::Vector3f GetDir1() const
        {
            return v1;
        }
        Eigen::Vector3f GetDir2() const
        {
            return v2;
        }
        Eigen::Vector3f GetDir3() const
        {
            return v3;
        }

        Box3D SwapDirections() const
        {
            return Box3D(p, v2, -v1, v3);
        }

    private:
        Eigen::Vector3f p;
        Eigen::Vector3f v1;
        Eigen::Vector3f v2;
        Eigen::Vector3f v3;

    };

    class Percentile
    {
    public:
        Percentile(const std::vector<float>& vals)
        {
            this->vals = vals;
            std::sort(this->vals.begin(), this->vals.end());
        }

        float get(float f) const
        {
            return vals.at((int)((vals.size() - 1) * f));
        }

        std::vector<float> vals;
    };

    class FitRectangle
    {
    public:
        int angleSteps = 18;
        float percentile = 0.05f;
        std::vector<Box3D> results;


        FitRectangle(int angleSteps = 18, float percentile = 0.05f)
            : angleSteps(angleSteps), percentile(percentile)
        {

        }
        FitRectangle(const FitRectangle& other) = default;

        static std::vector<float> ProjectPoints(const Line& line, const std::vector<Eigen::Vector3f> points)
        {
            std::vector<float> projected;
            for (const Eigen::Vector3f& p : points)
            {
                projected.push_back(line.GetT(p));
            }
            return projected;
        }

        static Eigen::Vector3f CalculateCenter(const std::vector<Eigen::Vector3f> points)
        {
            Eigen::Vector3f center = Eigen::Vector3f::Zero();
            for (const Eigen::Vector3f& p : points)
            {
                center += p;
            }
            return center / points.size();
        }

        Box3D Fit(const std::vector<Eigen::Vector3f> points)
        {
            results.clear();
            Eigen::Vector3f center = CalculateCenter(points);

            Line l3(center, Eigen::Vector3f::UnitZ());
            Percentile p3(ProjectPoints(l3, points));

            Box3D minBox(center, Eigen::Vector3f::Zero(), Eigen::Vector3f::Zero(), Eigen::Vector3f::Zero());
            float minVolume = FLT_MAX;

            for (int i = 0; i < angleSteps; i++)
            {
                float angle = M_PI_F / 2 * i / angleSteps;
                Line l1(center, Helpers::CartesianFromCylinder(1, angle, 0));
                Line l2(center, Helpers::CartesianFromCylinder(1, angle + M_PI_F / 2, 0));
                Percentile p1(ProjectPoints(l1, points));
                Percentile p2(ProjectPoints(l2, points));

                float v1_lo = p1.get(percentile);
                float v1_hi = p1.get(1 - percentile);
                float v2_lo = p2.get(percentile);
                float v2_hi = p2.get(1 - percentile);
                float v3_lo = p3.get(percentile);
                float v3_hi = p3.get(1 - percentile);

                float v1_mid = (v1_lo + v1_hi) / 2;
                float v2_mid = (v2_lo + v2_hi) / 2;
                float v3_mid = (v3_lo + v3_hi) / 2;


                Box3D box(center + l1.Dir() * v1_mid + l2.Dir() * v2_mid + l3.Dir() * v3_mid,
                          l1.Dir() * (v1_hi - v1_mid),
                          l2.Dir() * (v2_hi - v2_mid),
                          l3.Dir() * (v3_hi - v3_mid));
                results.push_back(box);
                float volume = box.Volume();
                if (volume < minVolume)
                {
                    minBox = box;
                    minVolume = volume;
                }
            }
            if (minBox.GetDir1().norm() < minBox.GetDir2().norm())
            {
                minBox = minBox.SwapDirections();
            }
            return minBox;
        }

    private:
    };
}
