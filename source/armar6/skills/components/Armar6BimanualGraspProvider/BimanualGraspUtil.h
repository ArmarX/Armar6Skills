#ifndef BIMANUALGRASPUTIL_H
#define BIMANUALGRASPUTIL_H

#include <vector>
#include "Armar6BimanualGraspProvider.h"


// danger zone (includes from .h file)

#include <ArmarXCore/core/Component.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>
#include <HRPGroup4/interface/Armar6BimanualGraspProviderInterface.h>
#include <RobotAPI/interface/units/GraspCandidateProviderInterface.h>

#include <VirtualRobot/Robot.h>

#include <RobotAPI/interface/core/RobotState.h>



#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>

#include <armar6/skills/libraries/Armar6Tools/Armar6GraspTrajectory.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/interface/StaticPlotterInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>

#include <VirtualRobot/math/Line.h>

//

// danger zone 2 (includes from .cpp file)

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <RobotAPI/libraries/core/math/MathUtils.h>
#include <RobotAPI/libraries/core/math/MatrixHelpers.h>
#include <RobotAPI/libraries/core/math/SVD.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>
#include <ArmarXGui/libraries/StructuralJson/JPathNavigator.h>
#include <ArmarXGui/libraries/StructuralJson/StructuralJsonParser.h>
#include <RobotAPI/libraries/SimpleJsonLogger/SimpleJsonLoggerEntry.h>

#include <VirtualRobot/math/Helpers.h>
#include <cmath>
#include "FitRectangle.h"

#include <RobotAPI/libraries/core/SimpleDiffIK.h>

#include <armar6/skills/libraries/Armar6Tools/Armar6HandV2Helper.h>


//



// using namespace armarx::math;
using namespace math;
using namespace armarx;




namespace maptools
{
    template<typename TKey, typename TValue>
    bool TryGetValue(const std::map<TKey, TValue>& map, const TKey& key, TValue& outVal)
    {
        typename std::map<TKey, TValue>::const_iterator it = map.find(key);
        if (it == map.end())
        {
            return false;
        }
        outVal = it->second;
        return true;
    }

    template<typename TKey, typename TValue, typename TDefaultValue>
    TValue GetValueOrDefault(const std::map<TKey, TValue>& map, const TKey& key, const TDefaultValue& defaultVal)
    {
        typename std::map<TKey, TValue>::const_iterator it = map.find(key);
        if (it == map.end())
        {
            return defaultVal;
        }
        return it->second;
    }

    template<typename TKey, typename TValue>
    bool HasKey(const std::map<TKey, TValue>& map, const TKey& key)
    {
        return map.count(key) > 0;
    }
}

#endif // BIMANUALGRASPUTIL_H
