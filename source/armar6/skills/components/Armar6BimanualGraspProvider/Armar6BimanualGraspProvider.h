/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::Armar6BimanualGraspProvider
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>
#include <ArmarXGui/libraries/StructuralJson/JPathNavigator.h>
#include <ArmarXGui/libraries/StructuralJson/StructuralJsonParser.h>

#include <ArmarXCore/core/Component.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>
#include <armar6/skills/interface/Armar6BimanualGraspProviderInterface.h>
#include <RobotAPI/interface/units/GraspCandidateProviderInterface.h>

#include <VirtualRobot/Robot.h>

#include <RobotAPI/interface/core/RobotState.h>

#include <pcl/point_types.h>

#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/passthrough.h>

#include <armar6/skills/libraries/Armar6Tools/Armar6GraspTrajectory.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/interface/StaticPlotterInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>
#include <RobotAPI/interface/observers/GraspCandidateObserverInterface.h>

#include <VirtualRobot/math/Line.h>
#include <armar6/skills/components/Armar6BimanualGraspProvider/Constants.h>

#include <mutex>

namespace armarx
{

    namespace Armar6BimanualGraspProviderModule
    {
        struct Extent
        {
            float min;
            float max;
            float p05;
            float p10;
            float p90;
            float p95;
        };

        enum GraspType
        {
            Top, Side
        };
        /*enum PreshapeType
        {
            Open, Preshaped
        };*/

        struct Result
        {
            Eigen::Vector3f center;
            Eigen::Vector3f v1;
            Eigen::Vector3f v2;
            Eigen::Vector3f v3;
            Extent extent1;
            Extent extent2;
            Extent extent3;
            int inputPointCount;
            //std::vector<Eigen::Vect points;
            int labelNr;
            Eigen::Vector3f approach;
            GraspType graspDirection;
            //PreshapeType preshape;
            std::string side;
            Eigen::Matrix4f graspPose;
        };

        //sequence<Result> ResultSeq;
    }

    /**
     * @class Armar6BimanualGraspProviderPropertyDefinitions
     * @brief
     */
    class Armar6BimanualGraspProviderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        Armar6BimanualGraspProviderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the debug observer that should be used");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the debug drawer topic that should be used");
            defineOptionalProperty<std::string>("StaticPlotterName", "StaticPlotter", "Name of the static plotter that should be used");

            defineOptionalProperty<std::string>("GraspCandidatesTopicName", "GraspCandidatesTopic", "Name of the Grasp Candidate Topic");
            defineOptionalProperty<std::string>("ConfigTopicName", "GraspCandidateProviderConfigTopic", "Name of the Grasp Candidate Provider Config Topic");

            defineOptionalProperty<std::string>("PointCloudFormat", "XYZRGBL", "Format of the input and output point cloud (XYZRGBA, XYZL, XYZRGBL)");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
            defineOptionalProperty<std::string>("providerName", "TabletopSegmentationResult", "name of the point cloud provider");
            defineOptionalProperty<std::string>("serviceProviderName", "TabletopSegmentation", "name of the service provider");
            defineOptionalProperty<std::string>("sourceFrameName", "root", "The source frame name");

            defineOptionalProperty<uint32_t>("BackgroundLabelId", 1, "Label in the pointcloud for the plane or background.", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<bool>("UseObjectIdMap", false, "Use ID map from config for named objects");

            defineOptionalProperty<float>("DownsamplingLeafSize", 5.f, "If >0, the pointcloud is downsampled with a leaf size of this value. In mm.", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<size_t>("MininumClusterPointCount", 100, "Minimum number of points per cluster/segmented object to be further processed.", PropertyDefinitionBase::eModifiable);

            /*defineOptionalProperty<float>("BoundingBoxZ1", 975.f, "");
            defineOptionalProperty<float>("BoundingBoxY1", 0.f, "");
            defineOptionalProperty<float>("BoundingBoxY2", 1300.f, "");

            defineOptionalProperty<float>("BoundingBoxX1", -200, "");
            defineOptionalProperty<float>("BoundingBoxX2", 200, "");*/

            defineOptionalProperty<bool>("enablePointCloudsVisu", true, "");
            defineOptionalProperty<bool>("enableVisu", true, "");
            defineOptionalProperty<bool>("enableCandidatesVisu", true, "");
            defineOptionalProperty<bool>("enableBoundigBoxesVisu", false, "");
            defineOptionalProperty<bool>("FlattenInZ", true, "");

            defineOptionalProperty<std::string>("RemoteGuiName", "RemoteGuiProvider", "Name of the remote gui");
            ARMARX_INFO << "Before observer property";
            defineOptionalProperty<std::string>("GraspCandidateObserverName", "GraspCandidateObserver", "Name of the grasp candidate observer");
            ARMARX_INFO << "Before observer property";
        }
    };



    /**
     * @defgroup Component-Armar6BimanualGraspProvider Armar6BimanualGraspProvider
     * @ingroup armar6_skills-Components
     * A description of the component Armar6BimanualGraspProvider.
     *
     * @class Armar6BimanualGraspProvider
     * @ingroup Component-Armar6BimanualGraspProvider
     * @brief Brief description of class Armar6BimanualGraspProvider.
     *
     * Detailed description of class Armar6BimanualGraspProvider.
     */
    class Armar6BimanualGraspProvider :
        virtual public armarx::Armar6BimanualGraspProviderInterface,
        virtual public visionx::PointCloudProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "Armar6BimanualGraspProvider";
        }

        void makeSlider(RemoteGui::detail::VBoxLayoutBuilder& rootLayoutBuilder, const std::string& name, int min, int max, float val);
        void makeCheckBox(RemoteGui::detail::HBoxLayoutBuilder& checkBoxLayoutBuilder, const std::string& name, bool val);



        struct BoundingBox
        {
            float x1 = -10000;
            float x2 = 10000;
            float y1 = -10000;
            float y2 = 10000;
            float z1 = -10000;
            float z2 = 10000;

            bool isInside(float x, float y, float z);
        };



        struct Config
        {
            std::map<std::string, Eigen::Matrix3f> ReferenceOrientationMap;
            float ObjectDepthPreshapeThreshold;
            float ObjectHeightPreshapeThreshold;
            float HandGraspingRadius;
            std::map<std::string, Eigen::Vector3f> GraspOffsetTCP;
            std::map<std::string, float> PreshapeVisu;
            std::map<std::string, float> OpenVisu;
            std::map<int, std::string> ObjectIdMap;
            std::map<std::string, std::string> ObjectGraspTypeMap;
            bool UseObjectIdMap;

            StringVariantBaseMap getAsVariantMap();
            IceUtil::Time disablePointCloudVisuUntil = IceUtil::Time::seconds(0);
            IceUtil::Time disableCandidatesVisuUntil = IceUtil::Time::seconds(0);

            bool enableVisu;
            bool enablePointCloudsVisu;
            bool enableCandidatesVisu;
            bool enableBoundigBoxesVisu;

            bool getEffectivePointCloudVisu()
            {
                return enablePointCloudsVisu && IceUtil::Time::now() > disablePointCloudVisuUntil;
            }
            bool getEffectiveCandidatesVisu()
            {
                return enableCandidatesVisu && IceUtil::Time::now() > disableCandidatesVisuUntil;
            }

        };



        struct GraspDescription
        {
            Eigen::Matrix4f pose;
            Eigen::Vector3f approach;
            Eigen::Vector3f inwards;
            std::string graspName;
            //Armar6BimanualGraspProviderModule::PreshapeType preshape;
        };

        static std::string GraspTypeToString(Armar6BimanualGraspProviderModule::GraspType graspType);
        //static std::string PreshapeTypeToString(Armar6BimanualGraspProviderModule::PreshapeType preshapeType);

        void setShowInvertedHands(bool value, const Ice::Current&) override;
        void setShowLongAndShortSide(bool value, const Ice::Current&) override;
        void setShowDiagonal(bool value, const Ice::Current&) override;

        void setHandOffsetY(float value, const Ice::Current&) override;
        void setHandDistanceOffsetX(float value, const Ice::Current&) override;

        void setVisualization(bool showAllGrasps, const std::string& chosenGraspName, const Ice::Current&) override;

    protected:
        /**
         * @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
         */
        void onInitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
         */
        void onConnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
         */
        void onDisconnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
         */
        void onExitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::process()
         */
        void process() override;

        void processPointCloud();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


        // GraspCandidateProviderInterface interface
    public:
        void requestService(const std::string& providerName, Ice::Int relativeTimeoutMS, const Ice::Current&);// override;
        void setServiceConfig(const std::string& providerName, const StringVariantBaseMap& config, const Ice::Current&);// override;

        // Armar6BimanualGraspProviderInterface interface
    public:
        //Armar6BimanualGraspProviderModule::ResultSeq getResult(const Ice::Current&) override;
        //Ice::Int getProcessCount(const Ice::Current&) override;


    private:
        void readConfig();
        std::map<std::string, Eigen::Matrix3f> readQuaternionMap(const RapidXmlReaderNode& node);
        std::map<int, std::string> readObjectIdMap(const RapidXmlReaderNode& node);
        std::map<std::string, std::string> readObjectGraspTypeMap(const RapidXmlReaderNode& node);
        std::map<std::string, Eigen::Vector3f> readVectorMap(const RapidXmlReaderNode& node);
        std::map<std::string, float> readPreshapeVisu(const RapidXmlReaderNode& node);
        void calculateReferenceOrientations();
        grasping::ProviderInfoPtr getProviderInfo();

    private:
        std::mutex resultMutex;
        std::mutex capturedPointsMutex;
        std::string pointCloudFormat;

        VirtualRobot::RobotPtr localRobot;

        armarx::RobotStateComponentInterfacePrx robotStateComponent;
        DebugObserverInterfacePrx debugObserver;
        DebugDrawerInterfacePrx debugDrawerTopic;
        StaticPlotterInterfacePrx staticPlotter;


        std::string sourceFrameName;
        std::string providerName;

        BoundingBox boundingBox;
        bool flattenInZ;

        //std::vector<Armar6BimanualGraspProviderModule::Result> resultList;
        //Armar6BimanualGraspProviderModule::ResultSeq resultList;
        int processCount;
        //
        //CapturedPointSeq capturedPoints;

        DebugDrawerHelperPtr debugDrawerHelper;

        RemoteGuiInterfacePrx remoteGuiPrx;

        SimplePeriodicTask<>::pointer_type guiTask;
        SimplePeriodicTask<>::pointer_type reportConfigTask;

        RemoteGui::TabProxy guiTab;

        RobotNameHelperPtr rnh;

        Config config;

        grasping::GraspCandidatesTopicInterfacePrx graspCandidatesTopic;
        grasping::GraspCandidateObserverInterfacePrx graspCandidateObserver;
        RequestableServiceListenerInterfacePrx serviceRequestTopic;
        std::map<std::string, Armar6GraspTrajectoryPtr> graspTrajectories;
        IceUtil::Time disableVisuUntil;


        // Bimanual Grasping GUI Configuration
        //        float rightHandRotationX;
        //        float rightHandRotationY;
        //        float rightHandRotationZ;

        float handOffsetY = armarx::defaultHandYOffset;
        float handDistanceOffsetX = armarx::defaultHandDistanceXOffset;

        bool showInvertedHands = false;
        bool showShortAndLongSideHands = false;
        bool showDiagonalGrasps = false;

        bool showAllGrasps = true;
        std::string chosenGraspName = "";

        grasping::BimanualGraspCandidateSeq candidates;
    };
}
