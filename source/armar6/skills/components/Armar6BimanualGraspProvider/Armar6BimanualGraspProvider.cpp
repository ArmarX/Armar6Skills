/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::Armar6BimanualGraspProvider
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

//                                                  (@@@@@@@@@@@@@@@@
//                                             @@@@@@@@@@@@@@@@@
//                                        @@@@@@@@@@@@@@@@@
//                                   @@@@@@@@@@@@@@@@&
//                              @@@@@@@@@@@@@@@@@
//                         @@@@@@@@@@@@@@@@@
//                    @@@@@@@@@@@@@@@@@
//               @@@@@@@@@@@@@@@@@
//              @@@@@@@@@@@@@@@@@
//                   @@@@@@@@@@@@@@@@@
//                         @@@@@@@@@@@@@@@@
//                             @@@@@@@@@@@@@@@@@
//                                  @@@@@@@@@@@@@@@@@
//               #@@@@@@@@@@@@@@@@       @@@@@@@@@@@@@@@@&
//                    @@@@@@@@@@@@@@@@@       @@@@@@@@@@@@@@@@@
//                         @@@@@@@@@@@@@@@@@       @@@@@@@@@@@@@@@@@
//                              @@@@@@@@@@@@@@@@@
//                                   @@@@@@@@@@@@@@@@%
//                                        @@@@@@@@@@@@@@@@@
//                                             %@@@@@@@@@@@@@@@@
//                                                  (@@@@@@@@@@@@@@@@
//                                                 @@@@@@@@@@@@@@@@@
//                                            @@@@@@@@@@@@@@@@@
//                                       #@@@@@@@@@@@@@@@@
//                                  (@@@@@@@@@@@@@@@@
//                             @@@@@@@@@@@@@@@@@
//                        @@@@@@@@@@@@@@@@@
//                   @@@@@@@@@@@@@@@@&
//              @@@@@@@@@@@@@@@@@

#include "Armar6BimanualGraspProvider.h"

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <RobotAPI/libraries/core/math/MathUtils.h>
#include <RobotAPI/libraries/core/math/MatrixHelpers.h>
#include <RobotAPI/libraries/core/math/SVD.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>
#include <ArmarXGui/libraries/StructuralJson/JPathNavigator.h>
#include <ArmarXGui/libraries/StructuralJson/StructuralJsonParser.h>
#include <RobotAPI/libraries/SimpleJsonLogger/SimpleJsonLoggerEntry.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <VirtualRobot/math/Helpers.h>
#include <cmath>
#include "FitRectangle.h"

#include <RobotAPI/libraries/diffik/SimpleDiffIK.h>

#include <armar6/skills/libraries/Armar6Tools/Armar6HandV2Helper.h>
#include <armar6/skills/components/Armar6BimanualGraspProvider/Constants.h>

using namespace math;
using namespace armarx;
using namespace armarx::math;




Eigen::Vector3f convertToEigen(armarx::Vector3BasePtr vec)
{
    return Eigen::Vector3f(vec->x, vec->y, vec->z);
}

Eigen::Quaternionf convertToEigen(armarx::QuaternionBasePtr quat)
{
    return Eigen::Quaternionf(quat->qw, quat->qx, quat->qy, quat->qz);
}

Eigen::Matrix4f convertToEigen(armarx::PoseBasePtr pose)
{
    auto position = convertToEigen(pose->position);
    auto orientation = convertToEigen(pose->orientation);

    return Helpers::Pose(position, orientation);
}



std::string Armar6BimanualGraspProvider::GraspTypeToString(Armar6BimanualGraspProviderModule::GraspType graspType)
{
    switch (graspType)
    {
        case Armar6BimanualGraspProviderModule::GraspType::Top:
            return "Top";
        case Armar6BimanualGraspProviderModule::GraspType::Side:
            return "Side";
        default:
            throw LocalException("Unknown GraspType");
    }
}

/*std::string Armar6BimanualGraspProvider::PreshapeTypeToString(Armar6BimanualGraspProviderModule::PreshapeType preshapeType)
{
    switch (preshapeType)
    {
        case Armar6BimanualGraspProviderModule::PreshapeType::Open:
            return "Open";
        case Armar6BimanualGraspProviderModule::PreshapeType::Preshaped:
            return "Preshaped";
        default:
            throw LocalException("Unknown GraspType");
    }
}*/

void Armar6BimanualGraspProvider::onInitPointCloudProcessor()
{
    offeringTopic("ServiceRequests");
    usingTopic("ServiceRequests");
    providerName = getProperty<std::string>("providerName").getValue();
    sourceFrameName = getProperty<std::string>("sourceFrameName").getValue();
    pointCloudFormat = getProperty<std::string>("PointCloudFormat").getValue();

    /*boundingBox.z1 = getProperty<float>("BoundingBoxZ1").getValue();
    boundingBox.y1 = getProperty<float>("BoundingBoxY1").getValue();
    boundingBox.y2 = getProperty<float>("BoundingBoxY2").getValue();
    boundingBox.x1 = getProperty<float>("BoundingBoxX1").getValue();
    boundingBox.x2 = getProperty<float>("BoundingBoxX2").getValue();*/

    config.enablePointCloudsVisu = getProperty<bool>("enablePointCloudsVisu").getValue();
    config.enableVisu = getProperty<bool>("enableVisu").getValue();
    flattenInZ = getProperty<bool>("FlattenInZ").getValue();

    usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
    usingPointCloudProvider(providerName);

    offeringTopic(getProperty<std::string>("DebugObserverName").getValue());
    offeringTopic(getProperty<std::string>("DebugDrawerTopicName").getValue());
    offeringTopic(getProperty<std::string>("StaticPlotterName").getValue());
    usingProxy(getProperty<std::string>("RemoteGuiName").getValue());

    //offeringTopic(getProperty<std::string>("GraspCandidatesTopicName").getValue());
    usingTopic(getProperty<std::string>("ConfigTopicName").getValue());

    processCount = 0;
    disableVisuUntil = TimeUtil::GetTime();

    readConfig();


}

void Armar6BimanualGraspProvider::readConfig()
{
    std::string packageName = "HRPGroup4";
    armarx::CMakePackageFinder finder(packageName);
    std::string dataDir = finder.getDataDir() + "/" + packageName;
    std::string filepath = dataDir + "/Armar6BimanualGraspProviderConfig.xml";

    RapidXmlReaderPtr reader = RapidXmlReader::FromFile(filepath);
    RapidXmlReaderNode rootNode = reader->getRoot("Armar6BimanualGraspProviderConfig");

    config.ReferenceOrientationMap = readQuaternionMap(rootNode.first_node("ReferenceOrientationMap"));
    config.ObjectDepthPreshapeThreshold = rootNode.first_node("ObjectDepthPreshapeThreshold").value_as_float();
    config.ObjectHeightPreshapeThreshold = rootNode.first_node("ObjectHeightPreshapeThreshold").value_as_float();
    config.HandGraspingRadius = rootNode.first_node("HandGraspingRadius").value_as_float();
    config.GraspOffsetTCP = readVectorMap(rootNode.first_node("GraspOffsetTCP"));
    config.PreshapeVisu = readPreshapeVisu(rootNode.first_node("PreshapeVisu"));
    config.OpenVisu = readPreshapeVisu(rootNode.first_node("OpenVisu"));
    config.ObjectIdMap = readObjectIdMap(rootNode.first_node("ObjectNameIdMap"));
    config.ObjectGraspTypeMap = readObjectGraspTypeMap(rootNode.first_node("ObjectGraspTypeMap"));
    config.UseObjectIdMap = getProperty<bool>("UseObjectIdMap").getValue();

    graspTrajectories["RightTopOpen"] = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/RightTopOpen.xml");
    graspTrajectories["RightSideOpen"] = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/RightSideOpen.xml");
    graspTrajectories["RightSideBimanual"] = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/RightSideBimanual.xml");
    graspTrajectories["LeftSideBimanual"] = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/LeftSideBimanual.xml");
    //graspTrajectories["RightTopOpen"] = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/RightTopOpen.xml");
    //graspTrajectories["RightSideOpen"] = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/RightSideOpen.xml");

    calculateReferenceOrientations();
}

std::map<int, std::string> Armar6BimanualGraspProvider::readObjectIdMap(const RapidXmlReaderNode& node)
{
    StructuralJsonParser parser(node.value());
    parser.parse();
    JPathNavigator rootNav(parser.parsedJson);
    std::map<int, std::string> map;
    for (const JPathNavigator& nav : rootNav.select("*"))
    {
        map[nav.asInt()] = nav.getKey();
    }
    return map;
}

std::map<std::string, std::string> Armar6BimanualGraspProvider::readObjectGraspTypeMap(const RapidXmlReaderNode& node)
{
    StructuralJsonParser parser(node.value());
    parser.parse();
    JPathNavigator rootNav(parser.parsedJson);
    std::map<std::string, std::string> map;
    for (const JPathNavigator& nav : rootNav.select("*"))
    {
        map[nav.getKey()] = nav.asString();
    }
    return map;
}


std::map<std::string, Eigen::Matrix3f> Armar6BimanualGraspProvider::readQuaternionMap(const RapidXmlReaderNode& node)
{
    StructuralJsonParser parser(node.value());
    parser.parse();
    JPathNavigator rootNav(parser.parsedJson);
    std::map<std::string, Eigen::Matrix3f> map;
    for (const JPathNavigator& nav : rootNav.select("*"))
    {
        float qw = nav.selectSingleNode("qw").asFloat();
        float qx = nav.selectSingleNode("qx").asFloat();
        float qy = nav.selectSingleNode("qy").asFloat();
        float qz = nav.selectSingleNode("qz").asFloat();
        map[nav.getKey()] = Eigen::Quaternionf(qw, qx, qy, qz).toRotationMatrix();
    }
    return map;
}

std::map<std::string, Eigen::Vector3f> Armar6BimanualGraspProvider::readVectorMap(const RapidXmlReaderNode& node)
{
    StructuralJsonParser parser(node.value());
    parser.parse();
    JPathNavigator rootNav(parser.parsedJson);
    std::map<std::string, Eigen::Vector3f> map;
    for (const JPathNavigator& nav : rootNav.select("*"))
    {
        float x = nav.selectSingleNode("x").asFloat();
        float y = nav.selectSingleNode("y").asFloat();
        float z = nav.selectSingleNode("z").asFloat();
        map[nav.getKey()] = Eigen::Vector3f(x, y, z);
    }
    return map;
}

std::map<std::string, float> Armar6BimanualGraspProvider::readPreshapeVisu(const RapidXmlReaderNode& node)
{
    StructuralJsonParser parser(node.value());
    parser.parse();
    JPathNavigator rootNav(parser.parsedJson);
    std::map<std::string, float> map;
    for (const JPathNavigator& nav : rootNav.select("*"))
    {
        map[nav.getKey()] = nav.asFloat();
    }
    return map;
}

void Armar6BimanualGraspProvider::calculateReferenceOrientations()
{
    Eigen::Matrix3f RightTopOpen = config.ReferenceOrientationMap["RightTopOpen"];
    config.ReferenceOrientationMap["RightSideOpen"] = Eigen::AngleAxisf(M_PI / 2, Eigen::Vector3f::UnitY()) * RightTopOpen;
    config.ReferenceOrientationMap["RightTopBlob"] = Eigen::AngleAxisf(::math::Helpers::deg2rad(45), Eigen::Vector3f::UnitZ()) * RightTopOpen;
}

grasping::ProviderInfoPtr Armar6BimanualGraspProvider::getProviderInfo()
{
    grasping::ProviderInfoPtr info = new grasping::ProviderInfo();
    if (config.UseObjectIdMap)
    {
        info->objectType = objpose::KnownObject;
    }
    else
    {
        info->objectType = objpose::UnknownObject;
    }
    info->currentConfig = config.getAsVariantMap();
    return info;
}

void Armar6BimanualGraspProvider::makeSlider(RemoteGui::detail::VBoxLayoutBuilder& rootLayoutBuilder, const std::string& name, int min, int max, float val)
{
    rootLayoutBuilder.addChild(
        RemoteGui::makeHBoxLayout()
        .addChild(RemoteGui::makeTextLabel(name))
        .addChild(RemoteGui::makeFloatSlider(name).min(min).max(max).value(val))
        .addChild(RemoteGui::makeFloatSpinBox(name + "_spin").min(min).max(max).value(val))
    );
}

void Armar6BimanualGraspProvider::makeCheckBox(RemoteGui::detail::HBoxLayoutBuilder& checkBoxLayoutBuilder, const std::string& name, bool val)
{
    checkBoxLayoutBuilder.addChild(
        RemoteGui::makeHBoxLayout()
        .addChild(RemoteGui::makeCheckBox(name).value(val))
        //.addChild(RemoteGui::makeTextLabel(name))
    );
}


void Armar6BimanualGraspProvider::onConnectPointCloudProcessor()
{
    serviceRequestTopic = getTopic<RequestableServiceListenerInterfacePrx>("ServiceRequests");

    robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
    localRobot = RemoteRobot::createLocalCloneFromFile(robotStateComponent, VirtualRobot::RobotIO::eStructure);
    debugObserver = getTopic<DebugObserverInterfacePrx>(getProperty<std::string>("DebugObserverName").getValue());
    debugDrawerTopic = getTopic<DebugDrawerInterfacePrx>(getProperty<std::string>("DebugDrawerTopicName").getValue());
    staticPlotter = getTopic<StaticPlotterInterfacePrx>(getProperty<std::string>("StaticPlotterName").getValue());
    remoteGuiPrx = getProxy<RemoteGuiInterfacePrx>(getProperty<std::string>("RemoteGuiName").getValue());
    graspCandidatesTopic = getTopic<grasping::GraspCandidatesTopicInterfacePrx>(getProperty<std::string>("GraspCandidatesTopicName").getValue());

    ARMARX_INFO << "Before observer getProxy";
    graspCandidateObserver = getProxy<grasping::GraspCandidateObserverInterfacePrx>(getProperty<std::string>("GraspCandidateObserverName").getValue());
    ARMARX_INFO << "After observer getProxy";

    rnh = RobotNameHelper::Create(robotStateComponent->getRobotInfo(), nullptr);

    debugDrawerHelper = DebugDrawerHelperPtr(new DebugDrawerHelper(debugDrawerTopic, getName(), localRobot));
    debugDrawerHelper->clearLayer();

    enableResultPointClouds<pcl::PointXYZRGBL>(getName() + "Result");

    RemoteGui::detail::VBoxLayoutBuilder rootLayoutBuilder = RemoteGui::makeVBoxLayout();

    //    auto makeSlider = [&](std::string name, int min, int max, float val)
    //    {
    //        rootLayoutBuilder.addChild(
    //            RemoteGui::makeHBoxLayout()
    //            .addChild(RemoteGui::makeTextLabel(name))
    //            .addChild(RemoteGui::makeFloatSlider(name).min(min).max(max).value(val))
    //            .addChild(RemoteGui::makeFloatSpinBox(name + "_spin").min(min).max(max).value(val))
    //        );
    //    };

    RemoteGui::detail::HBoxLayoutBuilder checkBoxLayoutBuilder = RemoteGui::makeHBoxLayout();

    //    auto makeCheckBox = [&](std::string name, bool val)
    //    {
    //        checkBoxLayoutBuilder.addChild(
    //            RemoteGui::makeHBoxLayout()
    //            .addChild(RemoteGui::makeCheckBox(name).value(val))
    //            //.addChild(RemoteGui::makeTextLabel(name))
    //        );
    //    };


    // ValueProxy<float> x1Slider = makeSlider ...
    //makeSlider(rootLayoutBuilder, "x1", -600, 600, boundingBox.x1);
    //makeSlider(rootLayoutBuilder, "x2", -600, 600, boundingBox.x2);
    //makeSlider(rootLayoutBuilder, "y1", 0, 1500, boundingBox.y1);
    //makeSlider(rootLayoutBuilder, "y2", 0, 1500, boundingBox.y2);
    //makeSlider(rootLayoutBuilder, "z1", 300, 1500, boundingBox.z1);
    rootLayoutBuilder.addChild(checkBoxLayoutBuilder);
    makeCheckBox(checkBoxLayoutBuilder, "FlattenInZ", true);
    makeCheckBox(checkBoxLayoutBuilder, "enableVisu", true);
    makeCheckBox(checkBoxLayoutBuilder, "enableCandidatesVisu", true);
    makeCheckBox(checkBoxLayoutBuilder, "enableBoundigBoxesVisu", false);
    makeCheckBox(checkBoxLayoutBuilder, "enablePointCloudsVisu", true);

    //    makeCheckBox(checkBoxLayoutBuilder, "showInvertedHands", false);
    //    makeCheckBox(checkBoxLayoutBuilder, "showShortAndLongSideHands", false);

    checkBoxLayoutBuilder.addChild(new RemoteGui::HSpacer());

    // Bimanual Grasping
    //    makeSlider(rootLayoutBuilder, "RightHandRotationX", -EIGEN_PI, EIGEN_PI, 0);
    //    makeSlider(rootLayoutBuilder, "RightHandRotationY", -EIGEN_PI, EIGEN_PI, 0);
    //    makeSlider(rootLayoutBuilder, "RightHandRotationZ", -EIGEN_PI, EIGEN_PI, 0);
    //    makeSlider(rootLayoutBuilder, "HandOffsetY", -100, 100, 22);
    //    makeSlider(rootLayoutBuilder, "handDistanceOffsetX", -100, 100, 0);

    rootLayoutBuilder.addChild(new RemoteGui::VSpacer());

    guiTask = new SimplePeriodicTask<>([&]()
    {
        guiTab.receiveUpdates();
        //boundingBox.x1 = guiTab.getValue<float>("x1").get();
        //boundingBox.x2 = guiTab.getValue<float>("x2").get();
        //boundingBox.y1 = guiTab.getValue<float>("y1").get();
        //boundingBox.y2 = guiTab.getValue<float>("y2").get();
        //boundingBox.z1 = guiTab.getValue<float>("z1").get();
        flattenInZ = guiTab.getValue<bool>("FlattenInZ").get();
        config.enableVisu = guiTab.getValue<bool>("enableVisu").get();
        config.enableCandidatesVisu = guiTab.getValue<bool>("enableCandidatesVisu").get();
        config.enableBoundigBoxesVisu = guiTab.getValue<bool>("enableBoundigBoxesVisu").get();
        config.enablePointCloudsVisu = guiTab.getValue<bool>("enablePointCloudsVisu").get();

        //        showInvertedHands = guiTab.getValue<bool>("showInvertedHands").get();
        //        showShortAndLongSideHands = guiTab.getValue<bool>("showShortAndLongSideHands").get();

        //guiTab.getValue<float>("x1_spin").set(boundingBox.x1);
        //guiTab.getValue<float>("x2_spin").set(boundingBox.x2);
        //guiTab.getValue<float>("y1_spin").set(boundingBox.y1);
        //guiTab.getValue<float>("y2_spin").set(boundingBox.y2);
        //guiTab.getValue<float>("z1_spin").set(boundingBox.z1);

        //        rightHandRotationX = guiTab.getValue<float>("RightHandRotationX").get();
        //        rightHandRotationY = guiTab.getValue<float>("RightHandRotationY").get();
        //        rightHandRotationZ = guiTab.getValue<float>("RightHandRotationZ").get();
        //        guiTab.getValue<float>("RightHandRotationX_spin").set(rightHandRotationX);
        //        guiTab.getValue<float>("RightHandRotationY_spin").set(rightHandRotationY);
        //        guiTab.getValue<float>("RightHandRotationZ_spin").set(rightHandRotationZ);

        //        handOffsetY = guiTab.getValue<float>("HandOffsetY").get();
        //        guiTab.getValue<float>("HandOffsetY_spin").set(handOffsetY);

        //        handDistanceOffsetX = guiTab.getValue<float>("handDistanceOffsetX").get();
        //        guiTab.getValue<float>("handDistanceOffsetX_spin").set(handDistanceOffsetX);

        guiTab.sendUpdates();

        //ARMARX_IMPORTANT << VAROUT(boundingBox.z1);
    }, 10, false, "guiTask");

    reportConfigTask = new SimplePeriodicTask<>([&]()
    {
        graspCandidatesTopic->reportProviderInfo(getName(), getProviderInfo());
    }, 1000, false, "reportConfigTask");

    RemoteGui::WidgetPtr rootLayout = rootLayoutBuilder;


    remoteGuiPrx->createTab(getName(), rootLayout);
    guiTab = RemoteGui::TabProxy(remoteGuiPrx, getName());
    guiTask->start();

    reportConfigTask->start();

}

void Armar6BimanualGraspProvider::onDisconnectPointCloudProcessor()
{
    guiTask->stop();
    guiTask = nullptr;
    reportConfigTask->stop();
}

void Armar6BimanualGraspProvider::onExitPointCloudProcessor()
{
    debugDrawerHelper->clearLayer();
}

void Armar6BimanualGraspProvider::process()
{
    if (pointCloudFormat == "XYZRGBL")
    {
        processPointCloud();
    }
    else
    {
        ARMARX_ERROR << "Could not process point cloud, because format '" << pointCloudFormat << "' is not suppoted / unknown";
    }
}

namespace maptools
{
    template<typename TKey, typename TValue>
    bool TryGetValue(const std::map<TKey, TValue>& map, const TKey& key, TValue& outVal)
    {
        typename std::map<TKey, TValue>::const_iterator it = map.find(key);
        if (it == map.end())
        {
            return false;
        }
        outVal = it->second;
        return true;
    }

    template<typename TKey, typename TValue, typename TDefaultValue>
    TValue GetValueOrDefault(const std::map<TKey, TValue>& map, const TKey& key, const TDefaultValue& defaultVal)
    {
        typename std::map<TKey, TValue>::const_iterator it = map.find(key);
        if (it == map.end())
        {
            return defaultVal;
        }
        return it->second;
    }

    template<typename TKey, typename TValue>
    bool HasKey(const std::map<TKey, TValue>& map, const TKey& key)
    {
        return map.count(key) > 0;
    }
}

void Armar6BimanualGraspProvider::processPointCloud()
{
    auto visualizeGrasps = [&](GraspDescription & grasp, bool right, bool reachable, std::string name)
    {
        if (config.getEffectiveCandidatesVisu())
        {
            DrawColor color = (reachable ? DrawColor {0, 1, 0, 1} : DrawColor {1, 0, 0, 1});

            Eigen::Matrix4f tcp2handRoot;
            if (right)
            {
                tcp2handRoot = rnh->getRobotArm(RobotNameHelper::LocationRight, localRobot).getTCP()->getPoseInRootFrame().inverse() * localRobot->getRobotNode("Hand R Root")->getPoseInRootFrame();
            }
            else
            {
                tcp2handRoot = rnh->getRobotArm(RobotNameHelper::LocationLeft, localRobot).getTCP()->getPoseInRootFrame().inverse() * localRobot->getRobotNode("Hand L Root")->getPoseInRootFrame();
            }
            debugDrawerHelper->drawRobot("grasp_" + name + (right ? "_r" : "_l"), right ? "armar6_rt/robotmodel/Armar6-SH/Armar6-RightHand-v3.xml" : "armar6_rt/robotmodel/Armar6-SH/Armar6-LeftHand-v3.xml", "armar6_rt", grasp.pose * tcp2handRoot, color);
            // void drawArrow(const std::string& name, const Eigen::Vector3f& pos, const Eigen::Vector3f& direction, const DrawColor& color, float length, float width);
            debugDrawerHelper->drawArrow("grasp_" + name + (right ? "_r" : "_l") + "_approach", Helpers::Position(grasp.pose) - grasp.approach * 200, grasp.approach, DrawColor {1, 1, 0, 1}, 200, 3);
        }
    };

    if (!showAllGrasps)
    {
        for (auto candidate : candidates)
        {
            if (candidate->graspName == chosenGraspName)
            {
                GraspDescription graspRight;
                graspRight.pose = convertToEigen(candidate->graspPoseRight);
                graspRight.approach = convertToEigen(candidate->approachVectorRight);

                GraspDescription graspLeft;
                graspLeft.pose = convertToEigen(candidate->graspPoseLeft);
                graspLeft.approach = convertToEigen(candidate->approachVectorLeft);

                visualizeGrasps(graspRight, true, true, candidate->graspName);
                visualizeGrasps(graspLeft, false, true, candidate->graspName);
                break;
            }
        }
    }

    typename pcl::PointCloud<pcl::PointXYZRGBL>::Ptr inputCloudPtr(new pcl::PointCloud<pcl::PointXYZRGBL>());
    typename pcl::PointCloud<pcl::PointXYZRGBL>::Ptr tmpCloudPtr(new pcl::PointCloud<pcl::PointXYZRGBL>());
    typename pcl::PointCloud<pcl::PointXYZRGBL>::Ptr outputCloudPtr(new pcl::PointCloud<pcl::PointXYZRGBL>());
    typename pcl::PointCloud<pcl::PointXYZRGBL>::Ptr segmentedCloudPtr(new pcl::PointCloud<pcl::PointXYZRGBL>());

    if (!waitForPointClouds(providerName, 10000))
    {
        ARMARX_INFO << "Timeout or error while waiting for point cloud data" << armarx::flush;
        return;
    }
    else
    {
        getPointClouds(providerName, inputCloudPtr);
    }

    typename pcl::PointCloud<pcl::PointXYZRGBL>::Ptr downsampledCloudPtr(new pcl::PointCloud<pcl::PointXYZRGBL>());

    float gridLeafSize = getProperty<float>("DownsamplingLeafSize");
    if (gridLeafSize > 0)
    {
        pcl::ApproximateVoxelGrid<pcl::PointXYZRGBL> grid;
        //    grid.setDownsampleAllData(false);
        grid.setLeafSize(gridLeafSize, gridLeafSize, gridLeafSize);
        grid.setInputCloud(inputCloudPtr);
        grid.filter(*downsampledCloudPtr);
        inputCloudPtr.swap(downsampledCloudPtr);

        //        pcl::VoxelGrid<pcl::PointXYZRGBL> grid;
        //        float N = 0.001f;
        //        Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
        //        transform(0, 0) = transform(0, 0) * N;
        //        transform(1, 1) = transform(1, 1) * N;
        //        transform(2, 2) = transform(2, 2) * N;
        //        pcl::transformPointCloud(*inputCloudPtr, *inputCloudPtr, transform);
        //        gridLeafSize /= 1000.0f;
        //        //    grid.setDownsampleAllData(false);
        //        grid.setLeafSize(gridLeafSize, gridLeafSize, gridLeafSize);
        //        grid.setMinimumPointsNumberPerVoxel(3);
        //        grid.setInputCloud(inputCloudPtr);
        //        grid.filter(*downsampledCloudPtr);
        //        inputCloudPtr.swap(downsampledCloudPtr);

        //        transform(0, 0) = transform(0, 0) * 1.0 / N;
        //        transform(1, 1) = transform(1, 1) * 1.0 / N;
        //        transform(2, 2) = transform(2, 2) * 1.0 / N;
        //        pcl::transformPointCloud(*inputCloudPtr, *inputCloudPtr, transform);
    }

    bool enableVisu = config.enableVisu;
    if (TimeUtil::GetTime() < disableVisuUntil)
    {
        enableVisu = false;
    }
    debugDrawerHelper->setVisuEnabled(enableVisu);
    if (!enableVisu)
    {
        debugDrawerHelper->clearLayer();
    }

    visionx::MetaPointCloudFormatPtr format = getPointCloudFormat(providerName);


    int inputSize = inputCloudPtr->points.size();

    if (inputSize < 100)
    {
        ARMARX_ERROR << "Input size too small!";
        return;
    }

    ARMARX_VERBOSE << "processPointCloud: " << inputCloudPtr->width << "x" << inputCloudPtr->height << " age: " << (TimeUtil::GetTime() - IceUtil::Time::microSeconds(inputCloudPtr->header.stamp)).toMilliSeconds();

    RemoteRobot::synchronizeLocalCloneToTimestamp(localRobot, robotStateComponent, inputCloudPtr->header.stamp);

    //auto mapping = armarx::Split(getProperty<std::string>("ObjectNameIdMap"), ";", true, true);
    //std::map<std::string, int> nameIdMap;
    //std::map<int, std::string> idNameMap;
    //for (auto entry : mapping)
    //{
    //    auto pair = armarx::Split(entry, ":");
    //    ARMARX_CHECK_EQUAL(pair.size(), 2) << entry;
    //    nameIdMap[pair.at(0)] = armarx::toInt(pair.at(1));
    //    idNameMap[armarx::toInt(pair.at(1))] = pair.at(0);
    //}

    Eigen::Matrix4f sourceFrameInRootFrame = Eigen::Matrix4f::Identity();
    if (sourceFrameName == "Global")
    {
        sourceFrameInRootFrame = localRobot->getRootNode()->getGlobalPose().inverse();
    }
    else
    {
        sourceFrameInRootFrame = localRobot->getRobotNode(sourceFrameName)->getPoseInRootFrame();
    }


    // Transform to global frame
    pcl::transformPointCloud(*inputCloudPtr, *tmpCloudPtr, sourceFrameInRootFrame);
    tmpCloudPtr.swap(inputCloudPtr);

    tmpCloudPtr->clear();
    //std::vector<Eigen::Vector3f> segmentedPoints;



    uint32_t backgroundLabel = getProperty<uint32_t>("BackgroundLabelId").getValue();
    std::map<uint32_t, std::vector<Eigen::Vector3f>> labeledPoints;
    std::map<uint32_t, DrawColor> colorMap;
    std::map<uint32_t, pcl::PointCloud<pcl::PointXYZRGBL>::Ptr> labeledClouds;

    for (const pcl::PointXYZRGBL& point : inputCloudPtr->points)
    {
        bool anyNan = ::isnan(point.x) || ::isnan(point.y) || ::isnan(point.z) || ::isnan(point.r) || ::isnan(point.g) || ::isnan(point.b);
        if (anyNan)
        {
            continue;
        }
        if (point.label == backgroundLabel)
        {
            //            ARMARX_VERBOSE << "Skipping background label with " << allPoints.size() << " points";
            continue;
        }
        pcl::PointXYZRGBL p;
        p.x = point.x;
        p.y = point.y;
        p.z = point.z;
        p.r = point.r;
        p.g = point.g;
        p.b = point.b;
        p.label = point.label;

        if (!labeledClouds[point.label])
        {
            labeledClouds[point.label].reset(new pcl::PointCloud<pcl::PointXYZRGBL>());
        }

        colorMap[point.label] = DrawColor {point.r / 255.f, point.g / 255.f, point.b / 255.f, 1.f};

        labeledClouds[point.label]->push_back(p);

        outputCloudPtr->push_back(p);
    }
    for (const auto& pair : labeledClouds)
    {
        pcl::PointCloud<pcl::PointXYZRGBL>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZRGBL>());
        pcl::StatisticalOutlierRemoval<pcl::PointXYZRGBL> sor;
        sor.setInputCloud(pair.second);
        sor.setMeanK(50);
        sor.setStddevMulThresh(1.0);
        sor.filter(*cloud_filtered);
        for (const pcl::PointXYZRGBL& point : cloud_filtered->points)
        {
            labeledPoints[point.label].push_back(Eigen::Vector3f(point.x, point.y, point.z));
        }
    }

    //ARMARX_IMPORTANT << "labeledPoints.size: " << labeledPoints.size();

    /*{
        std::unique_lock lock(capturedPointsMutex);
        capturedPoints.clear();
        for (const Eigen::Vector3f& p : segmentedPoints)
        {
            capturedPoints.push_back(CapturedPoint {p(0), p(1), p(2)});
        }
    }*/

    //ARMARX_IMPORTANT << VAROUT(labeledPoints.size());

    //Armar6BimanualGraspProviderModule::ResultSeq resultList;
    std::vector<Armar6BimanualGraspProviderModule::Result> resultList;
    int detectedObjects = 0;


    std::stringstream ssDebug;

    //    grasping::BimanualGraspCandidateSeq candidates;

    RobotNameHelper::RobotArm armR = rnh->getRobotArm("Right", localRobot);
    RobotNameHelper::RobotArm armL = rnh->getRobotArm("Left", localRobot);
    VirtualRobot::RobotNodeSetPtr rns = armR.getKinematicChain();
    VirtualRobot::RobotNodeSetPtr lns = armL.getKinematicChain();
    VirtualRobot::RobotNodePtr tcpR = armR.getTCP();
    VirtualRobot::RobotNodePtr tcpL = armL.getTCP();

    //    Eigen::VectorXf initialJV = Eigen::VectorXf::Zero(rns->getSize());

    StringVector2fSeqDict plots;

    //    ARMARX_INFO << "Generating grasp candidates";

    for (const auto& pair : labeledPoints)
    {
        std::vector<Eigen::Vector3f> allPoints = pair.second;
        uint32_t label = pair.first;

        if (label == backgroundLabel)
        {
            ARMARX_VERBOSE << "Skipping background label with " << allPoints.size() << " points";
            continue;
        }
        std::string objectName = "";
        bool isKnownObject = false;
        if (config.UseObjectIdMap)
        {
            objectName = maptools::GetValueOrDefault(config.ObjectIdMap, (int)label, "UNKNOWN");
            isKnownObject = maptools::HasKey(config.ObjectIdMap, (int)label);
        }
        else
        {
            objectName = "OBJ_" + std::to_string(label);
        }

        ARMARX_VERBOSE << VAROUT(objectName) << VAROUT(label) << " " << VAROUT(allPoints.size());

        if (allPoints.size() < getProperty<size_t>("MininumClusterPointCount").getValue())
        {
            ARMARX_VERBOSE << /*deactivateSpam(1, objectName) <<*/ "too few points for PCA for " << objectName << " (" << label << ")";
        }
        else
        {

            /*Eigen::MatrixXf pointsAsMatrix(3, allPoints.size());
            for (size_t i = 0; i < allPoints.size(); i++)
            {
                pointsAsMatrix.block<3, 1>(0, i) = allPoints.at(i);
            }

            Eigen::Vector3f center = MatrixHelpers::CalculateCog3D(pointsAsMatrix);

            Eigen::MatrixXf pointsNoMean = MatrixHelpers::SubtractVectorFromAllColumns3D(pointsAsMatrix, center);
            if (flattenInZ)
            {
                MatrixHelpers::SetRowToValue(pointsNoMean, 2, 0);
            }

            SVD svd(pointsNoMean);

            Eigen::Vector3f v1len = svd.getLeftSingularVector3D(0);
            Eigen::Vector3f v2len = svd.getLeftSingularVector3D(1);
            Eigen::Vector3f v3len = svd.getLeftSingularVector3D(2);


            Eigen::Vector3f v1 = svd.getLeftSingularVector3D(0).normalized();
            Eigen::Vector3f v2 = svd.getLeftSingularVector3D(1).normalized();
            Eigen::Vector3f v3 = svd.getLeftSingularVector3D(2).normalized();*/

            auto determineGraspType = [&](const Box3D & box)
            {
                float height = box.Size3();
                float width = box.Size1();
                return width > height ? Armar6BimanualGraspProviderModule::GraspType::Top : Armar6BimanualGraspProviderModule::GraspType::Side;
            };

            FitRectangle fitBox(90);
            Box3D box = fitBox.Fit(allPoints);
            Eigen::Vector3f center = box.GetPoint(0, 0, 0);
            Armar6BimanualGraspProviderModule::GraspType graspType = determineGraspType(box);

            std::string graspTypeStr = maptools::GetValueOrDefault(config.ObjectGraspTypeMap, objectName, "calculate");
            if (graspTypeStr == "TopBlob")
            {
                graspType = Armar6BimanualGraspProviderModule::GraspType::Top;
            }
            if (graspTypeStr == "Top")
            {
                graspType = Armar6BimanualGraspProviderModule::GraspType::Top;
            }
            if (graspTypeStr == "SideTop")
            {
                graspType = Armar6BimanualGraspProviderModule::GraspType::Side;
            }

            Vector2fSeq boxSizePlot;
            for (size_t i = 0; i < fitBox.results.size(); i++)
            {
                boxSizePlot.push_back(Vector2f {(float)i, fitBox.results.at(i).Volume()});
            }
            plots[objectName] = boxSizePlot;



            /*std::vector<Eigen::Vector3f> filteredPoints;
            if (graspType == Armar6BimanualGraspProviderModule::GraspType::Top)
            {
                for (const Eigen::Vector3f& p : allPoints)
                {
                    if ((p - center).norm() < config.HandGraspingRadius)
                    {
                        filteredPoints.push_back(p);
                    }
                }

                if (filteredPoints.size() < 10)
                {
                    filteredPoints = allPoints;
                }
            }
            else
            {
                filteredPoints = allPoints;
            }




            box = fitBox.Fit(filteredPoints);
            center = box.GetPoint(0, 0, 0);*/

            std::vector<Eigen::Vector3f> filteredPoints = allPoints;

            Eigen::Vector3f v1 = box.GetDir1().normalized();
            Eigen::Vector3f v2 = box.GetDir2().normalized();
            Eigen::Vector3f v3 = box.GetDir3().normalized();
            Line l1 = Line(center, v1);
            Line l2 = Line(center, v2);
            Line l3 = Line(center, v3);

            Percentile p1(FitRectangle::ProjectPoints(l1, filteredPoints));
            Percentile p2(FitRectangle::ProjectPoints(l2, filteredPoints));
            Percentile p3(FitRectangle::ProjectPoints(l3, filteredPoints));


            auto extractExtent = [&](const Percentile & percentile)
            {
                Armar6BimanualGraspProviderModule::Extent extent;
                extent.min = percentile.get(0.00f);
                extent.max = percentile.get(1.00f);
                extent.p05 = percentile.get(0.05f);
                extent.p10 = percentile.get(0.10f);
                extent.p90 = percentile.get(0.90f);
                extent.p95 = percentile.get(0.95f);
                return extent;
            };




            //            auto calculateGraspPoseBimanualTop = [&](const Armar6BimanualGraspProviderModule::Result & result, const Eigen::Vector3f & preferredForward, const std::string & graspTypeStr)
            //            {
            //                Eigen::Vector3f graspForwardVector = v2;

            //                //Armar6BimanualGraspProviderModule::PreshapeType preshape = Armar6BimanualGraspProviderModule::PreshapeType::Open;


            //                //@@@ disabled for now
            //                //float height = result.extent3.p95 - result.extent3.p05;
            //                //float depth = result.extent2.p95 - result.extent2.p05;
            //                //if (depth < config.ObjectDepthPreshapeThreshold && height < config.ObjectHeightPreshapeThreshold)
            //                //{
            //                //    preshape = Armar6BimanualGraspProviderModule::PreshapeType::Preshaped;
            //                //}

            //                if (graspForwardVector.dot(preferredForward) < 0)
            //                {
            //                    graspForwardVector = -graspForwardVector;
            //                }
            //                float angleV2 = Helpers::Angle(Eigen::Vector2f(graspForwardVector(0), graspForwardVector(1)));
            //                //ARMARX_INFO << "angleV2 = " << angleV2 << std::endl;
            //                float angleGraspOri = Helpers::Angle(Eigen::Vector2f(0, 1));
            //                //ARMARX_INFO << "angleGraspOri = " << angleGraspOri << std::endl;
            //                float rotationAngle = Helpers::AngleModPI(angleV2 - angleGraspOri);
            //                //ARMARX_INFO << "rotationAngle = " << rotationAngle << std::endl;
            //                Eigen::Matrix3f referenceOrientation = config.ReferenceOrientationMap.at("RightTopOpen");
            //                Eigen::AngleAxisf aa = Eigen::AngleAxisf(rotationAngle, Eigen::Vector3f::UnitZ());

            //                //                Eigen::Quaternionf rot(cos);

            //                Eigen::AngleAxisf rotateZ = Eigen::AngleAxisf(rightHandRotationZ, Eigen::Vector3f::UnitZ());
            //                Eigen::AngleAxisf rotateY = Eigen::AngleAxisf(rightHandRotationY, rotateZ.toRotationMatrix() * Eigen::Vector3f::UnitY());
            //                Eigen::AngleAxisf rotateZ2 = Eigen::AngleAxisf(rightHandRotationX, rotateZ.toRotationMatrix() * rotateY.toRotationMatrix() * Eigen::Vector3f::UnitZ());

            //                //rotateZ = rotateY.toRotationMatrix() * rotateZ;

            //                debugDrawerHelper->drawLine("rotateZ", center + 2 * v3, center + 2 * v3 + 1000 * Eigen::Vector3f::UnitZ(), 3, DrawColor {0, 0, 1, 1});
            //                debugDrawerHelper->drawLine("rotateY", center + 2 * v3, center + 2 * v3 + 1000 * rotateZ.toRotationMatrix() * Eigen::Vector3f::UnitY(), 3, DrawColor {0, 1, 0, 1});
            //                debugDrawerHelper->drawLine("rotateZ2", center + 2 * v3, center + 2 * v3 + 1000 * rotateZ.toRotationMatrix() * rotateY.toRotationMatrix() * Eigen::Vector3f::UnitZ(), 3, DrawColor {1, 0, 0, 1});

            //                Eigen::Matrix3f graspOri = (aa.toRotationMatrix() * (rotateZ2.toRotationMatrix() * (rotateY.toRotationMatrix() * (rotateZ.toRotationMatrix() * referenceOrientation))));

            //                if (graspTypeStr == "TopBlob")
            //                {
            //                    graspOri = config.ReferenceOrientationMap.at("RightTopBlob");
            //                }

            //                //ARMARX_INFO << "result.extent1: " << result.extent1.max << std::endl;
            //                //ARMARX_INFO << "result.extent1.p95: " << result.extent1.p95 << std::endl;

            //                Eigen::Vector3f graspPosRight = center + v3 * result.extent3.p95 + graspOri * config.GraspOffsetTCP.at("Right")
            //                                                - v1 * result.extent1.p95;

            //                GraspDescription grasp;
            //                grasp.pose = Helpers::CreatePose(graspPosRight, graspOri);
            //                grasp.approach = Eigen::Vector3f::UnitZ();
            //                //grasp.preshape = preshape;

            //                return grasp;
            //            };


            /*****************************************************************************************************************************************************************************
             *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             */


            auto calculateBimanualGraspPoseSide = [&](const Armar6BimanualGraspProviderModule::Result & result, std::pair<GraspDescription, GraspDescription>& grasps, bool invertGrasp, bool otherSideGrasp)
            {
                // Direction that hands point in, for grasping the long side and the short side of the object respectively
                // v1 is the long axis of the object in the robot coordinate system
                Eigen::Vector3f longSideGraspForwardVector = v1;
                Eigen::Vector3f shortSideGraspForwardVector = v2;

                // Invert Grasp -> Grasp on the opposite side of the object
                if (invertGrasp)
                {
                    longSideGraspForwardVector *= -1;
                    shortSideGraspForwardVector *= -1;
                }

                // Hand offset in the direction of the grasp (along the long side for grasping the long side)
                Eigen::Vector3f offset_v1 = v1 * (result.extent1.p95 - handDistanceOffsetX);
                Eigen::Vector3f offset_v2 = v2 * (result.extent2.p95 - handDistanceOffsetX);

                // Detect if orientation of the object coordinate system has flipped
                // It flips when the orientation of the robot goes on the other side of the v2 axis
                bool longSideGraspForwardVectorScalarProductWithUnitXLessThanZero = longSideGraspForwardVector.dot(Eigen::Vector3f(1.0, 0.0, 0.0)) < 0;
                if (longSideGraspForwardVectorScalarProductWithUnitXLessThanZero)
                {
                    longSideGraspForwardVector *= -1;
                    offset_v1 = v1 * (result.extent1.p05 + handDistanceOffsetX);
                }



                bool shortSideGraspForwardVectorScalarProductWithUnitXLessThanZero = shortSideGraspForwardVector.dot(Eigen::Vector3f(1.0, 0.0, 0.0)) < 0;
                if (shortSideGraspForwardVectorScalarProductWithUnitXLessThanZero)
                {
                    shortSideGraspForwardVector *= -1;
                    offset_v2 = v2 * (result.extent2.p05 + handDistanceOffsetX);
                }

                //                debugDrawerHelper->drawArrow(label + "_longSideGraspForwardVector", center, longSideGraspForwardVector, DrawColor {1, 0.7, 0.3, 1}, 200, 5);
                //                debugDrawerHelper->drawArrow(label + "_shortSideGraspForwardVector", center, shortSideGraspForwardVector, DrawColor {0.3, 0.7, 1, 1}, 200, 5);


                // Angle between robot and object
                float angleV1 = Helpers::Angle(Eigen::Vector2f(longSideGraspForwardVector(0), longSideGraspForwardVector(1)));
                float angleV2 = Helpers::Angle(Eigen::Vector2f(shortSideGraspForwardVector(0), shortSideGraspForwardVector(1)));

                // Calculate grasp positions
                Eigen::Vector3f graspPosR;
                Eigen::Vector3f graspPosL;

                // Depending on where the robot stands with respect to the object, grasp long or short side
                // For a 90 degree cone in front of a long side, grasp the long side. Otherwise grasp the short side
                bool graspLongSide = std::abs(angleV2) > EIGEN_PI / 4 && std::abs(angleV2) < EIGEN_PI / 2;
                if (otherSideGrasp)
                {
                    graspLongSide = !(graspLongSide);
                }

                if (graspLongSide)
                {
                    if (longSideGraspForwardVectorScalarProductWithUnitXLessThanZero)
                    {
                        // Coordinate system flipped
                        graspPosR = box.GetPoint(0, 1, -1) + offset_v1 - v2 * (handOffsetY + standardHandYOffset) + v3 * standardHandZOffset;
                        graspPosL = box.GetPoint(0, 1, -1) - offset_v1 - v2 * (handOffsetY + standardHandYOffset) + v3 * standardHandZOffset;
                    }
                    else
                    {
                        graspPosR = box.GetPoint(0, -1, -1) + offset_v1 + v2 * (handOffsetY + standardHandYOffset) + v3 * standardHandZOffset;
                        graspPosL = box.GetPoint(0, -1, -1) - offset_v1 + v2 * (handOffsetY + standardHandYOffset) + v3 * standardHandZOffset;
                    }
                }
                else
                {
                    if (shortSideGraspForwardVectorScalarProductWithUnitXLessThanZero)
                    {
                        // Coordinate systems flipped
                        graspPosR = box.GetPoint(-1, 0, -1) + offset_v2 + v1 * (handOffsetY + standardHandYOffset) + v3 * standardHandZOffset;
                        graspPosL = box.GetPoint(-1, 0, -1) - offset_v2 + v1 * (handOffsetY + standardHandYOffset) + v3 * standardHandZOffset;
                    }
                    else
                    {
                        graspPosR = box.GetPoint(1, 0, -1) + offset_v2 - v1 * (handOffsetY + standardHandYOffset) + v3 * standardHandZOffset;
                        graspPosL = box.GetPoint(1, 0, -1) - offset_v2 - v1 * (handOffsetY + standardHandYOffset) + v3 * standardHandZOffset;
                    }
                }

                //float angleGraspOri = Helpers::Angle(Eigen::Vector2f(1, 0));
                // Calculate angle of grasp (align with object, rotate by 90 degree for opposite side grasps)
                float rotationAngle = 0;
                if (graspLongSide)
                {
                    rotationAngle = Helpers::AngleModPI(angleV1 - /*angleGraspOri*/ + (invertGrasp ? EIGEN_PI : 0));
                }
                else
                {
                    rotationAngle = Helpers::AngleModPI(angleV2 - /*angleGraspOri*/ + (invertGrasp ? EIGEN_PI : 0));
                }

                // Calculate transformation matrix with rotationAngle and the reference hand pose
                Eigen::AngleAxisf aa = Eigen::AngleAxisf(rotationAngle, Eigen::Vector3f::UnitZ());
                Eigen::Matrix3f referenceOrientation = config.ReferenceOrientationMap.at("BimanualSideOpen");
                Eigen::Matrix3f graspOri = aa.toRotationMatrix() * referenceOrientation;

                // Right hand grasp descriptor
                GraspDescription graspR;
                graspR.pose = Helpers::CreatePose(graspPosR, graspOri);

                //graspR.preshape = Armar6BimanualGraspProviderModule::PreshapeType::Open;

                // Left hand grasp descriptor
                GraspDescription graspL;
                graspL.pose = Helpers::CreatePose(graspPosL, graspOri);
                //graspL.approach = (v1 - v2).normalized();
                //graspL.preshape = Armar6BimanualGraspProviderModule::PreshapeType::Open;

                if (graspLongSide)
                {
                    if (invertGrasp)
                    {
                        longSideGraspForwardVector *= -1;
                    }

                    if (longSideGraspForwardVectorScalarProductWithUnitXLessThanZero)
                    {
                        graspR.approach = (-longSideGraspForwardVector + shortSideGraspForwardVector).normalized();
                        graspL.approach = (longSideGraspForwardVector + shortSideGraspForwardVector).normalized();
                    }
                    else
                    {
                        graspR.approach = (-longSideGraspForwardVector - shortSideGraspForwardVector).normalized();
                        graspL.approach = (longSideGraspForwardVector - shortSideGraspForwardVector).normalized();
                    }
                    graspR.inwards = -longSideGraspForwardVector;
                    graspL.inwards = longSideGraspForwardVector;
                }
                else
                {
                    if (invertGrasp)
                    {
                        shortSideGraspForwardVector *= -1;
                    }

                    if (!longSideGraspForwardVectorScalarProductWithUnitXLessThanZero)
                    {
                        graspR.approach = (longSideGraspForwardVector - shortSideGraspForwardVector).normalized();
                        graspL.approach = (longSideGraspForwardVector + shortSideGraspForwardVector).normalized();
                    }
                    else
                    {
                        graspR.approach = (-longSideGraspForwardVector - shortSideGraspForwardVector).normalized();
                        graspL.approach = (-longSideGraspForwardVector + shortSideGraspForwardVector).normalized();
                    }
                    graspR.inwards = -shortSideGraspForwardVector;
                    graspL.inwards = shortSideGraspForwardVector;
                }

                graspR.graspName = graspLongSide ? "LongSide" : "ShortSide";
                graspL.graspName = graspLongSide ? "LongSide" : "ShortSide";

                std::pair<GraspDescription, GraspDescription> grasps_(graspR, graspL);
                grasps = grasps_;

                // Check if the distance between hands is sufficient
                float handsDistance = (graspPosR - graspPosL).norm();
                if (handsDistance < minimumHandDistance)
                {
                    //                    ARMARX_INFO << "Hand distance below minimum distance: " << handsDistance << " < " << minimumHandDistance << std::endl;
                    return false;
                }

                return true;
            };


            auto calculateBimanualGraspPoseDiagonal = [&](const Armar6BimanualGraspProviderModule::Result & result, std::pair<GraspDescription, GraspDescription>& grasps, bool invertGrasp, bool otherSideGrasp)
            {
                // Direction that hands point in, for grasping the long side and the short side of the object respectively
                // v1 is the long axis of the object in the robot coordinate system
                Eigen::Vector3f longSideGraspForwardVector = v1;
                Eigen::Vector3f shortSideGraspForwardVector = v2;

                // Invert Grasp -> Grasp on the opposite side of the object
                if (invertGrasp)
                {
                    longSideGraspForwardVector *= -1;
                    shortSideGraspForwardVector *= -1;
                }

                // Hand offset in the direction of the grasp (along the long side for grasping the long side)
                Eigen::Vector3f offset_v1 = v1 * (result.extent1.p95 - handDistanceOffsetX);
                Eigen::Vector3f offset_v2 = v2 * (result.extent2.p95 - handDistanceOffsetX);

                // Detect if orientation of the object coordinate system has flipped
                // It flips when the orientation of the robot goes on the other side of the v2 axis
                bool longSideGraspForwardVectorScalarProductWithUnitXLessThanZero = longSideGraspForwardVector.dot(Eigen::Vector3f(1.0, 0.0, 0.0)) < 0;
                if (longSideGraspForwardVectorScalarProductWithUnitXLessThanZero)
                {
                    longSideGraspForwardVector *= -1;
                    offset_v1 = v1 * (result.extent1.p05 + handDistanceOffsetX);
                }



                bool shortSideGraspForwardVectorScalarProductWithUnitXLessThanZero = shortSideGraspForwardVector.dot(Eigen::Vector3f(1.0, 0.0, 0.0)) < 0;
                if (shortSideGraspForwardVectorScalarProductWithUnitXLessThanZero)
                {
                    shortSideGraspForwardVector *= -1;
                    offset_v2 = v2 * (result.extent2.p05 + handDistanceOffsetX);
                }

                //                debugDrawerHelper->drawArrow(label + "_longSideGraspForwardVector", center, longSideGraspForwardVector, DrawColor {1, 0.7, 0.3, 1}, 200, 5);
                //                debugDrawerHelper->drawArrow(label + "_shortSideGraspForwardVector", center, shortSideGraspForwardVector, DrawColor {0.3, 0.7, 1, 1}, 200, 5);


                // Angle between robot and object
                float angleV1 = Helpers::Angle(Eigen::Vector2f(longSideGraspForwardVector(0), longSideGraspForwardVector(1)));
                float angleV2 = Helpers::Angle(Eigen::Vector2f(shortSideGraspForwardVector(0), shortSideGraspForwardVector(1)));

                // Calculate grasp positions
                Eigen::Vector3f graspPosR;
                Eigen::Vector3f graspPosL;

                // Depending on where the robot stands with respect to the object, grasp long or short side
                // For a 90 degree cone in front of a long side, grasp the long side. Otherwise grasp the short side
                bool graspLongSide = std::abs(angleV2) > EIGEN_PI / 4 && std::abs(angleV2) < EIGEN_PI / 2;
                //                if (otherSideGrasp)
                //                {
                graspLongSide = !(graspLongSide);
                //                }


                //                if (invertGrasp)
                //                {
                //                    ARMARX_INFO << "longSideGraspForwardVectorScalarProductWithUnitXLessThanZero: " << longSideGraspForwardVectorScalarProductWithUnitXLessThanZero
                //                                << ", shortSideGraspForwardVectorScalarProductWithUnitXLessThanZero: " << shortSideGraspForwardVectorScalarProductWithUnitXLessThanZero;
                //                }

                if (invertGrasp)
                {
                    auto temp = v1;
                    v1 = v2;
                    v2 = temp;
                }

                if (graspLongSide)
                {
                    if (longSideGraspForwardVectorScalarProductWithUnitXLessThanZero)
                    {
                        // Coordinate system flipped
                        graspPosR = box.GetPoint(-1, 0, -1) + offset_v2 + v1 * (handOffsetY + standardHandYOffset) * (invertGrasp ? -1 : 1) + v3 * standardHandZOffset;
                        graspPosL = box.GetPoint(0, 1 * (invertGrasp ? -1 : 1), -1) - offset_v1 - v2 * (handOffsetY + standardHandYOffset) + v3 * standardHandZOffset;
                    }
                    else
                    {
                        graspPosR = box.GetPoint(0, -1 * (invertGrasp ? -1 : 1), -1) + offset_v1 + v2 * (handOffsetY + standardHandYOffset) * (invertGrasp ? -1 : 1) + v3 * standardHandZOffset;
                        graspPosL = box.GetPoint(-1, 0, -1) - offset_v2 + v1 * (handOffsetY + standardHandYOffset) + v3 * standardHandZOffset;
                    }
                }
                else
                {
                    if (longSideGraspForwardVectorScalarProductWithUnitXLessThanZero)
                    {
                        // Coordinate systems flipped
                        graspPosR = box.GetPoint(-1, 0, -1) + offset_v2 + v1 * (handOffsetY + standardHandYOffset) * (invertGrasp ? -1 : 1) + v3 * standardHandZOffset;
                        graspPosL = box.GetPoint(1, 0, -1) - offset_v2 - v2 * (handOffsetY + standardHandYOffset) + v3 * standardHandZOffset;
                    }
                    else
                    {
                        graspPosR = box.GetPoint(1, 0, -1) + offset_v2 + v2 * (handOffsetY + standardHandYOffset) * (invertGrasp ? -1 : 1) + v3 * standardHandZOffset;
                        graspPosL = box.GetPoint(-1, 0, -1) - offset_v2 + v1 * (handOffsetY + standardHandYOffset) + v3 * standardHandZOffset;
                    }
                }

                //float angleGraspOri = Helpers::Angle(Eigen::Vector2f(1, 0));
                // Calculate angle of grasp (align with object, rotate by 90 degree for opposite side grasps)
                float rotationAngleR = 0;
                float rotationAngleL = 0;
                if (graspLongSide)
                {
                    rotationAngleR = Helpers::AngleModPI(angleV1 - /*angleGraspOri*/ + (invertGrasp ? EIGEN_PI : 0));
                    rotationAngleL = rotationAngleR;
                    if (longSideGraspForwardVectorScalarProductWithUnitXLessThanZero)
                    {
                        rotationAngleR += EIGEN_PI / 2  * (invertGrasp ? 0 : 1);
                        rotationAngleL += EIGEN_PI / 2  * (invertGrasp ? -1 : 0);
                    }
                    else
                    {
                        rotationAngleR += EIGEN_PI / 2  * (invertGrasp ? 1 : 0);
                        rotationAngleL += EIGEN_PI / 2  * (invertGrasp ? 0 : -1);
                    }
                }
                else
                {
                    rotationAngleR = Helpers::AngleModPI(angleV2 - /*angleGraspOri*/ + (invertGrasp ? EIGEN_PI : 0));
                    rotationAngleL = rotationAngleR;
                    if (longSideGraspForwardVectorScalarProductWithUnitXLessThanZero)
                    {
                        rotationAngleL += EIGEN_PI / 2  * (invertGrasp ? 0 : -1);
                        rotationAngleR += EIGEN_PI / 2 * (invertGrasp ? 1 : 0);
                    }
                    else
                    {
                        rotationAngleR += EIGEN_PI / 2 * (invertGrasp ? 0 : 1);
                        rotationAngleL += EIGEN_PI / 2 * (invertGrasp ? -1 : 0);
                    }
                }

                // Calculate transformation matrix with rotationAngle and the reference hand pose
                Eigen::AngleAxisf aaR = Eigen::AngleAxisf(rotationAngleR, Eigen::Vector3f::UnitZ());
                Eigen::AngleAxisf aaL = Eigen::AngleAxisf(rotationAngleL, Eigen::Vector3f::UnitZ());
                Eigen::Matrix3f referenceOrientation = config.ReferenceOrientationMap.at("BimanualSideOpen");
                Eigen::Matrix3f graspOriR = aaR.toRotationMatrix() * referenceOrientation;
                Eigen::Matrix3f graspOriL = aaL.toRotationMatrix() * referenceOrientation;

                // Right hand grasp descriptor
                GraspDescription graspR;
                graspR.pose = Helpers::CreatePose(graspPosR, graspOriR);

                //graspR.preshape = Armar6BimanualGraspProviderModule::PreshapeType::Open;

                // Left hand grasp descriptor
                GraspDescription graspL;
                graspL.pose = Helpers::CreatePose(graspPosL, graspOriL);
                //graspL.approach = (v1 - v2).normalized();
                //graspL.preshape = Armar6BimanualGraspProviderModule::PreshapeType::Open;

                if (invertGrasp)
                {
                    longSideGraspForwardVector *= -1;
                    shortSideGraspForwardVector *= -1;
                }

                if (graspLongSide)
                {
                    if (longSideGraspForwardVectorScalarProductWithUnitXLessThanZero)
                    {
                        graspR.approach = (-longSideGraspForwardVector - shortSideGraspForwardVector).normalized();
                        graspL.approach = (longSideGraspForwardVector + shortSideGraspForwardVector).normalized();
                    }
                    else
                    {
                        graspR.approach = (-longSideGraspForwardVector - shortSideGraspForwardVector).normalized();
                        graspL.approach = (longSideGraspForwardVector + shortSideGraspForwardVector).normalized();
                    }
                }
                else
                {
                    if (!longSideGraspForwardVectorScalarProductWithUnitXLessThanZero)
                    {
                        graspR.approach = (-longSideGraspForwardVector - shortSideGraspForwardVector).normalized();
                        graspL.approach = (longSideGraspForwardVector + shortSideGraspForwardVector).normalized();
                    }
                    else
                    {
                        graspR.approach = (-longSideGraspForwardVector - shortSideGraspForwardVector).normalized();
                        graspL.approach = (longSideGraspForwardVector + shortSideGraspForwardVector).normalized();
                    }
                }

                graspR.inwards = graspR.approach;
                graspL.inwards = graspL.approach;

                graspR.graspName = graspLongSide ? "LongSide" : "ShortSide";
                graspL.graspName = graspLongSide ? "LongSide" : "ShortSide";

                std::pair<GraspDescription, GraspDescription> grasps_(graspR, graspL);
                grasps = grasps_;

                // Check if the distance between hands is sufficient
                float handsDistance = (graspPosR - graspPosL).norm();
                if (handsDistance < minimumHandDistance)
                {
                    //                    ARMARX_INFO << "Hand distance below minimum distance: " << handsDistance << " < " << minimumHandDistance << std::endl;
                    return false;
                }

                return true;
            };


            Armar6BimanualGraspProviderModule::Result result;
            result.inputPointCount = allPoints.size();
            result.center = center;
            result.v1 = v1;
            result.v2 = v2;
            result.v3 = v3;

            result.extent1 = extractExtent(p1);
            result.extent2 = extractExtent(p2);
            result.extent3 = extractExtent(p3);

            //            ARMARX_INFO << "Lowest point of the box: " << result.extent3.min;
            //            ARMARX_INFO << "Center position of the box: " << center;

            /*
             * Rechte Hand Side
            {
               "agent" : "Armar6_2",
               "frame" : "root",
               "qw" : 0.71437555551528931,
               "qx" : -0.69958436489105225,
               "qy" : -0.011289859190583229,
               "qz" : -0.011046878062188625,
               "type" : "::armarx::FramedPoseBase",
               "x" : 124.22021484375,
               "y" : 943.135498046875,
               "z" : 1115.685546875
            }
            */

            /*
             * Linke Hand Side
             {
               "agent" : "Armar6_2",
               "frame" : "root",
               "qw" : 0.70677864551544189,
               "qx" : -0.70733582973480225,
               "qy" : -0.0083755897358059883,
               "qz" : -0.0083645675331354141,
               "type" : "::armarx::FramedPoseBase",
               "x" : -175.23974609375,
               "y" : 937.176025390625,
               "z" : 1119.5755615234375
            }
            */



            /*for (const Eigen::Vector3f& p : allPoints)
            {
                result.points.push_back(Armar6BimanualGraspProviderModule::Point {p(0), p(1), p(2)});
            }*/


            std::string labels = std::to_string(label);

            /*debugDrawerHelper->drawArrow("v1_" + labels + "_pos", center, v1, DrawColor {1, 0, 0, 1}, 50, 3);
            debugDrawerHelper->drawArrow("v1_" + labels + "_neg", center, Eigen::Vector3f(-v1), DrawColor {1, 0, 0, 1}, 50, 3);
            debugDrawerHelper->drawArrow("v2_" + labels + "_pos", center, v2, DrawColor {0, 1, 0, 1}, 50, 3);
            debugDrawerHelper->drawArrow("v2_" + labels + "_neg", center, Eigen::Vector3f(-v2), DrawColor {0, 1, 0, 1}, 50, 3);
            debugDrawerHelper->drawArrow("v3_" + labels + "_pos", center, v3, DrawColor {0, 0, 1, 1}, 50, 3);
            debugDrawerHelper->drawArrow("v3_" + labels + "_neg", center, Eigen::Vector3f(-v3), DrawColor {0, 0, 1, 1}, 50, 3);*/

            //            debugDrawerHelper->drawLine("ex1_" + labels, l1.Get(result.extent1.p05), l1.Get(result.extent1.p95), 3, DrawColor {1, 0, 0, 1});
            //            debugDrawerHelper->drawLine("ex2_" + labels, l2.Get(result.extent2.p05), l2.Get(result.extent2.p95), 3, DrawColor {0, 1, 0, 1});
            //            debugDrawerHelper->drawLine("ex3_" + labels, l3.Get(result.extent3.p05), l3.Get(result.extent3.p95), 3, DrawColor {0, 0, 1, 1});

            //debugDrawerHelper->drawBox("box", box.GetPose(), Eigen::Vector3f(box.Size1(), box.Size2(), box.Size3()), DrawColor {1, 1, 0, 1});
            //            debugDrawerHelper->drawSphere("box",  center, 20, DrawColor {1, 0, 1, 1});
            //debugDrawerHelper->drawArrow("ex1_" + labels, l1.Get(0), l1.Dir(), DrawColor {1, 0, 0, 1}, result.extent1.p95, 5);

            //            if (config.getEffectivePointCloudVisu())
            //            {
            //                //                debugDrawerHelper->drawPointCloud("obj_" + labels, allPoints, 3, colorMap[label]);
            //            }
            //            if (config.enableBoundigBoxesVisu)
            //            {
            //                DrawColor color = DrawColor {0, 0, 1, 0.5f};
            //                //                debugDrawerHelper->drawBox("bbox_" + labels, box.GetPose(), box.SizeVec(), color);
            //            }

            Eigen::Vector3f preferredForwardTop(-1, 1, 0);
            Eigen::Vector3f preferredForwardSide(1, 0, 0);

            std::vector<float> signs = {1, -1};
            //            if (showInvertedHands)
            //            {
            //                signs = {1, -1};
            //            }
            //            else
            //            {
            //                signs = {1};
            //            }

            std::vector<bool> sides = {false, true};
            //            if (showShortAndLongSideHands)
            //            {
            //                sides = {false, true};
            //            }
            //            else
            //            {
            //                sides = {false};
            //            }

            std::vector<bool> diagonals = {false, true};

            if (graspTypeStr == "TopBlob")
            {
                signs = {1};
            }

            /*****************************************************************************************************************************************************************************
             *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             * *****************************************************************************************************************************************************************************
             */

            if (showAllGrasps)
            {
                candidates.clear();
            }

            for (bool diagonal : diagonals)
            {
                for (bool side : sides)
                {
                    for (float fwdSign : signs)
                    {
                        //                GraspDescription grasp;
                        grasping::BimanualGraspCandidatePtr candidate = new grasping::BimanualGraspCandidate();
                        candidate->executionHintsRight = new grasping::GraspCandidateExecutionHints();
                        candidate->executionHintsLeft = new grasping::GraspCandidateExecutionHints();
                        candidate->sourceInfo = new grasping::GraspCandidateSourceInfo();
                        candidate->reachabilityInfoRight = new grasping::GraspCandidateReachabilityInfo();
                        candidate->reachabilityInfoLeft = new grasping::GraspCandidateReachabilityInfo();



                        // Generate bimanual side grasps
                        std::pair<GraspDescription, GraspDescription> grasps;
                        bool success = false;
                        if (diagonal)
                        {
                            success = calculateBimanualGraspPoseDiagonal(result, grasps, fwdSign < 0, side);
                        }
                        else
                        {
                            success = calculateBimanualGraspPoseSide(result, grasps, fwdSign < 0, side);
                        }
                        //                    bool success = calculateBimanualGraspPoseSide(result, grasps, fwdSign < 0, side);
                        //                    if (!success)
                        //                    {
                        //                        continue;
                        //                    }
                        labels = std::to_string(label) + (fwdSign > 0 ? "_Forward_" : "_Backward_") + grasps.first.graspName + (diagonal ? "_Diagonal" : "");
                        candidate->graspName = labels;


                        auto checkGraspReachability = [&](GraspDescription & grasp, bool right)
                        {
                            std::string handName = right ? "Right" : "Left";

                            Armar6GraspTrajectoryPtr graspTrajectory = graspTrajectories.at(handName + "SideBimanual")->getTransformedToGraspPose(grasp.pose);

                            Eigen::VectorXf initialJointValues = Eigen::VectorXf::Zero(8);
                            initialJointValues(5) = M_PI / 2;

                            SimpleDiffIK::Parameters ikParams;
                            ikParams.stepsInitial = 50;
                            ikParams.stepsFineTune = 10;
                            SimpleDiffIK::Reachability reachability = SimpleDiffIK::CalculateReachability(graspTrajectory->getAllKeypointPoses(), initialJointValues, right ? rns : lns, right ? tcpR : tcpL, ikParams);

                            if (right)
                            {
                                candidate->executionHintsRight->approach = grasping::SideApproach;
                                candidate->executionHintsRight->graspTrajectoryName = handName + "SideBimanual";
                                candidate->graspPoseRight = new Pose(grasp.pose);
                                candidate->approachVectorRight = new Vector3(grasp.approach);
                                candidate->inwardsVectorRight = new Vector3(grasp.inwards);
                                candidate->reachabilityInfoRight->reachable = reachability.reachable;
                                candidate->reachabilityInfoRight->minimumJointLimitMargin = reachability.minimumJointLimitMargin;
                                candidate->reachabilityInfoRight->jointLimitMargins = ::math::Helpers::VectorToStd(reachability.jointLimitMargins);
                                candidate->reachabilityInfoRight->maxPosError = reachability.maxPosError;
                                candidate->reachabilityInfoRight->maxOriError = reachability.maxOriError;
                            }
                            else
                            {
                                candidate->executionHintsLeft->approach = grasping::SideApproach;
                                candidate->executionHintsLeft->graspTrajectoryName = handName + "SideBimanual";
                                candidate->graspPoseLeft = new Pose(grasp.pose);
                                candidate->approachVectorLeft = new Vector3(grasp.approach);
                                candidate->inwardsVectorLeft = new Vector3(grasp.inwards);
                                candidate->reachabilityInfoLeft->reachable = reachability.reachable;
                                candidate->reachabilityInfoLeft->minimumJointLimitMargin = reachability.minimumJointLimitMargin;
                                candidate->reachabilityInfoLeft->jointLimitMargins = ::math::Helpers::VectorToStd(reachability.jointLimitMargins);
                                candidate->reachabilityInfoLeft->maxPosError = reachability.maxPosError;
                                candidate->reachabilityInfoLeft->maxOriError = reachability.maxOriError;
                            }

                            candidate->sourceFrame = "root";
                            candidate->targetFrame = "root";
                            candidate->sourceInfo->referenceObjectName = objectName;
                            candidate->sourceInfo->segmentationLabelID = label;
                            candidate->sourceInfo->bbox = new grasping::BoundingBox();
                            candidate->sourceInfo->bbox->center = new Vector3(box.GetCenter());
                            candidate->sourceInfo->bbox->ha1 = new Vector3(box.GetDir1());
                            candidate->sourceInfo->bbox->ha2 = new Vector3(box.GetDir2());
                            candidate->sourceInfo->bbox->ha3 = new Vector3(box.GetDir3());

                            result.graspPose = grasp.pose;
                            result.approach = grasp.approach;
                            result.side = right ? "Right" : "Left";
                            result.graspDirection = graspType;

                            //                        debugDrawerHelper->drawBox("handPose_" + labels + (right ? "_r" : "_l"), grasp.pose, Eigen::Vector3f(40, 40, 40), DrawColor {1, 0, 1, 1});

                            candidate->robotPose = new Pose(localRobot->getGlobalPose());
                            candidate->groupNr = label;
                            candidate->providerName = getName();
                            candidate->objectType = isKnownObject ? objpose::KnownObject : objpose::UnknownObject;

                            //rns->setJointValues({0, 0, 0, 0, 0, M_PI / 2, 0, 0});
                            //lns->setJointValues({0, 0, 0, 0, 0, M_PI / 2, 0, 0});

                            //SimpleDiffIK::Reachability reachability = graspTrajectory->calculateReachability(right ? rns : lns, right ? tcpR : tcpL);

                            return reachability.reachable;
                        };



                        if (showAllGrasps)
                        {
                            bool reachable = success && checkGraspReachability(grasps.first, true) && checkGraspReachability(grasps.second, false);
                            //                            ARMARX_INFO << labels << ": reachable: " << reachable << ", success: " << success << ", right: " << checkGraspReachability(grasps.first, true) << ", left: " << checkGraspReachability(grasps.second, false);
                            candidates.push_back(candidate);

                            if ((showInvertedHands || fwdSign == 1) && (showShortAndLongSideHands || side == false) && (showDiagonalGrasps || diagonal == false))
                            {
                                visualizeGrasps(grasps.first, true, reachable, labels);
                                visualizeGrasps(grasps.second, false, reachable, labels);
                            }
                        }
                        //                    else
                        //                    {
                        //                        if (...)
                        //                        {
                        //                            for (auto& grasp : candidates)
                        //                            {
                        //                                if (grasp->graspName != chosenGraspName)
                        //                                {

                        //                                }
                        //                            }
                        //                        }
                        //                    }

                        resultList.push_back(result);
                    }
                }
            }

            detectedObjects++;
        }
    }

    debugObserver->setDebugDatafield(getName(), "objects", new Variant(ssDebug.str()));
    staticPlotter->addPlot(getName() + "::ObjectSizes", plots);


    debugDrawerHelper->cyclicCleanup();

    typename pcl::PointCloud<pcl::PointXYZRGBL>::Ptr resultCloud = outputCloudPtr;
    //ARMARX_INFO << deactivateSpam(1) << "Input cloud " << inputSize << ", filtered cloud " << resultCloud->points.size();
    //ARMARX_INFO << deactivateSpam(1) << "center (in root) =  " << center.transpose() << " primary vector: "  << v1.transpose();

    /*debugObserver->setDebugDatafield(getName(), "center_x", new Variant(center(0)));
    debugObserver->setDebugDatafield(getName(), "center_y", new Variant(center(1)));
    debugObserver->setDebugDatafield(getName(), "center_z", new Variant(center(2)));

    debugObserver->setDebugDatafield(getName(), "v1_x", new Variant(v1(0)));
    debugObserver->setDebugDatafield(getName(), "v1_y", new Variant(v1(1)));
    debugObserver->setDebugDatafield(getName(), "v1_z", new Variant(v1(2)));*/

    {
        std::unique_lock lock(resultMutex);
        //this->resultList = resultList;
        //processCount++;
        debugObserver->setDebugDatafield(getName(), "processCount", new Variant(processCount));
        debugObserver->setDebugDatafield(getName(), "detectedObjects", new Variant(detectedObjects));
    }
    //    ARMARX_INFO << "Reporting " << candidates.size() << " grasp candidates";
    //    graspCandidatesTopic->reportBimanualGraspCandidates(getName(), candidates);

    graspCandidateObserver->setSelectedBimanualCandidates(candidates);

    //debugDrawerTopic->setArrowVisu(getName(), "v1", result.center, new Vector3(v1), DrawColor {1, 0, 0, 1}, 50, 3);
    //debugDrawerTopic->setArrowVisu(getName(), "v1_neg", result.center, new Vector3(Eigen::Vector3f(-v1)), DrawColor {1, 0, 0, 1}, 50, 3);
    //debugDrawerTopic->setArrowVisu(getName(), "v2", result.center, new Vector3(v2), DrawColor {0, 1, 0, 1}, 50, 3);
    //debugDrawerTopic->setArrowVisu(getName(), "v2_neg", result.center, new Vector3(Eigen::Vector3f(-v2)), DrawColor {0, 1, 0, 1}, 50, 3);


    provideResultPointClouds(resultCloud, getName() + "Result");
    //provideResultPointClouds(capturedCloudPtr, "PCA_captured");
    //provideResultPointClouds(segmentedCloudPtr, "PCA_segmented");
}

void Armar6BimanualGraspProvider::setShowInvertedHands(bool value, const Ice::Current& curr)
{
    showInvertedHands = value;
}

void Armar6BimanualGraspProvider::setShowLongAndShortSide(bool value, const Ice::Current& curr)
{
    showShortAndLongSideHands = value;
}

void Armar6BimanualGraspProvider::setShowDiagonal(bool value, const Ice::Current&)
{
    showDiagonalGrasps = value;
}

void Armar6BimanualGraspProvider::setHandOffsetY(float value, const Ice::Current& curr)
{
    handOffsetY = value;
}

void Armar6BimanualGraspProvider::setHandDistanceOffsetX(float value, const Ice::Current& curr)
{
    handDistanceOffsetX = value;
}

void Armar6BimanualGraspProvider::setVisualization(bool showAllGrasps, const std::string& chosenGraspName, const Ice::Current&)
{
    this->showAllGrasps = showAllGrasps;
    this->chosenGraspName = chosenGraspName;
}

armarx::PropertyDefinitionsPtr Armar6BimanualGraspProvider::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new Armar6BimanualGraspProviderPropertyDefinitions(
            getConfigIdentifier()));
}


bool Armar6BimanualGraspProvider::BoundingBox::isInside(float x, float y, float z)
{
    return x >= x1 && x <= x2 &&
           y >= y1 && y <= y2 &&
           z >= z1 && z <= z2;
}




/*
CapturedPointSeq armarx::Armar6BimanualGraspProvider::getSegmentedPoints(const Ice::Current&)
{
    std::unique_lock lock(capturedPointsMutex);
    return capturedPoints;
}*/

StringVariantBaseMap Armar6BimanualGraspProvider::Config::getAsVariantMap()
{
    StringVariantBaseMap map;
    map["ObjectDepthPreshapeThreshold"] = new Variant(ObjectDepthPreshapeThreshold);
    map["ObjectHeightPreshapeThreshold"] = new Variant(ObjectHeightPreshapeThreshold);
    map["HandGraspingRadius"] = new Variant(HandGraspingRadius);
    return map;
}


void armarx::Armar6BimanualGraspProvider::requestService(const std::string& providerName, Ice::Int relativeTimeoutMS, const Ice::Current&)
{
    if (providerName == getName() || providerName == "AllGraspProviders")
    {
        serviceRequestTopic->requestService(getProperty<std::string>("serviceProviderName").getValue(), relativeTimeoutMS);
    }

}

void armarx::Armar6BimanualGraspProvider::setServiceConfig(const std::string& providerName, const armarx::StringVariantBaseMap& config, const Ice::Current&)
{
    if (providerName == "__any__GraspProvider__" || providerName == getName())
    {
        if (config.count("DisableVisuOverrideTimeoutSeconds") > 0)
        {
            disableVisuUntil = TimeUtil::GetTime() + IceUtil::Time::secondsDouble(config.at("DisableVisuOverrideTimeoutSeconds")->getFloat());
        }
    }
}
/*
Armar6BimanualGraspProviderModule::ResultSeq armarx::Armar6BimanualGraspProvider::getResult(const Ice::Current&)
{
    std::unique_lock lock(resultMutex);
    return resultList;
}

Ice::Int Armar6BimanualGraspProvider::getProcessCount(const Ice::Current&)
{
    std::unique_lock lock(resultMutex);
    return processCount;
}
*/
