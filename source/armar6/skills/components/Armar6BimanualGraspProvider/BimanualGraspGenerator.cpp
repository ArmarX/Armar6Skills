#include "BimanualGraspUtil.h"





void generateBimanualGrasps(std::vector<Eigen::Vector3f>& allPoints, uint32_t label, Config& config, std::string& objectName, StringVector2fSeqDict& plots)
{
    auto determineGraspType = [&](const Box3D & box)
    {
        float height = box.Size3();
        float width = box.Size1();
        return width > height ? Armar6BimanualGraspProviderModule::GraspType::Top : Armar6BimanualGraspProviderModule::GraspType::Side;
    };

    FitRectangle fitBox(90);
    Box3D box = fitBox.Fit(allPoints);
    Eigen::Vector3f center = box.GetPoint(0, 0, 0);
    Armar6BimanualGraspProviderModule::GraspType graspType = determineGraspType(box);

    std::string graspTypeStr = maptools::GetValueOrDefault(config.ObjectGraspTypeMap, objectName, "calculate");
    if (graspTypeStr == "TopBlob")
    {
        graspType = Armar6BimanualGraspProviderModule::GraspType::Top;
    }
    if (graspTypeStr == "Top")
    {
        graspType = Armar6BimanualGraspProviderModule::GraspType::Top;
    }
    if (graspTypeStr == "SideTop")
    {
        graspType = Armar6BimanualGraspProviderModule::GraspType::Side;
    }

    Vector2fSeq boxSizePlot;
    for (size_t i = 0; i < fitBox.results.size(); i++)
    {
        boxSizePlot.push_back(Vector2f {(float)i, fitBox.results.at(i).Volume()});
    }
    plots[objectName] = boxSizePlot;



    std::vector<Eigen::Vector3f> filteredPoints;
    if (graspType == Armar6BimanualGraspProviderModule::GraspType::Top)
    {
        for (const Eigen::Vector3f& p : allPoints)
        {
            if ((p - center).norm() < config.HandGraspingRadius)
            {
                filteredPoints.push_back(p);
            }
        }

        if (filteredPoints.size() < 10)
        {
            filteredPoints = allPoints;
        }
    }
    else
    {
        filteredPoints = allPoints;
    }




    box = fitBox.Fit(filteredPoints);
    center = box.GetPoint(0, 0, 0);

    Eigen::Vector3f v1 = box.GetDir1().normalized();
    Eigen::Vector3f v2 = box.GetDir2().normalized();
    Eigen::Vector3f v3 = box.GetDir3().normalized();
    Line l1 = Line(center, v1);
    Line l2 = Line(center, v2);
    Line l3 = Line(center, v3);

    Percentile p1(FitRectangle::ProjectPoints(l1, filteredPoints));
    Percentile p2(FitRectangle::ProjectPoints(l2, filteredPoints));
    Percentile p3(FitRectangle::ProjectPoints(l3, filteredPoints));


    auto extractExtent = [&](const Percentile & percentile)
    {
        Armar6BimanualGraspProviderModule::Extent extent;
        extent.min = percentile.get(0.00f);
        extent.max = percentile.get(1.00f);
        extent.p05 = percentile.get(0.05f);
        extent.p10 = percentile.get(0.10f);
        extent.p90 = percentile.get(0.90f);
        extent.p95 = percentile.get(0.95f);
        return extent;
    };



    auto calculateGraspPoseBimanualTop = [&](const Armar6BimanualGraspProviderModule::Result & result, const Eigen::Vector3f & preferredForward, const std::string & graspTypeStr)
    {
        Eigen::Vector3f graspForwardVector = v2;

        Armar6BimanualGraspProviderModule::PreshapeType preshape = Armar6BimanualGraspProviderModule::PreshapeType::Open;


        //@@@ disabled for now
        //float height = result.extent3.p95 - result.extent3.p05;
        //float depth = result.extent2.p95 - result.extent2.p05;
        //if (depth < config.ObjectDepthPreshapeThreshold && height < config.ObjectHeightPreshapeThreshold)
        //{
        //    preshape = Armar6BimanualGraspProviderModule::PreshapeType::Preshaped;
        //}

        if (graspForwardVector.dot(preferredForward) < 0)
        {
            graspForwardVector = -graspForwardVector;
        }
        float angleV2 = Helpers::Angle(Eigen::Vector2f(graspForwardVector(0), graspForwardVector(1)));
        //ARMARX_INFO << "angleV2 = " << angleV2 << std::endl;
        float angleGraspOri = Helpers::Angle(Eigen::Vector2f(0, 1));
        //ARMARX_INFO << "angleGraspOri = " << angleGraspOri << std::endl;
        float rotationAngle = Helpers::AngleModPI(angleV2 - angleGraspOri);
        //ARMARX_INFO << "rotationAngle = " << rotationAngle << std::endl;
        Eigen::Matrix3f referenceOrientation = config.ReferenceOrientationMap.at("RightTopOpen");
        Eigen::AngleAxisf aa = Eigen::AngleAxisf(rotationAngle, Eigen::Vector3f::UnitZ());
        Eigen::AngleAxisf rotate1 = Eigen::AngleAxisf(EIGEN_PI / 2, Eigen::Vector3f::UnitY());
        Eigen::Matrix3f graspOri = rotate1.toRotationMatrix() * aa.toRotationMatrix() * referenceOrientation;

        if (graspTypeStr == "TopBlob")
        {
            graspOri = config.ReferenceOrientationMap.at("RightTopBlob");
        }

        ARMARX_INFO << "result.extent1: " << result.extent1.max << std::endl;
        ARMARX_INFO << "result.extent1.p95: " << result.extent1.p95 << std::endl;

        Eigen::Vector3f graspPosRight = center + v3 * result.extent3.p95 + graspOri * config.GraspOffsetTCP.at("Right")
                                        - v1 * result.extent1.p95;



        GraspDescription grasp;
        grasp.pose = Helpers::CreatePose(graspPosRight, graspOri);
        grasp.approach = Eigen::Vector3f::UnitZ();
        grasp.preshape = preshape;

        return grasp;
    };

    auto calculateGraspPoseTop = [&](const Armar6BimanualGraspProviderModule::Result & result, const Eigen::Vector3f & preferredForward, const std::string & graspTypeStr)
    {
        Eigen::Vector3f graspForwardVector = v2;

        Armar6BimanualGraspProviderModule::PreshapeType preshape = Armar6BimanualGraspProviderModule::PreshapeType::Open;


        //@@@ disabled for now
        //float height = result.extent3.p95 - result.extent3.p05;
        //float depth = result.extent2.p95 - result.extent2.p05;
        //if (depth < config.ObjectDepthPreshapeThreshold && height < config.ObjectHeightPreshapeThreshold)
        //{
        //    preshape = Armar6BimanualGraspProviderModule::PreshapeType::Preshaped;
        //}

        if (graspForwardVector.dot(preferredForward) < 0)
        {
            graspForwardVector = -graspForwardVector;
        }
        float angleV2 = Helpers::Angle(Eigen::Vector2f(graspForwardVector(0), graspForwardVector(1)));
        float angleGraspOri = Helpers::Angle(Eigen::Vector2f(0, 1));
        float rotationAngle = Helpers::AngleModPI(angleV2 - angleGraspOri);
        Eigen::Matrix3f referenceOrientation = config.ReferenceOrientationMap.at("RightTopOpen");
        Eigen::AngleAxisf aa = Eigen::AngleAxisf(rotationAngle, Eigen::Vector3f::UnitZ());
        Eigen::Matrix3f graspOri = aa.toRotationMatrix() * referenceOrientation;

        if (graspTypeStr == "TopBlob")
        {
            graspOri = config.ReferenceOrientationMap.at("RightTopBlob");
        }

        Eigen::Vector3f graspPos = center + v3 * result.extent3.p95 + graspOri * config.GraspOffsetTCP.at("Right");

        GraspDescription grasp;
        grasp.pose = Helpers::CreatePose(graspPos, graspOri);
        grasp.approach = Eigen::Vector3f::UnitZ();
        grasp.preshape = preshape;

        return grasp;
    };

    auto calculateGraspPoseSide = [&](const Armar6BimanualGraspProviderModule::Result & result, const Eigen::Vector3f & preferredForward)
    {
        Eigen::Vector3f graspForwardVector = v1;
        Eigen::Vector3f offset_v1 = v1 * result.extent1.p95;

        if (graspForwardVector.dot(preferredForward) < 0)
        {
            graspForwardVector = -graspForwardVector;
            offset_v1 = v1 * result.extent1.p05;
        }

        float angleV1 = Helpers::Angle(Eigen::Vector2f(graspForwardVector(0), graspForwardVector(1)));
        float angleGraspOri = Helpers::Angle(Eigen::Vector2f(1, 0));
        float rotationAngle = Helpers::AngleModPI(angleV1 - angleGraspOri);
        Eigen::Matrix3f referenceOrientation = config.ReferenceOrientationMap.at("RightSideOpen");
        Eigen::AngleAxisf aa = Eigen::AngleAxisf(rotationAngle, Eigen::Vector3f::UnitZ());
        Eigen::Matrix3f graspOri = aa.toRotationMatrix() * referenceOrientation;

        Eigen::Vector3f graspPos = center + offset_v1;

        GraspDescription grasp;
        grasp.pose = Helpers::CreatePose(graspPos, graspOri);
        grasp.approach = graspForwardVector.normalized();
        grasp.preshape = Armar6BimanualGraspProviderModule::PreshapeType::Open;

        return grasp;
    };

    Armar6BimanualGraspProviderModule::Result result;
    result.inputPointCount = allPoints.size();
    result.center = new Vector3(center);
    result.v1 = new Vector3(v1);
    result.v2 = new Vector3(v2);
    result.v3 = new Vector3(v3);

    result.extent1 = extractExtent(p1);
    result.extent2 = extractExtent(p2);
    result.extent3 = extractExtent(p3);


    /*
     * Rechte Hand
    {
       "agent" : "Armar6_2",
       "frame" : "root",
       "qw" : 0.71437555551528931,
       "qx" : -0.69958436489105225,
       "qy" : -0.011289859190583229,
       "qz" : -0.011046878062188625,
       "type" : "::armarx::FramedPoseBase",
       "x" : 124.22021484375,
       "y" : 943.135498046875,
       "z" : 1115.685546875
    }
    */

    /*
     * Linke Hand
     {
       "agent" : "Armar6_2",
       "frame" : "root",
       "qw" : 0.70677864551544189,
       "qx" : -0.70733582973480225,
       "qy" : -0.0083755897358059883,
       "qz" : -0.0083645675331354141,
       "type" : "::armarx::FramedPoseBase",
       "x" : -175.23974609375,
       "y" : 937.176025390625,
       "z" : 1119.5755615234375
    }
    */



    for (const Eigen::Vector3f& p : allPoints)
    {
        result.points.push_back(Armar6BimanualGraspProviderModule::Point {p(0), p(1), p(2)});
    }


    std::string labels = std::to_string(label);

    /*debugDrawerHelper->drawArrow("v1_" + labels + "_pos", center, v1, DrawColor {1, 0, 0, 1}, 50, 3);
    debugDrawerHelper->drawArrow("v1_" + labels + "_neg", center, Eigen::Vector3f(-v1), DrawColor {1, 0, 0, 1}, 50, 3);
    debugDrawerHelper->drawArrow("v2_" + labels + "_pos", center, v2, DrawColor {0, 1, 0, 1}, 50, 3);
    debugDrawerHelper->drawArrow("v2_" + labels + "_neg", center, Eigen::Vector3f(-v2), DrawColor {0, 1, 0, 1}, 50, 3);
    debugDrawerHelper->drawArrow("v3_" + labels + "_pos", center, v3, DrawColor {0, 0, 1, 1}, 50, 3);
    debugDrawerHelper->drawArrow("v3_" + labels + "_neg", center, Eigen::Vector3f(-v3), DrawColor {0, 0, 1, 1}, 50, 3);*/

    debugDrawerHelper->drawLine("ex1_" + labels, l1.Get(result.extent1.p05), l1.Get(result.extent1.p95), 3, DrawColor {1, 0, 0, 1});
    debugDrawerHelper->drawLine("ex2_" + labels, l2.Get(result.extent2.p05), l2.Get(result.extent2.p95), 3, DrawColor {0, 1, 0, 1});
    debugDrawerHelper->drawLine("ex3_" + labels, l3.Get(result.extent3.p05), l3.Get(result.extent3.p95), 3, DrawColor {0, 0, 1, 1});

    debugDrawerHelper->drawBox("box", box.GetPose(), Eigen::Vector3f(box.Size1(), box.Size2(), box.Size3()), DrawColor {1, 1, 0, 1});
    //debugDrawerHelper->drawArrow("ex1_" + labels, l1.Get(0), l1.Dir(), DrawColor {1, 0, 0, 1}, result.extent1.p95, 5);

    if (config.getEffectivePointCloudVisu())
    {
        debugDrawerHelper->drawPointCloud("obj_" + labels, allPoints, 3, colorMap[label]);
    }
    if (config.enableBoundigBoxesVisu)
    {
        DrawColor color = DrawColor {0, 0, 1, 0.5f};
        debugDrawerHelper->drawBox("bbox_" + labels, box.GetPose(), box.SizeVec(), color);
    }

    Eigen::Vector3f preferredForwardTop(-1, 1, 0);
    Eigen::Vector3f preferredForwardSide(1, 0, 0);

    std::vector<float> signs = {1, -1};
    if (graspTypeStr == "TopBlob")
    {
        signs = {1};
    }

    for (float fwdSign : signs)
    {
        labels = std::to_string(label) + (fwdSign > 0 ? "_fwd" : "_rev");

        GraspDescription grasp;
        grasping::GraspCandidatePtr candidate = new grasping::GraspCandidate();
        candidate->executionHints = new grasping::GraspCandidateExecutionHints();
        candidate->sourceInfo = new grasping::GraspCandidateSourceInfo();
        candidate->reachabilityInfo = new grasping::GraspCandidateReachabilityInfo();

        Armar6GraspTrajectoryPtr graspTrajectory;
        grasp.pose = Helpers::CreatePose(center, Eigen::Matrix3f::Identity());
        /*
        if (graspType == Armar6BimanualGraspProviderModule::GraspType::Top)
        {
            grasp = calculateGraspPoseTop(result, fwdSign * preferredForwardTop, graspTypeStr);
            candidate->executionHints->approach = grasping::TopApproach;
            candidate->executionHints->graspTrajectoryName = "RightTopOpen";
            graspTrajectory = graspTrajectories.at("RightTopOpen");
        }
        else if (graspType == Armar6BimanualGraspProviderModule::GraspType::Side)
        {
            grasp = calculateGraspPoseSide(result, fwdSign * preferredForwardSide);
            candidate->executionHints->approach = grasping::SideApproach;
            candidate->executionHints->graspTrajectoryName = "RightSideOpen";
            graspTrajectory = graspTrajectories.at("RightSideOpen");
        }
        else
        {
            ARMARX_ERROR << "Unsupported grasp type";
        }
        */
        grasp = calculateGraspPoseBimanualTop(result, fwdSign * preferredForwardTop, graspTypeStr);
        candidate->executionHints->approach = grasping::TopApproach;
        candidate->executionHints->graspTrajectoryName = "RightTopOpen";
        graspTrajectory = graspTrajectories.at("RightTopOpen");


        graspTrajectory = graspTrajectory->getTransformedToGraspPose(grasp.pose);


        std::map<std::string, float> fingerJointsVisu;
        if (grasp.preshape == Armar6BimanualGraspProviderModule::PreshapeType::Open)
        {
            fingerJointsVisu = config.OpenVisu;
            candidate->executionHints->preshape = grasping::OpenAperture;
        }
        else if (grasp.preshape == Armar6BimanualGraspProviderModule::PreshapeType::Preshaped)
        {
            fingerJointsVisu = config.PreshapeVisu;
            candidate->executionHints->preshape = grasping::PreshapedAperture;
        }
        else
        {
            ARMARX_ERROR << "Unsupported preshape type";
        }

        result.preshape = grasp.preshape;
        result.graspPose = new Pose(grasp.pose);
        result.approach = new Vector3(grasp.approach);
        result.side = "Right";
        result.graspDirection = graspType;

        candidate->robotPose = new Pose(localRobot->getGlobalPose());
        candidate->graspPose = new Pose(grasp.pose);
        candidate->groupNr = label;
        candidate->providerName = getName();
        candidate->side = "Right";
        candidate->approachVector = new Vector3(grasp.approach);
        candidate->graspSuccessProbability = 0.5f;
        candidate->objectType = isKnownObject ? grasping::KnownObject : grasping::UnknownObject;
        candidate->sourceFrame = "root";
        candidate->targetFrame = "root";
        candidate->sourceInfo->referenceObjectName = objectName;
        candidate->sourceInfo->segmentationLabelID = label;
        candidate->sourceInfo->bbox = new grasping::BoundingBox();
        candidate->sourceInfo->bbox->center = new Vector3(box.GetCenter());
        candidate->sourceInfo->bbox->ha1 = new Vector3(box.GetDir1());
        candidate->sourceInfo->bbox->ha2 = new Vector3(box.GetDir2());
        candidate->sourceInfo->bbox->ha3 = new Vector3(box.GetDir3());

        SimpleDiffIK::Reachability reachability = graspTrajectory->calculateReachability(rns, tcp);

        candidate->reachabilityInfo->reachable = reachability.reachable;
        candidate->reachabilityInfo->minimumJointLimitMargin = reachability.minimumJointLimitMargin;
        candidate->reachabilityInfo->jointLimitMargins = ::math::Helpers::VectorToStd(reachability.jointLimitMargins);
        candidate->reachabilityInfo->maxPosError = reachability.maxPosError;
        candidate->reachabilityInfo->maxOriError = reachability.maxOriError;
        candidates.push_back(candidate);


        if (config.getEffectiveCandidatesVisu())
        {
            DrawColor color = (reachability.reachable ? DrawColor {0, 1, 0, 1} : DrawColor {1, 0, 0, 1});

            Eigen::Matrix4f tcp2handRoot = rnh->getRobotArm(RobotNameHelper::LocationRight, localRobot).getTCP()->getPoseInRootFrame().inverse() * localRobot->getRobotNode("Hand R Root")->getPoseInRootFrame();

            debugDrawerHelper->drawRobot("grasp_" + labels, "armar6_rt/robotmodel/Armar6-SH/Armar6-RightHand-v3.xml", "armar6_rt", grasp.pose * tcp2handRoot, color);
            debugDrawerHelper->setRobotConfig("grasp_" + labels, fingerJointsVisu);
            //debugDrawerHelper->drawBox("tcp_" + labels, grasp.pose, 5, DrawColor {0, 1, 0, 1});
        }


        ssDebug << labels << " " << objectName << " (points=" << allPoints.size();
        if (reachability.reachable)
        {
            ssDebug << ", jointMargin=" << reachability.minimumJointLimitMargin;
        }
        else
        {
            ssDebug << ", not reachable";
        }

        ssDebug << ") " << candidate->side << GraspTypeToString(graspType) << PreshapeTypeToString(grasp.preshape) << " " << ((int)center(0)) << "," << ((int)center(1)) << "," << ((int)center(2)) << std::endl;



        resultList.push_back(result);
    }

    detectedObjects++;
}


