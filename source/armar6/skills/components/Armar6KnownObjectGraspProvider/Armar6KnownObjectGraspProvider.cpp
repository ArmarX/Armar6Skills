/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::Armar6KnownObjectGraspProvider
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6KnownObjectGraspProvider.h"

#include <filesystem>

#include <SimoxUtility/json.h>

#include <SimoxUtility/math/pose/pose.h>
#include <SimoxUtility/math/pose/transform.h>
#include <VirtualRobot/XML/ObjectIO.h>
#include <VirtualRobot/Grasping/GraspSet.h>

#include <ArmarXCore/core/time/CycleUtil.h>

#include <RobotAPI/libraries/core/Pose.h>



namespace armarx
{

    Armar6KnownObjectGraspProviderPropertyDefinitions::Armar6KnownObjectGraspProviderPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
    }

    armarx::PropertyDefinitionsPtr Armar6KnownObjectGraspProvider::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new Armar6KnownObjectGraspProviderPropertyDefinitions(getConfigIdentifier());

        defs->topic(debugObserver);
        defs->topic(graspCandidatesTopic);

        defs->optional(service.updatesPerSecond, "service.updatesPerSecond",
                       "The desired amount of updates per second while requested (Hz).");
        defs->defineOptionalProperty<int>("service.initialTimeoutMS", 0,
                                          "Initial relative timeout, i.e. time to provide candidates after start.\n"
                                          "-1: Run forever.");

        defs->optional(maxExtent, "grasps.maxExtent",
                       "Maximal extent of the object along the hand's 'forward axis' to generate a grasp candidate.");

        std::vector<std::string> sides { RobotNameHelper::LocationLeft, RobotNameHelper::LocationRight };
        for (const auto& side : sides)
        {
            defs->defineOptionalPropertyVector("grasp.tcpOffsetsLocal." + side, tcpOffsetsLocalPerSide.at(side),
                                               "TCP offset in following frame: "
                                               "\n- z: normal direction, out of object"
                                               "\n- x: away from the fingers, towards the wrist"
                                               "\n y: right of hand");

            defs->defineOptionalPropertyVector("grasp.approachDirsTcp." + side, approachDirsTcpPerSide.at(side),
                                               "Approach direction in TCP frame of each hand.");
        }

        return defs;
    }

    std::string Armar6KnownObjectGraspProvider::getDefaultName() const
    {
        return "Armar6KnownObjectGraspProvider";
    }


    void Armar6KnownObjectGraspProvider::onInitComponent()
    {
        handFilePerSide =
        {
            { RobotNameHelper::LocationLeft, "armar6_rt/robotmodel/Armar6-SH/Armar6-LeftHand-v3.xml" },
            { RobotNameHelper::LocationRight, "armar6_rt/robotmodel/Armar6-SH/Armar6-RightHand-v3.xml" },
        };


        std::vector<std::string> sides { RobotNameHelper::LocationLeft, RobotNameHelper::LocationRight };
        for (const auto& side : sides)
        {
            tcpOffsetsLocalPerSide[side] = getProperty<Eigen::Vector3f>("grasp.tcpOffsetsLocal." + side);
            approachDirsTcpPerSide[side] = getProperty<Eigen::Vector3f>("grasp.approachDirsTcp." + side);
        }
    }


    void Armar6KnownObjectGraspProvider::onConnectComponent()
    {
        if (!robot)
        {
            robot = RobotState::addRobot("robot", VirtualRobot::RobotIO::RobotDescription::eStructure);
        }
        robotNameHelper = RobotNameHelper::Create(RobotState::getRobotStateComponent()->getRobotInfo(), nullptr);
        for (const std::string& loc :
             {
                 RobotNameHelper::LocationLeft, RobotNameHelper::LocationRight
             })
        {
            sidePerRobotEEF[robotNameHelper->getArm(loc).getEndEffector()] = loc;
        }
        ARMARX_VERBOSE << "EEF to Side map:\n" ; //<< robotEefToSideMap;

        robotNameHelper->getRobotArm(RobotNameHelper::LocationLeft, robot).getHandRootNode();


        int initialTimeoutMS = getProperty<int>("service.initialTimeoutMS");
        service.updateTimeout(initialTimeoutMS);
        service.start([this] { this->provideGraspCandidates(); });

        createRemoteGuiTab();
        RemoteGui_startRunningTask();
    }


    void Armar6KnownObjectGraspProvider::onDisconnectComponent()
    {
    }


    void Armar6KnownObjectGraspProvider::onExitComponent()
    {
    }


    void Armar6KnownObjectGraspProvider::requestService(const std::string& providerName, Ice::Int relativeTimeoutMS, const Ice::Current&)
    {
        if (providerName == getName())
        {
            service.updateTimeout(relativeTimeoutMS);
            ARMARX_INFO << "Service requested for " << relativeTimeoutMS << " ms. "
                        << "\nRunning until: \t" << (service.timeout.toSeconds() == -1
                                ? "forever"
                                : TimeUtil::toStringDateTime(service.timeout))
                        << "\n(Now:          \t" << TimeUtil::toStringDateTime(TimeUtil::GetTime()) << ")";
        }
    }

    void Armar6KnownObjectGraspProvider::setServiceConfig(const std::string& providerName, const StringVariantBaseMap& config, const Ice::Current&)
    {
        if (providerName == getName())
        {
            (void) config;
        }
    }


    void Armar6KnownObjectGraspProvider::provideGraspCandidates()
    {
        ARMARX_DEBUG << "Providing grasp candidates";
        ARMARX_CHECK_NOT_NULL(graspCandidatesTopic);
        objpose::ObjectPoseSeq objectPoses = ObjectPoseClient::getObjectPoses();
        ARMARX_DEBUG << "Got " << objectPoses.size() << " object poses";
        grasping::GraspCandidateSeq candidates = makeGraspCandidates(objectPoses);
        ARMARX_DEBUG << "Made " << candidates.size() << " candidates";
        graspCandidatesTopic->reportGraspCandidates(getName(), candidates);
        ARMARX_DEBUG << "Reported grasp candidates.";
    }


    grasping::GraspCandidateSeq
    Armar6KnownObjectGraspProvider::makeGraspCandidates(const objpose::ObjectPoseSeq& objectPoses)
    {
        grasping::GraspCandidateSeq candidates;
        for (const auto& objectPose : objectPoses)
        {
            grasping::GraspCandidateSeq objectCandidates = makeGraspCandidates(objectPose);
            for (const auto& cand : objectCandidates)
            {
                candidates.push_back(cand);
            }
        }

        arviz.commit(vizLayers);
        vizLayers.clear();

        return candidates;
    }


    grasping::GraspCandidateSeq
    Armar6KnownObjectGraspProvider::makeGraspCandidates(const objpose::ObjectPose& objectPose)
    {
        namespace fs = std::filesystem;
        grasping::GraspCandidateSeq candidates;

        ARMARX_DEBUG << "Making grasp candidates for object " << objectPose.objectID;

        if (std::optional<ObjectInfo> objectInfo = objectFinder.findObject(objectPose.objectID))
        {
            PackageFileLocation graspsFile = objectInfo->file("xml", "_grasps-a6");
            if (fs::is_regular_file(graspsFile.absolutePath))
            {
                ARMARX_DEBUG << "- Found grasp file " << graspsFile.absolutePath;
                grasping::GraspCandidateSeq cands = makeGraspCandidatesFromGraspsFile(objectPose, graspsFile.absolutePath);
                candidates = cands;
            }
        }
        if (candidates.empty())
        {
            if (objectPose.localOOBB)
            {
                ARMARX_DEBUG << "- Creating grasps using bounding box (OOBB).";
                grasping::GraspCandidateSeq cands = makeGraspCandidatesFromOOBB(objectPose);
                candidates = cands;
            }
            else
            {
                ARMARX_INFO << "Cannot create grasp candidates for " << objectPose.objectID << " (no grasps file, no OOBB).";
            }
        }

        ARMARX_DEBUG << "- Created " << candidates.size() << " candidates.";
        return candidates;
    }


    grasping::GraspCandidateSeq
    Armar6KnownObjectGraspProvider::makeGraspCandidatesFromGraspsFile(const objpose::ObjectPose& objectPose, const std::string& graspsFile)
    {
        grasping::GraspCandidateSeq candidates;

        viz::Layer layer = arviz.layer("TCP poses from file");

        VirtualRobot::ManipulationObjectPtr object = VirtualRobot::ObjectIO::loadManipulationObject(graspsFile);
        ARMARX_CHECK_NOT_NULL(object);
        ARMARX_DEBUG << "- Loaded object " << objectPose.objectID << " with " << object->getAllGraspSets().size() << " grasp sets";
        for (VirtualRobot::GraspSetPtr graspSet : object->getAllGraspSets())
        {
            ARMARX_CHECK_NOT_NULL(graspSet);
            ARMARX_DEBUG << "- - Grasp set '" << graspSet->getName() << " with " << graspSet->getGrasps().size() << " grasps";
            int iGrasp = 0;
            for (VirtualRobot::GraspPtr grasp : graspSet->getGrasps())
            {
                std::string side = sidePerRobotEEF.at(grasp->getEefName());

                std::stringstream idss;
                idss << objectPose.objectID.str() << "::" << graspSet->getName() << "::" << iGrasp << "_" << grasp->getName();

                grasping::GraspCandidatePtr candidate = assembleGraspCandidate(
                        objectPose, grasp->getTransformation().inverse(), side, &layer, idss.str());
                if (candidate)
                {
                    candidates.push_back(candidate);
                }

                iGrasp++;
            }
        }

        vizLayers.push_back(layer);


        return candidates;
    }

    grasping::GraspCandidateSeq Armar6KnownObjectGraspProvider::makeGraspCandidatesFromOOBB(
        const objpose::ObjectPose& objectPose)
    {
        ARMARX_CHECK(objectPose.localOOBB);
        const simox::OrientedBoxf& oobb = *objectPose.localOOBB;

        std::vector<int> signs {-1, 1};
        const std::vector<simox::Color> colors { simox::Color::red(), simox::Color::green(), simox::Color::blue() };

        grasping::GraspCandidateSeq candidates;

        auto eefs = robot->getEndEffectors();
        for (auto eef : eefs)
        {
            const std::string side = this->sidePerRobotEEF.at(eef->getName());
            const Eigen::Vector3f tcpOffsetLocal = tcpOffsetsLocalPerSide.at(side);
            const Eigen::Vector3f approachDirTcp = approachDirsTcpPerSide.at(side);

            viz::Layer layer = arviz.layer(objectPose.objectID.str() + " " + side);

            for (int s : signs)
            {
                for (int ax = 0; ax < 3; ++ax)
                {
                    for (size_t rot = 0; rot < 4; ++rot)
                    {
                        const Eigen::Vector3f pointOnObject = oobb.center() + s * 0.5f * oobb.dimension(ax) * oobb.axis(ax);

                        /* A local orientation with the following properties:
                         * - z axis: points in normal direction out of the object
                         * - x axis: points away from the fingers, to the wrist
                         * - y axis: points to the right of the hand
                         */
                        const Eigen::Quaternionf oriOnObject =
                            Eigen::Quaternionf::FromTwoVectors(Eigen::Vector3f::UnitZ(), s * oobb.axis(ax))
                            * Eigen::AngleAxisf(float(rot * M_PI_2), Eigen::Vector3f::UnitZ());

                        const Eigen::Matrix4f poseOnObject = simox::math::pose(pointOnObject, oriOnObject);

                        const float extent = std::abs((oriOnObject.inverse() * oobb.rotation() * oobb.dimensions()).x());
                        if (extent > maxExtent)
                        {
                            continue;
                        }

                        Eigen::Vector3f tcpPos = pointOnObject + oriOnObject * tcpOffsetLocal;
                        Eigen::Quaternionf tcpOri = oriOnObject * Eigen::Quaternionf::FromTwoVectors(approachDirTcp, - Eigen::Vector3f::UnitZ());

                        Eigen::Matrix4f tcpPoseObject = simox::math::pose(tcpPos, tcpOri);

                        std::stringstream ss;
                        ss << "sign=" << s << " ax=" << ax << " rot=" << rot;
                        grasping::GraspCandidatePtr candidate = assembleGraspCandidate(objectPose, tcpPoseObject, side, &layer, ss.str());
                        if (candidate)
                        {
                            candidates.push_back(candidate);
                        }

                        if constexpr(false)
                        {
                            Eigen::Matrix4f poseOnObjectGlobal = objectPose.objectPoseGlobal * poseOnObject;
                            Eigen::Matrix4f tcpPoseGlobal = objectPose.objectPoseGlobal * tcpPoseObject;

                            std::stringstream ss;
                            ss << "sign" << s << " ax" << ax << " rot" << rot;
                            std::string id = ss.str();

                            layer.add(viz::Box("box " + id).pose(tcpPoseGlobal).size(10).color(colors.at(size_t(ax))));
                            layer.add(viz::Pose("local pose " + id).pose(poseOnObjectGlobal).scale(0.2f));
                            layer.add(viz::Pose("pose" + id).pose(tcpPoseGlobal).scale(0.25));

                            Eigen::Matrix4f handPoseGlobal = tcpPoseGlobal * robotNameHelper->getRobotArm(side, robot).getTcp2HandRootTransform();
                            layer.add(viz::Pose("hand pose " + id).pose(handPoseGlobal).scale(0.5));
                            viz::Robot robot = viz::Robot("hand " + id).file("armar6_rt", handFilePerSide.at(side)).pose(handPoseGlobal).scale(0.5);
                            if (extent > maxExtent)
                            {
                                robot.overrideColor(simox::Color::red());
                            }
                            layer.add(robot);

                            if constexpr(true)
                            {
                                layer.add(viz::Arrow("extent arrow " + id)
                                          .fromTo(simox::math::position(poseOnObjectGlobal),
                                                  simox::math::transform_position(poseOnObjectGlobal, Eigen::Vector3f(extent / 2, 0, 0)))
                                          .width(5).color(simox::Color::orange()));
                            }
                        }
                    }
                }
            }

            vizLayers.push_back(layer);

        }

        return candidates;
    }

    grasping::GraspCandidatePtr Armar6KnownObjectGraspProvider::assembleGraspCandidate(
        const objpose::ObjectPose& objectPose, const Eigen::Matrix4f& tcpPoseObject, const std::string& side,
        viz::Layer* layer, const std::string& visID)
    {
        Eigen::Matrix4f tcpPoseGlobal = objectPose.objectPoseGlobal * tcpPoseObject;
        Eigen::Matrix4f tcpPoseRobot = objectPose.objectPoseRobot * tcpPoseObject;

        std::optional<grasping::ApproachType> optApproachType = determineApproachType(tcpPoseRobot, side);
        if (!optApproachType)
        {
            // Skip this grasp.
            return nullptr;
        }
        grasping::ApproachType approachType = *optApproachType;


        grasping::GraspCandidatePtr cand = new grasping::GraspCandidate();

        cand->graspPose = new Pose(tcpPoseRobot);
        cand->robotPose = new Pose(objectPose.robotPose);
        switch (approachType)
        {
            case grasping::ApproachType::TopApproach:
                cand->approachVector = new Vector3(Eigen::Vector3f::UnitZ().eval());
                break;
            case grasping::ApproachType::SideApproach:
            {
                Eigen::Vector3f objectPos;
                if (objectPose.localOOBB)
                {
                    objectPos = simox::math::transform_position(objectPose.objectPoseRobot, objectPose.localOOBB->center());
                }
                else
                {
                    objectPos = simox::math::position(objectPose.objectPoseRobot);
                }
                Eigen::Vector3f approachVector = objectPos - simox::math::position(tcpPoseRobot);
                // Todo ("graspForwardVector")
                cand->approachVector = new Vector3(approachVector);
            }


            break;
            default:
                ARMARX_WARNING << "Unexpected approach type.";
        }

        cand->sourceFrame = robot->getRootNode()->getName();  // frame where graspPose is located
        // is that right?
        cand->targetFrame = robot->getRootNode()->getName();  // frame which should be moved to graspPose
        cand->side = side;

        cand->graspSuccessProbability = 0.5;

        cand->objectType = objpose::ObjectType::KnownObject;
        cand->groupNr = -1;
        cand->providerName = getName();

        cand->sourceInfo = new grasping::GraspCandidateSourceInfo();
        cand->sourceInfo->referenceObjectPose = new Pose(objectPose.objectPoseRobot);
        cand->sourceInfo->referenceObjectName = objectPose.objectID.str();
        cand->sourceInfo->segmentationLabelID = -1;
        if (objectPose.localOOBB)
        {
            cand->sourceInfo->bbox = convertBoundingBox(objectPose.localOOBB->transformed(objectPose.objectPoseRobot));
        }

        cand->executionHints = new grasping::GraspCandidateExecutionHints();
        cand->executionHints->preshape = grasping::ApertureType::AnyAperture; // Todo
        cand->executionHints->approach = approachType;
        switch (approachType)
        {
            case grasping::ApproachType::TopApproach:
                cand->executionHints->graspTrajectoryName = trajectoryNameTop;
                break;
            case grasping::ApproachType::SideApproach:
                cand->executionHints->graspTrajectoryName = trajectoryNameSide;
                break;
            default:
                ARMARX_WARNING << "Unexpected approach type.";
        }


        // cand->reachabilityInfo;

        if (!visID.empty() && layer)
        {
            layer->add(viz::Pose("tcp " + visID).pose(tcpPoseGlobal));
            viz::Color color;
            switch (approachType)
            {
                case grasping::ApproachType::TopApproach:
                    color = simox::Color::cyan();
                    break;
                case grasping::ApproachType::SideApproach:
                    color = simox::Color::lime();
                    break;
                case grasping::ApproachType::AnyApproach:
                    color = simox::Color::red();
                    break;
            }
            layer->add(viz::Box("approach type " + visID).pose(tcpPoseGlobal).size(10).color(color));

            Eigen::Matrix4f handPoseGlobal = tcpPoseGlobal * robotNameHelper->getRobotArm(side, robot).getTcp2HandRootTransform();
            layer->add(viz::Pose("hand pose " + visID).pose(handPoseGlobal).scale(0.5));
            layer->add(viz::Robot("hand " + visID).file("armar6_rt", handFilePerSide.at(side)).pose(handPoseGlobal).overrideColor(color));

        }

        return cand;
    }

    grasping::BoundingBoxPtr Armar6KnownObjectGraspProvider::convertBoundingBox(const simox::OrientedBoxf& oobb)
    {
        std::vector<Eigen::Vector3f> halfAxes(3);
        for (int i = 0; i < 3; ++i)
        {
            halfAxes[size_t(i)] = 0.5f * oobb.dimension(i) * oobb.axis(i);
        }
        // Sort descending by length.
        std::sort(halfAxes.begin(), halfAxes.end(), [](const Eigen::Vector3f & a, const Eigen::Vector3f & b)
        {
            return a.squaredNorm() > b.squaredNorm();
        });

        grasping::BoundingBoxPtr bbox = new grasping::BoundingBox();
        bbox->center = new Vector3(oobb.center());
        bbox->ha1 = new Vector3(halfAxes[0]);
        bbox->ha2 = new Vector3(halfAxes[1]);
        bbox->ha3 = new Vector3(halfAxes[2]);
        return bbox;
    }


    std::optional<grasping::ApproachType>
    Armar6KnownObjectGraspProvider::determineApproachType(
        const Eigen::Matrix4f& tcpPoseRobot, const std::string& side)
    {
        const Eigen::Vector3f gravity = - Eigen::Vector3f::UnitZ();

        const Eigen::Vector3f position = simox::math::position(tcpPoseRobot);
        const Eigen::Matrix3f orientation = simox::math::orientation(tcpPoseRobot);

        const Eigen::Vector3f forward = orientation.col(2);  // z axis
        Eigen::Vector3f up;
        if (side == RobotNameHelper::LocationLeft)
        {
            up = - orientation.col(0);
        }
        else
        {
            up = orientation.col(0);
        }
        Eigen::Vector3f sideOutwards = orientation.col(1);
        (void) sideOutwards;

        /* Infeasible grasps:
         * - hand's back points away from robot (origin)
         * - hand's back points downwards
         */
        if ((-forward).dot(position.normalized()) >= 0.f || (-forward).dot(gravity) >= 0.f)
        {
            return std::nullopt;
        }

        /* Feasible top grasp:
         * - hand's up points upwards (at max 45 deg)
         */
        if (up.dot(-gravity) >= 0.5f)
        {
            return grasping::ApproachType::TopApproach;
        }
        /* Feasible side grasp:
         * - hand's up and forward point sidewards (at max 45 deg)
         */
        if (std::abs(up.dot(gravity)) <= 0.5f && std::abs(forward.dot(gravity)) <= 0.5f)
        {
            return grasping::ApproachType::SideApproach;
        }

        // Anything else is invalid.
        return std::nullopt;
    }


    void Armar6KnownObjectGraspProvider::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        GridLayout requestGrid;
        int row = 0;
        tab.requestTimeoutMsSpinBox.setRange(0, 1e6);
        tab.requestTimeoutMsSpinBox.setValue(100);
        requestGrid.add(Label("Timeout (ms):"), {row, 0}).add(tab.requestTimeoutMsSpinBox, {row, 1});
        row++;

        tab.requestButton.setLabel("Request");
        requestGrid.add(tab.requestButton, {row, 0}, {1, 2});

        VBoxLayout root { requestGrid, VSpacer() };
        RemoteGui_createTab(getName(), root, &tab);
    }

    void Armar6KnownObjectGraspProvider::RemoteGui_update()
    {
        if (tab.requestButton.wasClicked())
        {
            int timeoutMs = tab.requestTimeoutMsSpinBox.getValue();
            requestService(getName(), timeoutMs);
        }
    }

}
