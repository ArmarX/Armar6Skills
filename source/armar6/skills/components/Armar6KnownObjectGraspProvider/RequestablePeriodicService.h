#pragma once

#include <atomic>
#include <condition_variable>
#include <functional>
#include <mutex>

#include <IceUtil/Time.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>


namespace armarx
{

    /**
     * @brief A requestable periodic service.
     *
     * Design to work with the RequestableServiceInterface.
     */
    class RequestablePeriodicService
    {
    public:

        void start(std::function<void(void)> fn);
        void updateTimeout(int relativeTimeoutMs);
        void setUpdatesPerSecond(int updatesPerSecond);

        void run(std::function<void(void)> fn);

        void waitUntilRequested();


    public:

        SimpleRunningTask<>::pointer_type task;

        std::mutex mutex;
        std::condition_variable conditionVariable;
        // If `IceUtil::Time::seconds(-1)`, run forever.
        IceUtil::Time timeout = IceUtil::Time::seconds(0);

        float updatesPerSecond = 10;
        std::atomic_bool updatesPerSecondChanged = false;

    };

}
