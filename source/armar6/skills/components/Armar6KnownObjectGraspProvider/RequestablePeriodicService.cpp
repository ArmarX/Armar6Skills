#include "RequestablePeriodicService.h"

#include <ArmarXCore/core/time/CycleUtil.h>



namespace armarx
{

    void RequestablePeriodicService::start(std::function<void ()> fn)
    {
        task = new SimpleRunningTask<>([this, fn]()
        {
            this->run(fn);
        });
        task->start();
    }

    void RequestablePeriodicService::run(std::function<void ()> fn)
    {
        CycleUtil cycle(int(1000 / updatesPerSecond));

        while (!task->isStopped())
        {
            // Check timeout and wait if necessary.
            waitUntilRequested();

            // Call the function.
            fn();

            cycle.waitForCycleDuration();
            if (updatesPerSecondChanged.exchange(false))
            {
                cycle = CycleUtil(int(1000 / updatesPerSecond));
            }
        }
    }

    void RequestablePeriodicService::updateTimeout(int relativeTimeoutMs)
    {
        bool notify = false;
        {
            std::scoped_lock lock(mutex);

            if (this->timeout.toSeconds() == -1)
            {
                // Already running forever.
                notify = true;
            }
            else if (relativeTimeoutMs == -1)
            {
                // Requested to run forever.
                this->timeout = IceUtil::Time::seconds(-1);
                notify = true;
            }
            else
            {
                IceUtil::Time newTimeout = TimeUtil::GetTime() + IceUtil::Time::milliSeconds(relativeTimeoutMs);
                if (this->timeout < newTimeout)
                {
                    this->timeout = newTimeout;
                    notify = true;
                }
            }
        }
        if (notify)
        {
            conditionVariable.notify_one();
        }
    }

    void RequestablePeriodicService::waitUntilRequested()
    {
        std::unique_lock lock(mutex);
        conditionVariable.wait(lock, [this]
        {
            return timeout.toSeconds() == -1 || TimeUtil::GetTime() < timeout;
        });
    }

    void RequestablePeriodicService::setUpdatesPerSecond(int updatesPerSecond)
    {
        this->updatesPerSecond = updatesPerSecond;
        this->updatesPerSecondChanged = true;
    }

}

