/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::Armar6KnownObjectGraspProvider
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
#include <ArmarXGui/libraries/RemoteGui/Client/Tab.h>
#include <ArmarXGui/libraries/RemoteGui/Client/Widgets.h>

#include <RobotAPI/interface/units/GraspCandidateProviderInterface.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>


#include <armar6/skills/interface/Armar6KnownObjectGraspProviderInterface.h>

#include "RequestablePeriodicService.h"


namespace armarx
{

    /**
     * @class Armar6KnownObjectGraspProviderPropertyDefinitions
     * @brief Property definitions of `Armar6KnownObjectGraspProvider`.
     */
    class Armar6KnownObjectGraspProviderPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        Armar6KnownObjectGraspProviderPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-Armar6KnownObjectGraspProvider Armar6KnownObjectGraspProvider
     * @ingroup armar6_skills-Components
     * A description of the component Armar6KnownObjectGraspProvider.
     *
     * @class Armar6KnownObjectGraspProvider
     * @ingroup Component-Armar6KnownObjectGraspProvider
     * @brief Brief description of class Armar6KnownObjectGraspProvider.
     *
     * Detailed description of class Armar6KnownObjectGraspProvider.
     */
    class Armar6KnownObjectGraspProvider :
        virtual public armarx::Component
        , virtual public armarx::Armar6KnownObjectGraspProviderInterface
        , virtual public armarx::RobotStateComponentPluginUser
        , virtual public armarx::ObjectPoseClientPluginUser
        , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        , virtual public armarx::ArVizComponentPluginUser
    {
        using RobotState = RobotStateComponentPluginUser;

    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // RequestableServiceListenerInterface interface
    public:
        void requestService(const std::string& providerName, Ice::Int relativeTimeoutMS, const Ice::Current& = Ice::emptyCurrent) override;
        void setServiceConfig(const std::string& providerName, const StringVariantBaseMap& config, const Ice::Current& = Ice::emptyCurrent) override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


        void createRemoteGuiTab();
        void RemoteGui_update() override;


    private:

        void provideGraspCandidates();

        grasping::GraspCandidateSeq makeGraspCandidates(const objpose::ObjectPoseSeq& objectPoses);
        grasping::GraspCandidateSeq makeGraspCandidates(const objpose::ObjectPose& objectPose);

        grasping::GraspCandidateSeq makeGraspCandidatesFromGraspsFile(const objpose::ObjectPose& objectPose, const std::string& graspsFile);
        grasping::GraspCandidateSeq makeGraspCandidatesFromOOBB(const objpose::ObjectPose& objectPose);

        grasping::GraspCandidatePtr assembleGraspCandidate(
            const objpose::ObjectPose& objectPose, const Eigen::Matrix4f& tcpPoseObject, const std::string& side,
            viz::Layer* layer = nullptr, const std::string& visID = "");

        static grasping::BoundingBoxPtr convertBoundingBox(const simox::OrientedBoxf& oobb);

        /**
         * @brief Determine the approach type.
         * @param tcpPoseRobot
         * @param side
         * @return The approach type. If none, the grasp is infeasible.
         */
        std::optional<grasping::ApproachType> determineApproachType(const Eigen::Matrix4f& tcpPoseRobot, const std::string& side);


    private:

        armarx::DebugObserverInterfacePrx debugObserver;

        RequestablePeriodicService service;
        grasping::GraspCandidatesTopicInterfacePrx graspCandidatesTopic;

        std::vector<viz::Layer> vizLayers;


        // Objects

        armarx::ObjectFinder objectFinder;


        // Robot

        VirtualRobot::RobotPtr robot;
        RobotNameHelperPtr robotNameHelper;

        const std::vector<std::string> sides { RobotNameHelper::LocationLeft, RobotNameHelper::LocationRight };
        std::map<std::string, std::string> sidePerRobotEEF;
        std::map<std::string, std::string> handFilePerSide;


        // OOBB grasp parameters

        float maxExtent = 100;  // 10 cm
        std::map<std::string, Eigen::Vector3f> tcpOffsetsLocalPerSide =
        {
            { RobotNameHelper::LocationLeft, Eigen::Vector3f(0, 0, 10)},
            { RobotNameHelper::LocationRight, Eigen::Vector3f(0, 0, 10)},
        };

        std::map<std::string, Eigen::Vector3f> approachDirsTcpPerSide =
        {
            { RobotNameHelper::LocationLeft, Eigen::Vector3f(1, 0, 0.5).normalized()},
            { RobotNameHelper::LocationRight, Eigen::Vector3f(-1, 0, 0.5).normalized()},
        };
        std::string trajectoryNameTop = "RightTopOpen";
        std::string trajectoryNameSide = "RightSideOpen";


        // Remote Gui
    private:

        struct Tab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::IntSpinBox requestTimeoutMsSpinBox;
            armarx::RemoteGui::Client::Button requestButton;
        };
        Tab tab;

    };
}
