/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::Armar6CartesianPositionControlComponent
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
//#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>
#include <RobotAPI/interface/components/CartesianPositionControlInterface.h>

#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>

#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>

#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <RobotAPI/libraries/natik/CartesianNaturalPositionControllerProxy.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/RemoteGuiComponentPlugin.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/KinematicUnitComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotUnitComponentPlugin.h>

namespace armarx
{
    /**
     * @class Armar6CartesianPositionControlComponentPropertyDefinitions
     * @brief Property definitions of `Armar6CartesianPositionControlComponent`.
     */
    class Armar6CartesianPositionControlComponentPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        Armar6CartesianPositionControlComponentPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-Armar6CartesianPositionControlComponent Armar6CartesianPositionControlComponent
     * @ingroup armar6_skills-Components
     * A description of the component Armar6CartesianPositionControlComponent.
     *
     * @class Armar6CartesianPositionControlComponent
     * @ingroup Component-Armar6CartesianPositionControlComponent
     * @brief Brief description of class Armar6CartesianPositionControlComponent.
     *
     * Detailed description of class Armar6CartesianPositionControlComponent.
     */
    class Armar6CartesianPositionControlComponent :
        virtual public armarx::Component,
        virtual public CartesianPositionControlInterface,
        virtual public RemoteGuiComponentPluginUser,
        virtual public ArVizComponentPluginUser,
        virtual public KinematicUnitComponentPluginUser,
        virtual public RobotStateComponentPluginUser,
        virtual public RobotUnitComponentPluginUser
    {
    public:

        class SideController
        {
        public:
            SideController(std::string side, RobotStateComponentInterfacePrx robotStateComponent, KinematicUnitInterfacePrx kinematicUnit, RobotUnitInterfacePrx robotUnit);
            void start();
            void stop();
            void setEnabled(bool enabled);
            void setTarget(const Eigen::Matrix4f& target, bool setOrientation);
            void setFTLimit(Ice::Float maxForce, Ice::Float maxTorque);
            void setCurrentFTAsOffset();
            void resetFTOffset();
            int getFTTresholdExceededCounter();
            void emulateFTTresholdExceeded();

        private:
            void run();

        private:
            std::string side;
            RobotStateComponentInterfacePrx robotStateComponent;
            KinematicUnitInterfacePrx kinematicUnit;
            RobotUnitInterfacePrx robotUnit;
            RunningTask<SideController>::pointer_type task;
            CartesianNaturalPositionControllerProxyPtr posCtrl;
            float _maxForce = -1;
            float _maxTorque = -1;
            int _ftTresholdExceededCounter = 0;
            bool _emulateFTflag = false;
        };

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        // Private methods and member variables go here.

        /// Debug observer. Used to visualize e.g. time series.
        armarx::DebugObserverInterfacePrx debugObserver;
        /// Debug drawer. Used for 3D visualization.
        //armarx::DebugDrawerTopic debugDrawer;

        std::map<std::string, SideController> controllers;



        // CartesianPositionControlInterface interface
    public:
        void setEnabled(const std::string& side, bool enabled, const Ice::Current&) override;
        void setTarget(const std::string& side, const Eigen::Matrix4f& target, bool setOrientation, const Ice::Current&) override;
        void setFTLimit(const std::string& side, Ice::Float maxForce, Ice::Float maxTorque, const Ice::Current&) override;
        void setCurrentFTAsOffset(const std::string& side, const Ice::Current&) override;
        void resetFTOffset(const std::string& side, const Ice::Current&) override;
        int getFTTresholdExceededCounter(const std::string& side, const Ice::Current&) override;
        void emulateFTTresholdExceeded(const std::string& side, const ::Ice::Current& = ::Ice::emptyCurrent) override;
    };
}
