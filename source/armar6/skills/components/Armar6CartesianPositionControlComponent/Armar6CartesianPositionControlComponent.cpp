/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::Armar6CartesianPositionControlComponent
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6CartesianPositionControlComponent.h"

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <RobotAPI/libraries/natik/CartesianNaturalPositionControllerProxy.h>
#include <RobotAPI/libraries/natik/NaturalIK.h>

#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>

#include <ArmarXCore/core/time/TimeUtil.h>


namespace armarx
{
    Armar6CartesianPositionControlComponentPropertyDefinitions::Armar6CartesianPositionControlComponentPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        //defineRequiredProperty<std::string>("PropertyName", "Description");
        //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");

        defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens to.");
        defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
        defineOptionalProperty<std::string>("KinematicUnitName", "Armar6KinematicUnit", "Name of KinematicUnit");
        defineOptionalProperty<std::string>("RobotUnitName", "Armar6Unit", "Name of RobotUnit");

        //defineOptionalProperty<std::string>("MyProxyName", "MyProxy", "Name of the proxy to be used");
    }


    std::string Armar6CartesianPositionControlComponent::getDefaultName() const
    {
        return "Armar6CartesianPositionControlComponent";
    }


    void Armar6CartesianPositionControlComponent::onInitComponent()
    {
        // Register offered topices and used proxies here.
        offeringTopicFromProperty("DebugObserverName");
        // debugDrawer.offeringTopic(*this);  // Calls this->offeringTopic().

        // Using a proxy will cause the component to wait until the proxy is available.
        // usingProxyFromProperty("MyProxyName");
    }


    void Armar6CartesianPositionControlComponent::onConnectComponent()
    {
        // Get topics and proxies here. Pass the *InterfacePrx type as template argument.
        getTopicFromProperty(debugObserver, "DebugObserverName");
        // debugDrawer.getTopic(*this);  // Calls this->getTopic().

        // getProxyFromProperty<MyProxyInterfacePrx>("MyProxyName");

        controllers.clear();
        controllers.insert(std::make_pair("Left", Armar6CartesianPositionControlComponent::SideController("Left", getRobotStateComponent(), getKinematicUnit(), getRobotUnit())));
        controllers.insert(std::make_pair("Right", Armar6CartesianPositionControlComponent::SideController("Right", getRobotStateComponent(), getKinematicUnit(), getRobotUnit())));
        controllers.at("Left").start();
        controllers.at("Right").start();
    }


    void Armar6CartesianPositionControlComponent::onDisconnectComponent()
    {
        controllers.at("Left").stop();
        controllers.at("Right").stop();
    }


    void Armar6CartesianPositionControlComponent::onExitComponent()
    {

    }




    armarx::PropertyDefinitionsPtr Armar6CartesianPositionControlComponent::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new Armar6CartesianPositionControlComponentPropertyDefinitions(
                getConfigIdentifier()));
    }

    void Armar6CartesianPositionControlComponent::setEnabled(const std::string& side, bool enabled, const Ice::Current&)
    {
        controllers.at(side).setEnabled(enabled);
    }

    void Armar6CartesianPositionControlComponent::setTarget(const std::string& side, const Eigen::Matrix4f& target, bool setOrientation, const Ice::Current&)
    {
        controllers.at(side).setTarget(target, setOrientation);
    }

    void Armar6CartesianPositionControlComponent::setFTLimit(const std::string& side, Ice::Float maxForce, Ice::Float maxTorque, const Ice::Current&)
    {
        controllers.at(side).setFTLimit(maxForce, maxTorque);
    }

    void Armar6CartesianPositionControlComponent::setCurrentFTAsOffset(const std::string& side, const Ice::Current&)
    {
        controllers.at(side).setCurrentFTAsOffset();
    }

    void Armar6CartesianPositionControlComponent::resetFTOffset(const std::string& side, const Ice::Current&)
    {
        controllers.at(side).resetFTOffset();
    }

    int Armar6CartesianPositionControlComponent::getFTTresholdExceededCounter(const std::string& side, const Ice::Current&)
    {
        return controllers.at(side).getFTTresholdExceededCounter();
    }

    void Armar6CartesianPositionControlComponent::emulateFTTresholdExceeded(const std::string& side, const Ice::Current&)
    {
        controllers.at(side).emulateFTTresholdExceeded();
    }

    Armar6CartesianPositionControlComponent::SideController::SideController(std::string side, RobotStateComponentInterfacePrx robotStateComponent, KinematicUnitInterfacePrx kinematicUnit, RobotUnitInterfacePrx robotUnit)
        : side(side), robotStateComponent(robotStateComponent), kinematicUnit(kinematicUnit), robotUnit(robotUnit)
    {

    }

    void Armar6CartesianPositionControlComponent::SideController::start()
    {
        task = new RunningTask<Armar6CartesianPositionControlComponent::SideController>(this, &Armar6CartesianPositionControlComponent::SideController::run);
        task->start();
    }

    void Armar6CartesianPositionControlComponent::SideController::stop()
    {
        task->stop();
    }

    void Armar6CartesianPositionControlComponent::SideController::setEnabled(bool enabled)
    {
        if (enabled)
        {
            posCtrl->getInternalController()->activateController();
        }
        else
        {
            posCtrl->getInternalController()->deactivateController();
        }
    }

    void Armar6CartesianPositionControlComponent::SideController::setTarget(const Eigen::Matrix4f& target, bool setOrientation)
    {
        //no natik here

        //posCtrl->setTarget(target, setOrientation ? NaturalDiffIK::Mode::All : NaturalDiffIK::Mode::Position);
        posCtrl->getInternalController()->setTarget(target, Eigen::Vector3f::Zero(), setOrientation);
    }

    void Armar6CartesianPositionControlComponent::SideController::setFTLimit(Ice::Float maxForce, Ice::Float maxTorque)
    {
        _maxForce = maxForce;
        _maxTorque = maxTorque;
    }

    void Armar6CartesianPositionControlComponent::SideController::setCurrentFTAsOffset()
    {
        FTSensorValue ftval = posCtrl->getInternalController()->getAverageFTValue();
        posCtrl->getInternalController()->setFTOffset(ftval);
    }

    void Armar6CartesianPositionControlComponent::SideController::resetFTOffset()
    {
        posCtrl->getInternalController()->resetFTOffset();
    }

    int Armar6CartesianPositionControlComponent::SideController::getFTTresholdExceededCounter()
    {
        return _ftTresholdExceededCounter;
    }

    void Armar6CartesianPositionControlComponent::SideController::emulateFTTresholdExceeded()
    {
        _emulateFTflag = true;
    }

    void Armar6CartesianPositionControlComponent::SideController::run()
    {
        RobotNameHelperPtr rnh = RobotNameHelper::Create(robotStateComponent->getRobotInfo(), StatechartProfilePtr());
        VirtualRobot::RobotPtr robot = RemoteRobot::createLocalClone(robotStateComponent);
        RobotNameHelper::Arm arm = rnh->getArm(side);
        RemoteRobot::synchronizeLocalClone(robot, robotStateComponent);
        RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr cla = rns->getNode(0);
        VirtualRobot::RobotNodePtr sho1 = rns->getNode(1);
        ARMARX_IMPORTANT_S << VAROUT(cla->getJointValue());
        cla->setJointValue(0);
        Eigen::Vector3f shoulder = sho1->getPositionInRootFrame();
        VirtualRobot::RobotNodePtr elb = rns->getNode(4);
        VirtualRobot::RobotNodePtr wri1 = rns->getNode(6);
        VirtualRobot::RobotNodePtr tcp = rns->getTCP();
        NaturalIK::ArmJoints natArm;
        natArm.rns = rns;
        natArm.shoulder = sho1;
        natArm.elbow = elb;
        natArm.tcp = tcp;


        KinematicUnitHelper kinUnitHelper(kinematicUnit);
        //DebugDrawerHelper debugDrawerHelper(getDebugDrawerTopic(), "ExecuteNaturalGrasp", robot);
        //Armar6HandV2Helper handHelper(getKinematicUnit(), getRobotUnitObserver(), side, in.getIsSimulation());

        //grasping::GraspCandidateSeq candidates = getGraspCandidateObserver()->getSelectedCandidates();
        //ARMARX_CHECK(candidates.size() == 1);

        //grasping::GraspCandidatePtr cnd = candidates.at(0);
        //GraspCandidateHelper gch(cnd, robot);

        //std::string packageName = "armar6_skills";
        //armarx::CMakePackageFinder finder(packageName);
        //std::string dataDir = finder.getDataDir() + "/" + packageName;

        //ARMARX_IMPORTANT << VAROUT(cnd->executionHints->graspTrajectoryName);
        //Armar6GraspTrajectoryPtr graspTrajectory = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/" + cnd->executionHints->graspTrajectoryName + ".xml");
        //ARMARX_IMPORTANT << "Read grasp trajectory, length: " << graspTrajectory->calculateLength().pos << ", duration: " << graspTrajectory->getDuration();
        //handHelper.shapeHand(graspTrajectory->GetHandValues(0));


        //float thumbOffset = in.getThumbOffset();


        std::vector<float> jointCosts = std::vector<float>(rns->getSize(), 1);
        jointCosts.at(0) = 4;

        RemoteRobot::synchronizeLocalClone(robot, robotStateComponent);
        NaturalIK ik(side, shoulder, 1.3f);
        float upper_arm_length = (sho1->getPositionInRootFrame() - elb->getPositionInRootFrame()).norm();
        float lower_arm_length = (elb->getPositionInRootFrame() - wri1->getPositionInRootFrame()).norm()
                                 + (wri1->getPositionInRootFrame() - tcp->getPositionInRootFrame()).norm();
        ik.setUpperArmLength(upper_arm_length);
        ik.setLowerArmLength(lower_arm_length);
        NJointCartesianNaturalPositionControllerConfigPtr config = CartesianNaturalPositionControllerProxy::MakeConfig(rns->getName(), elb->getName());
        //config->ftName = in.getFTSensorNameMap().at(side);
        config->runCfg.jointCosts = jointCosts;
        config->runCfg.elbowKp = 0;
        config->runCfg.jointLimitAvoidanceKp = 0;
        posCtrl.reset(new CartesianNaturalPositionControllerProxy(ik, natArm, robotUnit, "Armar6CartesianPositionControlComponent_" + side, config));
        posCtrl->setActivateControllerOnInit(false);
        posCtrl->init();


        while (!task->isStopped())
        {
            FTSensorValue ftval = posCtrl->getCurrentFTValue(true);
            if ((_maxForce > 0 && ftval.force.norm() > _maxForce) || (_maxTorque > 0 && ftval.torque.norm() > _maxTorque))
            {
                _ftTresholdExceededCounter++;
                posCtrl->getInternalController()->stopMovement();
            }
            if (_emulateFTflag)
            {
                _emulateFTflag = false;
                _ftTresholdExceededCounter++;
            }
            TimeUtil::SleepMS(10);
        }
    }

}
