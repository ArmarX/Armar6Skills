#include "Preprocessing.h"

#include <SimoxUtility/math/pose/pose.h>

#include <ArmarXCore/core/logging/Logging.h>

#include <VisionX/libraries/PointCloudTools/crop.h>
#include <VisionX/libraries/PointCloudTools/downsampling.h>
#include <VisionX/libraries/PointCloudTools/FramedPointCloud.h>
#include <VisionX/libraries/PointCloudTools/tools.h>


namespace armarx::guard_pose_estimator
{

    Preprocessing::Preprocessing()
    {

    }


    void Preprocessing::defineProperties(armarx::PropertyDefinitionContainer& props, std::string prefix)
    {
        props.defineOptionalProperty(prefix + "cropFloorHeight", cropFloorHeight, "Height of the floor for cropping.");

        props.defineOptionalProperty(prefix + "globalCropBoxEnabled", globalCropBoxEnabled, "Enable/disable box global cropping box (disables floor cropping).");
        props.defineOptionalPropertyVector(prefix + "globalCropBoxMin", globalCropBoxMin, "Minimum of globalCropping box.");
        props.defineOptionalPropertyVector(prefix + "globalCropBoxMax", globalCropBoxMax, "Maximum of globalCropping box.");

        props.defineOptionalProperty(prefix + "downsamplingLeafSize", downsamplingLeafSize,
                                     "Leaf size of downsampling (larger value reduces point cloud size). \n"
                                     "0 disables downsampling.");
    }

    void Preprocessing::readProperties(armarx::PropertyUser& props, std::string prefix)
    {
        props.getProperty(cropFloorHeight, prefix + "cropFloorHeight");

        props.getProperty(globalCropBoxEnabled, prefix + "globalCropBoxEnabled");
        props.getProperty(globalCropBoxMin, prefix + "globalCropBoxMin");
        props.getProperty(globalCropBoxMax, prefix + "globalCropBoxMax");

        props.getProperty(downsamplingLeafSize, prefix + "downsamplingLeafSize");
    }


    auto Preprocessing::preprocess(pcl::PointCloud<PointT>::ConstPtr input, VirtualRobot::RobotPtr robot) -> pcl::PointCloud<PointT>::Ptr
    {
        using PointCloudT = pcl::PointCloud<PointT>;

        pcl::PointIndices::Ptr cropIndices;
        if (globalCropBoxEnabled)
        {
            ARMARX_DEBUG << "Cropping by global crop box ... "
                         << "([" << globalCropBoxMin.transpose() << "] to [" << globalCropBoxMax.transpose() << "])";
            visionx::FramedPointCloud<PointT> framed(robot->getRootNode()->getName());
            pcl::copyPointCloud(*input, *framed.cloud);
            framed.changeFrame(armarx::GlobalFrame, robot);
            cropIndices = visionx::tools::getCropIndices(*framed.cloud, globalCropBoxMin, globalCropBoxMax);

            if (visu && visu->level >= 2)
            {
                armarx::FramedPose cropBoxPose(simox::math::pose(0.5 * (globalCropBoxMin + globalCropBoxMax)),
                                               armarx::GlobalFrame, "");
                cropBoxPose.changeFrame(robot, robot->getRootNode()->getName());

                visu->addLayer("CropBox").add(
                    viz::Box("crop")
                    .pose(cropBoxPose.toEigen())
                    .size(globalCropBoxMax - globalCropBoxMin)
                    .color(simox::Color::cyan(255, 64)));
            }
        }
        else
        {
            ARMARX_DEBUG << "Cropping floor at height " << cropFloorHeight;
            cropIndices = cropFloorIndices(*input, cropFloorHeight);
        }

        if (downsamplingLeafSize > 0)
        {
            ARMARX_DEBUG << "Downsampling ...";
            return visionx::tools::downsampleByVoxelGrid<PointCloudT>(
                       input, downsamplingLeafSize, cropIndices);
        }
        else
        {
            // Apply cropping indices.
            pcl::PointCloud<PointT>::Ptr copy(new pcl::PointCloud<PointT>);
            return visionx::getPointCloudWithIndices(*input, *cropIndices);
        }
    }


    pcl::PointIndices::Ptr Preprocessing::cropFloorIndices(const pcl::PointCloud<PointT>& input, float height)
    {
        pcl::PointIndices::Ptr indices(new pcl::PointIndices);
        for (size_t i = 0; i < input.size(); ++i)
        {
            if (input[i].z > height)
            {
                indices->indices.push_back(int(i));
            }
        }
        return indices;
    }


}
