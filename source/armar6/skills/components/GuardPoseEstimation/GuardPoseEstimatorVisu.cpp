#include "GuardPoseEstimatorVisu.h"

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotConfig.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>


namespace armarx
{

    void GuardPoseEstimatorVisu::defineProperties(armarx::PropertyDefinitionContainer& props, std::string prefix)
    {
        props.defineOptionalProperty<int>(
            prefix + "level", this->level,
            "Visualization verbosity level."
            "\n- 0: No visualization."
            "\n- 1: Guard pose and plane"
            "\n- 2: 1 + Guard point cloud"
            "\n- 3: 2 + Input point cloud"
        );

        props.defineOptionalProperty<bool>(
            prefix + "showRobot", this->showRobot,
            "Whether to visualize robot. Only active if visu level > 0.");

        props.defineOptionalPropertyVector<Eigen::Vector2f>(
            prefix + "planeSize", this->planeSize,
            "Size of visualized plane.");

        props.defineOptionalProperty<std::string>(
            prefix + "guardObjectFile", guardObjectFile,
            "Manipulation object file of the guard.");
    }


    void GuardPoseEstimatorVisu::readProperties(PropertyUser& props, std::string prefix)
    {
        level = props.getProperty<int>(prefix + "level");
        planeSize = props.getProperty<Eigen::Vector2f>(prefix + "planeSize");
        showRobot = props.getProperty<bool>(prefix + "showRobot");

        guardObjectFile = props.getProperty<std::string>(prefix + "guardObjectFile");
        guardObjectFile = ArmarXDataPath::resolvePath(guardObjectFile);
    }


    void GuardPoseEstimatorVisu::init(viz::Client arviz)
    {
        this->arviz = arviz;
    }

    viz::Layer& GuardPoseEstimatorVisu::addLayer(const std::string& name)
    {
        return layers.emplace_back(arviz.layer(name));
    }

    void GuardPoseEstimatorVisu::commit()
    {
        if (level > 0)
        {
            arviz.commit(layers);
            layers.clear();
        }
    }

    void GuardPoseEstimatorVisu::updateGuard(const GuardPose& guard, const pcl::PointCloud<PointT>& guardCloud)
    {
        if (level >= 1)
        {
            {
                viz::Layer& layerGuardPlane = addLayer("Guard Plane");

                layerGuardPlane.add(viz::Polygon("Plane")
                                    .plane(guard.position, guard.orientation, planeSize)
                                    .color(guard.valid ? viz::Color::gray(64) : viz::Color::red())
                                    .lineColor(viz::Color::gray(128)).lineWidth(3));
            }
            {
                viz::Layer& layerGuardObject = addLayer("Guard Object");
                layerGuardObject.add(viz::Object("guard")
                                     .pose(guard.position, guard.orientation)
                                     .file("", guardObjectFile));
            }
            {
                viz::Layer& layerGuardPose = addLayer("Guard Pose");

                layerGuardPose.add(viz::Pose("Guard").pose(guard.position, guard.orientation));
            }
        }
        if (level >= 2)
        {
            {
                viz::Layer& layerGuardCloud = addLayer("Point Cloud Guard");

                this->guardPointCloud.pointCloud(guardCloud);

                layerGuardCloud.add(this->guardPointCloud);
            }
        }
    }

    void GuardPoseEstimatorVisu::initRobot(const std::string& name, VirtualRobot::Robot& robot)
    {
        if (level > 0 && showRobot)
        {
            viz::Layer& layer = addLayer("Robot");

            this->robot = viz::Robot(name).file("", robot.getFilename());
            this->robot.joints(robot.getConfig()->getRobotNodeJointValueMap());
            layer.add(this->robot);
            layer.add(origin);
        }
    }

    void GuardPoseEstimatorVisu::updateRobot(VirtualRobot::Robot& robot)
    {
        if (level > 0 && showRobot)
        {
            viz::Layer& layer = addLayer("Robot");

            this->robot.joints(robot.getConfig()->getRobotNodeJointValueMap());
            layer.add(this->robot);
            layer.add(origin);
        }
    }

    void GuardPoseEstimatorVisu::updateInputPointCloud(
        const pcl::PointCloud<PointT>& cloud)
    {
        if (level >= 3)
        {
            viz::Layer& layer = addLayer("Input Point Cloud");

            this->inputPointCloud.pointCloud(cloud);
            this->inputPointCloud.transparency(0.25);

            layer.add(this->inputPointCloud);
        }
    }
}

