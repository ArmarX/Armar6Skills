/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::GuardPoseEstimation
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GuardPoseEstimation.h"

#include <stdexcept>

#include <pcl/io/pcd_io.h>

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/math/Helpers.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/time/TimeUtil.h>

#include <RobotAPI/libraries/core/Pose.h>

#include <VisionX/libraries/PointCloudTools/FramedPointCloud.h>


namespace armarx
{

    const simox::meta::EnumNames<GuardPoseEstimation::ExecutionMode>
    GuardPoseEstimation::executionModeNames =
    {
        { GuardPoseEstimation::ExecutionMode::ONLINE, "online" },
        { GuardPoseEstimation::ExecutionMode::RECORD, "record" },
        { GuardPoseEstimation::ExecutionMode::REPLAY, "replay" },
    };


    GuardPoseEstimationPropertyDefinitions::GuardPoseEstimationPropertyDefinitions(std::string prefix) :
        visionx::PointCloudProcessorPropertyDefinitions(prefix)
    {
        Recording recording;
        recording.defineProperties(*this, "record.");

        Replay replay;
        replay.defineProperties(*this, "replay.");
    }

    armarx::PropertyDefinitionsPtr GuardPoseEstimation::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs(new GuardPoseEstimationPropertyDefinitions(getConfigIdentifier()));

#if 0
        defs->topic(debugObserver);
        defs->topic(guardPoseTopic); // "GuardPoseTopic"
#else
        defs->defineOptionalProperty<std::string>("tpc.pub.DebugObserverTopicName", "DebugObserver", "DebugObserver topic.");
        defs->defineOptionalProperty<std::string>("tpc.pub.GuardPoseTopicName", "GuardPoseTopic", "GuardPose Topic.");
#endif

        defs->defineOptionalProperty<GuardPoseEstimation::ExecutionMode>("ExecutionMode", executionMode,
                "How to run the component:"
                "\n- online: Use live point cloud input and robot state."
                "\n- record: Just record point cloud input and robot state."
                "\n- replay: Use previously recorded point cloud input and robot state.")
        .map("online", GuardPoseEstimation::ExecutionMode::ONLINE)
        .map("record", GuardPoseEstimation::ExecutionMode::RECORD)
        .map("replay", GuardPoseEstimation::ExecutionMode::REPLAY);


        defs->defineOptionalProperty<bool>("ExecuteOnRequest", executeOnRequest,
                                           "If true, runs only when requested via the RequestableServiceListenerInterface.");


        estimator.defineProperties(*defs);

        return defs;
    }


    std::string GuardPoseEstimation::getDefaultName() const
    {
        return "GuardPoseEstimation";
    }


    void GuardPoseEstimation::onInitPointCloudProcessor()
    {
        offeringTopicFromProperty("tpc.pub.DebugObserverTopicName");
        offeringTopicFromProperty("tpc.pub.GuardPoseTopicName");

        usingTopic("ServiceRequests");

        executionMode = getProperty<ExecutionMode>("ExecutionMode");
        ARMARX_INFO << "Execution mode: " << executionModeNames.to_name(executionMode);
        executeOnRequest = getProperty<bool>("ExecuteOnRequest");
        if (executeOnRequest && executionMode == ExecutionMode::ONLINE)
        {
            ARMARX_INFO << "Running only when requested.";
        }

        estimator.readProperties(*this);


        if (executionMode == ExecutionMode::RECORD)
        {
            record.readProperties(*this, "record.");

            ARMARX_INFO << "Recodings directory: " << record.directory;
            if (record.dryRun)
            {
                ARMARX_IMPORTANT << "Dry run - not actually writing files!";
            }
        }
        else if (executionMode == ExecutionMode::REPLAY)
        {
            replay.readProperties(*this, "replay.");
        }
    }


    void GuardPoseEstimation::onConnectPointCloudProcessor()
    {
        getTopicFromProperty(debugObserver, "tpc.pub.DebugObserverTopicName");
        getTopicFromProperty(guardPoseTopic, "tpc.pub.GuardPoseTopicName");


        std::vector<std::string> providerNames = getPointCloudProviderNames();
        if (providerNames.size() == 1)
        {
            this->pointCloudProviderFrame = getPointCloudFrame(providerNames.front());
        }
        else
        {
            ARMARX_WARNING << "Point cloud transformation only supported for one point cloud provider.";
        }

        enableResultPointClouds<PointT>();

        ARMARX_INFO << "Load robot ...";
        this->robot = RobotState::addRobot(robotName, VirtualRobot::RobotIO::RobotDescription::eCollisionModel);
        ARMARX_VERBOSE << "Loaded robot.";

        estimator.setArviz(arviz);
        estimator.setRobot(robot);
        estimator.debugObserver = debugObserver;

        if (executionMode == ExecutionMode::RECORD)
        {
            record.robot = robot;
        }
        else if (executionMode == ExecutionMode::REPLAY)
        {
            replay.robot = robot;
            replay.runTask([this](pcl::PointCloud<PointT>::Ptr cloud)
            {
                std::scoped_lock lock(estimatorMutex);
                GuardPose guardPose = estimator.estimate(cloud);
                publishGuardPose(guardPose);
            });
        }
    }


    void GuardPoseEstimation::onDisconnectPointCloudProcessor()
    {
    }

    void GuardPoseEstimation::onExitPointCloudProcessor()
    {
    }


    void GuardPoseEstimation::process()
    {
        // Fetch input point cloud.
        pcl::PointCloud<PointT>::Ptr inputCloud(new pcl::PointCloud<PointT>());
        if (waitForPointClouds())
        {
            getPointClouds(inputCloud);
        }
        else
        {
            ARMARX_VERBOSE << "Timeout or error while waiting for point cloud data.";
            return;
        }

        if (executionMode == ExecutionMode::REPLAY)
        {
            return;
        }
        if (executionMode == ExecutionMode::ONLINE && executeOnRequest && TimeUtil::GetTime() > requestedUntil)
        {
            return;
        }

        StringVariantBaseMap debugValues;
        debugValues["Point Cloud Timestamp"] = new Variant(static_cast<int>(inputCloud->header.stamp));
        debugValues["Point Cloud Sequence"] = new Variant(static_cast<int>(inputCloud->header.seq));

        // Get robot state.
        RobotState::synchronizeLocalClone(robotName, Ice::Long(inputCloud->header.stamp));

        if (!pointCloudProviderFrame.empty())
        {
            const std::string rootName = robot->getRootNode()->getName();
            ARMARX_VERBOSE << "Transforming point cloud from '" << pointCloudProviderFrame
                           << "' to '" << rootName << "'";

            visionx::FramedPointCloud<PointT> framed(inputCloud, pointCloudProviderFrame);
            framed.changeFrame(rootName, robot);
        }

        float jointValue = robot->getRobotNode("Neck_1_Yaw")->getJointValue();

        Eigen::Vector3f axis = Eigen::Vector3f::UnitX();
        axis = Eigen::AngleAxisf(jointValue, Eigen::Vector3f::UnitZ()) * axis;
        float angle = math::Helpers::deg2rad(-3.8);

        // static Eigen::Matrix4f CreateTranslationRotationTranslationPose(const Eigen::Vector3f& translation1, const Eigen::Matrix3f& rotation, const Eigen::Vector3f& translation2);
        Eigen::Matrix4f tf = math::Helpers::CreateTranslationRotationTranslationPose(
                                 - robot->getRobotNode(pointCloudProviderFrame)->getPositionInRootFrame(),
                                 Eigen::AngleAxisf(angle, axis).toRotationMatrix(),
                                 robot->getRobotNode(pointCloudProviderFrame)->getPositionInRootFrame());
        {
            pcl::PointCloud<PointT>::Ptr buffer(new pcl::PointCloud<PointT>);
            pcl::transformPointCloud(*inputCloud, *buffer, tf);
            inputCloud = buffer;
        }


        pcl::PointCloud<PointT>::Ptr resultCloud;

        // Do processing.
        if (executionMode == ExecutionMode::ONLINE)
        {
            std::scoped_lock lock(estimatorMutex);
            GuardPose guardPose = estimator.estimate(inputCloud);
            publishGuardPose(guardPose);
            resultCloud = estimator.guardPointCloud;
        }
        else if (executionMode == ExecutionMode::RECORD)
        {
            record.process(*inputCloud);
            resultCloud.reset(new pcl::PointCloud<PointT>());
            estimator.visu.updateInputPointCloud(*inputCloud);
        }

        // Publish result point cloud.
        provideResultPointClouds(resultCloud);

        debugObserver->setDebugChannel(getName(), debugValues);
        estimator.visu.commit();
    }

    void GuardPoseEstimation::publishGuardPose(const GuardPose& guardPose)
    {
        ARMARX_VERBOSE << "Publishing guard pose.";
        if (guardPoseTopic)
        {
            guardPoseTopic->reportGuardPose(toIce(guardPose));
        }
    }


    data::GuardPose GuardPoseEstimation::getLatestGuardPose(const Ice::Current&)
    {
        std::scoped_lock lock(estimatorMutex);
        return toIce(estimator.getLatestGuardPose());
    }


    void GuardPoseEstimation::requestService(const std::string& providerName, int relativeTimeoutMS, const Ice::Current&)
    {
        if (providerName == getName())
        {
            requestedUntil = TimeUtil::GetTime() + IceUtil::Time::milliSeconds(relativeTimeoutMS);
        }
    }

    void GuardPoseEstimation::setServiceConfig(const std::string& providerName, const StringVariantBaseMap& config, const Ice::Current&)
    {
        if (providerName == getName())
        {
            // Not supported.
            (void) config;
        }
    }

}
