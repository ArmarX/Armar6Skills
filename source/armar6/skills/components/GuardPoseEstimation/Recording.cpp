#include "Recording.h"

#include <pcl/io/pcd_io.h>

#include <VirtualRobot/RobotConfig.h>
#include <VirtualRobot/EndEffector/EndEffector.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/time/TimeUtil.h>

#include <thread>

namespace armarx
{
    Recording::Recording()
    {
    }


    void Recording::defineProperties(PropertyDefinitionContainer& props, std::string prefix)
    {
        props.defineOptionalProperty<std::string>(
            prefix + "Directory", directory.string(),
            "Directory where recordings are stored.");

        props.defineOptionalProperty<bool>(
            prefix + "DateTimeDir", dateTimeDir,
            "Record into a new directory with current date time.");

        props.defineOptionalProperty<bool>(
            prefix + "DryRun", dryRun,
            "If true, do not actually store files when recording.");
    }


    void Recording::readProperties(PropertyUser& props, std::string prefix)
    {
        namespace fs = std::filesystem;

        this->dryRun = props.getProperty<bool>(prefix + "DryRun");
        this->directory = ArmarXDataPath::resolvePath(props.getProperty<std::string>(prefix + "Directory"));

        if (props.getProperty<bool>(prefix + "DateTimeDir"))
        {
            this->directory /= TimeUtil::toStringDateTime(TimeUtil::GetTime(TimeMode::SystemTime));
        }

        if (!this->dryRun)
        {
            if (!fs::exists(this->directory))
            {
                fs::create_directory(this->directory);
            }
        }
    }


    void Recording::process(const pcl::PointCloud<Recording::PointT>& cloud)
    {
        const std::filesystem::path pointCloudFilepath = storePointCloud(cloud);
        storeJson(cloud, pointCloudFilepath);
    }


    std::filesystem::path Recording::storePointCloud(const pcl::PointCloud<Recording::PointT>& cloud)
    {
        namespace fs = std::filesystem;

        std::stringstream filename;
        filename << "point-cloud-" << pointCloudSeq++ << ".pcd";

        const fs::path filepath = directory / filename.str();
        ARMARX_VERBOSE << "Storing point cloud as: '" << filepath.filename() << "'"
                       << "\n... in " << filepath.parent_path();

        if (!dryRun)
        {
            pcl::io::savePCDFileBinary(filepath.string(), cloud);
            ARMARX_VERBOSE << "Saved.";
        }

        return filepath;
    }


    void Recording::storeJson(const pcl::PointCloud<PointT>& cloud,
                              const std::filesystem::path pointCloudFilepath)
    {
        const nlohmann::json j = getJsonProcess(cloud, pointCloudFilepath.filename());

        const std::filesystem::path filepath = directory / recordingFileName;

        ARMARX_VERBOSE << "Appending to " << filepath.filename() << "'"
                       << "\n... in " << filepath.parent_path();
        if (!dryRun)
        {
            std::ofstream ofs(filepath, std::ios::app);
            ofs << j.dump() << std::endl;
        }
    }


    nlohmann::json Recording::getJsonProcess(
        const pcl::PointCloud<PointT>& inputCloud,
        const std::filesystem::path& pointCloudFilename) const
    {
        auto timestamp = inputCloud.header.stamp;
        Time time = Time::microSeconds(IceUtil::Int64(timestamp));

        nlohmann::json j;
        j["timestamp"] = timestamp;
        j["datetime"] = time.toDateTime();
        j["pointCloudFile"] = pointCloudFilename.string();
        j["robot"] = getJsonRobot();
        return j;
    }


    nlohmann::json Recording::getJsonRobot() const
    {
        nlohmann::json jrobot =
        {
            { "pose", robot->getGlobalPose() },
            { "config", robot->getConfig()->getRobotNodeJointValueMap() },
            { "eefs", getJsonRobotEEFs() },
        };
        return jrobot;
    }


    nlohmann::json Recording::getJsonRobotEEFs() const
    {
        nlohmann::json j;
        for (auto eef : robot->getEndEffectors())
        {
            j[eef->getTcpName()] = eef->getTcp()->getPoseInRootFrame();
        }
        return j;
    }


    void Replay::defineProperties(PropertyDefinitionContainer& props, std::string prefix)
    {
        props.defineOptionalProperty<std::string>(
            prefix + "Directory", directory,
            "Directory from which recordings are read.");
    }


    void Replay::readProperties(PropertyUser& props, std::string prefix)
    {
        this->directory = ArmarXDataPath::resolvePath(props.getProperty<std::string>(prefix + "Directory"));
    }


    void Replay::run(std::function<void(pcl::PointCloud<PointT>::Ptr cloud)> process)
    {
        ARMARX_INFO << "Running replay ...";
        ARMARX_VERBOSE << "Reading " << recordingFileName << "\n ... in " << directory;
        std::vector<nlohmann::json> jsonLines = readRecordingsJsonLines(directory / recordingFileName);
        ARMARX_VERBOSE << "Read " << jsonLines.size() << " lines.";

        if (jsonLines.empty())
        {
            ARMARX_WARNING << "No data.";
            return;
        }

        Time real_start = Time::now();
        Time rec_start = Time::microSeconds(jsonLines.front().at("timestamp").get<IceUtil::Int64>());

        //ARMARX_INFO << "Real start: " << real_start.toDateTime() << "\n"
        //            << "Rec. start: " << rec_start.toDateTime();

        for (size_t i = 0; i < jsonLines.size() && (!task || !task->isStopped()); ++i)
        {
            const nlohmann::json& j = jsonLines[i];

            Time rec_time = Time::microSeconds(j.at("timestamp").get<IceUtil::Int64>());
            Time rec_time_rel = (rec_time - rec_start);
            Time real_time_rel = Time::now() - real_start;
            // ARMARX_VERBOSE << "real_time_rel: " << real_time_rel.toMilliSecondsDouble() << " ms\n"
            //                << "rec_time_rel:  " << rec_time_rel.toMilliSecondsDouble() << " ms\n";
            if (rec_time_rel > real_time_rel)
            {
                Time to_wait = rec_time_rel - real_time_rel;
                // ARMARX_VERBOSE << "Sleeping for " << to_wait.toMilliSecondsDouble() << " ms";
                // TimeUtil::Sleep(to_wait);
                std::this_thread::sleep_for(std::chrono::milliseconds(to_wait.toMilliSeconds()));
            }

            // Set robot config.
            const std::map<std::string, float> config = j.at("robot").at("config").get<std::map<std::string, float>>();
            robot->setJointValues(config);
            robot->setGlobalPose(j.at("robot").at("pose").get<Eigen::Matrix4f>());

            // Load point cloud.
            const std::string pointCloudFile = j.at("pointCloudFile").get<std::string>();
            pcl::PointCloud<PointT>::Ptr pointCloud(new pcl::PointCloud<PointT>);
            pcl::io::loadPCDFile(directory / pointCloudFile, *pointCloud);
            ARMARX_VERBOSE << i << ": Loaded " << pointCloud->size() << " points.";

            process(pointCloud);
        }

        ARMARX_INFO << "Replay finished.";
    }

    void Replay::runTask(std::function<void(pcl::PointCloud<PointT>::Ptr cloud)> process)
    {
        task = new SimpleRunningTask<>([this, process]
        {
            this->run(process);
        });
        task->start();
    }


    std::vector<nlohmann::json> Replay::readRecordingsJsonLines(const std::filesystem::path& filepath) const
    {
        if (!std::filesystem::is_regular_file(filepath))
        {
            ARMARX_ERROR << "No file " << filepath.filename() << " in directory " << filepath.parent_path() << ".";
            return {};
        }

        std::ifstream ifs(filepath);

        std::vector<nlohmann::json> lines;
        std::string line;
        while (std::getline(ifs, line))
        {
            lines.push_back(nlohmann::json::parse(line));
        }

        return lines;
    }


}

