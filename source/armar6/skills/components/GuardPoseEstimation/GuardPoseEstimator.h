#pragma once

#include <mutex>

#include <VirtualRobot/VirtualRobot.h>

#include <Eigen/Core>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PointIndices.h>

#include <SimoxUtility/meta/EnumNames.hpp>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/application/properties/PropertyUser.h>

#include <RobotAPI/components/ArViz/Client/Client.h>

#include "GuardPose.h"
#include "GuardPoseEstimatorVisu.h"
#include "Preprocessing.h"


namespace armarx
{

    class GuardPoseEstimator
    {
    public:

        using PointT = pcl::PointXYZRGBA;

        enum class YawEstimationMode
        {
            NONE, OOBB, RECTANGLE,
            HORIZONTAL_TOP_EDGE  // Assume a horizontal top edge and estimate its height.
        };
        static const simox::meta::EnumNames<YawEstimationMode> YawEstimationModeNames;


    public:

        GuardPoseEstimator();

        void defineProperties(armarx::PropertyDefinitionContainer& properties, std::string prefix = "");
        void readProperties(armarx::PropertyUser& properties, std::string prefix = "");

        void setArviz(viz::Client arviz);
        void setRobot(VirtualRobot::RobotPtr robot);


        /**
         * @brief Estimate the guards plane, position and orientation.
         * @param inputCloud The input point cloud.
         */
        GuardPose estimate(pcl::PointCloud<PointT>::Ptr inputCloud);


        // Results
        GuardPose getLatestGuardPose() const;
        void setLatestGuardPose(GuardPose guardPose);

        pcl::PointCloud<PointT>::Ptr guardPointCloud { new pcl::PointCloud<PointT> };


    public:

        VirtualRobot::RobotPtr robot;

        struct Params
        {
            void defineProperties(armarx::PropertyDefinitionContainer& props, std::string prefix = "");
            void readProperties(armarx::PropertyUser& properties, std::string prefix = "");


            // Processing.

            // Plane fitting.
            float planeFittingDistanceThreshold = 1.0;
            bool refinePlaneSVD = true;

            // Color check.
            Eigen::Vector3f colorGuardHsv = { 0, 0.15f, 0.2f };
            Eigen::Vector3f colorHsvWeights = { 0, 0.5, 0.5 };
            float colorHsvMaxDistance = 1.0;

            // Clustering / segmentation.
            float clusterEuclideanTolerance = 2.0;
            Eigen::Vector3f clusterHsvWeights = { 0, 0.5, 0.5 };
            float clusterHsvTolerance = 1.0;

            // Yaw estimation.
            YawEstimationMode yawEstimationMode;

            Eigen::Vector2f yawRectRectSize = { 750, 1200 };
            size_t yawRectNumSteps = 180;

            // Validity checks.
            float validPlaneMaxAngle = 60;
            size_t validMinInliers = 2000;
        };
        Params params;

        guard_pose_estimator::Preprocessing preprocessing;

        GuardPoseEstimatorVisu visu;

        armarx::DebugObserverInterfacePrx debugObserver;


    private:


        pcl::PointCloud<PointT>::Ptr clusterGuardCloud(pcl::PointCloud<PointT>::Ptr guardCloud);
        std::vector<pcl::PointIndices> clusterGuardCloudColor(pcl::PointCloud<PointT>::Ptr guardCloud);


        bool checkNumInliers(size_t numInliers) const;
        bool checkColor(const pcl::PointCloud<PointT>& cloud, const std::vector<int>& indices);
        bool checkAngle(Eigen::Hyperplane3f plane) const;


        static pcl::PointCloud<PointT>::Ptr getLargestClusterPointCloud(
            pcl::PointCloud<PointT>::Ptr cloud,
            const std::vector<pcl::PointIndices>& clusters);


    private:

        mutable std::mutex latestGuardPoseMutex;
        GuardPose latestGuardPose;

    };
}

