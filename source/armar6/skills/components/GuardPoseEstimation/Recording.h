#pragma once

#include <filesystem>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <SimoxUtility/json.h>
#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/application/properties/PropertyUser.h>

#include <ArmarXCore/core/services/tasks/TaskUtil.h>


namespace armarx
{

    class Recording
    {
    public:
        using PointT = pcl::PointXYZRGBA;
        using Time = IceUtil::Time;


    public:

        Recording();

        void defineProperties(armarx::PropertyDefinitionContainer& properties, std::string prefix = "");
        void readProperties(armarx::PropertyUser& properties, std::string prefix = "");


    public:

        void process(const pcl::PointCloud<PointT>& cloud);

        std::filesystem::path storePointCloud(const pcl::PointCloud<PointT>& cloud);
        void storeJson(const pcl::PointCloud<PointT>& cloud,
                       std::filesystem::path pointCloudFilepath);


        nlohmann::json getJsonProcess(const pcl::PointCloud<PointT>& inputCloud,
                                      const std::filesystem::path& pointCloudFilename) const;
        nlohmann::json getJsonRobot() const;
        nlohmann::json getJsonRobotEEFs() const;


    public:
        // Settings

        std::string recordingFileName = "recording.json";

        std::filesystem::path directory = "armar6_skills/guard-pose-estimation-recordings";
        bool dateTimeDir = true;
        bool dryRun = false;


        VirtualRobot::RobotPtr robot;


    private:

        // Runtime

        long pointCloudSeq = 0;

    };


    class Replay
    {
    public:
        using PointT = pcl::PointXYZRGBA;
        using Time = IceUtil::Time;

    public:

        Replay() = default;

        void defineProperties(armarx::PropertyDefinitionContainer& properties, std::string prefix = "");
        void readProperties(armarx::PropertyUser& properties, std::string prefix = "");


        void run(std::function<void(pcl::PointCloud<PointT>::Ptr cloud)> process);
        void runTask(std::function<void(pcl::PointCloud<PointT>::Ptr cloud)> process);


        std::vector<nlohmann::json> readRecordingsJsonLines(const std::filesystem::path& filepath) const;


    public:

        std::string recordingFileName = "recording.json";

        std::filesystem::path directory = "armar6_skills/guard-pose-estimation-recordings";

        VirtualRobot::RobotPtr robot;

        SimpleRunningTask<>::pointer_type task;

    };

}
