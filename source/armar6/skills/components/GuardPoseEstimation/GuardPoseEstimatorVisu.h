#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include <Eigen/Core>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/application/properties/PropertyUser.h>

#include <RobotAPI/components/ArViz/Client/Client.h>

#include "GuardPose.h"


namespace armarx
{

    class GuardPoseEstimatorVisu
    {
    public:
        using PointT = pcl::PointXYZRGBA;

    public:

        GuardPoseEstimatorVisu() = default;

        void defineProperties(armarx::PropertyDefinitionContainer& props, std::string prefix = "visu.");
        void readProperties(armarx::PropertyUser& props, std::string prefix = "visu.");


        void init(viz::Client arviz);
        viz::Layer& addLayer(const std::string& name);
        void commit();


        void initRobot(const std::string& name, VirtualRobot::Robot& robot);
        void updateRobot(VirtualRobot::Robot& robot);

        void updateInputPointCloud(const pcl::PointCloud<PointT>& cloud);

        void updateGuard(const GuardPose& guard, const pcl::PointCloud<PointT>& guardCloud);


    public:

        // Params / settings
        int level = 0;

        Eigen::Vector2f planeSize { 750, 1200 };

        bool showRobot = true;

        std::vector<viz::Layer> layers;

        std::string guardObjectFile = "armar6_skills/objects/guard/guard.xml";


    private:

        viz::Client arviz;

        viz::Robot robot {""};

        viz::PointCloud inputPointCloud { "Input Point Cloud" };
        viz::PointCloud guardPointCloud { "Guard Point Cloud" };

        viz::Pose origin = viz::Pose("Origin").pose(Eigen::Matrix4f::Identity());

    };
}
