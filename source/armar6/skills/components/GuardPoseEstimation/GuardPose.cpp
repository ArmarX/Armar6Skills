#include "GuardPose.h"

#include <SimoxUtility/math/pose/transform.h>

#include <RobotAPI/libraries/core/Pose.h>


namespace
{
    float rotationAroundZ(const Eigen::Vector3f normal)
    {
        Eigen::Vector2f horizontalNormal = normal.head<2>().normalized();
        return std::atan2(horizontalNormal.y(), horizontalNormal.x());
    }
}

float armarx::GuardPose::rotationAroundZ() const
{
    return ::rotationAroundZ(plane.normal());
}


float armarx::GuardPose::rotationAroundZGlobal() const
{
    return ::rotationAroundZ(simox::math::transform_direction(robotPose, plane.normal()));
}



armarx::data::GuardPose armarx::toIce(const armarx::GuardPose& guardPose)
{
    data::GuardPose ice;
    ice.valid = guardPose.valid;
    ice.position = new armarx::Vector3(guardPose.position);
    ice.orientation = new armarx::Quaternion(guardPose.orientation);
    ice.rotationAroundZ = guardPose.rotationAroundZ();
    ice.rotationAroundZGlobal = guardPose.rotationAroundZGlobal();
    ice.planeNormal = new armarx::Vector3(guardPose.plane.normal().eval());
    ice.planeOffset = guardPose.plane.offset();

    ice.pTopRight = new armarx::Vector3(guardPose.pTopRight);
    ice.pTopLeft = new armarx::Vector3(guardPose.pTopLeft);
    ice.pBottomRight = new armarx::Vector3(guardPose.pBottomRight);
    ice.pBottomLeft = new armarx::Vector3(guardPose.pBottomLeft);

    ice.robotPose = new armarx::Pose(guardPose.robotPose);
    return ice;
}


armarx::GuardPose armarx::fromIce(const data::GuardPose& ice)
{
    armarx::GuardPose guardPose;
    guardPose.valid = ice.valid;
    guardPose.position = armarx::Vector3Ptr::dynamicCast(ice.position)->toEigen();
    guardPose.orientation = armarx::QuaternionPtr::dynamicCast(ice.orientation)->toEigen();
    guardPose.plane = Eigen::Hyperplane3f(armarx::Vector3Ptr::dynamicCast(ice.planeNormal)->toEigen(),
                                          ice.planeOffset);

    guardPose.pTopRight = armarx::Vector3Ptr::dynamicCast(ice.pTopRight)->toEigen();
    guardPose.pTopLeft = armarx::Vector3Ptr::dynamicCast(ice.pTopLeft)->toEigen();
    guardPose.pBottomRight = armarx::Vector3Ptr::dynamicCast(ice.pBottomRight)->toEigen();
    guardPose.pBottomLeft = armarx::Vector3Ptr::dynamicCast(ice.pBottomLeft)->toEigen();

    guardPose.robotPose = armarx::PosePtr::dynamicCast(ice.robotPose)->toEigen();
    return guardPose;
}


