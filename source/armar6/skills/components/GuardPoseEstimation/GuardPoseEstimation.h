/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::GuardPoseEstimation
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <pcl/point_types.h>

#include <SimoxUtility/meta/EnumNames.hpp>

#include <VirtualRobot/VirtualRobot.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/Component.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>

#include "GuardPose.h"
#include "GuardPoseEstimator.h"
#include "Recording.h"


namespace armarx
{


    /**
     * @class GuardPoseEstimationPropertyDefinitions
     * @brief Property definitions of `GuardPoseEstimation`.
     */
    class GuardPoseEstimationPropertyDefinitions :
        public visionx::PointCloudProcessorPropertyDefinitions
    {
    public:
        GuardPoseEstimationPropertyDefinitions(std::string prefix);
    };


    /**
     * @defgroup Component-GuardPoseEstimation GuardPoseEstimation
     * @ingroup armar6_skills-Components
     * A description of the component GuardPoseEstimation.
     *
     * @class GuardPoseEstimation
     * @ingroup Component-GuardPoseEstimation
     * @brief Brief description of class GuardPoseEstimation.
     *
     * Detailed description of class GuardPoseEstimation.
     */
    class GuardPoseEstimation :
        virtual public visionx::PointCloudProcessor,
        virtual public armarx::GuardPoseEstimationInterface,
        virtual public armarx::RobotStateComponentPluginUser,
        virtual public armarx::plugins::ArVizComponentPluginUser
    {
    private:

        using RobotState = RobotStateComponentPluginUser;
        using Time = IceUtil::Time;
        friend class GuardPoseEstimationPropertyDefinitions;


    public:

        /// The used point type.
        using PointT = pcl::PointXYZRGBA;

        /// The execution mode.
        enum class ExecutionMode
        {
            ONLINE,  ///< Use live point cloud input and robot state.
            RECORD,  ///< Just record point cloud input and robot state.
            REPLAY,  ///< Use previously recorded point cloud input and robot state.
        };
        static const simox::meta::EnumNames<ExecutionMode> executionModeNames;


    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        // GuardPoseInterface interface
        data::GuardPose getLatestGuardPose(const Ice::Current& = Ice::emptyCurrent) override;


        // RequestableServiceListenerInterface
        void requestService(const std::string& providerName, int relativeTimeoutMS,
                            const Ice::Current& = Ice::emptyCurrent) override;
        void setServiceConfig(const std::string& providerName, const StringVariantBaseMap& config,
                              const Ice::Current& = Ice::emptyCurrent) override;


    protected:

        /// @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
        void onInitPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
        void onConnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
        void onDisconnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
        void onExitPointCloudProcessor() override;


        /// @see visionx::PointCloudProcessor::process()
        void process() override;



        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        // Estimation

        void publishGuardPose(const GuardPose& guardPose);


    private:

        // Ice

        DebugObserverInterfacePrx debugObserver;
        GuardPoseTopicPrx guardPoseTopic;


        // Estimation

        ExecutionMode executionMode = ExecutionMode::ONLINE;
        bool executeOnRequest = true;

        IceUtil::Time requestedUntil = IceUtil::Time::now();


        const std::string robotName = "Armar6";
        VirtualRobot::RobotPtr robot;

        std::string pointCloudProviderFrame = "";


        std::mutex estimatorMutex;
        GuardPoseEstimator estimator;


        // Record / Replay

        Recording record;

        Replay replay;

    };


}
