#pragma once

#include <Eigen/Core>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PointIndices.h>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/application/properties/PropertyUser.h>

#include "GuardPoseEstimatorVisu.h"


namespace armarx::guard_pose_estimator
{

    class Preprocessing
    {
    public:

        using PointT = pcl::PointXYZRGBA;


    public:

        Preprocessing();

        void defineProperties(armarx::PropertyDefinitionContainer& properties, std::string prefix = "");
        void readProperties(armarx::PropertyUser& properties, std::string prefix = "");


        pcl::PointCloud<PointT>::Ptr preprocess(pcl::PointCloud<PointT>::ConstPtr input,
                                                VirtualRobot::RobotPtr robot);

        pcl::PointIndices::Ptr cropFloorIndices(const pcl::PointCloud<PointT>& input, float height);


        // Parameters.

        float cropFloorHeight = 10;
        bool globalCropBoxEnabled = false;
        Eigen::Vector3f globalCropBoxMin { 0, 0, 0 };
        Eigen::Vector3f globalCropBoxMax { 500, 2000, 2000 };

        float downsamplingLeafSize = 5.0;


        GuardPoseEstimatorVisu* visu = nullptr;

    };

}



