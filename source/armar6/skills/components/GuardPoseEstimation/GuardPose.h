#pragma once

#include <Eigen/Geometry>

#include <armar6/skills/interface/GuardPose.h>


namespace Eigen
{
    using Hyperplane3f = Hyperplane<float, 3>;
}

namespace armarx
{

    struct GuardPose
    {
        bool valid = false;

        Eigen::Vector3f position = Eigen::Vector3f::Zero();
        Eigen::Quaternionf orientation = Eigen::Quaternionf::Identity();

        Eigen::Hyperplane3f plane = Eigen::Hyperplane3f(Eigen::Vector3f::UnitZ(), 0);

        Eigen::Matrix4f robotPose = Eigen::Matrix4f::Identity();

        Eigen::Vector3f pTopRight;
        Eigen::Vector3f pTopLeft;
        Eigen::Vector3f pBottomRight;
        Eigen::Vector3f pBottomLeft;


        float rotationAroundZ() const;
        float rotationAroundZGlobal() const;


    };

    data::GuardPose toIce(const GuardPose& guardPose);
    GuardPose fromIce(const data::GuardPose& guardPose);

}
