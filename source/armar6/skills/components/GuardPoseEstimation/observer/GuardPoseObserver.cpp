/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::GuardPoseObserver
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GuardPoseObserver.h"

#include <SimoxUtility/math/pose/pose.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>


namespace armarx
{
    GuardPoseObserverPropertyDefinitions::GuardPoseObserverPropertyDefinitions(std::string prefix) :
        armarx::ObserverPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("GuardPoseTopicName", "GuardPoseTopic",
                                            "Name of the guard pose topic to subscribe.");

        defineOptionalProperty<bool>("IgnoreInvalid", true,
                                     "If true, invalid estimations are ignored.");

        defineOptionalPropertyVector<Eigen::Vector3f>("guard.InitialPosition", Eigen::Vector3f::Zero(),
                "Initial guard position.");
        Eigen::Quaternionf ori = Eigen::Quaternionf::Identity();
        defineOptionalPropertyVector<Eigen::Vector4f>("guard.InitialOrientation", {ori.w(), ori.x(), ori.y(), ori.z()},
                "Initial guard orientation as quaternion (w, x, y, z).");

        defineOptionalProperty<bool>(
            "visu.GuardObject", false, "Visualize the guard object.");
        defineOptionalProperty<bool>(
            "visu.GuardPose", false, "Visualize the guard object.");
        defineOptionalProperty<std::string>(
            "visu.Frame", armarx::GlobalFrame, "Visualize the guard object.")
        .map(armarx::GlobalFrame, armarx::GlobalFrame)
        .map("robot", "robot");
        defineOptionalProperty<std::string>(
            "visu.GuardObjectFile", "armar6_skills/objects/guard/guard.xml", "Manipulation object file of the guard.");
    }

    armarx::PropertyDefinitionsPtr GuardPoseObserver::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new GuardPoseObserverPropertyDefinitions(getConfigIdentifier()));
    }

    std::string GuardPoseObserver::getDefaultName() const
    {
        return "GuardPoseObserver";
    }


    void GuardPoseObserver::onInitObserver()
    {
        ignoreInvalid = getProperty<bool>("IgnoreInvalid");

        {
            latestGuardPose.position = getProperty<Eigen::Vector3f>("guard.InitialPosition");

            Eigen::Vector4f ori = getProperty<Eigen::Vector4f>("guard.InitialOrientation");
            latestGuardPose.orientation.w() = ori(0);
            latestGuardPose.orientation.x() = ori(1);
            latestGuardPose.orientation.y() = ori(2);
            latestGuardPose.orientation.z() = ori(3);
            latestGuardPose.orientation.normalize();

            latestGuardPose.plane = Eigen::Hyperplane3f(latestGuardPose.orientation * Eigen::Vector3f::UnitZ(),
                                    latestGuardPose.position);
        }
        latestGuardPoseIce = toIce(latestGuardPose);

        visuGuardObject = getProperty<bool>("visu.GuardObject");
        visuGuardPose = getProperty<bool>("visu.GuardPose");
        visuFrame = getProperty<std::string>("visu.Frame");
        visuGuardObjectFile = getProperty<std::string>("visu.GuardObjectFile");
        visuGuardObjectFile = ArmarXDataPath::resolvePath(visuGuardObjectFile);

        usingTopicFromProperty("GuardPoseTopicName");
    }


    void GuardPoseObserver::onConnectObserver()
    {
        offerChannel(channelName, "Guard Pose");

        onGuardPoseUpdate();
    }


    void GuardPoseObserver::onDisconnectComponent()
    {
    }


    void GuardPoseObserver::onExitComponent()
    {
    }


    void GuardPoseObserver::reportGuardPose(const data::GuardPose& guardPose, const Ice::Current&)
    {
        if (ignoreInvalid && !guardPose.valid)
        {
            return;
        }

        std::scoped_lock lock(latestGuardPoseMutex);
        this->latestGuardPoseIce = guardPose;
        this->latestGuardPose = fromIce(guardPose);

        onGuardPoseUpdate();
    }


    data::GuardPose GuardPoseObserver::getLatestGuardPose(const Ice::Current&)
    {
        std::scoped_lock lock(latestGuardPoseMutex);
        return latestGuardPoseIce;
    }


    void GuardPoseObserver::onGuardPoseUpdate()
    {
        offerOrUpdateDataField(channelName, "valid", Variant(latestGuardPose.valid), "Whether guard was detected in this frame.");
        offerOrUpdateDataField(channelName, "position", *latestGuardPoseIce.position, "Guard Position");
        offerOrUpdateDataField(channelName, "orientation", *latestGuardPoseIce.orientation, "Guard Orientation");
        offerOrUpdateDataField(channelName, "normal", *latestGuardPoseIce.planeNormal, "Guard Plane Normal");
        offerOrUpdateDataField(channelName, "rotationAroundZ", Variant(latestGuardPoseIce.rotationAroundZ), "Rotation around z axis.");
        offerOrUpdateDataField(channelName, "rotationAroundZGlobal", Variant(latestGuardPoseIce.rotationAroundZGlobal), "Rotation around global z axis.");
        offerOrUpdateDataField(channelName, "robotPose", *latestGuardPoseIce.robotPose, "Global Robot Pose.");

        if (visuGuardObject || visuGuardPose)
        {
            viz::Layer layer = arviz.layer("GuardPose");

            Eigen::Matrix4f pose = simox::math::pose(latestGuardPose.position, latestGuardPose.orientation);
            if (visuFrame == armarx::GlobalFrame)
            {
                pose = latestGuardPose.robotPose * pose;
            }

            if (visuGuardObject)
            {
                layer.add(viz::Object("Guard Object").pose(pose).file("", visuGuardObjectFile));
            }
            if (visuGuardPose)
            {
                layer.add(viz::Pose("Guard Pose").pose(pose));
            }

            arviz.commit(layer);
        }
    }
}
