/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::GuardPoseObserver
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/observers/Observer.h>

#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <armar6/skills/interface/GuardPose.h>
#include <armar6/skills/components/GuardPoseEstimation/GuardPose.h>


namespace armarx
{
    /**
     * @class GuardPoseObserverPropertyDefinitions
     * @brief Property definitions of `GuardPoseObserver`.
     */
    class GuardPoseObserverPropertyDefinitions :
        public ObserverPropertyDefinitions
    {
    public:
        GuardPoseObserverPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-GuardPoseObserver GuardPoseObserver
     * @ingroup armar6_skills-Components
     * A description of the component GuardPoseObserver.
     *
     * @class GuardPoseObserver
     * @ingroup Component-GuardPoseObserver
     * @brief Brief description of class GuardPoseObserver.
     *
     * Detailed description of class GuardPoseObserver.
     */
    class GuardPoseObserver :
        virtual public armarx::Observer,
        virtual public armarx::ArVizComponentPluginUser,
        virtual public armarx::GuardPoseObserverInterface
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // GuardPoseTopic interface
        void reportGuardPose(const data::GuardPose& guardPose, const Ice::Current&) override;

        // GuardPoseInterface interface
        data::GuardPose getLatestGuardPose(const Ice::Current&) override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitObserver() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectObserver() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        /// To be called with locked `latestGuardPoseMutex`.
        void onGuardPoseUpdate();


    private:

        bool ignoreInvalid = true;

        std::mutex latestGuardPoseMutex;
        data::GuardPose latestGuardPoseIce;
        armarx::GuardPose latestGuardPose;

        std::string channelName = "GuardPose";


        bool visuGuardObject = false;
        bool visuGuardPose = false;
        std::string visuFrame = armarx::GlobalFrame;
        std::string visuGuardObjectFile = "armar6_skills/objects/guard/guard.xml";

    };
}
