/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::Armar6DemoConnector
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6DemoConnector.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>


namespace armarx
{
    Armar6DemoConnectorPropertyDefinitions::Armar6DemoConnectorPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        //defineRequiredProperty<std::string>("PropertyName", "Description");
        //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");

        defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver",
                                            "Name of the topic the DebugObserver listens to.");

        defineOptionalProperty<std::string>("RequestTopic", "Armar6DemoConnectorRequestTopic", "Name of the topic to be used");
        defineOptionalProperty<std::string>("ResponseTopic", "Armar6DemoConnectorResponseTopic", "Name of the topic to be used");
    }


    std::string Armar6DemoConnector::getDefaultName() const
    {
        return "Armar6DemoConnector";
    }


    void Armar6DemoConnector::onInitComponent()
    {
        // Register offered topices and used proxies here.
        offeringTopicFromProperty("DebugObserverName");
        offeringTopicFromProperty("RequestTopic");
        usingTopicFromProperty("ResponseTopic");
        // debugDrawer.offeringTopic(*this);  // Calls this->offeringTopic().

        // Using a proxy will cause the component to wait until the proxy is available.
        // usingProxyFromProperty("MyProxyName");
        _lastKepalive = TimeUtil::GetTime();
    }


    void Armar6DemoConnector::onConnectComponent()
    {
        // Get topics and proxies here. Pass the *InterfacePrx type as template argument.
        getTopicFromProperty(_debugObserver, "DebugObserverName");
        getTopicFromProperty(_requestTopic, "DebugObserverName");
        // debugDrawer.getTopic(*this);  // Calls this->getTopic().

        // getProxyFromProperty<MyProxyInterfacePrx>("MyProxyName");


        Armar6DemoConnectorStatus s;
        s.skillName = "NONE";
        s.componentName = "NONE";
        s.status = "NONE";
        _status = s;
        _lastKepalive = TimeUtil::GetTime();

        _keepaliveTask = new RunningTask<Armar6DemoConnector>(this, &Armar6DemoConnector::keepaliveTaskRun);
    }


    void Armar6DemoConnector::onDisconnectComponent()
    {

    }


    void Armar6DemoConnector::onExitComponent()
    {

    }




    armarx::PropertyDefinitionsPtr Armar6DemoConnector::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new Armar6DemoConnectorPropertyDefinitions(
                getConfigIdentifier()));
    }

    void Armar6DemoConnector::notifySkillStarted(const std::string& skillName, const std::string& componentName, const Ice::Current&)
    {
        ScopedLock lock(_allMutex);
        Armar6DemoConnectorStatus s;
        s.skillName = skillName;
        s.componentName = componentName;
        s.status = "Started";
        s.timestamp = TimeUtil::GetTime().toMicroSeconds();
        _status = s;
        _skillShouldBeRunning = true;
    }

    void Armar6DemoConnector::notifySkillRunning(const std::string& skillName, const std::string& componentName, const std::string& phase, const armarx::aron::data::dto::DictPtr& args, const Ice::Current&)
    {
        ScopedLock lock(_allMutex);
        Armar6DemoConnectorStatus s;
        s.skillName = skillName;
        s.componentName = componentName;
        s.status = "Running";
        s.phase = phase;
        s.args = args;
        s.timestamp = TimeUtil::GetTime().toMicroSeconds();
        _status = s;

        if (!_skillShouldBeRunning)
        {
            ARMARX_WARNING << "skill should not be running, but is. skillName: " << skillName << " requestedSkillName: " << _requestedSkillName;
        }
    }

    void Armar6DemoConnector::notifySkillCompleted(const std::string& skillName, const std::string& componentName, const Ice::Current&)
    {
        ScopedLock lock(_allMutex);
        Armar6DemoConnectorStatus s;
        s.skillName = skillName;
        s.componentName = componentName;
        s.status = "Completed";
        s.timestamp = TimeUtil::GetTime().toMicroSeconds();
        _status = s;
        _skillShouldBeRunning = false;
    }

    Armar6DemoConnectorStatus Armar6DemoConnector::getStatus(const Ice::Current&)
    {
        ScopedLock lock(_allMutex);
        return _status;
    }

    void Armar6DemoConnector::startArmar6Skill(const std::string& skillName, const armarx::aron::data::dto::DictPtr& args, const Ice::Current&)
    {
        ScopedLock lock(_allMutex);
        _requestTopic->startArmar6Skill(skillName, args);
        _skillShouldBeRunning = true;
        _requestedSkillName = skillName;
    }

    void Armar6DemoConnector::stopArmar6Skill(const std::string& skillName, const Ice::Current&)
    {
        ScopedLock lock(_allMutex);
        _requestTopic->stopArmar6Skill(skillName);
        _skillShouldBeRunning = false;
    }

    void Armar6DemoConnector::notifyKeepalive(const std::string& skillName, const Ice::Current&)
    {
        ScopedLock lock(_allMutex);
        ARMARX_CHECK(_status.skillName == skillName);
        _lastKepalive = TimeUtil::GetTime();
    }

    void Armar6DemoConnector::keepaliveTaskRun()
    {
        while (!_keepaliveTask->isStopped())
        {
            TimeUtil::SleepMS(100);
            if (_skillShouldBeRunning)
            {
                ARMARX_ERROR << "stopping skill " << _requestedSkillName << " due to timeout";
                stopArmar6Skill(_requestedSkillName);
            }
        }
    }
}

ARMARX_DECOUPLED_REGISTER_COMPONENT(armarx::Armar6DemoConnector);
