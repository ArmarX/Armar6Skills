/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::Armar6DemoConnector
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/system/Synchronization.h>
//#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>
#include <armar6/skills/interface/Armar6DemoConnector.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/time/TimeUtil.h>


namespace armarx
{
    /**
     * @class Armar6DemoConnectorPropertyDefinitions
     * @brief Property definitions of `Armar6DemoConnector`.
     */
    class Armar6DemoConnectorPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        Armar6DemoConnectorPropertyDefinitions(std::string prefix);
    };

    /**
     * @defgroup Component-Armar6DemoConnector Armar6DemoConnector
     * @ingroup armar6_skills-Components
     * A description of the component Armar6DemoConnector.
     *
     * @class Armar6DemoConnector
     * @ingroup Component-Armar6DemoConnector
     * @brief Brief description of class Armar6DemoConnector.
     *
     * Detailed description of class Armar6DemoConnector.
     */
    class Armar6DemoConnector :
        virtual public Armar6DemoConnectorInterface,
        virtual public armarx::Component
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void notifySkillStarted(const std::string& skillName, const std::string& componentName, const Ice::Current&);
        void notifySkillRunning(const std::string& skillName, const std::string& componentName, const std::string& phase, const armarx::aron::data::dto::DictPtr& args, const Ice::Current&);
        void notifySkillCompleted(const std::string& skillName, const std::string& componentName, const Ice::Current&);
        Armar6DemoConnectorStatus getStatus(const Ice::Current&);
        void startArmar6Skill(const std::string& skillName, const armarx::aron::data::dto::DictPtr& args, const Ice::Current&);
        void stopArmar6Skill(const std::string& skillName, const Ice::Current& c = Ice::emptyCurrent);
        void notifyKeepalive(const std::string& skillName, const Ice::Current&);

    private:
        void keepaliveTaskRun();

        // Private methods and member variables go here.

        /// Debug observer. Used to visualize e.g. time series.
        armarx::DebugObserverInterfacePrx _debugObserver;
        /// Debug drawer. Used for 3D visualization.
        //armarx::DebugDrawerTopic debugDrawer;
        Armar6DemoConnectorRequestPrx _requestTopic;
        Armar6DemoConnectorStatus _status;
        Mutex _allMutex;
        IceUtil::Time _lastKepalive;
        RunningTask<Armar6DemoConnector>::pointer_type _keepaliveTask;
        float timeout = 1;
        bool _skillShouldBeRunning = false;
        std::string _requestedSkillName;

    };
}
