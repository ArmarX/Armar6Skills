/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::FitFixedSizeRectangleTest
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>


namespace armarx
{
    /**
     * @class FitFixedSizeRectangleTestPropertyDefinitions
     * @brief Property definitions of `FitFixedSizeRectangleTest`.
     */
    class FitKnownRectangleRotationMaxPointsTestPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        FitKnownRectangleRotationMaxPointsTestPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-FitFixedSizeRectangleTest FitFixedSizeRectangleTest
     * @ingroup armar6_skills-Components
     * A description of the component FitFixedSizeRectangleTest.
     *
     * @class FitFixedSizeRectangleTest
     * @ingroup Component-FitFixedSizeRectangleTest
     * @brief Brief description of class FitFixedSizeRectangleTest.
     *
     * Detailed description of class FitFixedSizeRectangleTest.
     */
    class FitKnownRectangleRotationMaxPointsTest :
        virtual public armarx::Component,
        virtual public armarx::ArVizComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


        void run();


    private:

        // Private methods and member variables go here.

        /// Debug observer. Used to visualize e.g. time series.
        armarx::DebugObserverInterfacePrx debugObserver;

        armarx::SimpleRunningTask<>::pointer_type task;


        size_t numPoints = 1e4;
        Eigen::Vector3f centerOffset = Eigen::Vector3f::Zero();

        size_t numSteps = 180;


    };
}
