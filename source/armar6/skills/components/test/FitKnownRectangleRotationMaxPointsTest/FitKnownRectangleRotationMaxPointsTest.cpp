/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::FitKnownRectangleRotationMaxPointsTest
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "FitKnownRectangleRotationMaxPointsTest.h"

#include <random>

#include <SimoxUtility/color/cmaps.h>


#include <armar6/skills/components/GuardPoseEstimation/FitKnownRectangleRotationMaxPoints.h>


namespace armarx
{
    FitKnownRectangleRotationMaxPointsTestPropertyDefinitions::FitKnownRectangleRotationMaxPointsTestPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
    }

    armarx::PropertyDefinitionsPtr FitKnownRectangleRotationMaxPointsTest::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs(new FitKnownRectangleRotationMaxPointsTestPropertyDefinitions(getConfigIdentifier()));

        defs->topic(debugObserver);

        defs->optional(numPoints, "numPoints", "numPoints");

        defs->optional(centerOffset, "centerOffset", "The center offset.");

        defs->optional(numSteps, "numSteps", "numSteps");

        return defs;
    }


    std::string FitKnownRectangleRotationMaxPointsTest::getDefaultName() const
    {
        return "FitKnownRectangleRotationMaxPointsTest";
    }


    void FitKnownRectangleRotationMaxPointsTest::onInitComponent()
    {
    }


    void FitKnownRectangleRotationMaxPointsTest::onConnectComponent()
    {
        task = new armarx::SimpleRunningTask<>([this]()
        {
            run();
        });
        task->start();
    }


    void FitKnownRectangleRotationMaxPointsTest::onDisconnectComponent()
    {
    }


    void FitKnownRectangleRotationMaxPointsTest::onExitComponent()
    {
    }


    using PointT = pcl::PointXYZRGBA;

    pcl::PointCloud<PointT>::Ptr makePointCloud(
        Eigen::Vector3f pos, Eigen::Quaternionf ori, Eigen::Vector2f rectSize,
        size_t numPoints = 1e4)
    {
        pcl::PointCloud<PointT>::Ptr cloud(new pcl::PointCloud<PointT>);

        std::default_random_engine gen;
        std::uniform_real_distribution<float> dx(- rectSize.x() / 2, rectSize.x() / 2);
        std::uniform_real_distribution<float> dy(- rectSize.y() / 2, rectSize.y() / 2);
        std::normal_distribution<float> dz(0, 10);

        Eigen::Vector3f vx = ori * Eigen::Vector3f::UnitX();
        Eigen::Vector3f vy = ori * Eigen::Vector3f::UnitY();
        Eigen::Vector3f vz = ori * Eigen::Vector3f::UnitZ();

        cloud->points.reserve(numPoints);
        for (size_t i = 0; i < numPoints; ++i)
        {
            Eigen::Vector3f v = pos + dx(gen) * vx + dy(gen) * vy + dz(gen) * vz;
            PointT& p = cloud->points.emplace_back();
            p.x = v.x();
            p.y = v.y();
            p.z = v.z();
            p.r = 64;
            p.g = 64;
            p.b = 64;
            p.a = 255;
        }

        return cloud;
    }



    void FitKnownRectangleRotationMaxPointsTest::run()
    {
        static const auto zero = Eigen::Vector3f::Zero();

        const Eigen::Vector3f truePos = { 100, 200, 300 };
        const Eigen::Quaternionf trueOri(Eigen::AngleAxisf(1.f, Eigen::Vector3f(1, 2, 1).normalized()));
        const Eigen::Vector2f rectSize = { 750, 1200 };

        viz::Layer layerTruth = arviz.layer("Truth");
        {
            layerTruth.add(viz::Pose("Truth").pose(truePos, trueOri));
            layerTruth.add(viz::Polygon("Rectangle").plane(truePos, trueOri, rectSize)
                           .lineColor(simox::Color::green()).lineWidth(1).color(simox::Color::black(0)));
        }

        // Make point cloud.
        pcl::PointCloud<PointT>::Ptr cloud = makePointCloud(truePos, trueOri, rectSize, numPoints);
        viz::Layer layerCloud = arviz.layer("Cloud");
        {
            layerCloud.add(viz::PointCloud("cloud").pointCloud(*cloud));
        }

        // Input

        const Eigen::Vector3f center = truePos + (trueOri * centerOffset);
        const Eigen::Vector3f normal = trueOri * Eigen::Vector3f::UnitZ();


        // Compute

        visionx::FitKnownRectangleRotationMaxPoints fitter;
        fitter.setNumSteps(numSteps);
        fitter.setRectangleSize(rectSize);

        ARMARX_INFO << "Fitting ...";

        IceUtil::Time start = IceUtil::Time::now();
        Eigen::Matrix4f pose = fitter.fit(*cloud, normal, center);
        IceUtil::Time time = IceUtil::Time::now() - start;

        ARMARX_INFO << "Done. "
                    << "\n- time:     " << time.toMilliSecondsDouble() << " ms"
                    << "\n- per step: " << time.toMilliSecondsDouble() / numSteps << " ms";

        std::vector<size_t> inlierCounts = fitter.getInlierCounts();

        Eigen::Vector3f vx = simox::math::transform_direction(pose, Eigen::Vector3f::UnitX());
        Eigen::Vector3f vy = simox::math::transform_direction(pose, Eigen::Vector3f::UnitY());


        viz::Layer layerRectLoc = arviz.layer("RectangleLocal");
        {
            Eigen::AngleAxisf rot(fitter.getBestAngle(), Eigen::Vector3f::UnitZ());

            layerRectLoc.add(viz::Arrow("x").fromTo(zero, rectSize.x() / 2 * (rot * Eigen::Vector3f::UnitX()))
                             .color(simox::Color::red()).width(5));
            layerRectLoc.add(viz::Arrow("y").fromTo(zero, rectSize.y() / 2 * (rot * Eigen::Vector3f::UnitY()))
                             .color(simox::Color::green()).width(5));
        }
        viz::Layer layerRect = arviz.layer("Rectangle");
        {
            layerRect.add(viz::Arrow("x").fromTo(center, center + rectSize.x() / 2 * vx)
                          .color(simox::Color::red()));
            layerRect.add(viz::Arrow("y").fromTo(center, center + rectSize.y() / 2 * vy)
                          .color(simox::Color::green()));
            layerRect.add(viz::Pose("pose").pose(pose).scale(1.1f));
            layerRect.add(viz::Polygon("Rectangle")
                          .plane(center, Eigen::Quaternionf(simox::math::orientation(pose)), rectSize)
                          .lineColor(simox::Color::orange()).lineWidth(1).color(simox::Color::black(0)));
        }

        viz::Layer layerCountsLoc = arviz.layer("CountsLocal");
        {
            simox::ColorMap cmap = simox::color::cmaps::inferno();
            cmap.set_vlimits(*std::min_element(inlierCounts.begin(), inlierCounts.end()),
                             *std::max_element(inlierCounts.begin(), inlierCounts.end()));

            for (size_t i = 0; i < inlierCounts.size(); ++i)
            {
                float angle = fitter.stepToAngle(i);
                Eigen::AngleAxisf rot(angle, Eigen::Vector3f::UnitZ());
                Eigen::Vector3f vx = rot * Eigen::Vector3f::UnitX();

                layerCountsLoc.add(viz::Arrow(std::to_string(angle))
                                   .fromTo(zero, rectSize.x() / 2 * vx)
                                   .color(cmap(inlierCounts[i])).width(2));
            }
        }
        viz::Layer layerCounts = arviz.layer("Counts");
        {
            simox::ColorMap cmap = simox::color::cmaps::inferno();
            cmap.set_vlimits(*std::min_element(inlierCounts.begin(), inlierCounts.end()),
                             *std::max_element(inlierCounts.begin(), inlierCounts.end()));

            for (size_t i = 0; i < inlierCounts.size(); ++i)
            {
                float angle = fitter.stepToAngle(i);
                Eigen::Vector3f vx = simox::math::transform_direction(fitter.angleToPose(angle),
                                     Eigen::Vector3f::UnitX());

                layerCounts.add(viz::Arrow(std::to_string(angle))
                                .fromTo(center, center + rectSize.x() / 2 * vx)
                                .color(cmap(inlierCounts[i])).width(2));
            }
        }

        viz::Layer layerAligned = arviz.layer("Aligned");
        {
            layerAligned.add(viz::PointCloud("aligned").pointCloud(*fitter.aligned));
            layerAligned.add(viz::Pose("origin").pose(Eigen::Matrix4f::Identity()));
        }


        ARMARX_INFO << "Inlier counts: " << inlierCounts;


        arviz.commit({layerTruth, layerCloud, layerRect, layerRectLoc, layerCounts, layerCountsLoc,
                      layerAligned});
    }

}
