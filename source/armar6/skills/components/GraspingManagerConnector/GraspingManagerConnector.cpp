/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::GraspingManagerConnector
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspingManagerConnector.h"

#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <armar6/skills/libraries/Armar6Tools/Armar6HandV2Helper.h>
#include <RobotAPI/libraries/GraspingUtility/GraspCandidateHelper.h>

#include <VirtualRobot/math/Helpers.h>

#include <ArmarXCore/core/util/ObjectToIceBlob.h>
#include <ArmarXCore/core/util/IceBlobToObject.h>

#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>

#include <RobotAPI/libraries/core/CartesianPositionController.h>
#include <VirtualRobot/MathTools.h>
#include <RobotAPI/libraries/core/FramedPose.h>

namespace armarx
{
    GraspingManagerConnectorPropertyDefinitions::GraspingManagerConnectorPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        //defineRequiredProperty<std::string>("PropertyName", "Description");
        //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");

        defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens to.");
        defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
        defineOptionalProperty<std::string>("GraspingManagerName", "GraspingManager", "Name of GraspingManager");
        //defineOptionalProperty<std::string>("TrajectoryPlayerName", "TrajectoryPlayer", "Name of TrajectoryPlayer");
        defineOptionalProperty<std::string>("PlatformUnitName", "Armar6PlatformUnit", "Name of PlatformUnit");
        defineOptionalProperty<std::string>("KinematicUnitName", "Armar6KinematicUnit", "Name of KinematicUnit");
        defineOptionalProperty<std::string>("CartesianPositionControlName", "Armar6CartesianPositionControlComponent", "Name of CartesianPositionControl");
        defineOptionalProperty<std::string>("RobotUnitName", "Armar6Unit", "Name of RobotUnit");

        defineOptionalProperty<float>("cfg.SideGraspMinHeight", 1160, "Minimum height of side grasps");
        defineOptionalProperty<float>("cfg.PreposeOffset", 150.0f, "Offset of proposed grasp along negative z axis", PropertyDefinitionBase::eModifiable);
        defineOptionalProperty<float>("cfg.UpwardsOffset", 100.0f, "Offset of proposed grasp in upwards direction", PropertyDefinitionBase::eModifiable);

        //defineOptionalProperty<std::string>("MyProxyName", "MyProxy", "Name of the proxy to be used");
    }


    std::string GraspingManagerConnector::getDefaultName() const
    {
        return "GraspingManagerConnector";
    }

    Eigen::Vector3f GraspingManagerConnector::PlatformPose2Vec(const Eigen::Matrix4f& pose)
    {
        Eigen::Vector3f pos = math::Helpers::GetPosition(pose);
        Eigen::Vector3f rpy = VirtualRobot::MathTools::eigen4f2rpy(pose);
        return Eigen::Vector3f(pos(0), pos(1), rpy(2));
    }

    Eigen::Matrix4f GraspingManagerConnector::PlatformVec2Pose(const Eigen::Vector3f& vec)
    {
        return math::Helpers::CreatePose(Eigen::Vector3f(vec(0), vec(1), 0), VirtualRobot::MathTools::rpy2eigen3f(0, 0, vec(2)));
    }


    void GraspingManagerConnector::onInitComponent()
    {
        // Register offered topices and used proxies here.
        offeringTopicFromProperty("DebugObserverName");
        usingProxyFromProperty("RobotStateComponentName");
        usingProxyFromProperty("GraspingManagerName");
        //usingProxyFromProperty("TrajectoryPlayerName");

        debugDrawer.offeringTopic(*this);  // Calls this->offeringTopic().

        // Using a proxy will cause the component to wait until the proxy is available.
        // usingProxyFromProperty("MyProxyName");

    }


    void GraspingManagerConnector::onConnectComponent()
    {
        // Get topics and proxies here. Pass the *InterfacePrx type as template argument.
        getTopicFromProperty(_debugObserver, "DebugObserverName");
        getProxyFromProperty(_gm, "GraspingManagerName");
        getProxyFromProperty(_robotStateComponent,  "RobotStateComponentName");
        //getProxyFromProperty(_trajectoryPlayer,  "TrajectoryPlayerName");
        _rnh = RobotNameHelper::Create(_robotStateComponent->getRobotInfo(), nullptr);

        _robot = RemoteRobot::createLocalCloneFromFile(_robotStateComponent, VirtualRobot::RobotIO::eStructure);


        debugDrawer.getTopic(*this);  // Calls this->getTopic().
        debugDrawer.setLayer("GraspingManagerConnector");

        // getProxyFromProperty<MyProxyInterfacePrx>("MyProxyName");

        try
        {
            _isSimulation = getRobotUnit()->isSimulation();
        }
        catch (...)
        {
            ARMARX_WARNING << "getRobotUnit()->isSimulation() failed. assuming _isSimulation=false";
            _isSimulation = false;
        }

        guiCreate();
    }


    void GraspingManagerConnector::onDisconnectComponent()
    {

    }


    void GraspingManagerConnector::onExitComponent()
    {

    }




    armarx::PropertyDefinitionsPtr GraspingManagerConnector::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new GraspingManagerConnectorPropertyDefinitions(getConfigIdentifier()));
    }

    void GraspingManagerConnector::guiCreate()
    {
        ARMARX_IMPORTANT << "create gui";
        auto labelCandidates = RemoteGui::makeLabel("candidates");
        auto labelPlanningStatus  = RemoteGui::makeLabel("planningStatus");
        auto gcoLayout = RemoteGui::makeHBoxLayout()
                         .addChild(RemoteGui::makeButton("selectGCs").label("capture GCs"))
                         .addChild(RemoteGui::makeButton("planGCs").label("plan - GCs"));
        auto buttonPlanSpray = RemoteGui::makeButton("planSpray").label("plan - WM:Spraybottle");
        auto ioLayout = RemoteGui::makeHBoxLayout()
                        .addChild(RemoteGui::makeButton("visualize"))
                        .addChild(RemoteGui::makeButton("store"))
                        .addChild(RemoteGui::makeButton("load"));
        auto labelIOStatus  = RemoteGui::makeLabel("ioStatus");
        auto buttonExecute = RemoteGui::makeButton("execute");
        auto labelExecutionStatus  = RemoteGui::makeLabel("executionStatus");
        auto buttonReset = RemoteGui::makeButton("reset");
        auto layout = RemoteGui::makeVBoxLayout()
                      .addChildren({labelCandidates, gcoLayout, buttonPlanSpray, labelPlanningStatus, ioLayout, labelIOStatus, buttonExecute, labelExecutionStatus, buttonReset});
        createOrUpdateRemoteGuiTab(getName(), layout, [this](RemoteGui::TabProxy & tab)
        {
            tab.receiveUpdates();
            guiUpdate(tab);
            tab.sendUpdates();
        });
    }

    void GraspingManagerConnector::gm_plan_gc(RemoteGui::TabProxy& tab, grasping::GraspCandidateSeq& gcoGrasps)
    {
        RemoteRobot::synchronizeLocalClone(_robot, _robotStateComponent);
        _stateAtPlanningTime.platformPose = _robot->getGlobalPose();
        _stateAtPlanningTime.rightArmConfig = _rnh->getRobotArm("Right", _robot).getKinematicChain()->getJointValueMap();
        _stateAtPlanningTime.leftArmConfig = _rnh->getRobotArm("Left", _robot).getKinematicChain()->getJointValueMap();

        ARMARX_IMPORTANT << "got " << gcoGrasps.size() << "grasp candidates.";
        GeneratedGraspList gmGrasps;

        float SideGraspMinHeight = getProperty<float>("cfg.SideGraspMinHeight");
        //std::vector<Eigen::Matrix4f>

        float PreposeOffset = getProperty<float>("cfg.PreposeOffset");
        float UpwardsOffset = getProperty<float>("cfg.UpwardsOffset");

        for (const grasping::GraspCandidatePtr& gc : gcoGrasps)
        {
            GeneratedGrasp gg;
            gg.graspName = gc->sourceInfo->referenceObjectName;
            gg.score = 1;
            gg.eefName = _rnh->getArm(gc->side).getEndEffector();
            armarx::GraspCandidateHelper gcHelper(gc, _robot);
            Eigen::Matrix4f graspPose = _robot->getGlobalPose() * gcHelper.getGraspPoseInRobotRoot();
            Eigen::Matrix4f prePose = _robot->getGlobalPose() * gcHelper.getPrePoseInRobotRoot(PreposeOffset);
            Eigen::Vector3f graspPos = math::Helpers::GetPosition(graspPose);
            float zOffset = 0;
            if (gcHelper.isSideGrasp() && graspPos(2) < SideGraspMinHeight)
            {
                zOffset = SideGraspMinHeight - graspPos(2);
                ARMARX_WARNING << "grasp z of " << gc->side << "_" << gc->groupNr << " is too low " << graspPos(2) << "; adding zOffset: " << zOffset;
            }

            //Eigen::Vector3f offset(0,0,0);
            if (gcHelper.isSideGrasp())
            {
                prePose = _robot->getGlobalPose() * gcHelper.getPrePoseInRobotRoot(PreposeOffset);
                prePose = math::Helpers::TranslatePose(prePose, 0, 0, UpwardsOffset);
            }
            else if (gcHelper.isTopGrasp())
            {
                //offset = Eigen::Vector3f(0, 0, 100);
                //graspPose = math::Helpers::TranslatePose(graspPose, offset);
                //prePose = math::Helpers::TranslatePose(prePose, offset);
                prePose = _robot->getGlobalPose() * gcHelper.getPrePoseInRobotRoot(PreposeOffset);
            }
            graspPose = math::Helpers::TranslatePose(graspPose, 0, 0, zOffset);
            prePose = math::Helpers::TranslatePose(prePose, 0, 0, zOffset);
            ARMARX_IMPORTANT << VAROUT(graspPose);

            gg.framedPose = new FramedPose(graspPose, GlobalFrame, _robot->getName());
            gg.framedPrePose = new FramedPose(prePose, GlobalFrame, _robot->getName());
            gmGrasps.emplace_back(gg);

        }
        gm_plan_int(tab, gmGrasps);
    }

    void GraspingManagerConnector::gm_plan_wm(RemoteGui::TabProxy& tab, const std::string& objectName)
    {
        RemoteRobot::synchronizeLocalClone(_robot, _robotStateComponent);
        GeneratedGraspList generatedGrasps = _gm->generateGraspsByObjectName(objectName);
        ARMARX_IMPORTANT << "Got " << generatedGrasps.size() << " grasps for object " << objectName;
        float SideGraspMinHeight = getProperty<float>("cfg.SideGraspMinHeight");
        GeneratedGraspList gmGrasps;
        for (GeneratedGrasp gg : generatedGrasps)
        {
            Eigen::Matrix4f graspPose = _robot->getGlobalPose() * defrost(gg.framedPose);
            Eigen::Vector3f graspPos = math::Helpers::GetPosition(graspPose);
            Eigen::Matrix4f prePose = _robot->getGlobalPose() * defrost(gg.framedPrePose);
            float zOffset = 0;
            if (graspPos(2) < SideGraspMinHeight)
            {
                zOffset = SideGraspMinHeight - graspPos(2);
                ARMARX_WARNING << "grasp z of " << gg.graspName << " is too low " << graspPos(2) << "; adding zOffset: " << zOffset;
            }
            graspPose = math::Helpers::TranslatePose(graspPose, 0, 0, zOffset);
            prePose = math::Helpers::TranslatePose(prePose, 0, 0, zOffset);
            GeneratedGrasp newGG;
            newGG.score = gg.score;
            newGG.eefName = gg.eefName;
            newGG.graspName = gg.graspName;
            newGG.framedPose = new FramedPose(graspPose, GlobalFrame, _robot->getName());
            newGG.framedPrePose = new FramedPose(prePose, GlobalFrame, _robot->getName());
            gmGrasps.emplace_back(newGG);
        }

        gm_plan_int(tab, gmGrasps);
    }

    void GraspingManagerConnector::gm_plan_int(RemoteGui::TabProxy& tab, GeneratedGraspList gmGrasps)
    {
        ARMARX_IMPORTANT << "calling _gm->generateGraspingTrajectoryListForGraspList(gmGrasps)";
        tab.getValue<std::string>("planningStatus").set("planning ...");
        tab.sendUpdates();
        GraspingTrajectoryList trajectories = _gm->generateGraspingTrajectoryListForGraspList(gmGrasps);
        ARMARX_IMPORTANT << "got " << trajectories.size() << " Grasping Trajectory";
        tab.getValue<std::string>("planningStatus").set("done.");
        tab.sendUpdates();
        for (const GraspingTrajectory& gt : trajectories)
        {
            ARMARX_IMPORTANT << VAROUT(gt.grasp.graspName);
        }
        if (trajectories.size() == 0)
        {
            ARMARX_WARNING << "got no GraspingTrajectory";
            return;
        }
        _plannedTrajectory = trajectories.at(0);
        TrajectoryPtr configTrajectory = TrajectoryPtr::dynamicCast(_plannedTrajectory.configTrajectory);
        ARMARX_IMPORTANT << VAROUT(configTrajectory->dim());
        double startT = configTrajectory->begin()->getTimestamp();
        double endT = configTrajectory->rbegin()->getTimestamp();

        ARMARX_IMPORTANT << VAROUT(startT) << VAROUT(endT);

        std::vector<Eigen::Vector3f> positions;
        for (double t = startT; t < endT; t += 0.1)
        {
            Eigen::Vector3f pos(
                configTrajectory->getState(t, 0, 0),
                configTrajectory->getState(t, 1, 0),
                configTrajectory->getState(t, 2, 0));
            positions.emplace_back(pos);
            ARMARX_IMPORTANT << pos.transpose();
        }

        ARMARX_IMPORTANT << VAROUT(positions.size());

        debugDrawer.drawLineSet("GraspingTrajectory", positions, 5, DrawColor{1, 0, 0, 1});
    }

    void GraspingManagerConnector::gm_visu(RemoteGui::TabProxy& tab)
    {
        _gm->visualizeGraspingTrajectory(_plannedTrajectory, 2);
    }

    static bool writeCompleteFile(std::string const& filename,
                                  const void* data, std::size_t size)
    {
        FILE* file = fopen(filename.c_str(), "wb");
        if (!file)
        {
            return false;
        }
        std::size_t written = std::fwrite(data, 1, size, file);
        if (written != size)
        {
            return false;
        }
        std::fclose(file);
        return true;
    }

    static std::string readCompleteFile(std::filesystem::path const& path)
    {
        FILE* f = fopen(path.string().c_str(), "rb");
        fseek(f, 0, SEEK_END);
        long fsize = ftell(f);
        fseek(f, 0, SEEK_SET);  /* same as rewind(f); */

        std::string result(fsize, '\0');
        std::size_t read = fread(result.data(), 1, fsize, f);
        result.resize(read);
        fclose(f);

        return result;
    }

    void GraspingManagerConnector::gm_store(RemoteGui::TabProxy& tab)
    {
        TrajectoryPtr platformTraj = TrajectoryPtr::dynamicCast(_plannedTrajectory.poseTrajectory);
        TrajectoryPtr armTraj = TrajectoryPtr::dynamicCast(_plannedTrajectory.configTrajectory);
        ObjectToIceBlobSerializer ser{_plannedTrajectory};
        std::string packageName = "armar6_skills";
        armarx::CMakePackageFinder finder(packageName);
        std::filesystem::path path(finder.getDataDir() + "/" + packageName + "/PlannedGrasps");

        if (!std::filesystem::exists(path))
        {
            ARMARX_INFO << "Creating path: " << path;
            std::filesystem::create_directory(path);
        }

        std::filesystem::path filePath = path / "tmp.bin";

        if (writeCompleteFile(filePath.string(), ser.begin(), ser.size()))
        {
            tab.getValue<std::string>("ioStatus").set("stored.");
        }
        else
        {
            ARMARX_WARNING << "Could not write history file: " << filePath;
        }
    }

    void GraspingManagerConnector::read_GraspingTrajectory_from_blob(GraspingTrajectory& result, const std::string_view& sv)
    {
        const auto first = reinterpret_cast<const Ice::Byte*>(sv.data());
        const auto last = first + sv.size();
        Ice::InputStream is{std::make_pair(first, last)};
        is.setValueFactoryManager(this->getIceManager()->getCommunicator()->getValueFactoryManager());
        is.read(result);
    }

    void GraspingManagerConnector::gm_load(RemoteGui::TabProxy& tab)
    {
        std::string packageName = "armar6_skills";
        armarx::CMakePackageFinder finder(packageName);
        std::filesystem::path path(finder.getDataDir() + "/" + packageName + "/PlannedGrasps");
        std::filesystem::path filePath = path / "tmp.bin";

        read_GraspingTrajectory_from_blob(_plannedTrajectory, readCompleteFile(filePath));
        {
            std::stringstream ss;
            ss << "loaded. rns=" << _plannedTrajectory.rnsToUse;
            tab.getValue<std::string>("ioStatus").set(ss.str());
        }

    }
    void GraspingManagerConnector::gm_exec(RemoteGui::TabProxy& tab)
    {
        TrajectoryPtr platformTraj = TrajectoryPtr::dynamicCast(_plannedTrajectory.poseTrajectory);
        TrajectoryPtr armTraj = TrajectoryPtr::dynamicCast(_plannedTrajectory.configTrajectory);

        ARMARX_INFO << VAROUT(platformTraj->output());
        ARMARX_INFO << VAROUT(armTraj->output());
        ARMARX_INFO << armTraj->getTimestamps();


        std::vector<Eigen::VectorXf> platformTrajEigen = TrajectotyPositionAsEigen(platformTraj);
        gm_exec_platform(tab, platformTrajEigen);
        TimeUtil::SleepMS(1000);
        gm_exec_arm(tab, armTraj);
        gm_exec_grasp(tab);

        tab.getValue<std::string>("executionStatus").set("done.");
        tab.sendUpdates();
    }

    void GraspingManagerConnector::gm_exec_platform(RemoteGui::TabProxy& tab, std::vector<Eigen::VectorXf>& platformTrajEigen)
    {
        size_t wp_i = 0;

        while (true)
        {
            RemoteRobot::synchronizeLocalClone(_robot, _robotStateComponent);
            Eigen::Vector3f target = platformTrajEigen.at(wp_i).head<3>();
            Eigen::Vector3f current = PlatformPose2Vec(_robot->getGlobalPose());

            getPlatformUnit()->moveTo(target(0), target(1), target(2), 50, 0.1);

            Eigen::Vector3f diff = target - current;
            float errP = diff.head<2>().norm();
            float errO = math::Helpers::AngleModPI(std::abs(diff(2)));

            {
                std::stringstream ss;
                ss.precision(2);
                ss << std::fixed << "moving platform. errPos: " << errP << " errOri: " << errO;
                tab.getValue<std::string>("executionStatus").set(ss.str());
                tab.sendUpdates();
                ARMARX_IMPORTANT << ss.str() << " diff: " << diff.transpose();
            }


            if (errP < 300 && errO < 0.2)
            {
                if (wp_i < platformTrajEigen.size() - 1)
                {
                    wp_i++;
                }
                else
                {
                    break;
                }
            }
            TimeUtil::SleepMS(100);
        }
        tab.getValue<std::string>("executionStatus").set("");
        tab.sendUpdates();
    }

    void GraspingManagerConnector::gm_exec_arm(RemoteGui::TabProxy& tab, TrajectoryPtr& armTraj)
    {
        TrajectoryPlayerInterfacePrx trajectoryPlayer = getTrajectoryPlayer();
        trajectoryPlayer->resetTrajectoryPlayer(false);
        TrajectoryPtr optimized_armTraj = armTraj->calculateTimeOptimalTrajectory(0.5, 0.5, 0.1, IceUtil::Time::milliSeconds(100));
        trajectoryPlayer->loadJointTraj(optimized_armTraj);
        trajectoryPlayer->setIsVelocityControl(true);

        trajectoryPlayer->startTrajectoryPlayer();
        float end_time = trajectoryPlayer->getEndTime();
        while (true)
        {
            float current_time = trajectoryPlayer->getCurrentTime();
            {
                std::stringstream ss;
                ss.precision(2);
                ss << std::fixed << "moving arm. " << current_time << " / " << end_time;
                tab.getValue<std::string>("executionStatus").set(ss.str());
                tab.sendUpdates();
            }
            if (current_time > end_time)
            {
                break;
            }
        }
        trajectoryPlayer->stopTrajectoryPlayer();
        tab.getValue<std::string>("executionStatus").set("");
        tab.sendUpdates();

        //_plannedTrajectory.grasp.
    }

    void GraspingManagerConnector::gm_exec_grasp(RemoteGui::TabProxy& tab)
    {
        tab.getValue<std::string>("executionStatus").set("Grasping");
        tab.sendUpdates();

        RemoteRobot::synchronizeLocalClone(_robot, _robotStateComponent);
        std::string side = getSideFromEef(_plannedTrajectory.grasp.eefName);
        getCartesianPositionControl()->setEnabled(side, true);
        Eigen::Matrix4f targetPrePose = defrost(_plannedTrajectory.grasp.framedPrePose);
        Eigen::Matrix4f targetGrasp = defrost(_plannedTrajectory.grasp.framedPose);


        Eigen::Vector3f approachVector = math::Helpers::GetPosition(targetPrePose - targetGrasp);
        float upDot = approachVector.normalized().dot(Eigen::Vector3f::UnitZ());
        ARMARX_IMPORTANT << VAROUT(approachVector) << VAROUT(upDot);
        bool isTopGrasp = upDot > 0.5;

        getCartesianPositionControl()->setFTLimit(side, 5, -1);
        int ftCtrInit = getCartesianPositionControl()->getFTTresholdExceededCounter(side);


        RobotNameHelper::RobotArm arm = _rnh->getRobotArm(side, _robot);

        while (true)
        {
            RemoteRobot::synchronizeLocalClone(_robot, _robotStateComponent);
            int ftCtrCurr = getCartesianPositionControl()->getFTTresholdExceededCounter(side);
            if (ftCtrCurr > ftCtrInit)
            {
                break;
            }
            targetGrasp = defrost(_plannedTrajectory.grasp.framedPose);
            if (isTopGrasp)
            {
                targetGrasp = math::Helpers::TranslatePose(targetGrasp, 0, 0, -50);
            }
            getCartesianPositionControl()->setTarget(side, targetGrasp, true);


            float posError = CartesianPositionController::GetPositionError(targetGrasp, arm.getTCP());
            {
                std::stringstream ss;
                ss.precision(2);
                ss << std::fixed << "Grasping: " << posError;
                tab.getValue<std::string>("executionStatus").set(ss.str());
            }
            tab.sendUpdates();

            if (posError < 5)
            {
                break;
            }

            TimeUtil::SleepMS(10);
        }



        Armar6HandV2Helper hh(getKinematicUnit(), getRobotUnitObserver(), side, _isSimulation);
        hh.shapeHand(1, 1);
        tab.getValue<std::string>("executionStatus").set("Grasping: Close hand");
        tab.sendUpdates();
        TimeUtil::SleepMS(1000);

        Eigen::Matrix4f targetLift = math::Helpers::TranslatePose(arm.getTCP()->getPoseInRootFrame(), Eigen::Vector3f(0, 0, 100));
        getCartesianPositionControl()->setTarget(side, targetLift, true);

        while (true)
        {
            RemoteRobot::synchronizeLocalClone(_robot, _robotStateComponent);
            float posError = CartesianPositionController::GetPositionError(targetLift, arm.getTCP());
            {
                std::stringstream ss;
                ss.precision(2);
                ss << std::fixed << "Lifting: " << posError;
                tab.getValue<std::string>("executionStatus").set(ss.str());
            }
            tab.sendUpdates();

            if (posError < 5)
            {
                break;
            }

            TimeUtil::SleepMS(10);
        }

        tab.getValue<std::string>("executionStatus").set("");
        tab.sendUpdates();

    }


    void GraspingManagerConnector::guiUpdate(RemoteGui::TabProxy& tab)
    {
        std::unique_lock lock(_allMutex);

        //ARMARX_IMPORTANT << "update gui";

        grasping::GraspCandidateSeq gcoGrasps = getGraspCandidateObserver()->getAllCandidates();

        grasping::GraspCandidateSeq sideGrasps;
        for (const auto& gc : gcoGrasps)
        {
            if (gc->executionHints->approach == grasping::ApproachType::SideApproach)
            {
                sideGrasps.push_back(gc);
            }
        }

        grasping::GraspCandidateSeq selectedGrasps = getGraspCandidateObserver()->getSelectedCandidates();

        tab.getValue<std::string>("candidates").set("got " + std::to_string(sideGrasps.size()) + " side grasp candidates, " + std::to_string(selectedGrasps.size()) + " selected");


        if (tab.getButton("selectGCs").clicked())
        {
            getGraspCandidateObserver()->setSelectedCandidates(sideGrasps);
        }
        if (tab.getButton("planGCs").clicked())
        {
            gm_plan_gc(tab, selectedGrasps);
        }
        if (tab.getButton("planSpray").clicked())
        {
            gm_plan_wm(tab, "spraybottle");
        }

        if (tab.getButton("visualize").clicked())
        {
            gm_visu(tab);
        }
        if (tab.getButton("store").clicked())
        {
            gm_store(tab);
        }
        if (tab.getButton("load").clicked())
        {
            gm_load(tab);
        }

        if (tab.getButton("execute").clicked())
        {
            gm_exec(tab);

        }



        if (tab.getButton("reset").clicked())
        {

            TrajectoryPtr platformTraj = TrajectoryPtr::dynamicCast(_plannedTrajectory.poseTrajectory);
            TrajectoryPtr armTraj = TrajectoryPtr::dynamicCast(_plannedTrajectory.configTrajectory);
            Eigen::VectorXf platformTarget = platformTraj->begin()->getPositionsAsVectorXf();
            getPlatformUnit()->moveTo(platformTarget(0), platformTarget(1), platformTarget(2), 50, 0.1);

            KinematicUnitHelper kh(getKinematicUnit());
            kh.setJointAngles(armTraj->begin()->getPositionsAsNameValueMap<float>());

            Armar6HandV2Helper hh(getKinematicUnit(), getRobotUnitObserver(), getSideFromEef(_plannedTrajectory.grasp.eefName), _isSimulation);
            hh.shapeHand(0, 0);

        }


    }

    std::vector<Eigen::VectorXf> GraspingManagerConnector::TrajectotyPositionAsEigen(const TrajectoryPtr& traj)
    {
        std::vector<Eigen::VectorXf> r;
        for (auto it = traj->begin(); it != traj->end(); ++it)
        {
            r.emplace_back(it->getPositionsAsVectorXf());
        }
        return r;
    }

    Eigen::Matrix4f GraspingManagerConnector::defrost(const FramedPoseBasePtr& pose)
    {
        return FramedPosePtr::dynamicCast(pose)->toRootEigen(_robot);
    }

    std::string GraspingManagerConnector::getSideFromEef(const std::string& eef)
    {
        std::map<std::string, std::string> eef2armMap;
        eef2armMap[_rnh->getArm("Right").getEndEffector()] = "Right";
        eef2armMap[_rnh->getArm("Left").getEndEffector()] = "Left";
        return eef2armMap.at(eef);
    }
}
