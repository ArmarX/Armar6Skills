armarx_add_component(GraspingManagerConnector
    SOURCES
        ./GraspingManagerConnector.cpp
    HEADERS
        ./GraspingManagerConnector.h
    DEPENDENCIES
        ArmarXCore
        ArmarXCoreInterfaces # for DebugObserverInterface
        RobotAPICore
        RobotAPIInterfaces
        RobotComponentsInterfaces
        RobotAPIComponentPlugins
        ArmarXGuiComponentPlugins
        RobotStatechartHelpers
        Armar6Tools
)
