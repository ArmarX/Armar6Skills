/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::GraspingManagerConnector
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/RemoteGuiComponentPlugin.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/GraspCandidateObserverComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/PlatformUnitComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/KinematicUnitComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/TrajectoryPlayerComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/CartesianPositionControlComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotUnitObserverComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotUnitComponentPlugin.h>

#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>

#include <RobotComponents/interface/components/GraspingManager/GraspingManagerInterface.h>

#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>

#include <RobotAPI/interface/components/TrajectoryPlayerInterface.h>
#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>
#include <RobotAPI/libraries/core/Trajectory.h>
#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>

namespace armarx
{


    /**
     * @class GraspingManagerConnectorPropertyDefinitions
     * @brief Property definitions of `GraspingManagerConnector`.
     */
    class GraspingManagerConnectorPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        GraspingManagerConnectorPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-GraspingManagerConnector GraspingManagerConnector
     * @ingroup armar6_skills-Components
     * A description of the component GraspingManagerConnector.
     *
     * @class GraspingManagerConnector
     * @ingroup Component-GraspingManagerConnector
     * @brief Brief description of class GraspingManagerConnector.
     *
     * Detailed description of class GraspingManagerConnector.
     */
    class GraspingManagerConnector :
        virtual public armarx::Component,
        virtual public RemoteGuiComponentPluginUser,
        virtual public ArVizComponentPluginUser,
        virtual public GraspCandidateObserverComponentPluginUser,
        virtual public PlatformUnitComponentPluginUser,
        virtual public KinematicUnitComponentPluginUser,
        virtual public TrajectoryPlayerComponentPluginUser,
        virtual public CartesianPositionControlComponentPluginUser,
        virtual public RobotUnitObserverComponentPluginUser,
        virtual public RobotUnitComponentPluginUser

    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;
        static Eigen::Vector3f PlatformPose2Vec(const Eigen::Matrix4f& pose);
        static Eigen::Matrix4f PlatformVec2Pose(const Eigen::Vector3f& vec);

        struct SateAtPlanningTime
        {
            Eigen::Matrix4f platformPose;
            std::map<std::string, float> leftArmConfig;
            std::map<std::string, float> rightArmConfig;
        };



    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void guiCreate();
        void guiUpdate(RemoteGui::TabProxy& tab);

        static std::vector<Eigen::VectorXf> TrajectotyPositionAsEigen(const TrajectoryPtr& traj);

        Eigen::Matrix4f defrost(const FramedPoseBasePtr& pose);
        std::string getSideFromEef(const std::string& eef);


    private:

        std::mutex _allMutex;

        // Private methods and member variables go here.

        /// Debug observer. Used to visualize e.g. time series.
        armarx::DebugObserverInterfacePrx _debugObserver;
        /// Debug drawer. Used for 3D visualization.
        armarx::DebugDrawerTopic debugDrawer;

        GraspingManagerInterfacePrx _gm;
        RobotNameHelperPtr _rnh;
        RobotStateComponentInterfacePrx                 _robotStateComponent = nullptr;
        //TrajectoryPlayerInterfacePrx                    _trajectoryPlayer = nullptr;
        VirtualRobot::RobotPtr _robot = nullptr;
        GraspingTrajectory _plannedTrajectory;
        grasping::GraspCandidatePtr _plannedGC;
        SateAtPlanningTime _stateAtPlanningTime;
        bool _isSimulation = false;

    private:
        void gm_plan_gc(RemoteGui::TabProxy& tab, grasping::GraspCandidateSeq& gcoGrasps);
        void gm_plan_wm(RemoteGui::TabProxy& tab, const std::string& objectName);
        void gm_plan_int(RemoteGui::TabProxy& tab, GeneratedGraspList gmGrasps);
        void gm_visu(RemoteGui::TabProxy& tab);
        void gm_store(RemoteGui::TabProxy& tab);
        void gm_load(RemoteGui::TabProxy& tab);
        void gm_exec(RemoteGui::TabProxy& tab);
        void gm_exec_platform(RemoteGui::TabProxy& tab, std::vector<Eigen::VectorXf>& platformTrajEigen);
        void gm_exec_arm(RemoteGui::TabProxy& tab, TrajectoryPtr& armTraj);
        void gm_exec_grasp(RemoteGui::TabProxy& tab);
        void read_GraspingTrajectory_from_blob(GraspingTrajectory& result, const std::string_view& sv);

    };
}
