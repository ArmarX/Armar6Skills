/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::Armar6GraspExecution
 * @author     armar-user ( armar-user at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
//#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>


namespace armarx
{
    /**
     * @class Armar6GraspExecutionPropertyDefinitions
     * @brief Property definitions of `Armar6GraspExecution`.
     */
    class Armar6GraspExecutionPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        Armar6GraspExecutionPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-Armar6GraspExecution Armar6GraspExecution
     * @ingroup armar6_skills-Components
     * A description of the component Armar6GraspExecution.
     *
     * @class Armar6GraspExecution
     * @ingroup Component-Armar6GraspExecution
     * @brief Brief description of class Armar6GraspExecution.
     *
     * Detailed description of class Armar6GraspExecution.
     */
    class Armar6GraspExecution :
        virtual public armarx::Component,
        virtual public armarx::ArVizComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        // Private methods and member variables go here.

        /// Debug observer. Used to visualize e.g. time series.
        armarx::DebugObserverInterfacePrx debugObserver;
        /// Debug drawer. Used for 3D visualization.
        //armarx::DebugDrawerTopic debugDrawer;


    };
}
