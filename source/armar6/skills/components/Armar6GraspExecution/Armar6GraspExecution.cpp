/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::Armar6GraspExecution
 * @author     armar-user ( armar-user at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Armar6GraspExecution.h"


namespace armarx
{
    Armar6GraspExecutionPropertyDefinitions::Armar6GraspExecutionPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("ConditionHandlerName", "ConditionHandler", "Name of the condition handler that should be used");
        defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the debug observer that should be used");
        defineOptionalProperty<std::string>("RequestableServicesTopicName", "ServiceRequests", "Name of the requestable services topic that should be used");
        defineOptionalProperty<std::string>("SystemObserverName", "SystemObserver", "Name of the system observer that should be used");
        defineOptionalProperty<std::string>("MessageDisplayName", "MessageDisplayTopic", "Name of the message display that should be used");
        defineOptionalProperty<std::string>("RemoteGuiName", "RemoteGuiProvider", "Name of the remote gui that should be used");
        defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the debug drawer topic that should be used");
        defineOptionalProperty<std::string>("ForceTorqueUnitObserverName", "ForceTorqueUnitObserver", "Name of the force torque unit observer that should be used");
        defineOptionalProperty<std::string>("GraspCandidateObserverName", "GraspCandidateObserver", "Name of the grasp candidate observer that should be used");
        defineOptionalProperty<std::string>("KinematicUnitName", "Armar6KinematicUnit", "Name of the kinematic unit that should be used");
        defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
        defineOptionalProperty<std::string>("RobotUnitName", "Armar6Unit", "Name of the robot unit that should be used");
        defineOptionalProperty<std::string>("RobotUnitObserverName", "RobotUnitObserver", "Name of the robot unit observer that should be used");
        defineOptionalProperty<std::string>("TextToSpeechTopicName", "TextToSpeech", "Name of the text to speech topic that should be used");
    }


    std::string Armar6GraspExecution::getDefaultName() const
    {
        return "Armar6GraspExecution";
    }


    void Armar6GraspExecution::onInitComponent()
    {
        // Register offered topices and used proxies here.
        offeringTopicFromProperty("DebugObserverName");
        // debugDrawer.offeringTopic(*this);  // Calls this->offeringTopic().

        // Using a proxy will cause the component to wait until the proxy is available.
        // usingProxyFromProperty("MyProxyName");
    }


    void Armar6GraspExecution::onConnectComponent()
    {
        // Get topics and proxies here. Pass the *InterfacePrx type as template argument.
        getTopicFromProperty(debugObserver, "DebugObserverName");
        // debugDrawer.getTopic(*this);  // Calls this->getTopic().

        // getProxyFromProperty<MyProxyInterfacePrx>("MyProxyName");
    }


    void Armar6GraspExecution::onDisconnectComponent()
    {

    }


    void Armar6GraspExecution::onExitComponent()
    {

    }




    armarx::PropertyDefinitionsPtr Armar6GraspExecution::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new Armar6GraspExecutionPropertyDefinitions(
                getConfigIdentifier()));
    }
}
