/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <armar6/skills/components/PlacementAreaProvider/PlacementAreaProvider.h>

using namespace armarx;

RemoteGui::WidgetPtr PlacementAreaProvider::createRemoteGui()
{
    using namespace RemoteGui;

    this->objPoses = getObjectPoses();
    objPoses.push_back(getDefaultObject());

    std::vector<std::string> objectNames = getObjectNames(this->objPoses);

    WidgetPtr rootLayout = makeVBoxLayout()
                           .addChild(tab->makeConstraintsLayout(objectNames))
                           .addChild(makeGroupBox("Position constraints").addChild(tab->makePositionLayout(objectNames)))
                           .addChild(makeGroupBox("Orientation constraints").addChild(tab->makeOrientationLayout()));

    return rootLayout;
}

void PlacementAreaProvider::processRemoteGui(RemoteGui::TabProxy& prx)
{
    prx.receiveUpdates();

    /*
     * if "go" button pressed:
     * 1. receive parameter values from gui
     * 2. select correct use case, determine special parameters
     * 3. run use case
     * 4. report results to listeners
     */

    if (prx.getButton("Go").clicked())
    {
        /*
         * save current octomap to binary file
         */
        std::vector<Vector3BasePtr> centers;
        double resolution;
        this->map->getOccupiedLeafCenters(centers, resolution);

        octomap::OcTree* tree = new octomap::OcTree(resolution);
        constructOctree(tree, centers);

        /*
         * construct octree from existing octree
         */
        placement = std::make_shared<PlacementArea>(
                        tree,
                        this->minSize,
                        this->minHeight,
                        this->maxHeight,
                        this->minVoxelCount,
                        this->threshold,
                        this->enableVoxelPreprocessing,
                        this->enablePlanePreprocessing);
        delete tree;

        ARMARX_INFO << "Receiving parameters from RemoteGUI";

        tab->updateDataset(prx);


        objpose::ObjectPose input;
        objpose::ObjectPose target;

        if (!determineObjectPose(tab->getDataset()->input_id, input))
        {
            ARMARX_WARNING << "Could not find object pose from name";
        }
        else if (toPositionConstraint(tab->getDataset()->pos_constraint) == POS_ON_OBJECT
                 && !determineObjectPose(tab->getDataset()->target_id, target))
        {
            ARMARX_WARNING << "Could not find target object pose from name";
        }
        else
        {
            /*
             * clear previously extracted results
             */
            this->result.clear();

            /*
             * select use case and compute suitable regions
             */
            computePlacementArea(input, target);

            if (this->result.empty())
            {
                ARMARX_INFO << "Could not extract any suitable placement areas from scene";
            }
            else
            {
                ARMARX_INFO << "Reporting" << std::to_string(this->result.size())
                            << " new suitable placement areas";
                listenerPrx->reportNewPlacementAreas(this->result);
            }

            /*
             * Visualize extracted placement areas, planar regions, octree
             */
            visu->visualizeOctree(this->placement);
            visu->visualizePlanes(this->placement->getPlanes());
            visu->visualizeSamples(this->result);
        }
    }

    if (prx.getButton("Update objects").clicked())
    {
        /*
         * update dataset and generate RemoteGui from new dataset
         */

        ARMARX_INFO << "Updating dataset and receiving new objects";

        tab->updateDataset(prx);
        createRemoteGui();
    }
}

void PlacementAreaProvider::computePlacementArea(const objpose::ObjectPose& input, const objpose::ObjectPose& target)
{
    placement::RemoteGuiDatasetPtr data = tab->getDataset();

    /*
     * select correct use case according to input constraints
     */
    switch (toPositionConstraint(data->pos_constraint))
    {
        case POS_EXACT:
        {
            if (toOrientationConstraint(data->rot_constraint) == ROT_EXACT)
            {
                ARMARX_INFO << "Computing placement area from target pose";
                computeAreaPose(toIce(input), new Pose(simox::math::rpy_to_mat3f(data->rpy), data->center), this->result);
            }
            else
            {
                ARMARX_INFO << "Computing placement area from target position";
                computeAreaPosition(toIce(input), new Vector3(data->center), this->result);
            }
            break;
        }
        case POS_REGION:
        {
            const simox::XYConstrainedOrientedBoxf xyOB(data->corner, data->yaw, data->dimensions);

            if (toOrientationConstraint(data->rot_constraint) == ROT_EXACT)
            {
                ARMARX_INFO << "Computing placement area from target region and target orientation";
                computeAreaRegionOrientation(toIce(input), xyOB, new Quaternion(simox::math::rpy_to_mat3f(data->rpy)), this->result);
            }
            else
            {
                ARMARX_INFO << "Computing placement area from target region";
                computeAreaRegion(toIce(input), xyOB, this->result);
            }
            break;
        }
        case POS_ON_OBJECT:
        {
            const simox::XYConstrainedOrientedBoxf xyOB = toXYConstrainedBox(determineOrientedBox(target));

            if (toOrientationConstraint(data->rot_constraint) == ROT_EXACT)
            {
                ARMARX_INFO << "Computing placement area on top of target object with target orientation";
                computeAreaRegionOrientation(toIce(input), xyOB, new Quaternion(simox::math::rpy_to_mat3f(data->rpy)), this->result);
            }
            else
            {
                ARMARX_INFO << "Computing placement area on top of target object";
                computeAreaRegion(toIce(input), xyOB, this->result);
            }
            break;
        }
        case POS_ARBITRARY:
        {
            if (toOrientationConstraint(data->rot_constraint) == ROT_EXACT)
            {
                ARMARX_INFO << "Computing placement area from target orientation";
                computeAreaOrientation(toIce(input), new Quaternion(simox::math::rpy_to_mat3f(data->rpy)), this->result);
            }
            else
            {
                ARMARX_INFO << "Computing placement area with arbitrary position and orientation";
                computeArea(toIce(input), this->result);
            }
            break;
        }
    }
}
