/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <armar6/skills/components/PlacementAreaProvider/Sampling/Sampling.h>

using namespace armarx;

Sampling::Sampling(size_t numSamples) : numSamples(numSamples)
{

}

void Sampling::sampleRegion(const simox::XYConstrainedOrientedBoxf& region,
                            const float height,
                            std::vector<Eigen::Vector3f>& sample)
{
    generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
    std::uniform_real_distribution<float> distribution(0.f, 1.f);

    for (size_t i = 0; i < this->numSamples; ++i)
    {
        Eigen::Vector3f pos = region.corner_min()
                              + distribution(generator) * region.axis(0) * region.dimension(0)
                              + distribution(generator) * region.axis(1) * region.dimension(1);
        pos(2) = height;

        sample.push_back(pos);
    }
}

void Sampling::sampleRegion(const PlanePtr& plane, std::vector<Eigen::Vector3f>& sample)
{
    generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
    std::uniform_int_distribution<size_t> sizeDist(0, plane->getInputCloud()->size() - 1);
    std::uniform_real_distribution<float> offsetDist(-1.f, 1.f);

    for (size_t i = 0; i < this->numSamples; ++i)
    {
        auto pos = plane->getInputCloud()->points[sizeDist(generator)];

        auto xOffset = offsetDist(generator);
        auto yOffset = offsetDist(generator);

        sample.push_back(Eigen::Vector3f(pos.x + xOffset, pos.y + yOffset, pos.z));
    }
}

void Sampling::sampleYawAngle(std::vector<float>& sample)
{
    generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
    std::uniform_real_distribution<float> distribution(0.f, 2 * M_PI);

    for (size_t i = 0; i < this->numSamples; ++i)
    {
        sample.push_back(distribution(generator));
    }
}

void Sampling::distributeSamples(const std::vector<PlanePtr>& planes, const size_t numSamples, std::vector<size_t>& distribution)
{
    distribution.clear();

    float n = 0.f;
    for (PlanePtr plane : planes)
    {
        n += static_cast<float>(plane->getInputCloud()->size());
    }

    for (PlanePtr plane : planes)
    {
        const float size = static_cast<float>(plane->getInputCloud()->size());
        const size_t samples = static_cast<size_t>((size / n) * static_cast<float>(numSamples));
        /*
         * generate at least one sample per planar region
         */
        distribution.push_back(samples > 1 ? samples : 1);
    }
}

size_t Sampling::getNumSamples() const
{
    return this->numSamples;
}

void Sampling::setNumSamples(const size_t numSamples)
{
    this->numSamples = numSamples;
}
