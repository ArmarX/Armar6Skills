/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Dense>

#include <ArmarXCore/interface/serialization/Shapes.h>
#include <SimoxUtility/shapes/XYConstrainedOrientedBox.h>

#include <armar6/skills/components/PlacementAreaProvider/DataStructures/Plane.h>

#include <chrono>
#include <random>
#include <vector>

namespace armarx
{
    /**
     * @brief The Sampling class provides basic functionality to generate
     * samplings of positions and/or orientations.
     */
    class Sampling
    {
    public:
        /**
         * @brief Constructor for the Sampling class
         * @param numSamples Number of samples to generate
         */
        Sampling(size_t numSamples);

        /**
         * @brief Generates a sampling of positions in the x-y-plane within a region
         * @param region Sampling region as horizontal oriented box
         * @param height Height of all generated samples
         * @param sample Generated sample of positions
         */
        void sampleRegion(const simox::XYConstrainedOrientedBoxf& region,
                          const float height,
                          std::vector<Eigen::Vector3f>& sample);

        /**
         * @brief Generates a sampling of positions from a horizontal plane
         * @param points Horizontal plane to sample from
         * @param sample Generated sample of positions
         */
        void sampleRegion(const PlanePtr& plane, std::vector<Eigen::Vector3f>& sample);

        /**
         * @brief Generates a sampling of yaw angles
         * @param sample Generated sample of Angles
         */
        void sampleYawAngle(std::vector<float>& sample);

        /**
         * @brief Calculates the number of samples generated for each planar region
         * @param planes Set (vector) of planar regions
         * @param numSamples Total number of generated samples to distribute
         * @param distribution Resulting number of samples for each planar region
         */
        static void distributeSamples(const std::vector<PlanePtr>& planes, const size_t numSamples, std::vector<size_t>& distribution);

        size_t getNumSamples() const;

        void setNumSamples(const size_t numSamples);

    private:
        size_t numSamples;
        std::default_random_engine generator;
    };

    using SamplingPtr = std::shared_ptr<Sampling>;
}
