/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <octomap/OcTreeNode.h>

namespace armarx
{
    /**
     * @brief The OcTreeNodeM class extends the OcTreeNode allowing it to be
     * marked.
     */
    class OcTreeNodeM : public octomap::OcTreeNode
    {
    public:
        /**
         * @brief Constructor for the OcTreeNodeM class
         * @param marked Determines whether the node is marked
         */
        OcTreeNodeM(bool marked = false);

        /**
         * @brief Check whether the node is marked
         * @return true if the node is marked, false otherwise
         */
        bool isMarked() const;

        /**
         * @brief Marks or unmarks a node
         * @param marked true if the node is marked, false otherwise
         */
        void mark(const bool marked);

    private:
        bool marked;
    };
}
