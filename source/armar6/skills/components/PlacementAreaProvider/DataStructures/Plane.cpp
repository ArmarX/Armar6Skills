/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <armar6/skills/components/PlacementAreaProvider/DataStructures/Plane.h>

using namespace armarx;

Plane::Plane(const std::vector<Eigen::Vector3f>& points,
             const float resolution,
             const float threshold,
             const int knn)
    : resolution(resolution), threshold(threshold), knn(knn)
{
    initialize(points);
    computeAABB(points);
}

bool Plane::isEmpty() const
{
    return this->pointCloud->empty();
}

float Plane::getResolution() const
{
    return this->resolution;
}

float Plane::getHeight() const
{
    return this->height;
}

void Plane::setHeight(float height)
{
    if (!isEmpty())
    {
        this->height = height;

        /*
         * update height of input point cloud
         */
        for (size_t i = 0; i < pointCloud->size(); ++i)
        {
            (*pointCloud)[i].z = height;
        }

        setInputCloud(pointCloud);
    }
}

bool Plane::contains(const Eigen::Vector3f& point) const
{
    /*
     * point is not in same horizontal plane
     */
    if (std::abs(this->height - point(2)) > this->threshold)
    {
        return false;
    }

    std::vector<int> indices(this->knn);
    std::vector<float> sqrDist(this->knn);

    Point3 searchPoint;
    searchPoint.x = point(0);
    searchPoint.y = point(1);
    searchPoint.z = point(2);

    int n = nearestKSearch(searchPoint, this->knn, indices, sqrDist);

    /*
     * no neighbours found
     */
    if (n < 1)
    {
        return false;
    }

    bool cflag = false;

    /*
     * distance to at least one neighbour within half voxel side length - margin
     */
    for (int i = 0; i < n; ++i)
    {
        cflag = cflag || std::sqrt(sqrDist[i]) < 0.5f * (resolution - this->threshold);
    }

    return cflag;
}

float Plane::getThreshold() const
{
    return this->threshold;
}

void Plane::setThreshold(float threshold)
{
    if (threshold > 0)
    {
        this->threshold = threshold;
    }
}

int Plane::getKnn() const
{
    return this->knn;
}

void Plane::setKnn(int knn)
{
    if (knn > 0 && knn <= static_cast<int>(this->pointCloud->size()))
    {
        this->knn = knn;
    }
}

const Eigen::AlignedBox3f Plane::getAABB() const
{
    return this->aabb;
}

void Plane::initialize(const std::vector<Eigen::Vector3f>& points)
{

    float height = 0.f;

    this->pointCloud = pcl::PointCloud<Point3>::Ptr(new pcl::PointCloud<Point3>());

    if (!points.empty())
    {
        /*
         * set height to
         * - average height of input points plus
         * - half resolution (voxel centers)
         */
        for (auto p : points)
        {
            height += p(2);
        }
        height = height / static_cast<float>(points.size()) + 0.5f * this->resolution;

        pointCloud->width = points.size();
        pointCloud->height = 1;
        pointCloud->points.resize(pointCloud->width * pointCloud->height);

        /*
         * generate input point cloud from vector of points
         */
        for (size_t i = 0; i < points.size(); ++i)
        {
            (*pointCloud)[i].x = points[i](0);
            (*pointCloud)[i].y = points[i](1);
            (*pointCloud)[i].z = height;
        }

        setInputCloud(pointCloud);
    }

    this->height = height;
}

void Plane::computeAABB(const std::vector<Eigen::Vector3f>& points)
{
    if (points.empty())
    {
        this->aabb = Eigen::AlignedBox3f();
    }
    else
    {
        /*
         * calculate AABB of points
         */
        const auto [xMin, xMax] = std::minmax_element(points.begin(), points.end(),
                                  [](const Eigen::Vector3f & l, const Eigen::Vector3f & r)
        {
            return l(0) < r(0);
        });

        const auto [yMin, yMax] = std::minmax_element(points.begin(), points.end(),
                                  [](const Eigen::Vector3f & l, const Eigen::Vector3f & r)
        {
            return l(1) < r(1);
        });

        const float xmin = (*xMin)(0);
        const float xmax = (*xMax)(0);
        const float ymin = (*yMin)(1);
        const float ymax = (*yMax)(1);

        const Eigen::Vector3f min(xmin, ymin, this->height - this->resolution);
        const Eigen::Vector3f max(xmax, ymax, this->height);

        this->aabb = Eigen::AlignedBox3f(min, max);
    }
}
