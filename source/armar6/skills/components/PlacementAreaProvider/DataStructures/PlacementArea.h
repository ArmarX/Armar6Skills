/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <queue>
#include <iostream>
#include <fstream>

#include <octomap/octomap.h>
#include <Eigen/Dense>

#include <armar6/skills/components/PlacementAreaProvider/DataStructures/OcTreeNodeM.h>
#include <armar6/skills/components/PlacementAreaProvider/DataStructures/Plane.h>

#include <armar6/skills/components/PlacementAreaProvider/Util/OctreeUtil.h>
#include <armar6/skills/components/PlacementAreaProvider/Util/TypeConversions.h>

namespace armarx
{
    /**
     * @brief The PlacementArea class represents a 3D environment by storing
     * occupied surface voxels in an octree.
     *
     * The PlacementArea class provides functionality to extract valid horizontal
     * surface areas which may be used as placement areas for objects.
     *
     * @see octomap::OcTree
     */
    class PlacementArea : public octomap::OccupancyOcTreeBase<OcTreeNodeM>,
        public armarx::Logging
    {
    public:
        /**
         * @brief Constructs a PlacementArea from an octomap::OcTree binary file while setting preprocessing flags.
         * @param path Path to the binary file holding the octomap::Octree
         * @param minSize Minimum side length of a quadratic plane
         * @param minHeight Minimum voxel height of scene voxels
         * @param maxHeight Maximum voxel height of scene voxels
         * @param minVoxelCount Minimum voxel quantity per plane
         * @param threshold Equality threshold for floating point comparison
         * @param enableVoxelPreprocessing Flag determining whether to filter invalid octree nodes for plane extraction, @see filterVoxels()
         * @param enablePlanePreprocessing Flag determining whether to preprocess the octree by extracting planes, @see computePlanes()
         */
        PlacementArea(std::string path,
                      float minSize,
                      float minHeight = 0.f,
                      float maxHeight = 4000.f,
                      size_t minVoxelCount = 2,
                      float threshold = 1e-6,
                      bool enableVoxelPreprocessing = true,
                      bool enablePlanePreprocessing = true);

        /**
         * @brief Constructs an empty PlacementArea while setting preprocessing flags.
         * @param resolution Resolution of the octomap::Octree
         * @param minSize Minimum side length of a quadratic plane
         * @param minHeight Minimum voxel height of scene voxels
         * @param maxHeight Maximum voxel height of scene voxels
         * @param minVoxelCount Minimum voxel quantity per plane
         * @param threshold Equality threshold for floating point comparison
         * @param enableVoxelPreprocessing Flag determining whether to filter unfit octree nodes for plane extraction, @see filterVoxels()
         * @param enablePlanePreprocessing Flag determining whether to preprocess the octree by extracting planes, @see computePlanes()
         */
        PlacementArea(float resolution,
                      float minSize,
                      float minHeight = 0.f,
                      float maxHeight = 4000.f,
                      size_t minVoxelCount = 2,
                      float threshold = 1e-6,
                      bool enableVoxelPreprocessing = true,
                      bool enablePlanePreprocessing = true);

        /**
         * @brief constructs a PlacementArea from a given OccupancyOctreeBase
         * @param minSize Minimum side length of a quadratic plane
         * @param minHeight Minimum voxel height of scene voxels
         * @param maxHeight Maximum voxel height of scene voxels
         * @param minVoxelCount Minimum voxel quantity per plane
         * @param threshold Equality threshold for floating point comparison
         * @param enableVoxelPreprocessing Flag determining whether to filter unfit octree nodes for plane extraction, @see filterVoxels()
         * @param enablePlanePreprocessing Flag determining whether to preprocess the octree by extracting planes, @see computePlanes()
         */
        template<class NODE>
        PlacementArea(octomap::OccupancyOcTreeBase<NODE>* tree,
                      float minSize,
                      float minHeight = 0.f,
                      float maxHeight = 4000.f,
                      size_t minVoxelCount = 2,
                      float threshold = 1e-6,
                      bool enableVoxelPreprocessing = true,
                      bool enablePlanePreprocessing = true) :
            octomap::OccupancyOcTreeBase<OcTreeNodeM>(10.0),
            minSize(minSize),
            minHeight(minHeight),
            maxHeight(maxHeight),
            minVoxelCount(minVoxelCount),
            threshold(threshold),
            enableVoxelPreprocessing(enableVoxelPreprocessing),
            enablePlanePreprocessing(enablePlanePreprocessing)
        {
            setTag("PlacementArea");

            this->resolution = tree->getResolution();

            typename octomap::OccupancyOcTreeBase<NODE>::leaf_iterator it = tree->begin_leafs();
            typename octomap::OccupancyOcTreeBase<NODE>::leaf_iterator end = tree->end_leafs();

            ARMARX_INFO << "Adding leaf nodes to placement area";
            for (; it != end; ++it)
            {
                if (tree->isNodeOccupied(*it))
                {
                    updateNode(it.getCoordinate(), true);
                }
            }

            ARMARX_INFO << "Expanding all pruned nodes.";
            expand();

            if (enableVoxelPreprocessing)
            {
                filterVoxels();
            }

            if (enablePlanePreprocessing)
            {
                computePlanes();
            }
        }

        /**
         * @brief Processing the entire octree by removing unfit octree nodes regarding plane extraction.
         * @see filterVoxels(const octomap::point3d& bbxMin, const octomap::point3d& bbxMax)
         */
        void filterVoxels();

        /**
         * @brief Processing the octree by removing unfit octree nodes regarding plane extraction.
         *
         * Occupied leaf nodes are removed if
         *  1. their top neighbour is an occupied leaf node
         *  2. they have no neighbours in the same plane
         *
         * In both cases, the original node can not be part of a valid placement area.
         *
         * @param bbxMin Minimimum of the bounding box to process
         * @param bbxMax Maximum of the bounding box to process
         */
        void filterVoxels(const octomap::point3d& bbxMin, const octomap::point3d& bbxMax);

        /**
         * @brief Processing the entire octree by extracting planar surfaces.
         * @see computePlanes(const octomap::point3d& bbxMin, const octomap::point3d& bbxMax)
         */
        void computePlanes();

        /**
         * @brief Processing the octree by extracting planar surfaces.
         * @param bbxMin Minimimum of the bounding box to process
         * @param bbxMax Maximum of the bounding box to process
         * @param append Flag determining whether to append planes or clear existing ones
         */
        void computePlanes(const octomap::point3d& bbxMin, const octomap::point3d& bbxMax, bool append = false);

        /**
         * @brief Removes octree nodes without neighbours in the same horizontal plane
         * @param key Key of the node to check
         * @param useNhood8 Flag determining whether to use nhood4 or nhood8 for neighbourhood calculations
         * @return true if the node was removed, false otherwise
         */
        bool removeSingleNode(const octomap::OcTreeKey& key, bool useNhood8 = false);

        /**
         * @brief Checks whether a given plane is smaller than minSize
         * @param plane Pointer to the plane to check
         * @return true if the plane is smaller than minSize, false otherwise
         */
        bool isSmallPlane(const PlanePtr plane) const;

        /**
         * @brief @see octomap::AbstractOcTree
         * @return new PlacementArea with the same parameters
         */
        PlacementArea* create() const;

        /**
         * @brief Checks whether a given node is marked
         * @param node Reference to the node
         * @return true if the node is marked, false otherwise
         */
        bool isNodeMarked(const OcTreeNodeM& node) const;

        /**
         * @brief Checks whether a given node is marked
         * @param node Pointer to the node
         * @return true if the node is marked, false otherwise
         */
        bool isNodeMarked(OcTreeNodeM* node) const;

        /**
         * @brief Marks a given node
         * @param node Reference to the node
         * @param marked true if the node is marked, false otherwise
         */
        void markNode(OcTreeNodeM& node, const bool marked);

        /**
         * @brief Marks a given node
         * @param node Pointer to the node
         * @param marked true if the node is marked, false otherwise
         */
        void markNode(OcTreeNodeM* node, const bool marked);

        /**
         * @brief Getter for possible placement areas.
         * @return a const reference to the vector of placement areas
         */
        const std::vector<PlanePtr>& getPlanes() const;

        /**
         * @brief Getter for minimum placement area size
         * @return minimum placement area size
         */
        float getMinSize() const;

        /**
         * @brief Getter for the minimum quantity of voxels per plane
         * @return minimum voxel quantity
         */
        size_t getMinVoxelCount() const;

        /**
         * @brief Getter for the floating point comparison equality threshold
         * @return equality threshold
         */
        float getThreshold() const;

    protected:
        void bfs(const octomap::OcTreeKey& start, std::vector<Eigen::Vector3f>& plane);

        bool setNodeOccupied(const octomap::OcTreeKey& key, bool occupied = true);

        bool setNodeOccupied(OcTreeNodeM* node, bool occupied = true);

    private:
        float minSize;
        float minHeight;
        float maxHeight;
        size_t minVoxelCount;
        float threshold;
        bool enableVoxelPreprocessing;
        bool enablePlanePreprocessing;

        std::vector<PlanePtr> planes;
    };

    using PlacementAreaPtr = std::shared_ptr<PlacementArea>;
}
