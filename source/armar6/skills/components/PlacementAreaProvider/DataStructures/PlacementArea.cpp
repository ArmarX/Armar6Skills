/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <armar6/skills/components/PlacementAreaProvider/DataStructures/PlacementArea.h>

using namespace armarx;

PlacementArea::PlacementArea(std::string path,
                             float minSize,
                             float minHeight,
                             float maxHeight,
                             size_t minVoxelCount,
                             float threshold,
                             bool enableVoxelPreprocessing,
                             bool enablePlanePreprocessing) :
    octomap::OccupancyOcTreeBase<OcTreeNodeM>(10.0),
    minSize(minSize),
    minHeight(minHeight),
    maxHeight(maxHeight),
    minVoxelCount(minVoxelCount),
    threshold(threshold),
    enableVoxelPreprocessing(enableVoxelPreprocessing),
    enablePlanePreprocessing(enablePlanePreprocessing)
{
    setTag("PlacementArea");

    std::ifstream sceneFile(path.c_str(), std::ios_base::binary);
    if (!readBinary(sceneFile))
    {
        ARMARX_INFO << "Could not read octree from binary file.";
    }

    ARMARX_INFO << "Expanding all pruned nodes.";
    expand();

    if (enableVoxelPreprocessing)
    {
        filterVoxels();
    }

    if (enablePlanePreprocessing)
    {
        computePlanes();
    }
}

PlacementArea::PlacementArea(float resolution,
                             float minSize,
                             float minHeight,
                             float maxHeight,
                             size_t minVoxelCount,
                             float threshold,
                             bool enableVoxelPreprocessing,
                             bool enablePlanePreprocessing) :
    octomap::OccupancyOcTreeBase<OcTreeNodeM>(resolution),
    minSize(minSize),
    minHeight(minHeight),
    maxHeight(maxHeight),
    minVoxelCount(minVoxelCount),
    threshold(threshold),
    enableVoxelPreprocessing(enableVoxelPreprocessing),
    enablePlanePreprocessing(enablePlanePreprocessing)
{
    setTag("PlacementArea");

    if (enableVoxelPreprocessing)
    {
        filterVoxels();
    }

    if (enablePlanePreprocessing)
    {
        computePlanes();
    }
}

void PlacementArea::filterVoxels()
{

    double xmin, ymin, zmin, xmax, ymax, zmax;
    getMetricMin(xmin, ymin, zmin);
    getMetricMax(xmax, ymax, zmax);

    octomap::point3d bbxMin(xmin, ymin, zmin);
    octomap::point3d bbxMax(xmax, ymax, zmax);

    filterVoxels(bbxMin, bbxMax);
}

void PlacementArea::filterVoxels(const octomap::point3d& bbxMin, const octomap::point3d& bbxMax)
{
    ARMARX_INFO << "Removing leaf nodes which can not be part of a horizontal plane";

    leaf_bbx_iterator it = this->begin_leafs_bbx(bbxMin, bbxMax);
    leaf_bbx_iterator end = this->end_leafs_bbx();

    for (; it != end; ++it)
    {
        if (isNodeOccupied(*it))
        {
            /*
             * mark node for deletion if
             * - its height is smaller than minimum voxel height OR
             * - its height is greater than maximimum voxel height OR
             * - it has no occupied neighbours in the same plane OR
             * - it has an occupied top neighbour
             */
            if (static_cast<float>(keyToCoord(it.getKey()).z()) < minHeight
                || static_cast<float>(keyToCoord(it.getKey()).z()) > maxHeight
                || isSingleNode(this, it.getKey())
                || hasTopNeighbour(this, it.getKey()))
            {
                markNode(*it, true);
            }
        }
    }

    for (it = this->begin_leafs_bbx(bbxMin, bbxMax); it != end; ++it)
    {
        /*
         * unmark and delete nodes marked for deletion
         */
        if (it->isMarked())
        {
            markNode(*it, false);
            deleteNode(it.getKey());
        }
    }
}

void PlacementArea::computePlanes()
{
    double xmin, ymin, zmin, xmax, ymax, zmax;
    getMetricMin(xmin, ymin, zmin);
    getMetricMax(xmax, ymax, zmax);

    octomap::point3d bbxMin(xmin, ymin, zmin);
    octomap::point3d bbxMax(xmax, ymax, zmax);

    computePlanes(bbxMin, bbxMax, false);
}

void PlacementArea::computePlanes(const octomap::point3d& bbxMin, const octomap::point3d& bbxMax, bool append)
{
    ARMARX_INFO << "Compute horizontal planes from octree";

    if (!enableVoxelPreprocessing)
    {
        filterVoxels(bbxMin, bbxMax);
    }

    if (!append)
    {
        this->planes.clear();
    }

    leaf_bbx_iterator it = this->begin_leafs_bbx(bbxMin, bbxMax);
    leaf_bbx_iterator end = this->end_leafs_bbx();

    /*
     * perform bfs for plane extraction on all occupied nodes in bounding box,
     * previously marked (i.e. visited) nodes are ignored
     */
    for (; it != end; ++it)
    {
        if (isNodeOccupied(*it) && !isNodeMarked(*it))
        {
            std::vector<Eigen::Vector3f> planePts;
            // perform bfs
            bfs(it.getKey(), planePts);

            auto planePtr = std::make_shared<Plane>(planePts, static_cast<float>(getResolution()));

            if (!isSmallPlane(planePtr))
            {
                this->planes.push_back(planePtr);
            }
        }
    }

    /*
     * unmark all leaf nodes for future queries
     */
    for (it = this->begin_leafs_bbx(bbxMin, bbxMax); it != end; ++it)
    {
        markNode(*it, false);
    }

    /*
     * adjust plane parameters (no op if parameters are invalid)
     */
    for (auto p : this->planes)
    {
        p->setThreshold(this->threshold);
        p->setKnn(4);
    }

    ARMARX_INFO << "Extracted " << this->planes.size() << " horizontal planes from octree";
}

const std::vector<PlanePtr>& PlacementArea::getPlanes() const
{
    return this->planes;
}

float PlacementArea::getMinSize() const
{
    return this->minSize;
}

size_t PlacementArea::getMinVoxelCount() const
{
    return this->minVoxelCount;
}

float PlacementArea::getThreshold() const
{
    return this->threshold;
}

void PlacementArea::bfs(const octomap::OcTreeKey& start, std::vector<Eigen::Vector3f>& plane)
{
    // assert start node is a leaf
    auto startNode = search(start);
    if (!startNode || nodeHasChildren(startNode))
    {
        return;
    }

    std::queue<octomap::OcTreeKey> q;

    plane.clear();

    q.push(start);
    startNode->mark(true);

    while (!q.empty())
    {
        auto key = q.front();
        q.pop();

        std::vector<octomap::OcTreeKey> neighbours;
        if (!nhood4(this, key, neighbours))
        {
            break;
        }

        /*
         * add neighbours to queue,
         * mark them as free to avoid double neighbours
         */
        for (auto nbr : neighbours)
        {
            auto node = search(nbr);
            if (node && !node->isMarked())
            {
                q.push(nbr);
                node->mark(true);
            }
        }

        plane.push_back(toVector3f(keyToCoord(key)));
    }
}

bool PlacementArea::setNodeOccupied(const octomap::OcTreeKey& key, bool occupied)
{
    auto node = search(key);
    return setNodeOccupied(node, occupied);
}

bool PlacementArea::setNodeOccupied(OcTreeNodeM* node, bool occupied)
{
    float logOdds = occupied ? std::numeric_limits<float>::max() : std::numeric_limits<float>::lowest();

    if (node)
    {
        node->setLogOdds(logOdds);
        return true;
    }
    else
    {
        return false;
    }
}

bool PlacementArea::removeSingleNode(const octomap::OcTreeKey& key, bool useNhood8)
{
    if (isSingleNode(this, key))
    {
        deleteNode(key);
        return true;
    }

    return false;
}

bool PlacementArea::isSmallPlane(const PlanePtr plane) const
{
    /*
     * filter single voxel planes
     */
    if (plane->getInputCloud()->size() < minVoxelCount)
    {
        return true;
    }

    const Eigen::Vector3f min = plane->getAABB().min();
    const Eigen::Vector3f max = plane->getAABB().max();

    /*
     * if one side length of AABB is smaller than minSize, remove plane
     */
    return max(0) - min(0) < minSize || max(1) - min(1) < minSize;
}

PlacementArea* PlacementArea::create() const
{
    return new PlacementArea(getResolution(),
                             this->minSize,
                             this->threshold,
                             this->enableVoxelPreprocessing,
                             this->enablePlanePreprocessing);
}

bool PlacementArea::isNodeMarked(const OcTreeNodeM& node) const
{
    return node.isMarked();
}

bool PlacementArea::isNodeMarked(OcTreeNodeM* node) const
{
    return node->isMarked();
}

void PlacementArea::markNode(OcTreeNodeM& node, const bool marked)
{
    node.mark(marked);
}

void PlacementArea::markNode(OcTreeNodeM* node, const bool marked)
{
    node->mark(marked);
}
