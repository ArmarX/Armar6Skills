/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vector>
#include <numeric>

#include <Eigen/Dense>

#include <pcl/kdtree/kdtree_flann.h>

namespace armarx
{
    using Point3 = pcl::PointXYZRGB;

    /**
     * @brief The Plane class provides a horizontal plane representation
     * extending a k-d-tree.
     *
     * The k-d-tree is initialized from a set of points representing voxel
     * centers of equal height in an equidistant grid.
     */
    class Plane : public pcl::KdTreeFLANN<Point3>
    {
    public:
        /**
         * @brief Constructor for the plane class.
         *
         * The plane is initialized from a vector of points representing
         * voxel centers of equal height in an equidistant grid.
         *
         * @param points Vector of points to initialize with
         * @param resolution Voxel resolution
         * @param threshold Threshold for float equality
         * @param knn Number of nearest neighbours to search
         */
        Plane(const std::vector<Eigen::Vector3f>& points,
              const float resolution,
              const float threshold = 1e-6,
              const int knn = 1);

        /**
         * @brief Checks whether the plane is empty
         * @return true if the plane is empty, false otherwise
         */
        bool isEmpty() const;

        /**
         * @brief Checks whether the plane contains a point
         *
         * This method performs a knn search and checks whether the point
         * resides within a plane voxel.
         *
         * @param point Point to check
         * @return true if the point resides within a plane voxel, false otherwise
         */
        bool contains(const Eigen::Vector3f& point) const;

        /**
         * @brief Getter for the voxel resolution
         * @return voxel resolution
         */
        float getResolution() const;

        /**
         * @brief Getter for the plane height
         * @return plane height
         */
        float getHeight() const;

        /**
         * @brief Setter for the plane height
         *
         * This sets the height of all points in the k-d-tree.
         *
         * @param height New plane height
         */
        void setHeight(float height);

        /**
         * @brief Getter for the float equality threshold
         * @return equality threshold
         */
        float getThreshold() const;

        /**
         * @brief Setter for the float equality threshold
         * @param threshold New equality threshold
         */
        void setThreshold(float threshold);

        /**
         * @brief Getter for the number of nearest neighbours
         * @return number of nearest neighbours
         */
        int getKnn() const;

        /**
         * @brief Setter for the number of nearest neighbours
         * @param knn New number of nearest neighbours
         */
        void setKnn(int knn);

        /**
         * @brief Getter for the AABB
         * @return the AABB of the plane
         */
        const Eigen::AlignedBox3f getAABB() const;

    protected:
        /**
         * @brief Initializes the k-d-tree from a vector of points.
         *
         * This method assumes that all input points represent voxel centers
         * of the same horizontal plane, i.e.
         * \f[
         *     \vert a - b\vert < threshold \forall a, b \in points
         * \f]
         *
         * @param points Vector of points representing the horizontal plane
         */
        void initialize(const std::vector<Eigen::Vector3f>& points);

        /**
         * @brief Computes the axis-aligned bounding box of the given set of points
         * @param points Vector of points to enclose
         */
        void computeAABB(const std::vector<Eigen::Vector3f>& points);

    private:
        float resolution;
        float height;
        float threshold;
        int knn;

        pcl::PointCloud<Point3>::Ptr pointCloud;
        Eigen::AlignedBox3f aabb;
    };

    using PlanePtr = std::shared_ptr<Plane>;
}
