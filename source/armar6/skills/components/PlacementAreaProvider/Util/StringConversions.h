/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <armar6/skills/components/PlacementAreaProvider/Util/TypeConversions.h>

namespace armarx
{
    inline std::string toString(PositionConstraint pos)
    {
        switch (pos)
        {
            case PositionConstraint::POS_EXACT:
                return "exact";
            case PositionConstraint::POS_REGION:
                return "2D region";
            case PositionConstraint::POS_ON_OBJECT:
                return "on object";
            case PositionConstraint::POS_ARBITRARY:
            default:
                return "arbitrary";
        }
    }

    inline PositionConstraint toPositionConstraint(std::string pos)
    {
        if (pos == "exact")
        {
            return PositionConstraint::POS_EXACT;
        }
        if (pos == "2D region")
        {
            return PositionConstraint::POS_REGION;
        }
        if (pos == "on object")
        {
            return PositionConstraint::POS_ON_OBJECT;
        }

        return PositionConstraint::POS_ARBITRARY;
    }

    inline std::vector<std::string> getPositionConstraintNames()
    {
        std::vector<std::string> names =
        {
            toString(PositionConstraint::POS_EXACT),
            toString(PositionConstraint::POS_REGION),
            toString(PositionConstraint::POS_ON_OBJECT),
            toString(PositionConstraint::POS_ARBITRARY)
        };

        return names;
    }

    inline std::string toString(OrientationConstraint quat)
    {
        switch (quat)
        {
            case OrientationConstraint::ROT_EXACT:
                return "exact";
            case OrientationConstraint::ROT_GOOD:
                return "on object";
            case OrientationConstraint::ROT_ARBITRARY:
            default:
                return "arbitrary";
        }
    }

    inline OrientationConstraint toOrientationConstraint(std::string quat)
    {
        if (quat == "exact")
        {
            return OrientationConstraint::ROT_EXACT;
        }
        if (quat == "good")
        {
            return OrientationConstraint::ROT_GOOD;
        }

        return OrientationConstraint::ROT_ARBITRARY;
    }

    inline std::vector<std::string> getOrientationConstraintNames()
    {
        std::vector<std::string> names =
        {
            toString(OrientationConstraint::ROT_EXACT),
            toString(OrientationConstraint::ROT_GOOD),
            toString(OrientationConstraint::ROT_ARBITRARY)
        };

        return names;
    }

    inline std::vector<std::string> getObjectNames(const objpose::ObjectPoseSeq& poses)
    {
        std::vector<std::string> names;

        for (auto pose : poses)
        {
            names.push_back(pose.objectID.str());
        }

        return names;
    }
}
