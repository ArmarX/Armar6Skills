/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/interface/serialization/Shapes.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>
#include <RobotAPI/libraries/core/Pose.h>

#include <SimoxUtility/math/convert.h>

#include <octomap/octomap.h>
#include <Eigen/Dense>

namespace armarx
{
    enum PositionConstraint
    {
        POS_EXACT,
        POS_REGION,
        POS_ON_OBJECT,
        POS_ARBITRARY
    };

    enum OrientationConstraint
    {
        ROT_EXACT,
        ROT_GOOD,
        ROT_ARBITRARY
    };

    inline Eigen::Vector3f toVector3f(const octomap::point3d& p)
    {
        return Eigen::Vector3f(p(0), p(1), p(2));
    }

    inline Eigen::Vector3f toVector3f(const Vector3BasePtr& p)
    {
        return Eigen::Vector3f(p->x, p->y, p->z);
    }

    inline Eigen::Vector3f toVector3f(const Vector3Base& p)
    {
        return Eigen::Vector3f(p.x, p.y, p.z);
    }

    inline octomap::point3d toPoint3d(const Eigen::Vector3f& p)
    {
        return octomap::point3d(p(0), p(1), p(2));
    }

    inline Eigen::Quaternionf toQuaternionf(const QuaternionBasePtr& q)
    {
        return Eigen::Quaternionf(q->qw, q->qx, q->qy, q->qz);
    }

    inline Eigen::Quaternionf toQuaternionf(const QuaternionBase& q)
    {
        return Eigen::Quaternionf(q.qw, q.qx, q.qy, q.qz);
    }

    inline Eigen::Vector3f quat2rpy(const QuaternionBasePtr& quaternion)
    {
        Eigen::Matrix3f rot = QuaternionPtr::dynamicCast(quaternion)->toEigen();
        return simox::math::mat3f_to_rpy(rot);
    }

    inline simox::XYConstrainedOrientedBoxf toXYConstrainedBox(const simox::OrientedBoxf& box)
    {
        /*
         * retreive yaw angle of box
         */
        Eigen::Matrix3f rot = box.transformation().block<3, 3>(0, 0);
        Eigen::Vector3f rpy = simox::math::mat3f_to_rpy(rot);

        /*
         * create new box with same dimensions, rotated by yaw angle
         */
        return simox::XYConstrainedOrientedBoxf(box.corner_min(), rpy[2], box.dimensions());
    }
}
