/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>

#include <octomap/octomap.h>

namespace armarx
{
    /**
     * @brief Checks whether a given octree node has an occupied top neighbour
     * @param tree OcTree to search
     * @param key Key of the node to check
     * @return true if the node has an occupied top neighbour, false otherwise
     */
    template<class NODE>
    inline bool hasTopNeighbour(octomap::OccupancyOcTreeBase<NODE>* tree, const octomap::OcTreeKey& key)
    {
        if (!tree->search(key))
        {
            return false;
        }

        auto topKey = key;
        topKey[2] += 1;

        auto node = tree->search(topKey);

        /*
         * check whether top neighbour of an occupied node exists and is occupied
         */
        return node && tree->isNodeOccupied(node);
    }

    /**
     * @brief Checks whether a given octree node has occupied neighbours (horizontally)
     * @param tree Octree to search
     * @param key Key of the node to check
     * @param useNhood8 Determines which neighbour relation to use
     * @return true if the node has an occupied neighbour, false otherwise
     */
    template<class NODE>
    inline bool isSingleNode(octomap::OccupancyOcTreeBase<NODE>* tree, const octomap::OcTreeKey& key, bool useNhood8 = false)
    {
        std::vector<octomap::OcTreeKey> neighbours;
        if (useNhood8)
        {
            nhood8(tree, key, neighbours);
        }
        else
        {
            nhood4(tree, key, neighbours);
        }

        /*
         * check whether node has no direct neighbours in its horizontal plane
         */
        return neighbours.empty();
    }

    /**
     * @brief Computes the 4-neighbourhood of an OcTreeNode consisting of back,
     * front, left and right neighbours.
     *
     * Neighbours are added if they exist in known space and if they are occupied.
     *
     * @param tree OcTree to search
     * @param key OcTreeKey of the node to compute the neighbourhood for
     * @param neighbours Resulting vector of neighbours, may be empty
     * @return false if key points to a node in unknown space, true otherwise
     */
    template<class NODE>
    inline bool nhood4(octomap::OccupancyOcTreeBase<NODE>* tree, const octomap::OcTreeKey& key, std::vector<octomap::OcTreeKey>& neighbours)
    {
        if (!tree->search(key))
        {
            return false;
        }

        // back, front, left and right neighbour keys
        std::vector<octomap::OcTreeKey> keys
        {
            octomap::OcTreeKey(key[0] - 1, key[1], key[2]),
            octomap::OcTreeKey(key[0] + 1, key[1], key[2]),
            octomap::OcTreeKey(key[0], key[1] - 1, key[2]),
            octomap::OcTreeKey(key[0], key[1] + 1, key[2])
        };

        for (auto k : keys)
        {
            // add occupied neighbour
            auto node = tree->search(k);
            if (node && tree->isNodeOccupied(node))
            {
                neighbours.push_back(k);
            }
        }

        return true;
    }

    /**
     * @brief Computes the 8-neighbourhood of an OcTreeNode by adding corner nodes
     * to the 4-neighbourhood.
     *
     * Neighbours are added if they exist in known space and if they are occupied.
     *
     * @see nhood4
     *
     * @param tree OcTree to search
     * @param key OcTreeKey of the node to compute the neighbourhood for
     * @param neighbours Resulting vector of neighbours, may be empty
     * @return false if key points to a node in unknown space, true otherwise
     */
    template<class NODE>
    inline bool nhood8(octomap::OccupancyOcTreeBase<NODE>* tree, const octomap::OcTreeKey& key, std::vector<octomap::OcTreeKey>& neighbours)
    {
        if (!nhood4(tree, key, neighbours))
        {
            return false;
        }

        // middle corner neighbour keys
        std::vector<octomap::OcTreeKey> keys
        {
            octomap::OcTreeKey(key[0] - 1, key[1] - 1, key[2]),
            octomap::OcTreeKey(key[0] - 1, key[1] + 1, key[2]),
            octomap::OcTreeKey(key[0] + 1, key[1] - 1, key[2]),
            octomap::OcTreeKey(key[0] + 1, key[1] + 1, key[2]),
        };

        for (auto k : keys)
        {
            // add occupied neighbour
            auto node = tree->search(k);
            if (node && tree->isNodeOccupied(node))
            {
                neighbours.push_back(k);
            }
        }

        return true;
    }

    /**
     * @brief Computes the neighbourhood of an OcTreeNode consisting of all 26
     * neighbours.
     *
     * The 8-neighbourhood is complemented by the 8-neighbourhoods of the bottom
     * and top neighbours, if they exist.
     *
     * Neighbours are added if they exist in known space and if they are occupied.
     *
     * @see nhood4
     * @see nhood8
     *
     * @param tree OcTree to search
     * @param key OcTreeKey of the node to compute the neighbourhood for
     * @param neighbours Resulting vector of neighbours, may be empty
     * @return false if key points to a node in unknown space, true otherwise
     */
    template<class NODE>
    inline bool nhood(octomap::OccupancyOcTreeBase<NODE>* tree, const octomap::OcTreeKey& key, std::vector<octomap::OcTreeKey>& neighbours)
    {
        if (!nhood8(tree, key, neighbours))
        {
            return false;
        }

        // search bottom and top neighbours
        std::vector<octomap::OcTreeKey> keys
        {
            octomap::OcTreeKey(key[0], key[1], key[2] - 1),
            octomap::OcTreeKey(key[0], key[1], key[2] + 1),
        };

        for (auto k : keys)
        {
            // add occupied neighbour
            auto node = tree->search(k);
            if (node && tree->isNodeOccupied(node))
            {
                neighbours.push_back(k);

                // add bottom/top neighbourhood
                nhood8(tree, k, neighbours);
            }
        }

        return true;
    }
}
