/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/logging/Logging.h>

#include <armar6/skills/components/PlacementAreaProvider/DataStructures/PlacementArea.h>
#include <octomap/OcTree.h>

namespace armarx
{
    inline bool octreeToPlacementArea(const std::string& a, const std::string& b)
    {
        ARMARX_INFO << "Converting octree into placement area";

        octomap::OcTree* tree = new octomap::OcTree(10.0);

        std::ifstream ifs(a.c_str(), std::ios_base::binary);
        if (!ifs.is_open())
        {
            ARMARX_WARNING << "Error, could not open input file stream";
            return false;
        }

        ARMARX_INFO << "Reading octree from binary file";
        if (!tree->readBinary(a))
        {
            ARMARX_WARNING << "Error, could not read octree from binary";
            return false;
        }

        ARMARX_INFO << "Octree has " << tree->size() << " nodes";

        PlacementArea* placement = new PlacementArea(
            tree->getResolution(),
            tree->getResolution(),
            2,
            1e-6,
            false,
            false);

        if (placement->size() > 0)
        {
            placement->clear();
        }

        octomap::OcTree::leaf_iterator it = tree->begin_leafs();
        octomap::OcTree::leaf_iterator end = tree->end_leafs();

        ARMARX_INFO << "Adding leaf nodes to placement area";
        for (; it != end; ++it)
        {
            if (tree->isNodeOccupied(*it))
            {
                placement->updateNode(it.getCoordinate(), true);
            }
        }

        std::ofstream ofs(b.c_str(), std::ios_base::binary);
        if (!ofs.is_open())
        {
            ARMARX_WARNING << "Error, could not open output file stream";
            return false;
        }

        ARMARX_INFO << "Writing placement area to binary file";
        if (!placement->writeBinary(ofs))
        {
            ARMARX_WARNING << "Error, could not write placement are to binary";
            return false;
        }

        return true;
    }
}
