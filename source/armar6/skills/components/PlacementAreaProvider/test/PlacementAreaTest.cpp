/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE armar6_skills::ArmarXObjects::PlacementArea

#define ARMARX_BOOST_TEST
#define BOOST_TEST_TOOLS_UNDER_DEBUGGER

#include <armar6/skills/Test.h>
#include <armar6/skills/components/PlacementAreaProvider/DataStructures/PlacementArea.h>

#include <iostream>
#include <fstream>

using namespace armarx;

const std::string FILE_PATH = "/home/max/Placement/PlacementAreaTest.ot";

/**
 * @brief Creates an equidistant voxel grid representing a four-legged table.
 * @param corner Reference corner of the tabletop
 * @param resolution Distance between adjacent voxels along the x-y-z axes
 * @param width Number of voxels representing the width
 * @param length Number of voxels representing the length
 * @param height Number of voxels representing the height
 * @param points Resulting vector of points
 */
void table(const octomap::point3d& corner,
           const double resolution,
           const size_t width,
           const size_t length,
           const size_t height,
           std::vector<octomap::point3d>& points)
{
    const size_t n = width * length + 4 * (height - 1);
    points.resize(n);

    /*
     * create tabletop
     */
    for (size_t idx = 0; idx < width * length; ++idx)
    {
        const double i = static_cast<double>(idx % width) + 0.5;
        const double j = static_cast<double>(idx / width) + 0.5;

        points[idx] = corner + octomap::point3d(i * resolution, j * resolution, 0.0);
    }

    /*
     * create table legs downwards from tabletop corners
     */
    for (size_t idx = width * length; idx < n; idx += 4)
    {
        const double height = static_cast<double>((idx - width * length) / 4) + 1.0;
        const octomap::point3d offset(0.0, 0.0, - height * resolution);

        points[idx] = points[0] + offset;
        points[idx + 1] = points[width - 1] + offset;
        points[idx + 2] = points[width * (length - 1)] + offset;
        points[idx + 3] = points[width * length - 1] + offset;
    }
}

struct Setup
{
    Setup() : tree(nullptr), width(4), length(8), height(5), resolution(10.0), filePath(FILE_PATH)
    {
        BOOST_TEST_MESSAGE("setup octree fixture");

        tree = std::make_shared<PlacementArea>(resolution, 2.0 * resolution, 1e6, false, false);

        const octomap::point3d corner(0.0, 0.0, (static_cast<double>(height) - 0.5) * resolution);

        /*
         * 4x8 tabletop, height 5
         */
        table(corner, resolution, width, length, height, points);

        /*
         * insert occupied table voxels into octree
         */
        for (auto point : points)
        {
            tree->updateNode(point, true);
        }

        /*
         * write octree to binary file
         */
        std::ofstream file(filePath.c_str(), std::ios_base::binary);

        if (file.is_open())
        {
            BOOST_TEST_MESSAGE("write octree to file");
            tree->writeBinary(filePath);
        }
    }

    PlacementAreaPtr tree;
    size_t width;
    size_t length;
    size_t height;
    double resolution;
    std::string filePath;

    std::vector<octomap::point3d> points;
};

BOOST_FIXTURE_TEST_SUITE(placementAreaSuite, Setup)

BOOST_AUTO_TEST_CASE(removeSingleNodeLegsTest)
{
    PlacementAreaPtr placementArea = std::make_shared<PlacementArea>(filePath, 2.0 * resolution, 1e-6, false, false);

    const size_t n = width * length + 4 * (height - 1);

    for (size_t idx = width * length; idx < n; ++idx)
    {
        octomap::OcTreeKey key;
        BOOST_CHECK(placementArea->coordToKeyChecked(points[idx], key));

        /*
         * table legs have no horizontal neighbours + too small
         */
        BOOST_CHECK_EQUAL(placementArea->removeSingleNode(key, false), true);

        /*
         * node deleted during removal
         */
        auto node = placementArea->search(points[idx]);
        BOOST_CHECK(!node);
    }
}

BOOST_AUTO_TEST_CASE(removeSingleNodeTopTest)
{
    PlacementAreaPtr placementArea = std::make_shared<PlacementArea>(filePath, 2.0 * resolution, 1e-6, false, false);

    for (size_t idx = 0; idx < width * length; ++idx)
    {
        octomap::OcTreeKey key;
        BOOST_CHECK(placementArea->coordToKeyChecked(points[idx], key));

        /*
         * table legs have no horizontal neighbours + too small
         */
        BOOST_CHECK_EQUAL(placementArea->removeSingleNode(key, false), false);

        /*
         * check whether node is still occupied
         */
        auto node = placementArea->search(points[idx]);
        BOOST_CHECK(node);
        BOOST_CHECK(placementArea->isNodeOccupied(node));
    }
}

BOOST_AUTO_TEST_CASE(voxelFilterVerticalTest)
{
    PlacementAreaPtr placementArea = std::make_shared<PlacementArea>(filePath, 2.0 * resolution, 1e-6, false, false);

    const size_t n = width * length + 4 * (height - 1);
    size_t idx;

    /*
     * sanity check: table nodes are leafs
     */
    for (idx = 0; idx < n; ++idx)
    {
        auto node = placementArea->search(points[idx]);
        BOOST_CHECK(node);
        BOOST_CHECK_EQUAL(placementArea->nodeHasChildren(node), false);
    }

    placementArea->filterVoxels();

    /*
     * no tabletop nodes have been removed
     */
    for (idx = 0; idx < width * length; ++idx)
    {
        auto node = placementArea->search(points[idx]);
        BOOST_CHECK(node);
        BOOST_CHECK_EQUAL(placementArea->isNodeOccupied(node), true);
    }

    /*
     * all table leg nodes have been removed
     */
    for (idx = width * length; idx < n; ++idx)
    {
        auto node = placementArea->search(points[idx]);
        BOOST_CHECK(!node);
    }
}

BOOST_AUTO_TEST_SUITE_END()
