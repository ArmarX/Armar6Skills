/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE armar6_skills::ArmarXObjects::PlacementAreaProvider

#define ARMARX_BOOST_TEST

#include <armar6/skills/Test.h>
#include <armar6/skills/components/PlacementAreaProvider/PlacementAreaProvider.h>

#include <iostream>

BOOST_AUTO_TEST_CASE(transformTest)
{
    armarx::PlacementAreaProviderPtr provider;

    /*
     * todo:
     * test correct creation of new simox::XYConstrainedBoxf from
     * translated corner coordinates and yaw angle
     */
    BOOST_CHECK_EQUAL(true, true);
}

BOOST_AUTO_TEST_CASE(fitsOnPlaneTest)
{
    armarx::PlacementAreaProviderPtr provider;

    /*
     * todo:
     * test correct contains-relation for simox::XYConstrainedBoxf on Plane
     */
    BOOST_CHECK_EQUAL(true, true);
}

BOOST_AUTO_TEST_CASE(computeAABBTest)
{
    armarx::PlacementAreaProviderPtr provider;

    /*
     * todo:
     * test correct AABB computation of simox::XYConstrainedBoxf
     */
    BOOST_CHECK_EQUAL(true, true);
}
