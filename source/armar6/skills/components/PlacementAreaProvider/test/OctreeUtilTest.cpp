/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE armar6_skills::ArmarXObjects::OctreeUtil

#define ARMARX_BOOST_TEST

#include <armar6/skills/Test.h>
#include <armar6/skills/components/PlacementAreaProvider/Util/OctreeUtil.h>
#include <armar6/skills/components/PlacementAreaProvider/DataStructures/PlacementArea.h>

#include <iostream>

using namespace armarx;

/**
 * @brief Creates an equidistant voxel grid representing a cuboid of size (width x length x height).
 * @param resolution Distance between adjacent points along the x-y-z axes
 * @param width Number of voxels representing the width
 * @param length Number of voxels representing the length
 * @param height Number of voxels representing the height
 * @param points Resulting vector of points
 */
void cuboid(const double resolution,
            const size_t width,
            const size_t length,
            const size_t height,
            std::vector<octomap::point3d>& points)
{
    points.resize(width * length * height);

    for (size_t idx = 0; idx < width * length * height; ++idx)
    {
        const double i = static_cast<double>(idx % width) + 0.5;
        const double j = static_cast<double>((idx / width) % length) + 0.5;
        const double k = static_cast<double>((idx / width) / length) + 0.5;

        points[idx] = octomap::point3d(i * resolution, j * resolution, k * resolution);
    }
}

struct Setup
{
    Setup() : tree(nullptr), width(3), length(3), height(3), resolution(10.0)
    {
        BOOST_TEST_MESSAGE("setup octree fixture");

        tree = new PlacementArea(resolution, resolution, 1e6, false, false);

        /*
         * insert occupied nodes into octree
         */
        cuboid(resolution, width, length, height, points);

        for (auto point : points)
        {
            tree->updateNode(point, true);
        }
    }

    ~Setup()
    {
        BOOST_TEST_MESSAGE("teardown octree fixture");

        delete tree;
    }

    PlacementArea* tree;
    size_t width;
    size_t length;
    size_t height;
    double resolution;

    std::vector<octomap::point3d> points;
};

BOOST_FIXTURE_TEST_SUITE(cubeOctreeTest, Setup)

BOOST_AUTO_TEST_CASE(hasTopNeighbourTest)
{
    for (size_t i = 0; i < points.size(); ++i)
    {
        auto key = tree->coordToKey(points[i]);

        /*
         * all voxels but the top 3x3 grid have an occupied top neighbour
         */
        if (i < width * length * (height - 1))
        {
            BOOST_CHECK_EQUAL(hasTopNeighbour(tree, key), true);
        }
        else
        {
            BOOST_CHECK_EQUAL(hasTopNeighbour(tree, key), false);
        }
    }
}

BOOST_AUTO_TEST_CASE(nhood4OutsideTest)
{
    /*
     * point outside of 3x3x3 cube
     */
    octomap::OcTreeKey point = tree->coordToKey(-0.5 * resolution, -0.5 * resolution, -0.5 * resolution);
    std::vector<octomap::OcTreeKey> nbrs;

    /*
     * point outside of known space
     */
    BOOST_CHECK_EQUAL(nhood(tree, point, nbrs), false);
}

BOOST_AUTO_TEST_CASE(nhood4BottomCenterTest)
{
    /*
     * bottom 3x3 grid center point
     */
    octomap::OcTreeKey center = tree->coordToKey(1.5 * resolution, 1.5 * resolution, 0.5 * resolution);
    std::vector<octomap::OcTreeKey> nbrs;

    /*
     * center within octree
     */
    BOOST_CHECK_EQUAL(nhood4(tree, center, nbrs), true);

    /*
     * assert size and occupied neighbours
     */
    BOOST_CHECK_EQUAL(nbrs.size(), 4);

    for (auto nbr : nbrs)
    {
        BOOST_CHECK_EQUAL(tree->isNodeOccupied(tree->search(nbr)), true);
    }
}

BOOST_AUTO_TEST_CASE(nhood4MiddleCenterTest)
{
    /*
     * middle 3x3 grid center point
     */
    octomap::OcTreeKey center = tree->coordToKey(1.5 * resolution, 1.5 * resolution, 1.5 * resolution);
    std::vector<octomap::OcTreeKey> nbrs;

    /*
     * center within octree
     */
    BOOST_CHECK_EQUAL(nhood4(tree, center, nbrs), true);

    /*
     * assert size and occupied neighbours
     */
    BOOST_CHECK_EQUAL(nbrs.size(), 4);

    for (auto nbr : nbrs)
    {
        BOOST_CHECK_EQUAL(tree->isNodeOccupied(tree->search(nbr)), true);
    }
}

BOOST_AUTO_TEST_CASE(nhood4TopCenterTest)
{
    /*
     * top 3x3 grid center point
     */
    octomap::OcTreeKey center = tree->coordToKey(1.5 * resolution, 1.5 * resolution, 2.5 * resolution);
    std::vector<octomap::OcTreeKey> nbrs;

    /*
     * center within octree
     */
    BOOST_CHECK_EQUAL(nhood4(tree, center, nbrs), true);

    /*
     * assert size and occupied neighbours
     */
    BOOST_CHECK_EQUAL(nbrs.size(), 4);

    for (auto nbr : nbrs)
    {
        BOOST_CHECK_EQUAL(tree->isNodeOccupied(tree->search(nbr)), true);
    }
}

BOOST_AUTO_TEST_CASE(nhood4CornerTest)
{
    /*
     * bottom 3x3 grid corner
     */
    octomap::OcTreeKey corner = tree->coordToKey(0.5 * resolution, 0.5 * resolution, 0.5 * resolution);
    std::vector<octomap::OcTreeKey> nbrs;

    /*
     * corner within octree
     */
    BOOST_CHECK_EQUAL(nhood4(tree, corner, nbrs), true);

    /*
     * assert size
     */
    BOOST_CHECK_EQUAL(nbrs.size(), 2);
}

BOOST_AUTO_TEST_CASE(nhood4EdgeCenterTest)
{
    /*
     * bottom 3x3 grid edge center point
     */
    octomap::OcTreeKey center = tree->coordToKey(1.5 * resolution, 0.5 * resolution, 0.5 * resolution);
    std::vector<octomap::OcTreeKey> nbrs;

    /*
     * center within octree
     */
    BOOST_CHECK_EQUAL(nhood4(tree, center, nbrs), true);

    /*
     * assert size and occupied neighbours
     */
    BOOST_CHECK_EQUAL(nbrs.size(), 3);

    for (auto nbr : nbrs)
    {
        BOOST_CHECK_EQUAL(tree->isNodeOccupied(tree->search(nbr)), true);
    }
}

BOOST_AUTO_TEST_CASE(nhood8BottomCenterTest)
{
    /*
     * bottom 3x3 grid center point
     */
    octomap::OcTreeKey center = tree->coordToKey(1.5 * resolution, 1.5 * resolution, 0.5 * resolution);
    std::vector<octomap::OcTreeKey> nbrs;

    /*
     * center within octree
     */
    BOOST_CHECK_EQUAL(nhood8(tree, center, nbrs), true);

    /*
     * assert size and occupied neighbours
     */
    BOOST_CHECK_EQUAL(nbrs.size(), 8);

    for (auto nbr : nbrs)
    {
        BOOST_CHECK_EQUAL(tree->isNodeOccupied(tree->search(nbr)), true);
    }
}
BOOST_AUTO_TEST_CASE(nhood8MiddleCenterTest)
{
    /*
     * middle 3x3 grid center point
     */
    octomap::OcTreeKey center = tree->coordToKey(1.5 * resolution, 1.5 * resolution, 1.5 * resolution);
    std::vector<octomap::OcTreeKey> nbrs;

    /*
     * center within octree
     */
    BOOST_CHECK_EQUAL(nhood8(tree, center, nbrs), true);

    /*
     * assert size and occupied neighbours
     */
    BOOST_CHECK_EQUAL(nbrs.size(), 8);

    for (auto nbr : nbrs)
    {
        BOOST_CHECK_EQUAL(tree->isNodeOccupied(tree->search(nbr)), true);
    }
}
BOOST_AUTO_TEST_CASE(nhood8TopCenterTest)
{
    /*
     * top 3x3 grid center point
     */
    octomap::OcTreeKey center = tree->coordToKey(1.5 * resolution, 1.5 * resolution, 2.5 * resolution);
    std::vector<octomap::OcTreeKey> nbrs;

    /*
     * center within octree
     */
    BOOST_CHECK_EQUAL(nhood8(tree, center, nbrs), true);

    /*
     * assert size and occupied neighbours
     */
    BOOST_CHECK_EQUAL(nbrs.size(), 8);

    for (auto nbr : nbrs)
    {
        BOOST_CHECK_EQUAL(tree->isNodeOccupied(tree->search(nbr)), true);
    }
}

BOOST_AUTO_TEST_CASE(nhood8CornerTest)
{
    /*
     * bottom 3x3 grid corner
     */
    octomap::OcTreeKey corner = tree->coordToKey(0.5 * resolution, 0.5 * resolution, 0.5 * resolution);
    std::vector<octomap::OcTreeKey> nbrs;

    /*
     * corner within octree
     */
    BOOST_CHECK_EQUAL(nhood8(tree, corner, nbrs), true);

    /*
     * assert size
     */
    BOOST_CHECK_EQUAL(nbrs.size(), 3);
}

BOOST_AUTO_TEST_CASE(nhood8EdgeCenterTest)
{
    /*
     * bottom 3x3 grid edge center
     */
    octomap::OcTreeKey center = tree->coordToKey(1.5 * resolution, 0.5 * resolution, 0.5 * resolution);
    std::vector<octomap::OcTreeKey> nbrs;

    /*
     * center within octree
     */
    BOOST_CHECK_EQUAL(nhood8(tree, center, nbrs), true);

    /*
     * assert size
     */
    BOOST_CHECK_EQUAL(nbrs.size(), 5);
}

BOOST_AUTO_TEST_CASE(nhoodCenterTest)
{
    /*
     * middle 3x3 grid center point
     */
    octomap::OcTreeKey center = tree->coordToKey(1.5 * resolution, 1.5 * resolution, 1.5 * resolution);
    std::vector<octomap::OcTreeKey> nbrs;

    /*
     * center within octree
     */
    BOOST_CHECK_EQUAL(nhood(tree, center, nbrs), true);

    /*
     * assert size and occupied neighbours
     */
    BOOST_CHECK_EQUAL(nbrs.size(), 26);

    for (auto nbr : nbrs)
    {
        BOOST_CHECK_EQUAL(tree->isNodeOccupied(tree->search(nbr)), true);
    }
}

BOOST_AUTO_TEST_CASE(nhoodBottomCornerTest)
{
    /*
     * bottom 3x3 grid corner
     */
    octomap::OcTreeKey corner = tree->coordToKey(0.5 * resolution, 0.5 * resolution, 0.5 * resolution);
    std::vector<octomap::OcTreeKey> nbrs;

    /*
     * corner within octree
     */
    BOOST_CHECK_EQUAL(nhood(tree, corner, nbrs), true);

    /*
     * assert size
     */
    BOOST_CHECK_EQUAL(nbrs.size(), 7);
}

BOOST_AUTO_TEST_CASE(nhoodMiddleCornerTest)
{
    /*
     * middle 3x3 grid corner
     */
    octomap::OcTreeKey corner = tree->coordToKey(0.5 * resolution, 0.5 * resolution, 1.5 * resolution);
    std::vector<octomap::OcTreeKey> nbrs;

    /*
     * corner within octree
     */
    BOOST_CHECK_EQUAL(nhood(tree, corner, nbrs), true);

    /*
     * assert size
     */
    BOOST_CHECK_EQUAL(nbrs.size(), 11);
}

BOOST_AUTO_TEST_CASE(nhoodBottomEdgeCenterTest)
{
    /*
     * bottom 3x3 grid edge center
     */
    octomap::OcTreeKey center = tree->coordToKey(1.5 * resolution, 0.5 * resolution, 0.5 * resolution);
    std::vector<octomap::OcTreeKey> nbrs;

    /*
     * corner within octree
     */
    BOOST_CHECK_EQUAL(nhood(tree, center, nbrs), true);

    /*
     * assert size
     */
    BOOST_CHECK_EQUAL(nbrs.size(), 11);
}

BOOST_AUTO_TEST_CASE(nhoodMiddleEdgeCenterTest)
{
    /*
     * middle 3x3 grid edge center
     */
    octomap::OcTreeKey center = tree->coordToKey(1.5 * resolution, 0.5 * resolution, 1.5 * resolution);
    std::vector<octomap::OcTreeKey> nbrs;

    /*
     * corner within octree
     */
    BOOST_CHECK_EQUAL(nhood(tree, center, nbrs), true);

    /*
     * assert size
     */
    BOOST_CHECK_EQUAL(nbrs.size(), 17);
}

BOOST_AUTO_TEST_SUITE_END()
