/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE armar6_skills::ArmarXObjects::TypeConversions

#define ARMARX_BOOST_TEST

#include <armar6/skills/Test.h>
#include <armar6/skills/components/PlacementAreaProvider/Util/TypeConversions.h>

#include <iostream>

BOOST_AUTO_TEST_CASE(toXYConstrainedBoxTest)
{
    /*
     * default 1x1x1 box
     */
    simox::OrientedBoxf box;
    float yaw = M_PI_2f32;

    /*
     * rotate by pi/2 around z-axis
     */
    //    Eigen::Matrix3f m;
    //    m = Eigen::AngleAxisf(yaw, Eigen::Vector3f::UnitZ()):
    //    simox::OrientedBoxf transformedBox = box.transformed(m);

    //    simox::XYConstrainedOrientedBoxf xyBox = armarx::toXYConstrainedBox(transformedBox);

    simox::XYConstrainedOrientedBoxf xyBox = armarx::toXYConstrainedBox(box);

    /*
     * check
     * - corner_min
     * - dimensions
     * - yaw angle
     */
    BOOST_CHECK_CLOSE(box.corner_min()(0), xyBox.corner_min()(0), 0.001);
    BOOST_CHECK_CLOSE(box.corner_min()(1), xyBox.corner_min()(1), 0.001);
    BOOST_CHECK_CLOSE(box.corner_min()(2), xyBox.corner_min()(2), 0.001);

    BOOST_CHECK_CLOSE(box.dimension(0), xyBox.dimension(0), 0.001);
    BOOST_CHECK_CLOSE(box.dimension(1), xyBox.dimension(1), 0.001);
    BOOST_CHECK_CLOSE(box.dimension(2), xyBox.dimension(2), 0.001);

    Eigen::Matrix3f xyRot = box.transformation().block<3, 3>(0, 0);
    Eigen::Vector3f rpy = xyRot.eulerAngles(0, 1, 2);

    //BOOST_CHECK_CLOSE(yaw, rpy(2), 0.001);
}
