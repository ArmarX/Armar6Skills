/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE armar6_skills::ArmarXObjects::OctomapConversions

#define ARMARX_BOOST_TEST

#include <armar6/skills/Test.h>
#include <armar6/skills/components/PlacementAreaProvider/Util/OctomapConversions.h>

#include <iostream>

using namespace armarx;

const std::string FILE_PATH = "/home/max/repos/armar6_skills/data/armar6_skills/binary_occupancy_map";
//const std::string FILE_PATH = "/home/max/Placement/ConversionTest";

/**
* @brief Creates an equidistant voxel grid representing a cuboid of size (width x length x height).
* @param resolution Distance between adjacent points along the x-y-z axes
* @param width Number of voxels representing the width
* @param length Number of voxels representing the length
* @param height Number of voxels representing the height
* @param points Resulting vector of points
*/
void cuboid(const double resolution,
            const size_t width,
            const size_t length,
            const size_t height,
            std::vector<octomap::point3d>& points)
{
    points.resize(width * length * height);

    for (size_t idx = 0; idx < width * length * height; ++idx)
    {
        const double i = static_cast<double>(idx % width) + 0.5;
        const double j = static_cast<double>((idx / width) % length) + 0.5;
        const double k = static_cast<double>((idx / width) / length) + 0.5;

        points[idx] = octomap::point3d(i * resolution, j * resolution, k * resolution);
    }
}

struct Setup
{
    Setup() : tree(nullptr), width(3), length(3), height(3), resolution(10.0)
    {
        BOOST_TEST_MESSAGE("setup octree fixture");

        tree = new octomap::OcTree(resolution);

        /*
         * insert occupied nodes into octree
         */
        cuboid(resolution, width, length, height, points);

        for (auto point : points)
        {
            tree->updateNode(point, true);
        }
    }

    ~Setup()
    {
        BOOST_TEST_MESSAGE("teardown octree fixture");

        delete tree;
    }

    octomap::OcTree* tree;
    size_t width;
    size_t length;
    size_t height;
    double resolution;

    std::vector<octomap::point3d> points;
};

BOOST_FIXTURE_TEST_SUITE(cubeConversionTest, Setup)

BOOST_AUTO_TEST_CASE(conversionTest)
{
    //    std::string path = FILE_PATH + ".occ";
    //    std::ofstream ofs(path.c_str(), std::ios_base::binary);

    //    BOOST_TEST_MESSAGE("writing octree to file");

    //    BOOST_CHECK(ofs.is_open());
    //    BOOST_CHECK(tree->writeBinary(ofs));

    BOOST_TEST_MESSAGE("converting octree to placement area");

    BOOST_CHECK(octreeToPlacementArea(FILE_PATH + ".occ", FILE_PATH + ".occb"));
}

BOOST_AUTO_TEST_SUITE_END()
