/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE armar6_skills::ArmarXObjects::Plane

#define ARMARX_BOOST_TEST

#include <armar6/skills/Test.h>
#include <armar6/skills/components/PlacementAreaProvider/DataStructures/Plane.h>

#include <iostream>

BOOST_AUTO_TEST_CASE(initEmptyTest)
{
    std::vector<Eigen::Vector3f> points;

    armarx::Plane plane(points, 0.f);

    BOOST_CHECK_CLOSE(plane.getResolution(), 0.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getHeight(), 0.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getThreshold(), 1e-6, 0.001);
    BOOST_CHECK_EQUAL(plane.getKnn(), 1);

    BOOST_CHECK(plane.isEmpty());
}

BOOST_AUTO_TEST_CASE(initializerTest)
{
    std::vector<Eigen::Vector3f> points;

    armarx::Plane plane(points, 10.f, 0.001f, 4);

    BOOST_CHECK_CLOSE(plane.getResolution(), 10.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getThreshold(), 0.001f, 0.001);
    BOOST_CHECK_EQUAL(plane.getKnn(), 4);
}

BOOST_AUTO_TEST_CASE(initSinglePointTest)
{
    std::vector<Eigen::Vector3f> points = { Eigen::Vector3f(5.f, 5.f, 15.f) };

    armarx::Plane plane(points, 10.f);

    BOOST_CHECK_CLOSE(plane.getResolution(), 10.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getHeight(), 20.f, 0.001);

    BOOST_REQUIRE_EQUAL(plane.getInputCloud()->empty(), false);
    BOOST_CHECK_EQUAL(plane.getInputCloud()->height, 1);
    BOOST_CHECK_EQUAL(plane.getInputCloud()->width, 1);

    /*
     * check updated input point cloud
     */
    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[0].x, 5.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[0].y, 5.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[0].z, 20.f, 0.001);
}

BOOST_AUTO_TEST_CASE(init2x2PlaneTest)
{
    /*
     * |
     * | (5,15) (15,15)
     * |
     * | (5, 5) (15, 5)
     * |
     * 0------------------
     */
    std::vector<Eigen::Vector3f> points =
    {
        Eigen::Vector3f(5.f, 5.f, 15.f), Eigen::Vector3f(15.f, 5.f, 15.f),
        Eigen::Vector3f(5.f, 15.f, 15.f), Eigen::Vector3f(15.f, 15.f, 15.f)
    };

    armarx::Plane plane(points, 10);

    BOOST_CHECK_CLOSE(plane.getResolution(), 10.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getHeight(), 20.f, 0.001);

    BOOST_REQUIRE(!plane.getInputCloud()->empty());
    BOOST_REQUIRE_EQUAL(plane.getInputCloud()->points.size(), 4);
    BOOST_CHECK_EQUAL(plane.getInputCloud()->height, 1);
    BOOST_CHECK_EQUAL(plane.getInputCloud()->width, 4);

    /*
     * check updated input point cloud
     */
    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[0].x, 5.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[0].y, 5.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[0].z, 20.f, 0.001);

    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[1].x, 15.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[1].y, 5.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[1].z, 20.f, 0.001);

    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[2].x, 5.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[2].y, 15.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[2].z, 20.f, 0.001);

    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[3].x, 15.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[3].y, 15.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[3].z, 20.f, 0.001);
}

BOOST_AUTO_TEST_CASE(setHeightSinglePointTest)
{
    std::vector<Eigen::Vector3f> points = { Eigen::Vector3f(5.f, 5.f, 15.f) };

    armarx::Plane plane(points, 10.f);

    BOOST_CHECK_CLOSE(plane.getHeight(), 20.f, 0.001);

    plane.setHeight(30.f);

    /*
     * check updated input point cloud
     */
    BOOST_CHECK_CLOSE(plane.getHeight(), 30.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[0].z, 30.f, 0.001);
}

BOOST_AUTO_TEST_CASE(setHeight2x2PlaneTest)
{
    /*
     * |
     * | (5,15) (15,15)
     * |
     * | (5, 5) (15, 5)
     * |
     * 0------------------
     */
    std::vector<Eigen::Vector3f> points =
    {
        Eigen::Vector3f(5.f, 5.f, 15.f), Eigen::Vector3f(15.f, 5.f, 15.f),
        Eigen::Vector3f(5.f, 15.f, 15.f), Eigen::Vector3f(15.f, 15.f, 15.f)
    };

    armarx::Plane plane(points, 10);

    BOOST_CHECK_CLOSE(plane.getHeight(), 20.f, 0.001);

    plane.setHeight(30.f);

    /*
     * check point update
     */
    BOOST_CHECK_CLOSE(plane.getHeight(), 30.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[0].z, 30.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[1].z, 30.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[2].z, 30.f, 0.001);
    BOOST_CHECK_CLOSE(plane.getInputCloud()->points[3].z, 30.f, 0.001);
}

BOOST_AUTO_TEST_CASE(setThresholdTest)
{
    std::vector<Eigen::Vector3f> points = { Eigen::Vector3f(5.f, 5.f, 15.f) };

    armarx::Plane plane(points, 10.f);

    BOOST_CHECK_CLOSE(plane.getThreshold(), 1e-6, 0.001);


    /*
     * expect threshold update
     */
    plane.setThreshold(0.001f);
    BOOST_CHECK_CLOSE(plane.getThreshold(), 0.001f, 0.001);

    /*
     * expect no update
     */
    plane.setThreshold(-1e6);
    BOOST_CHECK_CLOSE(plane.getThreshold(), 0.001f, 0.001);
}

BOOST_AUTO_TEST_CASE(setKnnTest)
{
    /*
     * |
     * | (5,15) (15,15)
     * |
     * | (5, 5) (15, 5)
     * |
     * 0------------------
     */
    std::vector<Eigen::Vector3f> points =
    {
        Eigen::Vector3f(5.f, 5.f, 15.f), Eigen::Vector3f(15.f, 5.f, 15.f),
        Eigen::Vector3f(5.f, 15.f, 15.f), Eigen::Vector3f(15.f, 15.f, 15.f)
    };

    armarx::Plane plane(points, 10.f);

    BOOST_CHECK_EQUAL(plane.getKnn(), 1);

    /*
     * expect knn update
     */
    plane.setKnn(4);
    BOOST_CHECK_EQUAL(plane.getKnn(), 4);

    /*
     * expect no update
     */
    plane.setKnn(-2);
    BOOST_CHECK_EQUAL(plane.getKnn(), 4);

    /*
     * expect no update
     */
    plane.setKnn(6);
    BOOST_CHECK_EQUAL(plane.getKnn(), 4);
}

BOOST_AUTO_TEST_CASE(containsSinglePointTest)
{
    std::vector<Eigen::Vector3f> points = { Eigen::Vector3f(5.f, 5.f, 15.f) };

    armarx::Plane plane(points, 10.f);

    /*
     * point exactly on plane
     */
    BOOST_CHECK_EQUAL(plane.contains(Eigen::Vector3f(5.f, 5.f, 20.f)), true);

    /*
     * vertical difference -> not on plane
     */
    BOOST_CHECK_EQUAL(plane.contains(Eigen::Vector3f(5.f, 5.f, 15.f)), false);

    /*
     * point on voxel corner -> within voxel
     */
    BOOST_CHECK_EQUAL(plane.contains(Eigen::Vector3f(10.f, 10.f, 20.f)), true);

    /*
     * point outside of plane
     */
    BOOST_CHECK_EQUAL(plane.contains(Eigen::Vector3f(15.f, 15.f, 20.f)), false);
}

BOOST_AUTO_TEST_CASE(contains2x2PlaneTest)
{
    /*
     * |
     * | (5,15) (15,15)
     * |
     * | (5, 5) (15, 5)
     * |
     * 0------------------
     */
    std::vector<Eigen::Vector3f> points =
    {
        Eigen::Vector3f(5.f, 5.f, 15.f), Eigen::Vector3f(15.f, 5.f, 15.f),
        Eigen::Vector3f(5.f, 15.f, 15.f), Eigen::Vector3f(15.f, 15.f, 15.f)
    };

    armarx::PlanePtr plane = std::make_shared<armarx::Plane>(points, 10.f);
    plane->setKnn(4);

    /*
     * points exactly plane points (sanity check)
     */
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(5.f, 5.f, 20.f)), true);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(15.f, 5.f, 20.f)), true);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(5.f, 15.f, 20.f)), true);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(15.f, 15.f, 20.f)), true);


    /*
     * points on voxel corners -> on plane
     */
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(0.f, 0.f, 20.f)), true);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(10.f, 0.f, 20.f)), true);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(20.f, 0.f, 20.f)), true);

    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(0.f, 10.f, 20.f)), true);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(10.f, 10.f, 20.f)), true);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(20.f, 10.f, 20.f)), true);

    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(0.f, 20.f, 20.f)), true);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(10.f, 20.f, 20.f)), true);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(20.f, 20.f, 20.f)), true);

    /*
     * vertical difference -> not on plane
     */
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(5.f, 5.f, 19.f)), false);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(15.f, 5.f, 19.f)), false);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(5.f, 15.f, 19.f)), false);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(15.f, 15.f, 19.f)), false);

    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(5.f, 5.f, 21.f)), false);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(15.f, 5.f, 21.f)), false);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(5.f, 15.f, 21.f)), false);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(15.f, 15.f, 21.f)), false);

    /*
     * points outside of plane
     */
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(-5.f, 10.f, 20.f)), false);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(25.f, 10.f, 20.f)), false);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(10.f, 25.f, 20.f)), false);
    BOOST_CHECK_EQUAL(plane->contains(Eigen::Vector3f(10.f, -5.f, 20.f)), false);
}
