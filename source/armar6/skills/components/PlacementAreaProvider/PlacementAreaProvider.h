/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <iostream>
#include <fstream>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/RemoteGuiComponentPlugin.h>

#include <armar6/skills/interface/PlacementAreaProviderInterface.h>

#include <armar6/skills/components/PlacementAreaProvider/DataStructures/PlacementArea.h>
#include <armar6/skills/components/PlacementAreaProvider/RemoteGui/RemoteGuiTab.h>
#include <armar6/skills/components/PlacementAreaProvider/Sampling/Sampling.h>
#include <armar6/skills/components/PlacementAreaProvider/Util/StringConversions.h>

#include <armar6/skills/components/PlacementAreaProvider/Visualization/Visualization.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

/*
 * testing
 */
#include <armar6/skills/components/PlacementAreaProvider/Util/OctomapConversions.h>

#include <ActiveVision/interface/OccupancyMappingInterface.h>

namespace armarx
{
    /**
     * @class PlacementAreaProviderPropertyDefinitions
     * @brief Property definitions of `PlacementAreaProvider`.
     */
    class PlacementAreaProviderPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        PlacementAreaProviderPropertyDefinitions(std::string prefix) :
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>
            (
                "PlacementAreaTopicName",
                "PlacementAreas",
                "Name of the offered topic."
            );
            defineOptionalProperty<std::string>
            (
                "SceneFilePath",
                "$HOME/repos/armar6_skills/data",
                "File path of the voxel grid representing the input scene."
            );
            defineOptionalProperty<float>
            (
                "Resolution",
                10.f,
                "Resolution of octree voxels at max depth."
            );
            defineOptionalProperty<float>
            (
                "MinPlaneSize",
                200.f,
                "Minimum dimensions [mm] of suitable planar surfaces."
            );
            defineOptionalProperty<float>
            (
                "MinPlaneHeight",
                700.f,
                "Minimum height [mm] of suitable planar surfaces."
            );
            defineOptionalProperty<float>
            (
                "MaxPlaneHeight",
                1500.f,
                "Maximum height [mm] of suitable planar surfaces."
            );
            defineOptionalProperty<size_t>
            (
                "MinVoxelCount",
                2,
                "Minimum quantity of voxels per planar surface."
            );
            defineOptionalProperty<float>
            (
                "ErrorThreshold",
                1e-6,
                "Error threshold for floating point equality comparisons."
            );
            defineOptionalProperty<float>
            (
                "SafetyMargin",
                500.f,
                "Safety margin [mm] for plane extraction from 2D regions."
            );
            defineOptionalProperty<size_t>
            (
                "NumSamples",
                100,
                "Number of samples used for the generation of placement areas."
            );
            defineOptionalProperty<bool>
            (
                "EnableVoxelPreprocessing",
                true,
                "Enables preprocessing of all occupied voxels to remove unfit voxels from the octree."
            );
            defineOptionalProperty<bool>
            (
                "EnablePlanePreprocessing",
                true,
                "Enables preprocessing of all occupied voxels to identify suitable planar surfaces."
            );
            defineOptionalProperty<bool>
            (
                "EnableSampleVisualization",
                true,
                "Enables visualization of sampled placement areas."
            );
            defineOptionalProperty<bool>
            (
                "EnablePlaneVisualization",
                true,
                "Enables visualization of planar surfaces extracted from occupancy map."
            );
            defineOptionalProperty<bool>
            (
                "EnableOctreeVisualization",
                true,
                "Enables visualization of occupancy map representing the scene."
            );
            defineOptionalProperty<std::string>
            (
                "VisuLayerName",
                "placement",
                "Name of the ArViz layer on which the occupancy map and samples are visualized."
            );
            defineOptionalProperty<std::string>
            (
                "DebugDrawerTopicName",
                "DebugDrawerUpdates",
                "Name of the Debug Drawer Topic offered."
            );
            defineOptionalProperty<std::string>
            (
                "OccupancyMappingName",
                "OccupancyMapping",
                "Name of the occupancy mapping provider."
            );
        }
    };

    /**
     * @defgroup Component-PlacementAreaProvider PlacementAreaProvider
     * @ingroup armar6_skills-Components
     * Given an octree representation of the surface voxels of a scene,
     * the PlacementAreaProvider component provides query-based extraction of
     * planar surfaces where an object may be placed.
     *
     * @class PlacementAreaProvider
     * @ingroup Component-PlacementAreaProvider
     * @brief The PlacementAreaProvider class provides interfaces for the extraction
     * of placement areas from a given scene.
     *
     * The PlacementAreaProvider class controls the extraction of planar surfaces
     * from a specialized octree data structure and handles queries for valid
     * placement areas.
     */
    class PlacementAreaProvider :
        virtual public armarx::Component,
        virtual public armarx::RemoteGuiComponentPluginUser,
        virtual public armarx::ArVizComponentPluginUser,
        virtual public armarx::ObjectPoseClientPluginUser,
        virtual public armarx::PlacementAreaProviderInterface
    {
    public:

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override;

        /**
         * @brief Computes a vector of oriented boxes representing valid placement areas for a given object.
         *
         * This method samples all planar surfaces within known space to find valid placement areas.
         *
         * @param objPose Pose of the object
         * @param placementAreas Vector of oriented boxes representing placement areas
         * @param c @see Ice::Current
         */
        void computeArea(const objpose::data::ObjectPose& objPose,
                         simox::XYConstrainedOrientedBoxfSeq& placementAreas,
                         const Ice::Current& c = Ice::emptyCurrent) const override;

        /**
         * @brief Computes a vector of oriented boxes representing valid placement areas for a given object.
         *
         * Given a specific position, this method checks whether a planar surface exists at that
         * location. In that case, different orientations are sampled in order to find valid
         * placement areas for the object.
         *
         * @param objPose Pose of the object
         * @param targetPosition Query position for the placement area
         * @param placementAreas Vector of oriented boxes representing placement areas
         * @param c @see Ice::Current
         */
        void computeAreaPosition(const objpose::data::ObjectPose& objPose,
                                 const Vector3BasePtr& targetPosition,
                                 simox::XYConstrainedOrientedBoxfSeq& placementAreas,
                                 const Ice::Current& c = Ice::emptyCurrent) const override;

        /**
         * @brief Computes a vector of oriented boxes representing valid placement areas for a given object.
         *
         * Given a specific orientation, this method samples all planar surfaces within known space
         * to find valid placement areas.
         *
         * @param objPose Pose of the object
         * @param targetOrientation Query orientation for the placement area
         * @param placementAreas Vector of oriented boxes representing placement areas
         * @param c @see Ice::Current
         */
        void computeAreaOrientation(const objpose::data::ObjectPose& objPose,
                                    const QuaternionBasePtr& targetOrientation,
                                    simox::XYConstrainedOrientedBoxfSeq& placementAreas,
                                    const Ice::Current& c = Ice::emptyCurrent) const override;

        /**
         * @brief Computes a vector of oriented boxes representing valid placement areas for a given object.
         *
         * Given a specific object pose, this method checks whether a planar surface exists at that
         * location. In that case, the method checks whether the object can be placed using the
         * given orientation.
         *
         * @param objPose Pose of the object
         * @param targetPose Query pose for the placement area
         * @param placementAreas Vector of oriented boxes representing placement areas
         * @param c @see Ice::Current
         */
        void computeAreaPose(const objpose::data::ObjectPose& objPose,
                             const PoseBasePtr& targetPose,
                             simox::XYConstrainedOrientedBoxfSeq& placementAreas,
                             const Ice::Current& c = Ice::emptyCurrent) const override;

        /**
         * @brief Computes a vector of oriented boxes representing valid placement areas for a given object.
         *
         * Given a 2D region, this method samples all planar surfaces close to the region to find
         * valid placement areas for the object.
         *
         * @param objPose Pose of the object
         * @param region Oriented box representing the query region
         * @param placementAreas Vector of oriented boxes representing placement areas
         * @param c @see Ice::Current
         */
        void computeAreaRegion(const objpose::data::ObjectPose& objPose,
                               const simox::XYConstrainedOrientedBoxf& region,
                               simox::XYConstrainedOrientedBoxfSeq& placementAreas,
                               const Ice::Current& c = Ice::emptyCurrent) const override;

        /**
         * @brief Computes a vector of oriented boxes representing valid placement areas for a given object.
         *
         * Given a 2D region and orientation, this method samples all planar surfaces close to the region
         * to find valid placement areas for the object. The method then checks whether the object can be
         * placed using the given orientation.
         *
         * @param objPose Pose of the object
         * @param region Oriented box representing the query region
         * @param targetOrientation Query orientation for the placement area
         * @param placementAreas Vector of oriented boxes representing placement areas
         * @param c @see Ice::Current
         */
        void computeAreaRegionOrientation(const objpose::data::ObjectPose& objPose,
                                          const simox::XYConstrainedOrientedBoxf& region,
                                          const QuaternionBasePtr& targetOrientation,
                                          simox::XYConstrainedOrientedBoxfSeq& placementAreas,
                                          const Ice::Current& c = Ice::emptyCurrent) const override;
    protected:

        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /**
         * @brief Selects the correct use case for given constraints and executes the corresponging method.
         *
         * Constraints and input data are read from a RemoteGuiDataset.
         * The correct use case is then executed for a given input object.
         * The target object only needs to be specified if the corresponding constraint is selected.
         *
         * @param input Object to compute suitable placement areas for
         * @param target Optional target object to place the input object on top of
         */
        void computePlacementArea(const objpose::ObjectPose& input, const objpose::ObjectPose& target);

        /**
         * @brief Computes the oriented box representing the OOBB of the given object.
         * @param objPose Input object
         * @return the OOBB of the input object as oriented box
         */
        simox::OrientedBoxf determineOrientedBox(const objpose::ObjectPose& objPose) const;

        /**
         * @brief Retreives the correct object given an object ID as string.
         * @param name Object ID
         * @param objPose Corresponding object
         * @return true if the object was found, false otherwise
         */
        bool determineObjectPose(const std::string& name, objpose::ObjectPose& objPose) const;

        /**
         * @brief Transforms the oriented box given a target position and a yaw angle.
         * @param box Oriented box to transform
         * @param targetPosition Target position of the oriented box
         * @param yaw Yaw angle to rotate with
         * @return the resulting oriented box
         */
        simox::XYConstrainedOrientedBoxf transform(const simox::XYConstrainedOrientedBoxf& box,
                const Eigen::Vector3f& targetPosition,
                const float yaw) const;

        /**
         * @brief Checks whether the projection of a given OOBB fits completely inside a given plane.
         * @param box Oriented box to check
         * @param plane Plane to check against
         * @return true if the box fits inside the plane, false otherwise
         */
        bool fitsOnPlane(const simox::XYConstrainedOrientedBoxf& box, const PlanePtr& plane) const;

        /**
         * @brief Checks whether the projection of a given OOBB fits completely inside the AABB of a given plane.
         *
         * Provides a rough estimate whether an object might fit on top of a
         * given plane by comparing their bounding boxes.s
         *
         * @see armarx::PlacementAreaProvider::fitsOnPlane()
         *
         * @param box Oriented box to check
         * @param plane Plane to check against
         * @return true if the box fits inside the plane, false otherwise
         */
        bool fitsInsideAABB(const simox::XYConstrainedOrientedBoxf& box, const PlanePtr& plane) const;

        /**
         * @brief Computes the AABB of a given oriented box
         * @param box Given oriented box
         * @return the resulting AABB
         */
        Eigen::AlignedBox3f computeAABB(const simox::XYConstrainedOrientedBoxf& box) const;

        /**
         * @brief Constructs and returns the default object which is available for placement queries.
         * @return the default object
         */
        objpose::ObjectPose getDefaultObject() const;

        /**
         * @brief Updates a given octomap::OcTree with a given set of occupied voxels.
         * @param tree Octree to update
         * @param centers Vector of points representing occupied voxel centers
         */
        void constructOctree(octomap::OcTree* tree, std::vector<Vector3BasePtr>& centers) const;

    private:
        RemoteGui::WidgetPtr createRemoteGui();

        void processRemoteGui(RemoteGui::TabProxy& prx);

    private:
        std::string sceneFilePath;
        float threshold;
        float minSize;
        float minHeight;
        float maxHeight;
        size_t minVoxelCount;
        float margin;
        size_t numSamples;

        bool enablePlanePreprocessing;
        bool enableVoxelPreprocessing;

        bool enableSampleVisu;
        bool enablePlaneVisu;
        bool enableOctreeVisu;
        std::string layerName;

        armarx::PlacementAreaTopicInterfacePrx listenerPrx;
        OccupancyMappingInterfacePrx map;

        std::vector<objpose::ObjectPose> objPoses;
        std::vector<simox::XYConstrainedOrientedBoxf> result;

        PlacementAreaPtr placement;
        SamplingPtr sampling;
        placement::VisualizationPtr visu;
        placement::RemoteGuiTabPtr tab;

        DebugDrawerInterfacePrx debugDrawer;

        mutable std::mutex mutex;
    };

    using PlacementAreaProviderPtr = std::shared_ptr<PlacementAreaProvider>;
}
