/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <armar6/skills/components/PlacementAreaProvider/PlacementAreaProvider.h>

using namespace armarx;

std::string PlacementAreaProvider::getDefaultName() const
{
    return "PlacementAreaProvider";
}

void PlacementAreaProvider::computeArea(const objpose::data::ObjectPose& objPose,
                                        simox::XYConstrainedOrientedBoxfSeq& placementAreas,
                                        const Ice::Current& c) const
{
    std::scoped_lock<std::mutex> lock(mutex);

    simox::OrientedBoxf oobb = determineOrientedBox(objPose);
    simox::XYConstrainedOrientedBoxf xyOB = toXYConstrainedBox(oobb);

    /*
     * extract planar surfaces from scene
     */
    if (!enablePlanePreprocessing)
    {
        placement->computePlanes();
    }

    std::vector<PlanePtr> planes;
    for (PlanePtr plane : placement->getPlanes())
    {
        /*
         * filter planes too small for this object
         */
        if (fitsInsideAABB(xyOB, plane))
        {
            planes.push_back(plane);
        }
    }

    /*
     * distribute number of samples over suitable planes
     */
    std::vector<size_t> distribution;
    Sampling::distributeSamples(planes, numSamples, distribution);

    ARMARX_INFO << "Sampling on " << std::to_string(planes.size()) << " planar regions";

    for (size_t k = 0; k < planes.size(); ++k)
    {
        ARMARX_INFO << deactivateSpam(0.5f) << "Sampling positions and yaw angles on plane";

        sampling->setNumSamples(distribution[k]);

        std::vector<Eigen::Vector3f> sample;
        sampling->sampleRegion(planes[k], sample);

        std::vector<float> yawSample;
        sampling->sampleYawAngle(yawSample);

        ARMARX_INFO << deactivateSpam(0.5f) << "Checking placement areas resulting from samples";

        /*
         * for each pair of targetPostion and yawAngle:
         * - translate object OB to targetPosition
         * - rotate object OB by yawAngle
         * - check if resulting OB fits inside plane
         */
        for (size_t i = 0; i < sampling->getNumSamples(); ++i)
        {
            simox::XYConstrainedOrientedBoxf rotOOBB = transform(xyOB, sample[i], yawSample[i]);

            if (fitsOnPlane(rotOOBB, planes[k]))
            {
                placementAreas.push_back(rotOOBB);
            }
        }
    }
}

void PlacementAreaProvider::computeAreaPosition(const objpose::data::ObjectPose& objPose,
        const Vector3BasePtr& targetPosition,
        simox::XYConstrainedOrientedBoxfSeq& placementAreas,
        const Ice::Current& c) const
{
    std::scoped_lock<std::mutex> lock(mutex);

    simox::OrientedBoxf oobb = determineOrientedBox(objPose);
    simox::XYConstrainedOrientedBoxf xyOB = toXYConstrainedBox(oobb);
    Eigen::Vector3f target = toVector3f(targetPosition);

    /*
     * extract planar surfaces from scene
     */
    if (!enablePlanePreprocessing)
    {
        double x, y, zmin, zmax;
        placement->getMetricMin(x, y, zmin);
        placement->getMetricMax(x, y, zmax);

        octomap::point3d bbxMin(target(0) - margin, target(1) - margin, this->minHeight);
        octomap::point3d bbxMax(target(0) + margin, target(1) + margin, this->maxHeight);

        placement->computePlanes(bbxMin, bbxMax);
    }

    for (PlanePtr plane : placement->getPlanes())
    {
        if (!plane->contains(target))
        {
            continue;
        }

        ARMARX_INFO << deactivateSpam(0.5f) << "Sampling yaw angles";

        std::vector<float> yawSamples;
        sampling->sampleYawAngle(yawSamples);

        ARMARX_INFO << deactivateSpam(0.5f) << "Checking placement areas resulting from samples";

        /*
         * for each rotation angle:
         * - translate object OB to target
         * - rotate object OB by yaw
         * - check if resulting OB fits inside plane
         */
        for (float yaw : yawSamples)
        {
            simox::XYConstrainedOrientedBoxf rotOOBB = transform(xyOB, target, yaw);

            if (fitsOnPlane(rotOOBB, plane))
            {
                placementAreas.push_back(rotOOBB);
            }
        }
    }
}

void PlacementAreaProvider::computeAreaOrientation(const objpose::data::ObjectPose& objPose,
        const QuaternionBasePtr& targetOrientation,
        simox::XYConstrainedOrientedBoxfSeq& placementAreas,
        const Ice::Current& c) const
{
    std::scoped_lock<std::mutex> lock(mutex);

    simox::OrientedBoxf oobb = determineOrientedBox(objPose);
    simox::XYConstrainedOrientedBoxf xyOB = toXYConstrainedBox(oobb);
    Eigen::Vector3f rpy = quat2rpy(targetOrientation);

    /*
     * extract planar surfaces from scene
     */
    if (!enablePlanePreprocessing)
    {
        placement->computePlanes();
    }

    std::vector<PlanePtr> planes;
    for (PlanePtr plane : placement->getPlanes())
    {
        /*
         * filter planes too small for this object
         */
        if (fitsInsideAABB(xyOB, plane))
        {
            planes.push_back(plane);
        }
    }

    /*
     * distribute number of samples over suitable planes
     */
    std::vector<size_t> distribution;
    Sampling::distributeSamples(planes, numSamples, distribution);

    ARMARX_INFO << "Sampling on " << std::to_string(planes.size()) << " planar regions";

    for (size_t k = 0; k < planes.size(); ++k)
    {
        ARMARX_INFO << deactivateSpam(0.5f) << "Sampling positions on plane";

        sampling->setNumSamples(distribution[k]);

        std::vector<Eigen::Vector3f> sample;
        sampling->sampleRegion(planes[k], sample);

        ARMARX_INFO << deactivateSpam(0.5f) << "Checking placement areas resulting from samples";

        /*
         * for each targetPostion:
         * - translate object OB to targetPosition
         * - rotate object OB by yawAngle
         * - check if resulting OB fits inside plane
         */
        for (Eigen::Vector3f pos : sample)
        {
            simox::XYConstrainedOrientedBoxf rotOOBB = transform(xyOB, pos, rpy(2));

            if (fitsOnPlane(rotOOBB, planes[k]))
            {
                placementAreas.push_back(rotOOBB);
            }
        }
    }
}

void PlacementAreaProvider::computeAreaPose(const objpose::data::ObjectPose& objPose,
        const PoseBasePtr& targetPose,
        simox::XYConstrainedOrientedBoxfSeq& placementAreas,
        const Ice::Current& c) const
{
    std::scoped_lock<std::mutex> lock(mutex);

    simox::OrientedBoxf oobb = determineOrientedBox(objPose);
    simox::XYConstrainedOrientedBoxf xyOB = toXYConstrainedBox(oobb);

    Eigen::Vector3f target = toVector3f(targetPose->position);
    Eigen::Vector3f rpy = quat2rpy(targetPose->orientation);

    /*
     * extract planar surfaces from scene
     */
    if (!enablePlanePreprocessing)
    {
        double x, y, zmin, zmax;
        placement->getMetricMin(x, y, zmin);
        placement->getMetricMax(x, y, zmax);

        octomap::point3d bbxMin(target(0) - margin, target(1) - margin, this->minHeight);
        octomap::point3d bbxMax(target(0) + margin, target(1) + margin, this->maxHeight);

        placement->computePlanes(bbxMin, bbxMax);
    }

    for (PlanePtr plane : placement->getPlanes())
    {
        if (!plane->contains(target))
        {
            continue;
        }

        ARMARX_INFO << deactivateSpam(0.5f) << "Checking placement area on plane";

        /*
         * - translate object OB to target
         * - rotate object OB by yaw
         * - check if resulting OB fits inside plane
         */
        simox::XYConstrainedOrientedBoxf rotOOBB = transform(xyOB, target, rpy(2));

        if (fitsOnPlane(rotOOBB, plane))
        {
            placementAreas.push_back(rotOOBB);
        }
    }
}

void PlacementAreaProvider::computeAreaRegion(const objpose::data::ObjectPose& objPose,
        const simox::XYConstrainedOrientedBoxf& region,
        simox::XYConstrainedOrientedBoxfSeq& placementAreas,
        const Ice::Current& c) const
{
    std::scoped_lock<std::mutex> lock(mutex);

    simox::OrientedBoxf oobb = determineOrientedBox(objPose);
    simox::XYConstrainedOrientedBoxf xyOB = toXYConstrainedBox(oobb);

    /*
     * extract planar surfaces from scene
     */
    if (!enablePlanePreprocessing)
    {
        Eigen::AlignedBox3f aabb = computeAABB(region);

        placement->computePlanes(toPoint3d(aabb.min()), toPoint3d(aabb.max()));
    }

    std::vector<PlanePtr> planes;
    for (PlanePtr plane : placement->getPlanes())
    {
        /*
         * filter planes too small for this object
         */
        if (fitsInsideAABB(xyOB, plane))
        {
            planes.push_back(plane);
        }
    }

    /*
     * distribute number of samples over suitable planes
     */
    std::vector<size_t> distribution;
    Sampling::distributeSamples(planes, numSamples, distribution);

    ARMARX_INFO << "Sampling on " << std::to_string(planes.size()) << " planar regions";

    for (size_t k = 0; k < planes.size(); ++k)
    {
        ARMARX_INFO << deactivateSpam(0.5f) << "Sampling positions in region and yaw angles";

        sampling->setNumSamples(distribution[k]);

        std::vector<Eigen::Vector3f> sample;
        sampling->sampleRegion(region, planes[k]->getHeight(), sample);

        std::vector<float> yawSamples;
        sampling->sampleYawAngle(yawSamples);

        ARMARX_INFO << deactivateSpam(0.5f) << "Checking placement areas resulting from samples";

        /*
         * for each pair of targetPostion and yawAngle:
         * - translate object OB to targetPosition
         * - rotate object OB by yawAngle
         * - check if resulting OB fits inside plane
         */
        for (size_t i = 0; i < sampling->getNumSamples(); ++i)
        {
            if (!planes[k]->contains(sample[i]))
            {
                continue;
            }

            simox::XYConstrainedOrientedBoxf rotOOBB = transform(xyOB, sample[i], yawSamples[i]);

            if (fitsOnPlane(rotOOBB, planes[k]))
            {
                placementAreas.push_back(rotOOBB);
            }
        }
    }
}

void PlacementAreaProvider::computeAreaRegionOrientation(const objpose::data::ObjectPose& objPose,
        const simox::XYConstrainedOrientedBoxf& region,
        const QuaternionBasePtr& targetOrientation,
        simox::XYConstrainedOrientedBoxfSeq& placementAreas,
        const Ice::Current& c) const
{
    std::scoped_lock<std::mutex> lock(mutex);

    simox::OrientedBoxf oobb = determineOrientedBox(objPose);
    simox::XYConstrainedOrientedBoxf xyOB = toXYConstrainedBox(oobb);

    Eigen::Vector3f rpy = quat2rpy(targetOrientation);

    /*
     * extract planar surfaces from scene
     */
    if (!enablePlanePreprocessing)
    {
        Eigen::AlignedBox3f aabb = computeAABB(region);

        placement->computePlanes(toPoint3d(aabb.min()), toPoint3d(aabb.max()));
    }

    std::vector<PlanePtr> planes;
    for (PlanePtr plane : placement->getPlanes())
    {
        /*
         * filter planes too small for this object
         */
        if (fitsInsideAABB(xyOB, plane))
        {
            planes.push_back(plane);
        }
    }

    /*
     * distribute number of samples over suitable planes
     */
    std::vector<size_t> distribution;
    Sampling::distributeSamples(planes, numSamples, distribution);

    ARMARX_INFO << "Sampling on " << std::to_string(planes.size()) << " planar regions";

    for (size_t k = 0; k < planes.size(); ++k)
    {
        ARMARX_INFO << deactivateSpam(0.5f) << "Sampling positions in region";

        sampling->setNumSamples(distribution[k]);

        std::vector<Eigen::Vector3f> sample;
        sampling->sampleRegion(region, planes[k]->getHeight(), sample);

        ARMARX_INFO << deactivateSpam(0.5f) << "Checking placement areas resulting from samples";

        /*
         * for each pair of targetPostion and yawAngle:
         * - translate object OB to targetPosition
         * - rotate object OB by yawAngle
         * - check if resulting OB fits inside plane
         */
        for (Eigen::Vector3f pos : sample)
        {
            if (!planes[k]->contains(pos))
            {
                continue;
            }

            simox::XYConstrainedOrientedBoxf rotOOBB = transform(xyOB, pos, rpy(2));

            if (fitsOnPlane(rotOOBB, planes[k]))
            {
                placementAreas.push_back(rotOOBB);
            }
        }
    }
}

void PlacementAreaProvider::onInitComponent()
{
    // Register offered topices and used proxies here.
    // Using a proxy will cause the component to wait until the proxy is available.
    // usingProxyFromProperty("MyProxyName");

    this->sceneFilePath = getProperty<std::string>("SceneFilePath").getValue();

    ArmarXDataPath::getAbsolutePath(sceneFilePath, sceneFilePath);

    this->threshold = getProperty<float>("ErrorThreshold").getValue();
    this->minSize = getProperty<float>("MinPlaneSize").getValue();
    this->minHeight = getProperty<float>("MinPlaneHeight").getValue();
    this->maxHeight = getProperty<float>("MaxPlaneHeight").getValue();
    this->minVoxelCount = getProperty<size_t>("MinVoxelCount").getValue();
    this->margin = getProperty<float>("SafetyMargin").getValue();
    this->numSamples = getProperty<size_t>("NumSamples").getValue();

    this->enablePlanePreprocessing = getProperty<bool>("EnablePlanePreprocessing").getValue();
    this->enableVoxelPreprocessing = getProperty<bool>("EnableVoxelPreprocessing").getValue();

    this->enableSampleVisu = getProperty<bool>("EnableSampleVisualization").getValue();
    this->enablePlaneVisu = getProperty<bool>("EnablePlaneVisualization").getValue();
    this->enableOctreeVisu = getProperty<bool>("EnableOctreeVisualization").getValue();
    this->layerName = getProperty<std::string>("VisuLayerName").getValue();

    offeringTopicFromProperty("PlacementAreaTopicName");
    offeringTopicFromProperty("DebugDrawerTopicName");

    usingProxyFromProperty("OccupancyMappingName");
}

void PlacementAreaProvider::onConnectComponent()
{
    getTopicFromProperty(listenerPrx, "PlacementAreaTopicName");
    getProxyFromProperty(map, "OccupancyMappingName");

    /*
     * 1. construct octree from occupancy mapping
     * 2. initialize placement area from octree
     */
    std::vector<Vector3BasePtr> centers;
    double resolution;
    this->map->getOccupiedLeafCenters(centers, resolution);

    octomap::OcTree* tree = new octomap::OcTree(resolution);
    constructOctree(tree, centers);

    placement = std::make_shared<PlacementArea>(
                    tree,
                    this->minSize,
                    this->minHeight,
                    this->maxHeight,
                    this->minVoxelCount,
                    this->threshold,
                    this->enableVoxelPreprocessing,
                    this->enablePlanePreprocessing);

    delete tree;

    sampling = std::make_shared<Sampling>(this->numSamples);
    tab = std::make_shared<placement::RemoteGuiTab>();

    visu = std::make_shared<placement::Visualization>(this->layerName, this->arviz);
    visu->setSampleVisu(this->enableSampleVisu);
    visu->setPlaneVisu(this->enablePlaneVisu);
    visu->setOctreeVisu(this->enableOctreeVisu);

    debugDrawer = getTopicFromProperty<DebugDrawerInterfacePrx>("DebugDrawerTopicName");
    visu->setDebugDrawer(this->debugDrawer);

    createOrUpdateRemoteGuiTab(createRemoteGui(), [this](RemoteGui::TabProxy & prx)
    {
        processRemoteGui(prx);
    });

    /*
     * initial visualization of (possibly preprocessed) octree
     * and preprocessed planar surfaces
     */
    visu->visualizeOctree(this->placement);

    if (this->enablePlanePreprocessing)
    {
        visu->visualizePlanes(this->placement->getPlanes());
    }
}

void PlacementAreaProvider::onDisconnectComponent()
{

}

void PlacementAreaProvider::onExitComponent()
{
    visu->clearLayer();
}

PropertyDefinitionsPtr PlacementAreaProvider::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new PlacementAreaProviderPropertyDefinitions(
            getConfigIdentifier()));
}

simox::OrientedBoxf PlacementAreaProvider::determineOrientedBox(const objpose::ObjectPose& objPose) const
{
    Eigen::Matrix4f globalPose = objPose.objectPoseGlobal;

    // ..todo:: check if localOOBB is already computed
    return objPose.localOOBB->transformed(globalPose);
}

bool PlacementAreaProvider::determineObjectPose(const std::string& name, objpose::ObjectPose& objPose) const
{
    /*
     * retreive object pose from name
     */
    for (auto obj : objPoses)
    {
        if (obj.objectID.str() == name)
        {
            objPose = obj;
            return true;
        }
    }

    return false;
}

simox::XYConstrainedOrientedBoxf PlacementAreaProvider::transform(const simox::XYConstrainedOrientedBoxf& box,
        const Eigen::Vector3f& targetPosition,
        const float yaw) const
{
    Eigen::Vector3f translation;
    translation = targetPosition - box.center();
    translation(2) = targetPosition(2) - box.corner_min()(2);

    /*
     * create new box, translated to target and rotated by angle yaw
     */
    return simox::XYConstrainedOrientedBoxf(box.corner_min() + translation, yaw, box.dimensions());
}

bool PlacementAreaProvider::fitsOnPlane(const simox::XYConstrainedOrientedBoxf& box, const PlanePtr& plane) const
{
    /*
     * determine box corners
     */
    Eigen::Vector3f bottomLeft = box.corner_min();
    Eigen::Vector3f bottomRight = box.corner_min()
                                  + box.axis(0) * box.dimension(0);
    Eigen::Vector3f topLeft = box.corner_min()
                              + box.axis(1) * box.dimension(1);
    Eigen::Vector3f topRight = box.corner_min()
                               + box.axis(0) * box.dimension(0)
                               + box.axis(1) * box.dimension(1);

    /*
     * box fits on plane if all four bottom corners are contained in plane
     */
    return plane->contains(bottomLeft)
           && plane->contains(bottomRight)
           && plane->contains(topLeft)
           && plane->contains(topRight);
}

bool PlacementAreaProvider::fitsInsideAABB(const simox::XYConstrainedOrientedBoxf& box, const PlanePtr& plane) const
{
    Eigen::Vector3f min = box.corner_min();
    Eigen::Vector3f max = box.corner_max();
    /*
     * height 0
     */
    min(2) = 0;
    max(2) = 0;

    Eigen::AlignedBox3f aabb(min, max);

    /*
     * translate box bottom left onto plane bottom left
     */
    aabb.translate(plane->getAABB().min() - min);

    return plane->getAABB().contains(aabb);
}

Eigen::AlignedBox3f PlacementAreaProvider::computeAABB(const simox::XYConstrainedOrientedBoxf& box) const
{
    Eigen::Vector3f min = box.corner_min();
    Eigen::Vector3f max = box.corner_max();

    double x, y, zmin, zmax;
    placement->getMetricMin(x, y, zmin);
    placement->getMetricMax(x, y, zmax);

    Eigen::Vector3f bbxMin(min(0) - margin, min(1) - margin, this->minHeight);
    Eigen::Vector3f bbxMax(max(0) + margin, max(1) + margin, this->maxHeight);

    return Eigen::AlignedBox3f(bbxMin, bbxMax);
}

void PlacementAreaProvider::constructOctree(octomap::OcTree* tree, std::vector<Vector3BasePtr>& centers) const
{
    for (Vector3BasePtr& center : centers)
    {
        tree->updateNode(center->x, center->y, center->z, true);
    }
}

objpose::ObjectPose PlacementAreaProvider::getDefaultObject() const
{
    /*
     * set default object
     */
    objpose::ObjectPose objPose;
    objPose.objectID = ObjectID("KIT", "rusk");
    objPose.objectPoseGlobal = Eigen::Matrix4f::Identity();
    /*
     * dimensions 15cm x 15cm x 7cm (height x width x depth)
     */
    const Eigen::Vector3f ruskCorner(0.f, 0.f, 0.f);
    const Eigen::Vector3f extend0(150.f, 0.f, 0.f);
    const Eigen::Vector3f extend1(0.f, 70.f, 0.f);
    const Eigen::Vector3f extend2(0.f, 0.f, 150.f);
    simox::OrientedBoxf bbx(ruskCorner, extend0, extend1, extend2);
    objPose.localOOBB = std::make_optional(bbx);

    return objPose;
}
