#include <armar6/skills/components/PlacementAreaProvider/Visualization/Visualization.h>

using namespace armarx;

placement::Visualization::Visualization(const std::string& layerName, viz::Client& arviz)
    : layerName(layerName), arviz(arviz)
{
    setTag("Visualization");

    layers =
    {
        arviz.layer(layerName + "_samples"),
        arviz.layer(layerName + "_planes"),
        arviz.layer(layerName + "_octree")
    };
}

placement::Visualization::~Visualization()
{
    if (octreeVisu)
    {
        delete octreeVisu;
    }
}

void placement::Visualization::visualizeSamples(const std::vector<simox::XYConstrainedOrientedBoxf>& samples)
{
    if (!enableSampleVisu)
    {
        return;
    }

    if (samples.empty())
    {
        return;
    }

    ARMARX_INFO << "Visualizing placement area samples";

    size_t i = 0;
    for (simox::XYConstrainedOrientedBoxf sample : samples)
    {
        viz::Box box("sample_" + std::to_string(i), sample);
        box.show();

        // KIT-Maigrün
        box.color(viz::Color::fromRGBA(140, 182, 60));

        layers[0].add(box);
        i++;
    }

    ARMARX_INFO << "Added " << std::to_string(i) << " boxes to sample visu layer";

    arviz.commit({layers[0]});
}

void placement::Visualization::visualizePlanes(const std::vector<PlanePtr>& planes)
{
    if (!enablePlaneVisu)
    {
        return;
    }

    if (planes.empty())
    {
        return;
    }

    ARMARX_INFO << "Visualizing planar regions";

    size_t i = 0;
    for (PlanePtr plane : planes)
    {
        for (size_t k = 0; k < plane->getInputCloud()->size(); ++k)
        {
            const Point3 p = plane->getInputCloud()->points[k];
            const float res = plane->getResolution();
            const Eigen::Vector3f pos(p.x, p.y, p.z - 0.5f * res);
            /*
             * place box of original voxel size at original voxel position
             */
            viz::Box box = viz::Box("plane_" + std::to_string(i)
                                    + "_cell_" + std::to_string(k))
                           .size(res)
                           .position(pos);

            // KIT-Blau exklusiv
            box.color(viz::Color::fromRGBA(70, 100, 170));

            layers[1].add(box);
        }
        i++;
    }

    ARMARX_INFO << "Added " << std::to_string(i) << " planar regions to plane visu layer";

    arviz.commit({layers[1]});
}

void placement::Visualization::visualizeOctree(PlacementAreaPtr tree)
{
    if (!enableOctreeVisu)
    {
        return;
    }

    if (!tree)
    {
        return;
    }

    ARMARX_INFO << "Visualizing filtered octree";

    octreeVisu->drawOccupancyMap(*tree, OccupancyMapVisualizer::VoxelVisualizationMode::Boxes);
}

void placement::Visualization::setArviz(armarx::viz::Client& arviz)
{
    this->arviz = arviz;
}

void placement::Visualization::setDebugDrawer(const DebugDrawerInterfacePrx& drawer)
{
    this->debugDrawer = drawer;
    this->octreeVisu = new OccupancyMapVisualizer(layerName + "_octree", debugDrawer);
}

void placement::Visualization::clearLayer()
{
    layers[0].clear();
    layers[1].clear();
    layers[2].clear();
    arviz.commit(layers);

    octreeVisu->clearLayer();
}

void placement::Visualization::setLayerName(const std::string& layerName)
{
    clearLayer();
    this->layerName = layerName;

    layers[0] = arviz.layer(layerName + "_samples");
    layers[1] = arviz.layer(layerName + "_planes");
    layers[2] = arviz.layer(layerName + "_octree");
}

const std::string placement::Visualization::getLayerName() const
{
    return this->layerName;
}

void placement::Visualization::setSampleVisu(const bool& enabled)
{
    this->enableSampleVisu = enabled;
}

void placement::Visualization::setPlaneVisu(const bool& enabled)
{
    this->enablePlaneVisu = enabled;
}

void placement::Visualization::setOctreeVisu(const bool& enabled)
{
    this->enableOctreeVisu = enabled;
}
