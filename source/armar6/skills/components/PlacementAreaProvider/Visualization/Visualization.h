/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PlacementAreaProvider
 * @author     Max Beddies ( ubyjy at student dot kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/ArViz/Client/Client.h>

#include <ArmarXCore/interface/serialization/Shapes.h>

#include <ActiveVision/libraries/OctoMapUtils/OccupancyMapVisualizer.h>

#include <armar6/skills/components/PlacementAreaProvider/DataStructures/PlacementArea.h>

/*
 * temporary:   switch to ArViz once OctomapUtils::OctomapVisualizer can be
 *              used without causing cyclic dependencies
 */
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

namespace armarx::placement
{
    /**
     * @brief The Visualization class provides visualization functionality for
     * the PlavementAreaProvider.
     *
     * The Visualization class provides functionality to visualize
     * octrees of type PlacementArea, planes and sampled oriented boxes.
     */
    class Visualization : public armarx::Logging
    {
    public:
        /**
         * @brief Constructor for the Visualization class.
         * @param layerName Name of the ArViz layer to visualize objects on
         * @param arviz ArViz client
         */
        Visualization(const std::string& layerName, viz::Client& arviz);

        ~Visualization();

        /**
         * @brief Visualize sampled placement areas as oriented boxes.
         * @param samples Generated OOBB samples
         */
        void visualizeSamples(const std::vector<simox::XYConstrainedOrientedBoxf>& samples);

        /**
         * @brief Visualize planar surfaces extracted from scene voxels.
         * @param planes Vector of extracted planes
         */
        void visualizePlanes(const std::vector<PlanePtr>& planes);

        /**
         * @brief Visualize the occupancy map representing the scene.
         * @param tree Occupancy map representing the scene
         */
        void visualizeOctree(PlacementAreaPtr tree);

        /**
         * @brief Setter for the ArViz client.
         * @param arviz ArViz client
         */
        void setArviz(viz::Client& arviz);

        /**
         * @brief Setter for the DebugDrawer.
         * @param drawer Proxy to the DebugDrawer
         */
        void setDebugDrawer(const DebugDrawerInterfacePrx& drawer);

        /**
         * @brief Clears all ArViz layers used by the Visualization class.
         */
        void clearLayer();

        /**
         * @brief Clears all layers and sets a new layer name
         * @param layerName Name of the new layer
         */
        void setLayerName(const std::string& layerName);

        /**
         * @brief Getter for the layer name
         * @return the layer name
         */
        const std::string getLayerName() const;

        /**
         * @brief Setter for the enableSampleVisu flag
         * @param enabled Determines whether samples are visualized
         */
        void setSampleVisu(const bool& enabled);

        /**
         * @brief Setter for the enablePlaneVisu flag
         * @param enabled Determines whether planes are visualized
         */
        void setPlaneVisu(const bool& enabled);

        /**
         * @brief Setter for the enableOctreeVisu flag
         * @param enabled Determines whether the octree is visualized
         */
        void setOctreeVisu(const bool& enabled);

    private:
        std::string layerName;
        std::vector<viz::Layer> layers;

        bool enableSampleVisu = true;
        bool enablePlaneVisu = true;
        bool enableOctreeVisu = true;

        viz::Client arviz;
        DebugDrawerInterfacePrx debugDrawer;

        OccupancyMapVisualizer* octreeVisu;
    };

    using VisualizationPtr = std::shared_ptr<Visualization>;
}
