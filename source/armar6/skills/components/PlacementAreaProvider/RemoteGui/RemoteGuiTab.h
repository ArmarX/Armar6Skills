#pragma once

#include <ArmarXGui/libraries/RemoteGui/Widgets.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>

#include <armar6/skills/components/PlacementAreaProvider/Util/StringConversions.h>

namespace armarx::placement
{
    struct RemoteGuiDataset
    {
        // Constraints
        std::string input_id;
        std::string pos_constraint = toString(POS_ARBITRARY);
        std::string rot_constraint = toString(ROT_ARBITRARY);

        // Position values
        Eigen::Vector3f center = Eigen::Vector3f(0.f, 0.f, 0.f);

        Eigen::Vector3f corner = Eigen::Vector3f(0.f, 0.f, 0.f);
        Eigen::Vector3f dimensions = Eigen::Vector3f(500.f, 500.f, 0.f);
        float yaw = 0.f;

        std::string target_id;

        // Orientation values
        Eigen::Vector3f rpy = Eigen::Vector3f(0.f, 0.f, 0.f);
    };

    using RemoteGuiDatasetPtr = std::shared_ptr<RemoteGuiDataset>;

    /**
     * @brief The RemoteGuiTab class constructs different parts of a RemoteGui::Tab
     * used by the PlacementAreaProvider from a RemoteGuiDataset.
     * Provides methods to update the dataset from a given RemoteGui::Tab.
     */
    class RemoteGuiTab
    {
    public:
        RemoteGuiTab();

        /**
         * @brief Constructs the grid layout for constraint selection.
         * @param objectNames Names of all availabel objects
         * @return Pointer to the resulting RemoteGui::Widget
         */
        RemoteGui::WidgetPtr makeConstraintsLayout(const std::vector<std::string>& objectNames) const;

        /**
         * @brief Constructs the v-box layout for position constraints.
         * @param objectNames Names of all availabel objects
         * @return Pointer to the resulting RemoteGui::Widget
         */
        RemoteGui::WidgetPtr makePositionLayout(const std::vector<std::string>& objectNames) const;

        /**
         * @brief Constructs the v-box layout for orientation constraints.
         * @return Pointer to the resulting RemoteGui::Widget
         */
        RemoteGui::WidgetPtr makeOrientationLayout() const;

        /**
         * @brief Updates the RemoteGuiDataset from a given RemoteGui::Tab
         * @param prx Proxy to the RemoteGui::Tab
         */
        void updateDataset(RemoteGui::TabProxy& prx);

        /**
         * @brief Retreives the RemoteGuiDataset.
         * @return The current dataset
         */
        RemoteGuiDatasetPtr getDataset() const;

    private:
        RemoteGuiDatasetPtr dataset;
    };

    using RemoteGuiTabPtr = std::shared_ptr<RemoteGuiTab>;
}
