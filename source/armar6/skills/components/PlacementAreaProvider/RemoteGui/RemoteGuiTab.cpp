#include <armar6/skills/components/PlacementAreaProvider/RemoteGui/RemoteGuiTab.h>

using namespace armarx;

placement::RemoteGuiTab::RemoteGuiTab()
{
    dataset = std::make_shared<placement::RemoteGuiDataset>();
}

RemoteGui::WidgetPtr placement::RemoteGuiTab::makeConstraintsLayout(const std::vector<std::string>& objectNames) const
{
    using namespace RemoteGui;

    WidgetPtr objectBox = makeComboBox("InputID").options(objectNames);
    WidgetPtr posConstraintBox = makeComboBox("PosConstraint").options(getPositionConstraintNames()).value(dataset->pos_constraint);
    WidgetPtr rotConstraintBox = makeComboBox("RotConstraint").options(getOrientationConstraintNames()).value(dataset->rot_constraint);

    WidgetPtr updateButton = makeButton("Update objects");
    WidgetPtr computeButton = makeButton("Go");

    WidgetPtr grid = makeSimpleGridLayout().cols(2)
                     .addChild(makeTextLabel("Object: ")).addChild(objectBox)
                     .addChild(makeTextLabel("Position constraint: ")).addChild(posConstraintBox)
                     .addChild(makeTextLabel("Orientation constraint: ")).addChild(rotConstraintBox)
                     .addChild(updateButton).addChild(computeButton);

    return grid;
}

RemoteGui::WidgetPtr placement::RemoteGuiTab::makePositionLayout(const std::vector<std::string>& objectNames) const
{
    using namespace RemoteGui;

    WidgetPtr center = makeVector3fSpinBoxes("Center").value(dataset->center).min(-100000.f).max(100000.f);
    WidgetPtr corner = makeVector3fSpinBoxes("Corner").value(dataset->corner).min(-100000.f).max(100000.f);
    WidgetPtr dimensions = makeVector3fSpinBoxes("Dimensions").value(dataset->dimensions).min(-100000.f).max(100000.f);;
    WidgetPtr yaw = makeFloatSpinBox("Yaw").value(dataset->yaw).min(-M_PI).max(M_PI);
    WidgetPtr objectBox = makeComboBox("TargetID").options(objectNames);

    WidgetPtr positionLayout = makeSimpleGridLayout().cols(2)
                               .addChild(makeTextLabel("Center position [mm]: ")).addChild(center);
    WidgetPtr regionLayout = makeSimpleGridLayout().cols(2)
                             .addChild(makeTextLabel("Corner position [mm]: ")).addChild(corner)
                             .addChild(makeTextLabel("Dimensions [mm]: ")).addChild(dimensions)
                             .addChild(makeTextLabel("Yaw angle [rad]: ")).addChild(yaw);
    WidgetPtr objectLayout = makeSimpleGridLayout().cols(2)
                             .addChild(makeTextLabel("Target object: ")).addChild(objectBox);

    WidgetPtr positionGroup = makeGroupBox("Position").addChild(positionLayout);
    WidgetPtr regionGroup = makeGroupBox("Region").addChild(regionLayout);
    WidgetPtr objectGroup = makeGroupBox("Object").addChild(objectLayout);

    WidgetPtr box = makeVBoxLayout()
                    .addChild(positionGroup)
                    .addChild(regionGroup)
                    .addChild(objectGroup);

    return box;
}

RemoteGui::WidgetPtr placement::RemoteGuiTab::makeOrientationLayout() const
{
    using namespace RemoteGui;

    WidgetPtr rpy = makeVector3fSpinBoxes("RPY").value(dataset->rpy).min(-M_PI).max(M_PI);

    return makeSimpleGridLayout().cols(2).addChild(makeTextLabel("RPY [rad]:")).addChild(rpy);
}

void placement::RemoteGuiTab::updateDataset(RemoteGui::TabProxy& prx)
{
    dataset->input_id = prx.getValue<std::string>("InputID").get();
    dataset->pos_constraint = prx.getValue<std::string>("PosConstraint").get();
    dataset->rot_constraint = prx.getValue<std::string>("RotConstraint").get();

    dataset->center = prx.getValue<Eigen::Vector3f>("Center").get();
    dataset->corner = prx.getValue<Eigen::Vector3f>("Corner").get();
    dataset->dimensions = prx.getValue<Eigen::Vector3f>("Dimensions").get();
    dataset->yaw = prx.getValue<float>("Yaw").get();
    dataset->target_id = prx.getValue<std::string>("TargetID").get();

    dataset->rpy = prx.getValue<Eigen::Vector3f>("RPY").get();
}

placement::RemoteGuiDatasetPtr placement::RemoteGuiTab::getDataset() const
{
    return dataset;
}
