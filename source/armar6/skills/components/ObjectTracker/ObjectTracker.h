/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::ObjectTracker
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
#include <ArmarXGui/libraries/RemoteGui/Client/EigenWidgets.h>

#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/Component.h>


namespace armarx
{
    /**
     * @class ObjectTrackerPropertyDefinitions
     * @brief Property definitions of `ObjectTracker`.
     */
    class ObjectTrackerPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ObjectTrackerPropertyDefinitions(std::string prefix);
    };

    using namespace armarx::RemoteGui::Client;
    struct ObjectTrackerTab : Tab
    {
        Vector3Widget targetPosition;
        FloatSpinBox angleThresholdToMove;
        ToggleButton enableTracking;
        ComboBox objectsToTrack;
    };

    /**
     * @defgroup Component-ObjectTracker ObjectTracker
     * @ingroup armar6_skills-Components
     * A description of the component ObjectTracker.
     *
     * @class ObjectTracker
     * @ingroup Component-ObjectTracker
     * @brief Brief description of class ObjectTracker.
     *
     * Detailed description of class ObjectTracker.
     */
    class ObjectTracker
        : virtual public armarx::Component
        , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        , virtual public armarx::ArVizComponentPluginUser
        , virtual public armarx::RobotStateComponentPluginUser
    {
    public:
        std::string getDefaultName() const override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void createRemoteGuiTab();
        void RemoteGui_update() override;

    private:
        void trackingLoop();

    private:
        KinematicUnitInterfacePrx kinematicUnit;
        DebugObserverInterfacePrx debugObserver;

        RunningTask<ObjectTracker>::pointer_type task;

        ObjectTrackerTab tab;

        std::atomic<bool> trackingEnabled{false};
        Eigen::Vector3f targetPosition = Eigen::Vector3f::Zero();
        float angleThresholdToMove = 0.08f;

        VirtualRobot::RobotPtr robot;
    };
}
