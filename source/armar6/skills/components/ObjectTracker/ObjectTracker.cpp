/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::ObjectTracker
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ObjectTracker.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include <ArmarXCore/core/time/CycleUtil.h>

#include <ArmarXCore/observers/variant/Variant.h>

namespace armarx
{
    ARMARX_DECOUPLED_REGISTER_COMPONENT(ObjectTracker);

    ObjectTrackerPropertyDefinitions::ObjectTrackerPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver",
                                            "Name of the topic the DebugObserver listens to.");

        defineOptionalProperty<std::string>("KinematicUnitName", "Armar6KinematicUnit", "Kinematic unit name");
        defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens to.");
    }


    std::string ObjectTracker::getDefaultName() const
    {
        return "ObjectTracker";
    }


    void ObjectTracker::onInitComponent()
    {
        usingProxyFromProperty("KinematicUnitName");

        offeringTopicFromProperty("DebugObserverName");
    }


    void ObjectTracker::onConnectComponent()
    {
        getProxyFromProperty(kinematicUnit, "KinematicUnitName");
        getTopicFromProperty(debugObserver, "DebugObserverName");

        robot = addRobot("ARMAR-6", VirtualRobot::RobotIO::eStructure);
        createRemoteGuiTab();
        RemoteGui_startRunningTask();

        task = new RunningTask<ObjectTracker>(this, &ObjectTracker::trackingLoop);
        task->start();
    }


    void ObjectTracker::onDisconnectComponent()
    {
        task->stop();
        task = nullptr;
    }


    void ObjectTracker::onExitComponent()
    {

    }


    armarx::PropertyDefinitionsPtr ObjectTracker::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new ObjectTrackerPropertyDefinitions(
                getConfigIdentifier()));
    }

    void ObjectTracker::createRemoteGuiTab()
    {
        tab.targetPosition.setValue(Eigen::Vector3f(0.0f, 500.0f, 1500.0f));
        tab.targetPosition.setSteps(1000);

        tab.angleThresholdToMove.setRange(0.0f, 1.5f);
        tab.angleThresholdToMove.setSteps(150);
        tab.angleThresholdToMove.setDecimals(2);
        tab.angleThresholdToMove.setValue(angleThresholdToMove);

        tab.enableTracking.setLabel("Enable Tracking");

        tab.objectsToTrack.addOption("<FIXED TARGET>");

        VBoxLayout root =
        {
            tab.targetPosition,
            HBoxLayout({Label("Angle Threshold:"), tab.angleThresholdToMove}),
            tab.enableTracking,
            tab.objectsToTrack,
            VSpacer()
        };

        RemoteGui_createTab("DoorOpener", root, &tab);
    }

    void ObjectTracker::RemoteGui_update()
    {
        bool enableTracking = tab.enableTracking.getValue();
        if (enableTracking != trackingEnabled)
        {
            if (enableTracking)
            {
                ARMARX_IMPORTANT << "Enabling tracking";
            }
            else
            {
                ARMARX_IMPORTANT << "Disabling tracking";
            }
            trackingEnabled = enableTracking;
        }

        // TODO: Mutex
        targetPosition = tab.targetPosition.getValue();
        angleThresholdToMove = tab.angleThresholdToMove.getValue();
    }

    void ObjectTracker::trackingLoop()
    {
        synchronizeLocalClone(robot);

        VirtualRobot::RobotNodePtr yawNode = robot->getRobotNode("Neck_1_Yaw");
        VirtualRobot::RobotNodePtr pitchNode = robot->getRobotNode("Neck_2_Pitch");

        NameControlModeMap controlModes;
        controlModes["Neck_1_Yaw"] = ePositionControl;
        controlModes["Neck_2_Pitch"] = ePositionControl;
        kinematicUnit->switchControlMode(controlModes);

        NameValueMap jointValues;
        float* yawTarget = &jointValues["Neck_1_Yaw"];
        float* pitchTarget = &jointValues["Neck_2_Pitch"];
        *yawTarget = yawNode->getJointValue();
        *pitchTarget = pitchNode->getJointValue();


        viz::Robot vizRobot = viz::Robot("robot")
                              .file("armar6_rt", "armar6_rt/robotmodel/Armar6-SH/Armar6-SH.xml")
                              .joints(robot->getConfig()->getRobotNodeJointValueMap());

        viz::Sphere vizFrom = viz::Sphere("from")
                              .radius(50.0f)
                              .colorGlasbeyLUT(0);

        viz::Sphere vizTo = viz::Sphere("to")
                            .radius(50.0f)
                            .colorGlasbeyLUT(1);

        viz::Layer robotLayer = arviz.layer("Robot");
        robotLayer.add(vizRobot);
        viz::Layer posesLayer = arviz.layer("Poses");
        posesLayer.add(vizFrom);
        posesLayer.add(vizTo);

        CycleUtil c(10);
        while (!task->isStopped())
        {
            synchronizeLocalClone(robot);

            // TODO: Mutex
            // TODO: The target comes in global if it is a localized object
            Eigen::Vector3f targetInRoot = targetPosition;


            Eigen::Vector3f headPosition = pitchNode->getPositionInRootFrame();
            float currentYaw = yawNode->getJointValue();
            float currentPitch = pitchNode->getJointValue();

            // TODO: Calculate pitch and yaw to get the target
            // First, the yaw angle
            Eigen::Vector3f yawInRoot = headPosition;

            Eigen::Vector3f diff = targetInRoot - yawInRoot;
            float diffY = diff.y();
            float diffX = diff.x();

            {
                float atanOut = std::atan2(diffY, diffX);
                float shiftedAtan = atanOut - M_PI_2;
                if (shiftedAtan < - M_PI)
                {
                    shiftedAtan += 2.0 * M_PI;
                }
                float yawMin = -M_PI_2;
                float yawMax = +M_PI_2;
                float yaw = std::max(yawMin, std::min(shiftedAtan, yawMax));

                float yawDiff = std::abs(yaw - currentYaw);
                if (yawDiff > angleThresholdToMove)
                {
                    *yawTarget = yaw;
                }

                debugObserver->setDebugDatafield("ObjectTracker", "Yaw_Atan", new armarx::Variant(atanOut));
                debugObserver->setDebugDatafield("ObjectTracker", "Yaw_AtanShifted", new armarx::Variant(shiftedAtan));
                debugObserver->setDebugDatafield("ObjectTracker", "Yaw_Target", new armarx::Variant(yaw));
            }

            // Now pitch
            {
                float diffZ = diff.z();
                float diffD = std::sqrt(diffX * diffX + diffY * diffY);
                float atanOut = std::atan2(diffZ, diffD);
                float shiftedAtan = atanOut - 0.0f;
                if (shiftedAtan < - M_PI)
                {
                    shiftedAtan += 2.0 * M_PI;
                }
                float pitchMin = -M_PI_2;
                float pitchMax = +M_PI_2;
                float pitch = -std::max(pitchMin, std::min(shiftedAtan, pitchMax));

                float pitchDiff = std::abs(pitch - currentPitch);
                if (pitchDiff > angleThresholdToMove)
                {
                    *pitchTarget = pitch;
                }

                debugObserver->setDebugDatafield("ObjectTracker", "Pitch_Atan", new armarx::Variant(atanOut));
                debugObserver->setDebugDatafield("ObjectTracker", "Pitch_AtanShifted", new armarx::Variant(shiftedAtan));
                debugObserver->setDebugDatafield("ObjectTracker", "Pitch_Target", new armarx::Variant(pitch));
            }


            vizFrom.position(headPosition);
            vizTo.position(targetInRoot);

            vizRobot.joints(jointValues);

            arviz.commit({robotLayer, posesLayer});

            if (trackingEnabled)
            {
                kinematicUnit->setJointAngles(jointValues);
            }

            c.waitForCycleDuration();
        }
    }
}
