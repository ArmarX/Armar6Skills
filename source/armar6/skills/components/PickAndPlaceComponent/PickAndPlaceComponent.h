/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    InteractiveSceneExplorationX::ArmarXObjects::PickAndPlaceComponent
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "BoxGraspCandidatePlanner.h"
#include "PickAndPlaceController.h"

#include <armar6/skills/interface/PickAndPlaceComponentInterface.h>
#include <armar6/skills/libraries/Armar6Tools/Armar6GraspTrajectory.h>

#include <RobotAPI/interface/units/HeadIKUnit.h>
#include <RobotAPI/interface/units/GraspCandidateProviderInterface.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/armem/client/plugins/PluginUser.h>
#include <RobotAPI/libraries/GraspingUtility/GraspCandidateReader.h>
#include <RobotAPI/libraries/GraspingUtility/GraspCandidateWriter.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
#include <ArmarXGui/libraries/RemoteGui/Client/EigenWidgets.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/observers/DebugObserver.h>
#include <ArmarXCore/libraries/DebugObserverHelper/DebugObserverHelper.h>

#include <SimoxUtility/caching/CacheMap.h>
#include <SimoxUtility/threads/CountingSemaphore.h>
#include <SimoxUtility/meta/EnumNames.hpp>

#include <Eigen/Core>

#include <atomic>

#include <armarx/manipulation/action_execution/ActionExecutorInterface.h>


namespace armarx
{
    enum class Action
    {
        Begin,

        None = Begin,

        Prepose,
        Push,
        Pick,
        Place,
        Retreat,
        PreposeReverse,
        LocalizeObjectInHand,

        End
    };

    const simox::meta::EnumNames<Action> action_names
    {
        {Action::None, "<None>" },
        {Action::Retreat, "Retreat" },
        {Action::Prepose, "Prepose" },
        {Action::PreposeReverse, "PreposeReverse" },
        {Action::Push, "Push" },
        {Action::Pick, "Pick" },
        {Action::Place, "Place" },
        {Action::LocalizeObjectInHand, "LocalizeObjectInHand" }
    };

    struct ControllerInfo
    {
        // Info for execute methods
        Action action = Action::None;
        Side side;
        objpose::ObjectPose objectPose;

        grasping::GraspCandidatePtr grasp;
        Eigen::Vector3f liftVector { 0.0f, 0.0f, 200.0f };
        Eigen::Vector3f placePositionGlobal = Eigen::Vector3f::Zero();

        std::map<std::string, Armar6GraspTrajectoryPtr> graspTrajectories;

        // Info for localize object in hand
        ObjectID objectID;
        int initialWaitMS = 0;
        float maxDistanceFromTcp = 50.0;
        int minPoseUpdates = 0;
        bool moveHead = false;
        float pollFrequency = 1.0;
        int timeoutMS = 100;
        objpose::ObjectPoseSeq acceptedObjectPoses;
    };
    struct ControllerOutput
    {
        bool executed = false;
    };

    struct DataControl
    {
        /// The object poses.
        objpose::ObjectPoseSeq objectPoses;
        /// Only non-static objects in `objectPoses` (points into `objectPoses`).
        std::vector<objpose::ObjectPose*> objectPosesDynamic;
        void setObjectPoses(const objpose::ObjectPoseSeq& objectPoses);

        ControllerInfo controllerInfo;
        ControllerOutput controllerOutput;

        // Variables used for getting grasp candidates
        std::unique_ptr<BoxGraspCandidatePlanner> boxGraspCandidatePlanner;
        VirtualRobot::RobotPtr robot;

        // Variables used by subscription of object memory
        bool autoUpdateGraspsAllObjects = false;
        bool objectsUpdated = false;

        // Thread control and mutexes
        std::atomic<bool> runSimulation = false;
        std::atomic<bool> runExecution = false;
        simox::threads::CountingSemaphore runFinished;

        std::mutex controlMutex;
        std::mutex objectPoseMutex;
    };

    struct PickAndPlaceTab : RemoteGui::Client::Tab
    {
        RemoteGui::Client::ComboBox selectObject;
        RemoteGui::Client::Button calculateGraspCandidates;
        RemoteGui::Client::ComboBox selectGrasp;
        RemoteGui::Client::Vector3Widget placePosition;
        RemoteGui::Client::ComboBox selectAction;
        RemoteGui::Client::ComboBox selectSide;
        RemoteGui::Client::Button executeAction;
        RemoteGui::Client::Button stop;
        RemoteGui::Client::Button saveObjectPoses;
        RemoteGui::Client::CheckBox autoUpdateGraspCandidates;
        RemoteGui::Client::CheckBox autoUpdatePlaceCandidate;
        RemoteGui::Client::CheckBox autoUpdateGraspsAllObjects;
     };

    struct GuiInfo
    {
        // UI state variables
        Action action = Action::None;
        Side side = Side::Right;
        int objectIndex = 0;
        std::string graspID = "";
        int planStepIndex = 0;
        bool autoUpdateGrasps = false;
        bool autoUpdatePlace = false;

        Eigen::Vector3f placePosition = Eigen::Vector3f::Zero();

        PickAndPlaceTab tab;

        // Variables only used in gui thread
        simox::caching::CacheMap<armarx::ObjectID, PackageFileLocation> simoxFileCache;
        objpose::ObjectPose graspCandidatesObject;
        std::map<std::string, std::string> graspCandidates;

        bool recreateTabNextLoop = false;
    };



    class PickAndPlaceComponent
        : public virtual Component
        , public virtual grasping::PickAndPlaceComponentInterface
        , public virtual LightweightRemoteGuiComponentPluginUser
        , public virtual RobotStateComponentPluginUser
        , public virtual ArVizComponentPluginUser
        , public virtual ObjectPoseClientPluginUser
        , public virtual armem::client::ListeningPluginUser
    {
    public:

        PickAndPlaceComponent();

        std::string getDefaultName() const override;
        static std::string GetDefaultName();

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        void createRemoteGuiTab();
        void RemoteGui_update() override;


#define ICE_CURRENT_ARG const Ice::Current& = Ice::emptyCurrent

        // PickAndPlaceComponentInterface interface
        grasping::CalculateGraspCandidatesOutput
        calculateGraspCandidates(const grasping::CalculateGraspCandidatesInput& input, ICE_CURRENT_ARG) override;

        grasping::ExecutePreposeOutput
        executePrepose(const grasping::ExecutePreposeInput& input, ICE_CURRENT_ARG) override;

        grasping::ExecutePushOutput
        executePush(const grasping::ExecutePushInput& input, ICE_CURRENT_ARG) override;

        grasping::ExecutePickOutput
        executePick(const grasping::ExecutePickInput& input, ICE_CURRENT_ARG) override;

        grasping::ExecutePlaceOutput
        executePlace(const grasping::ExecutePlaceInput& input, ICE_CURRENT_ARG) override;

        grasping::ExecuteRetreatOutput
        executeRetreat(const grasping::ExecuteRetreatInput& input, ICE_CURRENT_ARG) override;

        grasping::ExecutePreposeReverseOutput
        executePreposeReverse(const grasping::ExecutePreposeReverseInput& input, ICE_CURRENT_ARG) override;

        grasping::LocalizeObjectInHandOutput
        localizeObjectInHand(const grasping::LocalizeObjectInHandInput& input, ICE_CURRENT_ARG) override;

        aron::data::dto::DictPtr executeAction(const ::armarx::aron::data::dto::DictPtr& executableAction, ICE_CURRENT_ARG) override;
        void recover(ICE_CURRENT_ARG) override;
        void abort(ICE_CURRENT_ARG) override;

#undef ICE_CURRENT_ARG

        void saveObjectPoses();
        void visualizeGraspCandidates();
        void visualizePlacePosition();
        void visualizeLocalizePosition();
        void visualizeObjectPoses();

        void runControlLoop();

        void getGraspCandidatesInPluginFormat(
                DataControl& data, const Side& side, const objpose::ObjectPose& objectPose);


    private:

        DebugObserverInterfacePrx debugObserver;
        KinematicUnitInterfacePrx kinematicUnit;
        RobotUnitInterfacePrx robotUnit;
        ObserverInterfacePrx robotUnitObserver;
        ForceTorqueUnitObserverInterfacePrx forceTorqueObserver;
        HeadIKUnitInterfacePrx headIK;
        RobotNameHelperPtr robotNameHelper;
        SimpleRunningTask<>::pointer_type controlTask;
        PickAndPlaceConfig config;
        std::unique_ptr<PickAndPlaceController> controller;

        GuiInfo gui;
        DataControl data;

        std::string graspMemoryName = "Grasp";
        armarx::armem::GraspCandidateWriter graspWriter;
        armarx::armem::GraspCandidateReader graspReader;

        armarx::armem::client::Reader objectReader;

    };
}
