#pragma once

#include <SimoxUtility/meta/EnumNames.hpp>
#include <SimoxUtility/json/io.h>
#include <SimoxUtility/json/eigen_conversion.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/libraries/DebugObserverHelper/DebugObserverHelper.h>

#include <RobotAPI/interface/objectpose/ObjectPoseStorageInterface.h>
#include <RobotAPI/interface/units/HeadIKUnit.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>
#include <RobotAPI/libraries/core/CartesianPositionController.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>
#include <RobotAPI/components/ArViz/Client/Client.h>

#include <armar6/skills/interface/PickAndPlaceComponentInterface.h>
#include <armar6/skills/libraries/Armar6Tools/Armar6GraspTrajectory.h>


namespace armarx
{


    enum class PhaseType
    {
        ApproachPrePose,
        ApproachObject,
        SimpleGrasp,
        TrajectoryGrasp,
        Lift,
        Replace,
        Place,
        RetractAfterPlace,

        LocalizeApproach,
        LocalizeRotate,
    };

    enum class GraspProcedureType
    {
        Simple,
        Trajectory
    };

    enum class Side
    {
        Left,
        Right
    };

    const simox::meta::EnumNames<Side> side_names
    {
        { Side::Left, "Left" },
        { Side::Right, "Right" },
    };


    struct GeneralConfig
    {
        bool IsSimulation = false;
        float MaximalDistanceToStartInMM = 300.0f;
        float SimulationForceHeightThresholdInMM = 1050.0f;
        float ForceThresholdInPotatoes = 5.0f;
    };

    struct RetreatConfig
    {
        CartesianPositionControllerConfig ControllerConfig;
    };

    struct PickConfig
    {
        CartesianPositionControllerConfig ControllerConfig;
    };

    struct PlaceConfig
    {
        CartesianPositionControllerConfig ControllerConfig;
    };

    struct PreposeSingleConfig
    {
        std::vector<Eigen::Matrix4f> Waypoints;
    };

    struct PreposeConfig
    {
        CartesianPositionControllerConfig ControllerConfig;
        PreposeSingleConfig Left;
        PreposeSingleConfig Right;
    };

    struct SupportConfig
    {
        PreposeSingleConfig Left;
        PreposeSingleConfig Right;
    };

    struct LocalizeSingleConfig
    {
        FramedPose TcpPose;
    };

    struct LocalizeConfig
    {
        LocalizeSingleConfig Left;
        LocalizeSingleConfig Right;
    };

    //compare to json
    struct PickAndPlaceConfig
    {
        GeneralConfig General;
        RetreatConfig Retreat;
        PreposeConfig Prepose;
        SupportConfig Support;
        LocalizeConfig Localize;
        PickConfig Pick;
        PlaceConfig Place;
    };



    void from_json(nlohmann::json const& j, GeneralConfig& config);
    void from_json(nlohmann::json const& j, PreposeSingleConfig& config);
    void from_json(nlohmann::json const& j, PreposeConfig& config);
    void from_json(nlohmann::json const& j, SupportConfig& config);
    void from_json(nlohmann::json const& j, FramedPose& pose);
    void from_json(nlohmann::json const& j, LocalizeSingleConfig& config);
    void from_json(nlohmann::json const& j, LocalizeConfig& config);
    void from_json(nlohmann::json const& j, PickAndPlaceConfig& config);
    void from_json(nlohmann::json const& j, CartesianPositionControllerConfig& config);
    void from_json(nlohmann::json const& j, RetreatConfig& config);
    void from_json(nlohmann::json const& j, PickConfig& config);
    void from_json(nlohmann::json const& j, PlaceConfig& config);

    class PickAndPlaceController
    {

    public:
        PickAndPlaceController();
        PickAndPlaceController(
            VirtualRobot::RobotPtr const& robot,
            RobotNameHelperPtr const& robotNameHelper,
            KinematicUnitInterfacePrx const& kinematicUnit,
            ObserverInterfacePrx const& robotUnitObserver,
            PickAndPlaceConfig const& config,
            RobotUnitInterfacePrx const& robotUnit,
            RobotStateComponentInterfacePrx const& robotStatePrx,
            ForceTorqueUnitObserverInterfacePrx const& forceTorqueObserver,
            DebugObserverHelper const& debugObserverHelper,
            objpose::ObjectPoseStorageInterfacePrx const& objectPoseStorage);

        void stopExecution();
        void resetStop();
        void setName(std::string const& name);
        std::string getName();


        void executeRetreat(Side const& side);
        void executePrepose(Side const& side);
        void executePreposeReverse(Side const& side);
        void executePush();

        struct ExecutePickOutput
        {
            bool executed = false;
        };
        ExecutePickOutput executePick(
            Side const& side, grasping::GraspCandidatePtr const& grasp, Eigen::Vector3f const& liftVector,
            std::map<std::string, Armar6GraspTrajectoryPtr> const& graspTrajectories,
            objpose::ObjectPoseSeq& objectPoses, std::mutex& objectPoseMutex);

        void executePlace(Side const& side, Eigen::Vector3f const& placePositionGlobal,
                          objpose::ObjectPoseSeq& objectPoses, std::mutex& objectPoseMutex);

        objpose::ObjectPoseSeq executeLocalizeInHand(Side const& side, ObjectID const& objectID,
                objpose::ObjectPoseSeq const& objectPoses, std::mutex& objectPoseMutex, int initialWaitMS,
                float maxDistanceFromTcp, int minPoseUpdates, bool moveHead, float pollFrequency, int timeoutMS);

        void setRobot(const VirtualRobot::RobotPtr& value);

        void setRobotNameHelper(const RobotNameHelperPtr& value);

        void setKinematicUnit(const KinematicUnitInterfacePrx& value);

        void setRobotUnitObserver(const ObserverInterfacePrx& value);

        void setRobotUnit(const RobotUnitInterfacePrx& value);

        void setRobotStatePrx(const RobotStateComponentInterfacePrx& value);

        void setForceTorqueObserver(const ForceTorqueUnitObserverInterfacePrx& value);

        void setDebugObserver(const DebugObserverInterfacePrx& value);

        void setConfig(const PickAndPlaceConfig& value);

        void setHeadIK(const HeadIKUnitInterfacePrx& value);

        void setObjectPoseStorage(const objpose::ObjectPoseStorageInterfacePrx& value);

        void setDebugObserverHelper(const std::optional<DebugObserverHelper>& value);

    private:
        void attachObjectToRobotPose(std::string const& object, VirtualRobot::RobotNodePtr const& node,
                                     objpose::ObjectPoseSeq& objectPoses);
        void detachAllObjects(objpose::ObjectPoseSeq& objectPoses);
        void checkProxies();

        VirtualRobot::RobotPtr robot;
        RobotNameHelperPtr robotNameHelper;
        KinematicUnitInterfacePrx kinematicUnit;
        ObserverInterfacePrx robotUnitObserver;
        std::optional<PickAndPlaceConfig> config;
        RobotUnitInterfacePrx robotUnit;
        RobotStateComponentInterfacePrx robotStatePrx;
        ForceTorqueUnitObserverInterfacePrx forceTorqueObserver;
        std::optional<DebugObserverHelper> debugObserverHelper;
        HeadIKUnitInterfacePrx headIK;
        objpose::ObjectPoseStorageInterfacePrx objectPoseStorage;

        std::string name = "PickAndPlaceController";

        std::atomic<bool> stop = false;

    };
}


