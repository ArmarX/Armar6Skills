#pragma once
#include <ArmarXCore/core/Component.h>
#include <armarx/manipulation/core/ExecutableAction.h>
#include <armar6/skills/interface/PickAndPlaceComponentInterface.h>
#include <RobotAPI/libraries/armem_robot/types.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h>

namespace armarx
{
    class PickAndPlaceExecutionComponent
             : public virtual Component
             , public virtual ObjectPoseClientPluginUser
    {
    public:
        PickAndPlaceExecutionComponent();

        std::string getDefaultName() const override;
        static std::string GetDefaultName();

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        void run();

        manipulation::core::ExecutableAction preparePrepose(const std::string side);
        manipulation::core::ExecutableAction prepareRetract(const std::string side);
        manipulation::core::ExecutableAction preparePreposeReverse(const std::string side);
        manipulation::core::ExecutableAction preparePick(const grasping::GraspCandidatePtr& cnd, armem::Time obsTime);
        manipulation::core::ExecutableAction preparePlace(const std::string side, Eigen::Vector3f const& placePositionGlobal);

    private:
        armem::robot::RobotDescription robot;
        grasping::PickAndPlaceComponentInterfacePrx papComp;
    };

}
