#include "PickAndPlaceExecutionComponent.h"

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include <ArmarXGui/libraries/StructuralJson/StructuralJsonParser.h>
#include <ArmarXGui/libraries/StructuralJson/JPathNavigator.h>

#include <SimoxUtility/math/pose/pose.h>

#include <armarx/manipulation/core/aron_conversions.h>
#include <armarx/manipulation/core/aron/ExecutableAction.aron.generated.h>
#include <armarx/manipulation/core/aron/ExecutedAction.aron.generated.h>
#include <armarx/manipulation/core/Affordance.h>
#include <armarx/manipulation/core/ExecutedAction.h>



namespace armarx
{

    PickAndPlaceExecutionComponent::PickAndPlaceExecutionComponent()
    {

    }

    std::string PickAndPlaceExecutionComponent::GetDefaultName()
    {
        return "PickAndPlaceExecutionComponent";
    }

    PropertyDefinitionsPtr PickAndPlaceExecutionComponent::createPropertyDefinitions()
    {
        ARMARX_TRACE;

        armarx::PropertyDefinitionsPtr properties(new armarx::ComponentPropertyDefinitions(getConfigIdentifier()));
        properties->component(papComp);
        return properties;
    }

    void PickAndPlaceExecutionComponent::onInitComponent()
    {
        ARMARX_TRACE;
    }

    void PickAndPlaceExecutionComponent::onConnectComponent()
    {
        ARMARX_TRACE;
        //robot is currently hardcoded
        //TODO get robot from pap component)
        robot.name = "robot";
        robot.xml = PackagePath("armar6_rt", "robotmodel/Armar6-SH/Armar6-SH.xml");
        run();
    }

    void PickAndPlaceExecutionComponent::onDisconnectComponent()
    {
        ARMARX_TRACE;
    }

    void PickAndPlaceExecutionComponent::onExitComponent()
    {
        ARMARX_TRACE;
    }

    void PickAndPlaceExecutionComponent::run()
    {
        ARMARX_TRACE;

        std::string side = "Right";

        grasping::CalculateGraspCandidatesInput input;
        input.side = side;

        //get objectposes from scene and pick apple tea, because it has reachable grasp
        objpose::ObjectPoseSeq objectPoses = getObjectPoses();

        objpose::ObjectPose objectPose;

        for (objpose::ObjectPose & object : objectPoses)
        {
            std::string name = object.objectID.className();
            if(name.find("apple") != name.npos )
            {
                objectPose = object;
                break;
            }
        }


        //get grasp candidate from pap component to fill action with
        input.objectPose = objectPose.toIce();
        auto candidates = this->papComp->calculateGraspCandidates(input).candidates;
        armem::Time obsTime;
        grasping::GraspCandidatePtr candidate;
        for(auto &  cnd : candidates)
        {
            if (cnd.second->reachabilityInfo && cnd.second->reachabilityInfo->reachable)
            {
                obsTime = armem::MemoryID(cnd.first).timestamp;
                candidate = cnd.second;
                ARMARX_INFO << "Found candidate for " << candidate->sourceInfo->referenceObjectName;
                break;
            }
        }

        std::vector<manipulation::core::ExecutableAction> actions;

        //place pos is simply old object pos
        Eigen::Vector3f placePos = simox::math::position(objectPose.objectPoseGlobal);

        //prepare actions for pap phases
        //actions.push_back(preparePrepose(side));
        actions.push_back(preparePick(candidate, obsTime));
        actions.push_back(preparePlace(side, placePos));
        //actions.push_back(prepareRetract(side));
        //actions.push_back(preparePreposeReverse(side));


        //execute all actions
        for (manipulation::core::ExecutableAction & action : actions)
        {
            ARMARX_INFO << "Executing Action " << action.unimanual.at(0).hypothesis->affordance.get()->getAffordanceID();

            armarx::manipulation::core::arondto::ExecutedAction dto;
            dto.fromAron(papComp->executeAction(armarx::manipulation::core::toAron(action).toAronDTO()));

            manipulation::core::ExecutedAction ex = armarx::manipulation::core::fromAron(dto);

            if(ex.success)
            {
                ARMARX_INFO << "Sucess";
            }
            else
            {
                ARMARX_INFO << "Failed with comment: " << ex.comment->data();
            }
        }

    }

    std::string PickAndPlaceExecutionComponent::getDefaultName() const
    {
        return GetDefaultName();
    }


    manipulation::core::ExecutableAction PickAndPlaceExecutionComponent::preparePrepose(const std::string side)
    {
        //prepose only needs side
        ARMARX_TRACE;

        FramedPose pose;
        manipulation::core::arondto::Handedness handedness;
        if (side == "Right")
        {
            handedness =manipulation::core::arondto::Handedness::Right;
        }
        else
        {
            handedness =manipulation::core::arondto::Handedness::Left;
        }
        manipulation::core::ActionHypothesis hypothesis(
                    "Prepose",      // ActionType
                    this->robot,    //RobotDescription
                    pose,           //pose
                    0,              //obsTime, 0 because it's constant
                    std::nullopt,   //poseUncertainty
                    handedness,     //handedness
                    std::nullopt    //existCert
                    );

        manipulation::core::ExecutableAction preposeAction{
            manipulation::core::ExecutableAction::Unimanual{
                        .hypothesis=hypothesis,     //ActionHypothesis
                        .execPose=std::nullopt,     //execPose
                        .prePose=std::nullopt,   //prePose
                        .retractPose=std::nullopt,   //retractPose
                        .actionTrajectory=std::nullopt,   //actionTrajectory
                        .handedness=handedness   // Handedness
            },
            manipulation::core::ExecutableAction::Common{ //platformPose
            }};

        return preposeAction;
    }

    manipulation::core::ExecutableAction PickAndPlaceExecutionComponent::prepareRetract(const std::string side)
    {
        //retract only needs side
        ARMARX_TRACE;

        FramedPose pose;
        //get handedness
        manipulation::core::arondto::Handedness handedness;
        if (side == "Right")
        {
            handedness =manipulation::core::arondto::Handedness::Right;
        }
        else
        {
            handedness =manipulation::core::arondto::Handedness::Left;
        }
        manipulation::core::ActionHypothesis hypothesis(
                    "Retract",
                    this->robot,    //RobotDescription
                    pose,           //pose
                    0,              //obsTime, 0 because it's constant
                    std::nullopt,   //poseUncertainty
                    handedness,     //handedness
                    std::nullopt    //existCert
                    );

        manipulation::core::ExecutableAction retractAction{
            manipulation::core::ExecutableAction::Unimanual{
                    .hypothesis=hypothesis,     //ActionHypothesis
                    .execPose=std::nullopt,     //execPose
                    .prePose=std::nullopt,   //prePose
                    .retractPose=std::nullopt,   //retractPose
                    .actionTrajectory=std::nullopt,   //actionTrajectory
                    .handedness=handedness   // Handedness
            },
            manipulation::core::ExecutableAction::Common{
            }};

        return retractAction;
    }

    manipulation::core::ExecutableAction PickAndPlaceExecutionComponent::preparePreposeReverse(const std::string side)
    {
        //prepose reverse only need side
        ARMARX_TRACE;

        FramedPose pose;
        //get handedness
        manipulation::core::arondto::Handedness handedness;
        if (side == "Right")
        {
            handedness =manipulation::core::arondto::Handedness::Right;
        }
        else
        {
            handedness =manipulation::core::arondto::Handedness::Left;
        }
        manipulation::core::ActionHypothesis hypothesis(
                    "PreposeReverse",
                    this->robot,    //RobotDescription
                    pose,           //pose
                    0,              //obsTime, 0 because it's constant
                    std::nullopt,   //poseUncertainty
                    handedness,     //handedness
                    std::nullopt    //existCert
                    );

        manipulation::core::ExecutableAction preposeReverseAction{
            manipulation::core::ExecutableAction::Unimanual{
                    .hypothesis=hypothesis,     //ActionHypothesis
                    .execPose=std::nullopt,     //execPose
                    .prePose=std::nullopt,   //prePose
                    .retractPose=std::nullopt,   //retractPose
                    .actionTrajectory=std::nullopt,   //actionTrajectory
                    .handedness=handedness   // Handedness
            },
            manipulation::core::ExecutableAction::Common{
            }};

        return preposeReverseAction;
    }
    
    manipulation::core::ExecutableAction PickAndPlaceExecutionComponent::preparePick(const grasping::GraspCandidatePtr &cnd, armem::Time obsTime)
    {
        //fill as many fields of action as possible with grasp candidate
        //graspTrajectoryName, graspPose, approachVecotr, referenceObjectName and liftVector are used in execution
        //TODO add referenceObjectName to ActionHypothesis somehow, maybe link to object memory
        ARMARX_TRACE;

        //get Armar6GraspTrajectory
        std::string packageName = "armar6_skills";
        armarx::CMakePackageFinder finder(packageName);
        std::string dataDir = finder.getDataDir() + "/" + packageName;
        std::string filename = dataDir + "/motions/grasps/" + cnd->executionHints->graspTrajectoryName + ".xml";
        RapidXmlReaderPtr reader = RapidXmlReader::FromFile(filename);

        RapidXmlReaderNode root = reader->getRoot("Armar6GraspTrajectory");
        manipulation::core::HandTrajectory traj;
        float duration = 0.0;
        for (const RapidXmlReaderNode& kpNode : root.nodes("Keypoint"))
        {

            StructuralJsonParser p(kpNode.value());
            p.parse();
            JPathNavigator nav(p.parsedJson);
            float dt = nav.selectSingleNode("dt").asFloat();

            duration += dt; //attention, duration variable is insufficient if trajectory has different dts for its keypoints, this is however not the case for the 2 trajectories currently used in the PickAndPlaceComponent

            Eigen::Matrix4f pose;
            std::vector<JPathNavigator> rows = nav.select("Pose/*");
            for (int i = 0; i < 4; i++)
            {
                std::vector<JPathNavigator> cells = rows.at(i).select("*");
                for (int j = 0; j < 4; j++)
                {
                    pose(i, j) = cells.at(j).asFloat();
                }
            }
            FramedPose framedPose(pose, "root", robot.name);

            std::map<std::string, float> fingerValues;
            std::vector<JPathNavigator> cells = nav.select("HandValues/*");
            for (int j = 0; j < 3; j++)
            {
                fingerValues["finger_" + std::to_string(j)] = cells.at(j).asFloat();
            }
            traj.keypoints.push_back(manipulation::core::HandTrajectoryKeypoint(framedPose, fingerValues));

        }
        traj.duration = armem::Duration::SecondsDouble(duration);

        // get grasp pose
        FramedPose pose(cnd->graspPose->position, cnd->graspPose->orientation, cnd->sourceFrame, robot.name);        //write grasp pose into pose

        //get handedness
        manipulation::core::arondto::Handedness handedness;
        if (cnd->side == "Right")
        {
            handedness =manipulation::core::arondto::Handedness::Right;
        }
        else
        {
            handedness =manipulation::core::arondto::Handedness::Left;
        }

        //get liftVector as retractPose
        Eigen::Vector3f liftVector { 0.0f, 0.0f, 200.0f }; //default from PaPComponent
        Eigen::Quaternionf quat(0,0,0,0);

        FramedPose retractPose(toIce(liftVector), toIce(quat), "root", robot.name);

        //get approach Vector as prePose
        FramedPose prePose(cnd->approachVector, toIce(quat), "root", robot.name);


        manipulation::core::ActionHypothesis hypothesis(
                    "grasp",
                    this->robot,    //RobotDescription
                    pose,           //pose
                    obsTime.toMicroSecondsSinceEpoch(),              //obsTime
                    std::nullopt,   //poseUncertainty
                    handedness,     //handedness
                    std::nullopt    //existCert
                    );

        manipulation::core::ExecutableAction pickAction{
            manipulation::core::ExecutableAction::Unimanual{
                .hypothesis=hypothesis,     //ActionHypothesis
                .execPose=pose,   //prePose
                .prePose=prePose,   //prePose
                .retractPose=retractPose,   //retractPose
                .actionTrajectory=traj,           //actionTrajectory
                .handedness=handedness   // Handedness
            },
            manipulation::core::ExecutableAction::Common{
            }};

        return pickAction;
    }

    manipulation::core::ExecutableAction PickAndPlaceExecutionComponent::preparePlace(const std::string side, const Eigen::Vector3f &placePositionGlobal)
    {
        ARMARX_TRACE;

        //get handedness
        manipulation::core::arondto::Handedness handedness;
        if (side == "Right")
        {
            handedness =manipulation::core::arondto::Handedness::Right;
        }
        else
        {
            handedness =manipulation::core::arondto::Handedness::Left;
        }

        //write PlacePosition to pose
        Eigen::Quaternionf quat(0,0,0,0);
        FramedPose pose(toIce(placePositionGlobal), toIce(quat), "root", robot.name);

   manipulation::core::ActionHypothesis hypothesis(
                    "place",
                    this->robot,    //RobotDescription
                    pose,           //pose
                    0,              //obsTime, 0 because it's constant
                    std::nullopt,   //poseUncertainty
                    handedness,     //handedness
                    std::nullopt    //existCert
                    );

        manipulation::core::ExecutableAction placeAction{
            manipulation::core::ExecutableAction::Unimanual{
                    .hypothesis=hypothesis,     //ActionHypothesis
                    .execPose=std::nullopt,     //execPose
                    .prePose=std::nullopt,   //prePose
                    .retractPose=std::nullopt,   //retractPose
                    .actionTrajectory=std::nullopt,   //actionTrajectory
                    .handedness=handedness   // Handedness
            },
            manipulation::core::ExecutableAction::Common{
            }};

        return placeAction;
    }

    ARMARX_REGISTER_COMPONENT_EXECUTABLE(PickAndPlaceExecutionComponent, PickAndPlaceExecutionComponent::GetDefaultName());

}
