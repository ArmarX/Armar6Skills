#include "PickAndPlaceController.h"

#include <SimoxUtility/algorithm/string/string_tools.h>

#include <VirtualRobot/math/LinearInterpolatedPose.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/time/DateTime.h>
#include <ArmarXCore/core/time/Clock.h>
#include <ArmarXCore/core/time/Metronome.h>

#include <RobotAPI/libraries/RobotStatechartHelpers/ForceTorqueHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/PositionControllerHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <armar6/skills/libraries/Armar6Tools/Armar6HandV2Helper.h>


// JSON conversions
void armarx::from_json(nlohmann::json const& j, GeneralConfig& config)
{
    j["IsSimulation"].get_to(config.IsSimulation);
    j["MaximalDistanceToStartInMM"].get_to(config.MaximalDistanceToStartInMM);
}


void armarx::from_json(nlohmann::json const& j, PreposeSingleConfig& config)
{
    config.Waypoints.clear();
    for (auto& jw : j["Waypoints"])
    {
        float qw = jw["qw"];
        float qx = jw["qx"];
        float qy = jw["qy"];
        float qz = jw["qz"];
        float x = jw["x"];
        float y = jw["y"];
        float z = jw["z"];

        Eigen::Matrix4f pose = simox::math::pose(Eigen::Vector3f(x, y, z),
                               Eigen::Quaternionf(qw, qx, qy, qz));

        config.Waypoints.push_back(pose);
    }
}


void armarx::from_json(nlohmann::json const& j, PreposeConfig& config)
{
    j["ControllerConfig"].get_to(config.ControllerConfig);
    j["Left"].get_to(config.Left);
    j["Right"].get_to(config.Right);
}


void armarx::from_json(nlohmann::json const& j, SupportConfig& config)
{
    j["Left"].get_to(config.Left);
    j["Right"].get_to(config.Right);
}


void armarx::from_json(nlohmann::json const& j, FramedPose& pose)
{
    j.at("agent").get_to(pose.agent);
    j.at("frame").get_to(pose.frame);
    if (!pose.position)
    {
        pose.position = new Vector3();
    }
    j.at("x").get_to(pose.position->x);
    j.at("y").get_to(pose.position->y);
    j.at("z").get_to(pose.position->z);
    if (!pose.orientation)
    {
        pose.orientation = new Quaternion();
    }
    j.at("qw").get_to(pose.orientation->qw);
    j.at("qx").get_to(pose.orientation->qx);
    j.at("qy").get_to(pose.orientation->qy);
    j.at("qz").get_to(pose.orientation->qz);
}


void armarx::from_json(nlohmann::json const& j, LocalizeSingleConfig& config)
{
    j["TcpPose"].get_to(config.TcpPose);
}


void armarx::from_json(nlohmann::json const& j, LocalizeConfig& config)
{
    j["Left"].get_to(config.Left);
    j["Right"].get_to(config.Right);
}


void armarx::from_json(nlohmann::json const&j, CartesianPositionControllerConfig &config)
{
    j["KpOri"].get_to(config.KpOri);
    j["KpPos"].get_to(config.KpPos);
    j["maxOriVel"].get_to(config.maxOriVel);
    j["maxPosVel"].get_to(config.maxPosVel);
    j["thresholdOrientationNear"].get_to(config.thresholdOrientationNear);
    j["thresholdOrientationReached"].get_to(config.thresholdOrientationReached);
    j["thresholdPositionNear"].get_to(config.thresholdPositionNear);
    j["thresholdPositionReached"].get_to(config.thresholdPositionReached);
}


void armarx::from_json(nlohmann::json const&j, RetreatConfig &config)
{
    j["ControllerConfig"].get_to(config.ControllerConfig);
}


void armarx::from_json(nlohmann::json const&j, PickConfig &config)
{
    j["ControllerConfig"].get_to(config.ControllerConfig);
}


void armarx::from_json(nlohmann::json const&j, PlaceConfig &config)
{
    j["ControllerConfig"].get_to(config.ControllerConfig);
}


void armarx::from_json(nlohmann::json const& j, PickAndPlaceConfig& config)
{
    j["General"].get_to(config.General);
    j["Prepose"].get_to(config.Prepose);
    j["Support"].get_to(config.Support);
    j["Localize"].get_to(config.Localize);
    j["Retreat"].get_to(config.Retreat);
    j["Pick"].get_to(config.Pick);
    j["Place"].get_to(config.Place);
}


namespace armarx
{
    PickAndPlaceController::PickAndPlaceController()
    {
    }


    PickAndPlaceController::PickAndPlaceController(const VirtualRobot::RobotPtr& robot,
            const RobotNameHelperPtr& robotNameHelper,
            const KinematicUnitInterfacePrx& kinematicUnit,
            const ObserverInterfacePrx& robotUnitObserver,
            const PickAndPlaceConfig& config,
            const RobotUnitInterfacePrx& robotUnit,
            const RobotStateComponentInterfacePrx& robotStatePrx,
            const ForceTorqueUnitObserverInterfacePrx& forceTorqueObserver,
            const DebugObserverHelper& debugObserverHelper,
            const objpose::ObjectPoseStorageInterfacePrx& objectPoseStorage) :
        robot(robot), robotNameHelper(robotNameHelper), kinematicUnit(kinematicUnit),
        robotUnitObserver(robotUnitObserver), config(config), robotUnit(robotUnit),
        robotStatePrx(robotStatePrx), forceTorqueObserver(forceTorqueObserver),
        debugObserverHelper(debugObserverHelper), objectPoseStorage(objectPoseStorage)
    {
    }


    void PickAndPlaceController::stopExecution()
    {
        this->stop = true;
    }


    void PickAndPlaceController::resetStop()
    {
        this->stop = false;
    }


    void PickAndPlaceController::setName(const std::string& name)
    {
        this->name = name;
    }


    std::string PickAndPlaceController::getName()
    {
        return name;
    }


    void PickAndPlaceController::executeRetreat(const Side& side)
    {
        checkProxies();
        RobotNameHelper::Arm arm = robotNameHelper->getArm(side_names.to_name(side));
        RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

        Armar6HandV2Helper handHelper(kinematicUnit, robotUnitObserver, side_names.to_name(side), config->General.IsSimulation);
        std::string controllerName = getName() + "_" + side_names.to_name(side);
        VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(robotUnit, arm.getKinematicChain(),
                                              controllerName));
        //ForceTorqueHelper ftHelper(getForceTorqueObserver(), arm.getForceTorqueSensor());

        // Initial sync to get the right values
        RemoteRobot::synchronizeLocalClone(robot, robotStatePrx);

        Eigen::Matrix4f initial_tcp_pose = tcp->getPoseInRootFrame();

        PositionControllerHelper posHelper(tcp, velHelper, initial_tcp_pose);
        posHelper.readConfig(config->Retreat.ControllerConfig);

        auto waypoints = side == Side::Left ? config->Prepose.Left.Waypoints : config->Prepose.Right.Waypoints;
        // For retreat motion only move TCP to the final prepose
        // This is used if we have executed an action and want to retreat back to the prepose
        posHelper.addWaypoint(waypoints.back());

        Eigen::Vector3f initialTcpPosition = simox::math::position(initial_tcp_pose);
        if (posHelper.waypoints.size() > 1)
        {
            Eigen::Vector3f start_position = simox::math::position(posHelper.waypoints[1]);
            Eigen::Vector3f tcp_to_start = start_position - initialTcpPosition;
            float distance_tcp_to_start_in_mm = tcp_to_start.norm();
            ARMARX_INFO << "Distance TCP <-> start: " << distance_tcp_to_start_in_mm << " mm";
            float maximalDistanceToRetreatInMM = 1400.0f;
            if (distance_tcp_to_start_in_mm > maximalDistanceToRetreatInMM)
            {
                ARMARX_WARNING << "Maximal distance between start and TCP exceeded: " << distance_tcp_to_start_in_mm
                               << " mm > " << maximalDistanceToRetreatInMM << " mm";
                ARMARX_WARNING << "Motion is not started. Are you in the right configuration to start this motion?";
                return;
            }
        }


        Metronome metronome(Duration::MilliSeconds(10));
        while (!stop)
        {
            RemoteRobot::synchronizeLocalClone(robot, robotStatePrx);

            posHelper.updateRead();

            bool targetReached = posHelper.isFinalTargetReached();

            if (targetReached)
            {
                posHelper.immediateHardStop();
                // TODO: Indicate success
                ARMARX_IMPORTANT << "Goal reached!";
                break;
            }

            posHelper.updateWrite();

            metronome.waitForNextTick();
        }

        handHelper.shapeHand(0.0f, 0.0f);

        posHelper.immediateHardStop();
        posHelper.updateWrite();
    }


    void PickAndPlaceController::executePrepose(const Side& side)
    {
        checkProxies();
        RobotNameHelper::Arm arm = robotNameHelper->getArm(side_names.to_name(side));
        RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

        Armar6HandV2Helper handHelper(kinematicUnit, robotUnitObserver, side_names.to_name(side), config->General.IsSimulation);
        std::string controllerName = getName() + "_" + side_names.to_name(side);
        VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(robotUnit, arm.getKinematicChain(),
                                              controllerName));
        //ForceTorqueHelper ftHelper(getForceTorqueObserver(), arm.getForceTorqueSensor());

        // Initial sync to get the right values
        RemoteRobot::synchronizeLocalClone(robot, robotStatePrx);

        Eigen::Matrix4f initial_tcp_pose = tcp->getPoseInRootFrame();

        PositionControllerHelper posHelper(tcp, velHelper, initial_tcp_pose);
        posHelper.readConfig(config->Prepose.ControllerConfig);

        auto waypoints = side == Side::Left ? config->Prepose.Left.Waypoints : config->Prepose.Right.Waypoints;
        for (auto& waypoint : waypoints)
        {
            posHelper.addWaypoint(waypoint);
        }

        Eigen::Vector3f initialTcpPosition = simox::math::position(initial_tcp_pose);
        if (posHelper.waypoints.size() > 1)
        {
            Eigen::Vector3f start_position = simox::math::position(posHelper.waypoints[1]);
            Eigen::Vector3f tcp_to_start = start_position - initialTcpPosition;
            float distance_tcp_to_start_in_mm = tcp_to_start.norm();
            ARMARX_INFO << "Distance TCP <-> start: " << distance_tcp_to_start_in_mm << " mm";
            if (distance_tcp_to_start_in_mm > config->General.MaximalDistanceToStartInMM)
            {
                ARMARX_WARNING << "Maximal distance between start and TCP exceeded: " << distance_tcp_to_start_in_mm
                               << " mm > " << config->General.MaximalDistanceToStartInMM << " mm";
                ARMARX_WARNING << "Motion is not started. Are you in the right configuration to start this motion?";
                return;
            }
        }


        Metronome metronome(Duration::MilliSeconds(20));
        while (!stop)
        {
            RemoteRobot::synchronizeLocalClone(robot, robotStatePrx);

            posHelper.updateRead();

            bool targetReached = posHelper.isFinalTargetReached();

            if (targetReached)
            {
                posHelper.immediateHardStop();
                // TODO: Indicate success
                ARMARX_IMPORTANT << "Goal reached!";
                break;
            }

            posHelper.updateWrite();

            metronome.waitForNextTick();
        }

        posHelper.immediateHardStop();
        posHelper.updateWrite();
    }


    void PickAndPlaceController::executePreposeReverse(const Side& side)
    {
        checkProxies();

        RobotNameHelper::Arm arm = robotNameHelper->getArm(side_names.to_name(side));
        RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

        Armar6HandV2Helper handHelper(kinematicUnit, robotUnitObserver, side_names.to_name(side), config->General.IsSimulation);
        std::string controllerName = getName() + "_" + side_names.to_name(side);
        VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(robotUnit, arm.getKinematicChain(),
                                              controllerName));
        //ForceTorqueHelper ftHelper(getForceTorqueObserver(), arm.getForceTorqueSensor());

        Eigen::Matrix4f initialTcpPose = tcp->getPoseInRootFrame();

        PositionControllerHelper posHelper(tcp, velHelper, initialTcpPose);
        posHelper.readConfig(config->Prepose.ControllerConfig);

        auto waypoints = side == Side::Left ? config->Prepose.Left.Waypoints : config->Prepose.Right.Waypoints;
        std::reverse(waypoints.begin(), waypoints.end());
        for (auto& waypoint : waypoints)
        {
            posHelper.addWaypoint(waypoint);
        }

        // Initial sync to get the right values
        RemoteRobot::synchronizeLocalClone(robot, robotStatePrx);

        Eigen::Vector3f initialTcpPosition = simox::math::position(initialTcpPose);
        if (posHelper.waypoints.size() > 1)
        {
            Eigen::Vector3f start_position = simox::math::position(posHelper.waypoints[1]);
            Eigen::Vector3f tcp_to_start = start_position - initialTcpPosition;
            float distance_tcp_to_start_in_mm = tcp_to_start.norm();
            ARMARX_INFO << "Distance TCP <-> start: " << distance_tcp_to_start_in_mm << " mm";
            if (distance_tcp_to_start_in_mm > config->General.MaximalDistanceToStartInMM)
            {
                ARMARX_WARNING << "Maximal distance between start and TCP exceeded: " << distance_tcp_to_start_in_mm
                               << " mm > " << config->General.MaximalDistanceToStartInMM << " mm";
                ARMARX_WARNING << "Motion is not started. Are you in the right configuration to start this motion?";
                return;
            }
        }


        Metronome metronome(Duration::MilliSeconds(10));
        while (!stop)
        {
            RemoteRobot::synchronizeLocalClone(robot, robotStatePrx);

            posHelper.updateRead();

            bool targetReached = posHelper.isFinalTargetReached();

            if (targetReached)
            {
                posHelper.immediateHardStop();
                // TODO: Indicate success
                ARMARX_IMPORTANT << "Goal reached!";
                break;
            }

            posHelper.updateWrite();

            metronome.waitForNextTick();
        }

        posHelper.immediateHardStop();
        posHelper.updateWrite();
    }


    void PickAndPlaceController::executePush()
    {
        ARMARX_WARNING << "Not yet implemented";
    }


    PickAndPlaceController::ExecutePickOutput
    PickAndPlaceController::executePick(
        const Side& side, const grasping::GraspCandidatePtr& grasp, const Eigen::Vector3f& liftVector,
        const std::map<std::string, Armar6GraspTrajectoryPtr>& graspTrajectories,
        objpose::ObjectPoseSeq& objectPoses, std::mutex& objectPoseMutex)
    {
        ARMARX_TRACE;
        ExecutePickOutput output;

        checkProxies();

        if (!grasp)
        {
            ARMARX_WARNING << "No grasp selected!";
            return output;
        }
        if (!grasp->reachabilityInfo->reachable)
        {
            ARMARX_WARNING << "Grasp is not reachable!";
            return output;
        }

        if (side == Side::Left)
        {
            //            ARMARX_WARNING << "Picking with the left hand is not yet implemented.";
            //            return;
        }

        RobotNameHelper::Arm arm = robotNameHelper->getArm(side_names.to_name(side));
        RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

        Armar6HandV2Helper handHelper(kinematicUnit, robotUnitObserver, side_names.to_name(side), config->General.IsSimulation);
        std::string controllerName = getName() + "_" + side_names.to_name(side);
        VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(robotUnit, arm.getKinematicChain(),
                                              controllerName));

        ForceTorqueHelper ftHelper(forceTorqueObserver, arm.getForceTorqueSensor());

        RemoteRobot::synchronizeLocalClone(robot, robotStatePrx);
        // BEGIN
        GraspProcedureType graspProcedure = GraspProcedureType::Trajectory;

        // TODO: This needs to be configurable


        Armar6GraspTrajectoryPtr graspTrajectory;
        try
        {
            graspTrajectory = graspTrajectories.at(grasp->executionHints->graspTrajectoryName);
        }
        catch (const std::out_of_range&)
        {
            ARMARX_ERROR << "Failed to load trajectory '" << grasp->executionHints->graspTrajectoryName << "'"
                         << "\n" << "Known are: " << simox::alg::get_keys(graspTrajectories);
            return output;
        }
        ARMARX_INFO << "Loaded trajectory: " << grasp->executionHints->graspTrajectoryName;

        ARMARX_IMPORTANT << "Read grasp trajectory, length: " << graspTrajectory->calculateLength().pos
                         << ", duration: " << graspTrajectory->getDuration();
        handHelper.shapeHand(graspTrajectory->GetHandValues(0));

        // TODO: Make configurable
        std::string graspType = "Top";
        Eigen::Vector3f graspPositionOffset(0.0f, 0.0f, 0.0f);
        float minimumFingerShape = 0.0f;
        if (grasp->executionHints->graspTrajectoryName == "RightSideOpen")
        {
            graspType = "Side";

            // Do some preshaping
            minimumFingerShape = 0.30f;
            graspPositionOffset = Eigen::Vector3f(0.0f, 10.0f, 0.0f);
            if (side == Side::Left)
            {
                graspPositionOffset(0) = -10.0f;
            }
            else
            {
                handHelper.shapeHand(minimumFingerShape, 0.0f);
            }
        }

        Eigen::Vector3f approachVector = Vector3Ptr::dynamicCast(grasp->approachVector)->toEigen();
        float approachDistance = 120.0f;
        Eigen::Matrix4f graspPose = PosePtr::dynamicCast(grasp->graspPose)->toEigen();
        math::Helpers::Position(graspPose) = math::Helpers::Position(graspPose) + graspPositionOffset;
        Eigen::Matrix4f prePose1 = math::Helpers::TranslatePose(graspPose, approachVector * approachDistance);
        Eigen::Matrix4f prePose2 = math::Helpers::TranslatePose(graspPose, approachVector * approachDistance / 2);
        // TODO: Let's do placing later
        //Eigen::Matrix4f placePose = in.getPlacePose()->toEigen();



        std::vector<Eigen::Matrix4f> waypoints;

        math::LinearInterpolatedPose linPose(tcp->getPoseInRootFrame(), prePose1, 0, 1, true);
        waypoints.push_back(linPose.Get(0.5f));
        waypoints.push_back(prePose1);
        waypoints.push_back(prePose2);

        ARMARX_INFO << VAROUT(waypoints);
        ARMARX_INFO << VAROUT(graspPose);

        // END

        PositionControllerHelper posHelper(tcp, velHelper, waypoints);
        posHelper.readConfig(config->Pick.ControllerConfig);

        //        // Generate a grasp pose for the object
        //        Eigen::Matrix4f objectPoseRobot = PosePtr::dynamicCast(control.objectPose.objectPoseRobot)->toEigen();
        //        ARMARX_INFO << "Trying to pick up object: " << control.objectPose.objectID.name;
        //        ARMARX_INFO << "At pose: " << objectPoseRobot;

        //        Eigen::Matrix4f graspPoseRobot = PosePtr::dynamicCast(control.grasp->graspPose)->toEigen();
        //        posHelper.addWaypoint(graspPoseRobot);

        float defaultMaxPosVel = posHelper.posController.maxPosVel;

        PhaseType currentPhase = PhaseType::ApproachPrePose;

        DateTime graspStartTime;

        Metronome metronome(Duration::MilliSeconds(10));
        while (!stop)
        {
            RemoteRobot::synchronizeLocalClone(robot, robotStatePrx);

            posHelper.updateRead();

            // BEGIN

            float positionError = posHelper.getPositionError();
            float orientationError = posHelper.getOrientationError();
            //ARMARX_INFO << deactivateSpam(1) << VAROUT(positionError) << " " << VAROUT(orientationError) << " " << VAROUT(cv.transpose());

            debugObserverHelper->setChannelName("CartesianPositionControl");
            debugObserverHelper->setDebugObserverDatafield("positionError", positionError);
            debugObserverHelper->setDebugObserverDatafield("orientationError", orientationError);
            debugObserverHelper->sendDebugObserverBatch();

            PhaseType nextPhase = currentPhase;
            bool targetReached = posHelper.isFinalTargetReached();
            bool targetNear = posHelper.isFinalTargetNear();
            (void) targetNear;

            const Eigen::Vector3f currentForce = ftHelper.getForce();
            const Eigen::Vector3f currentTorque = ftHelper.getTorque();


            debugObserverHelper->setChannelName("ExecuteGrasp");
            debugObserverHelper->setDebugObserverDatafield("force_x", currentForce(0));
            debugObserverHelper->setDebugObserverDatafield("force_y", currentForce(1));
            debugObserverHelper->setDebugObserverDatafield("force_z", currentForce(2));
            debugObserverHelper->setDebugObserverDatafield("torque_x", currentTorque(0));
            debugObserverHelper->setDebugObserverDatafield("torque_y", currentTorque(1));
            debugObserverHelper->setDebugObserverDatafield("torque_z", currentTorque(2));
            debugObserverHelper->sendDebugObserverBatch();


            bool forceExceeded = currentForce.norm() > config->General.ForceThresholdInPotatoes;
            Eigen::Vector3f tcpPos = tcp->getPositionInRootFrame();
            Eigen::Matrix4f tcpPose = tcp->getPoseInRootFrame();
            if (config->General.IsSimulation && tcpPos(2) < config->General.SimulationForceHeightThresholdInMM)
            {
                ARMARX_IMPORTANT << "Simulating force exceeded";
                forceExceeded = true;
            }

            if (currentPhase == PhaseType::ApproachPrePose)
            {
                //getMessageDisplay()->setMessage("Approaching Pre Pose", posHelper.getStatusText());
                if (targetReached)
                {
                    ARMARX_IMPORTANT << "PrePose reached";
                    if (graspType == "Top")
                    {
                        Eigen::Vector3f TopGraspingVector = Eigen::Vector3f(0.0f, 0.0f, -100.0f);
                        ARMARX_IMPORTANT << "graspType = Top: Adding top grasping vector";
                        posHelper.setTarget(math::Helpers::TranslatePose(graspPose, TopGraspingVector));
                        posHelper.posController.maxPosVel = defaultMaxPosVel / 2;
                        ARMARX_IMPORTANT << "setting posController.maxPosVel = " << posHelper.posController.maxPosVel;
                    }
                    else
                    {
                        posHelper.setTarget(graspPose);
                    }
                    velHelper->controller->setKpJointLimitAvoidance(0);
                    velHelper->controller->setJointLimitAvoidanceScale(0);
                    ftHelper.setZero();
                    nextPhase = PhaseType::ApproachObject;
                }
            }
            if (currentPhase == PhaseType::ApproachObject)
            {
                //getMessageDisplay()->setMessage("Approaching Object", posHelper.getStatusText());

                if (forceExceeded || targetReached)
                {
                    posHelper.immediateHardStop();
                    posHelper.posController.maxPosVel = defaultMaxPosVel;
                    graspStartTime = DateTime::Now();

                    if (graspProcedure == GraspProcedureType::Simple)
                    {
                        nextPhase = PhaseType::SimpleGrasp;
                        Eigen::Matrix4f target = tcp->getPoseInRootFrame();
                        //target(2, 3) = fmax(target(2, 3), in.getGraspMinZHeight());
                        Eigen::Vector3f offsetDuringGrasp(0.0f, 0.0f, 0.0f);
                        target = ::math::Helpers::TranslatePose(target, offsetDuringGrasp);
                        posHelper.setTarget(target);

                    }
                    else if (graspProcedure == GraspProcedureType::Trajectory)
                    {
                        nextPhase = PhaseType::TrajectoryGrasp;

                        Eigen::Matrix4f startPose = graspTrajectory->getStartPose();
                        Eigen::Vector3f currentHandForward = ::math::Helpers::TransformDirection(tcpPose,
                                                             handHelper.getHandForwardVector());
                        Eigen::Vector3f trajHandForward = ::math::Helpers::TransformDirection(startPose,
                                                          handHelper.getHandForwardVector());
                        Eigen::Vector3f up(0, 0, 1);
                        //                        if (control.side == "Left")
                        //                        {
                        //                            up = Eigen::Vector3f(0, 0, -1);
                        //                        }

                        float angle = ::math::Helpers::Angle(currentHandForward, trajHandForward, up);
                        ARMARX_IMPORTANT << "Angle: " << angle;
                        Eigen::AngleAxisf aa(angle, up);

                        Eigen::Matrix4f transform = ::math::Helpers::CreateTranslationRotationTranslationPose(
                                                        -math::Helpers::GetPosition(startPose), aa.toRotationMatrix(),
                                                        math::Helpers::GetPosition(tcpPose));
                        graspTrajectory = graspTrajectory->getTransformed(transform);
                        //                        if (control.side == "Left")
                        //                        {
                        //                            for (int i = 0; i < (int)graspTrajectory->getKeypointCount(); ++i)
                        //                            {
                        //                                Armar6GraspTrajectory::Keypoint& kp = *graspTrajectory->getKeypoint(i);
                        //                                Eigen::Matrix4f target = kp.tcpTarget;
                        //                                target.block<3, 3>(0, 0) = Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitY()) * target.block<3, 3>(0, 0);
                        //                            }
                        //                            graspTrajectory->updateKeypointMap();
                        //                        }
                    }
                    else
                    {
                        throw LocalException("Unsupported GraspProcedureType");
                    }

                }
            }
            if (currentPhase == PhaseType::TrajectoryGrasp)
            {
                DateTime now = DateTime::Now();
                float t = (now - graspStartTime).toSecondsDouble();
                float duration = graspTrajectory->getDuration();
                if (t <= duration)
                {
                    Eigen::Matrix4f target = graspTrajectory->GetPose(t);
                    posHelper.setTarget(target);
                    posHelper.setFeedForwardVelocity(graspTrajectory->GetPositionDerivative(t),
                                                     graspTrajectory->GetOrientationDerivative(t));
                    Eigen::Vector3f handShape = graspTrajectory->GetHandValues(t);
                    handShape(0) = std::max(handShape(0), minimumFingerShape);
                    handHelper.shapeHand(handShape);
                }
                else
                {
                    t = graspTrajectory->getDuration();
                    nextPhase = PhaseType::Lift;
                    {
                        std::unique_lock lock(objectPoseMutex);
                        attachObjectToRobotPose(grasp->sourceInfo->referenceObjectName, tcp, objectPoses);
                        //attachObjectToRobotPose(objectPose, tcp, objectPoses);
                    }
                    Eigen::Matrix4f target = tcp->getPoseInRootFrame();
                    target.block<3, 1>(0, 3) += liftVector;
                    posHelper.setTarget(target);
                }
            }
            if (currentPhase == PhaseType::Lift)
            {
                if (targetReached)
                {
                    posHelper.immediateHardStop();
                    break;
                }
            }
            currentPhase = nextPhase;


            // END

            posHelper.updateWrite();

            metronome.waitForNextTick();
        }

        posHelper.immediateHardStop();
        posHelper.updateWrite();

        output.executed = true;

        return output;
    }


    void PickAndPlaceController::executePlace(const Side& side, const Eigen::Vector3f& placePositionGlobal,
            objpose::ObjectPoseSeq& objectPoses, std::mutex& objectPoseMutex)
    {
        checkProxies();

        RobotNameHelper::Arm arm = robotNameHelper->getArm(side_names.to_name(side));
        RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

        Armar6HandV2Helper handHelper(kinematicUnit, robotUnitObserver, side_names.to_name(side), config->General.IsSimulation);
        std::string controllerName = getName() + "_" + side_names.to_name(side);
        VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(robotUnit, arm.getKinematicChain(),
                                              controllerName));
        ForceTorqueHelper ftHelper(forceTorqueObserver, arm.getForceTorqueSensor());

        RemoteRobot::synchronizeLocalClone(robot, robotStatePrx);
        Eigen::Matrix4f robotPoseGlobal = robot->getGlobalPose();

        // BEGIN

        // END

        Eigen::Matrix4f initialTcpPose = tcp->getPoseInRootFrame();
        PositionControllerHelper posHelper(tcp, velHelper, initialTcpPose);
        posHelper.readConfig(config->Place.ControllerConfig);

        PhaseType currentPhase = PhaseType::Replace;

        Eigen::Matrix4f startPose = tcp->getPoseInRootFrame();
        // Make an intermediate pose which only moves in x,y not z
        Eigen::Vector3f placePositionRobot = (robotPoseGlobal.inverse() * placePositionGlobal.homogeneous()).head<3>();
        Eigen::Matrix4f intermediatePose = startPose;
        intermediatePose(0, 3) = placePositionRobot(0);
        intermediatePose(1, 3) = placePositionRobot(1);
        Eigen::Matrix4f placePose = intermediatePose;
        placePose(2, 3) = placePositionRobot(2);
        math::Helpers::Position(placePose) = placePositionRobot;

        std::vector<Eigen::Matrix4f> placeWaypoints;
        placeWaypoints.push_back(startPose);
        placeWaypoints.push_back(intermediatePose);
        //placeWaypoints.push_back(placePose);

        posHelper.setNewWaypoints(placeWaypoints);

        Metronome metronome(Duration::MilliSeconds(10));
        while (!stop)
        {
            RemoteRobot::synchronizeLocalClone(robot, robotStatePrx);

            posHelper.updateRead();

            // BEGIN

            float positionError = posHelper.getPositionError();
            float orientationError = posHelper.getOrientationError();
            //ARMARX_INFO << deactivateSpam(1) << VAROUT(positionError) << " " << VAROUT(orientationError) << " " << VAROUT(cv.transpose());

            debugObserverHelper->setChannelName("CartesianPositionControl");
            debugObserverHelper->setDebugObserverDatafield("positionError", positionError);
            debugObserverHelper->setDebugObserverDatafield("orientationError", orientationError);
            debugObserverHelper->sendDebugObserverBatch();


            PhaseType nextPhase = currentPhase;
            bool targetReached = posHelper.isFinalTargetReached();
            bool targetNear = posHelper.isFinalTargetNear();

            const Eigen::Vector3f currentForce = ftHelper.getForce();
            const Eigen::Vector3f currentTorque = ftHelper.getTorque();

            debugObserverHelper->setChannelName("ExecuteGrasp");
            debugObserverHelper->setDebugObserverDatafield("force_x", currentForce(0));
            debugObserverHelper->setDebugObserverDatafield("force_y", currentForce(1));
            debugObserverHelper->setDebugObserverDatafield("force_z", currentForce(2));
            debugObserverHelper->setDebugObserverDatafield("torque_x", currentTorque(0));
            debugObserverHelper->setDebugObserverDatafield("torque_y", currentTorque(1));
            debugObserverHelper->setDebugObserverDatafield("torque_z", currentTorque(2));
            debugObserverHelper->sendDebugObserverBatch();


            bool forceExceeded = currentForce.norm() > config->General.ForceThresholdInPotatoes;
            Eigen::Vector3f tcpPos = tcp->getPositionInRootFrame();
            if (config->General.IsSimulation && tcpPos(2) < config->General.SimulationForceHeightThresholdInMM)
            {
                ARMARX_IMPORTANT << "Simulating force exceeded";
                forceExceeded = true;
            }

            if (currentPhase == PhaseType::Replace)
            {
                //getMessageDisplay()->setMessage("Replacing", "");
                if (targetNear)
                {
                    ftHelper.setZero();
                    // TODO: Do we need to place it further down (offset) to trigger the force reaction?
                    posHelper.setTarget(placePose);
                    nextPhase = PhaseType::Place;
                }
            }
            if (currentPhase == PhaseType::Place)
            {
                {
                    //                    std::stringstream ss;
                    //                    ss << "Force: " << currentForce.norm();
                    //                    getMessageDisplay()->setMessage("Placing", ss.str());
                }
                if (forceExceeded || targetReached)
                {
                    posHelper.immediateHardStop();
                    handHelper.shapeHand(0, 0);
                    // TODO: Set waypoints here!
                    posHelper.setTarget(math::Helpers::TranslatePose(placePose, Eigen::Vector3f(0, 0, 300)));
                    // TODO: Where should we retract to after placing?
                    //posHelper.addWaypoint(in.getRetractAfterPlacePose()->toEigen());
                    //posHelper.addWaypoint(in.getFinalPrePose()->toEigen());
                    //posHelper.addWaypoint(in.getFinalPose()->toEigen());
                    // TODO: Are these adjustments necessary?
                    //                    velHelper->controller->setJointLimitAvoidanceScale(2);
                    //                    velHelper->controller->setKpJointLimitAvoidance(1);
                    nextPhase = PhaseType::RetractAfterPlace;

                    {
                        std::unique_lock lock(objectPoseMutex);
                        detachAllObjects(objectPoses);
                    }

                }
            }
            if (currentPhase == PhaseType::RetractAfterPlace)
            {
                //                getMessageDisplay()->setMessage("Move back", posHelper.getStatusText());
                if (targetReached)
                {
                    posHelper.immediateHardStop();
                    break;
                }
            }

            currentPhase = nextPhase;


            // END

            posHelper.updateWrite();

            metronome.waitForNextTick();
        }

        posHelper.immediateHardStop();
        posHelper.updateWrite();
    }


    objpose::ObjectPoseSeq PickAndPlaceController::executeLocalizeInHand(const Side& side, const ObjectID& objectID,
            const objpose::ObjectPoseSeq& objectPoses, std::mutex& objectPoseMutex, int initialWaitMS,
            float maxDistanceFromTcp, int minPoseUpdates, bool moveHead, float pollFrequency, int timeoutMS)
    {
        checkProxies();
        ARMARX_CHECK(headIK) << "HeadIK must be set before Execution!";

        if (initialWaitMS > 0)
        {
            Clock::WaitFor(Duration::MilliSeconds(initialWaitMS));
        }

        RemoteRobot::synchronizeLocalClone(robot, robotStatePrx);

        RobotNameHelper::RobotArm arm = robotNameHelper->getRobotArm(side_names.to_name(side), robot);
        // VirtualRobot::RobotNodeSetPtr rns = arm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = arm.getTCP();

        if (moveHead)
        {
            std::string nodeSetName = "IKVirtualGaze";  // "Head";
            FramedPositionPtr targetPose = new FramedPosition(tcp->getPoseInRootFrame(), robot->getRootNode()->getName(),
                    robot->getName());
            headIK->setHeadTarget(nodeSetName, targetPose);
            Clock::WaitFor(Duration::MilliSeconds(1000));
        }


        DateTime start = DateTime::Now();
        Duration timeout = Duration::MilliSeconds(timeoutMS);

        Metronome metronome(Frequency::HertzDouble(pollFrequency));

        ARMARX_IMPORTANT << "Start in-hand localization of object " << objectID
                         << "\n- side: " << side_names.to_name(side)
                         << "\n- start time: " << start
                         << "\n- timeout: " << timeout
                         << "\n- poll frequency: " << pollFrequency << " Hz"
                         << "\n- necessary pose updates: " << minPoseUpdates << " Hz"
                         << "\n- max distance from TCP: " << maxDistanceFromTcp << " mm"
                         ;

        objpose::ObjectPoseSeq acceptedObjectPoses;
        DateTime latestUpdate = start;

        DateTime now = start;
        while (timeoutMS > 0 && (now - start) < timeout
               && int(acceptedObjectPoses.size()) < minPoseUpdates)
        {
            RemoteRobot::synchronizeLocalClone(robot, robotStatePrx);
            std::optional<objpose::ObjectPose> pose;
            {
                std::unique_lock lock(objectPoseMutex);
                for (const objpose::ObjectPose& p : objectPoses)
                {
                    if (p.objectID == objectID)
                    {
                        pose = p;
                        break;
                    }
                }
            }
            if (pose)
            {
                bool accept = true;
                if (pose->timestamp <= latestUpdate)
                {
                    ARMARX_INFO << "Rejecting pose due to being too old.\n"
                                << pose->timestamp << " >= " << latestUpdate;
                    accept = false;
                }
                if (accept && maxDistanceFromTcp >= 0)
                {
                    // Check distance of object to TCP.
                    Eigen::Vector3f tcpPos = simox::math::position(tcp->getPoseInRootFrame());
                    Eigen::Vector3f objectPos;
                    if (auto oobb = pose->oobbRobot(); oobb)
                    {
                        objectPos = oobb->center();
                    }
                    else
                    {
                        objectPos = simox::math::position(pose->objectPoseRobot);
                    }

                    if ((objectPos - tcpPos).norm() > maxDistanceFromTcp)
                    {
                        ARMARX_INFO << "Rejecting pose due to being too far away from TCP.\n"
                                    << "|" << objectPos.transpose() << " - " << tcpPos << "| >= " << maxDistanceFromTcp;
                        accept = false;
                    }
                }

                if (accept)
                {
                    ARMARX_INFO << "Accepting pose."
                                << " (" << acceptedObjectPoses.size() + 1 << " of " << minPoseUpdates << ")"
                                << " | Timestamp: " << pose->timestamp;
                    acceptedObjectPoses.push_back(*pose);
                    latestUpdate = pose->timestamp;
                }
            }
            metronome.waitForNextTick();
            now = DateTime::Now();
        };

        return acceptedObjectPoses;
    }


    void PickAndPlaceController::attachObjectToRobotPose(const std::string &object,
            const VirtualRobot::RobotNodePtr& node,
            objpose::ObjectPoseSeq& objectPoses)
    {
        objpose::ObjectPose* info = nullptr;


        for (auto& objectInfo : objectPoses)
        {
            if (objectInfo.objectID.str() == object)
            {
                info = &objectInfo;
                break;
            }
        }

        if (info == nullptr)
        {
            ARMARX_WARNING << "Object info is unknwon";
            return;
        }

        if (!info->attachment)
        {
            info->attachment = objpose::ObjectAttachmentInfo();
        }
        info->attachment->agentName = robot->getName();
        info->attachment->frameName = node->getName();
        info->attachment->poseInFrame = node->getGlobalPose().inverse() * info->objectPoseGlobal;

        objpose::AttachObjectToRobotNodeInput input{};

        PoseBasePtr poseInFrame;
        toIce(poseInFrame, node->getGlobalPose().inverse() * info->objectPoseGlobal);

        input.objectID = {info->objectID.dataset(), info->objectID.className(), info->objectID.instanceName()};
        input.frameName = node->getName();
        input.agentName = robot->getName();


        objectPoseStorage->attachObjectToRobotNode(input);
    }


    void PickAndPlaceController::detachAllObjects(objpose::ObjectPoseSeq& objectPoses)
    {
        for (auto& objectInfo : objectPoses)
        {
            if (objectInfo.attachment && !objectInfo.attachment->agentName.empty() && !objectInfo.attachment->frameName.empty())
            {
                auto attachedToNode = robot->getRobotNode(objectInfo.attachment->frameName);
                objectInfo.objectPoseGlobal = attachedToNode->getGlobalPose() * objectInfo.attachment->poseInFrame;
                objectInfo.attachment->frameName = "";
                objectInfo.attachment->agentName = "";
            }
        }
        objpose::DetachAllObjectsFromRobotNodesInput input;
        objectPoseStorage->detachAllObjectsFromRobotNodes(input);
    }


    void PickAndPlaceController::checkProxies()
    {
        ARMARX_CHECK(robot) << "Robot must be set before Execution!";
        ARMARX_CHECK(robotNameHelper) << "RobotNameHelper must be set before Execution!";
        ARMARX_CHECK(kinematicUnit) << "KinematicUnit must be set before Execution!";
        ARMARX_CHECK(robotUnitObserver) << "RobotUnitObserver must be set before Execution!";
        ARMARX_CHECK(robotUnit) << "RobotUnit must be set before Execution!";
        ARMARX_CHECK(robotStatePrx) << "RobotStatePrx must be set before Execution!";
        ARMARX_CHECK(forceTorqueObserver) << "ForceTorqueObserver must be set before Execution!";
        ARMARX_CHECK(debugObserverHelper) << "DebugObserver must be set before Execution!";
        ARMARX_CHECK(config) << "Config must be set before Execution!";
    }


    void PickAndPlaceController::setDebugObserverHelper(const std::optional<DebugObserverHelper>& value)
    {
        debugObserverHelper = value;
    }


    void PickAndPlaceController::setObjectPoseStorage(const objpose::ObjectPoseStorageInterfacePrx& value)
    {
        objectPoseStorage = value;
    }


    void PickAndPlaceController::setHeadIK(const HeadIKUnitInterfacePrx& value)
    {
        headIK = value;
    }


    void PickAndPlaceController::setConfig(const PickAndPlaceConfig& value)
    {
        config = value;
    }

    /*
        void PickAndPlaceController::setDebugObserver(const DebugObserverInterfacePrx& value)
        {
            debugObserver = value;
        }
    */

    void PickAndPlaceController::setForceTorqueObserver(const ForceTorqueUnitObserverInterfacePrx& value)
    {
        forceTorqueObserver = value;
    }


    void PickAndPlaceController::setRobotStatePrx(const RobotStateComponentInterfacePrx& value)
    {
        robotStatePrx = value;
    }


    void PickAndPlaceController::setRobotUnit(const RobotUnitInterfacePrx& value)
    {
        robotUnit = value;
    }


    void PickAndPlaceController::setRobotUnitObserver(const ObserverInterfacePrx& value)
    {
        robotUnitObserver = value;
    }


    void PickAndPlaceController::setKinematicUnit(const KinematicUnitInterfacePrx& value)
    {
        kinematicUnit = value;
    }


    void PickAndPlaceController::setRobotNameHelper(const RobotNameHelperPtr& value)
    {
        robotNameHelper = value;
    }


    void PickAndPlaceController::setRobot(const VirtualRobot::RobotPtr& value)
    {
        robot = value;
    }

}

