#include "BoxGraspCandidatePlanner.h"

#include <armar6/skills/libraries/Armar6Tools/BimanualTransformationHelper.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <VirtualRobot/math/Helpers.h>

#include <RobotAPI/components/ArViz/Client/Elements.h>
#include <RobotAPI/components/ArViz/Client/Layer.h>

namespace armarx
{
    BoxGraspCandidatePlanner::BoxGraspCandidatePlanner()
    {

    }

    BoxGraspCandidatePlanner::BoxGraspCandidatePlanner(const viz::Client& arviz):
        arviz(arviz)
    {

    }

    std::string BoxGraspCandidatePlanner::getName()
    {
        return name;
    }

    void BoxGraspCandidatePlanner::setName(const std::string& name)
    {
        this->name = name;
    }

    GraspDescription BoxGraspCandidatePlanner::calculateGraspPoseSide(const Eigen::Vector3f& preferredForward,
            const Eigen::Matrix3f& referenceOriSide)
    {

        Eigen::Vector3f graspForwardVector = v1;
        Eigen::Vector3f offset_v1 = 0.95f * robotExtend1; // Was 95 percentile

        if (graspForwardVector.dot(preferredForward) < 0)
        {
            graspForwardVector = -graspForwardVector;
            offset_v1 = -0.95f * robotExtend1;
        }
        if (!isRightSide)
        {
            //graspForwardVector *= -1.0f;
            offset_v1 *= -1.0f;
        }

        float angleV1 = math::Helpers::Angle(Eigen::Vector2f(graspForwardVector(0), graspForwardVector(1)));
        float angleGraspOri = math::Helpers::Angle(Eigen::Vector2f(1, 0));
        float rotationAngle = math::Helpers::AngleModPI(angleV1 - angleGraspOri);
        Eigen::Matrix3f referenceOrientation = referenceOriSide;
        Eigen::AngleAxisf aa = Eigen::AngleAxisf(rotationAngle, Eigen::Vector3f::UnitZ());
        Eigen::Matrix3f graspOri = aa.toRotationMatrix() * referenceOrientation;

        // Move the grasp a bit down, so that the thumb can actually hit the object
        Eigen::Vector3f offsetDown = 5.0f * v3;
        Eigen::Vector3f graspPos = robotCenter + offset_v1 + offsetDown;

        GraspDescription grasp;
        grasp.pose = math::Helpers::CreatePose(graspPos, graspOri);
        grasp.approach = graspForwardVector.normalized();
        if (!isRightSide)
        {
            grasp.approach *= -1.0f;
        }
        grasp.preshape = grasping::OpenAperture;

        return grasp;

    }

    GraspDescription BoxGraspCandidatePlanner::calculateGraspPoseTop(const Eigen::Vector3f& preferredForward,
            const Eigen::Vector3f& graspOffsetTCP,
            const Eigen::Matrix3f& referenceOriTop)
    {
        Eigen::Vector3f graspForwardVector = v2;

        grasping::ApertureType preshape = grasping::OpenAperture;

        if (graspForwardVector.dot(preferredForward) < 0)
        {
            graspForwardVector = -graspForwardVector;
        }
        float angleV2 = math::Helpers::Angle(Eigen::Vector2f(graspForwardVector(0), graspForwardVector(1)));
        float angleGraspOri = math::Helpers::Angle(Eigen::Vector2f(0, 1));
        float rotationAngle = math::Helpers::AngleModPI(angleV2 - angleGraspOri);
        // TODO: Handle left as well
        Eigen::Matrix3f referenceOrientation = referenceOriTop;
        Eigen::AngleAxisf aa = Eigen::AngleAxisf(rotationAngle, Eigen::Vector3f::UnitZ());
        Eigen::Matrix3f graspOri = aa.toRotationMatrix() * referenceOrientation;

        Eigen::Vector3f graspPos = robotCenter + robotExtend3 + graspOri * graspOffsetTCP;



        GraspDescription grasp;
        grasp.pose = math::Helpers::CreatePose(graspPos, graspOri);
        grasp.approach = Eigen::Vector3f::UnitZ();
        grasp.preshape = preshape;

        return grasp;
    }

    void BoxGraspCandidatePlanner::commitExtendsToArviz(const Eigen::Matrix4f& robotPoseGlobal)
    {
        Eigen::Matrix3f robotRotGlobal = robotPoseGlobal.block<3, 3>(0, 0);
        Eigen::Vector3f globalCenter = (robotPoseGlobal * robotCenter.homogeneous()).head<3>();
        Eigen::Vector3f globalExtend1 = (robotRotGlobal * robotExtend1);
        Eigen::Vector3f globalExtend2 = (robotRotGlobal * robotExtend2);
        Eigen::Vector3f globalExtend3 = (robotRotGlobal * robotExtend3);
        viz::Layer bbLayer = arviz->layer("BoundingBox");
        viz::Arrow ex1 = viz::Arrow("ex1")
                         .position(globalCenter)
                         .direction(globalExtend1)
                         .length(globalExtend1.norm())
                         .width(2.0f)
                         .color(viz::Color::red());
        viz::Arrow ex2 = viz::Arrow("ex2")
                         .position(globalCenter)
                         .direction(globalExtend2)
                         .length(globalExtend2.norm())
                         .width(2.0f)
                         .color(viz::Color::green());
        viz::Arrow ex3 = viz::Arrow("ex3")
                         .position(globalCenter)
                         .direction(globalExtend3)
                         .length(globalExtend3.norm())
                         .width(2.0f)
                         .color(viz::Color::blue());

        bbLayer.add(ex1);
        bbLayer.add(ex2);
        bbLayer.add(ex3);
        arviz->commit(bbLayer);
    }

    grasping::GraspCandidatePtr BoxGraspCandidatePlanner::createGraspCandidate(
            const Side& side, float fwdSign, const grasping::ApproachType& graspType, const std::string& objectPoseID,
            std::map<std::string, Armar6GraspTrajectoryPtr>& graspTrajectories,
            const VirtualRobot::RobotNodeSetPtr& rns, const VirtualRobot::RobotNodePtr& tcp,
            const Eigen::Matrix4f& robotPoseGlobal, const Eigen::Vector3f& graspOffsetTCP,
            const Eigen::Matrix3f& referenceOriTop, const Eigen::Matrix3f& referenceOriSide,
            const Eigen::Matrix4f& objectPoseGlobal, const Eigen::Matrix4f &tcpInHandRoot)
    {
        if (side == Side::Left)
        {
            // FIME: Top grasps for the left hand are disabled, because we do not have a trajectory for them
            // break;
        }
        std::string suffix = (fwdSign > 0 ? "_fwd" : "_rev");

        Eigen::Vector3f preferredForwardTop = Eigen::Vector3f(-1, 1, 0);
        Eigen::Vector3f preferredForwardSide = Eigen::Vector3f(1, 0, 0);

        GraspDescription grasp;
        grasping::GraspCandidatePtr candidate = new grasping::GraspCandidate();
        candidate->executionHints = new grasping::GraspCandidateExecutionHints();
        candidate->sourceInfo = new grasping::GraspCandidateSourceInfo();
        candidate->reachabilityInfo = new grasping::GraspCandidateReachabilityInfo();

        Armar6GraspTrajectoryPtr graspTrajectory;
        grasp.pose = math::Helpers::CreatePose(robotCenter, Eigen::Matrix3f::Identity());
        if (graspType == grasping::TopApproach)
        {
            grasp = calculateGraspPoseTop(fwdSign * preferredForwardTop, graspOffsetTCP, referenceOriTop);
            candidate->executionHints->approach = grasping::TopApproach;
            candidate->executionHints->graspTrajectoryName = "RightTopOpen";
            graspTrajectory = graspTrajectories.at("RightTopOpen");
            if (side == Side::Left)
            {
                graspTrajectories["LeftTopOpen"] =
                    BimanualTransformationHelper::transformGraspTrajectoryToOtherHand(graspTrajectory);
                candidate->executionHints->graspTrajectoryName = "LeftTopOpen";
            }
        }
        else if (graspType == grasping::SideApproach)
        {
            grasp = calculateGraspPoseSide(fwdSign * preferredForwardSide, referenceOriSide);
            candidate->executionHints->approach = grasping::SideApproach;
            candidate->executionHints->graspTrajectoryName = "RightSideOpen";
            graspTrajectory = graspTrajectories.at("RightSideOpen");
        }
        else
        {
            ARMARX_ERROR << "Unsupported grasp type";
        }


        graspTrajectory = graspTrajectory->getTransformedToGraspPose(grasp.pose);


        std::map<std::string, float> fingerJointsVisu;
        std::map<std::string, float> openVisu =
        {
            {"Pinky R 1 Joint", 0},
            {"Pinky R 2 Joint", 0},
            {"Pinky R 3 Joint", 0},
            {"Ring R 1 Joint", 0},
            {"Ring R 2 Joint", 0},
            {"Ring R 3 Joint", 0},
            {"Middle R 1 Joint", 0},
            {"Middle R 2 Joint", 0},
            {"Middle R 3 Joint", 0},
            {"Index R 1 Joint", 0},
            {"Index R 2 Joint", 0},
            {"Index R 3 Joint", 0},
            {"Thumb R 1 Joint", 0},
            {"Thumb R 2 Joint", 0}
        };

        if (grasp.preshape == grasping::OpenAperture)
        {
            fingerJointsVisu = openVisu;
            candidate->executionHints->preshape = grasping::OpenAperture;
        }
        else if (grasp.preshape == grasping::PreshapedAperture)
        {
            // TODO: Add preshape visu
            fingerJointsVisu = openVisu;
            candidate->executionHints->preshape = grasping::PreshapedAperture;
        }
        else
        {
            ARMARX_ERROR << "Unsupported preshape type";
        }

        candidate->robotPose = new Pose(robotPoseGlobal);
        candidate->graspPose = new Pose(grasp.pose);
        candidate->tcpPoseInHandRoot = new Pose(tcpInHandRoot);
        candidate->groupNr = 1;
        candidate->providerName = getName();
        candidate->side = side_names.to_name(side);
        candidate->approachVector = new Vector3(grasp.approach);
        candidate->graspSuccessProbability = 0.5f;
        candidate->objectType = objpose::KnownObject;
        candidate->sourceFrame = "root";
        candidate->targetFrame = "root";
        candidate->sourceInfo->referenceObjectName = objectPoseID;
        candidate->sourceInfo->segmentationLabelID = 1;
        candidate->sourceInfo->bbox = new grasping::BoundingBox();
        candidate->sourceInfo->bbox->center = new Vector3(robotCenter);
        candidate->sourceInfo->bbox->ha1 = new Vector3(robotExtend1);
        candidate->sourceInfo->bbox->ha2 = new Vector3(robotExtend2);
        candidate->sourceInfo->bbox->ha3 = new Vector3(robotExtend3);
        candidate->sourceInfo->referenceObjectPose = new Pose(objectPoseGlobal);

        SimpleDiffIK::Reachability reachability = graspTrajectory->calculateReachability(rns, tcp);

        //ARMARX_IMPORTANT << labelstr << ": " << reachability.ikResults.at(0).posDiff.transpose();

        candidate->reachabilityInfo->reachable = reachability.reachable;
        candidate->reachabilityInfo->minimumJointLimitMargin = reachability.minimumJointLimitMargin;
        candidate->reachabilityInfo->jointLimitMargins = ::math::Helpers::VectorToStd(reachability.jointLimitMargins);
        candidate->reachabilityInfo->maxPosError = reachability.maxPosError;
        candidate->reachabilityInfo->maxOriError = reachability.maxOriError;

        return candidate;
    }

    void BoxGraspCandidatePlanner::setArviz(const viz::Client& value)
    {
        arviz = value;
    }

    grasping::GraspCandidateSeq BoxGraspCandidatePlanner::calculateGraspCandidates(
        const Side& side, const objpose::ObjectPose& objectPose, const VirtualRobot::RobotPtr& robot,
        const RobotNameHelperPtr& robotNameHelper,
        std::map<std::string, Armar6GraspTrajectoryPtr>& graspTrajectories)
    {
        return calculateGraspCandidates(side, objectPose.objectID.str(), objectPose.objectPoseGlobal,
                                        objectPose.localOOBB.value(), robot, robotNameHelper, graspTrajectories);
    }

    grasping::GraspCandidateSeq BoxGraspCandidatePlanner::calculateGraspCandidates(
        const Side& side, const std::string& objectPoseID, const Eigen::Matrix4f& objectPoseGlobal,
        const simox::OrientedBoxf& localOOBB, const VirtualRobot::RobotPtr& robot,
        const RobotNameHelperPtr& robotNameHelper,
        std::map<std::string, Armar6GraspTrajectoryPtr>& graspTrajectories)
    {
        ARMARX_VERBOSE << "Calculating grasp candidates";
        grasping::GraspCandidateSeq candidates;

        Eigen::Matrix4f robotPoseGlobal = robot->getGlobalPose();

        RobotNameHelper::RobotArm arm = robotNameHelper->getRobotArm(side_names.to_name(side), robot);
        VirtualRobot::RobotNodeSetPtr rns = arm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = arm.getTCP();

        Eigen::Vector3f localCenter = localOOBB.center();
        Eigen::Vector3f localExtend1 = localOOBB.extend(0);
        Eigen::Vector3f localExtend2 = localOOBB.extend(1);
        Eigen::Vector3f localExtend3 = localOOBB.extend(2);

        ARMARX_VERBOSE << "Got local OOBB, center: " << localCenter.transpose()
                       << ", extend1: " << localExtend1.transpose()
                       << ", extend2: " << localExtend2.transpose()
                       << ", extend3: " << localExtend3.transpose();

        Eigen::Matrix4f objectPoseRobot = robotPoseGlobal.inverse() * objectPoseGlobal;
        Eigen::Matrix3f objectRotRobot = objectPoseRobot.block<3, 3>(0, 0);

        robotCenter = (objectPoseRobot * localCenter.homogeneous()).head<3>();
        // TODO: These are actually half extends
        robotExtend1 = 0.5f * (objectRotRobot * localExtend1);
        robotExtend2 = 0.5f * (objectRotRobot * localExtend2);
        robotExtend3 = 0.5f * (objectRotRobot * localExtend3);


        v1 = robotExtend1.normalized();
        v2 = robotExtend2.normalized();
        v3 = robotExtend3.normalized();
        // These have to be sorted in a specific way
        {
            // v3 should point in the direction of Z
            Eigen::Vector3f z = Eigen::Vector3f::UnitZ();
            if (std::abs(v1.dot(z)) > 0.8f)
            {
                std::swap(v1, v3);
                std::swap(robotExtend1, robotExtend3);
            }
            if (std::abs(v2.dot(z)) > 0.8f)
            {
                std::swap(v2, v3);
                std::swap(robotExtend2, robotExtend3);
            }
            if (v3.z() < 0.0f)
            {
                v3 = -v3;
                robotExtend3 = -robotExtend3;
            }
            if (robotExtend1.norm() < robotExtend2.norm())
            {
                Eigen::Vector3f temp = v1;
                v1 = v2;
                v2 = -temp;

                temp = robotExtend1;
                robotExtend1 = robotExtend2;
                robotExtend2 = -temp;
            }
        }


        Eigen::Matrix3f referenceOriRightTopOpen = Eigen::Quaternionf(
                    -0.3687919974327087,
                    0.6144002676010132,
                    0.6136394143104553,
                    0.331589400768280)
                .toRotationMatrix();

        Eigen::Matrix3f referenceOriRightSide = Eigen::AngleAxisf(M_PI / 2, Eigen::Vector3f::UnitY()) * referenceOriRightTopOpen;

        // TODO: Check whether these make sense!
        Eigen::Matrix3f referenceOriLeftTopOpen =
            BimanualTransformationHelper::transformOrientationToOtherHand(referenceOriRightTopOpen, "Armar6").toRootEigen(robot);
        //Eigen::AngleAxisf(-M_PI / 3, Eigen::Vector3f::UnitX()) * Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitY()) * referenceOriRightTopOpen;
        Eigen::Matrix3f referenceOriLeftSide =
            //Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitY()) *
            BimanualTransformationHelper::transformOrientationToOtherHand(referenceOriRightSide, "Armar6").toRootEigen(robot);
        //Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitX()) *
        //Eigen::AngleAxisf(M_PI / 2, Eigen::Vector3f::UnitY()) *
        //referenceOriLeftTopOpen;

        // Left hand seems to need a bigger offset, otherwise the thumb misses the object
        isRightSide = (side == Side::Right);
        Eigen::Vector3f graspOffsetTCP = isRightSide ? Eigen::Vector3f(0.0f, 30.0f, 0.0f) : Eigen::Vector3f(0.0f, 50.0f, 0.0f);

        Eigen::Matrix3f referenceOriTop = isRightSide ? referenceOriRightTopOpen : referenceOriLeftTopOpen;
        Eigen::Matrix3f referenceOriSide = isRightSide ? referenceOriRightSide : referenceOriLeftSide;

        if (arviz)
        {
            commitExtendsToArviz(robotPoseGlobal);
        }

        std::string handRootNodeName = (side == Side::Right) ? "Hand R Root" : "Hand L Root";

        Eigen::Matrix4f tcpInHandRoot = (tcp->getPoseInRootFrame().inverse()* robot->getRobotNode(handRootNodeName)->getPoseInRootFrame()).inverse();


        std::vector<float> signs = {1, -1};

        // TODO: Make this selectable
        grasping::ApproachType graspType = grasping::TopApproach;
        for (float fwdSign : signs)
        {
            grasping::GraspCandidatePtr candidate = createGraspCandidate(side, fwdSign, graspType, objectPoseID,
                                                    graspTrajectories, rns, tcp, robotPoseGlobal, graspOffsetTCP,
                                                    referenceOriTop, referenceOriSide, objectPoseGlobal, tcpInHandRoot);
            candidates.push_back(candidate);
        }
        graspType = grasping::SideApproach;
        for (float fwdSign : signs)
        {
            grasping::GraspCandidatePtr candidate = createGraspCandidate(side, fwdSign, graspType, objectPoseID,
                                                    graspTrajectories, rns, tcp, robotPoseGlobal, graspOffsetTCP,
                                                    referenceOriTop, referenceOriSide, objectPoseGlobal, tcpInHandRoot);
            candidates.push_back(candidate);
        }

        std::string candidateReachability;
        for (auto& candidate : candidates)
        {
            candidateReachability += candidate->reachabilityInfo->reachable ? "1, " : "0, ";
        }
        ARMARX_VERBOSE << "Found " << candidates.size() << " grasp candidates: " << candidateReachability;


        return candidates;
    }
}



