#pragma once

#include "PickAndPlaceController.h"

#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>

#include <RobotAPI/components/ArViz/Client/Client.h>

namespace armarx
{

    struct GraspDescription
    {
        Eigen::Matrix4f pose;
        Eigen::Vector3f approach;
        grasping::ApertureType preshape;
    };


    class BoxGraspCandidatePlanner
    {
    public:
        BoxGraspCandidatePlanner();
        BoxGraspCandidatePlanner(viz::Client const& arviz);

        grasping::GraspCandidateSeq calculateGraspCandidates(Side const& side, std::string const& objectPoseID,
                Eigen::Matrix4f const& objectPoseGlobal, simox::OrientedBoxf const& localOOBB,
                VirtualRobot::RobotPtr const& robot, RobotNameHelperPtr const& robotNameHelper,
                std::map<std::string, Armar6GraspTrajectoryPtr>& graspTrajectories);

        grasping::GraspCandidateSeq calculateGraspCandidates(Side const& side, objpose::ObjectPose const& objectPose,
                VirtualRobot::RobotPtr const& robot, RobotNameHelperPtr const& robotNameHelper,
                std::map<std::string, Armar6GraspTrajectoryPtr>& graspTrajectories);

        std::string getName();

        void setName(std::string const& name);

        void setArviz(const viz::Client& value);

    private:
        GraspDescription calculateGraspPoseSide(Eigen::Vector3f const& preferredForward,
                                                Eigen::Matrix3f const& referenceOriSide);

        GraspDescription calculateGraspPoseTop(Eigen::Vector3f const& preferredForward,
                                               Eigen::Vector3f const& graspOffsetTCP,
                                               Eigen::Matrix3f const& referenceOriTop);

        void commitExtendsToArviz(Eigen::Matrix4f const& robotPoseGlobal);

        grasping::GraspCandidatePtr createGraspCandidate(
            const Side& side, float fwdSign, const grasping::ApproachType& graspType,
            const std::string& objectPoseID, std::map<std::string, Armar6GraspTrajectoryPtr>& graspTrajectories,
            VirtualRobot::RobotNodeSetPtr const& rns, VirtualRobot::RobotNodePtr const& tcp,
            Eigen::Matrix4f const& robotPoseGlobal, Eigen::Vector3f const& graspOffsetTCP,
            Eigen::Matrix3f const& referenceOriTop, Eigen::Matrix3f const& referenceOriSide,
            Eigen::Matrix4f const& objectPoseGlobal, Eigen::Matrix4f const& tcpInHandRoot);

        std::optional<viz::Client> arviz;

        std::string name = "BoxGraspCandidatePlanner";

        Eigen::Vector3f robotCenter;
        Eigen::Vector3f robotExtend1;
        Eigen::Vector3f robotExtend2;
        Eigen::Vector3f robotExtend3;

        Eigen::Vector3f v1;
        Eigen::Vector3f v2;
        Eigen::Vector3f v3;
        bool isRightSide;


    };
}
