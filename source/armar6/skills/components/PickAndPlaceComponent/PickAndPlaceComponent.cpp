/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    InteractiveSceneExplorationX::ArmarXObjects::PickAndPlaceComponent
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PickAndPlaceComponent.h"
#include "PickAndPlaceController.h"

#include <armar6/skills/libraries/Armar6Tools/Armar6HandV2Helper.h>
#include <armar6/skills/libraries/Armar6Tools/BimanualTransformationHelper.h>

#include <armarx/manipulation/core/ExecutableAction.h>
#include <armarx/manipulation/core/ExecutedAction.h>
#include <armarx/manipulation/core/Affordance.h>
#include <armarx/manipulation/core/FramedUncertainPose.h>
#include <armarx/manipulation/core/aron_conversions.h>
#include <armarx/manipulation/core/aron/ExecutableAction.aron.generated.h>
#include <armarx/manipulation/core/aron/ExecutedAction.aron.generated.h>

#include <RobotAPI/libraries/RobotStatechartHelpers/ForceTorqueHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/PositionControllerHelper.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>
#include <RobotAPI/libraries/ArmarXObjects/ice_conversions.h>
#include <RobotAPI/libraries/ArmarXObjects/aron_conversions/objpose.h>
#include <RobotAPI/libraries/aron/common/aron_conversions/core.h>
#include <RobotAPI/libraries/armem_objects/aron_conversions.h>
#include <RobotAPI/libraries/armem_objects/aron/ObjectInstance.aron.generated.h>

#include <ArmarXCore/core/time/Metronome.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>


#include <VirtualRobot/math/Helpers.h>
#include <VirtualRobot/math/Line.h>
#include <VirtualRobot/math/LinearInterpolatedPose.h>

#include <SimoxUtility/math/pose/pose.h>
#include <SimoxUtility/math/pose/transform.h>


namespace armarx
{

    PackageFileLocation
    getLocation(const ObjectID& id)
    {
        ARMARX_TRACE;

        ObjectFinder finder;
        std::optional<ObjectInfo> foundObject = finder.findObject(id.dataset(), id.className());
        if (!foundObject)
        {
            ARMARX_WARNING << "No object information found for object: " << id.className();
        }

        PackageFileLocation location = foundObject->simoxXML();
        return location;
    }


    PropertyDefinitionsPtr
    PickAndPlaceComponent::createPropertyDefinitions()
    {
        ARMARX_TRACE;

        armarx::PropertyDefinitionsPtr properties(new armarx::ComponentPropertyDefinitions(getConfigIdentifier()));

        properties->defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver",
                "Name of the topic the DebugObserver listens to.");
        properties->defineOptionalProperty<std::string>("ConfigPath", "armar6_skills/pick-and-place/PickAndPlaceComponent.json",
                "Path to the config file");

        //more modern variant to use components

        properties->component(kinematicUnit, "Armar6KinematicUnit");
        properties->component(robotUnit, "Armar6Unit");
        properties->component(robotUnitObserver, "RobotUnitObserver");
        properties->component(forceTorqueObserver, "Armar6ForceTorqueObserver");
        properties->component(headIK);
        return properties;
    }


    PickAndPlaceComponent::PickAndPlaceComponent(): graspWriter(memoryNameSystem()), graspReader(memoryNameSystem())
    {
    }


    std::string
    PickAndPlaceComponent::getDefaultName() const
    {
        return GetDefaultName();
    }


    std::string
    PickAndPlaceComponent::GetDefaultName()
    {
        return "PickAndPlaceComponent";
    }


    void
    PickAndPlaceComponent::onInitComponent()
    {
        ARMARX_TRACE;

        {
            std::string packageName = "armar6_skills";
            armarx::CMakePackageFinder finder(packageName);
            std::string dataDir = finder.getDataDir() + "/" + packageName;
            data.controllerInfo.graspTrajectories["RightTopOpen"] = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/RightTopOpen.xml");
            data.controllerInfo.graspTrajectories["RightSideOpen"] = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/RightSideOpen.xml");
        }

        offeringTopicFromProperty("DebugObserverName");


        armarx::CMakePackageFinder finder("armar6_skills");
        if (!finder.packageFound())
        {
            ARMARX_WARNING << "ArmarX Package has not been found!";
        }
        else
        {
            ARMARX_INFO << "Adding to datapaths: " << finder.getDataDir();
            armarx::ArmarXDataPath::addDataPaths(finder.getDataDir());
        }
        std::string relativeConfigPath = getProperty<std::string>("ConfigPath").getValue();
        std::string configPath = ArmarXDataPath::getAbsolutePath(relativeConfigPath);

        ARMARX_INFO << "Loading config from: " << configPath;
        nlohmann::json j = nlohmann::read_json(configPath);
        j.get_to(config);

        ARMARX_INFO << "Prepose waypoints for left: " << config.Prepose.Left.Waypoints.size()
                    << ", for right: " << config.Prepose.Right.Waypoints.size();
    }


    void PickAndPlaceComponent::onConnectComponent()
    {
        ARMARX_TRACE;

        graspWriter.connect();
        graspReader.connect();

        getTopicFromProperty(debugObserver, "DebugObserverName");

        VirtualRobot::RobotPtr robot;
        if (!hasRobot("robot"))
        {
            robot = addRobot("robot", VirtualRobot::RobotIO::eStructure);
        }
        else
        {
            robot = getRobot("robot");
        }
        robot->setPropagatingJointValuesEnabled(!config.General.IsSimulation);
        if (!hasRobot("gui-robot"))
        {
            data.robot = addRobot("gui-robot", VirtualRobot::RobotIO::eStructure);
        }
        else
        {
            data.robot = getRobot("gui-robot");
        }
        data.robot->setPropagatingJointValuesEnabled(!config.General.IsSimulation);
        robotNameHelper = RobotNameHelper::Create(getRobotStateComponent()->getRobotInfo(), nullptr);


        gui.simoxFileCache = simox::caching::CacheMap<armarx::ObjectID, PackageFileLocation>(getLocation);

        saveObjectPoses();


        createRemoteGuiTab();
        RemoteGui_startRunningTask();

        controlTask = new SimpleRunningTask<>([this]()
        {
            this->runControlLoop();
        });

        data.boxGraspCandidatePlanner = std::make_unique<BoxGraspCandidatePlanner>();
        data.boxGraspCandidatePlanner->setArviz(arviz);

        controller = std::make_unique<PickAndPlaceController>();
        controller->setRobot(robot);
        controller->setRobotNameHelper(robotNameHelper);
        controller->setKinematicUnit(kinematicUnit);
        controller->setRobotUnitObserver(robotUnitObserver);
        controller->setConfig(config);
        controller->setRobotUnit(robotUnit);
        controller->setRobotStatePrx(getRobotStateComponent());
        controller->setForceTorqueObserver(forceTorqueObserver);
        controller->setDebugObserverHelper(DebugObserverHelper(debugObserver, true));
        controller->setHeadIK(headIK);
        controller->setObjectPoseStorage(getObjectPoseClientPlugin().createObjectPoseStorage());

        objectReader = memoryNameSystem().getReader(armem::MemoryID("Object", "Instance"));

        controlTask->start();
    }


    void PickAndPlaceComponent::onDisconnectComponent()
    {
        ARMARX_TRACE;

        controller->stopExecution();
        controlTask->stop();
        controlTask = nullptr;
    }


    void PickAndPlaceComponent::onExitComponent()
    {
        ARMARX_TRACE;
    }


    void PickAndPlaceComponent::createRemoteGuiTab()
    {
        ARMARX_TRACE;

        using namespace RemoteGui::Client;
        GridLayout root;
        int row = 0;
        {
            std::unique_lock lock(data.objectPoseMutex);
            std::vector<std::string> options;
            options.reserve(data.objectPosesDynamic.size());
            for (auto* objectPose : data.objectPosesDynamic)
            {
                options.push_back(objectPose->objectID.str());
            }
            if (options.empty())
            {
                options.push_back("<None>");
            }

            gui.tab.selectObject.setOptions(options);

            if (0 <= gui.objectIndex and gui.objectIndex < static_cast<int>(options.size()))
            {
                if (! data.objectPosesDynamic.empty())
                {
                    auto* objectPose = data.objectPosesDynamic.at(gui.objectIndex);
                    gui.graspCandidatesObject = *objectPose;
                }
                gui.tab.selectObject.setIndex(gui.objectIndex);
            }

            root.add(Label("Object:"), Pos{row, 0});
            root.add(gui.tab.selectObject, Pos{row, 1});
            row += 1;
        }
        {
            gui.tab.calculateGraspCandidates.setLabel("Calculate Grasp Candidates");
            root.add(gui.tab.calculateGraspCandidates, Pos{row, 0}, Span{1, 2});
            row += 1;
        }
        {
            grasping::GraspCandidateDict candidates;
            try
            {
                std::unique_lock lock(data.controlMutex);
                ARMARX_INFO << "Querying for entity " << gui.graspCandidatesObject.objectID.str();
                candidates = graspReader.queryLatestGraspCandidateEntity(
                            getName(), gui.graspCandidatesObject.objectID.str());
            }
            catch (armem::error::QueryFailed &e)
            {
                ARMARX_WARNING << "No Entries for entity " << gui.graspCandidatesObject.objectID.str()
                             << " in Memory " << graspMemoryName << ".";
            }
            gui.graspCandidates.clear();
            std::vector<std::string> options;
            int selected = -1;
            int i = 0;
            for (auto& [id, grasp] : candidates)
            {
                // translate ids to readable strings

                bool isReachable = grasp->reachabilityInfo->reachable;
                std::stringstream key;
                key << gui.graspCandidatesObject.objectID.str() << " " << i
                    << " (" << (isReachable ? std::string("reachable") : std::string("unreachable")) << ")";
                gui.graspCandidates[key.str()] = id;
                ++i;

                options.push_back(key.str());
                if (gui.graspID == key.str())
                {
                    selected = i;
                }
            }
            if (options.empty())
            {
                options.push_back("<None>");
            }
            gui.tab.selectGrasp.setOptions(options);
            if (selected >= 0)
            {
                gui.tab.selectGrasp.setIndex(selected);
            }

            visualizeGraspCandidates();
            root.add(Label("Grasp:"), Pos{row, 0});
            root.add(gui.tab.selectGrasp, Pos{row, 1});
            row += 1;
        }
        {
            gui.tab.placePosition.setValue(gui.placePosition);
            gui.tab.placePosition.setDecimals(0);
            gui.tab.placePosition.setRange(-5000.0f, 6000.0f);
            gui.tab.placePosition.setSteps(500);
            root.add(Label("Place Position:"), Pos{row, 0});
            root.add(gui.tab.placePosition, Pos{row, 1});
            row += 1;
        }
        {
            std::vector<std::string> options;
            options.reserve(static_cast<int>(Action::End) - static_cast<int>(Action::Begin));

            for (Action action = Action::Begin; action != Action::End; action = static_cast<Action>(static_cast<int>(action) + 1))
            {
                options.push_back(action_names.to_name(action));

            }
            gui.tab.selectAction.setOptions(options);
            gui.tab.selectAction.setIndex(static_cast<int>(gui.action));

            root.add(Label("Action:"), Pos{row, 0});
            root.add(gui.tab.selectAction, Pos{row, 1});
            row += 1;
        }
        {
            gui.tab.selectSide.setOptions({side_names.to_name(Side::Left), side_names.to_name(Side::Right)});
            gui.tab.selectSide.setIndex(static_cast<int>(gui.side));

            root.add(Label("Side:"), Pos{row, 0});
            root.add(gui.tab.selectSide, Pos{row, 1});
            row += 1;
        }
        {
            gui.tab.executeAction.setLabel("Execute Action");
            root.add(gui.tab.executeAction, Pos{row, 0}, Span{1, 2});
            row += 1;
        }
        {
            gui.tab.stop.setLabel("STOP");
            root.add(gui.tab.stop, Pos{row, 0}, Span{2, 2});
            row += 2;
        }
        {
            gui.tab.saveObjectPoses.setLabel("Save Object Poses");
            root.add(gui.tab.saveObjectPoses, Pos{row, 0}, Span{1, 2});
            row += 1;
        }
        {
            gui.tab.autoUpdateGraspCandidates.setValue(gui.autoUpdateGrasps);
            root.add(Label("Auto Update Grasp Candidates:"), Pos{row, 0});
            root.add(gui.tab.autoUpdateGraspCandidates, Pos{row, 1});
            row += 1;
        }
        {
            gui.tab.autoUpdatePlaceCandidate.setValue(gui.autoUpdatePlace);
            root.add(Label("Auto Update Place Candidate:"), Pos{row, 0});
            root.add(gui.tab.autoUpdatePlaceCandidate, Pos{row, 1});
            row += 1;
        }
        {
            {
                std::unique_lock lock(data.controlMutex);
                gui.tab.autoUpdateGraspsAllObjects.setValue(data.autoUpdateGraspsAllObjects);
            }
            root.add(Label("Auto Update Grasp Candidates for all Objects:"), Pos{row, 0});
            root.add(gui.tab.autoUpdateGraspsAllObjects, Pos{row, 1});
            row += 1;
        }

        RemoteGui_createTab(getName(), root, &gui.tab);
    }


    void PickAndPlaceComponent::RemoteGui_update()
    {
        ARMARX_TRACE;

        bool recreate = gui.recreateTabNextLoop;

        Side newSide = static_cast<Side>(gui.tab.selectSide.getIndex());
        if (newSide != gui.side)
        {
            ARMARX_INFO << "Selected new side: " << side_names.to_name(newSide);
            gui.side = newSide;
        }

        Action newAction = static_cast<Action>(gui.tab.selectAction.getIndex());
        if (newAction != gui.action)
        {
            ARMARX_INFO << "Selected new action: " << action_names.to_name(newAction);
            gui.action = newAction;
            // TODO: Make the remote GUI for that specific action
        }

        int newObjectIndex = gui.tab.selectObject.getIndex();
        if (newObjectIndex != gui.objectIndex)
        {
            ARMARX_INFO << "Selected new object index: " << newObjectIndex;
            std::unique_lock lock(data.objectPoseMutex);
            if (0 <= newObjectIndex and newObjectIndex < static_cast<int>(data.objectPosesDynamic.size())
                and not data.objectPosesDynamic.empty())
            {
                auto* objectPose = data.objectPosesDynamic.at(newObjectIndex);
                ARMARX_INFO << "Selected new object: " << objectPose->objectID.str();
                gui.objectIndex = newObjectIndex;
                gui.tab.placePosition.setValue(simox::math::position(objectPose->objectPoseGlobal));
                gui.graspCandidatesObject = *objectPose;
                gui.recreateTabNextLoop = true;
            }
        }

        auto it = gui.graspCandidates.find(gui.tab.selectGrasp.getValue());
        if (it != gui.graspCandidates.end())
        {
            const std::string& newGraspID = it->second;
            if (newGraspID != gui.graspID)
            {
                if (newGraspID.size() > 0)
                {
                    ARMARX_INFO << "Selected new grasp: '" << newGraspID << "'";
                    gui.graspID = newGraspID;
                    visualizeGraspCandidates();
                }
            }
        }


        bool newAutoUpdateGrasps = gui.tab.autoUpdateGraspCandidates.getValue();
        if (newAutoUpdateGrasps != gui.autoUpdateGrasps)
        {
            gui.autoUpdateGrasps = newAutoUpdateGrasps;
        }
        bool newAutoUpdatePlace = gui.tab.autoUpdatePlaceCandidate.getValue();
        if (newAutoUpdatePlace != gui.autoUpdatePlace)
        {
            gui.autoUpdatePlace = newAutoUpdatePlace;
        }
        if (gui.tab.autoUpdateGraspsAllObjects.hasValueChanged())
        {
            std::unique_lock lock(data.controlMutex);
            data.autoUpdateGraspsAllObjects = gui.tab.autoUpdateGraspsAllObjects.getValue();
            if(data.autoUpdateGraspsAllObjects)
            {
                for (auto* objectPose : data.objectPosesDynamic)
                {
                    getGraspCandidatesInPluginFormat(data, gui.side, *objectPose);
                }
                gui.recreateTabNextLoop = true;
            }
        }
        Eigen::Vector3f newPlacePosition = gui.tab.placePosition.getValue();
        Eigen::Vector3f placeDiff = gui.placePosition - newPlacePosition;
        if (placeDiff.norm() > 0.1f)
        {
            gui.placePosition = newPlacePosition;
            visualizePlacePosition();
        }

        {
            std::unique_lock lock(data.controlMutex);
            if (gui.tab.saveObjectPoses.wasClicked() || data.objectsUpdated)
            {
                saveObjectPoses();
                data.objectsUpdated = false;
            }

        }

        if (gui.tab.calculateGraspCandidates.wasClicked())
        {
            std::unique_lock lock(data.controlMutex);
            std::unique_lock lock_poses(data.objectPoseMutex);
            data.controllerInfo.action = gui.action;
            data.controllerInfo.side = gui.side;

            if (0 <= gui.objectIndex and gui.objectIndex < static_cast<int>(data.objectPosesDynamic.size()))
            {
                data.controllerInfo.objectPose = *data.objectPosesDynamic.at(gui.objectIndex);
                getGraspCandidatesInPluginFormat(data, data.controllerInfo.side, data.controllerInfo.objectPose);
                gui.recreateTabNextLoop = true;
            }
            else
            {
                ARMARX_WARNING << "guiObjectIndex = " << gui.objectIndex
                               << ", objectPosesDynamic size: " << data.objectPosesDynamic.size();
            }
        }

        if (gui.tab.executeAction.wasClicked())
        {
            std::unique_lock lock(data.controlMutex);
            std::unique_lock lock_poses(data.objectPoseMutex);
            data.controllerInfo.action = gui.action;
            data.controllerInfo.side = gui.side;
            if (0 <= gui.objectIndex and gui.objectIndex < static_cast<int>(data.objectPosesDynamic.size()))
            {
                data.controllerInfo.objectPose = *data.objectPosesDynamic.at(gui.objectIndex);
            }
            if (gui.graspID.size() > 0)
            {
                try
                {
                    data.controllerInfo.grasp = graspReader.queryGraspCandidateInstanceByID(armem::MemoryID::fromString(gui.graspID));
                }
                catch (armem::error::QueryFailed &e)
                {
                    ARMARX_WARNING << "No grasp candidate with ID " << gui.graspID << " in memory " << graspMemoryName << ".";
                    data.controllerInfo.grasp = nullptr;
                }

            }
            else
            {
                data.controllerInfo.grasp = nullptr;
            }
            data.controllerInfo.placePositionGlobal = gui.placePosition;
            ARMARX_INFO << "Executing action: " << action_names.to_name(gui.action);
            data.runExecution = true;
        }

        if (gui.tab.stop.wasClicked())
        {
            ARMARX_INFO << "Stopping action";
            controller->stopExecution(); // und wenn running task existiert dann auch stop
        }

        if (gui.autoUpdateGrasps)
        {
            std::unique_lock lock(data.controlMutex);
            std::unique_lock lock_poses(data.objectPoseMutex);
            data.controllerInfo.action = gui.action;
            data.controllerInfo.side = gui.side;
            if (0 <= gui.objectIndex and gui.objectIndex < static_cast<int>(data.objectPosesDynamic.size()))
            {
                data.controllerInfo.objectPose = *data.objectPosesDynamic.at(gui.objectIndex);
                getGraspCandidatesInPluginFormat(data, data.controllerInfo.side, data.controllerInfo.objectPose);
            }
        }
        {
            std::unique_lock lock(data.controlMutex);
            if (data.autoUpdateGraspsAllObjects)
            {
                data.controllerInfo.side = gui.side;
            }
        }

        if (gui.autoUpdatePlace)
        {
            visualizePlacePosition();
        }

        // TODO: Make this optional?
        visualizeObjectPoses();

        if (recreate)
        {
            createRemoteGuiTab();
            gui.recreateTabNextLoop = false;
        }
    }


    grasping::CalculateGraspCandidatesOutput PickAndPlaceComponent::calculateGraspCandidates(
        const grasping::CalculateGraspCandidatesInput& input, const Ice::Current&)
    {
        ARMARX_TRACE;

        grasping::CalculateGraspCandidatesOutput output;

        const std::set<std::string> sides { side_names.to_name(Side::Left), side_names.to_name(Side::Right) };
        if (sides.count(input.side) == 0)
        {
            ARMARX_ERROR << "Invalid value for side: '" << input.side << "'. "
                         << "Expected one of:\n" << std::vector<std::string>(sides.begin(), sides.end());
            return output;
        }

        objpose::ObjectPose pose = input.objectPose;

        {
            std::unique_lock lock(data.controlMutex);
            getGraspCandidatesInPluginFormat(data, side_names.from_name(input.side), pose);

            try
            {
                output.candidates = graspReader.queryLatestGraspCandidateEntity(getName(), pose.objectID.str());
            }
            catch (armem::error::QueryFailed &e)
            {
                ARMARX_WARNING << "No Entries for entity " << pose.objectID.str() << " in Memory " << graspMemoryName << ".";
                return output;
            }
        }

        return output;
    }


    grasping::ExecutePreposeOutput
    PickAndPlaceComponent::executePrepose(
        const grasping::ExecutePreposeInput& input, const Ice::Current&)
    {
        ARMARX_TRACE;

        grasping::ExecutePreposeOutput output;
        output.executed = false;

        {
            std::unique_lock lock(data.controlMutex);
            ControllerInfo control = data.controllerInfo;

            control.action = Action::Prepose;

            const std::set<std::string> sides { side_names.to_name(Side::Left), side_names.to_name(Side::Right) };
            if (sides.count(input.side) == 0)
            {
                ARMARX_ERROR << "Invalid value for side: '" << input.side << "'. "
                             << "Expected one of:\n" << std::vector<std::string>(sides.begin(), sides.end());
                return output;
            }
            control.side = side_names.from_name(input.side);


            ARMARX_INFO << "Executing prepose action";
            data.controllerInfo = control;
            data.runExecution = true;
        }
        // Wait until finished.
        data.runFinished.wait();

        output.executed = true;
        return output;
    }


    grasping::ExecutePushOutput
    PickAndPlaceComponent::executePush(
        const grasping::ExecutePushInput& input, const Ice::Current&)
    {
        ARMARX_TRACE;

        grasping::ExecutePushOutput output;
        output.executed = false;

        {
            std::unique_lock lock(data.controlMutex);
            ControllerInfo control = data.controllerInfo;

            control.action = Action::Push;

            const std::set<std::string> sides { side_names.to_name(Side::Left), side_names.to_name(Side::Right) };
            if (sides.count(input.side) == 0)
            {
                ARMARX_ERROR << "Invalid value for side: '" << input.side << "'. "
                             << "Expected one of:\n" << std::vector<std::string>(sides.begin(), sides.end());
                return output;
            }
            control.side = side_names.from_name(input.side);

            ARMARX_INFO << "Executing push action";
            data.controllerInfo = control;
            data.runExecution = true;
        }
        // Wait until finished.
        data.runFinished.wait();

        output.executed = true;
        return output;
    }


    grasping::ExecutePickOutput PickAndPlaceComponent::executePick(
        const grasping::ExecutePickInput& input, const Ice::Current&)
    {
        ARMARX_TRACE;

        grasping::ExecutePickOutput output;
        output.executed = false;

        {
            std::unique_lock lock(data.controlMutex);
            ControllerInfo control = data.controllerInfo;

            control.action = Action::Pick;
            try
            {
                control.grasp = graspReader.queryGraspCandidateInstanceByID(armem::MemoryID::fromString(input.candidateID));
            }
            catch (armem::error::QueryFailed &e)
            {
                ARMARX_WARNING << "No grasp candidate with ID " << input.candidateID << " in memory " << graspMemoryName << ".";
                return output;
            }

            if (input.liftVector)  // Else leave default.
            {
                control.liftVector = Vector3Ptr::dynamicCast(input.liftVector)->toEigen();
            }

            control.side = side_names.from_name(control.grasp->side);

            ARMARX_INFO << "Executing pick action";
            data.controllerInfo = control;
            data.runExecution = true;
        }
        // Wait until finished.
        data.runFinished.wait();

        // Collect output
        {
            std::unique_lock lock(data.controlMutex);
            output.executed = data.controllerOutput.executed;
        }
        return output;
    }


    grasping::ExecutePlaceOutput
    PickAndPlaceComponent::executePlace(
        const grasping::ExecutePlaceInput& input, const Ice::Current&)
    {
        ARMARX_TRACE;

        grasping::ExecutePlaceOutput output;
        output.executed = false;

        {
            std::unique_lock lock(data.controlMutex);
            ControllerInfo control = data.controllerInfo;

            control.action = Action::Place;

            const std::set<std::string> sides { side_names.to_name(Side::Left), side_names.to_name(Side::Right) };
            if (sides.count(input.side) == 0)
            {
                ARMARX_ERROR << "Invalid value for side: '" << input.side << "'. "
                             << "Expected one of:\n" << std::vector<std::string>(sides.begin(), sides.end());
                return output;
            }
            control.side = side_names.from_name(input.side);

            if (input.placePositionGlobal)  // Else leave default.
            {
                control.placePositionGlobal = Vector3Ptr::dynamicCast(input.placePositionGlobal)->toEigen();
            }

            ARMARX_INFO << "Executing place action";
            data.controllerInfo = control;
            data.runExecution = true;
        }
        // Wait until finished.
        data.runFinished.wait();

        output.executed = true;
        return output;
    }


    grasping::ExecuteRetreatOutput
    PickAndPlaceComponent::executeRetreat(
        const grasping::ExecuteRetreatInput& input, const Ice::Current&)
    {
        ARMARX_TRACE;

        grasping::ExecuteRetreatOutput output;
        output.executed = false;

        {
            std::unique_lock lock(data.controlMutex);
            ControllerInfo control = data.controllerInfo;

            control.action = Action::Retreat;

            const std::set<std::string> sides { side_names.to_name(Side::Left), side_names.to_name(Side::Right) };
            if (sides.count(input.side) == 0)
            {
                ARMARX_ERROR << "Invalid value for side: '" << input.side << "'. "
                             << "Expected one of:\n" << std::vector<std::string>(sides.begin(), sides.end());
                return output;
            }
            control.side = side_names.from_name(input.side);

            ARMARX_INFO << "Executing retreat action";
            data.controllerInfo = control;
            data.runExecution = true;
        }
        // Wait until finished.
        data.runFinished.wait();

        output.executed = true;
        return output;
    }


    grasping::ExecutePreposeReverseOutput
    PickAndPlaceComponent::executePreposeReverse(
        const grasping::ExecutePreposeReverseInput& input, const Ice::Current&)
    {
        ARMARX_TRACE;

        grasping::ExecutePreposeReverseOutput output;
        output.executed = false;

        {
            std::unique_lock lock(data.controlMutex);
            ControllerInfo control = data.controllerInfo;

            control.action = Action::PreposeReverse;

            const std::set<std::string> sides { side_names.to_name(Side::Left), side_names.to_name(Side::Right) };
            if (sides.count(input.side) == 0)
            {
                ARMARX_ERROR << "Invalid value for side: '" << input.side << "'. "
                             << "Expected one of:\n" << std::vector<std::string>(sides.begin(), sides.end());
                return output;
            }
            control.side = side_names.from_name(input.side);

            ARMARX_INFO << "Executing prepose reverse action";
            data.controllerInfo = control;
            data.runExecution = true;
        }
        // Wait until finished.
        data.runFinished.wait();

        output.executed = true;
        return output;
    }


    grasping::LocalizeObjectInHandOutput
    PickAndPlaceComponent::localizeObjectInHand(
        const grasping::LocalizeObjectInHandInput& input, const Ice::Current&)
    {
        ARMARX_TRACE;

        viz::Client arviz = ArVizComponentPluginUser::createArVizClient();

        objpose::ObjectPoseSeq localObjectPoses = getObjectPoses();

        //controller->setHeadIK(headIK);

        ObjectID objectID(input.objectID.dataset, input.objectID.className);

        {
            std::unique_lock lock(data.controlMutex);
            ControllerInfo control = data.controllerInfo;

            control.action = Action::LocalizeObjectInHand;
            control.side = side_names.from_name(input.side);
            control.objectID = objectID;
            control.initialWaitMS = input.initialWaitMS;
            control.maxDistanceFromTcp = input.maxDistanceFromTcp;
            control.minPoseUpdates = input.minPoseUpdates;
            control.moveHead = input.moveHead;
            control.pollFrequency = input.pollFrequency;
            control.timeoutMS = input.timeoutMS;
            //TODO execute in control loop only

            ARMARX_INFO << "Executing LocalizeObjectInHand action";
            data.controllerInfo = control;
            data.runExecution = true;
        }
        // Wait until finished.
        data.runFinished.wait();

        objpose::ObjectPoseSeq acceptedObjectPoses;
        {
            std::unique_lock lock(data.controlMutex);
            acceptedObjectPoses = data.controllerInfo.acceptedObjectPoses;
        }

        grasping::LocalizeObjectInHandOutput output;

        RobotNameHelper::RobotArm arm;

        {
            std::unique_lock lock(data.controlMutex);
            synchronizeLocalClone(data.robot);

            RobotNameHelper::RobotArm arm = robotNameHelper->getRobotArm(input.side, data.robot);
        }

        // VirtualRobot::RobotNodeSetPtr rns = arm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = arm.getTCP();

        if (int(acceptedObjectPoses.size()) >= input.minPoseUpdates)
        {
            ARMARX_IMPORTANT << "In-hand localization: SUCCESS";
            // Success.
            output.success = true;

            // Use latest pose by default.
            objpose::ObjectPose pose = acceptedObjectPoses.back();
            if (input.interpolatePose)
            {
                ARMARX_INFO << "Interpolating over " << acceptedObjectPoses.size() << " poses";
                Eigen::Vector3f position = Eigen::Vector3f::Zero();
                for (const auto& p : acceptedObjectPoses)
                {
                    position += simox::math::position(p.objectPoseRobot);
                }
                position /= acceptedObjectPoses.size();

                simox::math::position(pose.objectPoseRobot) = position;

                simox::math::position(pose.objectPoseGlobal) =
                    simox::math::transform_position(pose.objectPoseGlobal, position);

                // ToDo: Orientation. Could use this? https://stackoverflow.com/a/27410865
            }

            output.objectPose = pose.toIce();
            output.tcpPoseRobot = new Pose(tcp->getPoseInRootFrame());

            {
                viz::Layer layer = arviz.layer("In Hand Localization: Accepted Poses");
                int i = 0;
                for (const objpose::ObjectPose& p : acceptedObjectPoses)
                {
                    layer.add(viz::Object(std::to_string(i))
                              .pose(p.objectPoseGlobal)
                              .fileByObjectFinder(objectID)
                              .overrideColor(simox::Color::blue(255, 128)));
                    ++i;
                }
                arviz.commit(layer);
            }
        }
        else
        {
            ARMARX_IMPORTANT << "In-hand localization: FAILURE due to timeout";
            output.success = false;
        }

        ARMARX_IMPORTANT << "Returning result";

        return output;
    }

    aron::data::dto::DictPtr PickAndPlaceComponent::executeAction(const aron::data::dto::DictPtr &executableAction, const Ice::Current&)
    {
        ARMARX_TRACE;
        armarx::manipulation::core::arondto::ExecutableAction dto;
        dto.fromAron(executableAction);

        manipulation::core::ExecutableAction action = armarx::manipulation::core::fromAron(dto);

        armarx::manipulation::core::arondto::ExecutedAction exDto;
        std::string actionType = action.unimanual.at(0).hypothesis->affordance.get()->getAffordanceID();

        if (actionType == "Prepose")
        {
            grasping::ExecutePreposeInput input;
            input.side = action.unimanual.at(0).handedness->toString();
            manipulation::core::ExecutedAction exAction(action, armem::Time::Now());

            bool executed = executePrepose(input).executed;
            exAction.endTime = std::make_unique<armem::Time>(armem::Time::Now());
            exAction.success = executed;
            exDto = armarx::manipulation::core::toAron(exAction);
        }
        else if (actionType == "grasp")
        {
            // executes prepose and pick currently
            // fill grasp candidate with necessary info and forward to controller
            // cannot call ice method executePick here because that one uses armem MemoryID
            grasping::GraspCandidatePtr candidate  = new grasping::GraspCandidate();

            auto hypothesis = action.unimanual.at(0).hypothesis;

            candidate->graspPose = new Pose(hypothesis->framedPose->pose->toEigen());

            candidate->sourceFrame = hypothesis->framedPose->pose->getFrame();
            candidate->targetFrame = hypothesis->framedPose->pose->getFrame();

            candidate->side = hypothesis->handedness->toString();
            candidate->approachVector = action.unimanual.at(0).prePose->position;
            candidate->executionHints = grasping::GraspCandidateExecutionHintsPtr();

            objpose::ObjectPose objectPose;

            {
                std::unique_lock lock(data.objectPoseMutex);
                for (objpose::ObjectPose & object : data.objectPoses)
                {
                    std::string name = object.objectID.className();
                    if(name.find("apple") != name.npos )
                    {
                        objectPose = object;
                        break;

                    }
                }
            }

            candidate->objectType = objectPose.objectType;

            candidate->reachabilityInfo = new grasping::GraspCandidateReachabilityInfo();
            candidate->reachabilityInfo->reachable = true;

            candidate->sourceInfo = new grasping::GraspCandidateSourceInfo();
            candidate->sourceInfo->referenceObjectName = objectPose.objectID.str();

            candidate->executionHints = new grasping::GraspCandidateExecutionHints();

            // this is kind of hacky at the moment, the controller needs to know if its a side or top grasp to adapt some values,
            // if however, the controller would be refactored some more, the actionTrajectory could contain the necessary info
            //TODO refactor controller so it does not need to know whether it is a side or top approach or add side/top info to action
            if (action.unimanual.at(0).actionTrajectory->duration.toSecondsDouble() > 2.0)
            {
                candidate->executionHints->graspTrajectoryName = "RightTopOpen";
            }
            else
            {
                candidate->executionHints->graspTrajectoryName = "RightSideOpen";
            }

            grasping::ExecutePreposeInput prepose;
            prepose.side = candidate->side;
            executePrepose(prepose);

            {
                std::unique_lock lock(data.controlMutex);
                ControllerInfo control = data.controllerInfo;

                control.action = Action::Pick;

                control.grasp = candidate;

                control.liftVector = action.unimanual.at(0).retractPose->getPosition()->toEigen();

                control.side = side_names.from_name(control.grasp->side);

                data.controllerInfo = control;
                data.runExecution = true;
            }
            // Wait until finished
            manipulation::core::ExecutedAction exAction(action, armem::Time::Now());
            data.runFinished.wait();
            bool executed;
            // Collect output
            {
                std::unique_lock lock(data.controlMutex);
                executed = data.controllerOutput.executed;
            }
            exAction.endTime = std::make_unique<armem::Time>(armem::Time::Now());
            exAction.success = executed;
            exDto = armarx::manipulation::core::toAron(exAction);
        }
        else if (actionType == "place")
        {
            //executes place, retreat and preposeReverse currently
            grasping::ExecutePlaceInput input;
            input.side = action.unimanual.at(0).handedness->toString();
            input.placePositionGlobal = action.unimanual.at(0).hypothesis->framedPose.get()->pose->position;
            manipulation::core::ExecutedAction exAction(action, armem::Time::Now());

            bool executed = executePlace(input).executed;
            exAction.endTime = std::make_unique<armem::Time>(armem::Time::Now());
            exAction.success = executed;
            exDto = armarx::manipulation::core::toAron(exAction);
            grasping::ExecuteRetreatInput retreat;
            grasping::ExecutePreposeReverseInput preposeReverse;
            retreat.side = input.side;
            preposeReverse.side = input.side;
            executeRetreat(retreat);
            executePreposeReverse(preposeReverse);
        }
        else if (actionType == "Retreat")
        {
            grasping::ExecuteRetreatInput input;
            input.side = action.unimanual.at(0).handedness->toString();
            manipulation::core::ExecutedAction exAction(action, armem::Time::Now());

            bool executed = executeRetreat(input).executed;
            exAction.endTime = std::make_unique<armem::Time>(armem::Time::Now());
            exAction.success = executed;
            exDto = armarx::manipulation::core::toAron(exAction);
        }
        else if (actionType == "PreposeReverse")
        {
            grasping::ExecutePreposeReverseInput input;
            input.side = action.unimanual.at(0).handedness->toString();
            manipulation::core::ExecutedAction exAction(action, armem::Time::Now());

            bool executed = executePreposeReverse(input).executed;
            exAction.endTime = std::make_unique<armem::Time>(armem::Time::Now());
            exAction.success = executed;
            exDto = armarx::manipulation::core::toAron(exAction);
        }
        else
        {
            manipulation::core::ExecutedAction exAction(action, armem::Time::Now());

            exAction.comment = "Action ist not supported by " + getName();

            exAction.success = false;
            exDto = armarx::manipulation::core::toAron(exAction);
        }
        return exDto.toAronDTO();
    }

    void PickAndPlaceComponent::recover(const Ice::Current &)
    {

    }

    void PickAndPlaceComponent::abort(const Ice::Current &)
    {
        controller->stopExecution();
    }


    void
    PickAndPlaceComponent::saveObjectPoses()
    {
        ARMARX_TRACE;

        std::unique_lock lock(data.objectPoseMutex);

        data.setObjectPoses(getObjectPoses());

        gui.simoxFileCache.clear();

        for (auto& objPose : data.objectPoses)
        {
            gui.simoxFileCache.insert(objPose.objectID);
        }
        gui.recreateTabNextLoop = true;
    }


    void
    PickAndPlaceComponent::visualizeGraspCandidates()
    {
        ARMARX_TRACE;

        viz::Layer graspsLayer = arviz.layer("Grasps");
        viz::Layer graspsNonReachableLayer = arviz.layer("Grasps Non-Reachable");

        grasping::GraspCandidateDict candidates;

        try
        {
            std::unique_lock lock(data.controlMutex);
            ARMARX_INFO << "Querying for entity " << gui.graspCandidatesObject.objectID.str();
            candidates = graspReader.queryLatestGraspCandidateEntity(getName(), gui.graspCandidatesObject.objectID.str());
        }
        catch (armem::error::QueryFailed &e)
        {
            ARMARX_WARNING << "No Entries for entity " << gui.graspCandidatesObject.objectID.str()
                         << " in Memory " << graspMemoryName << ".";
            return;
        }

        for (auto& [id, candidate] : candidates)
        {
            bool isReachable = candidate->reachabilityInfo->reachable;
            viz::Color color = isReachable ? viz::Color::green() : viz::Color::red();
            if (id == gui.graspID)
            {
                color.a = 255;
            }
            else
            {
                color.a = 80;
            }


            Eigen::Matrix4f tcp2handRoot = fromIce(candidate->tcpPoseInHandRoot).inverse();

            Eigen::Matrix4f graspPose = PosePtr::dynamicCast(candidate->graspPose)->toEigen();
            std::string modelFile = "armar6_rt/robotmodel/Armar6-SH/Armar6-" + candidate->side + "Hand-v3.xml";
            viz::Robot hand = viz::Robot("Grasp_" + armem::MemoryID::fromString(id).getEntityInstanceID().str())
                              .file("armar6_rt", modelFile)
                              .pose(fromIce(candidate->robotPose) * graspPose * tcp2handRoot)
                              .overrideColor(color);

            if (isReachable)
            {
                graspsLayer.add(hand);
            }
            else
            {
                graspsNonReachableLayer.add(hand);
            }
        }
        arviz.commit({graspsLayer, graspsNonReachableLayer});
    }


    void
    PickAndPlaceComponent::visualizePlacePosition()
    {
        ARMARX_TRACE;

        Eigen::Vector3f placePosition = gui.placePosition;

        Eigen::Matrix4f robotPoseGlobal;
        RobotNameHelper::RobotArm arm;
        {
            std::unique_lock lock(data.controlMutex);
            synchronizeLocalClone(data.robot);
            robotPoseGlobal = data.robot->getGlobalPose();
            arm = robotNameHelper->getRobotArm(side_names.to_name(gui.side), data.robot);
        }

        VirtualRobot::RobotNodeSetPtr rns = arm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = arm.getTCP();

        Eigen::Matrix4f startPose = tcp->getPoseInRootFrame();
        // Make an intermediate pose which only moves in x,y not z
        Eigen::Vector3f placePositionRobot = (robotPoseGlobal.inverse() * placePosition.homogeneous()).head<3>();
        Eigen::Matrix4f intermediatePose = startPose;
        intermediatePose(0, 3) = placePositionRobot(0);
        intermediatePose(1, 3) = placePositionRobot(1);
        Eigen::Matrix4f placePose = intermediatePose;
        placePose(2, 3) = placePositionRobot(2);
        math::Helpers::Position(placePose) = placePositionRobot;

        std::vector<Eigen::Matrix4f> placeWaypoints;
        placeWaypoints.push_back(startPose);
        placeWaypoints.push_back(intermediatePose);
        placeWaypoints.push_back(placePose);

        SimpleDiffIK::Reachability reachability = SimpleDiffIK::CalculateReachability(placeWaypoints,
                Eigen::VectorXf::Zero(rns->getSize()),
                rns, tcp);

        // TODO: Make color red if not reachable!
        viz::Color color = reachability.reachable ? viz::Color::green() : viz::Color::red();

        viz::Layer placeLayer = arviz.layer("Place");
        viz::Sphere vizPlace = viz::Sphere("PlacePosition")
                               .position(placePosition)
                               .color(color)
                               .radius(20.0f);
        placeLayer.add(vizPlace);

        arviz.commit(placeLayer);
    }


    void
    PickAndPlaceComponent::visualizeLocalizePosition()
    {
        ARMARX_TRACE;
        std::unique_lock lock(data.controlMutex);
        synchronizeLocalClone(data.robot);

        RobotNameHelper::RobotArm arm = robotNameHelper->getRobotArm(side_names.to_name(gui.side), data.robot);
        VirtualRobot::RobotNodePtr tcp = arm.getTCP();

        Eigen::Matrix4f startPose = tcp->getPoseInRootFrame();
        (void) startPose;

        const LocalizeSingleConfig& localizeConfig = (gui.side == Side::Left) ? config.Localize.Left : config.Localize.Right;
        FramedPose localizeTcpPoseFramed = localizeConfig.TcpPose;
        Eigen::Matrix4f localizeTcpPoseGlobal = localizeTcpPoseFramed.toGlobalEigen(data.robot);

        viz::Layer localizeLayer = arviz.layer("Localize");
        localizeLayer.add(viz::Pose("LocalizePose").pose(localizeTcpPoseGlobal));
        // localizeLayer.add(viz::Pose("LocalizePoseFrame").pose(globalFramePose));

        arviz.commit(localizeLayer);
    }


    void
    PickAndPlaceComponent::visualizeObjectPoses()
    {
        ARMARX_TRACE;
        std::unique_lock lock(data.controlMutex);
        synchronizeLocalClone(data.robot);

        viz::Layer objectLayer = arviz.layer("Objects");
        viz::Layer robotLayer = arviz.layer("Robot");
        {
            std::unique_lock lock(data.objectPoseMutex);
            for (const auto& object : data.objectPoses)
            {
                Eigen::Matrix4f pose = object.objectPoseGlobal;

                if (object.attachment && !object.attachment->agentName.empty() && !object.attachment->frameName.empty())
                {
                    auto attachedToNode = data.robot->getRobotNode(object.attachment->frameName);

                    pose = attachedToNode->getGlobalPose() * object.attachment->poseInFrame;
                }

                PackageFileLocation location = gui.simoxFileCache.get(object.objectID);
                viz::Object viz = viz::Object(object.objectID.className())
                                  .file(location.package, location.relativePath)
                                  .pose(pose);

                objectLayer.add(viz);
            }

            std::map<std::string, float> jointValues = data.robot->getConfig()->getRobotNodeJointValueMap();
            viz::Robot robot_viz = viz::Robot("Armar6")
                                   .file("armar6_rt", "armar6_rt/robotmodel/Armar6-SH/Armar6-SH.xml")
                                   .pose(data.robot->getGlobalPose())
                                   .joints(jointValues);

            robotLayer.add(robot_viz);
        }

        arviz.commit({objectLayer, robotLayer});
    }


    void
    PickAndPlaceComponent::runControlLoop()
    {
        ARMARX_TRACE;
        //synchronizeLocalClone(robot);

        memoryNameSystem().subscribe(armem::MemoryID("Object", "Instance"),
                                     [&](const std::vector<armem::MemoryID>& snapshotIDs)
        {
            std::unique_lock lock(data.controlMutex);
            if (data.autoUpdateGraspsAllObjects)
            {
                data.objectsUpdated = true;

                const armem::client::QueryResult result = objectReader.queryMemoryIDs(snapshotIDs);
                if (result.success)
                {
                    Side side = data.controllerInfo.side;

                    result.memory.forEachInstance([this, &side](const armem::wm::EntityInstance& instance)
                    {
                        const objpose::ObjectPose objectPose =
                               aron::fromAron<objpose::ObjectPose>(
                                   armem::arondto::ObjectInstance::FromAron(instance.data()).pose);
                        if (not objectPose.isStatic)
                        {
                            getGraspCandidatesInPluginFormat(data, side, objectPose);
                        }
                    });
                }         
            }
        });

        Metronome metronome(Duration::MilliSeconds(20));

        while (!controlTask->isStopped())
        {
            if (data.runExecution)
            {
                ControllerInfo control;
                {
                    std::unique_lock lock(data.controlMutex);
                    control = data.controllerInfo;
                }

                switch (control.action)
                {
                    case Action::Retreat:
                        controller->executeRetreat(control.side);
                        break;
                    case Action::Prepose:
                        controller->executePrepose(control.side);
                        break;
                    case Action::PreposeReverse:
                        controller->executePreposeReverse(control.side);
                        break;
                    case Action::Push:
                        controller->executePush();
                        break;
                    case Action::Pick:
                    {
                        PickAndPlaceController::ExecutePickOutput output =
                            controller->executePick(control.side, control.grasp, control.liftVector,
                                                    control.graspTrajectories, data.objectPoses, data.objectPoseMutex);
                        {
                            std::unique_lock lock(data.controlMutex);
                            data.controllerOutput = {};
                            data.controllerOutput.executed = output.executed;
                        }
                    }
                    break;
                    case Action::Place:
                    {
                        controller->executePlace(control.side, control.placePositionGlobal, data.objectPoses,
                                                 data.objectPoseMutex);
                    }
                    break;
                    case Action::LocalizeObjectInHand:
                    {
                        objpose::ObjectPoseSeq acceptedObjectPoses = controller->executeLocalizeInHand(
                                    control.side, control.objectID, data.objectPoses, data.objectPoseMutex,
                                    control.initialWaitMS, control.maxDistanceFromTcp, control.minPoseUpdates,
                                    control.moveHead, control.pollFrequency, control.timeoutMS);
                        {
                            std::unique_lock lock(data.controlMutex);
                            data.controllerInfo.acceptedObjectPoses = acceptedObjectPoses;
                        }
                    }
                    break;
                    default:
                        ARMARX_WARNING << "Unknown action value: " << int(gui.action);
                        break;
                }

                data.runExecution = false;
                controller->resetStop();
                data.runFinished.notify();
            }
            else if (data.runSimulation)
            {
                // TODO

                data.runSimulation = false;
            }
            else
            {
                metronome.waitForNextTick();
            }
        }
    }


    void
    PickAndPlaceComponent::getGraspCandidatesInPluginFormat(
            DataControl& data, const Side& side, const objpose::ObjectPose& objectPose)
    {
        //data.controlMutex needs to be locked before invoking this method
        ARMARX_TRACE;

        ARMARX_CHECK_GREATER(data.controllerInfo.graspTrajectories.size(), 0)
                << "Expected grasp trajectories to not be empty.";

        synchronizeLocalClone(data.robot);

        grasping::GraspCandidateSeq candidates = data.boxGraspCandidatePlanner->calculateGraspCandidates(
                side, objectPose, data.robot, robotNameHelper, data.controllerInfo.graspTrajectories);

        graspWriter.commitGraspCandidateSeq(candidates, armem::Time::Now(), getName());

    }

    ARMARX_REGISTER_COMPONENT_EXECUTABLE(PickAndPlaceComponent, PickAndPlaceComponent::GetDefaultName());

    void DataControl::setObjectPoses(const objpose::ObjectPoseSeq& _objectPoses)
    {
        this->objectPoses = _objectPoses;

        this->objectPosesDynamic.clear();
        for (objpose::ObjectPose& objectPose : this->objectPoses)
        {
            if (not objectPose.isStatic)
            {
                this->objectPosesDynamic.emplace_back(&objectPose);
            }
        }
    }

}
