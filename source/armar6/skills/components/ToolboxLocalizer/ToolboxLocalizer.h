/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::ToolboxLocalizer
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/RemoteGuiComponentPlugin.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>
#include <VisionX/interface/components/SimpleLocation.h>

#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>

namespace armarx
{
    /**
     * @class ToolboxLocalizerPropertyDefinitions
     * @brief
     */
    class ToolboxLocalizerPropertyDefinitions :
        public visionx::PointCloudProcessorPropertyDefinitions
    {
    public:
        ToolboxLocalizerPropertyDefinitions(std::string prefix);
    };

    struct MinBox
    {
        Eigen::Matrix4f pose;
        Eigen::Vector3f extents;
    };
    struct ToolboxLocalizerProperties
    {
        Eigen::Vector3f Dimension = Eigen::Vector3f(500.0f, 220.0f, 250.0f);
        Eigen::Vector3f Tolerance = Eigen::Vector3f(50.0f, 22.0f, 25.0f);
        Eigen::Vector3f offsetPosition = Eigen::Vector3f(0, 0, 0);
        Eigen::Matrix3f offsetOrientation = Eigen::Matrix3f::Identity();
        bool DrawPolygons = false;
        bool DrawRectangles = false;
        bool DrawBoxes = true;
        bool PrintFilteredSegments = false;
        std::string agentName = "Armar6";
        std::string sourceNodeName = "Global";
    };

    /**
     * @defgroup Component-ToolboxLocalizer ToolboxLocalizer
     * @ingroup armar6_skills-Components
     * A description of the component ToolboxLocalizer.
     *
     * @class ToolboxLocalizer
     * @ingroup Component-ToolboxLocalizer
     * @brief Brief description of class ToolboxLocalizer.
     *
     * Detailed description of class ToolboxLocalizer.
     */
    class ToolboxLocalizer
        : virtual public armarx::Component,
          virtual public RemoteGuiComponentPluginUser,
          virtual public visionx::PointCloudProcessor,
          virtual public armarx::SimpleLocationInterface
    {
    public:
        /// @see armarx::ManagedIceObject::getDefaultName()
        virtual std::string getDefaultName() const override;

        memoryx::ObjectLocalizationResultList
        localizeObjectClasses(const memoryx::ObjectClassNameList& objectClassNames,
                              const Ice::Current& c = ::Ice::Current()) override;

        void getLocation(armarx::FramedOrientationBasePtr& orientation, armarx::FramedPositionBasePtr& position,
                         const Ice::Current& c = ::Ice::Current()) override;

    protected:
        /// @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
        void onInitPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
        void onConnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
        void onDisconnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
        void onExitPointCloudProcessor() override;


        /// @see visionx::PointCloudProcessor::process()
        void process() override;

        /// @see PropertyUser::createPropertyDefinitions()
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        void guiCreate();

        void guiUpdate(RemoteGui::TabProxy& tab);


    private:
        DebugDrawerTopic debugDrawer;

        mutable std::mutex propertiesMutex;
        mutable std::mutex localizedBoxMutex;

        MinBox localizedBox;
        double localizedTimestamp;
        ToolboxLocalizerProperties properties;
    };
}
