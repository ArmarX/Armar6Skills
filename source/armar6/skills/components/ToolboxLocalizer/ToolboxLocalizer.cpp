/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::ToolboxLocalizer
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ToolboxLocalizer.h"

#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>
#include <RobotAPI/interface/core/PoseBase.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <VirtualRobot/math/Helpers.h>

#include <boost/geometry.hpp>
#include <MemoryX/core/entity/ProbabilityMeasures.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>

namespace armarx
{

    namespace bg = boost::geometry;
    namespace trans = boost::geometry::strategy::transform;
    using PointT = pcl::PointXYZRGBL;

    namespace
    {

        typedef bg::model::point<float, 2, bg::cs::cartesian> point_t;
        typedef bg::model::multi_point<point_t> mpoint_t;
        typedef boost::geometry::model::polygon<point_t> polygon_t;

        Eigen::Vector3f toVector3f(point_t p, float z)
        {
            return Eigen::Vector3f(p.get<0>(), p.get<1>(), z);
        }

        std::vector<Eigen::Vector3f> polygonToPoints(polygon_t const& polygon, float z)
        {
            std::vector<Eigen::Vector3f> result;

            auto& outer = polygon.outer();
            auto outerBegin = outer.begin();
            auto outerEnd = outer.end();
            for (auto iter = outerBegin; iter + 1 != outerEnd; ++iter)
            {
                point_t p = *iter;

                result.push_back(toVector3f(p, z));
            }
            result.push_back(toVector3f(outer.back(), z));

            return result;
        }

        struct SegmentData
        {
            mpoint_t points;
            polygon_t convexHull;
            float minZ = FLT_MAX;
            float maxZ = -FLT_MAX;
        };

        std::map<int, SegmentData> createSegments(pcl::PointCloud<PointT> const& pointCloud)
        {
            std::map<int, SegmentData> segments;
            for (PointT const& p : pointCloud)
            {
                SegmentData& segment = segments[p.label];
                bg::append(segment.points, point_t(p.x, p.y));

                segment.minZ = std::min(segment.minZ, p.z);
                segment.maxZ = std::max(segment.maxZ, p.z);
            }
            return segments;
        }

        void drawConvexHull(int label, SegmentData const& segment, DebugDrawerTopic& debugDrawer, bool drawPolygons)
        {
            std::string convexHullName = "ConvexHull_" + std::to_string(label);
            if (drawPolygons)
            {
                auto debugPolygon = polygonToPoints(segment.convexHull, segment.maxZ);

                debugDrawer.drawPolygon(convexHullName, debugPolygon, {0.0, 1.0, 0.0, 1.0}, 2.0f);
            }
            else
            {
                debugDrawer.removePolygon(convexHullName);
            }
        }

        struct MinOrientedBox
        {
            boost::geometry::model::box<point_t> box;
            float angle = 0.0f;
        };

        std::optional<MinOrientedBox> fitMinOrientedRectangle(SegmentData const& segment)
        {
            auto& outer = segment.convexHull.outer();
            auto outerBegin = outer.begin();
            auto outerEnd = outer.end();

            float minArea = FLT_MAX;
            polygon_t rotatedHull;
            boost::geometry::model::box<point_t> minBox{};
            float minAngle = 0.0f;
            for (auto iter = outerBegin; iter + 1 != outerEnd; ++iter)
            {
                point_t p1 = *(iter + 0);
                point_t p2 = *(iter + 1);
                float angle = std::atan2(p2.get<1>() - p1.get<1>(), p2.get<0>() - p1.get<0>());

                trans::rotate_transformer<boost::geometry::radian, float, 2, 2> rotate(angle);
                if (!bg::transform(segment.convexHull, rotatedHull, rotate))
                {
                    ARMARX_WARNING << "Could not rotate convex hull";
                    continue;
                }

                boost::geometry::model::box<point_t> box{};
                boost::geometry::envelope(rotatedHull, box);
                float area = bg::area(box);

                if (area < minArea)
                {
                    minArea = area;
                    minBox = box;
                    minAngle = angle;
                }
            }

            if (minArea < FLT_MAX)
            {
                MinOrientedBox result;
                result.box = minBox;
                result.angle = minAngle;
                return result;
            }

            return std::nullopt;
        }

        polygon_t toPolygon(MinOrientedBox const& orientedRect)
        {
            point_t P0 = orientedRect.box.min_corner();
            point_t P2 = orientedRect.box.max_corner();
            point_t P1(P2.get<0>(), P0.get<1>());
            point_t P3(P0.get<0>(), P2.get<1>());
            polygon_t boxPolygon({{P0, P1, P2, P3}});

            polygon_t originalBoxPolygon;
            trans::rotate_transformer<boost::geometry::radian, float, 2, 2> rotate(-orientedRect.angle);
            bg::transform(boxPolygon, originalBoxPolygon, rotate);

            return originalBoxPolygon;
        }

        Eigen::Vector3f getCenter(polygon_t const& polygon, float z)
        {
            Eigen::Vector3f sum = Eigen::Vector3f::Zero();
            int count = 0;
            for (point_t p : polygon.outer())
            {
                sum += toVector3f(p, 0.0f);
                count += 1;
            }
            Eigen::Vector3f center = Eigen::Vector3f::Zero();
            if (count > 0)
            {
                center = sum / count;
            }
            center.z() = z;
            return center;
        }


        MinBox createMinBox(SegmentData const& segment, MinOrientedBox const& orientedRect,
                            polygon_t const& orientedRectPolygon)
        {
            MinBox result;

            Eigen::Vector3f center = getCenter(orientedRectPolygon, 0.5f * (segment.maxZ + segment.minZ));

            Eigen::Vector2f minCorner(orientedRect.box.min_corner().get<0>(), orientedRect.box.min_corner().get<1>());
            Eigen::Vector2f maxCorner(orientedRect.box.max_corner().get<0>(), orientedRect.box.max_corner().get<1>());
            Eigen::Vector2f dimension = maxCorner - minCorner;

            result.extents = Eigen::Vector3f(dimension.x(), dimension.y(), segment.maxZ - segment.minZ);

            result.pose = Eigen::Matrix4f::Identity();
            math::Helpers::Position(result.pose) = center;
            math::Helpers::Orientation(result.pose) = Eigen::AngleAxisf(orientedRect.angle,
                    Eigen::Vector3f::UnitZ()).toRotationMatrix();

            return result;
        }


    }

    ToolboxLocalizerPropertyDefinitions::ToolboxLocalizerPropertyDefinitions(std::string prefix) :
        visionx::PointCloudProcessorPropertyDefinitions(prefix)
    {
        defineOptionalProperty<Eigen::Vector3f>("Dimension", Eigen::Vector3f(580.0f, 250.0f, 300.0f),
                                                "Dimension of the toolbox");
        defineOptionalProperty<Eigen::Vector3f>("Tolerance", Eigen::Vector3f(58.0f, 25.0f, 100.0f),
                                                "Tolerance for dimension while localizing");

        defineOptionalProperty<bool>("DrawPolygons", false, "Draw 2D projected polygons for toolbox candidates");
        defineOptionalProperty<bool>("DrawRectangles", false, "Draw 2D projected rectangles for toolbox candidates");
        defineOptionalProperty<bool>("DrawBoxes", true, "Draw 3D rectangles for toolbox candidates");
        defineOptionalProperty<bool>("PrintFilteredSegments", true, "Draw 3D rectangles for toolbox candidates");

        // object localization properties
        defineOptionalProperty<std::string>("AgentName", "Armar6",
                                            "Name of the agent for which the sensor values are provided");
        defineOptionalProperty<std::string>("SourceNodeName", "Global",
                                            "the robot node to use as source coordinate system for the captured point clouds");
        defineOptionalPropertyVector<Eigen::Vector3f>("PositionOffset", Eigen::Vector3f(0.0f, 0.0f, 0.0f),
                "Offset of the Position of the Box.", ',');
        defineOptionalPropertyVector<Eigen::Vector3f>("OrientationOffset", Eigen::Vector3f(0.0f, 0.0f, 0.0f),
                "Orientation Offset of the fitted box in degrees.", ',');
    }


    std::string ToolboxLocalizer::getDefaultName() const
    {
        return "ToolboxLocalizer";
    }


    void ToolboxLocalizer::onInitPointCloudProcessor()
    {
        debugDrawer.offeringTopic(*this);

        properties.Dimension = getProperty<decltype(properties.Dimension)>("Dimension");
        properties.Tolerance = getProperty<decltype(properties.Tolerance)>("Tolerance");

        properties.DrawPolygons = getProperty<decltype(properties.DrawPolygons)>("DrawPolygons");
        properties.DrawRectangles = getProperty<decltype(properties.DrawRectangles)>("DrawRectangles");
        properties.DrawBoxes = getProperty<decltype(properties.DrawBoxes)>("DrawBoxes");
        properties.offsetPosition = getProperty<decltype(properties.offsetPosition)>("PositionOffset");
        Eigen::Vector3f rpy = getProperty<Eigen::Vector3f>("OrientationOffset");
        properties.offsetOrientation = Eigen::AngleAxisf(rpy[0], Eigen::Vector3f::UnitX())
                                       * Eigen::AngleAxisf(rpy[1], Eigen::Vector3f::UnitY())
                                       * Eigen::AngleAxisf(rpy[2], Eigen::Vector3f::UnitZ());
        properties.agentName = getProperty<std::string>("AgentName");
        properties.sourceNodeName = getProperty<std::string>("SourceNodeName");
    }


    void ToolboxLocalizer::onConnectPointCloudProcessor()
    {
        debugDrawer.getTopic(*this);

        guiCreate();
    }


    void ToolboxLocalizer::onDisconnectPointCloudProcessor()
    {

    }


    void ToolboxLocalizer::onExitPointCloudProcessor()
    {

    }

    void ToolboxLocalizer::process()
    {
        // Fetch input point cloud.
        pcl::PointCloud<PointT>::Ptr inputCloud(new pcl::PointCloud<PointT>());

        if (waitForPointClouds())
        {
            getPointClouds(inputCloud);
        }
        else
        {
            ARMARX_VERBOSE << "Timeout or error while waiting for point cloud data.";
            return;
        }

        ToolboxLocalizerProperties props;
        {
            std::unique_lock<std::mutex> lock(propertiesMutex);
            props = properties;
        }

        std::map<int, SegmentData> segments = createSegments(*inputCloud);

        // Build 2D convex hull for every segment
        for (auto& pair : segments)
        {
            SegmentData& segment = pair.second;
            bg::convex_hull(segment.points, segment.convexHull);
            drawConvexHull(pair.first, segment, debugDrawer, props.DrawPolygons);
        }

        // Find box orientation and check dimensions
        for (auto& pair : segments)
        {
            SegmentData& segment = pair.second;

            auto minOrientedRect = fitMinOrientedRectangle(segment);
            if (!minOrientedRect)
            {
                ARMARX_INFO << "Skipping segment: " << pair.first
                            << ", because no min oriented rectangle could be fitted";
                continue;
            }

            polygon_t orientedRectPolygon = toPolygon(*minOrientedRect);

            std::string rectangleName = "Box2D_" + std::to_string(pair.first);
            if (props.DrawRectangles)
            {
                auto boxPoly = polygonToPoints(orientedRectPolygon, segment.maxZ + 100.0f);
                debugDrawer.drawPolygon(rectangleName, boxPoly, {0.0, 0.0, 1.0, 1.0}, 2.0f);
            }
            else
            {
                debugDrawer.removePolygon(rectangleName);
            }

            MinBox minBox = createMinBox(segment, *minOrientedRect, orientedRectPolygon);

            DrawColor drawColor;

            // Compare extents to tolerances
            Eigen::Vector3f minExtents = props.Dimension - props.Tolerance;
            Eigen::Vector3f maxExtents = props.Dimension + props.Tolerance;
            if (minExtents.x() <= minBox.extents.x() && minBox.extents.x() <= maxExtents.x()
                && minExtents.y() <= minBox.extents.y() && minBox.extents.y() <= maxExtents.y()
                && minExtents.z() <= minBox.extents.z() && minBox.extents.z() <= maxExtents.z())
            {
                drawColor = {0.0, 0.0, 1.0, 0.3};
            }
            else
            {
                ARMARX_INFO << "Segment[" << pair.first << "]:"
                            << "\nExtents: " << minBox.extents
                            << "\nMin: " << minExtents
                            << "\nMax: " << maxExtents;

                drawColor = {1.0, 0.0, 0.0, 0.3};
            }

            {
                ARMARX_INFO << "Toolbox was localized";
                std::lock_guard g{localizedBoxMutex};
                localizedBox = minBox;
                localizedTimestamp = inputCloud->header.stamp;
            }
            std::string boxName = "Box" + std::to_string(pair.first);
            if (props.DrawBoxes)
            {
                debugDrawer.drawBox(boxName, minBox.pose, minBox.extents, drawColor);
            }
            else
            {
                debugDrawer.removeBox(boxName);
            }
        }
    }


    armarx::PropertyDefinitionsPtr ToolboxLocalizer::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new ToolboxLocalizerPropertyDefinitions(
                getConfigIdentifier()));
    }

    void ToolboxLocalizer::guiCreate()
    {
        using namespace RemoteGui;


        auto dimensionSpinBox = makeVector3fSpinBoxes("Dimension")
                                .min(100.0f)
                                .max(600.0f)
                                .steps(50)
                                .value(properties.Dimension);

        auto toleranceSpinBox = makeVector3fSpinBoxes("Tolerance")
                                .min(10.0f)
                                .max(60.0f)
                                .steps(50)
                                .value(properties.Tolerance);

        auto positionOffsetSpinBox = makeVector3fSpinBoxes("Position")
                                     .min(-300.0f)
                                     .max(300.0f)
                                     .steps(300)
                                     .value(properties.offsetPosition);

        auto rotationOffsetSpinBox = makeVector3fSpinBoxes("Orientation")
                                     .min(0, -180.0f, -180.0f)
                                     .max(180.0f)
                                     .steps(360)
                                     .value(properties.offsetOrientation.eulerAngles(0, 1, 2));

        auto widget_DrawPolygons = makeCheckBox("DrawPolygons")
                                   .label("DrawPolygons")
                                   .value(properties.DrawPolygons);
        auto widget_DrawRectangles = makeCheckBox("DrawRectangles")
                                     .label("DrawRectangles")
                                     .value(properties.DrawRectangles);
        auto widget_DrawBoxes = makeCheckBox("DrawBoxes")
                                .label("DrawBoxes")
                                .value(properties.DrawBoxes);
        auto widget_PrintFilteredSegments = makeCheckBox("PrintFilteredSegments")
                                            .label("PrintFilteredSegments")
                                            .value(properties.PrintFilteredSegments);
        auto hLayout = makeHBoxLayout()
                       .addChildren(
        {widget_DrawPolygons, widget_DrawRectangles, widget_DrawBoxes, widget_PrintFilteredSegments});

        auto layout = makeVBoxLayout()
                      .addChildren({dimensionSpinBox, toleranceSpinBox, hLayout, positionOffsetSpinBox, rotationOffsetSpinBox});

        createOrUpdateRemoteGuiTab(getName(), layout, [this](TabProxy & tab)
        {
            tab.receiveUpdates();
            guiUpdate(tab);
        });
    }

    void ToolboxLocalizer::guiUpdate(RemoteGui::TabProxy& tab)
    {
        std::unique_lock<std::mutex> lock(propertiesMutex);

        properties.Dimension = tab.getValue<Eigen::Vector3f>("Dimension").get();
        properties.Tolerance = tab.getValue<Eigen::Vector3f>("Tolerance").get();

        properties.offsetPosition = tab.getValue<Eigen::Vector3f>("Position").get();
        Eigen::Vector3f rpy = tab.getValue<Eigen::Vector3f>("Orientation").get() / 180.0f * M_PI;
        properties.offsetOrientation = Eigen::AngleAxisf(rpy[0], Eigen::Vector3f::UnitX())
                                       * Eigen::AngleAxisf(rpy[1], Eigen::Vector3f::UnitY())
                                       * Eigen::AngleAxisf(rpy[2], Eigen::Vector3f::UnitZ());

        properties.DrawPolygons = tab.getValue<bool>("DrawPolygons").get();
        properties.DrawRectangles = tab.getValue<bool>("DrawRectangles").get();
        properties.DrawBoxes = tab.getValue<bool>("DrawBoxes").get();
        properties.PrintFilteredSegments = tab.getValue<bool>("PrintFilteredSegments").get();
    }

    memoryx::ObjectLocalizationResultList
    ToolboxLocalizer::localizeObjectClasses(const memoryx::ObjectClassNameList& objectClassNames,
                                            const Ice::Current& c)
    {
        ARMARX_CHECK_EQUAL(objectClassNames.size(), 1);
        ARMARX_INFO << "ToolboxLocalization requested";
        memoryx::ObjectLocalizationResultList resultList;
        memoryx::ObjectLocalizationResult result;
        Eigen::Vector3f extents;
        Eigen::Matrix4f pose;
        {
            std::lock_guard g{localizedBoxMutex};
            pose = localizedBox.pose;
            extents = localizedBox.extents;
        }
        {
            std::lock_guard g{propertiesMutex};
            Eigen::Vector3f position = pose.block<3, 1>(0, 3);
            position[2] -= extents[2] / 2.0f;
            Eigen::Matrix3f orientation = pose.block<3, 3>(0, 0);
            ARMARX_INFO << "original Orientation: \n" << orientation;
            ARMARX_INFO << "offset Orientation: \n" << properties.offsetOrientation;
            position += properties.offsetPosition;
            orientation *= properties.offsetOrientation;
            result.position = new FramedPosition(position, properties.sourceNodeName, properties.agentName);
            result.orientation = new FramedOrientation(orientation, properties.sourceNodeName, properties.agentName);
        }
        result.objectClassName = objectClassNames.at(0);
        result.recognitionCertainty = 0.6;
        result.timeStamp = new TimestampVariant(localizedTimestamp);

        Eigen::Vector3f cov(10, 10, 10000);
        result.positionNoise = new memoryx::MultivariateNormalDistribution(Eigen::Vector3f::Zero(), cov.asDiagonal());
        resultList.push_back(result);
        return resultList;
    }

    void ToolboxLocalizer::getLocation(FramedOrientationBasePtr& orientation, FramedPositionBasePtr& position,
                                       const Ice::Current& c)
    {
        throw LocalException("NYI");
    }
}
