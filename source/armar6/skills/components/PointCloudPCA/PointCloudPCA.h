/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PointCloudPCA
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once



#include <armar6/skills/interface/PointCloudPCAInterface.h>
#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/components/DebugDrawer/DebugDrawerHelper.h>
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

#include <VirtualRobot/Robot.h>

#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/passthrough.h>

#include <mutex>


namespace armarx
{


    /**
     * @class PointCloudPCAPropertyDefinitions
     * @brief
     */
    class PointCloudPCAPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        PointCloudPCAPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the debug observer that should be used");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the debug drawer topic that should be used");

            defineOptionalProperty<std::string>("PointCloudFormat", "XYZRGBA", "Format of the input and output point cloud (XYZRGBA, XYZL, XYZRGBL)");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
            defineOptionalProperty<std::string>("providerName", "OpenNIPointCloudProvider", "name of the point cloud provider");
            defineOptionalProperty<std::string>("sourceFrameName", "DepthCamera", "The source frame name");

            defineOptionalProperty<float>("BoundingBoxZ1", 975.f, "");
            defineOptionalProperty<float>("BoundingBoxY1", 0.f, "");
            defineOptionalProperty<float>("BoundingBoxY2", 1300.f, "");

            defineOptionalProperty<float>("BoundingBoxX1", -200, "");
            defineOptionalProperty<float>("BoundingBoxX2", 200, "");

            defineOptionalProperty<bool>("FlattenInZ", true, "");

            defineOptionalProperty<std::string>("RemoteGuiName", "RemoteGuiProvider", "Name of the remote gui");
        }
    };

    /**
     * @defgroup Component-PointCloudPCA PointCloudPCA
     * @ingroup armar6_skills-Components
     * A description of the component PointCloudPCA.
     *
     * @class PointCloudPCA
     * @ingroup Component-PointCloudPCA
     * @brief Brief description of class PointCloudPCA.
     *
     * Detailed description of class PointCloudPCA.
     */
    class PointCloudPCA :
        virtual public armarx::PointCloudPCAInterface,
        virtual public visionx::PointCloudProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "PointCloudPCA";
        }

        struct BoundingBox
        {
            float x1 = -10000;
            float x2 = 10000;
            float y1 = -10000;
            float y2 = 10000;
            float z1 = -10000;
            float z2 = 10000;

            bool isInside(float x, float y, float z);
        };

    protected:
        /**
         * @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
         */
        void onInitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
         */
        void onConnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
         */
        void onDisconnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
         */
        void onExitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::process()
         */
        void process() override;

        template<typename PointType> void processPointCloud();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        PCAResult3D getResult(const Ice::Current&) override;
        CapturedPointSeq getSegmentedPoints(const Ice::Current&) override;

    private:

        std::mutex resultMutex;
        std::mutex capturedPointsMutex;
        std::string pointCloudFormat;

        VirtualRobot::RobotPtr localRobot;

        armarx::RobotStateComponentInterfacePrx robotStateComponent;
        DebugObserverInterfacePrx debugObserver;
        DebugDrawerInterfacePrx debugDrawerTopic;

        std::string sourceFrameName;
        std::string providerName;

        BoundingBox boundingBox;
        bool flattenInZ;

        PCAResult3D result;

        CapturedPointSeq capturedPoints;

        DebugDrawerHelperPtr debugDrawerHelper;

        RemoteGuiInterfacePrx remoteGuiPrx;

        SimplePeriodicTask<>::pointer_type guiTask;

        RemoteGui::TabProxy guiTab;
    };
}
