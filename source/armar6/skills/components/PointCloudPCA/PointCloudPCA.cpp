/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::PointCloudPCA
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PointCloudPCA.h"

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <RobotAPI/libraries/core/math/MathUtils.h>
#include <RobotAPI/libraries/core/math/MatrixHelpers.h>
#include <RobotAPI/libraries/core/math/SVD.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>


#include <memory>

using namespace armarx;
using namespace armarx::math;



void PointCloudPCA::onInitPointCloudProcessor()
{
    providerName = getProperty<std::string>("providerName").getValue();
    sourceFrameName = getProperty<std::string>("sourceFrameName").getValue();
    pointCloudFormat = getProperty<std::string>("PointCloudFormat").getValue();

    boundingBox.z1 = getProperty<float>("BoundingBoxZ1").getValue();
    boundingBox.y1 = getProperty<float>("BoundingBoxY1").getValue();
    boundingBox.y2 = getProperty<float>("BoundingBoxY2").getValue();
    boundingBox.x1 = getProperty<float>("BoundingBoxX1").getValue();
    boundingBox.x2 = getProperty<float>("BoundingBoxX2").getValue();

    flattenInZ = getProperty<bool>("FlattenInZ").getValue();

    usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
    usingPointCloudProvider(providerName);

    offeringTopic(getProperty<std::string>("DebugObserverName").getValue());
    offeringTopic(getProperty<std::string>("DebugDrawerTopicName").getValue());
    usingProxy(getProperty<std::string>("RemoteGuiName").getValue());
}

void PointCloudPCA::onConnectPointCloudProcessor()
{
    robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
    localRobot = RemoteRobot::createLocalCloneFromFile(robotStateComponent, VirtualRobot::RobotIO::eStructure);
    debugObserver = getTopic<DebugObserverInterfacePrx>(getProperty<std::string>("DebugObserverName").getValue());
    debugDrawerTopic = getTopic<DebugDrawerInterfacePrx>(getProperty<std::string>("DebugDrawerTopicName").getValue());
    remoteGuiPrx = getProxy<RemoteGuiInterfacePrx>(getProperty<std::string>("RemoteGuiName").getValue());

    debugDrawerHelper = std::make_shared<DebugDrawerHelper>(debugDrawerTopic, "PointCloudPCA", localRobot);

    enableResultPointClouds<pcl::PointXYZRGBA>("PCA_live");

    enableResultPointClouds<pcl::PointXYZRGBA>("PCA_captured");
    if (pointCloudFormat == "XYZRGBA")
    {
        enableResultPointClouds<pcl::PointXYZRGBA>("PCA_segmented");
    }
    else if (pointCloudFormat == "XYZL")
    {
        enableResultPointClouds<pcl::PointXYZL>("PCA_segmented");
    }
    else if (pointCloudFormat == "XYZRGBL")
    {
        enableResultPointClouds<pcl::PointXYZRGBL>("PCA_segmented");
    }
    else
    {
        ARMARX_ERROR << "Could not initialize point cloud, because format '" << pointCloudFormat << "' is unknown";
    }

    RemoteGui::detail::VBoxLayoutBuilder rootLayoutBuilder = RemoteGui::makeVBoxLayout();

    auto makeSlider = [&](std::string name, int min, int max, float val)
    {
        rootLayoutBuilder.addChild(
            RemoteGui::makeHBoxLayout()
            .addChild(RemoteGui::makeTextLabel(name))
            .addChild(RemoteGui::makeFloatSlider(name).min(min).max(max).value(val))
            .addChild(RemoteGui::makeFloatSpinBox(name + "_spin").min(min).max(max).value(val))
        );
    };



    // ValueProxy<float> x1Slider = makeSlider ...
    makeSlider("x1", -600, 600, boundingBox.x1);
    makeSlider("x2", -600, 600, boundingBox.x2);
    makeSlider("y1", 0, 1500, boundingBox.y1);
    makeSlider("y2", 0, 1500, boundingBox.y2);
    makeSlider("z1", 300, 1500, boundingBox.z1);
    rootLayoutBuilder.addChild(RemoteGui::makeHBoxLayout()
                               .addChild(RemoteGui::makeTextLabel("FlattenInZ"))
                               .addChild(RemoteGui::makeCheckBox("FlattenInZ").value(flattenInZ))
                               .addChild(new RemoteGui::HSpacer()));
    rootLayoutBuilder.addChild(new RemoteGui::VSpacer());

    guiTask = new SimplePeriodicTask<>([&]()
    {
        guiTab.receiveUpdates();
        boundingBox.x1 = guiTab.getValue<float>("x1").get();
        boundingBox.x2 = guiTab.getValue<float>("x2").get();
        boundingBox.y1 = guiTab.getValue<float>("y1").get();
        boundingBox.y2 = guiTab.getValue<float>("y2").get();
        boundingBox.z1 = guiTab.getValue<float>("z1").get();
        flattenInZ = guiTab.getValue<bool>("FlattenInZ").get();

        guiTab.getValue<float>("x1_spin").set(boundingBox.x1);
        guiTab.getValue<float>("x2_spin").set(boundingBox.x2);
        guiTab.getValue<float>("y1_spin").set(boundingBox.y1);
        guiTab.getValue<float>("y2_spin").set(boundingBox.y2);
        guiTab.getValue<float>("z1_spin").set(boundingBox.z1);

        guiTab.sendUpdates();

        ARMARX_IMPORTANT << VAROUT(boundingBox.z1);
    }, 10);

    RemoteGui::WidgetPtr rootLayout = rootLayoutBuilder;

    remoteGuiPrx->createTab("PointCloudPCA", rootLayout);
    guiTab = RemoteGui::TabProxy(remoteGuiPrx, "PointCloudPCA");

    guiTask->start();
}

void PointCloudPCA::onDisconnectPointCloudProcessor()
{
    guiTask->stop();
    guiTask = nullptr;
}

void PointCloudPCA::onExitPointCloudProcessor()
{

}

void PointCloudPCA::process()
{
    if (pointCloudFormat == "XYZRGBA")
    {
        processPointCloud<pcl::PointXYZRGBA>();
    }
    else if (pointCloudFormat == "XYZL")
    {
        processPointCloud<pcl::PointXYZL>();
    }
    else if (pointCloudFormat == "XYZRGBL")
    {
        processPointCloud<pcl::PointXYZRGBL>();
    }
    else
    {
        ARMARX_ERROR << "Could not process point cloud, because format '" << pointCloudFormat << "' is unknown";
    }
}


template<typename PointType>
void PointCloudPCA::processPointCloud()
{
    typename pcl::PointCloud<PointType>::Ptr inputCloudPtr(new pcl::PointCloud<PointType>());
    typename pcl::PointCloud<PointType>::Ptr tmpCloudPtr(new pcl::PointCloud<PointType>());
    //typename pcl::PointCloud<PointType>::Ptr outputCloudPtr(new pcl::PointCloud<PointType>());
    typename pcl::PointCloud<pcl::PointXYZRGBA>::Ptr outputCloudPtr(new pcl::PointCloud<pcl::PointXYZRGBA>());
    //capturedCloudPtr.reset(new pcl::PointCloud<pcl::PointXYZRGBA>());
    typename pcl::PointCloud<PointType>::Ptr segmentedCloudPtr(new pcl::PointCloud<PointType>());

    if (!waitForPointClouds(providerName, 10000))
    {
        ARMARX_INFO << "Timeout or error while waiting for point cloud data" << armarx::flush;
        return;
    }
    else
    {
        getPointClouds(providerName, inputCloudPtr);
    }


    visionx::MetaPointCloudFormatPtr format = getPointCloudFormat(providerName);

    //ARMARX_IMPORTANT << "processPointCloud: " << inputCloudPtr->width << "x" << inputCloudPtr->height;

    RemoteRobot::synchronizeLocalCloneToTimestamp(localRobot, robotStateComponent, format->timeProvided);


    Eigen::Matrix4f sourceFrameInRootFrame = Eigen::Matrix4f::Identity();
    if (sourceFrameName == "Global")
    {
        sourceFrameInRootFrame = localRobot->getRootNode()->getGlobalPose().inverse();
    }
    else
    {
        sourceFrameInRootFrame = localRobot->getRobotNode(sourceFrameName)->getPoseInRootFrame();
    }


    // Transform to global frame
    pcl::transformPointCloud(*inputCloudPtr, *tmpCloudPtr, sourceFrameInRootFrame);
    tmpCloudPtr.swap(inputCloudPtr);

    tmpCloudPtr->clear();
    std::vector<Eigen::Vector3f> segmentedPoints;


    for (const PointType& point : inputCloudPtr->points)
    {
        pcl::PointXYZRGBA p;
        p.x = point.x;
        p.y = point.y;
        p.z = point.z;
        p.r = 0;
        p.g = 0;
        p.b = 0;
        p.a = 255;
        if (boundingBox.isInside(point.x, point.y, point.z))
        {
            p.g = 255;
            segmentedPoints.push_back(Eigen::Vector3f(point.x, point.y, point.z));
            segmentedCloudPtr->points.push_back(point);
        }
        else
        {
            p.r = 255;
        }
        outputCloudPtr->push_back(p);
    }

    {
        std::unique_lock lock(capturedPointsMutex);
        capturedPoints.clear();
        for (const Eigen::Vector3f& p : segmentedPoints)
        {
            capturedPoints.push_back(CapturedPoint {p(0), p(1), p(2)});
        }
    }


    Eigen::Vector3f center = Eigen::Vector3f::Zero();
    Eigen::Vector3f v1 = Eigen::Vector3f::Zero();
    Eigen::Vector3f v2 = Eigen::Vector3f::Zero();
    Eigen::Vector3f v3 = Eigen::Vector3f::Zero();

    if (segmentedPoints.size() < 10)
    {
        //ARMARX_WARNING << deactivateSpam(1) << "too few points for PCA!";
    }
    else
    {
        Eigen::MatrixXf pointsAsMatrix(3, segmentedPoints.size());
        for (size_t i = 0; i < segmentedPoints.size(); i++)
        {
            pointsAsMatrix.block<3, 1>(0, i) = segmentedPoints.at(i);
        }

        center = MatrixHelpers::CalculateCog3D(pointsAsMatrix);

        Eigen::MatrixXf pointsNoMean = MatrixHelpers::SubtractVectorFromAllColumns3D(pointsAsMatrix, center);

        if (flattenInZ)
        {
            MatrixHelpers::SetRowToValue(pointsNoMean, 2, 0);
        }

        SVD svd(pointsNoMean);

        //primaryVector = svd.getLeftSingularVector3D(0); // approach vector is biggest left singular vector
        //primaryVector.normalize();


        //ARMARX_INFO << "singularValues: " << svd.singularValues.transpose();

        v1 = svd.getLeftSingularVector3D(0).normalized();
        v2 = svd.getLeftSingularVector3D(1).normalized();
        v3 = svd.getLeftSingularVector3D(2).normalized();
    }

    typename pcl::PointCloud<pcl::PointXYZRGBA>::Ptr resultCloud = outputCloudPtr;
    //ARMARX_INFO << deactivateSpam(1) << "Input cloud " << inputSize << ", filtered cloud " << resultCloud->points.size();
    //ARMARX_INFO << deactivateSpam(1) << "center (in root) =  " << center.transpose() << " primary vector: "  << v1.transpose();

    debugObserver->setDebugDatafield("PointCloudPCA", "center_x", new Variant(center(0)));
    debugObserver->setDebugDatafield("PointCloudPCA", "center_y", new Variant(center(1)));
    debugObserver->setDebugDatafield("PointCloudPCA", "center_z", new Variant(center(2)));

    debugObserver->setDebugDatafield("PointCloudPCA", "v1_x", new Variant(v1(0)));
    debugObserver->setDebugDatafield("PointCloudPCA", "v1_y", new Variant(v1(1)));
    debugObserver->setDebugDatafield("PointCloudPCA", "v1_z", new Variant(v1(2)));

    {
        std::unique_lock lock(resultMutex);
        result.inputPointCount = resultCloud->points.size();
        result.center = new Vector3(center);
        result.v1 = new Vector3(v1);
        result.v2 = new Vector3(v2);
        result.v3 = new Vector3(v3);
    }

    debugDrawerHelper->drawArrow("v1", center, v1, DrawColor {1, 0, 0, 1}, 50, 3);
    debugDrawerHelper->drawArrow("v1_neg", center, Eigen::Vector3f(-v1), DrawColor {1, 0, 0, 1}, 50, 3);
    debugDrawerHelper->drawArrow("v2", center, v2, DrawColor {0, 1, 0, 1}, 50, 3);
    debugDrawerHelper->drawArrow("v2_neg", center, Eigen::Vector3f(-v2), DrawColor {0, 1, 0, 1}, 50, 3);
    if (!flattenInZ)
    {
        debugDrawerHelper->drawArrow("v3", center, v3, DrawColor {0, 0, 1, 1}, 50, 3);
        debugDrawerHelper->drawArrow("v3_neg", center, Eigen::Vector3f(-v3), DrawColor {0, 0, 1, 1}, 50, 3);
    }


    //debugDrawerTopic->setArrowVisu("PointCloudPCA", "v1", result.center, new Vector3(v1), DrawColor {1, 0, 0, 1}, 50, 3);
    //debugDrawerTopic->setArrowVisu("PointCloudPCA", "v1_neg", result.center, new Vector3(Eigen::Vector3f(-v1)), DrawColor {1, 0, 0, 1}, 50, 3);
    //debugDrawerTopic->setArrowVisu("PointCloudPCA", "v2", result.center, new Vector3(v2), DrawColor {0, 1, 0, 1}, 50, 3);
    //debugDrawerTopic->setArrowVisu("PointCloudPCA", "v2_neg", result.center, new Vector3(Eigen::Vector3f(-v2)), DrawColor {0, 1, 0, 1}, 50, 3);


    provideResultPointClouds(resultCloud, "PCA_live");
    //provideResultPointClouds(capturedCloudPtr, "PCA_captured");
    //provideResultPointClouds(segmentedCloudPtr, "PCA_segmented");
}

armarx::PropertyDefinitionsPtr PointCloudPCA::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new PointCloudPCAPropertyDefinitions(
            getConfigIdentifier()));
}


bool PointCloudPCA::BoundingBox::isInside(float x, float y, float z)
{
    return x >= x1 && x <= x2 &&
           y >= y1 && y <= y2 &&
           z >= z1 && z <= z2;
}


PCAResult3D armarx::PointCloudPCA::getResult(const Ice::Current&)
{
    std::unique_lock lock(resultMutex);
    return result;
}


CapturedPointSeq armarx::PointCloudPCA::getSegmentedPoints(const Ice::Current&)
{
    std::unique_lock lock(capturedPointsMutex);
    return capturedPoints;
}
