/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::DummyFingerJointReporter
 * @author     Fabian Paus ( fabian dot paus at gmail dot com )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/units/KinematicUnitInterface.h>

#include <set>
#include <map>

namespace armarx
{
    /**
     * @class DummyFingerJointReporterPropertyDefinitions
     * @brief
     */
    class DummyFingerJointReporterPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        DummyFingerJointReporterPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("RobotNodeSetName", "Robot", "Name of the robot node set to be used");
            defineOptionalProperty<std::string>("SourceJoints",
                                                "RightHandThumb;RightHandFingers;LeftHandThumb;LeftHandFingers",
                                                "Semicolon separated list of source joints");
            defineOptionalProperty<std::string>("TargetJoints", "\
                    Thumb R 1 Joint,Thumb R 2 Joint;\
                    Pinky R 1 Joint,Pinky R 2 Joint,Pinky R 3 Joint,Ring R 1 Joint,Ring R 2 Joint,Ring R 3 Joint,Middle R 1 Joint,Middle R 2 Joint,Middle R 3 Joint,Index R 1 Joint,Index R 2 Joint,Index R 3 Joint;\
                    Thumb L 1 Joint,Thumb L 2 Joint;\
                    Pinky L 1 Joint,Pinky L 2 Joint,Pinky L 3 Joint,Ling L 1 Joint,Ling L 2 Joint,Ling L 3 Joint,Middle L 1 Joint,Middle L 2 Joint,Middle L 3 Joint,Index L 1 Joint,Index L 2 Joint,Index L 3 Joint",
                                                "Semicolon separated list of target joint lists (correspond to SourceJoints)");
            defineOptionalProperty<float>("ConversionFactor", 1.0f, "Conversion factor for joint values");
        }
    };

    /**
     * @defgroup Component-DummyFingerJointReporter DummyFingerJointReporter
     * @ingroup armar6_skills-Components
     * A description of the component DummyFingerJointReporter.
     *
     * @class DummyFingerJointReporter
     * @ingroup Component-DummyFingerJointReporter
     * @brief Brief description of class DummyFingerJointReporter.
     *
     * Detailed description of class DummyFingerJointReporter.
     */
    class DummyFingerJointReporter
        : virtual public armarx::Component
        , virtual public KinematicUnitListener

    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "DummyFingerJointReporter";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // KinematicUnitListener interface
        void reportControlModeChanged(const NameControlModeMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointAngles(const NameValueMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointVelocities(const NameValueMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointTorques(const NameValueMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointAccelerations(const NameValueMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointCurrents(const NameValueMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointMotorTemperatures(const NameValueMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointStatuses(const NameStatusMap&, Ice::Long, bool, const Ice::Current&) override;

    private:
        std::string kinematicUnitTopicName;
        armarx::KinematicUnitListenerPrx kinematicUnitTopic;

        std::map<std::string, std::set<std::string>> sourceToTargets;
        float conversionFactor = 1.0f;
    };
}
