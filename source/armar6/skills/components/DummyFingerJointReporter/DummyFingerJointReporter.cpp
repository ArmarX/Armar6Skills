/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::DummyFingerJointReporter
 * @author     Fabian Paus ( fabian dot paus at gmail dot com )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DummyFingerJointReporter.h"

#include <SimoxUtility/algorithm/string/string_tools.h>

using namespace armarx;


void DummyFingerJointReporter::onInitComponent()
{
    std::string robotNodeSetName = getProperty<std::string>("RobotNodeSetName").getValue();
    kinematicUnitTopicName = robotNodeSetName + "State";
    ARMARX_INFO << "Listening to kinematic unit topic: " << kinematicUnitTopicName;



    std::string sourceJointsString = getProperty<std::string>("SourceJoints").getValue();
    std::vector<std::string> sourceJoints = simox::alg::split(sourceJointsString, ";");

    std::string targetJointsString = getProperty<std::string>("TargetJoints").getValue();
    std::vector<std::string> targetJointsStrings = simox::alg::split(targetJointsString, ";");

    if (sourceJoints.size() != targetJointsStrings.size())
    {
        throw LocalException() << "Number of source joints (" << sourceJoints.size()
                               << ") does not match target joints (" << targetJointsStrings.size() << ")";
    }
    for (std::size_t i = 0; i < sourceJoints.size(); ++i)
    {
        std::string const& sourceJoint = sourceJoints[i];
        std::string const& targetJointString = targetJointsStrings[i];
        std::vector<std::string> targetJoints = simox::alg::split(targetJointString, ",");

        ARMARX_INFO << "Mapping from '" << sourceJoint << "' to: ";
        for (std::string& joint : targetJoints)
        {
            simox::alg::trim(joint);
            ARMARX_INFO << "  '" << joint << "'";
        }

        sourceToTargets.emplace(sourceJoint, std::set<std::string>(targetJoints.begin(), targetJoints.end()));
    }

    conversionFactor = getProperty<float>("ConversionFactor");
    ARMARX_INFO << "Conversion factor: " << conversionFactor;

    usingTopic(kinematicUnitTopicName);
    offeringTopic(kinematicUnitTopicName);
}


void DummyFingerJointReporter::onConnectComponent()
{
    kinematicUnitTopic = getTopic<KinematicUnitListenerPrx>(kinematicUnitTopicName);
}


void DummyFingerJointReporter::onDisconnectComponent()
{

}


void DummyFingerJointReporter::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr DummyFingerJointReporter::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new DummyFingerJointReporterPropertyDefinitions(
            getConfigIdentifier()));
}

void DummyFingerJointReporter::reportControlModeChanged(const NameControlModeMap&, Ice::Long, bool, const Ice::Current&)
{

}

void DummyFingerJointReporter::reportJointAngles(const NameValueMap& jointAngles, Ice::Long timestamp, bool valueChanged, const Ice::Current&)
{
    NameValueMap additionalJointAngles;
    for (auto& nameValue : jointAngles)
    {
        std::string const& name = nameValue.first;
        float value = nameValue.second;

        auto targetsIter = sourceToTargets.find(name);
        if (targetsIter == sourceToTargets.end())
        {
            continue;
        }

        float mappedValue = conversionFactor * value;
        std::set<std::string> targets = targetsIter->second;
        for (std::string const& target : targets)
        {
            additionalJointAngles.emplace(target, mappedValue);
        }
    }

    if (additionalJointAngles.size() > 0)
    {
        ARMARX_INFO << deactivateSpam(1) << "Reporting additional joint angles for fingers";
        kinematicUnitTopic->reportJointAngles(additionalJointAngles, timestamp, valueChanged);
    }
}

void DummyFingerJointReporter::reportJointVelocities(const NameValueMap&, Ice::Long, bool, const Ice::Current&)
{

}

void DummyFingerJointReporter::reportJointTorques(const NameValueMap&, Ice::Long, bool, const Ice::Current&)
{

}

void DummyFingerJointReporter::reportJointAccelerations(const NameValueMap&, Ice::Long, bool, const Ice::Current&)
{

}

void DummyFingerJointReporter::reportJointCurrents(const NameValueMap&, Ice::Long, bool, const Ice::Current&)
{

}

void DummyFingerJointReporter::reportJointMotorTemperatures(const NameValueMap&, Ice::Long, bool, const Ice::Current&)
{

}

void DummyFingerJointReporter::reportJointStatuses(const NameStatusMap&, Ice::Long, bool, const Ice::Current&)
{

}


