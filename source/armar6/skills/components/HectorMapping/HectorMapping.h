/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::HectorMapping
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once




#include <ArmarXCore/core/Component.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>
#include <armar6/skills/interface/HectorMapping.h>

#include <pcl/point_types.h>

#include "hector_slam_lib/slam_main/HectorSlamProcessor.h"

#include <mutex>
#include <atomic>


namespace armarx
{

    using PointT = pcl::PointXYZ;

    struct ReportedVelocity
    {
        float dt;
        float x;
        float y;
        float theta;
    };

    /**
     * @class HectorMappingPropertyDefinitions
     * @brief
     */
    class HectorMappingPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        HectorMappingPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("providerName", "LaserScannerPointCloudProvider", "Name of the point cloud provider.");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the DebugDrawerTopic");
            defineOptionalProperty<bool>("UseOdometry", true, "Enable or disable the use of the platform odometry");
        }
    };

    /**
     * @defgroup Component-HectorMapping HectorMapping
     * @ingroup armar6_skills-Components
     * A description of the component HectorMapping.
     *
     * @class HectorMapping
     * @ingroup Component-HectorMapping
     * @brief Brief description of class HectorMapping.
     *
     * Detailed description of class HectorMapping.
     */

    class HectorMapping :
        virtual public visionx::PointCloudProcessor,
        virtual public HectorMappingInterface
    {
    public:

        void reportPlatformPose(PlatformPose const& currentPose, const Ice::Current& c = Ice::emptyCurrent) override;
        void reportNewTargetPose(Ice::Float newPlatformPositionX, Ice::Float newPlatformPositionY, Ice::Float newPlatformRotation, const Ice::Current& c = Ice::emptyCurrent) override;
        void reportPlatformVelocity(Ice::Float currentPlatformVelocityX, Ice::Float currentPlatformVelocityY, Ice::Float currentPlatformVelocityRotation, const Ice::Current& c = Ice::emptyCurrent) override;

        void reportPlatformOdometryPose(::Ice::Float, ::Ice::Float, ::Ice::Float, const ::Ice::Current& = ::Ice::emptyCurrent) override {}
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "HectorMapping";
        }

    protected:
        /**
         * @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
         */
        void onInitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
         */
        void onConnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
         */
        void onDisconnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
         */
        void onExitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::process()
         */
        void process() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:
        hectorslam::HectorSlamProcessor* slamProcessor;
        bool useOdometry = false;

        DebugDrawerInterfacePrx debugDrawer;
        long lastDebugUpdate;

        Eigen::Vector3f lastPose;

        std::atomic<bool> firstLaserScanData;
        std::mutex odometryMutex;
        std::vector<ReportedVelocity> reportedVelocities;
        Eigen::Vector3f lastVelocity;
        IceUtil::Time lastVelocityUpdate;
    };
}
