/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::HectorMapping
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "HectorMapping.h"

#include <pcl/common/transforms.h>
#include <pcl/filters/filter.h>

#include <Eigen/Core>
#include <RobotAPI/libraries/core/math/ColorUtils.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include "hector_slam_lib/map/GridMap.h"

using namespace armarx;

namespace
{
    Eigen::Vector3f addPose(Eigen::Vector3f pose, Eigen::Vector3f add)
    {
        Eigen::Vector3f result = pose + add;
        // Make sure that theta is in the range [-PI,PI]
        float theta = result[2];
        while (theta > M_PI)
        {
            theta = -2 * M_PI + theta;
        }
        while (theta < -M_PI)
        {
            theta = 2 * M_PI + theta;
        }
        result[2] = theta;
        return result;
    }

    Eigen::Vector3f integratePose(Eigen::Vector3f pose, Eigen::Vector3f v0, Eigen::Vector3f v1, float dt)
    {
        // Assumption: Linear acceleration between v0 and v1 in dt seconds
        // Travelled distance d = v0*dt + 0.5*(v1-v0)*dt = 0.5*dt*(v0+v1)

        float dTheta = 0.5f * dt * (v0[2] + v1[2]);
        // This is an approximation (the right solution would have to integrate over changing theta)
        float thetaMid = pose[2] + 0.5f * dTheta;
        Eigen::Matrix2f rotation = Eigen::Rotation2Df(thetaMid).matrix();
        Eigen::Vector2f globalV0 = rotation * v0.head<2>();
        Eigen::Vector2f globalV1 = rotation * v1.head<2>();

        Eigen::Vector2f globalDeltaPos = 0.5f * dt * (globalV0 + globalV1);
        Eigen::Vector3f d(globalDeltaPos.x(), globalDeltaPos.y(), dTheta);

        return addPose(pose, d);
    }
}

armarx::PropertyDefinitionsPtr HectorMapping::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new HectorMappingPropertyDefinitions(
            getConfigIdentifier()));
}



void armarx::HectorMapping::onInitPointCloudProcessor()
{
    useOdometry = getProperty<bool>("UseOdometry").getValue();
    usingTopic("PlatformState");
    offeringTopic(getProperty<std::string>("DebugDrawerTopicName").getValue());
    usingPointCloudProvider(getProperty<std::string>("providerName").getValue());

    int mapSizeX = 1024.0;
    int mapSizeY  = 1024.0;
    float mapResolution = 0.02;
    int multi_res_size = 3;
    Eigen::Vector2f startCoords(0.5, 0.5);

    slamProcessor = new hectorslam::HectorSlamProcessor(mapResolution,  mapSizeX,  mapSizeY,  startCoords, multi_res_size);

    slamProcessor->setUpdateFactorFree(0.4);
    slamProcessor->setUpdateFactorOccupied(0.9);
    slamProcessor->setMapUpdateMinDistDiff(0.4);
    slamProcessor->setMapUpdateMinAngleDiff(0.9);

}

void armarx::HectorMapping::onConnectPointCloudProcessor()
{
    debugDrawer = getTopic<DebugDrawerInterfacePrx>(getProperty<std::string>("DebugDrawerTopicName").getValue());
    lastDebugUpdate = 0.0;

    lastVelocityUpdate = TimeUtil::GetTime();
    lastVelocity = Eigen::Vector3f::Zero();
    firstLaserScanData = false;
}


void armarx::HectorMapping::onDisconnectPointCloudProcessor()
{
    firstLaserScanData = false;
}

void armarx::HectorMapping::onExitPointCloudProcessor()
{
    delete slamProcessor;

}

void armarx::HectorMapping::process()
{
    pcl::PointCloud<PointT>::Ptr inputCloud(new pcl::PointCloud<PointT>());

    if (!waitForPointClouds())
    {
        ARMARX_INFO << "Timeout or error while waiting for point cloud data";
        return;
    }
    else
    {
        getPointClouds(inputCloud);
    }


    // update gridmap map every 5 seconds
    if (lastDebugUpdate + 1000.0 * 1000.0 < inputCloud->header.stamp)
    {

        debugDrawer->clearLayer("hector_map");

        const int mapLevel = 0;
        //for (int mapLevel = 0; mapLevel < slamProcessor->getMapLevels(); mapLevel++)
        {
            hectorslam::GridMap gridMap = slamProcessor->getGridMap(mapLevel);

            DebugDrawer24BitColoredPointCloud cloud;
            cloud.points.reserve(gridMap.getSizeX() * gridMap.getSizeY());
            cloud.pointSize = gridMap.getCellLength() * 1000.0;

            for (int y = 0; y < gridMap.getSizeY(); y++)
            {
                for (int x = 0; x < gridMap.getSizeX(); x++)
                {
                    const int index = x + y * gridMap.getSizeX();
                    Eigen::Vector2f mapPoint(x, y);
                    Eigen::Vector2f worldPoint = gridMap.getWorldCoords(mapPoint);
                    DebugDrawer24BitColoredPointCloudElement point;
                    // FIXME gridMap.getCell(x, y) is not working.
                    LogOddsCell cell = gridMap.getCell(index);
                    point.color = colorutils::HeatMapRGBColor(cell.getValue());
                    point.x = worldPoint(0) * 1000.0;
                    point.y = worldPoint(1) * 1000.0;
                    point.z = 10000.0 + 5000.0 * mapLevel;
                    if (cell.isFree())
                    {
                        point.color = {0, 255, 0};
                    }
                    if (cell.isOccupied())
                    {
                        point.color = {255, 0, 0};
                    }
                    cloud.points.push_back(point);
                }
            }
            debugDrawer->set24BitColoredPointCloudVisu(getName(), "grid_" + to_string(mapLevel), cloud);




            Vector3Ptr position = new Vector3(lastPose(0) * 1000.0, lastPose(1) * 1000.0, 11000.0);
            debugDrawer->setArrowVisu(getName(), "pose", position,  new Vector3(0, 0, 1000.0), {1.0, 0.0, 0.0, 1.0}, 1000.0, 50.0);
            //debugDrawer->setRobotVisu(getName(), "pose", "");
        }
        lastDebugUpdate = inputCloud->header.stamp;
    }

    /*
    {
        std::unique_lock<std::mutex> lock(mutex);
        if (lastPoseUpdate)
        {
            Eigen::Vector3f diff = (slamProcessor->getLastScanMatchPose() - lastPose).cwiseAbs();
            if (diff.head<2>().norm() < 0.001 && diff(2) < 0.01)
            {
                ARMARX_INFO << deactivateSpam(1) << "waiting for odometry data. diff is" << diff;
                return;
            }
        }
        else
        {
            ARMARX_INFO << "processing initial laser scan.";
        }
    }
    */


    pcl::PointCloud<PointT>::Ptr scaledCloud(new pcl::PointCloud<PointT>());
    Eigen::Affine3f transform(Eigen::Scaling(0.001f));
    pcl::transformPointCloud(*inputCloud, *scaledCloud, transform);

    hectorslam::DataContainer dataContainer;
    dataContainer.setOrigo(Eigen::Vector2f::Zero());

    for (const PointT& p : scaledCloud->points)
    {
        // dataContainer.add(p.getVector3fMap().head<2>() * slamProcessor->getScaleToMap());
        dataContainer.add(Eigen::Vector2f(p.x, p.y) * slamProcessor->getScaleToMap());
    }

    // Apply odometry
    {
        std::lock_guard<std::mutex> lock(odometryMutex);
        for (ReportedVelocity const& report : reportedVelocities)
        {
            Eigen::Vector3f currentVelocity(report.x, report.y, report.theta);
            lastPose = integratePose(lastPose, lastVelocity, currentVelocity, report.dt);
            lastVelocity = currentVelocity;
        }
        reportedVelocities.clear();
    }

    slamProcessor->update(dataContainer, lastPose);
    lastPose = slamProcessor->getLastScanMatchPose();

    if (!firstLaserScanData)
    {
        lastVelocityUpdate = TimeUtil::GetTime();
        lastVelocity = Eigen::Vector3f::Zero();
        firstLaserScanData = true;
    }

    //ARMARX_LOG << "last pose " << lastPose;
}


void HectorMapping::reportPlatformVelocity(Ice::Float currentPlatformVelocityX, Ice::Float currentPlatformVelocityY, Ice::Float currentPlatformVelocityRotation, const Ice::Current& c)
{
    if (useOdometry)
    {
        if (!firstLaserScanData)
        {
            ARMARX_INFO << deactivateSpam(1) << "waiting for initial laser scan data";
            return;
        }
        IceUtil::Time now = TimeUtil::GetTime();
        std::lock_guard<std::mutex> lock(odometryMutex);
        float elapsedSeconds = static_cast<float>((now - lastVelocityUpdate).toSecondsDouble());
        reportedVelocities.push_back(ReportedVelocity {elapsedSeconds, 0.001f * currentPlatformVelocityX, 0.001f * currentPlatformVelocityY, currentPlatformVelocityRotation});
        lastVelocityUpdate = now;
    }
}


void HectorMapping::reportPlatformPose(PlatformPose const& currentPose, const Ice::Current& c)
{

}

void HectorMapping::reportNewTargetPose(Ice::Float newPlatformPositionX, Ice::Float newPlatformPositionY, Ice::Float newPlatformRotation, const Ice::Current& c)
{

}

ARMARX_DECOUPLED_REGISTER_COMPONENT(armarx::HectorMapping);
