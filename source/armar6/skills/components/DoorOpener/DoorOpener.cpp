/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::DoorOpener
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DoorOpener.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>
#include <ArmarXCore/core/time/CycleUtil.h>
#include <armar6/skills/libraries/Armar6Tools/Armar6HandV2Helper.h>

#include <RobotAPI/libraries/RobotStatechartHelpers/ForceTorqueHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/PositionControllerHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <RobotAPI/libraries/core/CartesianFeedForwardPositionController.h>
#include <VirtualRobot/math/AbstractFunctionR1R6.h>
#include <VirtualRobot/math/Helpers.h>
#include <Eigen/Geometry>


#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>

const double PI  = 3.141592653589793238463;
using namespace math;

class Circle : public AbstractFunctionR1R6
{
public:
    float T;
    float dt;
    float radius;
    float Pi = 3.1415;
    Eigen::Vector3f p;
    Eigen::Vector3f reference;

    Eigen::Vector3f GetPosition(float t) override
    {
        if (t < T && t >= 0)
        {
            p = reference + Eigen::Vector3f(radius * (1 - cos(2 * Pi / T / 4 * t)), -radius * sin(2 * Pi / T / 4 *  t), 0.0f);
        }
        else if (t < 0 && t > -T)
        {
            p = reference;
        }
        else
        {
            p = reference + Eigen::Vector3f(radius * (1 - cos(2 * Pi / 4)), -radius * sin(2 * Pi / 4), 0.0f);
        }
        return p;
    }

    virtual Eigen::Vector3f GetPositionDerivative(float t) override
    {
        p = radius * 2 * Pi / T / 4 * Eigen::Vector3f(sin(2 * Pi / T / 4 * t), -cos(2 * Pi / T / 4 *  t), 0.0f);

        return p;
    }
    Eigen::Quaternionf GetOrientation(float t) override
    {
        Eigen::Quaternionf ori = Eigen::Quaternionf(1, 0, 0, 0);
        return ori;
    }
    Eigen::Vector3f GetOrientationDerivative(float t) override
    {
        p = Eigen::Vector3f(0.0f, 0.0f, 0.0f);
        return p;
    }

};

namespace armarx
{
    typedef std::shared_ptr<Circle> CirclePtr;
    ARMARX_DECOUPLED_REGISTER_COMPONENT(DoorOpener);

    DoorOpenerPropertyDefinitions::DoorOpenerPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver",
                                            "Name of the topic the DebugObserver listens to.");

        defineOptionalProperty<std::string>("RobotUnitName", "Armar6Unit", "Robot unit (duh)");
        defineOptionalProperty<std::string>("RobotUnitObserverName", "RobotUnitObserver", "Robot unit observer (duh)");
        defineOptionalProperty<std::string>("KinematicUnitName", "Armar6KinematicUnit", "Kinematic unit name");
        defineOptionalProperty<std::string>("ForceTorqueObserverName", "Armar6ForceTorqueObserver", "Force observer");
    }


    std::string DoorOpener::getDefaultName() const
    {
        return "DoorOpener";
    }


    void DoorOpener::onInitComponent()
    {
        // Register offered topices and used proxies here.
        offeringTopicFromProperty("DebugObserverName");
        usingProxyFromProperty("RobotUnitName");
        usingProxyFromProperty("RobotUnitObserverName");
        usingProxyFromProperty("KinematicUnitName");
        usingProxyFromProperty("ForceTorqueObserverName");


        // debugDrawer.offeringTopic(*this);  // Calls this->offeringTopic().

        // Using a proxy will cause the component to wait until the proxy is available.
        // usingProxyFromProperty("MyProxyName");
    }


    void DoorOpener::onConnectComponent()
    {
        // Get topics and proxies here. Pass the *InterfacePrx type as template argument.
        getTopicFromProperty(debugObserver, "DebugObserverName");
        // debugDrawer.getTopic(*this);  // Calls this->getTopic().

        // getProxyFromProperty<MyProxyInterfacePrx>("MyProxyName");

        robot = addRobot("ARMAR-6", VirtualRobot::RobotIO::eStructure);

        getProxyFromProperty(robotUnit, "RobotUnitName");
        getProxyFromProperty(robotUnitObserver, "RobotUnitObserverName");
        getProxyFromProperty(kinematicUnit, "KinematicUnitName");
        getProxyFromProperty(forceTorqueUnit, "ForceTorqueObserverName");


        createRemoteGuiTab();
        RemoteGui_startRunningTask();
    }


    void DoorOpener::onDisconnectComponent()
    {
        task->stop();
        task = nullptr;
    }


    void DoorOpener::onExitComponent()
    {

    }


    armarx::PropertyDefinitionsPtr DoorOpener::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new DoorOpenerPropertyDefinitions(
                getConfigIdentifier()));
    }

    void DoorOpener::createRemoteGuiTab()
    {
        tab.execute.setLabel("Execute");
        tab.openHand.setLabel("Open Hand");
        tab.finish.setLabel("Finish");
        //tab.isSimulation.setName("Simulation");
        tab.isSimulation.setValue(state.isSimulation);
        tab.labelIsSimulation.setText("Simulation");

        //tab.compliance.setName("Radial Compliance");
        tab.compliance.setRange(0.0f, 5.0f);
        tab.compliance.setSteps(50);
        tab.compliance.setValue(state.compliance);
        tab.labelCompliance.setText("Radial Compliance");

        //tab.additionalApproachDoor.setName("AdditionalApproachDoor");
        tab.additionalApproachDoor.setRange(0.0f, 100.0f);
        tab.additionalApproachDoor.setSteps(100);
        tab.additionalApproachDoor.setValue(state.additionalApproachDoor);
        tab.labelAdditionalApproachDoor.setText("Distance to door after contact");

        //tab.approachHandle.setName("ApproachHandle");
        tab.approachHandle.setRange(0.0f, 100.0f);
        tab.approachHandle.setSteps(100);
        tab.approachHandle.setValue(state.approachHandle);
        tab.labelApproachHandle.setText("Max. movement in handle direction");

        //tab.forceThresholdContactDoor.setName("ForceThresholdContactDoor");
        tab.forceThresholdContactDoor.setRange(0.0f, 50.0f);
        tab.forceThresholdContactDoor.setSteps(50);
        tab.forceThresholdContactDoor.setValue(state.forceThresholdContactDoor);
        tab.LabelForceThresholdContactDoor.setText("Force threshold for door contact");

        //tab.forceThresholdContactHandle.setName("ForceThresholdContactHandle");
        tab.forceThresholdContactHandle.setRange(0.0f, 50.0f);
        tab.forceThresholdContactHandle.setSteps(50);
        tab.forceThresholdContactHandle.setValue(state.forceThresholdContactHandle);
        tab.LabelForceThresholdContactHandle.setText("Force threshold for handle contact");

        tab.zHeight.setRange(1000.0f, 1400.0f);
        tab.zHeight.setSteps(1000);
        tab.zHeight.setValue(state.zHeight);



        VBoxLayout root;
        root.addChild(tab.execute);
        root.addChild(new RemoteGui::HSpacer());
        root.addChild(tab.openHand);
        root.addChild(new RemoteGui::HSpacer());
        root.addChild(tab.finish);
        root.addChild(new RemoteGui::HSpacer());
        root.addChild(tab.labelIsSimulation);
        root.addChild(tab.isSimulation);
        root.addChild(new RemoteGui::HSpacer());
        root.addChild(tab.labelCompliance);
        root.addChild(tab.compliance);
        root.addChild(new RemoteGui::HSpacer());
        root.addChild(new RemoteGui::HSpacer());
        root.addChild(tab.LabelForceThresholdContactDoor);
        root.addChild(tab.forceThresholdContactDoor);
        root.addChild(new RemoteGui::HSpacer());
        root.addChild(tab.labelAdditionalApproachDoor);
        root.addChild(tab.additionalApproachDoor);
        root.addChild(new RemoteGui::HSpacer());
        root.addChild(new RemoteGui::HSpacer());
        root.addChild(tab.LabelForceThresholdContactHandle);
        root.addChild(tab.forceThresholdContactHandle);
        root.addChild(new RemoteGui::HSpacer());
        root.addChild(tab.labelApproachHandle);
        root.addChild(tab.approachHandle);
        root.addChild(new RemoteGui::HSpacer());
        root.addChild(Label("z height"));
        root.addChild(tab.zHeight);

        RemoteGui_createTab("DoorOpener", root, &tab);

    }

    void DoorOpener::RemoteGui_update()
    {
        std::scoped_lock lock(stateMutex);
        if (tab.execute.wasClicked())
        {
            ARMARX_IMPORTANT << "Starting execution";
            if (task && task->isRunning())
            {
                task->stop();
            }
            task = new RunningTask<DoorOpener>(this, &DoorOpener::controlLoop);
            task->start();

        }
        if (tab.openHand.wasClicked())
        {
            ARMARX_IMPORTANT << "Open hand clicked";
            state.openHand = true;
        }
        if (tab.finish.wasClicked())
        {
            ARMARX_IMPORTANT << "Finish clicked";
            state.finish = true;
        }
        state.isSimulation = tab.isSimulation.getValue();
        state.compliance = tab.compliance.getValue();
        state.additionalApproachDoor = tab.additionalApproachDoor.getValue();
        state.approachHandle = tab.approachHandle.getValue();
        state.forceThresholdContactDoor = tab.forceThresholdContactDoor.getValue();
        state.forceThresholdContactHandle = tab.forceThresholdContactHandle.getValue();
        state.zHeight = tab.zHeight.getValue();


    }

    void DoorOpener::controlLoop()
    {
        std::string side = "Right";
        bool isSimulation = state.isSimulation;
        RobotNameHelperPtr nameHelper(RobotNameHelper::Create(getRobotStateComponent()->getRobotInfo(), nullptr));
        RobotNameHelper::Arm arm = nameHelper->getArm(side);
        synchronizeLocalClone(robot);
        RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();


        //KinematicUnitHelper kinUnitHelper(getKinematicUnit());
        std::string controllerName = "DoorOpenerCartesianController_" + side;
        VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(robotUnit, arm.getKinematicChain(), controllerName));
        //ForceTorqueHelper ftHelper(getForceTorqueObserver(), arm.getForceTorqueSensor());
        Armar6HandV2Helper handHelper(kinematicUnit, robotUnitObserver, side, isSimulation);
        ForceTorqueHelper forceTorqueHelper(forceTorqueUnit, arm.getForceTorqueSensor());

        Eigen::Matrix4f initial_tcp_pose = tcp->getPoseInRootFrame();

        PositionControllerHelper posHelper(tcp, velHelper, initial_tcp_pose);
        {
            CartesianPositionControllerConfigPtr controllerConfig = new CartesianPositionControllerConfig();
            controllerConfig->KpOri = 1.0f;
            controllerConfig->KpPos = 0.5f; //1.0f;
            controllerConfig->maxOriVel = 1.0f;
            controllerConfig->maxPosVel = 80.0f;
            controllerConfig->thresholdOrientationNear = 0.1f;
            controllerConfig->thresholdOrientationReached = 0.05f;
            controllerConfig->thresholdPositionNear = 5.0f;
            controllerConfig->thresholdPositionReached = 5.0f;
            posHelper.readConfig(controllerConfig);
        }
        // does this make sense?
        CartesianFeedForwardPositionController pos_controller(tcp);

        // define handle position
        //Eigen::Vector3f tcpPos = math::Helpers::Position(initial_tcp_pose);
        //        Eigen::Matrix4f goal = initial_tcp_pose;
        //        math::Helpers::Position(goal) = tcpPos + Eigen::Vector3f(0.0f, 300.0f, 0.0f);


        Eigen::Matrix3f Rot = Eigen::Quaternionf(0.73738354444503784, -0.67436587810516357, -0.029755214229226112, -0.02471715584397316).normalized().toRotationMatrix();
        Eigen::Matrix4f goal =  Eigen::Matrix4f::Zero();
        math::Helpers::Orientation(goal) = Rot;
        {
            std::scoped_lock lock(stateMutex);
            math::Helpers::Position(goal) = Eigen::Vector3f(379.778f, 679.024f, state.zHeight); //position in root frame
            posHelper.addWaypoint(goal);
            math::Helpers::Position(goal) = Eigen::Vector3f(379.778f, 979.024f, state.zHeight); //position in root frame
            posHelper.addWaypoint(goal);
        }
        //ARMARX_INFO << "Handle Pose in root frame new: " << goal;




        // set circle properties
        CirclePtr circle(new Circle());
        circle->radius = 200;
        circle->T = 10;
        circle->dt = 0.01;
        AbstractFunctionR1R6Ptr motion;



        viz::Robot robotViz = viz::Robot("robot")
                              .file("armar6_rt", "armar6_rt/robotmodel/Armar6-SH/Armar6-SH.xml")
                              .joints(robot->getConfig()->getRobotNodeJointValueMap());
        viz::Layer robotLayer = arviz.layer("Robot");
        viz::Layer goalLayer = arviz.layer("Goal");
        robotLayer.add(robotViz);

        viz::Layer posesLayer = arviz.layer("Poses");
        viz::Layer trajectoryLayer = arviz.layer("Trajectory");
        posesLayer.add(viz::Sphere("start")
                       .pose(initial_tcp_pose)
                       .radius(50.0f)
                       .colorGlasbeyLUT(0));
        posesLayer.add(viz::Sphere("goal")
                       .pose(goal)
                       .radius(50.0f)
                       .colorGlasbeyLUT(1));
        arviz.commit({robotLayer, posesLayer});


        //unsigned long lastWaypointIndex = posHelper.currentWaypointIndex;
        PhaseType currentPhase = PhaseType::Preshape;
        forceTorqueHelper.setZero();
        Eigen::Vector3f initialForce = Eigen::Vector3f(0, 0, 0);

        CycleUtil c(10);

        Eigen::Vector3f velocity(0.0f, 0.0f, 0.0f);
        Eigen::Vector3f tangent_velocity(1.0f, 0.0f, 0.0f);
        Eigen::Vector3f vel_rad(1.0f, 0.0f, 0.0f);
        Eigen::Vector3f force_tan(0.0f, 0.0f, 0.0f);
        Eigen::Vector3f force_rad(0.0f, 0.0f, 0.0f);
        bool resetForce = false;
        float stateT = 0;
        while (!task->isStopped())
        {
            synchronizeLocalClone(robot);

            DoorOpenerState state;
            {
                std::scoped_lock lock(stateMutex);
                state = this->state;

                if (this->state.openHand)
                {
                    ARMARX_INFO << "Open hand in controlLoop: " << VAROUT(state.openHand);
                }

                this->state.openHand = false;
                this->state.finish = false;
            }
            Eigen::Vector3f approachHandleVector(-state.approachHandle, 0.0f, 0.0f);


            posHelper.updateRead();
            FramedDirectionPtr framedForce = forceTorqueHelper.forceDf->getDataField()->get<FramedDirection>();
            framedForce->changeFrame(robot, "root");
            Eigen::Vector3f originalForce = framedForce->toEigen();
            Eigen::Vector3f force = originalForce - initialForce;
            force.z() = 0;

            //if (state.openHand)
            if (state.openHand)
            {
                ARMARX_IMPORTANT << "Open hand";
                handHelper.shapeHand(0.0f, 0.0f);
            }

            nextPhase = currentPhase;
            if (currentPhase == PhaseType::Preshape)
            {
                ARMARX_INFO << "STATE: Preshape";
                handHelper.shapeHand(0.3f, 0.0f);
                usleep(100000);
                nextPhase = PhaseType::DoorContact;
                forceTorqueHelper.setZero();
                initialForce = originalForce;
            }

            if (currentPhase == PhaseType::DoorContact)
            {
                ARMARX_INFO << "STATE: DoorContact";
                bool targetReached = posHelper.isFinalTargetReached();
                bool forceExceeded = force.norm() > state.forceThresholdContactDoor;
                if (targetReached || forceExceeded)
                {
                    ARMARX_INFO << "Transition to ApproachHandle (" << VAROUT(targetReached) << "," << VAROUT(forceExceeded);
                    nextPhase = PhaseType::ApproachHandle;
                    Eigen::Matrix4f closerToDoor = goal;
                    math::Helpers::Position(closerToDoor) = tcp->getPositionInRootFrame() + Eigen::Vector3f(0.0f, state.additionalApproachDoor, 0.0f);
                    Eigen::Matrix4f towardsHandle = goal;
                    math::Helpers::Position(towardsHandle) = math::Helpers::Position(closerToDoor) + approachHandleVector;
                    std::vector<Eigen::Matrix4f> newGoals;
                    newGoals.push_back(closerToDoor);
                    newGoals.push_back(towardsHandle);
                    posHelper.setNewWaypoints(newGoals);
                    handHelper.shapeHand(0.5f, 0.0f);
                    ARMARX_INFO << "STATE: ApproachHandle";
                }
            }
            if (currentPhase == PhaseType::ApproachHandle)
            {
                if (posHelper.currentWaypointIndex == 1)
                {
                    handHelper.shapeHand(0.0f, 0.0f);
                }
                bool forceExceeded = std::abs(force.x()) > state.forceThresholdContactHandle; //project in approach direction
                if (posHelper.isFinalTargetReached() || forceExceeded)
                {
                    ARMARX_INFO << "Transition to Grasp (" << VAROUT(posHelper.isFinalTargetReached()) << "," << VAROUT(forceExceeded);
                    nextPhase = PhaseType::Grasp;
                }
            }
            if (currentPhase == PhaseType::Grasp)
            {
                handHelper.shapeHand(1.0f, 1.0f);
                usleep(100000);
                //nextPhase = PhaseType::Pull;
                circle->reference = tcp->getPositionInRootFrame();
                posesLayer.clear();
                arviz.commit({posesLayer});
                motion = circle;
                forceTorqueHelper.setZero();
                initialForce = originalForce;
                resetForce = true;
                ARMARX_INFO << "STATE: Pull";
                trajectoryLayer.clear();
                for (int i = 0; i < 17; i++)
                {
                    Eigen::Vector3f goal = circle->GetPosition(1.0 / 16.0f  * circle->T  * i);
                    viz::Sphere s = viz::Sphere("traj" + std::to_string(i));
                    s.position(goal);
                    s.radius(10.0f);
                    s.colorGlasbeyLUT(5);
                    trajectoryLayer.add(s);

                }
                arviz.commit(trajectoryLayer);
                ARMARX_INFO << "after commit";

                break;
            }
            if (currentPhase == PhaseType::Pull)
            {

                //                if ((circle->GetPosition(circle->T) - tcp->getPositionInRootFrame()).norm() < 10)
                //if ((circle->T - stateT) < 0.1)
                if (false)
                {
                    nextPhase = PhaseType::Release;
                }
                //                if (resetForce & (stateT > 1))
                //                {
                //                    forceTorqueHelper.setZero();
                //                    initialForce = originalForce;
                //                    resetForce = false;
                //                }
                Eigen::Vector3f current_position = tcp->getPositionInRootFrame();
                stateT = std::atan2(-(current_position - circle->reference).y(), (circle->radius - (current_position - circle->reference).x())) * circle->T * 2 / circle->Pi;

                math::Helpers::Position(goal) = circle->GetPosition(stateT);
                posHelper.setTarget(goal);

                //tangent_velocity = pos_controller.calculate(motion, state, VirtualRobot::IKSolver::CartesianSelection::Position);
                tangent_velocity = motion->GetPositionDerivative(stateT);
                if (stateT < circle->T && stateT >= -0.5)
                {
                    velocity = pos_controller.calculate(motion, stateT, VirtualRobot::IKSolver::CartesianSelection::Position);
                }
                else
                {
                    velocity = Eigen::Vector3f(0.0f, 0.0f, 0.0f);
                }
                Eigen::Vector3f velOri(0.0f, 0.0f, 0.0f);


                Eigen::Vector3f force_tan = tangent_velocity.normalized() * tangent_velocity.normalized().dot(force);
                Eigen::Vector3f force_rad = force - force_tan;
                vel_rad = force_rad * state.compliance;

                posHelper.setFeedForwardVelocity(velocity + vel_rad, velOri); //

                robotLayer.add(viz::Sphere("current_goal")
                               .pose(goal)
                               .radius(40.0f)
                               .colorGlasbeyLUT(4));
                robotViz.joints(rns->getJointValueMap());
                arviz.commit(robotLayer);

            }
            if (currentPhase == PhaseType::Release)
            {


                ARMARX_INFO << "STATE: Release";
                posHelper.immediateHardStop();
                handHelper.shapeHand(0.0f, 0.0f);
                trajectoryLayer.clear();
                arviz.commit(trajectoryLayer);

                break;
            }

            //            force_tan = tangent_velocity.normalized() * tangent_velocity.normalized().dot(force);
            //            force_rad = force - force_tan;

            //Visualization
            debugObserver->setDebugDatafield("DoorOpener", "PositionError", new armarx::Variant(posHelper.getPositionError()));
            debugObserver->setDebugDatafield("DoorOpener", "Velocity_x", new armarx::Variant(velocity.x()));
            debugObserver->setDebugDatafield("DoorOpener", "Velocity_y", new armarx::Variant(velocity.y()));

            debugObserver->setDebugDatafield("DoorOpener", "Force_tan", new armarx::Variant(force_tan.norm()));
            debugObserver->setDebugDatafield("DoorOpener", "Force_rad", new armarx::Variant(force_rad.norm()));
            debugObserver->setDebugDatafield("DoorOpener", "Force", new armarx::Variant(force.norm()));

            debugObserver->setDebugDatafield("DoorOpener", "Velocity_rad_added", new armarx::Variant(vel_rad.norm()));
            debugObserver->setDebugDatafield("DoorOpener", "State", new armarx::Variant(stateT));

            debugObserver->setDebugDatafield("DoorOpener", "Velocity_tangent_x", new armarx::Variant(tangent_velocity.x()));
            debugObserver->setDebugDatafield("DoorOpener", "Velocity_tangent_y", new armarx::Variant(tangent_velocity.y()));
            debugObserver->setDebugDatafield("DoorOpener", "Velocity_tangent_z", new armarx::Variant(tangent_velocity.z()));

            viz::Layer forceLayer = arviz.layer("Force");

            viz::Arrow forceviz_tan = viz::Arrow("force_tan");
            forceviz_tan.fromTo(tcp->getPositionInRootFrame(), tcp->getPositionInRootFrame() + 20 * force_tan);
            forceviz_tan.width(10);
            forceviz_tan.color(viz::Color::blue());
            viz::Arrow forceviz_rad = viz::Arrow("force_rad");
            forceviz_rad.fromTo(tcp->getPositionInRootFrame(), tcp->getPositionInRootFrame() + 20 * force_rad);
            forceviz_rad.width(10);
            forceviz_rad.color(viz::Color::green());
            forceLayer.add(forceviz_tan);
            forceLayer.add(forceviz_rad);

            posHelper.updateWrite();
            robotViz.joints(rns->getJointValueMap());
            arviz.commit(robotLayer);
            viz::Arrow forceviz = viz::Arrow("force");
            forceviz.fromTo(tcp->getPositionInRootFrame(), tcp->getPositionInRootFrame() + 20 * force);
            forceviz.width(10);
            forceviz.color(viz::Color::red());
            forceLayer.add(forceviz);
            arviz.commit(forceLayer);
            //if (state.finish)
            if (state.finish)
            {
                nextPhase = PhaseType::Release;
                ARMARX_INFO << "Finish";
            }
            currentPhase = nextPhase;

            //            {
            //                std::scoped_lock lock(stateMutex);
            //                this->state.openHand = false;
            //                this->state.finish = false;
            //            }

            c.waitForCycleDuration();
            double duration = c.getAverageDuration().toMilliSecondsDouble();
            debugObserver->setDebugDatafield("DoorOpener", "CycleDuration", new armarx::Variant(duration));
        }

        posHelper.immediateHardStop();
        posHelper.updateWrite();
    }
}
