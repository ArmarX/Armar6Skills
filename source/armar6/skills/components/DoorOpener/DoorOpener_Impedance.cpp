/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::DoorOpener
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DoorOpener_Impedance.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>
#include <ArmarXCore/core/time/CycleUtil.h>
#include <armar6/skills/libraries/Armar6Tools/Armar6HandV2Helper.h>

#include <RobotAPI/libraries/RobotStatechartHelpers/ForceTorqueHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/PositionControllerHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <RobotAPI/libraries/core/CartesianFeedForwardPositionController.h>
#include <VirtualRobot/math/AbstractFunctionR1R6.h>
#include <VirtualRobot/math/Helpers.h>
#include <Eigen/Geometry>



#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>

const double PI  = 3.141592653589793238463;
using namespace math;

class Circle : public AbstractFunctionR1R6
{
public:
    float T;
    float dt;
    float radius;
    float Pi = 3.1415;
    Eigen::Vector3f p;
    Eigen::Vector3f reference;

    Eigen::Vector3f GetPosition(float t) override
    {
        if (t < T && t >= 0)
        {
            p = reference + Eigen::Vector3f(radius * (1 - cos(2 * Pi / T / 4 * t)), -radius * sin(2 * Pi / T / 4 *  t), 0.0f);
        }
        else if (t < 0 && t > -T)
        {
            p = reference;
        }
        else
        {
            p = reference + Eigen::Vector3f(radius * (1 - cos(2 * Pi / 4)), -radius * sin(2 * Pi / 4), 0.0f);
        }
        return p;
    }

    virtual Eigen::Vector3f GetPositionDerivative(float t) override
    {
        if (t < T && t >= 0)
        {
            p = radius * 2 * Pi / T / 4 * Eigen::Vector3f(sin(2 * Pi / T / 4 * t), -cos(2 * Pi / T / 4 *  t), 0.0f);
        }
        else
        {
            p = Eigen::Vector3f(0.0f, 0.0f, 0.0f);
        }
        return p;
    }
    Eigen::Quaternionf GetOrientation(float t) override
    {
        Eigen::Quaternionf ori = Eigen::Quaternionf(1, 0, 0, 0);
        return ori;
    }
    Eigen::Vector3f GetOrientationDerivative(float t) override
    {
        p = Eigen::Vector3f(0.0f, 0.0f, 0.0f);
        return p;
    }

};

namespace armarx
{
    typedef std::shared_ptr<Circle> CirclePtr;
    ARMARX_DECOUPLED_REGISTER_COMPONENT(DoorOpener);

    DoorOpenerPropertyDefinitions::DoorOpenerPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver",
                                            "Name of the topic the DebugObserver listens to.");

        defineOptionalProperty<std::string>("RobotUnitName", "Armar6Unit", "Robot unit (duh)");
        defineOptionalProperty<std::string>("RobotUnitObserverName", "RobotUnitObserver", "Robot unit observer (duh)");
        defineOptionalProperty<std::string>("KinematicUnitName", "Armar6KinematicUnit", "Kinematic unit name");
        defineOptionalProperty<std::string>("ForceTorqueObserverName", "Armar6ForceTorqueObserver", "Force observer");
    }


    std::string DoorOpener::getDefaultName() const
    {
        return "DoorOpener";
    }


    void DoorOpener::onInitComponent()
    {
        // Register offered topices and used proxies here.
        offeringTopicFromProperty("DebugObserverName");
        usingProxyFromProperty("RobotUnitName");
        usingProxyFromProperty("RobotUnitObserverName");
        usingProxyFromProperty("KinematicUnitName");
        usingProxyFromProperty("ForceTorqueObserverName");


        // debugDrawer.offeringTopic(*this);  // Calls this->offeringTopic().

        // Using a proxy will cause the component to wait until the proxy is available.
        // usingProxyFromProperty("MyProxyName");
    }


    void DoorOpener::onConnectComponent()
    {
        // Get topics and proxies here. Pass the *InterfacePrx type as template argument.
        getTopicFromProperty(debugObserver, "DebugObserverName");
        // debugDrawer.getTopic(*this);  // Calls this->getTopic().

        // getProxyFromProperty<MyProxyInterfacePrx>("MyProxyName");

        robot = addRobot("ARMAR-6", VirtualRobot::RobotIO::eStructure);

        getProxyFromProperty(robotUnit, "RobotUnitName");
        getProxyFromProperty(robotUnitObserver, "RobotUnitObserverName");
        getProxyFromProperty(kinematicUnit, "KinematicUnitName");
        getProxyFromProperty(forceTorqueUnit, "ForceTorqueObserverName");


        createRemoteGuiTab();
        RemoteGui_startRunningTask();
    }


    void DoorOpener::onDisconnectComponent()
    {
        task->stop();
        task = nullptr;


    }


    void DoorOpener::onExitComponent()
    {

    }


    armarx::PropertyDefinitionsPtr DoorOpener::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new DoorOpenerPropertyDefinitions(
                getConfigIdentifier()));
    }

    void DoorOpener::createRemoteGuiTab()
    {
        tab.buttonExecute.setLabel("Execute");
        tab.openHand.setLabel("Open Hand");
        tab.buttonFinish.setLabel("Finish");

        //VBoxLayout root = {tab.buttonExecute, VSpacer()};
        VBoxLayout root;
        root.addChild(tab.buttonExecute);
        root.addChild(new RemoteGui::HSpacer());
        root.addChild(tab.openHand);
        root.addChild(new RemoteGui::HSpacer());
        root.addChild(tab.buttonFinish);


        RemoteGui_createTab("DoorOpener", root, &tab);

    }

    void DoorOpener::RemoteGui_update()
    {
        if (tab.buttonExecute.wasClicked())
        {
            ARMARX_IMPORTANT << "Starting execution";
            task = new RunningTask<DoorOpener>(this, &DoorOpener::controlLoop);
            task->start();
        }
        if (tab.openHand.wasClicked())
        {
            ARMARX_IMPORTANT << "Open hand";
            openHand = true;
        }
        if (tab.buttonFinish.wasClicked())
        {
            nextPhase = PhaseType::Release;
            if (nextPhase == PhaseType::Release)
            {
                ARMARX_IMPORTANT << "test";
            }

        }
    }

    void DoorOpener::controlLoop()
    {
        std::string side = "Right";
        bool isSimulation = true;
        RobotNameHelperPtr nameHelper(RobotNameHelper::Create(getRobotStateComponent()->getRobotInfo(), nullptr));
        RobotNameHelper::Arm arm = nameHelper->getArm(side);
        synchronizeLocalClone(robot);
        RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

        //KinematicUnitHelper kinUnitHelper(getKinematicUnit());
        std::string controllerName = "DoorOpenerCartesianController_" + side;
        VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(robotUnit, arm.getKinematicChain(), controllerName));
        //ForceTorqueHelper ftHelper(getForceTorqueObserver(), arm.getForceTorqueSensor());
        Armar6HandV2Helper handHelper(kinematicUnit, robotUnitObserver, side, isSimulation);
        ForceTorqueHelper forceTorqueHelper(forceTorqueUnit, arm.getForceTorqueSensor());

        Eigen::Matrix4f initial_tcp_pose = tcp->getPoseInRootFrame();

        // Impedance Control
        const std::vector<float> Kpos = {1000, 1000, 1000};
        const std::vector<float> Kori = {500, 500, 500};
        const std::vector<float> Dpos = {200, 200, 200};
        const std::vector<float> Dori = {200, 200, 200};
        float knullval = 3;
        float dnullval = 1;

        std::vector<float> knull;
        std::vector<float> dnull;

        for (size_t i = 0; i < 8; ++i)
        {
            knull.push_back(knullval);
            dnull.push_back(dnullval);
        }
        std::vector<float> desiredJointPositionRight = robot->getRobotNodeSet("RightArm")->getJointValues();


        NJointTaskSpaceImpedanceControlConfigPtr rightConfig = new NJointTaskSpaceImpedanceControlConfig;
        rightConfig->nodeSetName = "RightArm";
        rightConfig->desiredPosition = math::Helpers::Position(initial_tcp_pose);
        rightConfig->desiredOrientation = math::Helpers::Orientation(initial_tcp_pose);
        rightConfig->Kpos = Eigen::Map<const Eigen::Vector3f>(Kpos.data(), Kpos.size());
        rightConfig->Kori = Eigen::Map<const Eigen::Vector3f>(Kori.data(), Kori.size());
        rightConfig->Dpos = Eigen::Map<const Eigen::Vector3f>(Dpos.data(), Dpos.size());
        rightConfig->Dori = Eigen::Map<const Eigen::Vector3f>(Dori.data(), Dori.size());
        rightConfig->desiredJointPositions = Eigen::Map<const Eigen::VectorXf>(desiredJointPositionRight.data(), desiredJointPositionRight.size());
        rightConfig->Knull = Eigen::Map<const Eigen::VectorXf>(knull.data(), knull.size());
        rightConfig->Dnull = Eigen::Map<const Eigen::VectorXf>(dnull.data(), dnull.size());
        rightConfig->torqueLimit = 30;

        rightController
            = NJointTaskSpaceImpedanceControlInterfacePrx::checkedCast(robotUnit->createNJointController("NJointTaskSpaceImpedanceController", "RightController", rightConfig));
        rightController->activateController();



        PositionControllerHelper posHelper(tcp, velHelper, initial_tcp_pose);
        {
            CartesianPositionControllerConfigPtr controllerConfig = new CartesianPositionControllerConfig();
            controllerConfig->KpOri = 1.0f;
            controllerConfig->KpPos = 0.5f; //1.0f;
            controllerConfig->maxOriVel = 1.0f;
            controllerConfig->maxPosVel = 80.0f;
            controllerConfig->thresholdOrientationNear = 0.1f;
            controllerConfig->thresholdOrientationReached = 0.05f;
            controllerConfig->thresholdPositionNear = 40.0f;
            controllerConfig->thresholdPositionReached = 5.0f;
            posHelper.readConfig(controllerConfig);
        }
        // does this make sense?
        CartesianFeedForwardPositionController pos_controller(tcp);

        // define handle position
        Eigen::Vector3f tcpPos = math::Helpers::Position(initial_tcp_pose);
        Eigen::Matrix4f goal = initial_tcp_pose;
        math::Helpers::Position(goal) = tcpPos + Eigen::Vector3f(0.0f, 10.0f, 0.0f);


        //        Eigen::Matrix3f Rot = Eigen::Quaternionf(-0.5, 0.5, -0.5, -0.5).normalized().toRotationMatrix();
        //        Eigen::Matrix4f goal =  Eigen::Matrix4f::Zero();
        //        math::Helpers::Orientation(goal) = Rot;
        //        math::Helpers::Position(goal) = Eigen::Vector3f(379.778f, 979.024f, 993.15f); //position in root frame

        //ARMARX_INFO << "Handle Pose in root frame new: " << goal;

        //        if (posHelper.currentWaypointIndex < 1)
        //        {
        //            posHelper.addWaypoint(goal);
        //        }

        //rightController->setPose(goal);


        // set circle properties
        CirclePtr circle(new Circle());
        circle->radius = 200;
        circle->T = 10;
        circle->dt = 0.01;
        AbstractFunctionR1R6Ptr motion;



        viz::Robot robotViz = viz::Robot("robot")
                              .file("armar6_rt", "armar6_rt/robotmodel/Armar6-SH/Armar6-SH.xml")
                              .joints(robot->getConfig()->getRobotNodeJointValueMap());
        viz::Layer forceLayer = arviz.layer("Force");
        viz::Layer robotLayer = arviz.layer("Robot");
        viz::Layer goalLayer = arviz.layer("Goal");
        robotLayer.add(robotViz);

        viz::Layer posesLayer = arviz.layer("Poses");
        posesLayer.add(viz::Sphere("start")
                       .pose(initial_tcp_pose)
                       .radius(50.0f)
                       .colorGlasbeyLUT(0));
        posesLayer.add(viz::Sphere("goal")
                       .pose(goal)
                       .radius(50.0f)
                       .colorGlasbeyLUT(1));
        arviz.commit({robotLayer, posesLayer});


        //unsigned long lastWaypointIndex = posHelper.currentWaypointIndex;
        PhaseType currentPhase = PhaseType::Preshape;
        forceTorqueHelper.setZero();
        Eigen::Vector3f initialForce = Eigen::Vector3f(0, 0, 0);

        CycleUtil c(10);
        Eigen::Vector3f velocity(1.0f, 0.0f, 0.0f);
        float state = 0;
        while (!task->isStopped())
        {
            synchronizeLocalClone(robot);
            if (currentPhase != PhaseType::Approach)
            {
                posHelper.updateRead();
            }
            FramedDirectionPtr framedForce = forceTorqueHelper.forceDf->getDataField()->get<FramedDirection>();
            framedForce->changeFrame(robot, "root");
            Eigen::Vector3f originalForce = framedForce->toEigen();
            Eigen::Vector3f force = originalForce - initialForce;
            force.z() = 0;
            if (rightController->hasControllerError())
            {
                ARMARX_INFO << "has controller error";
            }

            if (openHand)
            {
                handHelper.shapeHand(0.0f, 0.0f);
                openHand = false;
            }

            nextPhase = currentPhase;
            if (currentPhase == PhaseType::Preshape)
            {
                ARMARX_INFO << "STATE: Preshape";
                handHelper.shapeHand(0.3f, 0.3f);
                TimeUtil::SleepMS(100);
                nextPhase = PhaseType::Approach;
                forceTorqueHelper.setZero();
                initialForce = originalForce;
            }

            if (currentPhase == PhaseType::Approach)
            {
                ARMARX_INFO << "STATE: Approach";
                //                bool targetReached = posHelper.isFinalTargetReached();
                //                if (targetReached | (force.norm() > 5))
                //                {
                //                    nextPhase = PhaseType::Grasp;// need to clear waypoint in case of force?
                //                }

                ARMARX_INFO << "in while";
                rightController->setPose(goal);
                synchronizeLocalClone(robot);
                ARMARX_INFO << "Position Error" << (math::Helpers::Position(goal)  - math::Helpers::Position(tcp->getPoseInRootFrame())).norm();

                ARMARX_INFO << "out of while";
                if ((math::Helpers::Position(goal)  - math::Helpers::Position(tcp->getPoseInRootFrame())).norm() < 5)
                {
                    nextPhase = PhaseType::Grasp;
                }

            }
            if (currentPhase == PhaseType::Grasp)
            {
                ARMARX_INFO << "STATE: Grasp";
                handHelper.shapeHand(1.0f, 1.0f);
                TimeUtil::SleepMS(100);
                nextPhase = PhaseType::Pull;
                circle->reference = tcp->getPositionInRootFrame();
                posesLayer.clear();
                arviz.commit({posesLayer});
                motion = circle;
                forceTorqueHelper.setZero();
                initialForce = originalForce;
                ARMARX_INFO << "STATE: Pull";

                for (int i = 0; i < 17; i++)
                {
                    Eigen::Vector3f goal = circle->GetPosition(1.0 / 16.0f  * circle->T  * i);
                    viz::Sphere s = viz::Sphere("traj" + std::to_string(i));
                    s.position(goal);
                    s.radius(10.0f);
                    s.colorGlasbeyLUT(5);
                    arviz.commitLayerContaining("Trajectory" + std::to_string(i), s);
                }


            }
            if (currentPhase == PhaseType::Pull)
            {

                //                if ((circle->GetPosition(circle->T) - tcp->getPositionInRootFrame()).norm() < 10)
                //if ((circle->T - state) < 0.1)
                if (false)
                {
                    nextPhase = PhaseType::Release;
                }
                //IceUtil::Time now = TimeUtil::GetTime();
                Eigen::Vector3f current_position = tcp->getPositionInRootFrame();

                //state = std::atan(-(current_position - circle->reference).y() / (circle->radius - (current_position - circle->reference).x())) * circle->T * 2 / circle->Pi;
                state = std::atan2(-(current_position - circle->reference).y(), (circle->radius - (current_position - circle->reference).x())) * circle->T * 2 / circle->Pi;

                //ARMARX_LOG << VAROUT(state);
                math::Helpers::Position(goal) = circle->GetPosition(state);
                //posHelper.addWaypoint(goal);
                rightController->setPose(goal);
                velocity = pos_controller.calculate(motion, state, VirtualRobot::IKSolver::CartesianSelection::Position);
                Eigen::Vector3f velOri(0.0f, 0.0f, 0.0f);
                debugObserver->setDebugDatafield("DoorOpener", "PositionError", new armarx::Variant(posHelper.getPositionError()));
                debugObserver->setDebugDatafield("DoorOpener", "Velocity_x", new armarx::Variant(velocity.x()));
                debugObserver->setDebugDatafield("DoorOpener", "Velocity_y", new armarx::Variant(velocity.y()));

                Eigen::Vector3f force_tan = velocity.normalized() * velocity.normalized().dot(force);
                Eigen::Vector3f force_rad = force - force_tan;
                Eigen::Vector3f vel_rad = force_rad * 1.0;
                debugObserver->setDebugDatafield("DoorOpener", "Force_tan", new armarx::Variant(force_tan.norm()));
                debugObserver->setDebugDatafield("DoorOpener", "Force_rad", new armarx::Variant(force_rad.norm()));
                debugObserver->setDebugDatafield("DoorOpener", "Velocity_rad_added", new armarx::Variant(vel_rad.norm()));
                //posHelper.setFeedForwardVelocity(velocity + vel_rad, velOri); //

                robotLayer.add(viz::Sphere("current_goal")
                               .pose(goal)
                               .radius(40.0f)
                               .colorGlasbeyLUT(4));
                robotViz.joints(rns->getJointValueMap());
                arviz.commit(robotLayer);

            }
            if (currentPhase == PhaseType::Release)
            {


                ARMARX_INFO << "STATE: Release";
                posHelper.immediateHardStop();
                handHelper.shapeHand(0.0f, 0.0f);
                for (int i = 0; i < 17; i++)
                {
                    viz::Sphere s = viz::Sphere("traj" + std::to_string(i));
                    arviz.layerContaining("Trajectory" + std::to_string(i), s).clear();
                    arviz.commit(arviz.layerContaining("Trajectory" + std::to_string(i), s));
                }
                //break;
            }

            Eigen::Vector3f force_tan = velocity.normalized() * velocity.normalized().dot(force);
            Eigen::Vector3f force_rad = force - force_tan;
            viz::Arrow forceviz_tan = viz::Arrow("force_tan");
            forceviz_tan.fromTo(tcp->getPositionInRootFrame(), tcp->getPositionInRootFrame() + 20 * force_tan);
            forceviz_tan.width(10);
            forceviz_tan.color(viz::Color::blue());
            viz::Arrow forceviz_rad = viz::Arrow("force_rad");
            forceviz_rad.fromTo(tcp->getPositionInRootFrame(), tcp->getPositionInRootFrame() + 20 * force_rad);
            forceviz_rad.width(10);
            forceviz_rad.color(viz::Color::green());
            forceLayer.add(forceviz_tan);
            forceLayer.add(forceviz_rad);
            if (currentPhase != PhaseType::Approach)
            {
                posHelper.updateWrite();
            }
            robotViz.joints(rns->getJointValueMap());
            arviz.commit(robotLayer);
            viz::Arrow forceviz = viz::Arrow("force");
            forceviz.fromTo(tcp->getPositionInRootFrame(), tcp->getPositionInRootFrame() + 20 * force);
            forceviz.width(10);
            forceviz.color(viz::Color::red());
            forceLayer.add(forceviz);
            arviz.commit(forceLayer);
            currentPhase = nextPhase;
            c.waitForCycleDuration();
            double duration = c.getAverageDuration().toMilliSecondsDouble();
            debugObserver->setDebugDatafield("DoorOpener", "CycleDuration", new armarx::Variant(duration));

        }
        rightController->deactivateController();
        rightController->deleteController();
        //        posHelper.immediateHardStop();
        //        posHelper.updateWrite();
    }
}
