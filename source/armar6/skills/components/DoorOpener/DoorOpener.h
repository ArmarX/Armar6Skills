/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::DoorOpener
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>
#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
//#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>



namespace armarx
{
    /**
     * @class DoorOpenerPropertyDefinitions
     * @brief Property definitions of `DoorOpener`.
     */
    class DoorOpenerPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        DoorOpenerPropertyDefinitions(std::string prefix);
    };

    using namespace armarx::RemoteGui::Client;
    struct DoorOpenerTab : Tab
    {
        Button execute;
        Button openHand;
        Button finish;
        Label labelIsSimulation;
        CheckBox isSimulation;
        Label labelCompliance;
        FloatSlider compliance;
        Label labelAdditionalApproachDoor;
        FloatSlider additionalApproachDoor;
        Label labelApproachHandle;
        FloatSlider approachHandle;
        Label LabelForceThresholdContactDoor;
        FloatSlider forceThresholdContactDoor;
        Label LabelForceThresholdContactHandle;
        FloatSlider forceThresholdContactHandle;
        FloatSlider zHeight;



    };

    struct DoorOpenerState
    {
        bool openHand = false;
        bool finish = false;
        bool isSimulation = false;
        float compliance = 3.0f;
        float additionalApproachDoor = 60.0f;
        float approachHandle = 250.0f;
        float forceThresholdContactDoor = 5.0f;
        float forceThresholdContactHandle = 14.0f;
        float zHeight = 1280.0f;
    };




    /**
     * @defgroup Component-DoorOpener DoorOpener
     * @ingroup armar6_skills-Components
     * A description of the component DoorOpener.
     *
     * @class DoorOpener
     * @ingroup Component-DoorOpener
     * @brief Brief description of class DoorOpener.
     *
     * Detailed description of class DoorOpener.
     */
    class DoorOpener
        : virtual public armarx::Component
        , virtual public armarx::RobotStateComponentPluginUser
        , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        , virtual public armarx::ArVizComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;
        //static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        //static SubClassRegistry Registry;
        enum class PhaseType
        {
            DoorContact,
            Preshape,
            ApproachHandle,
            Grasp,
            Pull,
            Release
        };

    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void createRemoteGuiTab();
        void RemoteGui_update() override;
        Eigen::Vector3f CircleMotionT(Eigen::Vector3f, float);

    private:
        void controlLoop();

    private:
        RunningTask<DoorOpener>::pointer_type task;

        // Private methods and member variables go here.

        /// Debug observer. Used to visualize e.g. time series.
        armarx::DebugObserverInterfacePrx debugObserver;

        RobotUnitInterfacePrx robotUnit;
        ObserverInterfacePrx robotUnitObserver;
        KinematicUnitInterfacePrx kinematicUnit;
        ForceTorqueUnitObserverInterfacePrx forceTorqueUnit;

        VirtualRobot::RobotPtr robot;

        DoorOpenerTab tab;

        std::mutex stateMutex;
        DoorOpenerState state;
        PhaseType nextPhase;
    };
}
